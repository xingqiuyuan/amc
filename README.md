Aerosol Modeling for CHIMERE (but not only)
===========================================

Brief history
-------------

Particles have become a significant part of air pollution and are already included in air dispersion models
for a long time. Nevertheless, as particle dynamics was being studied, they became more and more complex to
represent, ranging from a few nanometers to tenth of micrometers, incorporating numerous chemical species
(especially organics), emitted from various anthropic/biogenic sources, each one characterized by a specific
chemical composition (external mixing).

Most current models rely on simplifying assumptions, mainly because of the computing time requirements, which
prevent from giving sounder predictions for the particle concentration level. Therefore, INERIS engaged in two
Ph-D in order to relax these assumptions. One was led by Marion Devilliers on the modeling of nanoparticles,
another by Hilel Dergaoui on the mathematical representation of the external mixing.

AMC takes over the Ph-Ds results and aims at building an operational aerosol model free of any limiting
assumptions, representing as accurately as possible atmospheric particles in terms of size range, chemical
compositions, particle layers, while preserving the computing time.

Organisation
------------

AMC is split in several modules :
* base : core part.
* discretization : size and composition discretization.
* dynamics : compute evolution rates for dynamic processes (coagulation, condensation:evaporation, nucleation).
* new_particle_formation : specific model to handle the dynamics of nucleated nanoparticles.
* numerics : numerical solvers performing the time integration.
* parameterization : parameterizations of micro-physical processes
* redistribution : how particles are redistributed among existing sizes and compositions.
* repartition_coefficient : coagulation repartition coefficients.
* share : independent modules shared by other ones, such as modal distributions.
* species : where are stored and managed chemical species and tracers.
* thermodynamics : thermodynamic models driving the condensation/evaporation process.
