// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_METEOROLOGICAL_DATA_HXX

namespace AMC
{
  /*! 
   * \class ClassMeteorologicalData
   */
  class ClassMeteorologicalData : protected ClassSpecies
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Temperature in Kelvin.*/
    static real temperature_;

    /*!< Pressure in Pascal.*/
    static real pressure_;

    /*!< Relative humidity adim [0, 1].*/
    static real relative_humidity_;

    /*!< Eddy dissipation rate, given as ratio with cinematic air viscosity in s^{-2}.*/
    static real eddy_dissipation_rate_;

    /*!< Air dynamic viscosity.*/
    static real air_dynamic_viscosity_;

    /*!< Water dynamic viscosity.*/
    static real water_dynamic_viscosity_;

    /*!< Air free mean path.*/
    static real air_free_mean_path_;

    /*!< Air density.*/
    static real air_density_;

    /*!< Water surface tension.*/
    static real water_surface_tension_;

    /*!< Sulfuric acid saturation vapor pressure.*/
    static real sulfuric_acid_saturation_vapor_pressure_;

    /*!< Gas diffusivity.*/
    static vector1r gas_diffusivity_;

    /*!< Gas quadratic mean velocity.*/
    static vector1r gas_quadratic_mean_velocity_;

    /*!< Saturation vapor pressure.*/
    static vector1r saturation_vapor_pressure_;

    /*!< Saturation vapor concentration.*/
    static vector1r saturation_vapor_concentration_;

    /*!< Partition coefficient.*/
    static vector1r partition_coefficient_;

    /*!< Henry constant.*/
    static vector1r henry_constant_;

    /*!< Function sorting the saturation vapor concentration field.*/
    static bool sort_saturation_vapor_concentration(int i, int j);

    /*!< Map of variables.*/
    static map<string, real* > variable_0d_;
    static map<string, vector<real>* > variable_1d_;
    static map<string, string> variable_0d_unit_;
    static map<string, string> variable_1d_unit_;

#ifdef AMC_WITH_GERBER
    /*!< Gerber parameterization is my friend.*/
    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const vector1i &parameterization_section,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;

    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_SURFACE_TENSION
    friend void ClassParameterizationSurfaceTensionAverage::ComputeSurfaceTension(const int &section_min,
                                                                                  const int &section_max,
                                                                                  const vector1i &parameterization_section,
                                                                                  const real *concentration_aer_number,
                                                                                  const real *concentration_aer_mass) const;

#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
    friend void ClassParameterizationSurfaceTensionJacobson::ComputeSurfaceTension(const int &section_min,
                                                                                   const int &section_max,
                                                                                   const vector1i &parameterization_section,
                                                                                   const real *concentration_aer_number,
                                                                                   const real *concentration_aer_mass) const;
#endif
#endif

    /*!< Thermodynamic methods are my friends.*/
#ifdef AMC_WITH_ISOROPIA
    friend void ClassModelThermodynamicIsoropia::ComputeLocalEquilibrium(const int &section_min,
                                                                         const int &section_max,
                                                                         const vector1i &section_index,
                                                                         const real *concentration_aer_number,
                                                                         const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropia::ComputeGlobalEquilibrium(const int &section_min,
                                                                          const int &section_max,
                                                                          const real *concentration_gas,
                                                                          const real *concentration_aer_mass,
                                                                          vector1r &concentration_aer_total,
                                                                          vector1r &equilibrium_aer_internal,
                                                                          real &liquid_water_content,
                                                                          vector1r &concentration_aer_equilibrium,
                                                                          vector1r &concentration_gas_equilibrium) const;
#endif

#ifdef AMC_WITH_PANKOW
    friend void ClassModelThermodynamicPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &section_index,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicPankow::ComputeGlobalEquilibrium(const int &section_min,
                                                                        const int &section_max,
                                                                        const real *concentration_gas,
                                                                        const real *concentration_aer_mass,
                                                                        vector1r &concentration_aer_total,
                                                                        vector1r &equilibrium_aer_internal,
                                                                        real &liquid_water_content,
                                                                        vector1r &concentration_aer_equilibrium,
                                                                        vector1r &concentration_gas_equilibrium) const;
#endif

#ifdef AMC_WITH_AEC
    friend void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                     const int &section_max,
                                                                     const real *concentration_gas,
                                                                     const real *concentration_aer_mass,
                                                                     vector1r &concentration_aer_total,
                                                                     vector1r &equilibrium_aer_internal,
                                                                     real &liquid_water_content,
                                                                     vector1r &concentration_aer_equilibrium,
                                                                     vector1r &concentration_gas_equilibrium) const;
#endif

#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
    friend void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                            const int &section_max,
                                                                            const vector1i &section_index,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropiaAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                             const int &section_max,
                                                                             const real *concentration_gas,
                                                                             const real *concentration_aer_mass,
                                                                             vector1r &concentration_aer_total,
                                                                             vector1r &equilibrium_aer_internal,
                                                                             real &liquid_water_content,
                                                                             vector1r &concentration_aer_equilibrium,
                                                                             vector1r &concentration_gas_equilibrium) const;
#endif

    /*!< Coagulation parameterization ComputeKernel methods are also my friends.*/
#ifdef AMC_WITH_COAGULATION
    friend void ClassParameterizationCoagulationVoid::ComputeKernel(const vector2i &couple,
                                                                    Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationUnit::ComputeKernel(const vector2i &couple,
                                                                    Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationTable::ComputeKernel(const vector2i &couple,
                                                                     Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationBrownian::ComputeKernel(const vector2i &couple,
                                                                        Array<real, 2> &kernel) const;
#ifdef AMC_WITH_WAALS_VISCOUS
    friend void ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(const vector2i &couple,
                                                                                    Array<real, 2> &kernel) const;
#endif
    friend void ClassParameterizationCoagulationTurbulent::ComputeKernel(const vector2i &couple,
                                                                         Array<real, 2> &kernel) const;

    template<class C>
    friend void ClassParameterizationCoagulationGravitational<C>::ComputeKernel(const vector2i &couple,
                                                                                Array<real, 2> &kernel) const;
#endif

    /*!< Even condensation parameterization ComputeKernel methods are my friends.*/
#ifdef AMC_WITH_CONDENSATION
    friend void ClassParameterizationCondensationVoid::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;
#endif
#endif

#ifdef AMC_WITH_VEHKAMAKI
    friend void ClassParameterizationNucleationVehkamaki::ComputeKernel(const int &section_index,
                                                                        const real *concentration_gas,
                                                                        vector1r &rate_aer_number,
                                                                        vector1r &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_MERIKANTO
    friend void ClassParameterizationNucleationMerikanto::ComputeKernel(const int &section_index,
                                                                        const real *concentration_gas,
                                                                        vector1r &rate_aer_number,
                                                                        vector1r &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_KELVIN_EFFECT
    friend void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                                       const int section_max,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass,
                                                                       vector1r &kelvin_effect) const;
#endif

#ifdef AMC_WITH_ADSORPTION
    friend void ClassModelAdsorptionPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    friend void NPF::ClassMeteoData::InitStep();
#endif

  public:

    /*!< Init meteorological data and related parameters.*/
    static void Init();

    /*!< Get methods.*/
    static vector<string> GetVariable0dList();
    static vector<string> GetVariable1dList();

    static map<string, real> GetVariable0d();
    static real GetVariable0d(const string name);
    static vector<real> GetVariable1d(const string name);
    static real GetVariable1d(const string name, const int i);

    static string GetVariable0dUnit(const string name);
    static string GetVariable1dUnit(const string name);
    static map<string, string> GetVariable0dUnitList();
    static map<string, string> GetVariable1dUnitList();

    /*!< Get sorted saturation vapor concentration.*/
    static void GetSortedSaturationVaporConcentration(vector<int> &index_ascending,
                                                      vector<real> &saturation_vapor_concentration);

    /*!< Set some meteorological parameters.*/
    static void SetTemperature(const real &temperature);
    static void SetPressure(const real &pressure);
    static void SetRelativeHumidity(const real &relative_humidity);

    /*!< Init at beginning of time step physical properties for dynamics
      processes which depends of meteorological parameters.*/
    static void UpdateMeteo(map<string, real> &variable);
    static void UpdateCoagulation();
    static void UpdateCondensation();
    static void UpdateDiffusion();

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_METEOROLOGICAL_DATA_HXX
#endif
