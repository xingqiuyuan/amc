// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_HXX

namespace AMC
{
  /*! 
   * \class ClassParameterization
   */
  class ClassParameterization : protected ClassAerosolData
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of diameter parameterizations.*/
    static int Nparam_diameter_;

    /*!< List of diameter parameterizations.*/
    static vector<ClassParameterizationDiameterBase* > parameterization_diameter_list_;

    /*!< Index of general sections associated to each diameter parameterization.*/
    static vector2i parameterization_diameter_section_;

    /*!< Vector of pointers to thermodynamic models for each section.*/
    static vector<ClassParameterizationDiameterBase* >  parameterization_diameter_;

#ifdef AMC_WITH_GERBER
    /*!< Standalone Gerber diameter parameterization.*/
    static ClassParameterizationDiameterGerber*  parameterization_diameter_gerber_;
#endif

#ifdef AMC_WITH_SURFACE_TENSION
    /*!< Number of surface tension parameterizations.*/
    static int Nparam_surface_tension_;

    /*!< List of surface tension parameterizations.*/
    static vector<ClassParameterizationSurfaceTensionBase* > parameterization_surface_tension_list_;

    /*!< Index of general sections associated to each diameter parameterization.*/
    static vector2i parameterization_surface_tension_section_;

    /*!< Vector of pointers to thermodynamic models for each section.*/
    static vector<ClassParameterizationSurfaceTensionBase* >  parameterization_surface_tension_;
#endif

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static ClassParameterizationDiameterBase* GetParameterizationDiameterList(const int &i);
#ifdef AMC_WITH_SURFACE_TENSION
    static ClassParameterizationSurfaceTensionBase* GetParameterizationSurfaceTensionList(const int &i);
#endif

    /*!< Compute surface tension.*/
    static void ComputeSurfaceTension(const int section_min,
                                      const int section_max,
                                      const real *concentration_aer_number,
                                      const real *concentration_aer_mass);

    /*!< Compute the runtime particle diameter and mass with respect to concentrations.*/
    static void ComputeParticleDiameter(const int section_min, const int section_max,
                                        const real *concentration_aer_number,
                                        const real *concentration_aer_mass);

#ifdef AMC_WITH_GERBER
    /*!< Compute the runtime particle diameter thanks to Gerber parameterization.*/
    static void ComputeParticleDiameterGerber(const int section_min, const int section_max,
                                              const real *concentration_aer_number,
                                              const real *concentration_aer_mass);
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_HXX
#endif
