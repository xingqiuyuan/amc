// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_METEOROLOGICAL_DATA_CXX

#include "ClassMeteorologicalData.hxx"

namespace AMC
{
  // Function sorting the saturation vapor concentration field.
  bool ClassMeteorologicalData::sort_saturation_vapor_concentration(int i, int j)
  {
    return ClassMeteorologicalData::saturation_vapor_concentration_[i]
      < ClassMeteorologicalData::saturation_vapor_concentration_[j];
  }


  // Init meteorological data and related parameters.
  void ClassMeteorologicalData::Init()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset()
                         << "Init meteorological data with default values." << endl;
#endif

    // Temperature.
    temperature_ = real(AMC_TEMPERATURE_REFERENCE);

    // Pressure.
    pressure_ = real(AMC_PRESSURE_REFERENCE);

    // Relative humidity.
    relative_humidity_ = real(AMC_RELATIVE_HUMIDITY_DEFAULT);

    // Eddy dissipation rate.
    eddy_dissipation_rate_ = real(AMC_EDDY_DISSIPATION_RATE_DEFAULT);

    // Air dynamic viscosity.
    air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);

    // Water dynamic viscosity. Needed for gravitational coagulation.
    water_dynamic_viscosity_ = real(AMC_WATER_DYNAMIC_VISCOSITY_DEFAULT);

    // Air free mean path.
    air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);

    // Air density.
    air_density_ = real(AMC_AIR_DENSITY_DEFAULT);

    // Surface tension of water.
    water_surface_tension_ = real(AMC_WATER_SURFACE_TENSION_DEFAULT);

    // Gas diffusivity.
    gas_diffusivity_.assign(ClassSpecies::Ngas_, real(0));

    // Gas quadratic mean velocity.
    gas_quadratic_mean_velocity_.assign(ClassSpecies::Ngas_, real(0));

    // Saturation vapor pressure.
    saturation_vapor_pressure_.assign(ClassSpecies::Ngas_, real(0));

    // Saturation vapor concentration.
    saturation_vapor_concentration_.assign(ClassSpecies::Ngas_, real(0));

    // Partition coefficient.
    partition_coefficient_.assign(ClassSpecies::Ngas_, real(0));

    // Henry constant.
    henry_constant_.assign(ClassSpecies::Ngas_, real(0));

    //
    // Map of variables.
    //

    variable_0d_["temperature"] = &temperature_; 
    variable_0d_["pressure"] = &pressure_; 
    variable_0d_["relative humidity"] = &relative_humidity_; 
    variable_0d_["eddy dissipation rate"] = &eddy_dissipation_rate_; 
    variable_0d_["air dynamic viscosity"] = &air_dynamic_viscosity_;
    variable_0d_["water dynamic viscosity"] = &water_dynamic_viscosity_;
    variable_0d_["air free mean path"] = &air_free_mean_path_;
    variable_0d_["air density"] = &air_density_;
    variable_0d_["water surface tension"] = &water_surface_tension_;

    variable_1d_["gas diffusivity"] = &gas_diffusivity_;
    variable_1d_["gas quadratic mean velocity"] = &gas_quadratic_mean_velocity_;
    variable_1d_["saturation vapor pressure"] = &saturation_vapor_pressure_;
    variable_1d_["saturation vapor concentration"] = &saturation_vapor_concentration_;
    variable_1d_["partition coefficient"] = &partition_coefficient_;
    variable_1d_["henry constant"] = &henry_constant_;

    variable_0d_unit_["temperature"] = "K";
    variable_0d_unit_["pressure"] = "Pa";
    variable_0d_unit_["relative humidity"] = "[0, 1]";
    variable_0d_unit_["eddy dissipation rate"] = "m^2.s^{-3}";
    variable_0d_unit_["air dynamic viscosity"] = "kg.m^{-1}.s^{-1}";
    variable_0d_unit_["water dynamic viscosity"] = "kg.m^{-1}.s^{-1}";
    variable_0d_unit_["air free mean path"] = "µm";
    variable_0d_unit_["air density"] = "kg.m^{-3}";
    variable_0d_unit_["water surface tension"] = "N.m^{-1}";

    variable_1d_unit_["gas diffusivity"] = "m^2.s^{-1}";
    variable_1d_unit_["gas quadratic mean velocity"] = "m.s^{-1}";
    variable_1d_unit_["saturation vapor pressure"] = "Pa";
    variable_1d_unit_["saturation vapor concentration"] = "µg.m^{-3}";
    variable_1d_unit_["partition coefficient"] = "m^3.µg^{-1}";
    variable_1d_unit_["henry constant"] = "mol.L^{-1}.atm^{-1}";

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "temperature = " << temperature_ << " K" << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "pressure = " << pressure_ << " Pa" << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "relative_humidity = " << relative_humidity_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "eddy dissipation rate = "
                         << eddy_dissipation_rate_ << " m^2.s^{-3}" << endl;
#endif
  }


  // Get methods.
  vector<string> ClassMeteorologicalData::GetVariable0dList()
  {
    vector<string> name;

    for (map<string, real* >::iterator it = variable_0d_.begin(); it != variable_0d_.end(); ++it)
      name.push_back(it->first);

    return name;
  }


  vector<string> ClassMeteorologicalData::GetVariable1dList()
  {
    vector<string> name;

    for (map<string, vector<real>* >::iterator it = variable_1d_.begin(); it != variable_1d_.end(); ++it)
      name.push_back(it->first);

    return name;
  }


  real ClassMeteorologicalData::GetVariable0d(const string name)
  {
    for (map<string, real* >::iterator it = variable_0d_.begin(); it != variable_0d_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return *(it->second);

    // If nothing found, return negative value.
    return real(-1);
  }


  map<string, real> ClassMeteorologicalData::GetVariable0d()
  {
    map<string, real> variable;

    for (map<string, real* >::iterator it = variable_0d_.begin(); it != variable_0d_.end(); ++it)
      variable[it->first] = *(it->second);

    return variable;
  }


  vector<real> ClassMeteorologicalData::GetVariable1d(const string name)
  {
    for (map<string, vector<real>* >::iterator it = variable_1d_.begin(); it != variable_1d_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return *(it->second);

    // If nothing found, return empty vector.
    return vector<real>();
  }


  real ClassMeteorologicalData::GetVariable1d(const string name, const int i)
  {
    if (i < ClassSpecies::Ngas_)
      for (map<string, vector<real>* >::iterator it = variable_1d_.begin(); it != variable_1d_.end(); ++it)
        if ((it->first).find(name) != string::npos)
          return (*it->second)[i];

    // If nothing found, return negative value.
    return real(-1);
  }


  string ClassMeteorologicalData::GetVariable0dUnit(const string name)
  {
    for (map<string, string>::iterator it = variable_0d_unit_.begin(); it != variable_0d_unit_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return it->second;

    // If nothing found, return empty string.
    return "";
  }


  string ClassMeteorologicalData::GetVariable1dUnit(const string name)
  {
    for (map<string, string>::iterator it = variable_1d_unit_.begin(); it != variable_1d_unit_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return it->second;

    // If nothing found, return empty string.
    return "";
  }


  map<string, string> ClassMeteorologicalData::GetVariable0dUnitList()
  {
    return variable_0d_unit_;
  }


  map<string, string> ClassMeteorologicalData::GetVariable1dUnitList()
  {
    return variable_1d_unit_;
  }


  // Get sorted saturation vapor concentration.
  void ClassMeteorologicalData::GetSortedSaturationVaporConcentration(vector<int> &index_ascending,
                                                                      vector<real> &saturation_vapor_concentration)
  {
    index_ascending.resize(ClassSpecies::Ngas_);
    for (int i = 0; i < ClassSpecies::Ngas_; ++i)
      index_ascending[i] = i;

    // Sort them in ascending order.
    std::sort(index_ascending.begin(), index_ascending.end(), sort_saturation_vapor_concentration);

    saturation_vapor_concentration.assign(ClassSpecies::Ngas_, real(0));

    for (int i = 0; i < ClassSpecies::Ngas_; ++i)
      {
        saturation_vapor_concentration[i] = ClassMeteorologicalData::saturation_vapor_concentration_[index_ascending[i]];
        index_ascending[i] = ClassSpecies::semivolatile_[index_ascending[i]];
      }
  }


  // Set some meteorological parameters.
  void ClassMeteorologicalData::SetTemperature(const real &temperature)
  {
    temperature_ = temperature;
  }


  void ClassMeteorologicalData::SetPressure(const real &pressure)
  {
    pressure_ = pressure;
  }


  void ClassMeteorologicalData::SetRelativeHumidity(const real &relative_humidity)
  {
    relative_humidity_ = relative_humidity;
  }


  // Init at beginning of time step physical properties for dynamics
  // processes which depends of meteorological parameters.
  void ClassMeteorologicalData::UpdateMeteo(map<string, real> &variable)
  {
    temperature_ = AMC_TEMPERATURE_REFERENCE;
    pressure_ = AMC_PRESSURE_REFERENCE;
    relative_humidity_ = AMC_RELATIVE_HUMIDITY_DEFAULT;
    eddy_dissipation_rate_ = AMC_EDDY_DISSIPATION_RATE_DEFAULT;

    map<string, real>::iterator it;

    if ((it = variable.find("temperature")) != variable.end())
      {
        temperature_ = it->second;
#ifdef AMC_WITH_DEBUG_METEO
        CBUG << "temperature = " << temperature_ << endl;
#endif
      }

    if ((it = variable.find("pressure")) != variable.end())
      {
        pressure_ = it->second;
#ifdef AMC_WITH_DEBUG_METEO
        CBUG << "pressure = " << pressure_ << endl;
#endif
      }

    if ((it = variable.find("relative_humidity")) != variable.end())
      {
        relative_humidity_ = it->second;
#ifdef AMC_WITH_DEBUG_METEO
        CBUG << "relative_humidity = " << relative_humidity_ << endl;
#endif
      }

    if ((it = variable.find("eddy_dissipation_rate")) != variable.end())
      {
        eddy_dissipation_rate_ = it->second;
#ifdef AMC_WITH_DEBUG_METEO
        CBUG << "eddy_dissipation_rate = " << eddy_dissipation_rate_ << endl;
#endif
      }
  }


  // Init at beginning of time step physical properties for
  // coagulation process which depends of meteorological parameters.
  void ClassMeteorologicalData::UpdateCoagulation()
  {
#ifdef AMC_WITH_DEBUG_METEO
    CBUG << "temperature = " << temperature_ << endl;
    CBUG << "pressure = " << pressure_ << endl;
    CBUG << "relative_humidity = " << relative_humidity_ << endl;
#endif

    // Air dynamic viscosity.
    air_dynamic_viscosity_ = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature_);

    // Water dynamic viscosity. Needed for gravitational coagulation.
    water_dynamic_viscosity_ = ClassParameterizationPhysics::ComputeWaterDynamicViscosity(temperature_);

    // Air free mean path.
    air_free_mean_path_ = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature_, pressure_);

    // Air density.
    air_density_ = ClassParameterizationPhysics::ComputeAirDensity(temperature_, pressure_,
                                                                   relative_humidity_);
#ifdef AMC_WITH_DEBUG_METEO
    CBUG << "air_dynamic_viscosity = " << air_dynamic_viscosity_ << endl;
    CBUG << "water_dynamic_viscosity = " << water_dynamic_viscosity_ << endl;
    CBUG << "air_free_mean_path = " << air_free_mean_path_ << endl;
    CBUG << "air_density = " << air_density_ << endl;
#endif
  }


  void ClassMeteorologicalData::UpdateCondensation()
  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
    ClassCondensationFluxCorrectionDryInorganicSalt::Update(temperature_);
#endif

#ifdef AMC_WITH_DEBUG_METEO
    CBUG << "temperature = " << temperature_ << endl;
    CBUG << "pressure = " << pressure_ << endl;
#endif

    // Surface tension of water.
    water_surface_tension_ = ClassParameterizationPhysics::ComputeWaterSurfaceTension(temperature_);

    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      {
        const int j = ClassSpecies::semivolatile_[i];

        // Molar mass in kg / mol.
        real molar_mass = ClassSpecies::molar_mass_[j] * 1.e-9;

        // Gas diffusivity.
        gas_diffusivity_[i] = ClassParameterizationPhysics::ComputeGasDiffusivity(temperature_, pressure_,
                                                                                  ClassSpecies::collision_factor_[j],
                                                                                  ClassSpecies::molecular_diameter_[j],
                                                                                  molar_mass);
        // Gas velocity.
        gas_quadratic_mean_velocity_[i] = ClassParameterizationPhysics::ComputeGasQuadraticMeanVelocity(temperature_, molar_mass);

        // Van't Hoff coefficient for saturation concentration or
        // partition coefficient change due to change in temperature.
        const real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature_, real(1),
                                              ClassSpecies::vaporization_enthalpy_[j]);

        // If the Antoine law is defined, prefer it, otherwise uses the vaporization enthalpy.
        if (ClassSpecies::use_antoine_law_[j])
          saturation_vapor_pressure_[i] = pow(10, ClassSpecies::antoine_law_a_[j]
                                              - ClassSpecies::antoine_law_b_[j] / temperature_);
        else
          saturation_vapor_pressure_[i] = vant_hoff_coefficient * ClassSpecies::saturation_vapor_pressure_[j];

        // Saturation vapor concentration.
        saturation_vapor_concentration_[i] = vant_hoff_coefficient * ClassSpecies::saturation_vapor_concentration_[j]
          * (AMC_TEMPERATURE_REFERENCE / temperature_);

        // The partition coefficient is like the inverse of the saturation vapor concentration.
        partition_coefficient_[i] = ClassSpecies::partition_coefficient_[j]
          * (temperature_ / AMC_TEMPERATURE_REFERENCE) / vant_hoff_coefficient;

        // Henry's constant.
        henry_constant_[i] = ClassSpecies::henry_constant_[j] / vant_hoff_coefficient;
      }
  }


  void ClassMeteorologicalData::UpdateDiffusion()
  {

  }


  // Clear static data.
  void ClassMeteorologicalData::Clear()
  {
    // Always clear mas first.
    variable_0d_.clear();
    variable_1d_.clear();
    variable_0d_unit_.clear();
    variable_1d_unit_.clear();

    temperature_ = real(AMC_TEMPERATURE_REFERENCE);
    pressure_ = real(AMC_PRESSURE_REFERENCE);
    relative_humidity_ = real(AMC_RELATIVE_HUMIDITY_DEFAULT);
    eddy_dissipation_rate_ = real(AMC_EDDY_DISSIPATION_RATE_DEFAULT);
    air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);
    water_dynamic_viscosity_ = real(AMC_WATER_DYNAMIC_VISCOSITY_DEFAULT);
    water_surface_tension_ = real(AMC_WATER_SURFACE_TENSION_DEFAULT);
    air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);
    air_density_ = real(AMC_AIR_DENSITY_DEFAULT);
    sulfuric_acid_saturation_vapor_pressure_ = real(0);
    gas_diffusivity_.clear();
    gas_quadratic_mean_velocity_.clear();
    saturation_vapor_pressure_.clear();
    saturation_vapor_concentration_.clear();
    partition_coefficient_.clear();
    henry_constant_.clear();
  }
}

#define AMC_FILE_CLASS_METEOROLOGICAL_DATA_CXX
#endif
