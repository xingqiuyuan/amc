// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AMX_CXX

#include "ClassAMX.hxx"

namespace AMC
{
  // Type.
  template<class S, class R, class D, class C>
  int ClassAMX<S, R, D, C>::Type()
  {
    return S::Type() * 1000 + R::Type() * 100 +  D::Type() * 10 + C::Type();
  }


  // Info.
  template<class S, class R, class D, class C>
  string ClassAMX<S, R, D, C>::Info()
  {
    ostringstream oss;
    oss << "AMC built with composition discretization of type \""
        << (D::Type() == AMC_DISCRETIZATION_COMPOSITION_TYPE_BASE ? "base" : "table")
        << "\", with size redistribution of type \"";

    if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID)
      oss << "Euler Hybrid";
    else if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED)
      oss << "Euler Mixed";
    else if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER)
      oss << "Moving Diameter";
    else if (S::Type() >= AMC_REDISTRIBUTION_SIZE_TYPE_TABLE)
      {
        oss << "Tabulated of ";
        if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID)
          oss << "Euler Hybrid";
        else if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED)
          oss << "Euler Mixed";
        else if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER)
          oss << "Moving Diameter";
        else
          throw AMC::Error("Unknown size redistribution type (" + to_str(S::Type() % 10) + ").");
      }
    else
      throw AMC::Error("Unknown size redistribution type (" + to_str(S::Type()) + ").");

    oss << "\", with composition redistribution of type \""
        << (R::Type() == AMC_REDISTRIBUTION_COMPOSITION_TYPE_BASE ? "base" : "table");

#ifdef AMC_WITH_COAGULATION
    // C has no Type() otherwise.
    oss << "\" and with coagulation repartition coefficient of type \""
        << (C::Type() == AMC_COEFFICIENT_REPARTITION_TYPE_STATIC ? "static" : "moving");
#endif

    oss << "\".";

    return oss.str();
  }


  // Is AMC initialized ?
  template<class S, class R, class D, class C>
  bool ClassAMX<S, R, D, C>::IsInitialized()
  {
    return ClassConfiguration::IsInitialized<init_flag::amx>();
  }


  // Initiate all AMC modules.
  template<class S, class R, class D, class C>
  void ClassAMX<S, R, D, C>::Initiate(const string configuration_file, const string prefix)
  {
    if (ClassConfiguration::IsInitialized<init_flag::amx>())
      throw AMC::Error("AMX already initialized.");

    // Configuration.
    ClassConfiguration::Init(configuration_file, prefix);

    Ops::Ops &ops = *ClassConfiguration::GetOps();

    // Main class.
    ClassAMC<D>::Init(ops);

    // Redistribution.
    ClassRedistribution<S, R, D>::Init(ops);

    // Micro-physical processes.
#ifdef AMC_WITH_COAGULATION
    ClassDynamicsCoagulation<C>::Init(ops);
    ClassNumericalSolverCoagulation<C>::Init(ops);
#endif

#ifdef AMC_WITH_CONDENSATION
    ClassDynamicsCondensation::Init(ops);
    ClassNumericalSolverCondensation::Init(ops);
#endif

#ifdef AMC_WITH_NUCLEATION
    ClassDynamicsNucleation::Init(ops);
#endif

#ifdef AMC_WITH_DIFFUSION
    ClassDynamicsDiffusion::Init(ops);
#endif

    // What was actually read ?
    ClassConfiguration::WhatWasRead();

    // At last, say that we have initialized.
    ClassConfiguration::SetInitialized<init_flag::amx>();
  }


  // Terminate.
  template<class S, class R, class D, class C>
  void ClassAMX<S, R, D, C>::Terminate()
  {
    if (! ClassConfiguration::IsInitialized<init_flag::amx>())
      throw AMC::Error("AMX not yet initialized.");

    // Micro-physical processes.
#ifdef AMC_WITH_NUCLEATION
    ClassDynamicsNucleation::Clear();
#endif

#ifdef AMC_WITH_CONDENSATION
    ClassNumericalSolverCondensation::Clear();
    ClassDynamicsCondensation::Clear();
#endif

#ifdef AMC_WITH_COAGULATION
    ClassNumericalSolverCoagulation<C>::Clear();
    ClassDynamicsCoagulation<C>::Clear();
#endif

    // Redistribution.
    ClassRedistribution<S, R, D>::Clear();

    // Dry correction flux data.
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
    ClassCondensationFluxCorrectionDryInorganicSalt::Clear();
#endif

    // Main class.
    ClassAMC<D>::Clear();

    // This also unset initialization flag.
    ClassConfiguration::Clear();
  }


  /*!< Forward in time.*/
  template<class S, class R, class D, class C>
  void ClassAMX<S, R, D, C>::Forward(real *concentration_aer_number,
                                     real *concentration_aer_mass,
                                     real *concentration_gas,
                                     const real time_step)
  {
  }


  //
  // Partial specialization.
  //


  // Type.
  template<class S, class R, class D>
  int ClassAMX<S, R, D, int>::Type()
  {
    return S::Type() * 100 + R::Type() * 10 + D::Type();
  }


  // Info.
  template<class S, class R, class D>
  string ClassAMX<S, R, D, int>::Info()
  {
    ostringstream oss;
    oss << "AMC built with composition discretization of type \""
        << (D::Type() == AMC_DISCRETIZATION_COMPOSITION_TYPE_BASE ? "base" : "table")
        << "\", with size redistribution of type \"";

    if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID)
      oss << "Euler Hybrid";
    else if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED)
      oss << "Euler Mixed";
    else if (S::Type() == AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER)
      oss << "Moving Diameter";
    else if (S::Type() >= AMC_REDISTRIBUTION_SIZE_TYPE_TABLE)
      {
        oss << "Tabulated of ";
        if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID)
          oss << "Euler Hybrid";
        else if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED)
          oss << "Euler Mixed";
        else if ((S::Type() % 10) == AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER)
          oss << "Moving Diameter";
        else
          throw AMC::Error("Unknown size redistribution type (" + to_str(S::Type() % 10) + ").");
      }
    else
      throw AMC::Error("Unknown size redistribution type (" + to_str(S::Type()) + ").");

    oss << "\", with composition redistribution of type \""
        << (R::Type() == AMC_REDISTRIBUTION_COMPOSITION_TYPE_BASE ? "base" : "table");

    oss << "\".";

    return oss.str();
  }


  // Is AMC initialized ?
  template<class S, class R, class D>
  bool ClassAMX<S, R, D, int>::IsInitialized()
  {
    return ClassConfiguration::IsInitialized<init_flag::amx>();
  }


  // Initiate all AMC modules.
  template<class S, class R, class D>
  void ClassAMX<S, R, D, int>::Initiate(const string configuration_file, const string prefix)
  {
    if (ClassConfiguration::IsInitialized<init_flag::amx>())
      throw AMC::Error("AMX already initialized.");

    // Configuration.
    ClassConfiguration::Init(configuration_file, prefix);

    Ops::Ops &ops = *ClassConfiguration::GetOps();

    // Main class.
    ClassAMC<D>::Init(ops);

    // Redistribution.
    ClassRedistribution<S, R, D>::Init(ops);

#ifdef AMC_WITH_CONDENSATION
    ClassDynamicsCondensation::Init(ops);
    ClassNumericalSolverCondensation::Init(ops);
#endif

#ifdef AMC_WITH_NUCLEATION
    ClassDynamicsNucleation::Init(ops);
#endif

#ifdef AMC_WITH_DIFFUSION
    ClassDynamicsDiffusion::Init(ops);
#endif

    // What was actually read ?
    ClassConfiguration::WhatWasRead();

    // At last, say that we have initialized.
    ClassConfiguration::SetInitialized<init_flag::amx>();
  }


  // Terminate.
  template<class S, class R, class D>
  void ClassAMX<S, R, D, int>::Terminate()
  {
    if (! ClassConfiguration::IsInitialized<init_flag::amx>())
      throw AMC::Error("AMX not yet initialized.");

    // Micro-physical processes.
#ifdef AMC_WITH_NUCLEATION
    ClassDynamicsNucleation::Clear();
#endif

#ifdef AMC_WITH_CONDENSATION
    ClassNumericalSolverCondensation::Clear();
    ClassDynamicsCondensation::Clear();
#endif

    // Redistribution.
    ClassRedistribution<S, R, D>::Clear();

    // Dry correction flux data.
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
    ClassCondensationFluxCorrectionDryInorganicSalt::Clear();
#endif

    // Main class.
    ClassAMC<D>::Clear();

    // This also unset initialization flag.
    ClassConfiguration::Clear();
  }


  /*!< Forward in time.*/
  template<class S, class R, class D>
  void ClassAMX<S, R, D, int>::Forward(real *concentration_aer_number,
                                       real *concentration_aer_mass,
                                       real *concentration_gas,
                                       const real time_step)
  {
  }
}

#define AMC_FILE_CLASS_AMX_CXX
#endif
