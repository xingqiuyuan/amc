// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_THERMODYNAMICS_CXX

#include "ClassThermodynamics.hxx"

namespace AMC
{
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
  // Partition general sections between threads.
  inline void ClassThermodynamics::thread_partition_(const int section_min, const int section_max)
  {
    // Need to redefine thread partition because of varying min and max sections.
    const int Ng = section_max - section_min;

    Ng_thread_.assign(Nthread_, Ng / Nthread_);
    for (int i = 0; i < Ng % Nthread_; ++i)
      Ng_thread_[i]++;

    // Section offset per threads.
    thread_offset_.assign(Nthread_, 0);
    thread_offset_.front() = section_min;
    for (int i = 1; i < Nthread_; ++i)
      thread_offset_[i] = thread_offset_[i - 1] + Ng_thread_[i - 1];

#ifdef AMC_WITH_DEBUG_THERMODYNAMIC
    CBUG << "Ng = " << Ng << endl;
    CBUG << "Ng_thread = " << Ng_thread_ << endl;
    CBUG << "thread_offset = " << thread_offset_ << endl;
#endif
  }
#endif

  // Init.
  void ClassThermodynamics::Init(Ops::Ops &ops)
  {
    const string prefix_orig = ops.GetPrefix();

    // Clear everything.
    ClassThermodynamics::Clear();

    //
    // Thermodynamic models.
    //

    ops.SetPrefix(prefix_orig + ".thermodynamic.");
    vector1s model_thermo_list = ops.GetEntryList();
    ops.Set("list", "", model_thermo_list);
    Nmodel_thermo_ = int(model_thermo_list.size());

    model_thermodynamic_list_.resize(Nmodel_thermo_);
    model_thermodynamic_section_.resize(Nmodel_thermo_);

    vector1b section_assigned(ClassAerosolData::Ng_, false);

    if (Nmodel_thermo_ == 0)
      throw AMC::Error("No thermodynamic model specified, provide at least one, like \"void\" if thermodynamic is not to be used.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset()
                         << "Number of thermodynamic models = " << Nmodel_thermo_ << endl;
#endif

    for (int i = 0; i < Nmodel_thermo_; i++)
      {
        ops.SetPrefix(prefix_orig + ".thermodynamic." + model_thermo_list[i] + ".");
        string type = ops.Get<string>("type");

        if (ops.Exists("section"))
          {
            if (ops.Exists("section.min") || ops.Exists("section.max"))
              {
                int section_min = ops.Get<int>("section.min", "", 0);
                int section_max = ops.Get<int>("section.max", "", ClassAerosolData::Ng_);
                int Nsection = section_max - section_min;
                model_thermodynamic_section_[i].resize(Nsection);
                for (int j = 0; j < Nsection; j++)
                  model_thermodynamic_section_[i][j] = section_min + j;
              }
            else
              ops.Set("section", "", model_thermodynamic_section_[i]);
          }

        if (model_thermodynamic_section_[i].empty())
          {
            model_thermodynamic_section_[i].resize(ClassAerosolData::Ng_);
            for (int j = 0; j < ClassAerosolData::Ng_; j++)
              model_thermodynamic_section_[i][j] = j;
          }

        if (type == "void")
          {
            ClassModelThermodynamicVoid model(false);
            model_thermodynamic_list_[i] = model.clone();
          }
#ifdef AMC_WITH_ISOROPIA
        else if (type == "isoropia")
          {
            ClassModelThermodynamicIsoropia model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
#ifdef AMC_WITH_PANKOW
        else if (type == "pankow")
          {
            ClassModelThermodynamicPankow model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
#ifdef AMC_WITH_AEC
        else if (type == "aec")
          {
            ClassModelThermodynamicAEC model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
        else if (type == "isoropia_aec")
          {
            ClassModelThermodynamicIsoropiaAEC model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
#ifdef AMC_WITH_H2O
        else if (type == "h2o")
          {
            ClassModelThermodynamicH2O model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
        else if (type == "isoropia_h2o")
          {
            ClassModelThermodynamicIsoropiaH2O model(ops, false);
            model_thermodynamic_list_[i] = model.clone();
          }
#endif
        else
          throw AMC::Error("Not implemented thermodynamic model \"" + type + "\".");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "Associating general sections "
                             << model_thermodynamic_section_[i] << " with thermodynamic model \""
                             << model_thermo_list[i] << "\" of type \""
                             << model_thermodynamic_list_[i]->GetName() << "\"." << endl;
#endif

        for (int j = 0; j < int(model_thermodynamic_section_[i].size()); j++)
          section_assigned[model_thermodynamic_section_[i][j]] = true;
      }

    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      if (! section_assigned[i])
        throw AMC::Error("General section " + to_str(i) + " has not been assigned any thermodynamic model.");

    // Pointers to thermodynamic model on a per section basis.
    model_thermodynamic_.resize(ClassAerosolData::Ng_);

    for (int i = 0; i < Nmodel_thermo_; i++)
      for (int j = 0; j < int(model_thermodynamic_[i].size()); j++)
        model_thermodynamic_[model_thermodynamic_section_[i][j]].
          push_back(model_thermodynamic_list_[i]);

#ifdef AMC_WITH_THREAD_THERMODYNAMIC
    //
    // Thread configuration.
    //

    ops.SetPrefix(prefix_orig + ".thermodynamic.");

    // Number of CPU.
    const int number_cpu = util::get_cpu_number();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(2) << Reset() << "util::get_cpu_number() found " << number_cpu
                         << " cores on this computer, so at most " << number_cpu
                         << " threads may be run concurrently." << endl;
#endif

    // By default we do not set to the maximum because thread efficiency tends to be reduced.
    Nthread_ = ops.Get<int>("thread_number", "", number_cpu - 2);
    if (Nthread_ >= number_cpu)
      throw AMC::Error("Requested number of threads (" + to_str(Nthread_) +
                       ") exceeds the number of CPU (" + to_str(number_cpu) + ").");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info() << Reset()
                         << "Compute thermodynamics with " << Nthread_ << " threads." << endl;
#endif

    thread_partition_(0, ClassAerosolData::Ng_);

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < Nthread_; i++)
      *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "thread " << i
                           << " : Ng_thread = " << Ng_thread_[i]
                           << ", thread_offset = " << thread_offset_[i] << endl;
#endif
#endif

#ifdef AMC_WITH_ADSORPTION
    //
    // Adsorption model. For the moment allow only one adsorption model.
    //

    ops.SetPrefix(prefix_orig + ".");
    if (ops.Exists("adsorption"))
      {
        ops.SetPrefix(prefix_orig + ".adsorption.");

        string type = ops.Get<string>("type");

        if (ops.Exists("section"))
          ops.Set("section", "", model_adsorption_section_);
          
        if (model_adsorption_section_.empty())
          {
            model_adsorption_section_.resize(ClassAerosolData::Ng_);
            for (int j = 0; j < ClassAerosolData::Ng_; j++)
              model_adsorption_section_[j] = j;
          }

        if (type == "pankow")
          model_adsorption_ = new ClassModelAdsorptionPankow(ops);
        else
          throw AMC::Error("Not implemented adsorption model \"" + type + "\".");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Associating general sections "
                             << model_adsorption_section_ << " with adsorption model of type \""
                             << model_adsorption_->GetName() << "\"." << endl;
#endif
      }
    else
      {
        model_adsorption_ = NULL;
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "No adsorption model set." << endl;
#endif
      }
#endif
  }


  // Clear method.
  void ClassThermodynamics::Clear()
  {
    Nmodel_thermo_ = 0;
    model_thermodynamic_list_.clear();
    model_thermodynamic_.clear();
    model_thermodynamic_section_.clear();
#ifdef AMC_WITH_ADSORPTION
    if (model_adsorption_ != NULL)
      {
        delete model_adsorption_;
        model_adsorption_ = NULL;
      }

    model_adsorption_section_.clear();
#endif
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
    Nthread_ = 0;
    Ng_thread_.clear();
    thread_offset_.clear();
#endif
  }


  // Get methods.
  int ClassThermodynamics::GetNmodel()
  {
    return Nmodel_thermo_;
  }


  ClassModelThermodynamicBase* ClassThermodynamics::GetModelList(const int i)
  {
    return model_thermodynamic_list_[i];
  }


#ifdef AMC_WITH_ADSORPTION
  ClassModelAdsorptionBase* ClassThermodynamics::GetModelAdsorption()
  {
    return model_adsorption_;
  }
#endif


#ifdef AMC_WITH_TEST
  // Test thermodynamics.
  void ClassThermodynamics::Test(const real *concentration_aer_number,
                                 const real *concentration_aer_mass,
                                 const int N)
  {
    // Test on all general sections.
    int section_min(0), section_max(ClassAerosolData::Ng_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Test AMC Thermodynamics with N = " << N << endl;
#endif

#ifdef AMC_WITH_TIMER
    AMCTimer<CPU>::Clear();
    AMCTimer<Wall>::Clear();
    int time_index = AMCTimer<CPU>::Add("test_thermodynamic_amc");
    time_index = AMCTimer<Wall>::Add("test_thermodynamic_amc");
    AMCTimer<CPU>::Start();
    AMCTimer<Wall>::Start();
#endif

    for (int i = 0; i < N; i++)
      ClassThermodynamics::Compute(section_min, section_max,
                                   concentration_aer_number,
                                   concentration_aer_mass);

#ifdef AMC_WITH_LOGGER
#ifdef AMC_WITH_TIMER
    AMCTimer<CPU>::Stop(time_index);
    AMCTimer<Wall>::Stop(time_index);
    *AMCLogger::GetLog() << Bmagenta() << Debug() << Reset() << "CPU time = "
                         << AMCTimer<CPU>::GetTimer(time_index) << " seconds, ("
                         <<  AMCTimer<CPU>::GetTimer(time_index) / float(N > 0 ? N : 1)
                         << " seconds/test)" << endl;
    *AMCLogger::GetLog() << Bmagenta() << Debug() << Reset() << "Wall elapsed time = "
                         << AMCTimer<Wall>::GetTimer(time_index) << " seconds, ("
                         <<  AMCTimer<Wall>::GetTimer(time_index) / float(N > 0 ? N : 1)
                         << " seconds/test)" << endl;
#endif
#endif
  }
#endif


  // Compute thermodynamics.
  void ClassThermodynamics::Compute(const int section_min,
                                    const int section_max,
                                    const real *concentration_aer_number,
                                    const real *concentration_aer_mass)
  {
    // Reset data.
    for (int i = section_min; i < section_max; ++i)
      {
        ClassAerosolData::liquid_water_content_[i] = real(0);

        int j = i * ClassSpecies::Ngas_ - 1;
        for (int k = 0; k < ClassSpecies::Ngas_; ++k)
          {
            ClassAerosolData::adsorption_coefficient_[j] = real(-1);
            ClassAerosolData::equilibrium_gas_surface_[++j] = real(-1);
          }

        j = i * ClassSpeciesEquilibrium::Nspecies_ - 1;
        for (int k = 0; k < ClassSpeciesEquilibrium::Nspecies_; k++)
          ClassAerosolData::equilibrium_aer_internal_[++j] = real(0);
      }

#ifdef AMC_WITH_THREAD_THERMODYNAMIC
    // Need to redefine thread partition because of varying min and max sections.
    thread_partition_(section_min, section_max);

#pragma omp parallel num_threads(Nthread_)
    {
      const int thread_index = omp_get_thread_num();

      const int thread_section_min = thread_offset_[thread_index];
      const int thread_section_max = thread_section_min + Ng_thread_[thread_index];

      for (int i = 0; i < Nmodel_thermo_; ++i)
        model_thermodynamic_list_[i]->ComputeLocalEquilibrium(thread_section_min,
                                                              thread_section_max,
                                                              model_thermodynamic_section_[i],
                                                              concentration_aer_number,
                                                              concentration_aer_mass);
    }
#else
    for (int i = 0; i < Nmodel_thermo_; ++i)
      model_thermodynamic_list_[i]->ComputeLocalEquilibrium(section_min,
                                                            section_max,
                                                            model_thermodynamic_section_[i],
                                                            concentration_aer_number,
                                                            concentration_aer_mass);
#endif

#ifdef AMC_WITH_ADSORPTION
    if (model_adsorption_ != NULL)
      model_adsorption_->ComputeLocalEquilibrium(section_min,
                                                 section_max,
                                                 model_adsorption_section_,
                                                 concentration_aer_number,
                                                 concentration_aer_mass);
#endif
  }
}

#define AMC_FILE_CLASS_THERMODYNAMIC_CXX
#endif
