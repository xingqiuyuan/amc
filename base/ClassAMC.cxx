// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AMC_CXX

#include "ClassAMC.hxx"

namespace AMC
{
  // Init method.
  template<class D>
  void ClassAMC<D>::Init(const string configuration_file, const string prefix)
  {
    // Configuration.
    ClassConfiguration::Init(configuration_file, prefix);

    // Main class.
    ClassAMC<D>::Init(*ClassConfiguration::GetOps());
  }


  template<class D>
  void ClassAMC<D>::Init(Ops::Ops &ops)
  {
    // Clear everything.
    ClassAMC<D>::Clear();

    const string prefix = ClassConfiguration::GetPrefix() + ".";

    //
    // Read species data.
    //

    ops.SetPrefix(prefix);
    ClassSpecies::Init(ops);

    ops.SetPrefix(prefix);
    ClassSpeciesEquilibrium::Init(ops);

    //
    // Read phase data.
    //

    ops.SetPrefix(prefix);
    ClassPhase::Init(ops);

    // Meteorological related data.
    ClassMeteorologicalData::Init();

    //
    // Read discretization (size and composition).
    //

    ops.SetPrefix(prefix);
    ClassDiscretization<D>::Init(ops);

    // Proxies.
    ClassAerosolData::Ng_ = ClassDiscretization<D>::Ng_;
    ClassAerosolData::general_section_  = ClassDiscretization<D>::general_section_;
    ClassAerosolData::general_section_reverse_  = ClassDiscretization<D>::general_section_reverse_;

#ifdef AMC_WITH_LAYER
    //
    // Particle layers.
    //

    ops.SetPrefix(prefix);
    ClassLayer::Init(ops);
#endif

    //
    // Aerosol Data.
    //

    ClassAerosolData::Init();

    //
    // Display info on general sections, subsequent to previous settings.
    //

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      {
        const ClassDiscretizationComposition<D> *internal_mixing_ptr =
          ClassDiscretizationClass<D>::discretization_composition_[general_section_[i][0]]
          ->GetClassInternalMixing(general_section_[i][1]);
        *AMCLogger::GetLog() << Bblue() << Info(1) << Reset() << "General section " << i << " :" << endl;
        *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "\tsize section = "
                             << ClassAerosolData::general_section_[i][0] << endl;
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "\tcomposition class = "
                             << ClassAerosolData::general_section_[i][1] << endl;
        *AMCLogger::GetLog() << Fgreen() << Info(2) << Reset() << "\t\tcomposition class name = \""
                             << internal_mixing_ptr->GetName() << "\"." << endl;
#ifdef AMC_WITH_TRACE
        *AMCLogger::GetLog() << Fgreen() << Info(2) << Reset() << "\t\tis trace ?  "
                             << (internal_mixing_ptr->IsTrace() ? "Yes" : "No") << endl;
#endif
#ifdef AMC_WITH_LAYER
        *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "\tnumber of layers = "
                             << ClassLayer::Nlayer_[i] << endl;
#endif
      }
#endif

    // Thermodynamic models.
    ops.SetPrefix(ClassConfiguration::GetPrefix());
    ClassThermodynamics::Init(ops);

    // Parameterizations.
    ops.SetPrefix(ClassConfiguration::GetPrefix());
    ClassParameterization::Init(ops);

    // We are up.
    ClassConfiguration::SetInitialized<init_flag::amc>();
  }


  // Is AMC initialized ?
  template<class D>
  bool ClassAMC<D>::IsInitialized()
  {
    return (ClassConfiguration::IsInitialized<init_flag::amc>() ||
            ClassConfiguration::IsInitialized<init_flag::amx>());
  }


  // Clear method.
  template<class D>
  void ClassAMC<D>::Clear()
  {
    // Parameterizations.
    ClassParameterization::Clear();

    // Thermodynamics.
    ClassThermodynamics::Clear();

    // Aerosol Data.
    ClassAerosolData::Clear();

#ifdef AMC_WITH_LAYER
    ClassLayer::Clear();
#endif
    ClassDiscretization<D>::Clear();
    ClassMeteorologicalData::Clear();
    ClassPhase::Clear();
    ClassSpeciesEquilibrium::Clear();
    ClassSpecies::Clear();

    // We are down.
    ClassConfiguration::SetInitialized<init_flag::no>();
  }


  // Generate modal distribution.
#ifdef AMC_WITH_MODAL
  template<class D>
  void ClassAMC<D>::GenerateModalDistribution(const ClassModalDistribution<real> &modal_distribution,
                                              real *concentration_aer_number,
                                              real *concentration_aer_mass,
                                              vector<real> &diameter_mean,
                                              vector<vector<real> > &class_distribution)
  {
    // Compute one sectional distribution from given
    // modal distribution and model size discretization.
    vector1r distribution_number, distribution_mass;

    modal_distribution.ComputeSectionalDistribution(ClassDiscretizationSize::diameter_bound_,
                                                    diameter_mean, distribution_number,
                                                    distribution_mass);

    // The mass distribution  given from modal distribution is in fact
    // one volume distribution. So we convert it into one mass distribution.
    // the 1.e6 factor translates from cm^{-3} to m^{-3}.
    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      {
        // from #.cm-3 to #.m-3.
        distribution_number[i] *= real(1e6);
        // from µm3.cm-3 to µg.m-3.
        distribution_mass[i] *= ClassDiscretizationSize::density_fixed_ * real(1e6);
      }

    // Allocate model concentration arrays.
    //concentration_aer_number = new real[ClassAerosolData::Ng_];
    //concentration_aer_mass = new real[ClassAerosolData::NgNspecies_];
    // Let allocate outside of function.

    // How we distribute particles among several classes. Random if not given. 
    if (int(class_distribution.size()) == 0)
      {
        class_distribution.resize(ClassDiscretizationSize::Nsection_);
        for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
          generate_random_vector(ClassDiscretizationClass<D>::Nclass_[i], class_distribution[i], real(1));
      }

    // Fill concentration vectors.
    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      {
        vector1r *class_distribution_ptr;
        if (int(class_distribution.size()) ==  ClassDiscretizationSize::Nsection_)
          class_distribution_ptr = &class_distribution[i];
        else
          class_distribution_ptr = &class_distribution[0];

        for (int j = 0; j < ClassDiscretizationClass<D>::Nclass_[i]; j++)
          {
            const int k = ClassAerosolData::general_section_reverse_[i][j];
            concentration_aer_number[k] = distribution_number[i] * (*class_distribution_ptr)[j];

            vector1r composition(ClassSpecies::Nspecies_);

            ClassDiscretizationClass<D>::discretization_composition_[i]->GenerateRandomComposition(j, composition);

            int l = k * ClassSpecies::Nspecies_;
            for (int m = 0; m < ClassSpecies::Nspecies_; m++)
              concentration_aer_mass[l++] = distribution_mass[i]
                * (*class_distribution_ptr)[j] * composition[m];
          }
      }

    // Avoid too few concentrations in some sections.
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      if (concentration_aer_number[i] <= AMC_NUMBER_CONCENTRATION_MINIMUM)
        {
          concentration_aer_number[i] = real(0);
          const int j = ClassSpecies::Nspecies_ * i;
          for (int k = 0; k < ClassSpecies::Nspecies_; ++k)
            concentration_aer_mass[j + k] = real(0);
        }
  }
#endif


  // Compute internally mixed distribution from external mixing.
  template<class D>
  void ClassAMC<D>::ComputeConcentrationIM(const real *concentration_aer_number,
                                           const real *concentration_aer_mass,
                                           vector<real> &concentration_aer_number_im,
                                           vector<real> &concentration_aer_mass_im)
  {
    concentration_aer_number_im.assign(ClassDiscretizationSize::Nsection_, real(0));
    concentration_aer_mass_im.assign(ClassDiscretizationSize::Nsection_ * ClassSpecies::Nspecies_, real(0));

    int i(-1);
    for (int j = 0; j < ClassDiscretizationSize::Nsection_; j++)
      for (int k = 0; k < ClassDiscretizationClass<D>::Nclass_[j]; k++)
        concentration_aer_number_im[j] += concentration_aer_number[++i];

    i = -1;
    for (int j = 0; j < ClassDiscretizationSize::Nsection_; j++)
      for (int k = 0; k < ClassDiscretizationClass<D>::Nclass_[j]; k++)
        for (int l = 0; l < ClassSpecies::Nspecies_; l++)
          concentration_aer_mass_im[j * ClassSpecies::Nspecies_ + l] += concentration_aer_mass[++i];
  }


  // Compute external mixing degree.
  template<class D>
  void ClassAMC<D>::ComputeExternalMixingDegree(const real *concentration_aer_number,
                                                const real *concentration_aer_mass,
                                                vector<real> &degree_num,
                                                vector<vector<real> > &degree_mass)
  {
    degree_num.assign(ClassDiscretizationSize::Nsection_, real(0));
    degree_mass.assign(ClassDiscretizationSize::Nsection_, vector1r(ClassSpecies::Nspecies_, real(0)));

    int h(0), i(0);

    for (int j = 0; j < ClassDiscretizationSize::Nsection_; j++)
      {
        int hold(h);
        int iold(i);

        real concentration_num_average(real(0));
        vector1r concentration_mass_average(ClassSpecies::Nspecies_, real(0));

        for (int k = 0; k < ClassDiscretizationClass<D>::Nclass_[j]; k++)
          {
            concentration_num_average += concentration_aer_number[h++];
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              concentration_mass_average[l] += concentration_aer_mass[i++];
          }

        const real div = real(1) / real(ClassDiscretizationClass<D>::Nclass_[j]);

        concentration_num_average *= div;
        for (int l = 0; l < ClassSpecies::Nspecies_; l++)
          concentration_mass_average[l] *= div;

        // Come back to beginning of section.
        h = hold;
        i = iold;

        for (int k = 0; k < ClassDiscretizationClass<D>::Nclass_[j]; k++)
          {
            real tmp = concentration_aer_number[h++] - concentration_num_average;
            degree_num[j] += tmp * tmp;

            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              {
                tmp = concentration_aer_mass[i++] - concentration_mass_average[l];
                degree_mass[j][l] += tmp * tmp;
              }
          }

        if (concentration_num_average > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            degree_num[j] = sqrt(degree_num[j] * div) / concentration_num_average;
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              degree_mass[j][l] = sqrt(degree_mass[j][l] * div) / concentration_mass_average[l];
          }
      }
  }
}

#define AMC_FILE_CLASS_AMC_CXX
#endif
