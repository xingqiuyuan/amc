// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AMX_HXX

namespace AMC
{
  /*! 
   * \class ClassAMX
   * \brief Executable class.
   */
  template<class S, class R, class D, class C = int>
  class ClassAMX
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef S redistribution_size_type;
    typedef R redistribution_composition_type;
    typedef D discretization_composition_type;
    typedef C coagulation_repartition_coefficient_type;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Info.*/
    static string Info();

    /*!< Is AMC initialized ?.*/
    static bool IsInitialized();

    /*!< Initiate.*/ 
    static void Initiate(const string configuration_file = "", const string prefix = "");


    /*!< Terminate.*/ 
    static void Terminate();


    /*!< Forward in time.*/
    static void Forward(real *concentration_aer_number,
                        real *concentration_aer_mass,
                        real *concentration_gas,
                        const real time_step);
  };


  /*!< Partial specialization.*/
  template<class S, class R, class D>
  class ClassAMX<S, R, D, int>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef S redistribution_size_type;
    typedef R redistribution_composition_type;
    typedef D discretization_composition_type;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Info.*/
    static string Info();

    /*!< Is AMC initialized ?.*/
    static bool IsInitialized();

    /*!< Initiate.*/ 
    static void Initiate(const string configuration_file = "", const string prefix = "");


    /*!< Terminate.*/ 
    static void Terminate();


    /*!< Forward in time.*/
    static void Forward(real *concentration_aer_number,
                        real *concentration_aer_mass,
                        real *concentration_gas,
                        const real time_step);
  };
}

#define AMC_FILE_CLASS_AMX_HXX
#endif
