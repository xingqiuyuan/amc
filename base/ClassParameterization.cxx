// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CXX

#include "ClassParameterization.hxx"

namespace AMC
{
  void ClassParameterization::Init(Ops::Ops &ops)
  {
    const string prefix_orig = ops.GetPrefix();

    // Clear everything.
    ClassParameterization::Clear();

    //
    // Diameter parameterizations.
    //

    ops.SetPrefix(prefix_orig + ".diameter.");
    vector1s param_diameter_name = ops.GetEntryList();
    Nparam_diameter_ = int(param_diameter_name.size());

    parameterization_diameter_list_.resize(Nparam_diameter_);
    parameterization_diameter_section_.resize(Nparam_diameter_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset()
                         << "Number of diameter parameterizations = " << Nparam_diameter_ << endl;
#endif

    int k(0);
    for (int i = 0; i < Nparam_diameter_; i++)
      {
        ops.SetPrefix(prefix_orig + ".diameter." + param_diameter_name[i] + ".");
        string type = ops.Get<string>("type");

        if (ops.Exists("section"))
          ops.Set("section", "", parameterization_diameter_section_[k]);

        if (type == "fixed")
          {
            ClassParameterizationDiameterFixed model(ops);
            parameterization_diameter_list_[k] = model.clone();
          }
        else if (type == "density_fixed")
          {
            ClassParameterizationDiameterDensityFixed model(ops);
            parameterization_diameter_list_[k] = model.clone();
          }
        else if (type == "density_moving")
          {
            ClassParameterizationDiameterDensityMoving model(ops);
            parameterization_diameter_list_[k] = model.clone();
          }
#ifdef AMC_WITH_FRACTAL_SOOT
        else if (type == "fractal_soot")
          {
            ClassParameterizationDiameterFractalSoot model(ops);
            parameterization_diameter_list_[k] = model.clone();
          }
#endif
#ifdef AMC_WITH_GERBER
        else if (type == "gerber")
          {
            ClassParameterizationDiameterGerber model(ops, ClassAerosolData::Ng_);

            if (ops.Get<bool>("standalone", "", false))
              {
                parameterization_diameter_list_[k] = model.clone(); 
                model.GetParameterizationSection(parameterization_diameter_section_[k]);
              }
            else
              {
                parameterization_diameter_gerber_ = model.clone();
                continue;
              }
          }
#endif
        else
          throw AMC::Error("Not implemented diameter parameterization \"" + type + "\".");

        // If nothing was given, default to all sections.
        if (parameterization_diameter_section_[k].empty())
          {
            parameterization_diameter_section_[k].resize(Ng_);
            for (int j = 0; j < Ng_; j++)
              parameterization_diameter_section_[k][j] = j;
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Associating general sections "
                             << parameterization_diameter_section_[k] << " with diameter parameterization \""
                             << param_diameter_name[k] << "\" of type \""
                             << parameterization_diameter_list_[k]->GetName() << "\"." << endl;
#endif

        // Increment counter.
        k++;
      }

    if (k < Nparam_diameter_)
      {
        Nparam_diameter_ = k;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Byellow() << Warning(1) << Reset() << "Resizing parameterization diameter arrays"
                             << ", new number of diameter parameterizations = " << Nparam_diameter_ << endl;
#endif

        parameterization_diameter_list_.resize(Nparam_diameter_);
        parameterization_diameter_section_.resize(Nparam_diameter_);
      }

    // Pointers to diameter parameterization on a per section basis.
    parameterization_diameter_.resize(ClassAerosolData::Ng_);

    for (int i = 0; i < Nparam_diameter_; i++)
      for (int j = 0; j < int(parameterization_diameter_section_[i].size()); j++)
        parameterization_diameter_[parameterization_diameter_section_[i][j]] = parameterization_diameter_list_[i];

#if defined(AMC_WITH_GERBER) && defined(AMC_WITH_LOGGER)
    if (parameterization_diameter_gerber_ == NULL)
      *AMCLogger::GetLog() << Bmagenta() << Warning(1) << Reset()
                           << "No standalone Gerber parameterization defined,"
                           << " the \"ComputeDiameterGerber()\" method will have no effect." << endl;
#endif

    //
    // Surface tension parameterizations.
    //

#ifdef AMC_WITH_SURFACE_TENSION
    ops.SetPrefix(prefix_orig + ".surface_tension.");
    vector1s param_surface_tension_name = ops.GetEntryList();
    Nparam_surface_tension_ = int(param_surface_tension_name.size());

    parameterization_surface_tension_list_.resize(Nparam_surface_tension_);
    parameterization_surface_tension_section_.resize(Nparam_surface_tension_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Number of surface_tension parameterizations = "
                         << Nparam_surface_tension_ << endl;
#endif

    for (int i = 0; i < Nparam_surface_tension_; i++)
      {
        ops.SetPrefix(prefix_orig + ".surface_tension." + param_surface_tension_name[i] + ".");
        string type = ops.Get<string>("type");

        if (ops.Exists("section"))
          ops.Set("section", "", parameterization_surface_tension_section_[i]);
          
        if (parameterization_surface_tension_section_[i].empty())
          {
            parameterization_surface_tension_section_[i].resize(ClassAerosolData::Ng_);
            for (int j = 0; j < ClassAerosolData::Ng_; j++)
              parameterization_surface_tension_section_[i][j] = j;
          }

        if (type == "fixed")
          {
            ClassParameterizationSurfaceTensionFixed model(ops, ClassAerosolData::Ng_);
            parameterization_surface_tension_list_[i] = model.clone();
          }
        else if (type == "average")
          {
            ClassParameterizationSurfaceTensionAverage model(ops);
            parameterization_surface_tension_list_[i] = model.clone();
          }
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
        else if (type == "jacobson")
          {
            ClassParameterizationSurfaceTensionJacobson model(ops);
            parameterization_surface_tension_list_[i] = model.clone();
          }
#endif
        else
          throw AMC::Error("Not implemented surface tension parameterization \"" + type + "\".");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Associating general sections "
                             << parameterization_surface_tension_section_[i]
                             << " with surface_tension parameterization \""
                             << param_surface_tension_name[i] << "\" of type \""
                             << parameterization_surface_tension_list_[i]->GetName() << "\"." << endl;
#endif
      }

    // Pointers to surface_tension parameterization on a per section basis.
    parameterization_surface_tension_.resize(ClassAerosolData::Ng_);

    for (int i = 0; i < Nparam_surface_tension_; i++)
      for (int j = 0; j < int(parameterization_surface_tension_section_[i].size()); j++)
        parameterization_surface_tension_[parameterization_surface_tension_section_[i][j]]
          = parameterization_surface_tension_list_[i];
#endif
  }


  // Clear method.
  void ClassParameterization::Clear()
  {
    Nparam_diameter_ = 0;
    parameterization_diameter_list_.clear();
    parameterization_diameter_.clear();
    parameterization_diameter_section_.clear();
#ifdef AMC_WITH_GERBER
    if (parameterization_diameter_gerber_ != NULL)
      delete parameterization_diameter_gerber_;
#endif
#ifdef AMC_WITH_SURFACE_TENSION
    Nparam_surface_tension_ = 0;
    parameterization_surface_tension_list_.clear();
    parameterization_surface_tension_.clear();
    parameterization_surface_tension_section_.clear();
#endif
  }


  // Get methods.
  ClassParameterizationDiameterBase* ClassParameterization::GetParameterizationDiameterList(const int &i)
  {
    return parameterization_diameter_list_[i];
  }

#ifdef AMC_WITH_SURFACE_TENSION
  ClassParameterizationSurfaceTensionBase* ClassParameterization::
  GetParameterizationSurfaceTensionList(const int &i)
  {
    return parameterization_surface_tension_list_[i];
  }
#endif


  // Compute surface tension.
  void ClassParameterization::ComputeSurfaceTension(const int section_min,
                                                    const int section_max,
                                                    const real *concentration_aer_number,
                                                    const real *concentration_aer_mass)
  {
    // Reset data.
    int h(section_min * ClassPhase::Nphase_ - 1);
    for (int i = section_min; i < section_max; ++i)
      ClassAerosolData::surface_tension_[++h] = AMC_SURFACE_TENSION_DEFAULT;

#ifdef AMC_WITH_SURFACE_TENSION
    // Compute surface tension.
    for (int i = 0; i < Nparam_surface_tension_; i++)
      parameterization_surface_tension_list_[i]->
        ComputeSurfaceTension(section_min,
                                 section_max,
                                 parameterization_surface_tension_section_[i],
                                 concentration_aer_number,
                                 concentration_aer_mass);
#endif
  }


  // Compute the runtime particle diameter and mass with respect to concentrations.
  void ClassParameterization::ComputeParticleDiameter(const int section_min,
                                                      const int section_max,
                                                      const real *concentration_aer_number,
                                                      const real *concentration_aer_mass)
  {
    // Reset data.
    ClassAerosolData::SetDiameter(section_min, section_max);

    // Compute diameter and single mass.
    for (int i = 0; i < Nparam_diameter_; i++)
      parameterization_diameter_list_[i]->ComputeDiameter(section_min,
                                                             section_max,
                                                             parameterization_diameter_section_[i],
                                                             concentration_aer_number,
                                                             concentration_aer_mass);
  }


#ifdef AMC_WITH_GERBER
  // Compute the runtime particle diameter thanks to Gerber parameterization.
  void ClassParameterization::ComputeParticleDiameterGerber(const int section_min,
                                                            const int section_max,
                                                            const real *concentration_aer_number,
                                                            const real *concentration_aer_mass)
  {
    // Reset data.
    ClassAerosolData::SetDiameter(section_min, section_max);

    // Compute diameter and single mass.
    if (parameterization_diameter_gerber_ != NULL)
      parameterization_diameter_gerber_->ComputeDiameter(section_min,
                                                         section_max,
                                                         concentration_aer_number,
                                                         concentration_aer_mass);
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CXX
#endif
