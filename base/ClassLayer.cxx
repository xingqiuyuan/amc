// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_LAYER_CXX

#include "ClassLayer.hxx"

namespace AMC
{
  // Init.
  void ClassLayer::Init(Ops::Ops &ops)
  {
    const int Nspecies = ClassSpecies::GetNspecies();
    const int Ng = ClassAerosolData::GetNg();
    vector2i general_section_reverse;
    ClassAerosolData::GetGeneralSectionReverse(general_section_reverse);

    const string prefix_orig = ops.GetPrefix();
    ops.SetPrefix(prefix_orig + "layer.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info() << Reset() << "Init particle layers :" << endl;
#endif

    // By default all species are layered.
    is_species_layered_.assign(Nspecies, AMC_LAYER_SPECIES_DEFAULT);

    if (ops.Exists("species"))
      {
        const bool is_species_layered = ops.Get<bool>("species.default", "", AMC_LAYER_SPECIES_DEFAULT);
        is_species_layered_.assign(Nspecies, is_species_layered);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "\tset all species to "
                             << (is_species_layered ? "yes" : "no") << endl;
#endif

        const vector1s entry_name = ops.GetEntryList("species");

        for (int i = 0; i < int(entry_name.size()); i++)
          if (entry_name[i] != "default")
            {
              const int species_index = ClassSpecies::GetIndex(entry_name[i]);
              if (species_index < 0)
                throw AMC::Error("Species \"" + entry_name[i] + "\" is not in model list.");

              const bool is_species_layered = ops.Get<bool>("species." + entry_name[i]);
              is_species_layered_[species_index] = ops.Get<bool>("species." + entry_name[i]);

#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset() << "\tset species \""
                                   << entry_name[i] << "\" to " << (is_species_layered ? "yes" : "no") << endl;
#endif
            }
      }

    for (int i = 0; i < Nspecies; ++i)
      if (is_species_layered_[i])
        species_index_layer_.push_back(i);
    Nspecies_layer_ = int(species_index_layer_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Debug(2) << Reset() << "is_species_layered = " << is_species_layered_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "Nspecies_layer = " << Nspecies_layer_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "species_index_layer = " << species_index_layer_ << endl;
#endif


    // By default, only one layer, that is to say no layers.
    Nlayer_.assign(Ng, AMC_LAYER_NUMBER_DEFAULT);
    layer_radius_fraction_.assign(Ng, vector1r(AMC_LAYER_NUMBER_DEFAULT + 1, 0));
    for (int i = 0; i < Ng; i++)
      layer_radius_fraction_[i].back() = real(1);

    if (ops.Exists("radius_fraction"))
      {
        const vector1s entry_name = ops.GetEntryList("radius_fraction");

        for (int i = 0; i < int(entry_name.size()); i++)
          {
            ops.SetPrefix(prefix_orig + "layer.radius_fraction." + entry_name[i] + ".");

            // The size section to which apply layers.
            const int section_size = ops.Get<int>("section_size");

            // The class compositions to which apply layers
            // within this size section, by default all of them.
            vector1i class_composition;
            for (int j = 0; j < int(general_section_reverse[section_size].size()); j++)
              class_composition.push_back(j);

            ops.Set("class_composition", "", class_composition, class_composition);

            vector1r radius_fraction;
            ops.Set("radius_fraction", "", radius_fraction);
            radius_fraction.push_back(real(1));

            vector1i general_section;
            for (int j = 0; j < int(class_composition.size()); j++)
              general_section.push_back(general_section_reverse[section_size][class_composition[j]]);

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "entry_name = \"" << entry_name[i] << "\"" << endl;
            *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "\tset layers of size section "
                                 << section_size << " and class compositions " << class_composition
                                 << " with radius fractions " << radius_fraction << endl;
            *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "\tgeneral_section = " << general_section << endl;
#endif

            // Set layers.
            const int Nlayer = int(radius_fraction.size());
            for (int j = 0; j < int(general_section.size()); j++)
              {
                const int k = general_section[j];
                Nlayer_[k] = Nlayer;
                layer_radius_fraction_[k].assign(Nlayer + 1, real(0));
                for(int l = 1; l <= Nlayer; l++)
                  layer_radius_fraction_[k][l] = radius_fraction[l - 1];
              }
          }
      }

    // Are there multiple layers ?
    multiple_layer_ = Nlayer_[0] != 1;

    if (! multiple_layer_)
      for (int i = 1; i < Ng; ++i)
        if (Nlayer_[i] > Nlayer_[i - 1])
          {
            multiple_layer_ = true;
            break;
          }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Multiple layers ? " << (multiple_layer_ ? "yes" : "no") << endl;
    for (int i = 0; i < Ng; ++i)
      *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "section = " << i << ", Nlayer = " << Nlayer_[i]
                           << ", layer_radius_fraction = " << layer_radius_fraction_[i] << endl;
#endif

    // Max number of layers across general sections
    Nlayer_max_ = 0;
    for (int i = 0; Ng; i++)
      if (Nlayer_[i] > Nlayer_max_)
        Nlayer_max_ = Nlayer_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "Nlayer_max = " << Nlayer_max_ << endl;
#endif

#ifndef AMC_WITH_FORCE_LAYER
    // If no layers but AMC was compiled with, what is the point ?
    if (! multiple_layer_)
      throw AMC::Error("AMC compiled with layers but none were defined, you should recompile with option \"layer=no\".");
#endif

    // Revert to original prefix.
    ops.SetPrefix(prefix_orig);
  }


  // Clear method.
  void ClassLayer::Clear()
  {
    multiple_layer_ = false;
    Nlayer_.clear();
    Nspecies_layer_ = 0;
    Nlayer_max_ = 0;
    is_species_layered_.clear();
    species_index_layer_.clear();
    layer_radius_fraction_.clear();
  }


  // Get methods.
  int ClassLayer::GetNlayerMax()
  {
    return Nlayer_max_;
  }

  bool ClassLayer::IsMultipleLayer()
  {
    return multiple_layer_;
  }

  int ClassLayer::GetNspeciesLayer()
  {
    return Nspecies_layer_;
  }

  void ClassLayer::GetSpeciesIndexLayer(vector<int> &species_index_layer)
  {
    species_index_layer = species_index_layer_;
  }

  void ClassLayer::GetNlayer(vector<int> &Nlayer)
  {
    Nlayer = Nlayer_;
  }

  void ClassLayer::IsSpeciesLayered(vector<bool> &is_species_layered)
  {
    is_species_layered = is_species_layered_;
  }

  void ClassLayer::GetLayerRadiusFraction(vector<vector<real> > &layer_radius_fraction)
  {
    layer_radius_fraction = layer_radius_fraction_;
  }
}

#define AMC_FILE_CLASS_LAYER_CXX
#endif
