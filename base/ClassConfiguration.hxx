// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONFIGURATION_HXX

namespace AMC
{
  class init_flag
  {
  public:
    static const int no  = 0;
    static const int amc = 1;
    static const int amx = 2;
  };

  /*! 
   * \class ClassConfiguration
   */
  class ClassConfiguration
  {
  protected:

    /*!< Is AMC initialized ?*/
    static int initialized_;

    /*!< Configuration file.*/
    static string configuration_file_;

    /*!< Prefix in configuration file.*/
    static string prefix_;

    /*!< Ops instance.*/
    static Ops::Ops *ops_;

  public:

    /*!< Init.*/
    static void Init(const string configuration_file, const string prefix);

    /*!< Is AMC initialized ?*/
    template<int N>
    static bool IsInitialized();

    /*!< Get methods.*/
    static Ops::Ops* GetOps();
    static string GetFile();
    static string GetPrefix();

    /*!< Set methods.*/
    template<int N>
    static void SetInitialized();

    /*!< Clear static data.*/
    static void Clear();

    /*!< What was read.*/
    static void WhatWasRead(string file_name = "");
  };
}

#define AMC_FILE_CLASS_CONFIGURATION_HXX
#endif
