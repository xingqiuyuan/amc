// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONFIGURATION_CXX

#include "ClassConfiguration.hxx"

namespace AMC
{
  // Init.
  void ClassConfiguration::Init(const string configuration_file, const string prefix)
  {
    // Set config file and prefix to be used.
    if (configuration_file_.empty()) configuration_file_ = configuration_file;
    if (prefix_.empty()) prefix_ = prefix;

    if (configuration_file_.empty() || prefix_.empty())
      throw AMC::Error("Configuration file or prefix is empty.");

    // Check file.
    check_file(configuration_file_);

    // Close ops if previously opened.
    if (ops_ != NULL)
      {
        delete ops_;
        ops_ = NULL;
      }

    ops_ = new Ops::Ops(configuration_file_);
    if (ops_ == NULL)
      throw AMC::Error("Failed to create Ops instance.");

    // Check prefix.
    if (! ops_->Exists(prefix_))
      throw AMC::Error("In configuration file \"" + configuration_file_ +
                       "\", could not find prefix \"" + prefix_ + "\".");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info()
                         << "Init AMC with configuration file \"" << configuration_file_
                         << "\" and prefix \"" << prefix_ << "\"." << Reset().Str() << endl;
#endif
  }


  // Is AMC initialized ?
  template<int N>
  bool ClassConfiguration::IsInitialized()
  {
    return initialized_ == N;
  }


  // Get methods.
  Ops::Ops* ClassConfiguration::GetOps()
  {
    return ops_;
  }

  string ClassConfiguration::GetFile()
  {
    return configuration_file_;
  }

  string ClassConfiguration::GetPrefix()
  {
    return prefix_;
  }


  // Set methods.
  template<int N>
  void ClassConfiguration::SetInitialized()
  {
    initialized_ = N;
  }


  // Clear static data.
  void ClassConfiguration::Clear()
  {
    initialized_ = init_flag::no;
    prefix_ = "";
    configuration_file_ = "";
    delete ops_;
  }


  // What was read.
  void ClassConfiguration::WhatWasRead(string file_name)
  {
    if (ops_ == NULL)
      return;

    // Finally write "what was read".
    ops_->SetPrefix(prefix_ + ".");

    file_name = ops_->Get<string>("what_was_read", "", file_name);

    // Eventually add prefix.
    const string::size_type pos = file_name.find("&p");
    if (pos != string::npos)
      file_name.replace(pos, 2, prefix_);

    if (! file_name.empty())
      ops_->WriteLuaDefinition(file_name);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(3) << Reset()
                         << "Write what was read from configuration file \"" << configuration_file_
                         << "\" with prefix \"" << prefix_ << "\" to path \"" << file_name << "\"." << endl;
#endif
  }
}

#define AMC_FILE_CLASS_CONFIGURATION_CXX
#endif
