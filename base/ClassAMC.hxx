// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AMC_HXX

namespace AMC
{
  /*! 
   * \class ClassAMC
   * \brief Aerosol Model for Chimere base class.
   */
  template<class D>
  class ClassAMC : protected ClassMeteorologicalData, protected ClassAerosolData, protected ClassDiscretization<D>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef D discretization_composition_type;

  public:

    /*!< Init data.*/
    static void Init(const string configuration_file = "", const string prefix = "");
    static void Init(Ops::Ops &ops);

    /*!< Is AMC initialized ?.*/
    static bool IsInitialized();
    
     /*!< Clear data.*/
    static void Clear();

#ifdef AMC_WITH_MODAL
    /*!< Generate modal distribution.*/
    static void GenerateModalDistribution(const ClassModalDistribution<real> &modal_distribution,
                                          real *concentration_aer_number,
                                          real *concentration_aer_mass,
                                          vector<real> &diameter_mean,
                                          vector<vector<real> > &class_distribution);
#endif

    /*!< Compute internally mixed concentrations from externally mixed concentrations.*/
    static void ComputeConcentrationIM(const real *concentration_aer_number,
                                       const real *concentration_aer_mass,
                                       vector<real> &concentration_aer_number_im,
                                       vector<real> &concentration_aer_mass_im);

    /*!< Compute external mixing degree.*/
    static void ComputeExternalMixingDegree(const real *concentration_aer_number,
                                            const real *concentration_aer_mass,
                                            vector<real> &degree_num,
                                            vector<vector<real> > &degree_mass);
  };
}

#define AMC_FILE_CLASS_AMC_HXX
#endif
