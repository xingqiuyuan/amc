// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AEROSOL_DATA_CXX

#include "ClassAerosolData.hxx"

namespace AMC
{
  // Init.
  void ClassAerosolData::Init()
  {
    //
    // General section vector size dependent.
    //

    NgNgas_ = Ng_ * ClassSpecies::Ngas_;
    NgNspecies_ = Ng_ * ClassSpecies::Nspecies_;
    NgNspecies_equilibrium_ = Ng_ * ClassSpeciesEquilibrium::Nspecies_;
    NgNphase_ = Ng_ * ClassPhase::Nphase_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info(1) << "Ng = " << Ng_ << Reset().Str() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "NgNgas = " << NgNgas_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "NgNspecies = " << NgNspecies_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "NgNspecies_equilibrium = " << NgNspecies_equilibrium_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset() << "NgNphase = " << NgNphase_ << endl;
#endif

    equilibrium_gas_surface_.assign(NgNgas_, real(0));
    equilibrium_aer_internal_.assign(NgNspecies_, real(0));
    liquid_water_content_.assign(Ng_, real(0));
    temperature_.assign(Ng_, AMC_TEMPERATURE_REFERENCE);
    surface_tension_.assign(NgNphase_, real(0));
    density_.assign(Ng_, ClassDiscretizationSize::density_fixed_);
    mass_.resize(Ng_);
    diameter_.resize(Ng_);
    surface_.resize(Ng_);

    // Current phase of each species.
    phase_.resize(NgNspecies_);

    // Assign it according to the species prefered phase,
    // Thermodynamics may then alter this, depending mostly on
    // weither an aqueous phase is present.
    // For most species this field remains constant.
    for (int i = 0; i < Ng_; i++)
      for (int j = 0; j < ClassSpecies::Nspecies_; j++)
        phase_[i * ClassSpecies::Nspecies_ + j] = ClassSpecies::phase_[j][0];

    // Adsorption coefficient.
    adsorption_coefficient_.assign(NgNgas_, real(-1));

    // Init size related variable with default discretization.
    SetDiameter(0, Ng_);

#ifdef AMC_WITH_LAYER
    NgNspeciesNlayer_ = 0;
    for (int i = 0; i < Ng_; i++)
      for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
        if (ClassLayer::species_index_layer_[j])
          NgNspeciesNlayer_ += Nlayer_[i];
        else
          NgNspeciesNlayer_++;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << "NgNspeciesNlayer = " << NgNspeciesNlayer_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << "Init layer repartition for each general section "
                         << "section and species with uniform distribution." << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << "You may change them by calling the method "
                         << "AerosolData::SetLayerRepartition(path) where \"path\" is a text file containing lines "
                         << "like \"species_name(string) section_size(integer) class_composition(integer) "
                         << "fraction_layer0(float) fraction_layer1(float) ..." << endl;
#endif

    // Init layer repartition with uniform distribution.
    layer_repartition_.resize(NgNspecies_);
    layer_radius_.resize(Ng_);

    for (int i = 0; i < Ng_; i++)
      {
        layer_radius_[i].assign(ClassLayer::Nlayer_[i] + 1, real(0));

        const real radius_mean = ClassDiscretizationSize::diameter_mean_[general_section_[i][0]] * real(0.5);
        for (int j = 1; j <= ClassLayer::Nlayer_[i]; ++j)
          layer_radius_[i][j] = radius_mean * ClassLayer::layer_radius_fraction_[i][j];
      }

    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (is_species_layered_[i])
        for (int j = 0; j < Ng_; j++)
          layer_repartition_[j * ClassSpecies::Nspecies_ + i].
            assign(ClassLayer::Nlayer_[j], real(1) / real(ClassLayer::Nlayer_[j]));
      else
        for (int j = 0; j < Ng_; j++)
          layer_repartition_[j * ClassSpecies::Nspecies_ + i].assign(1, real(1));
#endif
  }


  // Get methods.
  int ClassAerosolData::GetNg()
  {
    return Ng_;
  }


  int ClassAerosolData::GetNgNspecies()
  {
    return NgNspecies_;
  }


  int ClassAerosolData::GetNgNgas()
  {
    return NgNgas_;
  }


  int ClassAerosolData::GetNgNphase()
  {
    return NgNphase_;
  }


  void ClassAerosolData::GetGeneralSection(vector<vector<int> > &general_section)
  {
    general_section = general_section_;
  }


  vector<vector<int> > ClassAerosolData::GetGeneralSection()
  {
    return general_section_;
  }


  void ClassAerosolData::GetGeneralSectionReverse(vector<vector<int> > &general_section_reverse)
  {
    general_section_reverse = general_section_reverse_;
  }


  void ClassAerosolData::GetEquilibriumGasSurface(vector<real> &equilibrium_gas_surface)
  {
    equilibrium_gas_surface = equilibrium_gas_surface_;
  }


  void ClassAerosolData::GetEquilibriumAerosolInternal(vector<real> &equilibrium_aer_internal)
  {
    equilibrium_aer_internal = equilibrium_aer_internal_;
  }


  void ClassAerosolData::GetLiquidWaterContent(vector<real> &liquid_water_content)
  {
    liquid_water_content = liquid_water_content_;
  }


  void ClassAerosolData::GetDensity(vector<real> &density)
  {
    density = density_;
  }


  void ClassAerosolData::GetDiameter(vector<real> &diameter)
  {
    diameter = diameter_;
  }


  void ClassAerosolData::GetMass(vector<real> &mass)
  {
    mass = mass_;
  }


  void ClassAerosolData::GetPhase(vector1i &phase)
  {
    phase = phase_;
  }


  void ClassAerosolData::GetTemperature(vector<real> &temperature)
  {
    temperature = temperature_;
  }


  void ClassAerosolData::GetSurfaceTension(vector<real> &surface_tension)
  {
    surface_tension = surface_tension_;
  }


  void ClassAerosolData::GetAdsorptionCoefficient(vector<real> &adsorption_coefficient)
  {
    adsorption_coefficient = adsorption_coefficient_;
  }

#ifdef AMC_WITH_LAYER
  void ClassAerosolData::GetLayerRepartition(vector<vector<real> > &layer_repartition)
  {
    layer_repartition = layer_repartition_;
  }

  void ClassAerosolData::GetLayerRadius(vector<vector<real> > &layer_radius)
  {
    layer_radius = layer_radius_;
  }
#endif


  // Set methods.
  void ClassAerosolData::SetTemperature(const real &temperature)
  {
    temperature_.assign(Ng_, temperature);
  }

#ifdef AMC_WITH_LAYER
  void ClassAerosolData::SetLayerRepartition(const string &path)
  {
    // Path where to find repartition.
    check_file(path);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Set species layer repartition from file \""
                         << path << "\"." << endl;
#endif

    ifstream fin(path.c_str());

    string line;
    while (getline(fin, line))
      if (line[0] != '#')
        {
          int section_size;
          int class_composition;
          string species_name;
          fin >> species_name >> section_size >> class_composition;

          const int species_index = ClassSpecies::GetIndex(species_name);

          if (species_index >= 0)
            {
              if (ClassLayer::is_species_layered_[species_index])
                {
                  const int i = general_section_reverse_[section_size][class_composition];
                  const int j = i * ClassSpecies::Nspecies_;
                  for (int k = 0; k < Nlayer_[i]; k++)
                    {
                      fin >> layer_repartition_[j][k];
                      if (layer_repartition_[j][k] < real(0) || layer_repartition_[j][k] > real(1))
                        throw AMC::Error("Layer repartition fractions must be within [0, 1].");
                    }
                }
#ifdef AMC_WITH_LOGGER
              else
                *AMCLogger::GetLog() << Bcyan() << Warning() << Reset() << "Species \"" << species_name
                                     << "\" is not currently layered, silently ignoring." << endl;
#endif
            }
#ifdef AMC_WITH_LOGGER
          else
            *AMCLogger::GetLog() << Bmagenta() << Warning() << Reset() << "Species \"" << species_name
                                 << "\" is not part of model list, silently ignoring." << endl;
#endif
        }

    fin.close();
  }
#endif

  void ClassAerosolData::SetDiameter(const int section_min, const int section_max)
  {
    for (int i = section_min; i < section_max; i++)
      {
        const int j = general_section_[i][0];
        diameter_[i] = ClassDiscretizationSize::diameter_mean_[j];
        surface_[i] = ClassDiscretizationSize::surface_mean_[j];
        mass_[i] = ClassDiscretizationSize::mass_mean_[j];
        density_[i] = ClassDiscretizationSize::density_fixed_;
      }
  }


  // Clear method.
  void ClassAerosolData::Clear()
  {
    Ng_ = 0;
    NgNspecies_ = 0;
    NgNspecies_equilibrium_ = 0;
    NgNgas_ = 0;
    NgNphase_ = 0;
    general_section_.clear();
    general_section_reverse_.clear();
    phase_.clear();
    equilibrium_gas_surface_.clear();
    equilibrium_aer_internal_.clear();
    liquid_water_content_.clear();
    density_.clear();
    diameter_.clear();
    surface_.clear();
    mass_.clear();
    temperature_.clear();
    surface_tension_.clear();
    adsorption_coefficient_.clear();
#ifdef AMC_WITH_LAYER
    layer_repartition_.clear();
    layer_radius_.clear();
#endif
  }


  // Compute aerosol concentration mass total.
  void ClassAerosolData::ComputeConcentrationAerosolMassTotal(const real *concentration_aer_mass,
                                                              vector<real> &concentration_aer_mass_total)
  {
    concentration_aer_mass_total.assign(ClassAerosolData::Ng_, real(0));

    int h(0);
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      for (int j = 0; j < ClassSpecies::Nspecies_; j++)
        concentration_aer_mass_total[i] += concentration_aer_mass[h++];
  }


  // Compute mass conservation.
  void ClassAerosolData::ComputeMassConservation(const real *concentration_gas,
                                                 const real *concentration_aer_mass,
                                                 vector<real> &mass_conservation)
  {
    mass_conservation.assign(ClassSpecies::Nspecies_, real(0));

    int h(-1);
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          mass_conservation[j] += concentration_aer_mass[++h];

    if (concentration_gas != NULL)
      for (int i = 0; i < ClassSpecies::Ngas_; i++)
        mass_conservation[ClassSpecies::semivolatile_[i]] += concentration_gas[i];
  }


#ifdef AMC_WITH_LAYER
  // Compute the layer radius.
  void ClassAerosolData::ComputeLayerRadius(const int section_min,
                                            const int section_max,
                                            const real *concentration_aer_number,
                                            const real *concentration_aer_mass)
  {
    for (int i = section_min; i < section_max; ++i)
      {
        real mass_total(real(0));

        int j = i * ClassSpecies::Nspecies_ - 1;
        for (int k = 0; k < ClassSpecies::Nspecies_; ++k)
          mass_total += concentration_aer_mass[++j];

        real radius_dry = ClassDiscretizationSize::diameter_mean_[ClassAerosolData::general_section_[i][0]];
        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM && mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
          radius_dry = pow(mass_total / (concentration_aer_number[i] * ClassDiscretizationSize::density_fixed_), AMC_FRAC3);

        radius_dry *= real(0.5);

#ifdef AMC_WITH_DEBUG_LAYER
        CBUG << "i = " << i << endl;
        CBUG << "radius_dry = " << radius_dry << endl;
#endif

        for (int k = 1; k <= ClassLayer::Nlayer_[i]; ++k)
          ClassAerosolData::layer_radius_[i][k] = radius_dry * ClassLayer::layer_radius_fraction_[i][k];

#ifdef AMC_WITH_DEBUG_LAYER
        CBUG << "layer_radius = " << ClassAerosolData::layer_radius_[i] << endl;
#endif
      }
  }


  // Compute the layer concentrations, relevant only for mass concentrations.
  void ClassAerosolData::ComputeConcentrationLayer(const real *concentration_aer_mass,
                                                   vector<vector<real> > &concentration_aer_mass_layer)
  {
    concentration_aer_mass_layer.resize(ClassAerosolData::NgNspecies_);

    for (int i = 0; i < ClassSpecies::Nspecies_; ++i)
      if (ClassLayer::is_species_layered_[i])
        for (int j = 0; j < ClassAerosolData::Ng_; ++j)
          {
            const int k = j * ClassSpecies::Nspecies_ + i;
            concentration_aer_mass_layer[k].resize(ClassLayer::Nlayer_[j]);
            for (int l = 0; l < ClassLayer::Nlayer_[j]; ++l)
              concentration_aer_mass_layer[k][l] = concentration_aer_mass[k] * ClassAerosolData::layer_repartition_[k][l];
          }
  }
#endif


  /*!< Compute the particle composition in each general section.*/
  void ClassAerosolData::ComputeParticleComposition(const int section_min, const int section_max,
                                                    const real *concentration_aer_number,
                                                    const real *concentration_aer_mass,
                                                    vector<vector<real> > &composition)
  {
    composition.resize(ClassAerosolData::Ng_);

    for (int i = section_min; i < section_max; i++)
      {
        real mass_total(real(0));

        int j = i * ClassSpecies::Nspecies_;
        for (int k = 0; k < ClassSpecies::Nspecies_; k++)
          mass_total += concentration_aer_mass[j + k];

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
            {
              composition[i].assign(ClassSpecies::Nspecies_, real(0));
              for (int k = 0; k < ClassSpecies::Nspecies_; k++)
                composition[i][k] = concentration_aer_mass[j + k] / mass_total;
            }
      }
  }


  // Read and write concentrations.
  bool ClassAerosolData::ReadConcentration(const string &path,
                                           real *concentration_aer_number,
                                           real *concentration_aer_mass,
                                           real *concentration_gas,
                                           const int pos)
  {
    check_file(path);

    int record_size = ClassAerosolData::Ng_ + ClassAerosolData::NgNspecies_;
    if (concentration_gas != NULL)
      record_size += ClassSpecies::Ngas_;
    record_size *= sizeof(real);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "record_size = " << record_size << endl;
#endif

    // Open file for reading.
    ifstream fin(path.c_str(), ifstream::binary);

    if (fin.good())
      {
        // File size.
        fin.seekg(0, ios_base::end);
        const int file_size = fin.tellg();
        fin.seekg(0, ios_base::beg);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "file_size = " << file_size << endl;
#endif

        if (file_size % record_size > 0)
          throw AMC::Error("Size of file \"" + path + "\" (" + to_str(file_size) +
                           ") is not a multiple of record size (" + to_str(record_size) +
                           "), probably file and configuration mismatch.");

        // Position in file.
        int file_pos;
        if (pos >= 0)
          file_pos = record_size * pos;          
        else
          file_pos = file_size + record_size * pos;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "file_pos = " << file_pos << endl;
#endif

        if (file_pos >= file_size || file_pos < 0)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Bmagenta() << Warning() << Reset() << "File position (" << pos
                                 << ") exceeds file size (" << file_size << ") or is negative, close and exit." << endl;
#endif
            fin.close();
            return false;
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(2) << Reset() << "Read concentration from file \""
                             << path << "\" at position " << file_pos << ", record = " << pos
                             << " (from the end if < 0)." << endl;
#endif

        fin.seekg(file_pos, ios_base::beg);
        fin.read(reinterpret_cast<char*>(concentration_aer_number), ClassAerosolData::Ng_ * sizeof(real));
        fin.read(reinterpret_cast<char*>(concentration_aer_mass), ClassAerosolData::NgNspecies_ * sizeof(real));
        if (concentration_gas != NULL)
          fin.read(reinterpret_cast<char*>(concentration_gas), ClassSpecies::Ngas_ * sizeof(real));

        fin.close();

        return true;
      }
    else
      throw AMC::Error("Unable to open file \"" + path + "\" for reading.");
  }


  void ClassAerosolData::WriteConcentration(const real *concentration_aer_number,
                                            const real *concentration_aer_mass,
                                            const real *concentration_gas,
                                            const string &path)
  {
    ofstream fout(path.c_str(), ofstream::binary | ios_base::app);

    if (fout.good())
      {
        fout.write(reinterpret_cast<const char*>(concentration_aer_number), ClassAerosolData::Ng_ * sizeof(real));
        fout.write(reinterpret_cast<const char*>(concentration_aer_mass), ClassAerosolData::NgNspecies_ * sizeof(real));
        if (concentration_gas != NULL)
          fout.write(reinterpret_cast<const char*>(concentration_gas), ClassSpecies::Ngas_ * sizeof(real));
        fout.close();
      }
    else
      throw AMC::Error("Unable to open file \"" + path + "\" for writing.");
  }
}

#define AMC_FILE_CLASS_AEROSOL_DATA_CXX
#endif
