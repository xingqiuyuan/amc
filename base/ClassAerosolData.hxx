// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_AEROSOL_DATA_HXX

namespace Driver
{
  class ClassDriverChamprobois;
}


namespace AMC
{
  /*! 
   * \class ClassAerosolData
   * \brief Class containing most aerosol related data.
   */
  class ClassAerosolData :
    protected ClassPhase, protected ClassDiscretizationSize
#ifdef AMC_WITH_TRACE
    , protected ClassTrace
#endif
#ifdef AMC_WITH_LAYER
    , protected ClassLayer
#endif
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of generalized sections : proxy from ClassDiscretization.*/
    static int Ng_;

    /*!< Size of mass concentration vector.*/
    static int NgNspecies_;

    /*!< Size of mass equilibrium concentration vector.*/
    static int NgNspecies_equilibrium_;

    /*!< Size of surface gas concentration vector.*/
    static int NgNgas_;

    /*!< Size of surface tension and Kelvin effect vectors.*/
    static int NgNphase_;

    /*!< General section, index of size section, index of class
      composition in discretization : proxy from ClassDiscretization.*/
    static vector2i general_section_;

    /*!< General section per size section and class composition : proxy from ClassDiscretization.*/
    static vector2i general_section_reverse_;

    /*!< Gas surface concentrations from thermodynamic equilibrium.*/
    static vector1r equilibrium_gas_surface_;

    /*!< Aerosol internal species concentrations from thermodynamic equilibrium.*/
    static vector1r equilibrium_aer_internal_;

    /*!< Aerosol liquid water content.*/
    static vector1r liquid_water_content_;

    /*!< Particle wet density per general section.*/
    static vector1r density_;

    /*!< Particle wet diameter per general section.*/
    static vector1r diameter_;

    /*!< Particle wet surface per general section.*/
    static vector1r surface_;

    /*!< Particle wet mass per general section.*/
    static vector1r mass_;

    /*!< Particle temperature.*/
    static vector1r temperature_;

    /*!< Phase index for each chemical species, negative means solid phase.*/
    static vector1i phase_;

    /*!< Average particule surface tension for each liquid phase.*/
    static vector1r surface_tension_;

    /*!< Adsorption coefficient.*/
    static vector1r adsorption_coefficient_;

#ifdef AMC_WITH_LAYER
    /*!< Size of mass concentration vector.*/
    static int NgNspeciesNlayer_;

    /*!< Layer radius.*/
    static vector2r layer_radius_;

    /*!< Repartition for each species and general section among layers.*/
    static vector2r layer_repartition_;
#endif

    /*!< The Champrobois driver has direct access.*/
    friend class Driver::ClassDriverChamprobois;

    /*!< Diameter parameterizations are my friends.*/
    friend void ClassParameterizationDiameterFixed::ComputeDiameter(const int section_min,
                                                                    const int section_max,
                                                                    const vector1i &parameterization_section,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;

    friend ClassParameterizationDiameterDensityFixed::ClassParameterizationDiameterDensityFixed(Ops::Ops &ops, const string name);

    friend void ClassParameterizationDiameterDensityFixed::ComputeDiameter(const int section_min,
                                                                           const int section_max,
                                                                           const vector1i &parameterization_section,
                                                                           const real *concentration_aer_number,
                                                                           const real *concentration_aer_mass) const;

    friend void ClassParameterizationDiameterDensityMoving::ComputeDiameter(const int section_min,
                                                                            const int section_max,
                                                                            const vector1i &parameterization_section,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

#ifdef AMC_WITH_GERBER
    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const vector1i &parameterization_section,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;

    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_FRACTAL_SOOT
    friend void ClassParameterizationDiameterFractalSoot::ComputeDiameter(const int section_min,
                                                                          const int section_max,
                                                                          const vector1i &parameterization_section,
                                                                          const real *concentration_aer_number,
                                                                          const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_SURFACE_TENSION
    friend void ClassParameterizationSurfaceTensionFixed::ComputeSurfaceTension(const int &section_min,
                                                                                const int &section_max,
                                                                                const vector1i &parameterization_section,
                                                                                const real *concentration_aer_number,
                                                                                const real *concentration_aer_mass) const;

    friend void ClassParameterizationSurfaceTensionAverage::ComputeSurfaceTension(const int &section_min,
                                                                                  const int &section_max,
                                                                                  const vector1i &parameterization_section,
                                                                                  const real *concentration_aer_number,
                                                                                  const real *concentration_aer_mass) const;

#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
    friend void ClassParameterizationSurfaceTensionJacobson::ComputeSurfaceTension(const int &section_min,
                                                                                   const int &section_max,
                                                                                   const vector1i &parameterization_section,
                                                                                   const real *concentration_aer_number,
                                                                                   const real *concentration_aer_mass) const;
#endif
#endif

#ifdef AMC_WITH_ISOROPIA
    friend void ClassModelThermodynamicIsoropia::ComputeLocalEquilibrium(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &section_index,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropia::ComputeGlobalEquilibrium(const int &section_min,
                                                                          const int &section_max,
                                                                          const real *concentration_gas,
                                                                          const real *concentration_aer_mass,
                                                                          vector1r &concentration_aer_total,
                                                                          vector1r &equilibrium_aer_internal,
                                                                          real &liquid_water_content,
                                                                          vector1r &concentration_aer_equilibrium,
                                                                          vector1r &concentration_gas_equilibrium) const;
#endif

#ifdef AMC_WITH_PANKOW
    friend void ClassModelThermodynamicPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &section_index,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicPankow::ComputeGlobalEquilibrium(const int &section_min,
                                                                        const int &section_max,
                                                                        const real *concentration_gas,
                                                                        const real *concentration_aer_mass,
                                                                        vector1r &concentration_aer_total,
                                                                        vector1r &equilibrium_aer_internal,
                                                                        real &liquid_water_content,
                                                                        vector1r &concentration_aer_equilibrium,
                                                                        vector1r &concentration_gas_equilibrium) const;
#endif

#ifdef AMC_WITH_AEC
    friend void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                     const int &section_max,
                                                                     const real *concentration_gas,
                                                                     const real *concentration_aer_mass,
                                                                     vector1r &concentration_aer_total,
                                                                     vector1r &equilibrium_aer_internal,
                                                                     real &liquid_water_content,
                                                                     vector1r &concentration_aer_equilibrium,
                                                                     vector1r &concentration_gas_equilibrium) const;
#endif

#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
    friend void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                            const int &section_max,
                                                                            const vector1i &section_index,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropiaAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                             const int &section_max,
                                                                             const real *concentration_gas,
                                                                             const real *concentration_aer_mass,
                                                                             vector1r &concentration_aer_total,
                                                                             vector1r &equilibrium_aer_internal,
                                                                             real &liquid_water_content,
                                                                             vector1r &concentration_aer_equilibrium,
                                                                             vector1r &concentration_gas_equilibrium) const;
#endif

    /*!< Coagulation parameterization ComputeKernel methods are also my friends.*/
#ifdef AMC_WITH_COAGULATION
    friend void ClassParameterizationCoagulationVoid::ComputeKernel(const vector2i &couple,
                                                                    Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationUnit::ComputeKernel(const vector2i &couple,
                                                                    Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationTable::ComputeKernel(const vector2i &couple,
                                                                    Array<real, 2> &kernel) const;

    friend void ClassParameterizationCoagulationBrownian::ComputeKernel(const vector2i &couple,
                                                                        Array<real, 2> &kernel) const;

#ifdef AMC_WITH_WAALS_VISCOUS
    friend void ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(const vector2i &couple,
                                                                                    Array<real, 2> &kernel) const;
#endif
    friend void ClassParameterizationCoagulationTurbulent::ComputeKernel(const vector2i &couple,
                                                                         Array<real, 2> &kernel) const;

    template<class C>
    friend void ClassParameterizationCoagulationGravitational<C>::ComputeKernel(const vector2i &couple,
                                                                                Array<real, 2> &kernel) const;
#endif

    /*!< Even condensation parameterization ComputeKernel methods are my friends.*/
#ifdef AMC_WITH_CONDENSATION
    friend void ClassParameterizationCondensationVoid::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;
#endif
#endif

#ifdef AMC_WITH_KELVIN_EFFECT
    friend void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                                       const int section_max,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass,
                                                                       vector1r &kelvin_effect) const;
#endif

#ifdef AMC_WITH_ADSORPTION
    friend void ClassModelAdsorptionPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;
#endif

  public:

    /*!< Init.*/
    static void Init();

    /*!< Get methods.*/
    static int GetNg();
    static int GetNgNspecies();
    static int GetNgNgas();
    static int GetNgNphase();
    static void GetGeneralSection(vector<vector<int> > &general_section);
    static void GetGeneralSectionReverse(vector<vector<int> > &general_section_reverse);
#ifndef SWIG
    static vector<vector<int> > GetGeneralSection();
#endif
    static void GetEquilibriumGasSurface(vector<real> &equilibrium_gas_surface);
    static void GetEquilibriumAerosolInternal(vector<real> &equilibrium_aer_internal);
    static void GetLiquidWaterContent(vector<real> &liquid_water_content);
    static void GetDensity(vector<real> &density);
    static void GetDiameter(vector<real> &diameter);
    static void GetMass(vector<real> &mass);
    static void GetPhase(vector1i &phase);
    static void GetTemperature(vector<real> &temperature);
    static void GetSurfaceTension(vector<real> &surface_tension);
    static void GetAdsorptionCoefficient(vector<real> &adsorption_coefficient);
#ifdef AMC_WITH_LAYER
    static void GetLayerRepartition(vector<vector<real> > &layer_repartition);
    static void GetLayerRadius(vector<vector<real> > &layer_radius);
#endif

    /*!< Set methods.*/
    static void SetTemperature(const real &temperature);
#ifdef AMC_WITH_LAYER
    static void SetLayerRepartition(const string &path);
#endif
    static void SetDiameter(const int section_min, const int section_max);

    /*!< Clear method.*/
    static void Clear();

    /*!< Compute aerosol concentration mass total.*/
    static void ComputeConcentrationAerosolMassTotal(const real *concentration_aer_mass,
                                                     vector<real> &concentration_aer_mass_total);


    /*!< Compute mass conservation.*/
    static void ComputeMassConservation(const real *concentration_gas,
                                        const real *concentration_aer_mass,
                                        vector<real> &mass_conservation);

#ifdef AMC_WITH_LAYER
    /*!< Compute the layer radius.*/
    static void ComputeLayerRadius(const int section_min,
                                   const int section_max,
                                   const real *concentration_aer_number,
                                   const real *concentration_aer_mass);

    /*!< Compute the layer concentrations, relevant only for mass concentrations.*/
    static void ComputeConcentrationLayer(const real *concentration_aer_mass,
                                          vector<vector<real> > &concentration_aer_mass_layer);
#endif

    /*!< Compute the particle composition in each general section.*/
    static void ComputeParticleComposition(const int section_min, const int section_max,
                                           const real *concentration_aer_number,
                                           const real *concentration_aer_mass,
                                           vector<vector<real> > &composition);

    /*!< Read and write concentrations.*/
    static bool ReadConcentration(const string &path,
                                  real *concentration_aer_number,
                                  real *concentration_aer_mass,
                                  real *concentration_gas,
                                  const int pos);

    static void WriteConcentration(const real *concentration_aer_number,
                                   const real *concentration_aer_mass,
                                   const real *concentration_gas,
                                   const string &path);
  };
}

#define AMC_FILE_CLASS_AEROSOL_DATA_HXX
#endif
