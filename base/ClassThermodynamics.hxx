// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_THERMODYNAMICS_HXX

namespace AMC
{
  /*! 
   * \class ClassThermodynamics
   */
  class ClassThermodynamics : protected ClassAerosolData
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of thermodynamic models.*/
    static int Nmodel_thermo_;

    /*!< List of thermodynamic models.*/
    static vector<ClassModelThermodynamicBase* > model_thermodynamic_list_;

    /*!< Index of general sections associated to each thermodynamic model.*/
    static vector2i model_thermodynamic_section_;

    /*!< Vector of pointers to thermodynamic models for each section.*/
    static vector<vector<ClassModelThermodynamicBase* > > model_thermodynamic_;

#ifdef AMC_WITH_THREAD_THERMODYNAMIC
    /*!< Thread configuration.*/
    static int Nthread_;
    static vector1i Ng_thread_;
    static vector1i thread_offset_;

    /*!< Partition general sections between threads.*/
    static void thread_partition_(const int section_min, const int section_max);
#endif

#ifdef AMC_WITH_ADSORPTION
    static ClassModelAdsorptionBase* model_adsorption_;
    static vector1i model_adsorption_section_;
#endif

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNmodel();
    static ClassModelThermodynamicBase* GetModelList(const int i);

#ifdef AMC_WITH_ADSORPTION
    static ClassModelAdsorptionBase* GetModelAdsorption();
#endif

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    static void Test(const real *concentration_aer_number,
                     const real *concentration_aer_mass,
                     const int N = 100);
#endif

    /*!< Compute.*/
    static void Compute(const int section_min,
                        const int section_max,
                        const real *concentration_aer_number,
                        const real *concentration_aer_mass);
  };
}

#define AMC_FILE_CLASS_THERMODYNAMICS_HXX
#endif
