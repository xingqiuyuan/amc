// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_LAYER_HXX

namespace AMC
{
  /*! 
   * \class ClassLayer
   */
  class ClassLayer
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of layered species.*/
    static int Nspecies_layer_;

    /*!< Max number of layer across general sections.*/
    static int Nlayer_max_;

    /*!< Is there multiple layers ?.*/
    static bool multiple_layer_;

    /*!< Number of aerosol layers for each general section.*/
    static vector1i Nlayer_;

    /*!< Whether one species is concerned by layers or not.*/
    static vector1i species_index_layer_;

    /*!< Whether one species is concerned by layers or not.*/
    static vector1b is_species_layered_;

    /*!< Radius layer fractions for each general section.*/
    static vector2r layer_radius_fraction_;

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNlayerMax();
    static bool IsMultipleLayer();
    static int GetNspeciesLayer();
    static void GetSpeciesIndexLayer(vector<int> &species_index_layer);
    static void GetNlayer(vector<int> &Nlayer);
    static void IsSpeciesLayered(vector<bool> &is_species_layered);
    static void GetLayerRadiusFraction(vector<vector<real> > &layer_radius_fraction);
  };
}

#define AMC_FILE_CLASS_LAYER_HXX
#endif
