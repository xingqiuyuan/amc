// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_GERBER_HXX

#define AMC_GERBER_PARAMETER_RURAL_0  0.2789
#define AMC_GERBER_PARAMETER_RURAL_1  3.115
#define AMC_GERBER_PARAMETER_RURAL_2  5.415e-11
#define AMC_GERBER_PARAMETER_RURAL_3 -1.399

#define AMC_GERBER_PARAMETER_URBAN_0  0.3926
#define AMC_GERBER_PARAMETER_URBAN_1  3.101
#define AMC_GERBER_PARAMETER_URBAN_2  4.19e-11
#define AMC_GERBER_PARAMETER_URBAN_3 -1.404

#define AMC_GERBER_PARAMETER_SEA_SALT_0  0.7674
#define AMC_GERBER_PARAMETER_SEA_SALT_1  3.079
#define AMC_GERBER_PARAMETER_SEA_SALT_2  2.572e-11
#define AMC_GERBER_PARAMETER_SEA_SALT_3 -1.424

#define AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_0  0.4809
#define AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_1  3.082
#define AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_2  3.110e-11
#define AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_3 -1.428

#define AMC_GERBER_NUMBER_PARAMETER 4

#define AMC_GERBER_CONVERT_RADIUS_CM_TO_DIAMETER_UM 2.e4
#define AMC_GERBER_CONVERT_DIAMETER_UM_TO_RADIUS_CM 5.e-5

#define AMC_GERBER_CONVERT_DENSITY_FROM_UM3_TO_CM3  1.e12
#define AMC_GERBER_CONVERT_DIAMETER_FROM_CM_TO_UM   1.e4
#define AMC_GERBER_CONVERT_CM3_TO_UM3 1.e12

#define AMC_GERBER_PI4_DIV_3      4.1887902047863905
#define AMC_GERBER_INV_PI4_DIV_3  0.238732414637843

#define AMC_GERBER_ACCURACY_DEFAULT 1.e6
#define AMC_GERBER_PGTOL_DEFAULT 1.e-5
#define AMC_GERBER_NITER_MAX_DEFAULT 100

namespace AMC
{
  class ClassParameterizationDiameterGerber : public ClassParameterizationDiameterDensityFixed
  {
  private:

    /*!< Number of general sections.*/
    int Ng_;

    /*!< Number of parameters.*/
    int Nparameter_;

    /*!< parameter index for each section.*/
    vector1i parameterization_index_;

    /*!< Gerber parameters.*/
    vector2r parameter_;

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterGerber(Ops::Ops &ops, const int Ng);

    /*!< Destructor.*/
    ~ClassParameterizationDiameterGerber();

    /*!< Get methods.*/
    int GetNparameter() const;
    void GetParameter(vector<vector<real> > &parameter) const;
    void GetParameterizationSection(vector<int> &parameterization_section) const;
    void GetParameterizationIndex(vector<int> &parameterization_index) const;

    /*!< Clone.*/
    ClassParameterizationDiameterGerber* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationDiameterBase *param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    void ComputeDiameter(const int section_min,
                         const int section_max,
                         const vector1i &parameterization_section,
                         const real *concentration_aer_number,
                         const real *concentration_aer_mass) const;

    void ComputeDiameter(const int section_min,
                         const int section_max,
                         const real *concentration_aer_number,
                         const real *concentration_aer_mass) const;
#endif

    /*!< Compute diameter.*/
    real ComputeDiameter(const real &diameter,
                         const real &temperature,
                         const real &relative_humidity,
                         const int parameterization_index = 0) const;

    /*!< Compute liquid water content.*/
    static real ComputeLiquidWaterContentValue(const vector<real> &parameter,
                                               const real &diameter,
                                               const real &temperature,
                                               const real &relative_humidity);

    /*!< Compute diameter gradient.*/
    static void ComputeLiquidWaterContentGradient(const vector<real> &parameter,
                                                  const real &diameter,
                                                  const real &temperature,
                                                  const real &relative_humidity,
                                                  vector<real> &gradient);

    /*!< Compute parameters from initial data.*/
    static void ComputeParameter(const vector<vector<real> > &data,
                                 vector<real> &parameter,
                                 const int Niter_max = AMC_GERBER_NITER_MAX_DEFAULT,
                                 const double accuracy = AMC_GERBER_ACCURACY_DEFAULT,
                                 const double pgtol = AMC_GERBER_PGTOL_DEFAULT);
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_GERBER_HXX
#endif
