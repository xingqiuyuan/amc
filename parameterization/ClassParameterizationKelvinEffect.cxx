// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_KELVIN_EFFECT_CXX

#include "ClassParameterizationKelvinEffect.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationKelvinEffect::ClassParameterizationKelvinEffect(Ops::Ops &ops)
    : exponential_cutoff_(AMC_KELVIN_EFFECT_EXPONENTIONAL_CUTOFF)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug() << Reset() << "Instantiate Kelvin Effect." << endl;
#endif

    // Proxy from ClassSpecies to speed up computation.
    molar_mass_ = ClassSpecies::molar_mass_;

    exponential_cutoff_ = ops.Get<real>("exponential_cutoff", "", exponential_cutoff_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "exponential_cutoff = " << exponential_cutoff_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationKelvinEffect::~ClassParameterizationKelvinEffect()
  {
    return;
  }


  // Compute Kelvin effect and correct equilibrium gas surface.
  void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                              const int section_max,
                                                              const real *concentration_aer_number,
                                                              const real *concentration_aer_mass,
                                                              vector1r &kelvin_effect) const
  {
    // Loop on sections, if not too few particles.
    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        {
          // Start index in concentration mass.
          const int j = i * ClassSpecies::Nspecies_;

          // Start index in kelvin effect and tension surface.
          const int k = i * ClassPhase::Nphase_;

          // Gas concentration index.
          const int l = i * ClassSpecies::Ngas_;

          // Average phase molar mass.
          vector1r mass_total(ClassPhase::Nphase_, real(0)),
            mol_total(ClassPhase::Nphase_, real(0));

          // If particle is solid, inorganic species are solid salt
          // for which Kelvin effect has (as far as I know) little meaning,
          // salt are like polygons, i.e. flat surfaces ...
          for (int m = 0; m < ClassSpecies::Nspecies_; m++)
            {
              const int n = j + m;
              const int phase_index = ClassAerosolData::phase_[n];

              if (phase_index >= 0)
                {
                  real mass_species = concentration_aer_mass[n];
#ifdef AMC_WITH_LAYER
                  mass_species *= ClassAerosolData::layer_repartition_[n].back();
#endif
                  mass_total[phase_index] += mass_species;
                  mol_total[phase_index] += mass_species / ClassParameterizationKelvinEffect::molar_mass_[m];
                }
            }

            // Add liquid water content to the aqueous phase, if accounted.
            if (ClassPhase::aqueous_phase_index_ >= 0)
              {
                mass_total[ClassPhase::aqueous_phase_index_] += ClassAerosolData::liquid_water_content_[i];
                mol_total[ClassPhase::aqueous_phase_index_] += ClassAerosolData::liquid_water_content_[i]
                  / AMC_WATER_MOLAR_MASS;
              }

            // Kelvin exponential prefactor : to speed up computation.
            // = N.m-1 * µg.mol-1 / (J.K-1.mol-1 * K * µg.µm-3 * µm)
            // = (N * µg * mol * K * µm3) / (m * mol * J * K * µg * µm)
            // = (N * µm2) / (m * J) = 1.e-12 * (N * m2) / (m * J) = 1.e-12 (N * m) / J = 1.e-12
            real exponential_prefactor = real(4e-12)
              / (AMC_IDEAL_GAS_CONSTANT * ClassMeteorologicalData::temperature_
                 * ClassAerosolData::density_[i] * ClassAerosolData::diameter_[i]);

            // Beware of empty phases.
            for (int m = 0; m < ClassPhase::Nphase_; m++)
              if (mass_total[m] > real(0))
                {
                  // Average phase molar mass in µg.mol^{-1}.
                  real molar_mass_average = mass_total[m] / mol_total[m];

#ifdef AMC_WITH_DEBUG_KELVIN_EFFECT
                  cout << "molar_mass_average = " << molar_mass_average << endl;
#endif

                  // In the end, kelvin effect. Adimensioned.
                  const int n = k + m;

                  kelvin_effect[n] = exponential_prefactor * ClassAerosolData::surface_tension_[n] * molar_mass_average;
                  if (kelvin_effect[n] < exponential_cutoff_)
                    kelvin_effect[n] = real(1) + kelvin_effect[n] * (real(1) + real(0.5) * kelvin_effect[n]);
                  else
                    kelvin_effect[n] = exp(kelvin_effect[n]);
                }

            // Correct the equilibrium gas surface concentration, but only if it does not come from aDsorption.
            for (int m = 0; m < ClassSpecies::Ngas_; m++)
              {
                // Position in gas concentration vector.
                int n = l + m;

                // Correct only if it does not come from aDsorption.
                if (ClassAerosolData::equilibrium_gas_surface_[n] >= real(0))
                  if (ClassAerosolData::adsorption_coefficient_[n] < real(0))
                    {
                      // The phase index where lies the semivolatile species.
                      const int &phase_index = ClassAerosolData::phase_[j + ClassSpecies::semivolatile_[m]];

                      // Account for Kelvin effect, eventually.
                      ClassAerosolData::equilibrium_gas_surface_[n] *= kelvin_effect[k + phase_index];
                    }
              }
        }
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassParameterizationKelvinEffect::Test(const real &temperature,
                                               const real &diameter,
                                               const real &density,
                                               const real &liquid_water_content,
                                               const vector<real> &surface_tension,
                                               const vector<real> &concentration_aer_mass,
                                               vector<real> &kelvin_effect) const
  {
    // Phase for each species, only relevant for species which may absorb in several phases.
    vector1i phase(ClassSpecies::Nspecies_);

    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (int(ClassSpecies::phase_[i].size()) == 1)
        phase[i] = ClassSpecies::phase_[i][0];
      else
        {
          if (ClassSpecies::phase_[i][0] == ClassPhase::aqueous_phase_index_
              && liquid_water_content > real(0))
            phase[i] = ClassPhase::aqueous_phase_index_;
          else
            phase[i] = ClassSpecies::phase_[i][1];
        }

    // Average phase molar mass.
    vector1r mass_total(ClassPhase::Nphase_, real(0)),
      mol_total(ClassPhase::Nphase_, real(0));

    // If particle is solid, inorganic species may be solid salt
    // for which Kelvin effect has (as far as I know) little meaning,
    // salt are like polygons, i.e. flat surfaces ...
    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      {
        const int &phase_index = phase[i];

        if (phase_index >= 0)
          {
            mass_total[phase_index] += concentration_aer_mass[i];
            mol_total[phase_index] += concentration_aer_mass[i] / molar_mass_[i];
          }
      }

    // Add liquid water content to the aqueous phase, if accounted and if any.
    if (ClassPhase::aqueous_phase_index_ >= 0)
      if (liquid_water_content > real(0))
        {
          mass_total[ClassPhase::aqueous_phase_index_] += liquid_water_content;
          mol_total[ClassPhase::aqueous_phase_index_] += liquid_water_content / AMC_WATER_MOLAR_MASS;
        }

    // Turn density to right units.
    real density2 = density * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

    // Kelvin exponential prefactor : to speed up computation.
    real exponential_prefactor = real(4e-6)
      / (AMC_IDEAL_GAS_CONSTANT * temperature * density2 * diameter);

    // Beware of empty phases.
    kelvin_effect.assign(ClassPhase::Nphase_, real(1));

    for (int i = 0; i < ClassPhase::Nphase_; i++)
      if (mass_total[i] > real(0))
        {
          // Average phase molar mass in µg.mol^{-1}.
          real molar_mass_average = mass_total[i] / mol_total[i];

#ifdef AMC_WITH_DEBUG_KELVIN_EFFECT
          cout << "molar_mass_average = " << molar_mass_average << endl;
#endif

          kelvin_effect[i] = exponential_prefactor * surface_tension[i] * molar_mass_average;
          if (kelvin_effect[i] < exponential_cutoff_)
            kelvin_effect[i] = real(1) + kelvin_effect[i] * (real(1) + real(0.5) * kelvin_effect[i]);
          else
            kelvin_effect[i] = exp(kelvin_effect[i]);
        }
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_KELVIN_EFFECT_CXX
#endif
