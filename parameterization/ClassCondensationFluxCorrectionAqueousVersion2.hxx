// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION2_HXX

namespace AMC
{
  class ClassCondensationFluxCorrectionAqueousVersion2 : public ClassCondensationFluxCorrectionAqueousBase
  {

  public:

    /*!< Constructors.*/
    ClassCondensationFluxCorrectionAqueousVersion2(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassCondensationFluxCorrectionAqueousVersion2();

    /*!< Compute equilibrium gas correction thanks to ion hydronium flux limitation.*/
    void ComputeFluxCorrection(const real *equilibrium_aer_internal,
                               const real *concentration_gas,
                               const vector1r &coefficient,
                               real *equilibrium_gas_surface,
                               vector1r &rate_aer_mass) const;
  };
}

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION2_HXX
#endif
