// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_GERBER_CXX

#include "ClassParameterizationDiameterGerber.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationDiameterGerber::ClassParameterizationDiameterGerber(Ops::Ops &ops, const int Ng)
    : ClassParameterizationDiameterDensityFixed(ops, "Gerber"), Nparameter_(0), Ng_(Ng)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Gerber diameter parameterization." << endl; 
    *AMCLogger::GetLog() << Bred() << Warning() << Reset() << "Gerber parameterization is a formula to compute"
                         << " the wet particle diameter, hence it overwrites the liquid water content." << endl;
#endif

    // Convert fixed density from µg/µm3 to µg/cm3.
    this->density_fixed_ *= AMC_GERBER_CONVERT_DENSITY_FROM_UM3_TO_CM3;

    // List of parameterizations.
    vector1s param_list = ops.GetEntryList("list");

    Nparameter_ = int(param_list.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "Nparameter = " << Nparameter_ << endl; 
#endif

    const string prefix_orig = ops.GetPrefix();

    parameterization_index_.assign(Ng_, -1);

    for (int i = 0; i < Nparameter_; ++i)
      {
        ops.SetPrefix(prefix_orig + "list." + param_list[i] + ".");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "Init Gerber parameterization \""
                             << param_list[i] << "\"." << endl;
#endif

        // Sections associated with this parameterization.
        if (ops.Exists("section"))
          {
            if (ops.Is<int>("section"))
              parameterization_index_[ops.Get<int>("section")] = i;
            else
              {
                vector1i section_index;
                ops.Set("section", "", section_index);
                for (int j = 0; j < int(section_index.size()); ++j)
                  parameterization_index_[section_index[j]] = i;
              }
          }
        else
          {
            if (i == 0)
              for (int j = 0; j < Ng_; ++j)
                parameterization_index_[j] = 0;
            else
              throw AMC::Error("Only the first parameterization may be used as the default one.");
          }

        // Choose predefined parameters or give them directly.
        if (ops.Exists("parameter"))
          {
            if (ops.Is<string>("parameter"))
              {
                const string parameter_name = ops.Get<string>("parameter");

                if (parameter_name == "rural")
                  parameter_.push_back({AMC_GERBER_PARAMETER_RURAL_0, AMC_GERBER_PARAMETER_RURAL_1,
                        AMC_GERBER_PARAMETER_RURAL_2, AMC_GERBER_PARAMETER_RURAL_3});
                else if (parameter_name == "urban")
                  parameter_.push_back({AMC_GERBER_PARAMETER_URBAN_0, AMC_GERBER_PARAMETER_URBAN_1,
                        AMC_GERBER_PARAMETER_URBAN_2, AMC_GERBER_PARAMETER_URBAN_3});
                else if (parameter_name == "sea_salt")
                  parameter_.push_back({AMC_GERBER_PARAMETER_SEA_SALT_0, AMC_GERBER_PARAMETER_SEA_SALT_1,
                        AMC_GERBER_PARAMETER_SEA_SALT_2, AMC_GERBER_PARAMETER_SEA_SALT_3});
                else if (parameter_name == "ammonium_sulfate")
                  parameter_.push_back({AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_0, AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_1,
                        AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_2, AMC_GERBER_PARAMETER_AMMONIUM_SULFATE_3});
                else
                  throw AMC::Error("Gerber's parameter named \"" + parameter_name + "\" not implemented.");
              }
            else
              {
                vector1r parameter;
                ops.Set("parameter", "", parameter);

                if (int(parameter.size()) != AMC_GERBER_NUMBER_PARAMETER)
                  throw AMC::Error("Wrong number of Gerber's parameters, expected " +
                                   to_str(AMC_GERBER_NUMBER_PARAMETER) + " of them.");

                parameter_.push_back(parameter);
              }
          }
        else
          throw AMC::Error("Must give Gerber parameters : either a string \"rural\", \"urban\", \"sea_salt\", \"ammonium_sulfate\" or a sequence of 4 numbers {C1, C2, C3, C4}");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(2) << Reset() << "\tC1, C2, C3, C4 = " << parameter_.back() << endl;
#endif
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Gerber parameterization index for each general section = "
                         << parameterization_index_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationDiameterGerber::~ClassParameterizationDiameterGerber()
  {
    return;
  }


  // Get methods.
  int ClassParameterizationDiameterGerber::GetNparameter() const
  {
    return Nparameter_;
  }


  void ClassParameterizationDiameterGerber::GetParameter(vector<vector<real> > &parameter) const
  {
    parameter = parameter_;
  }


  void ClassParameterizationDiameterGerber::GetParameterizationSection(vector<int> &parameterization_section) const
  {
    for (int i = 0; i < int(parameterization_index_.size()); ++i)
      if (parameterization_index_[i] >= 0)
        parameterization_section.push_back(parameterization_index_[i]);
  }


  void ClassParameterizationDiameterGerber::GetParameterizationIndex(vector<int> &parameterization_index) const
  {
    parameterization_index = parameterization_index_;
  }


  // Clone.
  ClassParameterizationDiameterGerber* ClassParameterizationDiameterGerber::clone() const
  {
    return new ClassParameterizationDiameterGerber(*this);
  }


  // Copy.
  void ClassParameterizationDiameterGerber::Copy(const ClassParameterizationDiameterBase *param)
  {
    parameter_ = reinterpret_cast<const ClassParameterizationDiameterGerber*>(param)->parameter_;
    ClassParameterizationDiameterDensityFixed::Copy(param);
  }


  // Compute diameter for AMC.
  void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                            const int section_max,
                                                            const vector1i &parameterization_section,
                                                            const real *concentration_aer_number,
                                                            const real *concentration_aer_mass) const
  {
    const int Nsection = int(parameterization_section.size());

    const real C3_correction = real(1) + real(4.e-3) * (real(298) - ClassMeteorologicalData::temperature_);
    const real relative_humidity_log = log(ClassMeteorologicalData::relative_humidity_ < real(1)
                                           ? ClassMeteorologicalData::relative_humidity_ : real(1));

    for (int i = 0; i < Nsection; ++i)
      {
        const int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            const int k = parameterization_index_[j];

            const real &C1 = parameter_[k][0];
            const real &C2 = parameter_[k][1];
            const real C3 = parameter_[k][2] * C3_correction;
            const real &C4 = parameter_[k][3];

            // Dry single particle mass in µg.
            real dry_mass = real(0);

            int l = j * ClassSpecies::Nspecies_ - 1;
            for (int m = 0; m < ClassSpecies::Nspecies_; ++m)
              dry_mass += concentration_aer_mass[++l];

            if (dry_mass > AMC_MASS_CONCENTRATION_MINIMUM)
              {
                dry_mass /= concentration_aer_number[j];

                // Dry volume in cm3.
                real dry_volume_cm3 = dry_mass / this->density_fixed_;

                // Dry radius in cm.
                real dry_radius_cm = pow(AMC_GERBER_INV_PI4_DIV_3 * dry_volume_cm3, AMC_FRAC3);

                // Gerber formula : wet volume in cm3.
                real wet_volume_cm3 = dry_volume_cm3
                  + (C1 * pow(dry_radius_cm, C2) * AMC_GERBER_PI4_DIV_3
                     / (C3 * pow(dry_radius_cm, C4) - relative_humidity_log));

                // Wet diameter in µm.
                ClassAerosolData::diameter_[j] = pow(AMC_INV_PI6 * wet_volume_cm3, AMC_FRAC3)
                  * AMC_GERBER_CONVERT_DIAMETER_FROM_CM_TO_UM;

                // Wet particle mass : cm3 x µg.cm^{-3} = µg.
                ClassAerosolData::mass_[j] = wet_volume_cm3 * this->density_fixed_;

                // Hence deduce the liquid water content (µg.m^{-3}).
                ClassAerosolData::liquid_water_content_[j] = (ClassAerosolData::mass_[j] - dry_mass)
                  * concentration_aer_number[j];

                getchar();
              }
          }
      }
  }


  void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                            const int section_max,
                                                            const real *concentration_aer_number,
                                                            const real *concentration_aer_mass) const
  {
    const real C3_correction = real(1) + real(4.e-3) * (real(298) - ClassMeteorologicalData::temperature_);
    const real relative_humidity_log = log(ClassMeteorologicalData::relative_humidity_ < real(1) ?
                                           ClassMeteorologicalData::relative_humidity_ : real(1));

    for (int j = 0; j < ClassAerosolData::Ng_; ++j)
      {
        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            const int k = parameterization_index_[j];

            const real &C1 = parameter_[k][0];
            const real &C2 = parameter_[k][1];
            const real C3 = parameter_[k][2] * C3_correction;
            const real &C4 = parameter_[k][3];

            // Dry single particle mass in µg.
            real dry_mass = real(0);

            int l = j * ClassSpecies::Nspecies_ - 1;
            for (int m = 0; m < ClassSpecies::Nspecies_; ++m)
              dry_mass += concentration_aer_mass[++l];

            if (dry_mass > AMC_MASS_CONCENTRATION_MINIMUM)
              {
                dry_mass /= concentration_aer_number[j];

                // Dry volume in cm3.
                real dry_volume_cm3 = dry_mass / this->density_fixed_;

                // Dry radius in cm.
                real dry_radius_cm = pow(AMC_GERBER_INV_PI4_DIV_3 * dry_volume_cm3, AMC_FRAC3);

                // Gerber formula : wet volume in cm3.
                real wet_volume_cm3 = dry_volume_cm3
                  + (C1 * pow(dry_radius_cm, C2) * AMC_GERBER_PI4_DIV_3
                     / (C3 * pow(dry_radius_cm, C4) - relative_humidity_log));

                // Wet diameter in µm.
                ClassAerosolData::diameter_[j] = pow(AMC_INV_PI6 * wet_volume_cm3, AMC_FRAC3)
                  * AMC_GERBER_CONVERT_DIAMETER_FROM_CM_TO_UM;

                // Wet particle mass : cm3 x µg.cm^{-3} = µg.
                ClassAerosolData::mass_[j] = wet_volume_cm3 * this->density_fixed_;

                // Hence deduce the liquid water content (µg.m^{-3}).
                ClassAerosolData::liquid_water_content_[j] = (ClassAerosolData::mass_[j] - dry_mass)
                  * concentration_aer_number[j];
              }
          }
      }
  }


  // Compute diameter.
  real ClassParameterizationDiameterGerber::ComputeDiameter(const real &diameter,
                                                            const real &temperature,
                                                            const real &relative_humidity,
                                                            const int parameterization_index) const
  {
    // Gerber's formula needs the radius in centimeter.
    real radius_dry_cm = diameter * real(AMC_GERBER_CONVERT_DIAMETER_UM_TO_RADIUS_CM);

    const real &C1 = parameter_[parameterization_index][0];
    const real &C2 = parameter_[parameterization_index][1];
    const real &C3 = parameter_[parameterization_index][2];
    const real &C4 = parameter_[parameterization_index][3];

    // Wet diameter power 3 in micrometers. 1.25e17 converts from centimer radius to micrometer diameter.
    return AMC_GERBER_CONVERT_RADIUS_CM_TO_DIAMETER_UM
      * pow(radius_dry_cm * radius_dry_cm * radius_dry_cm
            + C1 * pow(radius_dry_cm, C2)
            / (C3 * (real(1) + real(4.e-3) * (real(298) - temperature))
               * pow(radius_dry_cm, C4) - log(relative_humidity < real(1) ? relative_humidity : real(1))), AMC_FRAC3);
  }


  // Compute liquid water content.
  real ClassParameterizationDiameterGerber::ComputeLiquidWaterContentValue(const vector<real> &parameter,
                                                                           const real &diameter,
                                                                           const real &temperature,
                                                                           const real &relative_humidity)
  {
    // Gerber's formula needs the radius in centimeter.
    real radius_dry_cm = diameter * real(AMC_GERBER_CONVERT_DIAMETER_UM_TO_RADIUS_CM);

    const real &C1 = parameter[0];
    const real &C2 = parameter[1];
    const real &C3 = parameter[2];
    const real &C4 = parameter[3];

    // Liquid water content volume in µm3.
    return AMC_GERBER_PI4_DIV_3 * AMC_GERBER_CONVERT_CM3_TO_UM3
      * (C1 * pow(radius_dry_cm, C2)
         / (C3 * (real(1) + real(4.e-3) * (real(298) - temperature))
            * pow(radius_dry_cm, C4) - log(relative_humidity < real(1) ? relative_humidity : real(1))));
  }


  // Compute diameter gradient.
  void ClassParameterizationDiameterGerber::ComputeLiquidWaterContentGradient(const vector<real> &parameter,
                                                                              const real &diameter,
                                                                              const real &temperature,
                                                                              const real &relative_humidity,
                                                                              vector<real> &gradient)
  {
    // Gerber's formula needs the radius in centimeter.
    real radius_dry_cm = diameter * real(AMC_GERBER_CONVERT_DIAMETER_UM_TO_RADIUS_CM);

    const real &C1 = parameter[0];
    const real &C2 = parameter[1];
    const real &C3 = parameter[2];
    const real &C4 = parameter[3];

    real coef_c3 = pow(radius_dry_cm, C4);
    real coef_temperature = real(1) + real(4.e-3) * (real(298) - temperature);
    real num = C1 * pow(radius_dry_cm, C2);
    real denum1 = C3 * coef_temperature * coef_c3;
    real denum = denum1 - log(relative_humidity < real(1) ? relative_humidity : real(1));
    real f = num / denum;
    real f2 = f * f;

    real log_radius = log(radius_dry_cm);

    gradient[0] = f / C1;
    gradient[1] = log_radius * f;
    gradient[2] = - f2 * coef_temperature * coef_c3 / num;
    gradient[3] = - f2 * denum1 / num;

    for (int i = 0; i < AMC_GERBER_NUMBER_PARAMETER; i++)
      gradient[i] *= (AMC_GERBER_PI4_DIV_3 * AMC_GERBER_CONVERT_CM3_TO_UM3);
  }


  // Compute parameters from initial data.
  void ClassParameterizationDiameterGerber::ComputeParameter(const vector<vector<real> > &data,
                                                             vector<real> &parameter,
                                                             const int Niter_max,
                                                             const double accuracy,
                                                             const double pgtol)
  {
#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    int time_index = AMCTimer<CPU>::Add("compute_gerber_parameter");
    AMCTimer<CPU>::Start();
#endif

    // Number of values with which constrain.
    const int Ndata = int(data.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Ndata = " << Ndata << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Initial Gerber parameter = " << parameter << endl;
#endif

    // Setulb initialization.
    int Nmetric = 5;
    int Nparam = AMC_GERBER_NUMBER_PARAMETER;
    vector<double> lbound(Nparam, double(0));
    vector<double> ubound(Nparam, double(0));
    vector<int> bound_type(Nparam, 0);
    string task(' ', 61);
    int iprint = -1;
    int fint = 1;
    int work_arr_length = (2 * Nmetric + 4) * Nparam
      + (11 * Nmetric + 8) * Nmetric;
    vector<double> work_arr(work_arr_length);
    vector<int> work_iarr(3 * Nparam);
    vector<char> work_carr(60);
    vector<int> work_larr(4), work_iarr0(44);
    vector<double> work_arr0(29);

    task.substr(0, 5) = "START";

    int Niter = 0;
    while (task.substr(0, 4) != "STOP" && Niter < Niter_max)
      {
        // Compute function and its gradient.
        double function(double(0));
        vector<double> gradient(AMC_GERBER_NUMBER_PARAMETER, double(0));
        vector1r gradient_tmp(AMC_GERBER_NUMBER_PARAMETER);

        for (int i = 0; i < Ndata; i++)
          {
            const vector<real> &data_tmp = data[i];

            double tmp = - data_tmp[3]
              + ClassParameterizationDiameterGerber::
              ComputeLiquidWaterContentValue(parameter,
                                             data_tmp[0],
                                             data_tmp[1],
                                             data_tmp[2]);
            function += tmp * tmp;

            ClassParameterizationDiameterGerber::
              ComputeLiquidWaterContentGradient(parameter,
                                                data_tmp[0],
                                                data_tmp[1],
                                                data_tmp[2],
                                                gradient_tmp);

            for (int j = 0; j < AMC_GERBER_NUMBER_PARAMETER; j++)
              gradient[j] += double(tmp * gradient_tmp[j]);
          }

        function *= double(0.5);

        setulb_(&Nparam, &Nmetric, parameter.data(), lbound.data(),
                ubound.data(), bound_type.data(), &function,
                gradient.data(), const_cast<double*>(&accuracy), const_cast<double*>(&pgtol),
                work_arr.data(), work_iarr.data(), (char*)task.c_str(), &iprint,
                work_carr.data(), work_larr.data(), work_iarr0.data(),
                work_arr0.data(), &fint, &fint);
        
        if (task.substr(0, 5) == "NEW_X")
          setulb_(&Nparam, &Nmetric, parameter.data(), lbound.data(),
                  ubound.data(), bound_type.data(), &function,
                  gradient.data(), const_cast<double*>(&accuracy), const_cast<double*>(&pgtol),
                  work_arr.data(), work_iarr.data(), (char*)task.c_str(), &iprint,
                  work_carr.data(), work_larr.data(), work_iarr0.data(),
                  work_arr0.data(), &fint, &fint);
        Niter++;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << Reset() << "Computed Gerber parameters = " << parameter << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Niter = " << Niter << endl;
#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Computed Gerber parameters in "
                         << AMCTimer<CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_GERBER_CXX
#endif
