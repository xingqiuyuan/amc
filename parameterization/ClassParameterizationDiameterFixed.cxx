// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FIXED_CXX

#include "ClassParameterizationDiameterFixed.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationDiameterFixed::ClassParameterizationDiameterFixed(Ops::Ops &ops)
    : ClassParameterizationDiameterBase("Fixed")
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Fixed diameter parameterization." << endl; 
#endif

    // Read section index.
    ops.Set("section", "", section_index_);

    // Read fixed diameters.
    ops.Set("diameter", "", diameter_);

    if (diameter_.size() != section_index_.size())
      throw AMC::Error("Not equal numbers of diameters and corresponding sections.");

    // Single volume in µm3.
    volume_.resize(int(diameter_.size()));
    for (int i = 0; i < int(diameter_.size()); i++)
      volume_[i] = AMC_PI6 * diameter_[i] * diameter_[i] * diameter_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "For section index = "
                         << section_index_ << ", use diameter = " << diameter_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationDiameterFixed::~ClassParameterizationDiameterFixed()
  {
    return;
  }


  // Get methods.
  void ClassParameterizationDiameterFixed::GetSectionIndexList(vector<int> &section_index) const
  {
    section_index = section_index_;
  }


  void ClassParameterizationDiameterFixed::GetDiameterList(vector<real> &diameter) const
  {
    diameter = diameter_;
  }


  // Clone.
  ClassParameterizationDiameterFixed* ClassParameterizationDiameterFixed::clone() const
  {
    return new ClassParameterizationDiameterFixed(*this);
  }


  // Copy.
  void ClassParameterizationDiameterFixed::Copy(const ClassParameterizationDiameterBase *param)
  {
    section_index_ = reinterpret_cast<const ClassParameterizationDiameterFixed*>(param)->section_index_;
    diameter_ = reinterpret_cast<const ClassParameterizationDiameterFixed*>(param)->diameter_;
    ClassParameterizationDiameterBase::Copy(*param);
  }


  // Compute diameter for AMC.
  void ClassParameterizationDiameterFixed::ComputeDiameter(const int section_min,
                                                           const int section_max,
                                                           const vector1i &parameterization_section,
                                                           const real *concentration_aer_number,
                                                           const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            real mass_total(real(0));

            int k = j * ClassSpecies::Nspecies_;
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              mass_total += concentration_aer_mass[l++];

            // Add the liquid water content, if any.
            mass_total += ClassAerosolData::liquid_water_content_[j];

            if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
              {
                ClassAerosolData::mass_[j] = mass_total / concentration_aer_number[j];
                ClassAerosolData::diameter_[j] = ClassParameterizationDiameterFixed::diameter_[i];
                ClassAerosolData::density_[j] = ClassAerosolData::mass_[j] / ClassParameterizationDiameterFixed::volume_[i];
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FIXED_CXX
#endif
