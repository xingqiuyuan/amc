// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_BASE_HXX

#define AMC_FLUX_CORRECTION_AQUEOUS_LIMITATION_HYDRONIUM_DEFAULT 0.1
#define AMC_FLUX_CORRECTION_AQUEOUS_NGAS  4
#define AMC_FLUX_CORRECTION_AQUEOUS_HYDRONIUM_MOLAR_MASS 1.e6

namespace AMC
{
  class ClassCondensationFluxCorrectionAqueousBase
  {
  public:
    typedef AMC_PARAMETERIZATION_REAL real;

  protected:

    /*!< Correction name.*/
    string name_;

    /*!< Index hydronium ion in equilibrium aerosol internal concentration vector.*/
    int index_hydronium_;

    /*!< Index of sulfate, nitrate, chloride and ammonia species in gas concentration vector.*/
    vector1i index_;

    /*!< Hydronium limitation.*/
    real limitation_hydronium_;

    /*!< The molar weight of corrected gas species.*/
    vector1r molar_mass_;

  public:

    /*!< Constructors.*/
    ClassCondensationFluxCorrectionAqueousBase(Ops::Ops &ops, const string name = "");

    /*!< Destructor.*/
    virtual ~ClassCondensationFluxCorrectionAqueousBase();

    /*!< Get methods.*/
    string GetName() const;

    /*!< Compute equilibrium gas correction thanks to ion hydronium flux limitation.*/
    virtual void ComputeFluxCorrection(const real *equilibrium_aer_internal,
                                       const real *concentration_gas,
                                       const vector1r &coefficient,
                                       real *equilibrium_gas_surface,
                                       vector1r &rate_aer_mass) const = 0;
  };
}

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_BASE_HXX
#endif
