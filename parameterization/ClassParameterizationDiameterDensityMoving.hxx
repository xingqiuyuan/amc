// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_MOVING_HXX


namespace AMC
{
  class ClassParameterizationDiameterDensityMoving : public  ClassParameterizationDiameterBase
  {
  private:

    /*!< Number of group.*/
    int Ngroup_;

    /*!< Number of species per group.*/
    vector1i Nspecies_;

    /*!< Species index for each group.*/
    vector2i species_index_;

    /*!< Group species density.*/
    vector1r density_;

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterDensityMoving(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationDiameterDensityMoving();

    /*!< Get methods.*/
    void GetSpeciesIndex(const int &i, vector<int> &species_index) const;
    real GetDensity(const int &i) const;
    int GetNspecies(const int &i) const;
    int GetNgroup() const;

    /*!< Clone.*/
    ClassParameterizationDiameterDensityMoving* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationDiameterBase *param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    void ComputeDiameter(const int section_min,
                         const int section_max,
                         const vector1i &parameterization_section,
                         const real *concentration_aer_number,
                         const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_MOVING_HXX
#endif
