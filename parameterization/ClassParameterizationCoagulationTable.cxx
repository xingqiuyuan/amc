// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TABLE_CXX

#include "ClassParameterizationCoagulationTable.hxx"

namespace AMC
{
  // Construct method.
  void ClassParameterizationCoagulationTable::construct(Ops::Ops &ops)
  {
    // Keep orginal prefix.
    string prefix_orig = ops.GetPrefix();

    // Parameterizations to tabulate.
    vector<string> parameterization_name = ops.GetEntryList("parameterization");
    Nparam_ = int(parameterization_name.size());
    parameterization_.resize(Nparam_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "Number of parameterizations = " << Nparam_ << endl;
    if (Nparam_ > 1)
      *AMCLogger::GetLog() << Fcyan() << Warning() << Reset() << "Kernel parameterizations may not be additive." << endl;      
#endif

    // Parameterization name.
    for (int i = 0; i < Nparam_; i++)
      {
        ops.SetPrefix(prefix_orig + parameterization_name[i] + ".");

        if (parameterization_name[i] == "brownian")
          {
            ClassParameterizationCoagulationBrownian param(ops);
            parameterization_[i] = param.clone();
          }
#ifdef AMC_WITH_WAALS_VISCOUS
        else if (parameterization_name[i] == "brownian_waals_viscous")
          {
            ClassParameterizationCoagulationBrownianWaalsViscous param(ops);
            parameterization_[i] = param.clone();
          }
#endif
        else if (parameterization_name[i] == "turbulent")
          {
            ClassParameterizationCoagulationTurbulent param(ops);
            parameterization_[i] = param.clone();
          }
#ifdef AMC_WITH_COLLISION_EFFICIENCY
        else if (parameterization_name[i] == "gravitational_friedlander")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyFriedlander> param(ops);
            parameterization_[i] = param.clone();
          }
        else if (parameterization_name[i] == "gravitational_seinfeld")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencySeinfeld> param(ops);
            parameterization_[i] = param.clone();
          }
        else if (parameterization_name[i] == "gravitational_jacobson")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyJacobson> param(ops);
            parameterization_[i] = param.clone();
          }
#endif
        else
          throw AMC::Error("Parameterization \"" + parameterization_name[i] + "\" not implemented, valid types are " +
                           "\"brownian\", \"brownian_waals_viscous\", \"turbulent\", " +
                           "\"gravitational\" and \"table\".");
      }

    ops.SetPrefix(prefix_orig);

    // Table parameterization name.
    for (int i = 0; i < Nparam_; i++)
      this->name_ += "-" + parameterization_[i]->GetName();

    // Number of variables.
    for (int i = 0; i < Nparam_; i++)
      if (parameterization_[i]->GetNvariable() > this->Nvariable_)
        {
          this->Nvariable_ = parameterization_[i]->GetNvariable();
          parameterization_[i]->GetVariableName(this->variable_name_);
        }

    if (this->Nvariable_ > AMC_COAGULATION_TABLE_NUMBER_VARIABLE_MAX)
      throw AMC::Error("Number of variables (" + to_str(this->Nvariable_) + ") exceeds harcoded" +
                       " maximum (" + to_str(AMC_COAGULATION_TABLE_NUMBER_VARIABLE_MAX) + ").");

    // Read table variable discretization.
    Ndisc_.assign(this->Nvariable_, 0);
    value_min_.assign(this->Nvariable_, real(0));
    value_max_.assign(this->Nvariable_, real(0));

    for (int i = 0; i < this->Nvariable_; i++)
      {
        ops.SetPrefix(prefix_orig + this->variable_name_[i] + ".");
        Ndisc_[i] = ops.Get<int>("Ndisc");
        value_min_[i] = ops.Get<real>("value_min");
        value_max_[i] = ops.Get<real>("value_max");

        if (this->variable_name_[i].find("diameter") != string::npos)
          {
            value_min_[i] = log(value_min_[i]);
            value_max_[i] = log(value_max_[i]);
          }

        if (Ndisc_[i] == 0)
          value_max_[i] = value_min_[i];

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "Variable " << i << " :" << endl;
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "\tname = " << this->variable_name_[i] << endl;
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "\tNdisc = " << Ndisc_[i] << endl;
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "\tvalue_min = " << value_min_[i] << endl;
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "\tvalue_max = " << value_max_[i] << endl;
#endif
      }

    Npoint_.resize(this->Nvariable_);
    for (int i = 0; i < this->Nvariable_; i++)
      Npoint_[i] = Ndisc_[i] + 1;

    delta_.assign(this->Nvariable_, real(0));
    delta_inv_.assign(this->Nvariable_, real(0));
    for (int i = 0; i < this->Nvariable_; i++)
      if (Ndisc_[i] > 0)
        {
          delta_[i] = (value_max_[i] - value_min_[i]) / real(Ndisc_[i]);
          delta_inv_[i] = real(1) / delta_[i];
        }

    // Increment Ndisc for computation needs.
    Ndisc_work_ = Ndisc_;
    for (int i = 0; i < this->Nvariable_; i++)
      if (Ndisc_work_[i] == 0)
        Ndisc_work_[i] = 1;

    // Number of edges of hypercube.
    for (int i = 0; i < this->Nvariable_; i++)
      Nedge_ *= 2;

    // Revert to originale prefix.
    ops.SetPrefix(prefix_orig);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Nedge = " << Nedge_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Npoint = " << Npoint_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "delta = " << delta_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "delta_inv = " << delta_inv_ << endl;
#endif
  }


  // Constructors.
  ClassParameterizationCoagulationTable::ClassParameterizationCoagulationTable()
    : ClassParameterizationCoagulationBase("Table", 0), Nparam_(0),
      Ntable_(0), Nedge_(0), Ncube_(0), is_table_computed_(false)
  {
    return;
  }

  
  ClassParameterizationCoagulationTable::ClassParameterizationCoagulationTable(Ops::Ops &ops, const bool read_table)
    : ClassParameterizationCoagulationBase("Table", 0), Nparam_(0),
      Ntable_(1), Nedge_(1), Ncube_(1), is_table_computed_(false)
  {
    // Path to table file.
    path_ = ops.Get<string>("path", "", path_);

    // If real path, read directly in constructor.
    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

    // If file exists and its size is non zero, then read it,
    // or read the remaining of configuration file.
    if (file_size > 0 && read_table)
      Read();
    else
      construct(ops);

    // Increment for each hypercube edge from the down left corner.
    increment_.assign(Nedge_, vector1i());
    vector1i index(this->Nvariable_, 0);

    for (int i = 0; i < Nedge_; i++)
      {
        increment_[i] = index;

        index[0]++;
        for (int j = 0; j < this->Nvariable_ - 1; j++)
          if (index[j] == 2)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "increment = " << increment_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationCoagulationTable::~ClassParameterizationCoagulationTable()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationTable* ClassParameterizationCoagulationTable::clone() const
  {
    return new ClassParameterizationCoagulationTable(*this);
  }


  // Get methods.
  int ClassParameterizationCoagulationTable::GetNtable() const
  {
    return Ntable_;
  }

  
  int ClassParameterizationCoagulationTable::GetNcube() const
  {
    return Ncube_;
  }

  
  void ClassParameterizationCoagulationTable::GetNdisc(vector<int> &Ndisc) const
  {
    Ndisc = Ndisc_;
  }


  void ClassParameterizationCoagulationTable::GetDisc(const int &i, vector<real> &disc) const
  {
    disc.resize(Npoint_[i]);
    for (int j = 0; j < Npoint_[i]; j++)
      disc[j] = value_min_[i] + real(j) * delta_[i];

    // If diameters.
    if (i < 2)
      for (int j = 0; j < Npoint_[i]; j++)
        disc[j] = exp(disc[j]);
  }

  
  void ClassParameterizationCoagulationTable::GetNpoint(vector<int> &Npoint) const
  {
    Npoint = Npoint_;
  }

    
  void ClassParameterizationCoagulationTable::GetTable(vector<real> &kernel) const
  {
    kernel = table_edge_kernel_;
  }


  ClassParameterizationCoagulationBase* ClassParameterizationCoagulationTable::GetParameterization(const int &i)
  {
    return parameterization_[i];
  }


  // Compute table.
  void ClassParameterizationCoagulationTable::Compute()
  {
    if (is_table_computed_)
      throw AMC::Error("Table seems already computed, do you want to recompute it ? use \"Clear()\" method first.");

    // Record the beginning time.
    clock_t t_begin = clock();

    // Table size.
    for (int i = 0; i < this->Nvariable_; i++)
      Ntable_ *= Npoint_[i];

    // Number of hypercubes.
    for (int i = 0; i < this->Nvariable_; i++)
      Ncube_ *= Ndisc_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Ntable = " << Ntable_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Ncube = " << Ncube_ << endl;
#endif

    // Allocate table.
    table_edge_kernel_.assign(Ntable_, real(0));
    table_edge_index_.assign(Ncube_, vector1i(Nedge_, 0));

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Debug(3) << Reset() << "Free memory remaining = "
                         << util::get_memory_usage() * 100 << "%" << endl;
#endif

    // Compute kernel at table edges.
    vector1i index(this->Nvariable_, 0);
    vector1r variable(this->Nvariable_);

    for (unsigned long i = 0; i < Ntable_; i++)
      {
        // Take advantage that coagulation kernels are symetric
        // with respect to their diameters.
        for (int j = 0; j < this->Nvariable_; j++)
          variable[j] = value_min_[j] + delta_[j] * real(index[j]);

        // Diameters are discretized with respect to their logarithm.
        variable[0] = exp(variable[0]);
        variable[1] = exp(variable[1]);

        // Sum kernel parameterizations.
        // But be aware that coagulation kernels may not be additive.
        for (int j = 0; j < Nparam_; j++)
          table_edge_kernel_[i] += parameterization_[j]->KernelTable(variable);

        // Increment index.
        index[0]++;
        for (int j = 0; j < this->Nvariable_ - 1; j++)
          if (index[j] == Npoint_[j])
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

    // Compute edge indexes in table for each hypercube.
    index = vector1i(this->Nvariable_, 0);

    for (unsigned long i = 0; i < Ncube_; i++)
      {
        for (int j = 0; j < Nedge_; j++)
          {
            vector1i index2(index);
            for (int k = 0; k < this->Nvariable_; k++)
              index2[k] += increment_[j][k];

            // Locate point position in table.
            long unsigned k = index2.back();
            for (int l = this->Nvariable_ - 2; l >= 0; l--)
              k = index2[l] + k * Npoint_[l];

            table_edge_index_[i][j] = k;
          }

        // Increment index.
        index[0]++;
        for (int j = 0; j < this->Nvariable_ - 1; j++)
          if (index[j] == Ndisc_[j])
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

    // How much CPU time did we used ?
    clock_t t_end = clock();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Computed coagulation table for parameterizations \""
                         << this->name_ << "\" in " << real(t_end - t_begin) / real(CLOCKS_PER_SEC) << " seconds." << endl;
#else
    cout << "Computed coagulation table for parameterization \"" << this->name_
         << "\" in " << real(t_end - t_begin) / real(CLOCKS_PER_SEC) << " seconds." << endl;
#endif

    // At last.
    is_table_computed_ = true;
  }


  // Manage table.
  void ClassParameterizationCoagulationTable::Read()
  {
    check_file(path_);

    ifstream fin(path_.c_str(), ifstream::binary);

    fin.read(reinterpret_cast<char*>(&this->Nvariable_), sizeof(int));
    fin.read(reinterpret_cast<char*>(&Nedge_), sizeof(int));
    Ndisc_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(Ndisc_.data()), this->Nvariable_ * sizeof(int));
    Ndisc_work_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(Ndisc_work_.data()), this->Nvariable_ * sizeof(int));
    Npoint_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(Npoint_.data()), this->Nvariable_ * sizeof(int));
    value_min_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(value_min_.data()), this->Nvariable_ * sizeof(real));
    value_max_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(value_max_.data()), this->Nvariable_ * sizeof(real));
    delta_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(delta_.data()), this->Nvariable_ * sizeof(real));
    delta_inv_.resize(this->Nvariable_);
    fin.read(reinterpret_cast<char*>(delta_inv_.data()), this->Nvariable_ * sizeof(real));
    fin.read(reinterpret_cast<char*>(&Ntable_), sizeof(unsigned long));
    fin.read(reinterpret_cast<char*>(&Ncube_), sizeof(unsigned long));

    table_edge_index_.resize(Ncube_);
    for (unsigned long i = 0; i < Ncube_; i++)
      {
        table_edge_index_[i].resize(Nedge_);
        fin.read(reinterpret_cast<char*>(table_edge_index_[i].data()), Nedge_ * sizeof(int));
      }

    table_edge_kernel_.resize(Ntable_);
    fin.read(reinterpret_cast<char*>(table_edge_kernel_.data()), Ntable_ * sizeof(real));

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Read table for parameterizations \""
                         << this->name_ << "\" from file \"" << path_ << "\"." << endl;
#endif

    is_table_computed_ = true;
  }

  void ClassParameterizationCoagulationTable::Write() const
  {
    if (! is_table_computed_)
      throw AMC::Error("Table not yet computed.");

    ofstream fout(path_.c_str(), ofstream::binary);
    fout.write(reinterpret_cast<const char*>(&this->Nvariable_), sizeof(int));
    fout.write(reinterpret_cast<const char*>(&Nedge_), sizeof(int));
    fout.write(reinterpret_cast<const char*>(Ndisc_.data()), this->Nvariable_ * sizeof(int));
    fout.write(reinterpret_cast<const char*>(Ndisc_work_.data()), this->Nvariable_ * sizeof(int));
    fout.write(reinterpret_cast<const char*>(Npoint_.data()), this->Nvariable_ * sizeof(int));
    fout.write(reinterpret_cast<const char*>(value_min_.data()), this->Nvariable_ * sizeof(real));
    fout.write(reinterpret_cast<const char*>(value_max_.data()), this->Nvariable_ * sizeof(real));
    fout.write(reinterpret_cast<const char*>(delta_.data()), this->Nvariable_ * sizeof(real));
    fout.write(reinterpret_cast<const char*>(delta_inv_.data()), this->Nvariable_ * sizeof(real));
    fout.write(reinterpret_cast<const char*>(&Ntable_), sizeof(unsigned long));
    fout.write(reinterpret_cast<const char*>(&Ncube_), sizeof(unsigned long));

    for (unsigned long i = 0; i < Ncube_; i++)
      fout.write(reinterpret_cast<const char*>(table_edge_index_[i].data()), Nedge_ * sizeof(int));

    fout.write(reinterpret_cast<const char*>(table_edge_kernel_.data()), Ntable_ * sizeof(real));
    fout.close();

    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Wrote table for parameterizations \""
                         << this->name_ << "\" in file \"" << path_ << "\" of size "
                         << file_size / int(pow(2, 20)) << "Mo" << endl;
#endif
  }

  void ClassParameterizationCoagulationTable::Clear()
  {
    Ntable_ = 0;
    Ncube_ = 0;
    table_edge_index_.clear();
    table_edge_kernel_.clear();
    is_table_computed_ = false;
  }


  // Compute kernel for tabulation, it is obviously irrelevant in this context.
  real ClassParameterizationCoagulationTable::KernelTable(const vector1r &variable) const
  {
    vector1i index(this->Nvariable_, 0);
    vector1r position(this->Nvariable_, 0);

    for (int i = 0; i < 2; i++)
      {
        real log_variable = log(variable[i]);
        index[i] = int((log_variable - value_min_[i]) * delta_inv_[i] + AMC_COAGULATION_TABLE_EPSILON);
        position[i] = (log_variable - value_min_[i]) * delta_inv_[i] - real(index[i]);

        // Prevent extrapolation.
        if (index[i] < 0)
          {
            index[i] = 0;
            position[i] = real(0);
          }

        if (index[i] >= Ndisc_work_[i])
          {
            index[i] = Ndisc_work_[i] - 1;
            position[i] = real(1);
          }
      }

    for (int i = 2; i < this->Nvariable_; i++)
      {
        index[i] = int((variable[i] - value_min_[i]) * delta_inv_[i] + AMC_COAGULATION_TABLE_EPSILON);
        position[i] = (variable[i] - value_min_[i]) * delta_inv_[i] - real(index[i]);

        // Prevent extrapolation.
        if (index[i] < 0)
          {
            index[i] = 0;
            position[i] = real(0);
          }

        if (index[i] >= Ndisc_work_[i])
          {
            index[i] = Ndisc_work_[i] - 1;
            position[i] = real(1);
          }
      }

    // Index in table.
    unsigned long table_index_base = index.back();
    for (int i = this->Nvariable_ - 2; i>= 2; i--)
      table_index_base = table_index_base * Ndisc_work_[i] + index[i];
    table_index_base *= (Ndisc_work_[0] * Ndisc_work_[1]);

    unsigned long table_index = table_index_base + Ndisc_work_[0] * index[1] + index[0];

    const vector1i &edge_index = table_edge_index_[table_index];

    // Compute fraction position inside hypercube.
    vector1r edge_coefficient(Nedge_, real(1));
    for (int i = 0; i < Nedge_; i++)
      for (int j = 0; j < this->Nvariable_; j++)
        edge_coefficient[i] *= real(increment_[i][j]) * position[j]
          + real(1 - increment_[i][j]) * (real(1) - position[j]);

    // Finally compute kernel with linear interpolation.
    real table_kernel(real(0));
    for (int i = 0; i < Nedge_; i++)
      table_kernel += table_edge_kernel_[edge_index[i]] * edge_coefficient[i];

    return table_kernel;
  }


  // Compute kernel.
  void ClassParameterizationCoagulationTable::ComputeKernel(const vector2i &couple,
                                                            Array<real, 2> &kernel) const
  {
    vector1r variable(AMC_COAGULATION_TABLE_NUMBER_VARIABLE_MAX);

    variable[2] = ClassMeteorologicalData::temperature_;
    variable[3] = ClassMeteorologicalData::pressure_;
    variable[4] = ClassMeteorologicalData::relative_humidity_;
    variable[5] = ClassMeteorologicalData::eddy_dissipation_rate_;

    vector1i index(this->Nvariable_, 0);
    vector1r position(this->Nvariable_, 0);
    for (int i = 2; i < this->Nvariable_; i++)
      {
        index[i] = int((variable[i] - value_min_[i]) * delta_inv_[i] + AMC_COAGULATION_TABLE_EPSILON);
        position[i] = (variable[i] - value_min_[i]) * delta_inv_[i] - real(index[i]);

        // Prevent extrapolation.
        if (index[i] < 0)
          {
            index[i] = 0;
            position[i] = real(0);
          }

        if (index[i] >= Ndisc_work_[i])
          {
            index[i] = Ndisc_work_[i] - 1;
            position[i] = real(1);
          }
      }

    unsigned long table_index;
    unsigned long table_index_base = index.back();
    for (int i = this->Nvariable_ - 2; i>= 2; i--)
      table_index_base = table_index_base * Ndisc_work_[i] + index[i];
    table_index_base *= (Ndisc_work_[0] * Ndisc_work_[1]);

    int Ncouple = int(couple.size());
    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        variable[0] = log(ClassAerosolData::diameter_[g1]);
        variable[1] = log(ClassAerosolData::diameter_[g2]);

        for (int j = 0; j < 2; j++)
          {
            index[j] = int((variable[j] - value_min_[j]) * delta_inv_[j] + AMC_COAGULATION_TABLE_EPSILON);
            position[j] = (variable[j] - value_min_[j]) * delta_inv_[j] - real(index[j]);

            // Prevent extrapolation.
            if (index[j] < 0)
              {
                index[j] = 0;
                position[j] = real(0);
              }

            if (index[j] >= Ndisc_work_[j])
              {
                index[j] = Ndisc_work_[j] - 1;
                position[j] = real(1);
              }
          }

        // Index in table.
        table_index = table_index_base + Ndisc_work_[0] * index[1] + index[0];

        const vector1i &edge_index = table_edge_index_[table_index];

        // Compute fraction position inside hypercube.
        vector1r edge_coefficient(Nedge_, real(1));
        for (int j = 0; j < Nedge_; j++)
          for (int k = 0; k < this->Nvariable_; k++)
            edge_coefficient[j] *= real(increment_[j][k]) * position[k]
              + real(1 - increment_[j][k]) * (real(1) - position[k]);

        // Finally compute kernel with linear interpolation.
        real table_kernel(real(0));
        for (int j = 0; j < Nedge_; j++)
          table_kernel += table_edge_kernel_[edge_index[j]] * edge_coefficient[j];

        kernel(g1, g2) += table_kernel;
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TABLE_CXX
#endif
