// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FRACTAL_SOOT_HXX

#define AMC_DIAMETER_FRACTAL_SOOT_SPHERULE_DIAMETER_DEFAULT 0.025
#define AMC_DIAMETER_FRACTAL_SOOT_FRACTAL_DIMENSION_DEFAULT 1.7
#define AMC_DIAMETER_FRACTAL_SOOT_FRACTAL_PREFACTOR_DEFAULT 2.2
#define AMC_DIAMETER_FRACTAL_SOOT_SURFACE_STICKING_RATIO    0.1
#define AMC_DIAMETER_FRACTAL_SOOT_SPHERE_DENSITY_PACKING_MAXIMUM  0.74048048969306113
#define AMC_DIAMETER_FRACTAL_SOOT_FRACTION_THRESHOLD 0.8
#define AMC_DIAMETER_FRACTAL_SOOT_DENSITY 2.25
#define AMC_DIAMETER_FRACTAL_SOOT_PI     3.1415926535897932
#define AMC_DIAMETER_FRACTAL_SOOT_PI6    0.5235987755982988
#define AMC_DIAMETER_FRACTAL_SOOT_CONVERT_FROM_GCM3_TO_MICROGMICROM3  1.e-6


namespace AMC
{
  class ClassParameterizationDiameterFractalSoot : public ClassParameterizationDiameterDensityFixed
  {
  private:

    /*!< Soot index in mass concentration vector.*/
    int soot_index_;

    /*!< fraction soot threshold.*/
    real fraction_soot_threshold_;

    /*!< Soot spherule diameter.*/
    real spherule_diameter_;

    /*!< Soot spherule surface.*/
    real spherule_surface_;

    /*!< Soot spherule volume.*/
    real spherule_volume_;

    /*!< Soot spherule mass.*/
    real spherule_single_mass_;

    /*!< Ratio of surface of spherule which sticks to other spherules.*/
    real surface_sticking_ratio_;

    /*!< Soot fractal dimension.*/
    real fractal_dimension_;

    /*!< Soot fractal prefactor.*/
    real fractal_prefactor_;

    /*!< Soot fractal prefactor inverse.*/
    real fractal_prefactor_inverse_;

    /*!< Maximum of sphere packing density.*/
    real sphere_packing_density_max_;

    /*!< Inverse maximum of sphere packing density.*/
    real sphere_packing_density_max_inverse_;

    /*!< Opposite of maximum of sphere packing density.*/
    real sphere_packing_density_max_opposite_;

    /*!< Construct method.*/
    void Construct();

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterFractalSoot(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationDiameterFractalSoot();

    /*!< Clone.*/
    ClassParameterizationDiameterFractalSoot* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationDiameterBase *param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    void ComputeDiameter(const int section_min,
                         const int section_max,
                         const vector1i &parameterization_section,
                         const real *concentration_aer_number,
                         const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FRACTAL_SOOT_HXX
#endif
