// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_HXX

#define AMC_COAGULATION_BROWNIAN_KNUDSEN_MIN  0.01
#define AMC_COAGULATION_BROWNIAN_KNUDSEN_MAX  100.0
#define AMC_COAGULATION_BROWNIAN_8PLUSPI     11.141592653589793     // 8 + Pi
#define AMC_COAGULATION_BROWNIAN_FRAC3        0.3333333333333333
#define AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT  1.3806488e-23  // Boltzmann constant m^2.kg.s^{-2}.K^{-1}


namespace AMC
{
  class ClassParameterizationCoagulationBrownian : public ClassParameterizationCoagulationBase
  {
  protected:

    /*!< Knudsen minimum and maximum.*/
    real knudsen_min_, knudsen_max_;

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationBrownian(const string name = "Brownian", const int Nvariable = 2);
    ClassParameterizationCoagulationBrownian(Ops::Ops &ops, const string name = "Brownian", const int Nvariable = 2);

    /*!< Destructor.*/
    ~ClassParameterizationCoagulationBrownian();

    /*!< Clone.*/
    ClassParameterizationCoagulationBrownian* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationCoagulationBase *param);

    /*!< Get methods.*/
    real GetKnudsenMin() const; 
    real GetKnudsenMax() const;

    /*!< Set methods.*/
    void SetKnudsenMin(const real &knudsen_min);
    void SetKnudsenMax(const real &knudsen_max);

    /*!< Compute brownian kernel.*/
    real KernelContinuous(const real &diameter1, const real &diameter2,
                          const real &knudsen1, const real &knudsen2,
                          const real &temperature, const real &air_dynamic_viscosity) const;

    real KernelFreeMolecular(const real &diameter1, const real &diameter2,
                             const real &mass1, const real &mass2,
                             const real &temperature) const;

    real KernelTransition(const real &diameter1, const real &diameter2,
                          const real &knudsen1, const real &knudsen2,
                          const real &mass1, const real &mass2,
                          const real &temperature, const real &air_dynamic_viscosity) const;

    /*!< Compute kernel for tabulation.*/
    real KernelTable(const vector1r &variable) const;

#ifndef SWIG
    /*!< Compute kernel for AMC.*/
    void ComputeKernel(const vector2i &couple,
                       Array<real, 2> &kernel) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_HXX
#endif
