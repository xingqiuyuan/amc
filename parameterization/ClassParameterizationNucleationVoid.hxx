// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VOID_HXX

namespace AMC
{
  class ClassParameterizationNucleationVoid : public ClassParameterizationNucleationBase
  {
  public:

    ClassParameterizationNucleationVoid();

    ~ClassParameterizationNucleationVoid();

    /*!< Clone.*/
    ClassParameterizationNucleationVoid* clone() const;

#ifndef SWIG
    /*!< Compute kernel.*/
    void ComputeKernel(const int &section_index,
                       const real *concentration_gas,
                       vector1r &rate_aer_number,
                       vector1r &rate_aer_mass) const;

    bool ComputeKernel(const vector1r &concentration_gas,
                       real &diameter,
                       real &rate_aer_number,
                       vector1r &rate_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VOID_HXX
#endif
