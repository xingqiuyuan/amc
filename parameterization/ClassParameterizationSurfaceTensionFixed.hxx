// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_FIXED_HXX

#define AMC_PARAMETERIZATION_SURFACE_TENSION_FIXED  5.e-2  // Default particle surface tension in N.m^{-1}

namespace AMC
{
  class ClassParameterizationSurfaceTensionFixed : public ClassParameterizationSurfaceTensionBase
  {
  private:

    /*!< Vector of fixed surface tension for each general section and phases.*/
    vector2r surface_tension_fixed_;

  public:

    /*!< Constructors.*/
    ClassParameterizationSurfaceTensionFixed(Ops::Ops &ops, const int &Ng);

    /*!< Destructor.*/
    ~ClassParameterizationSurfaceTensionFixed();

    /*!< Get methods.*/
    void GetSurfaceTensionFixed(vector<vector<real> > &surface_tension_fixed) const;

    /*!< Clone.*/
    ClassParameterizationSurfaceTensionFixed* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationSurfaceTensionBase *param);

#ifndef SWIG
    /*!< Compute surface tension for AMC.*/
    void ComputeSurfaceTension(const int &section_min,
                               const int &section_max,
                               const vector1i &parameterization_section,
                               const real *concentration_aer_number,
                               const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_FIXED_HXX
#endif
