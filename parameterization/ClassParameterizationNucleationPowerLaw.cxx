// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_POWER_LAW_CXX

#include "ClassParameterizationNucleationPowerLaw.hxx"

namespace AMC
{
  // Get available parameterization.
  int ClassParameterizationNucleationPowerLaw::GetAvailableParameter(vector<string> &name,
                                                                     vector<real> &P,
                                                                     vector<real> &K,
                                                                     vector<real> &logK,
                                                                     string configuration_file)
  {
    if (configuration_file.empty())
      configuration_file = configuration_file_;

    if (configuration_file.empty())
      configuration_file = ClassConfiguration::GetFile();

    if (configuration_file.empty())
      throw AMC::Error(string("Unable to find one configuration file for power law parameters, ") +
                       string("should be in \"") + configuration_file_ + string("\"."));

    check_file(configuration_file);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << "Get available power law parameters with file \""
                         << configuration_file << "\"." << endl;
#endif

    Ops::Ops ops(configuration_file);

    int i(0);

    ops.SetPrefix("power_law.");
    vector1s publication_name = ops.GetEntryList();
    for (int j = 0; j < int(publication_name.size()); j++)
      {
        vector1s param_name = ops.GetEntryList(publication_name[j]);

        for (int k = 0; k < int(param_name.size()); k++)
          {
            string prefix = publication_name[j] + "." + param_name[k];

            name.push_back(prefix);
            P.push_back(ops.Get<real>(prefix + ".P"));

            if (ops.Exists(prefix + ".logK"))
              {
                logK.push_back(ops.Get<real>(prefix + ".logK"));
                K.push_back(pow(real(10), logK.back()));
              }
            else if (ops.Exists(prefix + ".K"))
              {
                K.push_back(ops.Get<real>(prefix + ".K"));
                logK.push_back(log10(K.back()));
              }
            else
              throw AMC::Error("Did not find logK or K value for \"" + prefix + "\".");

            i++;
          }
      }

    ops.Close();

    return i;
  }


  // Get configuration file.
  string ClassParameterizationNucleationPowerLaw::GetConfigurationFile()
  {
    return configuration_file_;
  }


  // Constructors.
  ClassParameterizationNucleationPowerLaw::ClassParameterizationNucleationPowerLaw()
    : ClassParameterizationNucleationBase("PowerLaw"),
      diameter_(AMC_NUCLEATION_POWER_LAW_DIAMETER_DEFAULT),
      K_(real(0)), P_(real(0)), P_is_activation_(false), P_is_kinetic_(false),
      density_(real(0)), mass_(real(0))
  {
    return;
  }


  ClassParameterizationNucleationPowerLaw::ClassParameterizationNucleationPowerLaw(Ops::Ops &ops)
    : ClassParameterizationNucleationBase("PowerLaw"),
      diameter_(AMC_NUCLEATION_POWER_LAW_DIAMETER_DEFAULT),
      K_(real(0)), P_(real(0)), P_is_activation_(false), P_is_kinetic_(false),
      density_(real(0)), mass_(real(0))
  {
    string prefix_orig = ops.GetPrefix();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Instantiate Power Law nucleation parameterization"
                         << " with prefix \"" << prefix_orig << "\"." << endl;
#endif

    //
    // Number of species.
    //

    if (ops.Exists("species"))
      {
        vector1s species_name;
        ops.Set("species", "", species_name);
        for (int i = 0; i < int(species_name.size()); ++i)
          this->set_index_amc(species_name[i]);
      }

    // Add water.
    this->species_index_amc_.push_back(-1);
    this->gas_index_amc_.push_back(-1);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset()
                         << "Number of species involved in nucleation parameterization : " << Nspecies_ << endl;
#endif

    //
    // Parameters.
    //

    string type = ops.Get<string>("type");

    if (type.find(":") != string::npos)
      {
        type = type.substr(type.find(":") + 1);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(2) << Reset() << "using parameters \"" << type << "\"." << endl;
#endif

        ops.SetPrefix("power_law.");

        P_ = ops.Get<real>(type + ".P");

        if (ops.Exists(type + ".logK"))
          K_ = pow(real(10), ops.Get<real>(type + ".logK"));
        else if (ops.Exists(type + ".K"))
          K_ = ops.Get<real>(type + ".K");
        else
          throw AMC::Error("Did not find logK or K value for \"" + type + "\".");

        ops.SetPrefix(prefix_orig);

        // Rename parameterization.
        this->name_ += ":" + type;
      }

    // Eventually redefine parameters.
    P_ = ops.Get<real>("P", "", P_);
    P_is_activation_ = P_ == real(1);
    P_is_kinetic_ = P_ == real(2);

    if (P_ <= real(0))
      throw AMC::Error("Inconsistent factor P (=" + to_str(P_) + ") for power law nucleation parameterization.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "P = " << P_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "P_is_activation = " << (P_is_activation_ ? "true" : "false") << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "P_is_kinetic = " << (P_is_kinetic_ ? "true" : "false") << endl;
#endif

    if (ops.Exists("logK"))
      K_ = pow(real(10), ops.Get<real>("logK"));
    else if (ops.Exists("K"))
      K_ = ops.Get<real>("K");

    if (K_ <= real(0))
      throw AMC::Error("Inconsistent prefactor K (=" + to_str(K_) + ") for power law nucleation parameterization.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Debug(1) << Reset() << "K = " << K_ << endl;
    *AMCLogger::GetLog() << Bcyan() << Debug(2) << Reset() << "logK = " << log10(K_) << endl;
#endif

    //
    // Particle diameter and its composition.
    //

    diameter_ = ops.Get<real>("diameter", "", diameter_);

    if (ops.Exists("species_fraction"))
      ops.Set("species_fraction", "", species_fraction_);
    else
      species_fraction_.push_back(AMC_NUCLEATION_POWER_LAW_FRACTION_SULFATE_DEFAULT);

    if (int(species_fraction_.size()) != this->Nspecies_)
      throw AMC::Error("Wrong number of mass fractions, should be = " + to_str(this->Nspecies_) + ".");

    // Water mass fracion of nucleated particle.
    real water_fraction = real(1);
    for (int i = 0; i < this->Nspecies_; ++i)
      water_fraction -= species_fraction_[i];

    if (water_fraction < real(0))
      throw AMC::Error("For nucleation power law, water mass fraction < 0");

    species_fraction_.push_back(water_fraction);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Debug(1) << Reset() << "diameter = " << diameter_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "species_fraction = " << species_fraction_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "water_fraction = " << water_fraction << endl;
#endif

    // Particle density in µg.µm^{-3}.
    density_ = real(0);
    for (int i = 0; i < this->Nspecies_; ++i)
      density_ += species_fraction_[i] / ClassSpecies::density_[this->species_index_amc_[i]];
    density_ += water_fraction / AMC_WATER_DENSITY;
    density_ = real(1) / density_;

    // Single particle mass in µg.
    mass_ = density_ * AMC_PI6 * diameter_ * diameter_ * diameter_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "density = " << density_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "mass = " << mass_ << endl;
#endif

    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassParameterizationNucleationPowerLaw::~ClassParameterizationNucleationPowerLaw()
  {
    return;
  }


  // Clone.
  ClassParameterizationNucleationPowerLaw* ClassParameterizationNucleationPowerLaw::clone() const
  {
    return new ClassParameterizationNucleationPowerLaw(*this);
  }

  // Get parameters.
  void ClassParameterizationNucleationPowerLaw::GetGetSpeciesFraction(vector<real> &species_fraction) const
  {
    species_fraction = species_fraction_;
  }

  real ClassParameterizationNucleationPowerLaw::GetDiameter() const
  {
    return diameter_;
  }


  real ClassParameterizationNucleationPowerLaw::GetDensity() const
  {
    return density_;
  }


  real ClassParameterizationNucleationPowerLaw::GetK() const
  {
    return K_;
  }


  real ClassParameterizationNucleationPowerLaw::GetLogK() const
  {
    return log10(K_);
  }


  real ClassParameterizationNucleationPowerLaw::GetP() const
  {
    return P_;
  }


  // Compute kernel.
  real ClassParameterizationNucleationPowerLaw::ComputeKernel(const real &concentration_sulfate_molec_cm3) const
  {
    real flux_number_cm3s = K_;
    if (P_is_activation_)
      flux_number_cm3s *= concentration_sulfate_molec_cm3;
    else if (P_is_kinetic_)
      flux_number_cm3s *= concentration_sulfate_molec_cm3 * concentration_sulfate_molec_cm3;
    else
      flux_number_cm3s *= pow(concentration_sulfate_molec_cm3, P_);

    return flux_number_cm3s;
  }


  void ClassParameterizationNucleationPowerLaw::ComputeKernel(const int &section_index,
                                                              const real *concentration_gas,
                                                              vector1r &rate_aer_number,
                                                              vector1r &rate_aer_mass) const
  {
    // Sulfuric acid concentration in molecule / cm3.
    real concentration_sulfate_molec_cm3 = concentration_gas[this->gas_index_amc_[AMC_NUCLEATION_SULFATE_INDEX]]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;

    // Number flux. The 1e6 factor converts from #.cm^{-3} to #.m^{-3}.
    real flux_number_m3s = K_ * real(1e6);
    if (P_is_activation_)
      flux_number_m3s *= concentration_sulfate_molec_cm3;
    else if (P_is_kinetic_)
      flux_number_m3s *= concentration_sulfate_molec_cm3 * concentration_sulfate_molec_cm3;
    else
      flux_number_m3s *= pow(concentration_sulfate_molec_cm3, P_);

    // Update rates, do not forget we possibly add other things (coagulation, condensation).
    rate_aer_number[section_index] += flux_number_m3s;

    const int i = section_index * ClassSpecies::Nspecies_;
    real flux_mass_m3s = flux_number_m3s * mass_;
    for (int j = 0; j < ClassParameterizationNucleationBase::Nspecies_; ++j)
      rate_aer_mass[i + this->species_index_amc_[j]] += flux_mass_m3s * species_fraction_[j];
  }


#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  bool ClassParameterizationNucleationPowerLaw::ComputeKernel(const vector1r &concentration_gas,
                                                              real &diameter,
                                                              real &rate_aer_number,
                                                              vector1r &rate_aer_mass) const
  {
    // Sulfuric acid concentration in molecule / cm3.
    real concentration_sulfate_molec_cm3 = concentration_gas[AMC_NUCLEATION_SULFATE_INDEX]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;

    // Number flux in #.cm^{-3}.s{-1}.
    real flux_number_cm3s = K_;
    if (P_is_activation_)
      flux_number_cm3s *= concentration_sulfate_molec_cm3;
    else if (P_is_kinetic_)
      flux_number_cm3s *= concentration_sulfate_molec_cm3 * concentration_sulfate_molec_cm3;
    else
      flux_number_cm3s *= pow(concentration_sulfate_molec_cm3, P_);

    rate_aer_number = flux_number_cm3s * real(1e6);
    const real flux_mass_m3s = rate_aer_number * mass_;

    // This includes water (<=).
    for (int i = 0; i <= ClassParameterizationNucleationBase::Nspecies_; ++i)
      rate_aer_mass[this->index_npf_[i]] = flux_mass_m3s * species_fraction_[i];

    diameter = diameter_;

    return flux_number_cm3s > real(1);
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_POWER_LAW_CXX
#endif
