// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_BASE_CXX

#include "ClassParameterizationDiameterBase.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationDiameterBase::ClassParameterizationDiameterBase(const string name)
    : name_(name)
  {
    return;
  }


  // Destructor.
  ClassParameterizationDiameterBase::~ClassParameterizationDiameterBase()
  {
    return;
  }


  // Get methods.
  string ClassParameterizationDiameterBase::GetName() const
  {
    return name_;
  }

  // Copy.
  void ClassParameterizationDiameterBase::Copy(const ClassParameterizationDiameterBase &param)
  {
    name_ = param.name_;
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_BASE_CXX
#endif
