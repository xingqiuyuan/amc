// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_JACOBSON_CXX

#include "ClassParameterizationCollisionEfficiencyJacobson.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCollisionEfficiencyJacobson::ClassParameterizationCollisionEfficiencyJacobson()
    : ClassParameterizationCollisionEfficiencyBase("Jacobson")
  {
    return;
  }


  // Destructor.
  ClassParameterizationCollisionEfficiencyJacobson::~ClassParameterizationCollisionEfficiencyJacobson()
  {
    return;
  }


  // Clone.
  ClassParameterizationCollisionEfficiencyJacobson* ClassParameterizationCollisionEfficiencyJacobson::clone() const
  {
    return new ClassParameterizationCollisionEfficiencyJacobson(*this);
  }


  // Compute collision efficiency.
  real ClassParameterizationCollisionEfficiencyJacobson::Compute(const real &diameter1,
                                                                 const real &diameter2,
                                                                 const real &slip_flow_correction1,
                                                                 const real &slip_flow_correction2,
                                                                 const real &terminal_velocity1,
                                                                 const real &terminal_velocity2,
                                                                 const real &temperature,
                                                                 const real &air_free_mean_path,
                                                                 const real &air_density,
                                                                 const real &air_dynamic_viscosity,
                                                                 const real &water_dynamic_viscosity) const
  {
    // Stokes number of smallest particle. 2 * 1.e6 / G
    real St = real(2.038735983690112e5) * abs(terminal_velocity1 - terminal_velocity2);

    // Reynolds number of largest particle. Based on diameter.
    real Re = real(1.e-6) * air_density / air_dynamic_viscosity;

    if (diameter2 >= diameter1)
      {
        St *= terminal_velocity1 / diameter2;
        Re *= diameter2 * terminal_velocity2;
      }
    else
      {
        St *= terminal_velocity2 / diameter1;
        Re *= diameter1 * terminal_velocity1;
      }

    // Parameterization.
    real Ea = St / (St + real(0.5));
    Ea *= Ea;

    real Ev(real(0));
    if (St > real(1.214))
      {
        Ev = real(1) + real(0.75) * log(real(2) * St) / (St - real(1.214));
        Ev = real(1) / (Ev * Ev);
      }

    return (real(60) * Ev + Ea * Re) / (real(60) + Re);
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_JACOBSON_CXX
#endif
