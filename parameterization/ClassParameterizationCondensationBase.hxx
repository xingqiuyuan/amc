// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_BASE_HXX

#define AMC_CONDENSATION_IDEAL_GAS_CONSTANT 8.314462175

namespace AMC
{
  class ClassParameterizationCondensationBase
  {
  public:

    typedef AMC_PARAMETERIZATION_REAL real;
    typedef vector<int> vector1i;
    typedef vector<vector1i> vector2i;
    typedef vector<real> vector1r;
    typedef vector<vector1r> vector2r;

  protected:

    /*!< Parameterization name.*/
    string name_;

  public:

    /*!< Constructor.*/
    ClassParameterizationCondensationBase(const string name = "");

    /*!< Destructor.*/
    virtual ~ClassParameterizationCondensationBase();

    /*!< Get methods.*/
    string GetName() const;

    /*!< Clone.*/
    virtual ClassParameterizationCondensationBase* clone() const = 0;

    /*!< Compute coefficient.*/
    virtual void ComputeCoefficient(const int section_min,
                                    const int section_max,
                                    const vector1i &parameterization_section,
                                    const real *concentration_aer_number,
                                    vector1r &condensation_coefficient) const = 0;

    /*!< Compute kernel.*/
    virtual void ComputeKernel(const int section_min,
                               const int section_max,
                               const vector1i &parameterization_section,
                               const real *concentration_aer_number,
                               const real *concentration_aer_mass,
                               const real *concentration_gas,
                               vector1r &condensation_coefficient,
                               vector1r &rate_aer_mass) const = 0;
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_BASE_HXX
#endif
