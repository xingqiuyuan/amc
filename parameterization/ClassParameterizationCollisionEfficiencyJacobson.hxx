// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_JACOBSON_HXX


namespace AMC
{
  class ClassParameterizationCollisionEfficiencyJacobson : public ClassParameterizationCollisionEfficiencyBase
  {

  public:

    /*!< Constructors.*/
    ClassParameterizationCollisionEfficiencyJacobson();

    /*!< Destructor.*/
    ~ClassParameterizationCollisionEfficiencyJacobson();

#ifndef SWIG
    /*!< Clone.*/
    ClassParameterizationCollisionEfficiencyJacobson* clone() const;
#endif

    /*!< Compute collision efficiency.*/
    real Compute(const real &diameter1,
                 const real &diameter2,
                 const real &slip_flow_correction1,
                 const real &slip_flow_correction2,
                 const real &terminal_velocity1,
                 const real &terminal_velocity2,
                 const real &temperature,
                 const real &air_free_mean_path,
                 const real &air_density,
                 const real &air_dynamic_viscosity,
                 const real &water_dynamic_viscosity) const;
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_JACOBSON_HXX
#endif
