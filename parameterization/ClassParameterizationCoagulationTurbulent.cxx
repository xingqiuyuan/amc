// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TURBULENT_CXX

#include "ClassParameterizationCoagulationTurbulent.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCoagulationTurbulent::ClassParameterizationCoagulationTurbulent()
    : ClassParameterizationCoagulationBase("Turbulent", 4)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Turbulent coagulation." << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";
    this->variable_name_[4] = "relative_humidity";
    this->variable_name_[5] = "eddy_dissipation_rate";

    return;
  }

  ClassParameterizationCoagulationTurbulent::ClassParameterizationCoagulationTurbulent(Ops::Ops &ops)
    : ClassParameterizationCoagulationBase("Turbulent", 4)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Turbulent coagulation." << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";
    this->variable_name_[4] = "relative_humidity";
    this->variable_name_[5] = "eddy_dissipation_rate";

    return;
  }


  // Destructor.
  ClassParameterizationCoagulationTurbulent::~ClassParameterizationCoagulationTurbulent()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationTurbulent* ClassParameterizationCoagulationTurbulent::clone() const
  {
    return new ClassParameterizationCoagulationTurbulent(*this);
  }


  // Compute brownian kernel.
  real ClassParameterizationCoagulationTurbulent::Kernel(const real &diameter1, const real &diameter2,
                                                         const real &eddy_dissipation_rate,
                                                         const real &air_dynamic_viscosity,
                                                         const real &air_density) const
  {
    real diameter_sum = diameter1 + diameter2;

    return sqrt(AMC_COAGULATION_TURBULENT_PI_DIV_120 * eddy_dissipation_rate
                * air_density / air_dynamic_viscosity) * real(1.e-18)
      * diameter_sum * diameter_sum * diameter_sum;
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationTurbulent::KernelTable(const vector1r &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];
    real relative_humidity = variable[4];
    real eddy_dissipation_rate = variable[5];

    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);
    real air_density = ClassParameterizationPhysics::ComputeAirDensity(temperature, pressure, relative_humidity);

    return Kernel(diameter1, diameter2, eddy_dissipation_rate, air_dynamic_viscosity, air_density);
  }


  // Compute kernel for AMC.
  void ClassParameterizationCoagulationTurbulent::ComputeKernel(const vector2i &couple,
                                                                Array<real, 2> &kernel) const
  {
    int Ncouple = int(couple.size());
    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        kernel(g1, g2) += Kernel(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                 ClassMeteorologicalData::eddy_dissipation_rate_,
                                 ClassMeteorologicalData::air_dynamic_viscosity_,
                                 ClassMeteorologicalData::air_density_);
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TURBULENT_CXX
#endif
