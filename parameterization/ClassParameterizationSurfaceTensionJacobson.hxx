// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_JACOBSON_HXX

#define PARAMETERIZATION_SURFACE_TENSION_LOGARITHM_CUTOFF_DEFAULT 0.01

namespace AMC
{
  class ClassParameterizationSurfaceTensionJacobson : public  ClassParameterizationSurfaceTensionBase
  {
  private:

    /*!< Number of organic aqueous species.*/
    int Norganic_aqueous_;

    /*!< Number of ionic inorganic species.*/
    int Ninorganic_ionic_;

    /*!< Logarithm cutoff.*/
    real logarithm_cutoff_;

    /*!< Index of aqueous organic species.*/
    vector1i organic_aqueous_;

    /*!< Index of ionic inorganic species.*/
    vector1i inorganic_ionic_;

    /*!< factor for organic aqueous species derived from their molar mass.*/
    vector1i factor_organic_aqueous_;

    /*!< Molar mass of ionic inorganic species.*/
    vector1i molar_mass_inorganic_ionic_;

  public:

    /*!< Constructors.*/
    ClassParameterizationSurfaceTensionJacobson(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationSurfaceTensionJacobson();

    /*!< Clone.*/
    ClassParameterizationSurfaceTensionJacobson* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationSurfaceTensionBase *param);

#ifndef SWIG
    /*!< Compute surface tension for AMC.*/
    void ComputeSurfaceTension(const int &section_min,
                               const int &section_max,
                               const vector1i &parameterization_section,
                               const real *concentration_aer_number,
                               const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_JACOBSON_HXX
#endif
