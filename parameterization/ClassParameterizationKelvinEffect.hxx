// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_KELVIN_EFFECT_HXX

#define AMC_KELVIN_EFFECT_SURFACE_TENSION_DEFAULT 5.e-2
#define AMC_KELVIN_EFFECT_EXPONENTIONAL_CUTOFF 0.1

namespace AMC
{
  class ClassParameterizationKelvinEffect
  {
  public:
    typedef AMC_PARAMETERIZATION_REAL real;

  private:

    /*!< Molar mass of species : proxy from ClassSpecies.*/
    vector1r molar_mass_;

    /*!< Exponential cutoff.*/
    real exponential_cutoff_;

  public:

    /*!< Constructors.*/
    ClassParameterizationKelvinEffect(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationKelvinEffect();

    /*!< Compute Kelvin effect and correct equilibrium gas surface.*/
    void ComputeKelvinEffect(const int section_min,
                             const int section_max,
                             const real *concentration_aer_number,
                             const real *concentration_aer_mass,
                             vector1r &kelvin_effect) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    void Test(const real &temperature,
              const real &diameter,
              const real &density,
              const real &liquid_water_content,
              const vector<real> &surface_tension,
              const vector<real> &concentration_aer_mass,
              vector<real> &kelvin_effect) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_KELVIN_EFFECT_HXX
#endif
