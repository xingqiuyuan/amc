// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_BASE_CXX

#include "ClassParameterizationSurfaceTensionBase.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationSurfaceTensionBase::ClassParameterizationSurfaceTensionBase(Ops::Ops &ops, const string name)
    : name_(name), phase_index_(-1)
  {
    // Phase index. A negative one (default) means all phase are managed.
    phase_index_ = ops.Get<int>("phase_index", "", phase_index_);

    if (phase_index_ >= ClassPhase::GetNphase())
      throw AMC::Error("Specified phase index " + to_str(phase_index_) +
                       " which is >= to the current number of phase in AMC.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "phase_index = " << phase_index_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Surface tension parameterization \"" << name_  << "\" manages"
                         << ((phase_index_ < 0) ? " all phases." : "phase \"" + ClassPhase::GetName(phase_index_)) << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationSurfaceTensionBase::~ClassParameterizationSurfaceTensionBase()
  {
    return;
  }


  // Get methods.
  string ClassParameterizationSurfaceTensionBase::GetName() const
  {
    return name_;
  }


  int ClassParameterizationSurfaceTensionBase::GetPhaseIndex() const
  {
    return phase_index_;
  }


  // Copy.
  void ClassParameterizationSurfaceTensionBase::Copy(const ClassParameterizationSurfaceTensionBase &param)
  {
    name_ = param.name_;
    phase_index_ = param.phase_index_;
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_BASE_CXX
#endif
