flux_correction_aqueous = {
   -- Order of species is hardcoded, you may change their name (eg. H2SO4 -> sulfate)
   -- but do not alter the order.
   species = {"H2SO4", "NH3", "HNO3", "HCl"},
   molar_mass = {98.0, 17.0, 63.0, 36.5},
   hydronium_name = "Hp",
   limitation_hydronium = 0.1
}