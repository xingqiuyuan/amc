// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_BASE_HXX

#define AMC_PARAMETERIZATION_DIAMETER_DENSITY_DEFAULT 1.2 // Default particle density in g.cm^{-3}
#define AMC_PARAMETERIZATION_DIAMETER_CONVERT_FROM_GCM3_TO_MICROGMICROM3  1.e-6 // Convert factor from g.cm^{-3} to µg.µm^{-3}

namespace AMC
{
  class ClassParameterizationDiameterBase
  {
  public:

    typedef AMC_PARAMETERIZATION_REAL real;
    typedef vector<int> vector1i;
    typedef vector<vector1i> vector2i;
    typedef vector<real> vector1r;
    typedef vector<vector1r> vector2r;

  protected:

    /*!< Parameterization name.*/
    string name_;

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterBase(const string name);

    /*!< Destructor.*/
    virtual ~ClassParameterizationDiameterBase();

    /*!< Get methods.*/
    string GetName() const;

    /*!< Clone.*/
    virtual ClassParameterizationDiameterBase* clone() const = 0;

    /*!< Copy.*/
    virtual void Copy(const ClassParameterizationDiameterBase &param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    virtual void ComputeDiameter(const int section_min,
                                 const int section_max,
                                 const vector1i &parameterization_section,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const = 0;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_BASE_HXX
#endif
