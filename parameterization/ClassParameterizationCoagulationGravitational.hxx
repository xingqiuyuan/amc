// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_GRAVITATIONAL_HXX


#define AMC_COAGULATION_GRAVITATIONAL_GRAVITY_CONSTANT 9.81


namespace AMC
{
  template<class C>
  class ClassParameterizationCoagulationGravitational : public ClassParameterizationCoagulationBase
  {
  public:

    typedef C collision_efficiency_parameterization;

  private:

    /*!< Collision efficiency parameterization.*/
    collision_efficiency_parameterization* collision_efficiency_;

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationGravitational();
    ClassParameterizationCoagulationGravitational(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationCoagulationGravitational();

    /*!< Clone.*/
    ClassParameterizationCoagulationGravitational<C>* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationCoagulationBase *param);

    /*!< Get collision efficiency parameterization.*/
    C* GetCollisionEfficiencyParameterizationPtr();

    /*!< Compute gravitational kernel.*/
    real Kernel(const real &diameter1, const real &diameter2,
                const real &mass1, const real &mass2,
                const real &temperature,
                const real &air_free_mean_path,
                const real &air_density,
                const real &air_dynamic_viscosity,
                const real &water_dynamic_viscosity) const;

    /*!< Compute kernel for tabulation.*/
    real KernelTable(const vector1r &variable) const;

#ifndef SWIG
    /*!< Compute kernel for AMC.*/
    void ComputeKernel(const vector2i &couple,
                       Array<real, 2> &kernel) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_GRAVITATIONAL_HXX
#endif
