// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_FIXED_CXX

#include "ClassParameterizationDiameterDensityFixed.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationDiameterDensityFixed::ClassParameterizationDiameterDensityFixed(Ops::Ops &ops, const string name)
    : ClassParameterizationDiameterBase(name), density_fixed_(AMC_PARAMETERIZATION_DIAMETER_DENSITY_DEFAULT)
  {
    // Density fixed, default is to use that of size discretization.
    if (ops.Exists("density_fixed"))
        density_fixed_ = ops.Get<real>("density_fixed") * AMC_PARAMETERIZATION_DIAMETER_CONVERT_FROM_GCM3_TO_MICROGMICROM3;
    else
      density_fixed_ = ClassDiscretizationSize::density_fixed_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Fixed Density diameter parameterization." << endl; 
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "density_fixed = " << density_fixed_ << " µg.µm^{-3}" << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationDiameterDensityFixed::~ClassParameterizationDiameterDensityFixed()
  {
    return;
  }


  // Get methods.
  real ClassParameterizationDiameterDensityFixed::GetDensity() const
  {
    return density_fixed_;
  }


  // Clone.
  ClassParameterizationDiameterDensityFixed* ClassParameterizationDiameterDensityFixed::clone() const
  {
    return new ClassParameterizationDiameterDensityFixed(*this);
  }


  // Copy.
  void ClassParameterizationDiameterDensityFixed::Copy(const ClassParameterizationDiameterBase *param)
  {
    density_fixed_ = reinterpret_cast<const ClassParameterizationDiameterDensityFixed*>(param)->density_fixed_;
    ClassParameterizationDiameterBase::Copy(*param);
  }


  // Compute diameter for AMC.
  void ClassParameterizationDiameterDensityFixed::ComputeDiameter(const int section_min,
                                                                  const int section_max,
                                                                  const vector1i &parameterization_section,
                                                                  const real *concentration_aer_number,
                                                                  const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            real mass_total = real(0);

            int k = j * ClassSpecies::Nspecies_;
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              mass_total += concentration_aer_mass[k++];

            // Add the liquid water content, if any.
            mass_total += ClassAerosolData::liquid_water_content_[j];

            if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
              {
                ClassAerosolData::mass_[j] = mass_total / concentration_aer_number[j];
                ClassAerosolData::diameter_[j] = pow(AMC_INV_PI6 * ClassAerosolData::mass_[j]
                                                     / density_fixed_, AMC_FRAC3);
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_FIXED_CXX
#endif
