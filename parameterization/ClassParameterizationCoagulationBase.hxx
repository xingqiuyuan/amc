// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BASE_HXX

#define AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3  1.e-6  // Convert factor from g.cm^{-3} to µg.µm^{-3}
#define AMC_COAGULATION_PI                                  3.141592653589793  // Pi
#define AMC_COAGULATION_PI6                                 0.5235987755982988  // Pi / 6
#define AMC_COAGULATION_PARTICLE_DENSITY_DEFAULT            1.0  // Default particle density in g.cm^{-3}


namespace AMC
{
  class ClassParameterizationCoagulationBase
  {
  public:

    typedef AMC_PARAMETERIZATION_REAL real;
    typedef vector<int> vector1i;
    typedef vector<vector1i> vector2i;
    typedef vector<real> vector1r;
    typedef vector<vector1r> vector2r;

  protected:

    /*!< Parameterization name.*/
    string name_;

    /*!< Number of variables needed.*/
    int Nvariable_;

    /*!< Particle fixed density.*/
    real density_fixed_;

    /*!< Variable name.*/
    vector<string> variable_name_;

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationBase(const string name, const int Nvariable);

    /*!< Destructor.*/
    virtual ~ClassParameterizationCoagulationBase();

    /*!< Get methods.*/
    string GetName() const;
    int GetNvariable() const;
    void GetVariableName(vector<string> &variable_name) const;
    real GetDensityFixed() const;

    /*!< Clone.*/
    virtual ClassParameterizationCoagulationBase* clone() const = 0;

    /*!< Copy.*/
    virtual void Copy(const ClassParameterizationCoagulationBase &param);

    /*!< Compute kernel for tabulation.*/
    virtual real KernelTable(const vector1r &variable) const = 0;

#ifndef SWIG
    /*!< Compute kernel for AMC.*/
    virtual void ComputeKernel(const vector2i &couple,
                               Array<real, 2> &kernel) const = 0;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BASE_HXX
#endif
