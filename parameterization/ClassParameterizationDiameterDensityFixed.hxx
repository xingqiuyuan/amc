// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_FIXED_HXX


namespace AMC
{
  class ClassParameterizationDiameterDensityFixed : public ClassParameterizationDiameterBase
  {
  protected:

    /*!< Fixed density.*/
    real density_fixed_;

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterDensityFixed(Ops::Ops &ops, const string name = "DensityFixed");

    /*!< Destructor.*/
    ~ClassParameterizationDiameterDensityFixed();

    /*!< Get methods.*/
    real GetDensity() const;

    /*!< Clone.*/
    virtual ClassParameterizationDiameterDensityFixed* clone() const;

    /*!< Copy.*/
    virtual void Copy(const ClassParameterizationDiameterBase *param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    virtual void ComputeDiameter(const int section_min,
                                 const int section_max,
                                 const vector1i &parameterization_section,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_FIXED_HXX
#endif
