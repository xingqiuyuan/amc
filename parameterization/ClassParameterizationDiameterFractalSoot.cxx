// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FRACTAL_SOOT_CXX

#include "ClassParameterizationDiameterFractalSoot.hxx"


namespace AMC
{
  ClassParameterizationDiameterFractalSoot::ClassParameterizationDiameterFractalSoot(Ops::Ops &ops)
    : ClassParameterizationDiameterDensityFixed(ops, "FractalSoot"),
      spherule_diameter_(AMC_DIAMETER_FRACTAL_SOOT_SPHERULE_DIAMETER_DEFAULT),
      spherule_single_mass_(real(0)), soot_index_(0), fractal_prefactor_inverse_(real(0)),
      surface_sticking_ratio_(AMC_DIAMETER_FRACTAL_SOOT_SURFACE_STICKING_RATIO),
      fractal_dimension_(AMC_DIAMETER_FRACTAL_SOOT_FRACTAL_DIMENSION_DEFAULT),
      fractal_prefactor_(AMC_DIAMETER_FRACTAL_SOOT_FRACTAL_PREFACTOR_DEFAULT),
      sphere_packing_density_max_(AMC_DIAMETER_FRACTAL_SOOT_SPHERE_DENSITY_PACKING_MAXIMUM),
      spherule_surface_(real(0)), spherule_volume_(real(0)),
      fraction_soot_threshold_(AMC_DIAMETER_FRACTAL_SOOT_FRACTION_THRESHOLD),
      sphere_packing_density_max_inverse_(real(0))
  {
    soot_index_ = ops.Get<int>("soot_index");
    spherule_diameter_ = ops.Get<real>("spherule_diameter", "", spherule_diameter_);
    surface_sticking_ratio_ = ops.Get<real>("surface_sticking_ratio", "", surface_sticking_ratio_);
    fractal_dimension_ = ops.Get<real>("fractal.dimension", "", fractal_dimension_);
    fractal_prefactor_ = ops.Get<real>("fractal.prefactor", "", fractal_prefactor_);
    sphere_packing_density_max_ = ops.Get<real>("sphere_packing_density_max", "", sphere_packing_density_max_);
    fraction_soot_threshold_ = ops.Get<real>("fraction_soot_threshold", "", fraction_soot_threshold_);

    if (sphere_packing_density_max_ <= real(0) || sphere_packing_density_max_ > real(1))
      throw AMC::Error("Wrong sphere packing density maximum value, should be between ]0, 1]");

    spherule_surface_ = AMC_DIAMETER_FRACTAL_SOOT_PI
      * spherule_diameter_ * spherule_diameter_ * (real(1) - surface_sticking_ratio_);

    spherule_volume_ = AMC_DIAMETER_FRACTAL_SOOT_PI6
      * spherule_diameter_ * spherule_diameter_ * spherule_diameter_;

    spherule_single_mass_ = AMC_DIAMETER_FRACTAL_SOOT_DENSITY
      * AMC_DIAMETER_FRACTAL_SOOT_CONVERT_FROM_GCM3_TO_MICROGMICROM3
      * spherule_volume_;

    fractal_prefactor_inverse_ = real(1) / fractal_prefactor_;

    sphere_packing_density_max_inverse_ = real(1) / sphere_packing_density_max_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Instantiate diffusion limited condensation "
                         << "parameterization for SOOT particles." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug() << Reset() << "soot_index = " << soot_index_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug() << Reset() << "spherule_diameter (µm) = " << spherule_diameter_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "fractal_dimension = " << fractal_dimension_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "fractal_prefactor = " << fractal_prefactor_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "fraction_soot_threshold = " << fraction_soot_threshold_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "spherule_single_mass (µg) = " << spherule_single_mass_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "spherule_surface (µm2) = " << spherule_surface_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "spherule_volume (µm3) = " << spherule_volume_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationDiameterFractalSoot::~ClassParameterizationDiameterFractalSoot()
  {
    return;
  }


  // Clone.
  ClassParameterizationDiameterFractalSoot* ClassParameterizationDiameterFractalSoot::clone() const
  {
    return new ClassParameterizationDiameterFractalSoot(*this);
  }


  // Copy.
  void ClassParameterizationDiameterFractalSoot::Copy(const ClassParameterizationDiameterBase *param)
  {
    soot_index_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)->soot_index_;
    spherule_diameter_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)->spherule_diameter_;
    fractal_dimension_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)->fractal_dimension_;
    fractal_prefactor_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)->fractal_prefactor_;
    fraction_soot_threshold_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)
      ->fraction_soot_threshold_;
    sphere_packing_density_max_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)
      ->sphere_packing_density_max_;
    surface_sticking_ratio_ = reinterpret_cast<const ClassParameterizationDiameterFractalSoot*>(param)
      ->surface_sticking_ratio_;

    spherule_surface_ = AMC_DIAMETER_FRACTAL_SOOT_PI
      * spherule_diameter_ * spherule_diameter_ * (real(1) - surface_sticking_ratio_);

    spherule_volume_ = AMC_DIAMETER_FRACTAL_SOOT_PI6
      * spherule_diameter_ * spherule_diameter_ * spherule_diameter_;

    spherule_single_mass_ = AMC_DIAMETER_FRACTAL_SOOT_DENSITY
      * AMC_DIAMETER_FRACTAL_SOOT_CONVERT_FROM_GCM3_TO_MICROGMICROM3
      * spherule_volume_;

    fractal_prefactor_inverse_ = real(1) / fractal_prefactor_;

    sphere_packing_density_max_inverse_ = real(1) / sphere_packing_density_max_;

    ClassParameterizationDiameterDensityFixed::Copy(param);
  }


  // Compute diameter for AMC.
  void ClassParameterizationDiameterFractalSoot::ComputeDiameter(const int section_min,
                                                                 const int section_max,
                                                                 const vector1i &parameterization_section,
                                                                 const real *concentration_aer_number,
                                                                 const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        int k =  j * ClassSpecies::Nspecies_;

        const real &concentration_aer_soot = concentration_aer_mass[k + soot_index_];

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Number of spherules in particles.
            real spherule_number = concentration_aer_soot / (spherule_single_mass_ * concentration_aer_number[j]);

            // Total mass.
            real dry_mass(real(0));
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              dry_mass += concentration_aer_mass[k++];

            // Add water content, if any.
            real mass_total = dry_mass + ClassAerosolData::liquid_water_content_[j];

            // If less than one spherule, let diameter unchanged.
            if (spherule_number > real(1))
              if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
                {
                  // Soot fraction.
                  real fraction_soot = concentration_aer_soot / dry_mass;

                  // Single mass.
                  ClassAerosolData::mass_[j] = mass_total / concentration_aer_number[j];

                  if (fraction_soot < fraction_soot_threshold_)
                    {
                      // Volume diameter. Thanks to sphere packing theory.
                      real volume = spherule_number * spherule_volume_ * sphere_packing_density_max_inverse_;

                      // Remove soot from dry mass in µg.
                      real mass_wo_soot = (mass_total - concentration_aer_soot) / concentration_aer_number[j];

                      // Add volume of other species and water, use fixed density.
                      volume += mass_wo_soot / this->density_fixed_ * sphere_packing_density_max_;

                      ClassAerosolData::diameter_[j] = pow(AMC_INV_PI6 * volume, AMC_FRAC3);

                      // Particle surface.
                      ClassAerosolData::surface_[j] = AMC_PI * ClassAerosolData::diameter_[j]
                        * ClassAerosolData::diameter_[j];
                    }
                  else
                    {
                      // Gyration diameter. Thanks to fractal representation.
                      ClassAerosolData::diameter_[j] = pow(spherule_number * fractal_prefactor_inverse_, fractal_dimension_)
                        * spherule_diameter_;

                      // Particle surface.
                      ClassAerosolData::surface_[j] = spherule_number * spherule_surface_;
                    }
                }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FRACTAL_SOOT_CXX
#endif
