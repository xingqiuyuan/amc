// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_MERIKANTO_CXX

#include "ClassParameterizationNucleationMerikanto.hxx"

namespace AMC
{
  // Threshold parameters due to validity range of formula.
  inline real ClassParameterizationNucleationMerikanto::threshold_relative_humidity(const real &relative_humidity) const
  {
    if (relative_humidity > AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MAX)
      return AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MAX;
    else if (relative_humidity < AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MIN)
      return AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MIN;
    else
      return relative_humidity;
  }


  inline real ClassParameterizationNucleationMerikanto::threshold_temperature(const real &temperature) const
  {
    if (temperature > AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MAX)
      return AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MAX;
    else if (temperature < AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MIN)
      return AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MIN;
    else
      return temperature;
  }


  inline real ClassParameterizationNucleationMerikanto::threshold_concentration_ammonium_ppt(const real &concentration_ammonium_ppt) const
  {
    if (concentration_ammonium_ppt > AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MAX)
      return AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MAX;
    else if (concentration_ammonium_ppt < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN)
      return AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN;
    else
      return concentration_ammonium_ppt;
  }


  inline real ClassParameterizationNucleationMerikanto::
  threshold_concentration_sulfate_molec_cm3(const real &concentration_sulfate_molec_cm3) const
  {
    if (concentration_sulfate_molec_cm3 > AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MAX)
      return AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MAX;
    else if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN)
      return AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN;
    return concentration_sulfate_molec_cm3;
  }


  // Logarithm of nucleation rate.
  real ClassParameterizationNucleationMerikanto::nucleation_rate_cm3s_log(const real &temperature,
                                                                          const real &relative_humidity,
                                                                          const real &relative_humidity_log,
                                                                          const real &concentration_sulfate_molec_cm3_log,
                                                                          const real &concentration_ammonium_ppt,
                                                                          const real &concentration_ammonium_ppt_log) const
  {
    real f1 = MERIKANTO_POLYNOMIAL_FIT(-358.2337705052991 , 4.8630382337426985   , -0.02175548069741675  , 0.00003212869941055865);
    real f2 = MERIKANTO_POLYNOMIAL_FIT(-980.923146020468  , 10.054155220444462   , -0.03306644502023841  , 0.000034274041225891804);
    real f3 = MERIKANTO_POLYNOMIAL_FIT(1200.472096232311  , -17.37107890065621   , 0.08170681335921742   , -0.00012534476159729881);
    real f4 = MERIKANTO_POLYNOMIAL_FIT(-14.833042158178936, 0.2932631303555295   , -0.0016497524241142845, 2.844074805239367e-6);
    real f5 = MERIKANTO_POLYNOMIAL_FIT(-4.39129415725234e6, 56383.93843154586    , -239.835990963361     , 0.33765136625580167);
    real f6 = MERIKANTO_POLYNOMIAL_FIT(4.905527742256349  , -0.05463019231872484 , 0.00020258394697064567, -2.502406532869512e-7);
    real f7 = MERIKANTO_POLYNOMIAL_FIT(-231375.56676032578, 2919.2852552424706   , -12.286497122264588   , 0.017249301826661612);
    real f8 = MERIKANTO_POLYNOMIAL_FIT(75061.15281456841  , -931.8802278173565   , 3.863266220840964     , -0.005349472062284983);
    real f9 = MERIKANTO_POLYNOMIAL_FIT(-3180.5610833308   , 39.08268568672095    , -0.16048521066690752  , 0.00022031380023793877);
    real f10 = MERIKANTO_POLYNOMIAL_FIT(-100.21645273730675, 0.977886555834732    , -0.0030511783284506377, 2.967320346100855e-6);
    real f11 = MERIKANTO_POLYNOMIAL_FIT(5599.912337254629  , -70.70896612937771   , 0.2978801613269466    , -0.00041866525019504);
    real f12 = MERIKANTO_POLYNOMIAL_FIT(2.360931724951942e6, -29752.130254319443  , 125.04965118142027    , -0.1752996881934318);
    real f13 = MERIKANTO_POLYNOMIAL_FIT(16597.75554295064  , -175.2365504237746   , 0.6033215603167458    , -0.0006731787599587544);
    real f14 = MERIKANTO_POLYNOMIAL_FIT(-89.38961120336789 , 1.153344219304926    , -0.004954549700267233 , 7.096309866238719e-6);
    real f15 = MERIKANTO_POLYNOMIAL_FIT(-629.7882041830943 , 7.772806552631709    , -0.031974053936299256 , 0.00004383764128775082);
    real f16 = MERIKANTO_POLYNOMIAL_FIT(-732006.8180571689 , 9100.06398573816     , -37.771091915932004   , 0.05235455395566905);
    real f17 = MERIKANTO_POLYNOMIAL_FIT(40751.075322248245 , -501.66977622013934  , 2.063469732254135     , -0.002836873785758324);
    real f18 = MERIKANTO_POLYNOMIAL_FIT(-1911.0303773001353, 23.6903969622286     , -0.09807872005428583  , 0.00013564560238552576);
    real f19 = MERIKANTO_POLYNOMIAL_FIT(2.792313345723013  , -0.03422552111802899 , 0.00014019195277521142, -1.9201227328396297e-7);
    real f20 = MERIKANTO_POLYNOMIAL_FIT(3.1712136610383244 , -0.037822330602328806, 0.0001500555743561457 , -1.9828365865570703e-7);

    return real(-12.861848898625231) + f1 * relative_humidity
      + f2 * relative_humidity_log + f6 * concentration_ammonium_ppt
      + concentration_sulfate_molec_cm3_log * (f3 + concentration_sulfate_molec_cm3_log * f4
                                               + concentration_ammonium_ppt_log
                                               * (f11 + concentration_ammonium_ppt_log
                                                  * (f18 + f19 * concentration_sulfate_molec_cm3_log
                                                     * concentration_ammonium_ppt_log)))
      + concentration_ammonium_ppt_log * (f7 + relative_humidity * f10 + relative_humidity_log * f14
                                          + concentration_ammonium_ppt_log
                                          * (f8 + concentration_ammonium_ppt_log * (f9 + relative_humidity_log * f20)))
      + ((f13 * relative_humidity_log
          + f15 * relative_humidity / (concentration_ammonium_ppt * concentration_ammonium_ppt * concentration_ammonium_ppt)
          + f5 / concentration_sulfate_molec_cm3_log)
         + concentration_ammonium_ppt_log
         * (f12 + concentration_ammonium_ppt_log
            * (f16 + concentration_ammonium_ppt_log * f17))) / concentration_sulfate_molec_cm3_log;
  }


  // Critical cluster radius in nanometer.
  real ClassParameterizationNucleationMerikanto::critical_cluster_radius_nm(const real &temperature,
                                                                            const real &nucleation_rate_log,
                                                                            const real &concentration_sulfate_molec_cm3_log,
                                                                            const real &concentration_ammonium_ppt_log) const
  {
    return real(0.328886) + temperature * (real(-0.00337417) + temperature * real(0.0000183474))
      + concentration_sulfate_molec_cm3_log * (real(0.00254198) + real(-0.0000949811) * temperature
                                               + real(0.000744627) * concentration_sulfate_molec_cm3_log)
      + concentration_ammonium_ppt_log * (real(0.0243034) + real(0.0000158932) * temperature
                                          + real(-0.00203460) * concentration_sulfate_molec_cm3_log
                                          + concentration_ammonium_ppt_log
                                          * (real(-0.000559304) + temperature * real(-4.88951e-7)
                                             + concentration_ammonium_ppt_log * real(0.000138470)))
      + nucleation_rate_log * (real(4.14108e-6) + real(-0.0000268131) * temperature
                               * concentration_ammonium_ppt_log * (real(0.00128791) + real(-3.80352e-6) * temperature)
                               + real(-0.0000187902) * nucleation_rate_log);
  }


  // Number molecule total.
  real ClassParameterizationNucleationMerikanto::number_molecule_total(const real &temperature,
                                                                       const real &nucleation_rate_log,
                                                                       const real &concentration_sulfate_molec_cm3_log,
                                                                       const real &concentration_ammonium_ppt_log) const
  {
    return real(57.4009) + temperature * (real(-0.299634) + real(0.000739548) * temperature)
      + concentration_sulfate_molec_cm3_log
      * (real(-5.09060) + real(0.0110166) * temperature
         + concentration_sulfate_molec_cm3_log * real(0.0675003))
      + concentration_ammonium_ppt_log
      * (real(-0.810283) + real(0.0159051) * temperature
         + real(-0.204417) * concentration_sulfate_molec_cm3_log + concentration_ammonium_ppt_log
         * (real(0.0891816) + real(-0.000496903) * temperature + real(0.00570439) * concentration_ammonium_ppt_log))
      + nucleation_rate_log
      * (real(3.40987) + real(-0.0149170) * temperature + concentration_ammonium_ppt_log
         * (real(0.0845909) + real(-0.000148006) * temperature + real(0.00503805) * nucleation_rate_log));
  }


  // Number molecule sulfate.
  real ClassParameterizationNucleationMerikanto::number_molecule_ammonium(const real &temperature,
                                                                          const real &nucleation_rate_log,
                                                                          const real &concentration_sulfate_molec_cm3_log,
                                                                          const real &concentration_ammonium_ppt_log) const
  {
    return real(71.2007) + temperature * (real(-0.840960) + temperature * real(0.00248030))
      + concentration_sulfate_molec_cm3_log * (real(2.77986) + temperature * real(-0.0147502)
                                               + real(0.0122645) * concentration_sulfate_molec_cm3_log)
      + concentration_ammonium_ppt_log
      * (real(-2.00993) + real(0.00868912) * temperature + real(-0.00914118) * concentration_sulfate_molec_cm3_log
         + concentration_ammonium_ppt_log * (real(0.137412) + real(-0.000625323) * temperature
                                             + real(0.0000937733) * concentration_ammonium_ppt_log))
      + nucleation_rate_log * (real(0.520297) + real(-0.00241987) * temperature
                               + concentration_ammonium_ppt_log * (real(0.0791639) + real(-0.000302159) * temperature
                                                                   + real(0.00469770) * concentration_ammonium_ppt_log));
  }


  // Number molecule sulfate.
  real ClassParameterizationNucleationMerikanto::number_molecule_sulfate(const real &temperature,
                                                                         const real &nucleation_rate_log,
                                                                         const real &concentration_sulfate_molec_cm3_log,
                                                                         const real &concentration_ammonium_ppt_log) const
  {
    return real(-4.71542) + temperature * (real(0.134364) + real(-0.000471847) * temperature)
      + concentration_sulfate_molec_cm3_log * (real(-2.56401) + real(0.0113533) * temperature
                                               + real(0.00108019) * concentration_sulfate_molec_cm3_log)
      + concentration_ammonium_ppt_log * (real(0.517137) + real(-0.00278825) * temperature
                                          + concentration_ammonium_ppt_log
                                          * (real(0.806697) + real(-0.00318491) * temperature
                                             + concentration_ammonium_ppt_log
                                             * (real(-0.0995118) + real(0.000400728) * temperature)))
      + nucleation_rate_log * (real(1.32765) + real(-0.00616765) * temperature
                               + concentration_ammonium_ppt_log * (real(-0.110614) + real(0.000436758) * temperature)
                               + real(0.000916366) * nucleation_rate_log);
  }


  // Number molecule sulfate.
  real ClassParameterizationNucleationMerikanto::onset_temperature(const real &temperature,
                                                                   const real &relative_humidity,
                                                                   const real &concentration_sulfate_molec_cm3_log,
                                                                   const real &concentration_ammonium_ppt_log) const
  {
    return real(143.600) + real(1.01789) * relative_humidity
      + concentration_sulfate_molec_cm3_log * (real(10.1964) + real(-0.184988) * concentration_sulfate_molec_cm3_log)
      + concentration_ammonium_ppt_log * (real(-17.1618) + real(109.9247) / concentration_sulfate_molec_cm3_log
                                          + real(0.773412) * concentration_sulfate_molec_cm3_log
                                          + real(-0.155764) * concentration_ammonium_ppt_log);
  }


  // Constructors.
  ClassParameterizationNucleationMerikanto::ClassParameterizationNucleationMerikanto()
    : ClassParameterizationNucleationBase("Merikanto")
  {
    this->set_index_amc("NH3");

    // Add water.
    this->species_index_amc_.push_back(-1);
    this->gas_index_amc_.push_back(-1);

    return;
  }


  // Destructor.
  ClassParameterizationNucleationMerikanto::~ClassParameterizationNucleationMerikanto()
  {
    return;
  }


  // Clone.
  ClassParameterizationNucleationMerikanto* ClassParameterizationNucleationMerikanto::clone() const
  {
    return new ClassParameterizationNucleationMerikanto(*this);
  }


  // Convert concentrations.
  real ClassParameterizationNucleationMerikanto::ConvertAmmoniumFromUGM3toPPT(const real &concentration_ammonium_ugm3,
                                                                              const real temperature,
                                                                              const real pressure)
  {
    return concentration_ammonium_ugm3
      * AMC_NUCLEATION_MERIKANTO_CONVERT_AMMONIUM_FROM_UGM3_TO_PPT
      * temperature / pressure;
  }


  // Compute onset temperature.
  real ClassParameterizationNucleationMerikanto::ComputeTemperatureOnset(const real &temperature,
                                                                         const real &relative_humidity,
                                                                         const real &concentration_sulfate_molec_cm3,
                                                                         const real &concentration_ammonium_ppt) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    // Convert from ug/m3 to ppt.
    real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
    real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    return onset_temperature(temperature_valid,
                             relative_humidity_valid,
                             concentration_sulfate_molec_cm3_log,
                             concentration_ammonium_ppt_log);
  }


  // Compute mole and mass fractions.
  void ClassParameterizationNucleationMerikanto::ComputeMoleFraction(const real &temperature,
                                                                     const real &relative_humidity,
                                                                     const real &concentration_sulfate_molec_cm3,
                                                                     const real &concentration_ammonium_ppt,
                                                                     vector<real> &mole_fraction) const
  {
    // Compute number of molecules : sulfate, ammonium and total.
    vector<real> number_molecule;

    ComputeNumberMolecule(temperature,
                          relative_humidity,
                          concentration_sulfate_molec_cm3,
                          concentration_ammonium_ppt,
                          number_molecule);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "number_molecule = " << number_molecule << endl;
#endif

    // Three components in critical cluster : sulfate, ammonium and water.
    mole_fraction.assign(3, real(0));

    if (number_molecule[2] > real(0))
      {
        mole_fraction[0] = number_molecule[0] / number_molecule[2];
        mole_fraction[1] = number_molecule[1] / number_molecule[2];

        // This may be negative for water.
        mole_fraction[2] = real(1) - mole_fraction[0] - mole_fraction[1];
        mole_fraction[2] = mole_fraction[2] > real(0) ? mole_fraction[2] : real(0);
      }
  }


  void ClassParameterizationNucleationMerikanto::ComputeMassFraction(const real &temperature,
                                                                     const real &relative_humidity,
                                                                     const real &concentration_sulfate_molec_cm3,
                                                                     const real &concentration_ammonium_ppt,
                                                                     vector<real> &mass_fraction) const
  {
    // Compute mole fractions at first.
    vector<real> mole_fraction;

    ComputeMoleFraction(temperature,
                        relative_humidity,
                        concentration_sulfate_molec_cm3,
                        concentration_ammonium_ppt,
                        mole_fraction);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "mole_fraction = " << mole_fraction << endl;
#endif

    real mass_total = AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_SULFATE * mole_fraction[0]
      + AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_AMMONIUM * mole_fraction[1]
      + AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_WATER * mole_fraction[2];

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "mass_total = " << mass_total << endl;
#endif

    // Three components in critical cluster : sulfate, ammonium and water.
    mass_fraction.assign(3, real(0));

    if (mass_total > real(0))
      {
        mass_fraction[0] = AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_SULFATE * mole_fraction[0] / mass_total;
        mass_fraction[1] = AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_AMMONIUM * mole_fraction[1] / mass_total;
        mass_fraction[2] = AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_WATER * mole_fraction[2] / mass_total;
      }
  }


  // Number of molecules in cluster.
  void ClassParameterizationNucleationMerikanto::ComputeNumberMolecule(const real &temperature,
                                                                       const real &relative_humidity,
                                                                       const real &concentration_sulfate_molec_cm3,
                                                                       const real &concentration_ammonium_ppt,
                                                                       vector<real> &number_molecule) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
    real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    // Two components in critical cluster : sulfate and ammonium, plus total number.
    number_molecule.resize(3);

    // If below onset value, nucleation rate is null.
    if (temperature_valid < onset_temperature(temperature_valid,
                                              relative_humidity_valid,
                                              concentration_sulfate_molec_cm3_log,
                                              concentration_ammonium_ppt_log))
      {
        // Flux number in #.cm^{-3}.s^{-1}.
        real nucleation_rate_log = nucleation_rate_cm3s_log(temperature_valid,
                                                            relative_humidity_valid,
                                                            relative_humidity_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_valid,
                                                            concentration_ammonium_ppt_log);

        nucleation_rate_log = nucleation_rate_log > AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN \
          ? nucleation_rate_log : AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN;

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "nucleation_rate_log = " << nucleation_rate_log << endl;
#endif

        number_molecule[0] = number_molecule_sulfate(temperature_valid,
                                                     nucleation_rate_log,
                                                     concentration_sulfate_molec_cm3_log,
                                                     concentration_ammonium_ppt_log);

        number_molecule[1] = number_molecule_ammonium(temperature_valid,
                                                      nucleation_rate_log,
                                                      concentration_sulfate_molec_cm3_log,
                                                      concentration_ammonium_ppt_log);

        number_molecule[2] = number_molecule_total(temperature_valid,
                                                   nucleation_rate_log,
                                                   concentration_sulfate_molec_cm3_log,
                                                   concentration_ammonium_ppt_log);
      }
  }


  // Critical cluster radius.
  real ClassParameterizationNucleationMerikanto::ComputeCriticalClusterRadius(const real &temperature,
                                                                              const real &relative_humidity,
                                                                              const real &concentration_sulfate_molec_cm3,
                                                                              const real &concentration_ammonium_ppt) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
    real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    // Nucleation rate in #.cm^{-3}.s^{-1}.
    if (temperature_valid < onset_temperature(temperature_valid,
                                              relative_humidity_valid,
                                              concentration_sulfate_molec_cm3_log,
                                              concentration_ammonium_ppt_log))
      {
        real nucleation_rate_log = nucleation_rate_cm3s_log(temperature_valid,
                                                            relative_humidity_valid,
                                                            relative_humidity_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_valid,
                                                            concentration_ammonium_ppt_log);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "nucleation_rate_log = " << nucleation_rate_log << endl;
#endif

        return critical_cluster_radius_nm(temperature,
                                          nucleation_rate_log,
                                          concentration_sulfate_molec_cm3_log,
                                          concentration_ammonium_ppt_log);
      }

    return real(0);
  }


  real ClassParameterizationNucleationMerikanto::ComputeKernel(const real &temperature,
                                                               const real &relative_humidity,
                                                               const real &concentration_sulfate_molec_cm3,
                                                               const real &concentration_ammonium_ppt) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
    real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    // Nucleation rate in #.cm^{-3}.s^{-1}.
#ifdef AMC_WITH_DEBUG_MERIKANTO
    real temperature_onset = onset_temperature(temperature_valid,
                                               relative_humidity_valid,
                                               concentration_sulfate_molec_cm3_log,
                                               concentration_ammonium_ppt_log);
    CBUG << "temperature_onset = " << temperature_onset << endl;

    if (temperature_valid < temperature_onset)
#else
    if (temperature_valid < onset_temperature(temperature_valid,
                                              relative_humidity_valid,
                                              concentration_sulfate_molec_cm3_log,
                                              concentration_ammonium_ppt_log))

#endif
      {
        real nucleation_rate_log = nucleation_rate_cm3s_log(temperature_valid,
                                                            relative_humidity_valid,
                                                            relative_humidity_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_valid,
                                                            concentration_ammonium_ppt_log);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "nucleation_rate_log = " << nucleation_rate_log << endl;
#endif

        return exp(nucleation_rate_log);
      }

    return real(0);
  }


  void ClassParameterizationNucleationMerikanto::ComputeKernel(const int &section_index,
                                                               const real *concentration_gas,
                                                               vector1r &rate_aer_number,
                                                               vector1r &rate_aer_mass) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(ClassMeteorologicalData::relative_humidity_);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(ClassMeteorologicalData::temperature_);

    real concentration_sulfate_molec_cm3 = concentration_gas[this->gas_index_amc_[AMC_NUCLEATION_SULFATE_INDEX]]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;
    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    // Convert from ug/m3 to ppt.
    real concentration_ammonium_ppt = concentration_gas[this->gas_index_amc_[AMC_NUCLEATION_MERIKANTO_AMMONIUM_INDEX]]
      * AMC_NUCLEATION_MERIKANTO_CONVERT_AMMONIUM_FROM_UGM3_TO_PPT
      * temperature_valid / ClassMeteorologicalData::pressure_;
    real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
    real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    // If gas concentrations are below the minimal value, we consider there is no nucleation
    // instead of clipping the value, which may lead to some overestimation.
    if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN) return;
    if (concentration_ammonium_ppt < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN) return;

    // If below onset value, nucleation rate is null.
#ifdef AMC_WITH_DEBUG_MERIKANTO
    real temperature_onset = onset_temperature(temperature_valid,
                                               relative_humidity_valid,
                                               concentration_sulfate_molec_cm3_log,
                                               concentration_ammonium_ppt_log);
    CBUG << "temperature_onset = " << temperature_onset << endl;

    if (temperature_valid < temperature_onset)
#else
    if (temperature_valid < onset_temperature(temperature_valid,
                                              relative_humidity_valid,
                                              concentration_sulfate_molec_cm3_log,
                                              concentration_ammonium_ppt_log))

#endif
      {
        // Flux number in #.cm^{-3}.s^{-1}.
        real nucleation_rate_log = nucleation_rate_cm3s_log(temperature_valid,
                                                            relative_humidity_valid,
                                                            relative_humidity_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_valid,
                                                            concentration_ammonium_ppt_log);

        nucleation_rate_log = nucleation_rate_log > AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN \
          ? nucleation_rate_log : AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN;

        real nucleation_rate_m3s = exp(nucleation_rate_log) * real(1e6);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "nucleation_rate_m3s = " << nucleation_rate_m3s << endl;
#endif

        // Flux mass.
        real number_molec_sulfate = number_molecule_sulfate(temperature_valid,
                                                            nucleation_rate_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_log);

        real number_molec_ammonium = number_molecule_ammonium(temperature_valid,
                                                              nucleation_rate_log,
                                                              concentration_sulfate_molec_cm3_log,
                                                              concentration_ammonium_ppt_log);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "number_molec_sulfate = " << number_molec_sulfate << endl;
        CBUG << "number_molec_ammonium = " << number_molec_ammonium << endl;
#endif

        // Update rates, do not forget we possibly add other things (coagulation, condensation).
        rate_aer_number[section_index] += nucleation_rate_m3s;

        // Mass flux : 1 µg.m^{-3} = 
        const int i = section_index * this->Nspecies_;
        rate_aer_mass[i + this->species_index_amc_[AMC_NUCLEATION_SULFATE_INDEX]] +=
          AMC_NUCLEATION_MERIKANTO_SULFATE_CONVERT_FROM_MOLEC_TO_UG * number_molec_sulfate * nucleation_rate_m3s;
        rate_aer_mass[i + this->species_index_amc_[AMC_NUCLEATION_MERIKANTO_AMMONIUM_INDEX]] +=
          AMC_NUCLEATION_MERIKANTO_AMMONIUM_CONVERT_FROM_MOLEC_TO_UG * number_molec_ammonium * nucleation_rate_m3s;
      }
  }


#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  bool ClassParameterizationNucleationMerikanto::ComputeKernel(const vector1r &concentration_gas,
                                                               real &diameter,
                                                               real &rate_aer_number,
                                                               vector1r &rate_aer_mass) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(NPF::ClassMeteoData::relative_humidity_);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(NPF::ClassMeteoData::temperature_);

    real concentration_sulfate_molec_cm3 = concentration_gas[AMC_NUCLEATION_SULFATE_INDEX]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;
    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

    // Convert from ug/m3 to ppt.
  real concentration_ammonium_ppt = concentration_gas[this->index_npf_[AMC_NUCLEATION_MERIKANTO_AMMONIUM_INDEX]]
    * AMC_NUCLEATION_MERIKANTO_CONVERT_AMMONIUM_FROM_UGM3_TO_PPT
    * temperature_valid / NPF::ClassMeteoData::pressure_;
  real concentration_ammonium_ppt_valid = threshold_concentration_ammonium_ppt(concentration_ammonium_ppt);
  real concentration_ammonium_ppt_log = log(concentration_ammonium_ppt_valid);

#ifdef AMC_WITH_DEBUG_MERIKANTO
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
    CBUG << "concentration_ammonium_ppt_valid = " << concentration_ammonium_ppt_valid << endl;
#endif

    // If gas concentrations are below the minimal value, we consider there is no nucleation
    // instead of clipping the value, which may lead to some overestimation.
    if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN) return false;
    if (concentration_ammonium_ppt < AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN) return false;

    // If below onset value, nucleation rate is null.
#ifdef AMC_WITH_DEBUG_MERIKANTO
    real temperature_onset = onset_temperature(temperature_valid,
                                               relative_humidity_valid,
                                               concentration_sulfate_molec_cm3_log,
                                               concentration_ammonium_ppt_log);
    CBUG << "temperature_onset = " << temperature_onset << endl;

    if (temperature_valid < temperature_onset)
#else
    if (temperature_valid < onset_temperature(temperature_valid,
                                              relative_humidity_valid,
                                              concentration_sulfate_molec_cm3_log,
                                              concentration_ammonium_ppt_log))

#endif
      {
        // Flux number in #.cm^{-3}.s^{-1}.
        real nucleation_rate_log = nucleation_rate_cm3s_log(temperature_valid,
                                                            relative_humidity_valid,
                                                            relative_humidity_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_valid,
                                                            concentration_ammonium_ppt_log);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "nucleation_rate_log = " << nucleation_rate_log << endl;
#endif

        nucleation_rate_log = nucleation_rate_log > AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN \
          ? nucleation_rate_log : AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN;

        // Nucleation number rate in #.m^{-3}.s^{-1}.
        rate_aer_number = exp(nucleation_rate_log) * real(1e6);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "rate_aer_number = " << rate_aer_number << endl;
#endif

        // Particle diameter in µm.
        diameter = real(2.e-3) * critical_cluster_radius_nm(NPF::ClassMeteoData::temperature_,
                                                            nucleation_rate_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_log);

        // Flux mass.
        real number_molec_sulfate = number_molecule_sulfate(temperature_valid,
                                                            nucleation_rate_log,
                                                            concentration_sulfate_molec_cm3_log,
                                                            concentration_ammonium_ppt_log);

        real number_molec_ammonium = number_molecule_ammonium(temperature_valid,
                                                              nucleation_rate_log,
                                                              concentration_sulfate_molec_cm3_log,
                                                              concentration_ammonium_ppt_log);

        real number_molec_total = number_molecule_total(temperature_valid,
                                                        nucleation_rate_log,
                                                        concentration_sulfate_molec_cm3_log,
                                                        concentration_ammonium_ppt_log);

        real  number_molec_water = number_molec_total - number_molec_sulfate - number_molec_ammonium;
        number_molec_water = number_molec_water > real(0) ? number_molec_water : real(0);

#ifdef AMC_WITH_DEBUG_MERIKANTO
        CBUG << "number_molec_sulfate = " << number_molec_sulfate << endl;
        CBUG << "number_molec_ammonium = " << number_molec_ammonium << endl;
        CBUG << "number_molec_water = " << number_molec_water << endl;
        CBUG << "number_molec_total = " << number_molec_total << endl;
#endif

        // Mass flux µg.cm^{-3}.s{-1}.
        rate_aer_mass[AMC_NUCLEATION_SULFATE_INDEX] =
          AMC_NUCLEATION_MERIKANTO_SULFATE_CONVERT_FROM_MOLEC_TO_UG * number_molec_sulfate * rate_aer_number;
        rate_aer_mass[this->index_npf_[AMC_NUCLEATION_MERIKANTO_AMMONIUM_INDEX]] =
          AMC_NUCLEATION_MERIKANTO_AMMONIUM_CONVERT_FROM_MOLEC_TO_UG * number_molec_ammonium * rate_aer_number;
        rate_aer_mass[this->index_npf_[ClassParameterizationNucleationBase::Nspecies_]] =
          AMC_NUCLEATION_MERIKANTO_WATER_CONVERT_FROM_MOLEC_TO_UG * number_molec_water * rate_aer_number;

        return true;
      }

    return false;
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_MERIKANTO_CXX
#endif
