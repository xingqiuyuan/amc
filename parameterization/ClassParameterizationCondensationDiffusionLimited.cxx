// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_CXX

#include "ClassParameterizationCondensationDiffusionLimited.hxx"

namespace AMC
{
  // Constructors.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ClassParameterizationCondensationDiffusionLimited(Ops::Ops &ops,
                                                                                                                   string name)
    : ClassParameterizationCondensationBase(name),
      knudsen_min_(AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MIN),
      knudsen_max_(AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MAX),
      correction_factor_(NULL), flux_correction_aqueous_(NULL), flux_correction_dry_(NULL)
  {
    knudsen_min_ = ops.Get<real>("knudsen_min", "", AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MIN);
    knudsen_max_ = ops.Get<real>("knudsen_max", "", AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MAX);

    correction_factor_ = new FC();
    this->name_ += "-" + correction_factor_->GetName();

    flux_correction_aqueous_ = new FW(ops);
    this->name_ += "-AqueousFluxCorrection" + flux_correction_aqueous_->GetName();

    flux_correction_dry_ = new FD(ops);
    this->name_ += "-DryFluxCorrection" + flux_correction_dry_->GetName();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Parameterization name = " << this->name_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset()
                         << "knudsen_min = " << knudsen_min_ << ", knudsen_max = " << knudsen_max_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset()
                         << "Use correction factor formula \"" << correction_factor_->GetName() << "\"." << endl;
#endif

    return;
  }


  // Destructor.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::~ClassParameterizationCondensationDiffusionLimited()
  {
    delete flux_correction_aqueous_;
    delete flux_correction_dry_;
    delete correction_factor_;

    return;
  }


  // Clone.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>* ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::clone() const
  {
    return new ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>(*this);
  }


  // Get methods.
  template<class FC, class FW, class FD>
  FC* ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::GetCorrectionFactor() const
  {
    return correction_factor_;
  }


  template<class FC, class FW, class FD>
  FW* ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::GetFluxCorrectionAqueous() const
  {
    return flux_correction_aqueous_;
  }


  template<class FC, class FW, class FD>
  FD* ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::GetFluxCorrectionDry() const
  {
    return flux_correction_dry_;
  }


  // Compute coefficient.
  template<class FC, class FW, class FD>
  real ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(const real &diameter,
                                                                                         const real &velocity,
                                                                                         const real &diffusivity,
                                                                                         const real &accomodation) const
  {
    real knudsen = 2.e6 * correction_factor_->FreeMeanPath(diffusivity, velocity) / diameter;

    // Compute coefficient according to regime : continous, free molecular or transition.
    if (knudsen < knudsen_min_)
      return AMC_CONDENSATION_DIFFUSION_LIMITED_2_PI_Em6 * diffusivity * diameter;
    else if (knudsen >= knudsen_max_)
      return AMC_CONDENSATION_DIFFUSION_LIMITED_PI_DIV4_Em12
        * velocity * accomodation * diameter * diameter;
    else
      return AMC_CONDENSATION_DIFFUSION_LIMITED_2_PI_Em6 * diffusivity * diameter
        * correction_factor_->CorrectionFactor(knudsen, accomodation);
  }


  // Compute coefficient.
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(const int section_min,
                                                                                         const int section_max,
                                                                                         const vector1i &parameterization_section,
                                                                                         const real *concentration_aer_number,
                                                                                         vector1r &condensation_coefficient) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        const int &j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        // Start index in rate mass.
        int k = j * ClassSpecies::Ngas_;

        for (int l = 0; l < ClassSpecies::Ngas_; l++)
          {
            int m = ClassSpecies::semivolatile_[l];

            condensation_coefficient[k++] = concentration_aer_number[j]
              * ComputeCoefficient(ClassAerosolData::diameter_[j],
                                   ClassMeteorologicalData::gas_quadratic_mean_velocity_[l],
                                   ClassMeteorologicalData::gas_diffusivity_[l],
                                   ClassSpecies::accomodation_coefficient_[m]);
          }
      }
  }


  // Compute kernel.
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeKernel(const int section_min,
                                                                                    const int section_max,
                                                                                    const vector1i &parameterization_section,
                                                                                    const real *concentration_aer_number,
                                                                                    const real *concentration_aer_mass,
                                                                                    const real *concentration_gas,
                                                                                    vector1r &condensation_coefficient,
                                                                                    vector1r &rate_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        const int &j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        // Start index in rate mass.
        int n = j * ClassSpecies::Nspecies_;

        // Start index in equilibrium array.
        int o = j * ClassSpeciesEquilibrium::Nspecies_;

        // Start index in condensation specific arrays.
        int k = j * ClassSpecies::Ngas_;

        // If too few particles.
        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Compute kernel coefficient for this section.
            vector1r coefficient(ClassSpecies::Ngas_, real(0));
            vector1r rate_aer_mass_local(ClassSpecies::Ngas_, real(0));

            for (int l = 0; l < ClassSpecies::Ngas_; l++)
              {
                const int m = ClassSpecies::semivolatile_[l];

                // The equilibrium gas surface concentration
                // is set to a negative value if it has no physical meaning
                // from previous thermodynamic computation (e.g. no absorbing material).
                if (ClassAerosolData::equilibrium_gas_surface_[k + l] >= real(0))
                  {
                    coefficient[l] = concentration_aer_number[j] * 
                      ComputeCoefficient(ClassAerosolData::diameter_[j],
                                         ClassMeteorologicalData::gas_quadratic_mean_velocity_[l],
                                         ClassMeteorologicalData::gas_diffusivity_[l],
                                         ClassSpecies::accomodation_coefficient_[m]);

                    // Single particle mass rate. Kelvin effect is already accounted.
                    rate_aer_mass_local[l] = coefficient[l]
                      * (concentration_gas[l] - ClassAerosolData::equilibrium_gas_surface_[k + l]);

                    // Prevent non physical cases, if no species mass in particle, evaporation cannot take place.
                    if (concentration_aer_mass[n + m] == real(0))
                      if (rate_aer_mass_local[l] < real(0))
                        rate_aer_mass_local[l] = real(0);
                  }
              }

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
            cout << "coefficient = " << coefficient << endl;
            cout << "rate_aer_mass_local = " << rate_aer_mass_local << endl;
            cout << "liquid_water_content = " << ClassAerosolData::liquid_water_content_[j] << endl;
#endif

            // If particle is wet, eventually compute H+ flux correction for aqueous phase.
            // If it is dry, also correct rate and equilibrium gas concentrations for inorganic salts.
            if (ClassAerosolData::liquid_water_content_[j] > real(0))
              flux_correction_aqueous_->ComputeFluxCorrection(ClassAerosolData::equilibrium_aer_internal_.data() + o,
                                                              concentration_gas,
                                                              coefficient,
                                                              ClassAerosolData::equilibrium_gas_surface_.data() + k,
                                                              rate_aer_mass_local);
            else
              flux_correction_dry_->ComputeFluxCorrection(ClassMeteorologicalData::temperature_, coefficient,
                                                          ClassAerosolData::equilibrium_aer_internal_.data() + o,
                                                          concentration_gas,
                                                          ClassAerosolData::equilibrium_gas_surface_.data() + k,
                                                          rate_aer_mass_local);

            // Send back condensation coefficient and rate.
            for (int l = 0; l < ClassSpecies::Ngas_; l++)
              {
                condensation_coefficient[k + l] = coefficient[l];
                rate_aer_mass[n + ClassSpecies::semivolatile_[l]] = rate_aer_mass_local[l];
              }
          }
      }
  }

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  // Compute coefficient for NPF in m3.s^{-1}
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(vector2r &coefficient) const
  {
    for (int i = 0; i < NPF::ClassParticleData::Nparticle_active_; ++i)
      for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
        coefficient[i][j] =
          ComputeCoefficient(NPF::ClassParticleData::diameter_[ NPF::ClassParticleData::index_active_[i]],
                             NPF::ClassMeteoData::gas_quadratic_mean_velocity_[j],
                             NPF::ClassMeteoData::gas_diffusivity_[j],
                             NPF::ClassSpecies::accomodation_coefficient_[j]);
  }
#endif


#ifdef AMC_WITH_TEST
  // Test.
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::_TestCoefficient_(const real &temperature,
                                                                                        const real &pressure,
                                                                                        const real &diameter,
                                                                                        vector<real> &condensation_coefficient) const
  {
    condensation_coefficient.assign(ClassSpecies::Ngas_, real(0));

    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      {
        int j = ClassSpecies::semivolatile_[i];

        // Molar mass in kg / mol.
        real molar_mass = ClassSpecies::molar_mass_[j] * 1.e-9;

        // Gas diffusivity.
        real gas_diffusivity =
          ClassParameterizationPhysics::ComputeGasDiffusivity(temperature, pressure,
                                                              ClassSpecies::collision_factor_[j],
                                                              ClassSpecies::molecular_diameter_[j],
                                                              molar_mass);
        // Gas velocity.
        real gas_quadratic_mean_velocity =
          ClassParameterizationPhysics::ComputeGasQuadraticMeanVelocity(temperature, molar_mass);

        // Condensation coefficient.
        condensation_coefficient[i] = ComputeCoefficient(diameter,
                                                         gas_quadratic_mean_velocity,
                                                         gas_diffusivity,
                                                         ClassSpecies::accomodation_coefficient_[j]);
      }
  }


  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::_Test_(const real &temperature,
                                                                             const real &pressure,
                                                                             const real &diameter,
                                                                             const real &density,
                                                                             const vector<real> &concentration_gas,
                                                                             const vector<real> &concentration_aer_mass,
                                                                             const vector<real> &equilibrium_aer_internal,
                                                                             vector<real> &equilibrium_gas_surface,
                                                                             vector<real> &rate_aer_mass) const
  {
    vector1r coefficient(ClassSpecies::Ngas_, real(0));
    rate_aer_mass.assign(ClassSpecies::Ngas_, real(0));

    const real &liquid_water_content = equilibrium_aer_internal.back();

    // Density in µg.µm^{-3}.
    real density2 = density * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

    // Single aerosol mass (µg).
    real single_mass = density2 * AMC_PI6 * diameter * diameter * diameter;

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
    cout << "single_mass = " << single_mass << endl;
#endif

    // Guess concentration number from dry diameter and mass concentration.
    real concentration_aer_num(real(0));
    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      concentration_aer_num += concentration_aer_mass[i];

    concentration_aer_num /= single_mass;

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
    CBUG << "concentration_aer_num = " << concentration_aer_num << endl;
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "Guessed number aerosol concentration = "
                         << concentration_aer_num << " #.m^{-3}" << endl;
#endif

    // Compute wet diameter with liquid water content.
    real diameter_wet = pow((single_mass + liquid_water_content / concentration_aer_num)
                            / density2 * AMC_INV_PI6, AMC_FRAC3);

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
    CBUG << "diameter_wet = " << diameter_wet << endl;
#endif

    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      {
        const int &j = ClassSpecies::semivolatile_[i];

        // Molar mass in kg / mol.
        real molar_mass = ClassSpecies::molar_mass_[j] * 1.e-9;

        // Gas diffusivity.
        real gas_diffusivity =
          ClassParameterizationPhysics::ComputeGasDiffusivity(temperature, pressure,
                                                              ClassSpecies::collision_factor_[j],
                                                              ClassSpecies::molecular_diameter_[j],
                                                              molar_mass);
        // Gas velocity.
        real gas_quadratic_mean_velocity =
          ClassParameterizationPhysics::ComputeGasQuadraticMeanVelocity(temperature, molar_mass);

        // The equilibrium gas surface concentration
        // is set to a negative value if it has no physical meaning
        // from previous thermodynamic computation (e.g. no absorbing material).
        if (equilibrium_gas_surface[i] >= real(0))
          {
            // Condensation coefficient.
            coefficient[i] = concentration_aer_num
              * ComputeCoefficient(diameter_wet,
                                   gas_quadratic_mean_velocity,
                                   gas_diffusivity,
                                   ClassSpecies::accomodation_coefficient_[j]);

            // Single particle mass rate. Kelvin effect is already accounted.
            rate_aer_mass[i] = coefficient[i] * (concentration_gas[i] - equilibrium_gas_surface[i]);

            // Prevent non physical cases possibly due to Kelvin effect,
            // On thermodynamic output, the equilibrium gas surface concentration
            // is set to the gas concentration if no absorbing material,
            // thus avoiding c/e, but the kelvin effect may perturbate this.
            if (concentration_aer_mass[j] == real(0))
              if (rate_aer_mass[i] < real(0))
                rate_aer_mass[i] = real(0);
          }
      }

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
    CBUG << "coefficient = " << coefficient << endl;
    CBUG << "rate_aer_mass = " << rate_aer_mass << endl;
    CBUG << "liquid_water_content = " << liquid_water_content << endl;
#endif

    // If particle is wet, eventually compute H+ flux correction for aqueous phase.
    // If it is dry, also correct rate and equilibrium gas concentrations for inorganic salts.
    if (liquid_water_content > real(0))
      flux_correction_aqueous_->ComputeFluxCorrection(equilibrium_aer_internal.data(),
                                                      concentration_gas.data(),
                                                      coefficient,
                                                      equilibrium_gas_surface.data(),
                                                      rate_aer_mass);
    else
      flux_correction_dry_->ComputeFluxCorrection(temperature,
                                                  coefficient,
                                                  equilibrium_aer_internal.data(),
                                                  concentration_gas.data(),
                                                  equilibrium_gas_surface.data(),
                                                  rate_aer_mass);

#ifdef AMC_WITH_DEBUG_DIFFUSION_LIMITED
    CBUG << "rate_aer_mass_corrected = " << rate_aer_mass << endl;
#endif
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_CXX
#endif
