// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TABLE_HXX

#define AMC_COAGULATION_TABLE_NUMBER_VARIABLE_MAX 6
#define AMC_COAGULATION_TABLE_EPSILON         1.e-6  // Small value to avoid roundoff errors.

namespace AMC
{
  class ClassParameterizationCoagulationTable : public ClassParameterizationCoagulationBase
  {
  public:
#ifdef AMC
    typedef AMC::real real;
    typedef AMC::vector1i vector1i;
    typedef AMC::vector1r vector1r;
    typedef AMC::vector2r vector12r;
#else
    typedef double real;
    typedef vector<int> vector1i;
    typedef vector<real> vector1r;
    typedef vector<real> vector12r;    
#endif

  private:

    /*!< Number of parameterizations.*/
    int Nparam_;

    /*!< Number of edges of hypercube.*/
    int Nedge_;

    /*!< Size of table.*/
    unsigned long Ntable_;

    /*!< Number of cubes.*/
    unsigned long Ncube_;

    /*!< Is table computed ?.*/
    bool is_table_computed_;

    /*!< Pointer to the parameterizations to tabulate.*/
    vector<ClassParameterizationCoagulationBase* > parameterization_;

    /*!< File path to table.*/
    string path_;

    /*!< Number of discretization sections for each variable.*/
    vector1i Ndisc_;

    /*!< Working number of discretization sections for each variable.*/
    vector1i Ndisc_work_;

    /*!< Number of discretization points for each variable.*/
    vector1i Npoint_;

    /*!< Increment to circle on all hypercube edges.*/
    vector2i increment_;

    /*!< Min of discretization for each variable.*/
    vector1r value_min_;

    /*!< Max of discretization for each variable.*/
    vector1r value_max_;

    /*!< Delta of discretization for each variable.*/
    vector1r delta_;

    /*!< inverse of previous delta.*/
    vector1r delta_inv_;

    /*!< Table of hypercube edges locations.*/
    vector2i table_edge_index_;

    /*!< Tabulated flat kernel array at edges.*/
    vector1r table_edge_kernel_;

    /*!< Construct method.*/
    void construct(Ops::Ops &ops);

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationTable();
    ClassParameterizationCoagulationTable(Ops::Ops &ops, const bool read_table = true);

    /*!< Destructor.*/
    ~ClassParameterizationCoagulationTable();

    /*!< Clone.*/
    ClassParameterizationCoagulationTable* clone() const;

    /*!< Get methods.*/
    int GetNtable() const;
    int GetNcube() const;
    void GetNdisc(vector<int> &Ndisc) const;
    void GetDisc(const int &i, vector<real> &disc) const;
    void GetNpoint(vector<int> &Npoint) const;
    void GetTable(vector<real> &kernel) const;
    ClassParameterizationCoagulationBase* GetParameterization(const int &i);

    /*!< Manage table.*/
    void Compute();
    void Read();
    void Write() const;
    void Clear();

    /*!< Compute kernel for tabulation. This is a fake function, it is obviously irrelevant in this context.*/
    real KernelTable(const vector1r &variable) const;

#ifndef SWIG
    /*!< Compute kernel.*/
    void ComputeKernel(const vector2i &couple,
                       Array<real, 2> &kernel) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TABLE_HXX
#endif
