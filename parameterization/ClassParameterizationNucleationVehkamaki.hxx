// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VEHKAMAKI_HXX

#ifndef SWIG
#define VEHKAMAKI_POLYNOMIAL_FIT(a0, a1, a2, a3, b0) real(a0) \
      + temperature * (real(a1) + temperature * (real(a2) + temperature * real(a3))) \
      + real(b0) * sulfuric_acid_mole_fraction_inv
#define SULFATE_DENSITY(a0, a1, a2, a3, a4, a5, a6) \
  a0 + xm * (a1 + xm * (a2 + xm * (a3 + xm * (a4 + xm * (a5 + xm * a6)))));
#endif

#define AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MIN 230.15
#define AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MAX 300.15
#define AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MIN 1.e-4
#define AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MAX 1
#define AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MIN 1.e4
#define AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MAX 1.e11
#define AMC_NUCLEATION_VEHKAMAKI_CLUSTER_TOTAL_NUMBER_MOLECULE_MIN 4

#define AMC_NUCLEATION_VEHKAMAKI_RATIO_MOL_WEIGHT_WATER_SULFATE 0.1836734693877551

namespace AMC
{
  class ClassParameterizationNucleationVehkamaki : public ClassParameterizationNucleationBase
  {
  private:

    /*!< Threshold parameters due to validity range of formula.*/
    real threshold_relative_humidity(const real &relative_humidity) const;
    real threshold_temperature(const real &temperature) const;
    real threshold_concentration_sulfate_molec_cm3(const real &concentration_sulfate_molec_cm3) const;

    /*!< Nucleation rate.*/
    real nucleation_rate(const real &concentration_sulfate_molec_cm3_log,
                         const real &sulfuric_acid_mole_fraction_inv,
                         const real &temperature,
                         const real &relative_humidity_log) const;

    /*!< Sulfuric acid density.*/
    real sulfuric_acid_density(const real &sulfuric_acid_mass_fraction, const real &temperature) const;

    /*!< Compute sulfuric acid mol fraction.*/
    real sulfuric_acid_mole_fraction(const real &concentration_sulfate_molec_cm3_log,
                                     const real &temperature,
                                     const real &relative_humidity_log) const;

    /*!< Critical cluster radius.*/
    real critical_cluster_radius(const real &critical_cluster_total_number_molecule,
                                 const real &sulfuric_acid_mole_fraction) const;

    /*!< Total number of molecules of critical cluster.*/
    real critical_cluster_total_number_molecule(const real &concentration_sulfate_molec_cm3_log,
                                                const real &temperature,
                                                const real &relative_humidity_log,
                                                const real &sulfuric_acid_mole_fraction_inv) const;

  public:

    /*!< Constructors.*/
    ClassParameterizationNucleationVehkamaki();

    /*!< Destructor.*/
    ~ClassParameterizationNucleationVehkamaki();

    /*!< Clone.*/
    ClassParameterizationNucleationVehkamaki* clone() const;

    /*!< Compute kernel.*/
    real ComputeNumberMoleculeTotal(const real &temperature,
                                    const real &relative_humidity,
                                    const real &concentration_sulfate_molec_cm3) const;

    real ComputeSulfuricAcidMoleFraction(const real &concentration_sulfate_molec_cm3,
                                         const real &temperature,
                                         const real &relative_humidity) const;

    real ComputeSulfuricAcidDensity(const real &temperature,
                                    const real &relative_humidity,
                                    const real &concentration_sulfate_molec_cm3) const;

    real ComputeCriticalClusterRadius(const real &temperature,
                                      const real &relative_humidity,
                                      const real &concentration_sulfate_molec_cm3) const;

    real ComputeKernel(const real &temperature,
                       const real &relative_humidity,
                       const real &concentration_sulfate_molec_cm3) const;

#ifndef SWIG
    void ComputeKernel(const int &section_index,
                       const real *concentration_gas,
                       vector1r &rate_aer_number,
                       vector1r &rate_aer_mass) const;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    bool ComputeKernel(const vector1r &concentration_gas,
                       real &diameter,
                       real &rate_aer_number,
                       vector1r &rate_aer_mass) const;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VEHKAMAKI_HXX
#endif
