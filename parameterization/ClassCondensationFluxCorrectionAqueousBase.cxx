// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_BASE_CXX

#include "ClassCondensationFluxCorrectionAqueousBase.hxx"

#define H2SO4 0
#define NH3   1
#define HNO3  2
#define HCl   3

namespace AMC
{
  // Constructors.
  ClassCondensationFluxCorrectionAqueousBase::ClassCondensationFluxCorrectionAqueousBase(Ops::Ops &ops, const string name)
    : name_(name), index_hydronium_(-1), limitation_hydronium_(AMC_FLUX_CORRECTION_AQUEOUS_LIMITATION_HYDRONIUM_DEFAULT)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info() << Reset() << "Instantiating aqueous flux correction \"" << name_ << "\"." << endl;
#endif

    // Read standalone configuration.
    string prefix_orig = ops.GetPrefix();

    ops.SetPrefix("flux_correction_aqueous.");

    vector1s species_name;
    ops.Set("species", "", species_name);

    ops.Set("molar_mass", "", molar_mass_);
    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      molar_mass_[i] *= real(1e6);

    // how much we limit hydronium flux 0 < < 1.
    limitation_hydronium_ = ops.Get<real>("limitation_hydronium");

    string hydronium_name = ops.Get<string>("hydronium_name");

    // Revert to AMC specific configuration.
    ops.SetPrefix(prefix_orig);

    index_.assign(AMC_FLUX_CORRECTION_AQUEOUS_NGAS, -1);

    if (ops.Exists("flux_correction.aqueous"))
      {
        ops.SetPrefix(prefix_orig + "flux_correction.aqueous.");

        // how much we limit hydronium flux 0 < < 1.
        limitation_hydronium_ = ops.Get<real>("limitation_hydronium", "", limitation_hydronium_);

        index_[H2SO4] = ops.Get<int>(species_name[H2SO4], "", -1);
        index_[NH3] = ops.Get<int>(species_name[NH3], "", -1);
        index_[HNO3] = ops.Get<int>(species_name[HNO3], "", -1);
        index_[HCl] = ops.Get<int>(species_name[HCl], "", -1);

        index_hydronium_ = ops.Get<int>("index_hydronium", "", -1);

        ops.SetPrefix(prefix_orig);
      }

    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      if (index_[i] < 0)
        for (int j = 0; j < ClassSpecies::Ngas_; j++)
          if (species_name[i] == ClassSpecies::name_[ClassSpecies::semivolatile_[j]])
            {
              index_[i] = j;
              break;
            }

    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      if (index_[i] < 0)
        throw AMC::Error("Unable to find AMC index of species \"" + species_name[i] + "\".");

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "Gas species \"" << species_name[i]
                           << "\" at index " << index_[i] << endl;
#endif

    if (index_hydronium_ < 0)
      for (int i = 0; i < ClassSpeciesEquilibrium::Nspecies_; i++)
        if (hydronium_name == ClassSpeciesEquilibrium::name_[i])
          {
            index_hydronium_ = i;
            break;
          }

    if (index_hydronium_ < 0)
      throw AMC::Error("Unable to find index of H+ ions in equilibrium species list.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "index_hydronium = " << index_hydronium_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "limitation_hydronium = " << limitation_hydronium_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassCondensationFluxCorrectionAqueousBase::~ClassCondensationFluxCorrectionAqueousBase()
  {
    return;
  }


  // Get methods.
  string ClassCondensationFluxCorrectionAqueousBase::GetName() const
  {
    return name_;
  }
}

#undef H2SO4
#undef NH3
#undef HNO3
#undef HCl

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_BASE_CXX
#endif
