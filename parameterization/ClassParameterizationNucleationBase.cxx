// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_BASE_CXX

#include "ClassParameterizationNucleationBase.hxx"

namespace AMC
{
  // Set gas and species index.
  void ClassParameterizationNucleationBase::set_index_amc(const string name)
  {
    bool species_not_found(true);
    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      if (ClassSpecies::name_[ClassSpecies::semivolatile_[i]] == name)
        {
          species_index_amc_.push_back(ClassSpecies::semivolatile_[i]);
          gas_index_amc_.push_back(i);
          species_not_found = false;
          ++Nspecies_;
          break;
        }

    if (species_not_found)
      throw AMC::Error("Unable to find species \"" + name + "\" index in AMC aerosol and gas species.");
  }


  // Constructor.
  ClassParameterizationNucleationBase::ClassParameterizationNucleationBase(const string name)
    : name_(name), Nspecies_(0)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << Reset()
                         << "Instantiate nucleation parameterization \"" << name_ << "\"." << endl;
#endif

    // Sulfate index.
    set_index_amc("H2SO4");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "species_index[sulfate] = "
                         << species_index_amc_[AMC_NUCLEATION_SULFATE_INDEX] << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "gas_index[sulfate] = "
                         << gas_index_amc_[AMC_NUCLEATION_SULFATE_INDEX] << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationNucleationBase::~ClassParameterizationNucleationBase()
  {
    return;
  }


  // Get methods.
  string ClassParameterizationNucleationBase::GetName() const
  {
    return name_;
  }


  int ClassParameterizationNucleationBase::GetNspecies() const
  {
    return Nspecies_;
  }


  const vector<int>& ClassParameterizationNucleationBase::GetSpeciesIndex() const
  {
    return species_index_amc_;
  }


  const vector<int>& ClassParameterizationNucleationBase::GetGasIndex() const
  {
    return gas_index_amc_;
  }


  void ClassParameterizationNucleationBase::GetSpeciesIndex(vector<int> &species_index) const
  {
    species_index = species_index_amc_;
  }


  void ClassParameterizationNucleationBase::GetGasIndex(vector<int> &gas_index) const
  {
    gas_index = gas_index_amc_;
  }


  // Set methods.
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  void ClassParameterizationNucleationBase::SetIndexNPF()
  {
    vector1i species_index_npf;
    NPF::ClassSpecies::GetSpeciesIndex(species_index_npf);

    index_npf_.clear();
    for (int i = 0; i < int(species_index_amc_.size()); ++i)
      for (int j = 0; j < int(species_index_npf.size()); ++j)
        if (species_index_amc_[i] == species_index_npf[j])
          {
            index_npf_.push_back(j);
            break;
          }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "For nucleation parameterization \"" << name_
                         << "\" index_npf = " << index_npf_ << endl;
#endif
  }
#endif

  // Convert concentrations.
  real ClassParameterizationNucleationBase::ConvertSulfateFromUGM3toMOLECULECM3(const real &concentration_sulfate_ugm3)
  {
    return concentration_sulfate_ugm3 * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;
  }

  real ClassParameterizationNucleationBase::ConvertAmmoniumFromUGM3toMOLECULECM3(const real &concentration_ammonium_ugm3)
  {
    return concentration_ammonium_ugm3 * AMC_NUCLEATION_CONVERT_AMMONIUM_FROM_UGM3_TO_MOLECULE_CM3;
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_BASE_CXX
#endif
