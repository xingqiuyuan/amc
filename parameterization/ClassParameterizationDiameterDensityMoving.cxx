// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_MOVING_CXX

#include "ClassParameterizationDiameterDensityMoving.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationDiameterDensityMoving::ClassParameterizationDiameterDensityMoving(Ops::Ops &ops)
    : ClassParameterizationDiameterBase("DensityMoving"), Ngroup_(0)
  {
    // Keep original prefix in mind.
    string prefix_orig = ops.GetPrefix();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Moving Density diameter parameterization." << endl; 
#endif

    // Density for each group, default is to use that of each species.
    vector1s group_list = ops.GetEntryList("group");
    Ngroup_ = int(group_list.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "Ngroup = " << Ngroup_ << endl;
#endif

    Nspecies_.assign(Ngroup_ + 1, 0);
    species_index_.resize(Ngroup_ + 1);
    density_.resize(Ngroup_);

    vector1b species_assigned(ClassSpecies::Nspecies_, false);

    for (int i = 0; i < Ngroup_; i++)
      {
        ops.SetPrefix(prefix_orig + "density." + group_list[i] + ".");
        ops.Set("index", "", species_index_[i]);
        Nspecies_[i] = int(species_index_[i].size());
        density_[i] = ops.Get<real>("density") * AMC_PARAMETERIZATION_DIAMETER_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "For group \"" << group_list[i] << "\" species index = "
                             << species_index_[i] << ", use density = " << density_[i] << " µg.µm^{-3}" << endl;
#endif
        for (int j = 0; j < Nspecies_[i]; j++)
          species_assigned[species_index_[i][j]] = true;
      }

    // Last group consists in remaining species, use their default densities.
    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (! species_assigned[i])
        {
          species_index_.back().push_back(i);
          Nspecies_.back()++;
        }

    // Return to original prefix.
    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassParameterizationDiameterDensityMoving::~ClassParameterizationDiameterDensityMoving()
  {
    return;
  }


  // Get methods.
  void ClassParameterizationDiameterDensityMoving::GetSpeciesIndex(const int &i, vector<int> &species_index) const
  {
    species_index = species_index_[i];
  }

  real ClassParameterizationDiameterDensityMoving::GetDensity(const int &i) const
  {
    return density_[i];
  }

  int ClassParameterizationDiameterDensityMoving::GetNspecies(const int &i) const
  {
    return Nspecies_[i];
  }

  int  ClassParameterizationDiameterDensityMoving::GetNgroup() const
  {
    return Ngroup_;
  }


  // Clone.
  ClassParameterizationDiameterDensityMoving* ClassParameterizationDiameterDensityMoving::clone() const
  {
    return new ClassParameterizationDiameterDensityMoving(*this);
  }


  // Copy.
  void ClassParameterizationDiameterDensityMoving::Copy(const ClassParameterizationDiameterBase *param)
  {
    species_index_ = reinterpret_cast<const ClassParameterizationDiameterDensityMoving*>(param)->species_index_;
    density_ = reinterpret_cast<const ClassParameterizationDiameterDensityMoving*>(param)->density_;
    Nspecies_ = reinterpret_cast<const ClassParameterizationDiameterDensityMoving*>(param)->Nspecies_;
    Ngroup_ = reinterpret_cast<const ClassParameterizationDiameterDensityMoving*>(param)->Ngroup_;
    ClassParameterizationDiameterBase::Copy(*param);
  }


  // Compute diameter for AMC.
  void ClassParameterizationDiameterDensityMoving::ComputeDiameter(const int section_min,
                                                                   const int section_max,
                                                                   const vector1i &parameterization_section,
                                                                   const real *concentration_aer_number,
                                                                   const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            real volume(real(0));
            real mass_total(real(0));

            int k = j * ClassSpecies::Nspecies_;
            for (int l = 0; l < Ngroup_; l++)
              {
                const vector1i &species_index = species_index_[l];

                real mass(real(0));
                for (int m = 0; m < Nspecies_[l]; m++)
                  mass += concentration_aer_mass[k + species_index[m]];

                volume += mass / density_[l];
                mass_total += mass;
              }

            const vector1i &species_index = species_index_.back();
            for (int m = 0; m < Nspecies_.back(); m++)
              {
                const int &n = species_index[m];
                mass_total += concentration_aer_mass[k + n];
                volume += concentration_aer_mass[k + n] / ClassSpecies::density_[n];
              }

            // Add the liquid water content, if any.
            mass_total += ClassAerosolData::liquid_water_content_[j];
            volume += ClassAerosolData::liquid_water_content_[i] / AMC_WATER_DENSITY;

            if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
              {
                ClassAerosolData::density_[j] = mass_total / volume;
                ClassAerosolData::mass_[j] = mass_total / concentration_aer_number[j];
                ClassAerosolData::diameter_[j] = pow(AMC_INV_PI6 * volume / concentration_aer_number[j], AMC_FRAC3);
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_DENSITY_MOVING_CXX
#endif
