// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_CXX

#include "ClassParameterizationCoagulationBrownian.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCoagulationBrownian::ClassParameterizationCoagulationBrownian(const string name, const int Nvariable)
    :  ClassParameterizationCoagulationBase(name, Nvariable),
       knudsen_min_(AMC_COAGULATION_BROWNIAN_KNUDSEN_MIN),
       knudsen_max_(AMC_COAGULATION_BROWNIAN_KNUDSEN_MAX)
  {
    this->density_fixed_ *= AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Brownian coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "knudsen_min = " << knudsen_min_
                         << ", knudsen_max = " << knudsen_max_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "density_fixed = " << this->density_fixed_ << " ug/µm3" << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";

    return;
  }

  ClassParameterizationCoagulationBrownian::ClassParameterizationCoagulationBrownian(Ops::Ops &ops,
                                                                                     const string name,
                                                                                     const int Nvariable)
    :  ClassParameterizationCoagulationBase(name, Nvariable),
       knudsen_min_(AMC_COAGULATION_BROWNIAN_KNUDSEN_MIN),
       knudsen_max_(AMC_COAGULATION_BROWNIAN_KNUDSEN_MAX)
  {
    knudsen_min_ = ops.Get<real>("knudsen_min", "", AMC_COAGULATION_BROWNIAN_KNUDSEN_MIN);
    knudsen_max_ = ops.Get<real>("knudsen_max", "", AMC_COAGULATION_BROWNIAN_KNUDSEN_MAX);
    this->density_fixed_ =  ops.Get<real>("density_fixed", "", this->density_fixed_)
      * AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Brownian coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "knudsen_min = " << knudsen_min_
                         << ", knudsen_max = " << knudsen_max_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "density_fixed = " << this->density_fixed_ << " ug/µm3" << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";

    return;
  }


  // Destructor.
  ClassParameterizationCoagulationBrownian::~ClassParameterizationCoagulationBrownian()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationBrownian* ClassParameterizationCoagulationBrownian::clone() const
  {
    return new ClassParameterizationCoagulationBrownian(*this);
  }


  // Copy.
  void ClassParameterizationCoagulationBrownian::Copy(const ClassParameterizationCoagulationBase *param)
  {
    knudsen_min_ = reinterpret_cast<const ClassParameterizationCoagulationBrownian*>(param)->knudsen_min_;
    knudsen_max_ = reinterpret_cast<const ClassParameterizationCoagulationBrownian*>(param)->knudsen_max_;
    ClassParameterizationCoagulationBase::Copy(*param);
  }


  // Get methods.
  real ClassParameterizationCoagulationBrownian::GetKnudsenMin() const
  {
    return knudsen_min_;
  }
 
  real ClassParameterizationCoagulationBrownian::GetKnudsenMax() const
  {
    return knudsen_max_;
  }


  // Set methods.
  void ClassParameterizationCoagulationBrownian::SetKnudsenMin(const real &knudsen_min)
  {
    knudsen_min_ = knudsen_min;
  }

  void ClassParameterizationCoagulationBrownian::SetKnudsenMax(const real &knudsen_max)
  {
    knudsen_max_ = knudsen_max;
  }


  // Compute brownian kernel.
  real ClassParameterizationCoagulationBrownian::KernelContinuous(const real &diameter1, const real &diameter2,
                                                                  const real &knudsen1, const real &knudsen2,
                                                                  const real &temperature,
                                                                  const real &air_dynamic_viscosity) const
  {
    real f1 = (real(5) + knudsen1 * (real(4) + knudsen1 * (real(6) + real(18) *knudsen1)))
      / (real(5) + knudsen1 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen1));
    real f2 = (real(5) + knudsen2 * (real(4) + knudsen2 * (real(6) + real(18) *knudsen2)))
      / (real(5) + knudsen2 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen2));
    real r = diameter1 / diameter2;

    // 2 * Kb / 3.
    return 9.204325333333333e-24 * temperature / air_dynamic_viscosity * (f1 + f2 + f2 * r + f1 / r);
  }


  real ClassParameterizationCoagulationBrownian::KernelFreeMolecular(const real &diameter1, const real &diameter2,
                                                                     const real &mass1, const real &mass2,
                                                                     const real &temperature) const
  {
    real diameter_sum = diameter1 + diameter2;

    // sqrt(Kb * Pi / 2 * 1.e9) * 1.e-12
    return 1.4726568044299332e-19 * (diameter_sum * diameter_sum) * sqrt(temperature * (real(1) / mass1 + real(1) / mass2));
  }


  real ClassParameterizationCoagulationBrownian::KernelTransition(const real &diameter1, const real &diameter2,
                                                                  const real &knudsen1, const real &knudsen2,
                                                                  const real &mass1, const real &mass2,
                                                                  const real &temperature,
                                                                  const real &air_dynamic_viscosity) const
  {
    // Convert from µm to m.
    real diam1 = diameter1 * real(1e-6);
    real diam2 = diameter2 * real(1e-6);
    real diam_sum = diam1 + diam2;

    real f1 = (real(5) + knudsen1 * (real(4) + knudsen1 * (real(6) + real(18) *knudsen1)))
      / (real(5) + knudsen1 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen1));
    real f2 = (real(5) + knudsen2 * (real(4) + knudsen2 * (real(6) + real(18) *knudsen2)))
      / (real(5) + knudsen2 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen2));

    real diffusion_mutual = AMC_COAGULATION_BROWNIAN_FRAC3 * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature
      * (f1 / diam1 + f2 / diam2) / (air_dynamic_viscosity * AMC_COAGULATION_PI);

    // The 1.e-9 factor in 8.e-9 converts from the particle mass mass1 and mass2 from µg to kg.
    real tmp = real(8e-9) * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature / AMC_COAGULATION_PI;
    real tmp1 = f1 * sqrt(tmp * mass1) / air_dynamic_viscosity;
    real tmp2 = f2 * sqrt(tmp * mass2) / air_dynamic_viscosity;

    real l1 = AMC_COAGULATION_BROWNIAN_FRAC3 * tmp1 / diam1;
    real l2 = AMC_COAGULATION_BROWNIAN_FRAC3 * tmp2 / diam2;

    real a1 = diam1 + l1;
    real a2 = diam2 + l2;
    real b1 = diam1 * diam1 + l1 * l1;
    real b2 = diam2 * diam2 + l2 * l2;

    real g1 = (a1 * a1 * a1 - b1 * sqrt(b1)) / tmp1 - diam1;
    real g2 = (a2 * a2 * a2 - b2 * sqrt(b2)) / tmp2 - diam2;

    // The 1e9 factor in 8e9 converts from the particle mass1 and mass2 from µg to kg.
    real velocity_mutual = real(8e9) * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT
      * temperature * (real(1) / mass1 + real(1) / mass2) / AMC_COAGULATION_PI;

    return real(2) * AMC_COAGULATION_PI * diffusion_mutual * diam_sum
      / (diam_sum / (diam_sum + real(2) * sqrt(g1 * g1 + g2 * g2))
         + real(8) * diffusion_mutual / (sqrt(velocity_mutual) * diam_sum));
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationBrownian::KernelTable(const vector1r &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];

    // Air dynamic viscosity.
    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);

    // Air free mean path.
    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);

    // Knudsen number.
    real knudsen1 = real(2) * air_free_mean_path / diameter1;
    real knudsen2 = real(2) * air_free_mean_path / diameter2;

    // Particle mass
    real mass1 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter1 * diameter1 * diameter1;
    real mass2 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter2 * diameter2 * diameter2;

    if (knudsen1 < knudsen_min_ && knudsen2 < knudsen_min_)
      return KernelContinuous(diameter1, diameter2,
                              knudsen1, knudsen2,
                              temperature,
                              air_dynamic_viscosity);
    else if (knudsen1 >= knudsen_max_ && knudsen2 >= knudsen_max_)
      return KernelFreeMolecular(diameter1, diameter2,
                                 mass1, mass2, 
                                 temperature);
    else
      return KernelTransition(diameter1, diameter2,
                              knudsen1, knudsen2,
                              mass1, mass2, 
                              temperature,
                              air_dynamic_viscosity);
  }


  // Compute kernel for AMC.
  void ClassParameterizationCoagulationBrownian::ComputeKernel(const vector2i &couple, Array<real, 2> &kernel) const
  {
    int Ncouple = int(couple.size());

    for (int i = 0; i < Ncouple; i++)
      {
        const int g1 = couple[i][0];
        const int g2 = couple[i][1];

        real knudsen1 = real(2) * ClassMeteorologicalData::air_free_mean_path_ / ClassAerosolData::diameter_[g1];
        real knudsen2 = real(2) * ClassMeteorologicalData::air_free_mean_path_ / ClassAerosolData::diameter_[g2];

        if (knudsen1 < knudsen_min_ && knudsen2 < knudsen_min_)
          kernel(g1, g2) += KernelContinuous(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                             knudsen1, knudsen2,
                                             ClassMeteorologicalData::temperature_,
                                             ClassMeteorologicalData::air_dynamic_viscosity_);
        else if (knudsen1 >= knudsen_max_ && knudsen2 >= knudsen_max_)
          kernel(g1, g2) += KernelFreeMolecular(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                                ClassAerosolData::mass_[g1], ClassAerosolData::mass_[g2],
                                                ClassMeteorologicalData::temperature_);
        else
          kernel(g1, g2) += KernelTransition(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                             knudsen1, knudsen2,
                                             ClassAerosolData::mass_[g1], ClassAerosolData::mass_[g2], 
                                             ClassMeteorologicalData::temperature_,
                                             ClassMeteorologicalData::air_dynamic_viscosity_);
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_CXX
#endif
