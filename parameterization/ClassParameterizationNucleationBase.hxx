// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_BASE_HXX

#define AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3 6.14504213e9
#define AMC_NUCLEATION_CONVERT_AMMONIUM_FROM_UGM3_TO_MOLECULE_CM3 3.54243605e10
#define AMC_NUCLEATION_SULFATE_INDEX 0

namespace AMC
{
  class ClassParameterizationNucleationBase
  {
  public:
    typedef AMC_PARAMETERIZATION_REAL real;
    typedef vector<int> vector1i;
    typedef vector<real> vector1r;

  protected:

    /*!< Species class may access elements of this class.*/
    friend class ClassSpecies;

    /*!< Parameterization name.*/
    string name_;

    /*!< Number of species involved in nucleation.*/
    int Nspecies_;

    /*!< Index in AMC of nucleation species.*/
    vector1i species_index_amc_;

    /*!< Gas index in AMC of nucleation species.*/
    vector1i gas_index_amc_;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    /*!< Index in NPF of nucleation species.*/
    vector1i index_npf_;
#endif

    /*!< Set gas and species index.*/
    void set_index_amc(const string name);

  public:

    /*!< Constructors.*/
    ClassParameterizationNucleationBase(const string name = "");

    /*!< Destructor.*/
    virtual ~ClassParameterizationNucleationBase();

    /*!< Get methods.*/
    string GetName() const;
    int GetNspecies() const;

#ifndef SWIG
    const vector<int>& GetSpeciesIndex() const;
    const vector<int>& GetGasIndex() const;
#endif
    void GetSpeciesIndex(vector<int> &species_index) const;
    void GetGasIndex(vector<int> &gas_index) const;

    /*!< Set methods.*/
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    void SetIndexNPF();
#endif

    /*!< Clone.*/
    virtual ClassParameterizationNucleationBase* clone() const = 0;

    /*!< Convert concentrations.*/
    static real ConvertSulfateFromUGM3toMOLECULECM3(const real &concentration_sulfate_ugm3);
    static real ConvertAmmoniumFromUGM3toMOLECULECM3(const real &concentration_ammonium_ugm3);

#ifndef SWIG
    /*!< Compute nucleation.*/
    virtual void ComputeKernel(const int &section_index,
                               const real *concentration_gas,
                               vector1r &rate_aer_number,
                               vector1r &rate_aer_mass) const = 0;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    virtual bool ComputeKernel(const vector1r &concentration_gas,
                               real &diameter,
                               real &rate_aer_number,
                               vector1r &rate_aer_mass) const = 0;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_BASE_HXX
#endif
