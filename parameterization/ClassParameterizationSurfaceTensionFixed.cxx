// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_FIXED_CXX

#include "ClassParameterizationSurfaceTensionFixed.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationSurfaceTensionFixed::ClassParameterizationSurfaceTensionFixed(Ops::Ops &ops, const int &Ng)
    : ClassParameterizationSurfaceTensionBase(ops, "Fixed")
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Fixed surface tension parameterization." << endl; 
#endif

    // Read fixed surface tension.
    if (this->phase_index_ < 0)
      {
        string prefix_orig = ops.GetPrefix();

        surface_tension_fixed_.resize(ClassPhase::Nphase_);

        vector1s value_name = ops.GetEntryList("value_list");
        for (int i = 0; i < int(value_name.size()); i++)
          {
            ops.SetPrefix(prefix_orig + ".value_list." + value_name[i] + ".");
            int phase_index = ops.Get<int>("phase");
            if (ops.Is<real>("value"))
              surface_tension_fixed_[phase_index] = vector1r(Ng, ops.Get<real>("value"));
            else
              ops.Set("value", "", surface_tension_fixed_[phase_index]);
          }

        ops.SetPrefix(prefix_orig);
      }
    else
      {
        surface_tension_fixed_.resize(1);
        if (ops.Is<real>("value"))
          surface_tension_fixed_[0] = vector1r(Ng, ops.Get<real>("value"));
        else
          ops.Set("value", "", surface_tension_fixed_[0]);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "surface_tension_fixed = " << surface_tension_fixed_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationSurfaceTensionFixed::~ClassParameterizationSurfaceTensionFixed()
  {
    return;
  }


  // Get methods.
  void ClassParameterizationSurfaceTensionFixed::GetSurfaceTensionFixed(vector<vector<real> > &surface_tension_fixed) const
  {
    surface_tension_fixed = surface_tension_fixed_;
  }


  // Clone.
  ClassParameterizationSurfaceTensionFixed* ClassParameterizationSurfaceTensionFixed::clone() const
  {
    return new ClassParameterizationSurfaceTensionFixed(*this);
  }


  // Copy.
  void ClassParameterizationSurfaceTensionFixed::Copy(const ClassParameterizationSurfaceTensionBase *param)
  {
    surface_tension_fixed_ = reinterpret_cast<const ClassParameterizationSurfaceTensionFixed*>(param)->surface_tension_fixed_;
    ClassParameterizationSurfaceTensionBase::Copy(*param);
  }


  // Compute surface tension for AMC.
  void ClassParameterizationSurfaceTensionFixed::ComputeSurfaceTension(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &parameterization_section,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        // Index of surface tension vector.
        int k = j * ClassPhase::Nphase_;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            if (this->phase_index_ < 0)
              for (int l = 0; l < ClassPhase::Nphase_; l++)
                ClassAerosolData::surface_tension_[k + l] = surface_tension_fixed_[l][j];
            else
              ClassAerosolData::surface_tension_[k + this->phase_index_] = surface_tension_fixed_[this->phase_index_][j];
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_FIXED_CXX
#endif
