// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BASE_CXX

#include "ClassParameterizationCoagulationBase.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCoagulationBase::ClassParameterizationCoagulationBase(const string name, const int Nvariable)
    : name_(name), Nvariable_(Nvariable), density_fixed_(AMC_COAGULATION_PARTICLE_DENSITY_DEFAULT)
  {
    Nvariable_ += 2;
    variable_name_.resize(Nvariable_);
    variable_name_[0] = "diameter0";
    variable_name_[1] = "diameter1";

    return;
  }


  // Destructor.
  ClassParameterizationCoagulationBase::~ClassParameterizationCoagulationBase()
  {
    return;
  }

  // Copy.
  void ClassParameterizationCoagulationBase::Copy(const ClassParameterizationCoagulationBase &param)
  {
    name_ = param.name_;
    Nvariable_ = param.Nvariable_;
    variable_name_ = param.variable_name_;
  }

  // Get methods.
  string ClassParameterizationCoagulationBase::GetName() const
  {
    return name_;
  }

  int ClassParameterizationCoagulationBase::GetNvariable() const
  {
    return Nvariable_;
  }

  void ClassParameterizationCoagulationBase::GetVariableName(vector<string> &variable_name) const
  {
    variable_name = variable_name_;
  }

  real ClassParameterizationCoagulationBase::GetDensityFixed() const
  {
    return density_fixed_;
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BASE_CXX
#endif
