// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_PHYSICS_HXX

#define AMC_PHYSICS_PI                           3.141592653589793     // Pi.
#define AMC_PHYSICS_IDEAL_GAS_CONSTANT           8.314462175           // Ideal gas constant J.mol^{-1}.K^{-1}
#define AMC_PHYSICS_MOLAR_MASS_AIR               2.87058e-2            // Air molar mass kg.mol^{-1}.
#define AMC_PHYSICS_MOLAR_MASS_WATER             1.8016e-2             // Water molar mass kg.mol^{-1}.
#define AMC_PHYSICS_MOLAR_MASS_WATER_MINUS_AIR  -1.06898e-2            // Difference beteen water and air molar mass kg.mol^{-1}.
#define AMC_PHYSICS_INV_TEMPERATURE_REFERENCE    0.003355704697986577  // Inverse of standard reference temperature in K^{-1}.
#define AMC_PHYSICS_TEMPERATURE_REFERENCE      298.0                   // Standard reference temperature in Kelvin.
#define AMC_PHYSICS_8_DIV_PI                     2.5464790894703255    // 8 / Pi.
#define AMC_PHYSICS_PARTICLE_DENSITY             1.0                   // Particle density in g.cm^{-3}
#define AMC_PHYSICS_BOLTZMANN_CONSTANT           1.3806488e-23         // Boltzmann constant m^2.kg.s^{-2}.K^{-1}
#define AMC_PHYSICS_8PLUSPI                     11.141592653589793     // 8 + Pi

#define AMC_PHYSICS_CUNNINGHAM_ALLEN_A 1.257
#define AMC_PHYSICS_CUNNINGHAM_ALLEN_B 0.4
#define AMC_PHYSICS_CUNNINGHAM_ALLEN_C 1.1

#define AMC_PHYSICS_CUNNINGHAM_KASTEN_A 1.249
#define AMC_PHYSICS_CUNNINGHAM_KASTEN_B 0.42
#define AMC_PHYSICS_CUNNINGHAM_KASTEN_C 0.87

#define AMC_PHYSICS_CUNNINGHAM_MILLIKAN_A 0.864
#define AMC_PHYSICS_CUNNINGHAM_MILLIKAN_B 0.29
#define AMC_PHYSICS_CUNNINGHAM_MILLIKAN_C 1.25

// Whether to use a faster computation.
#define AMC_PHYSICS_WITH_PARTICLE_KNUDSEN_NUMBER_FAST
#ifdef AMC_PHYSICS_WITH_PARTICLE_KNUDSEN_NUMBER_FAST
#define AMC_PHYSICS_HAS_PARTICLE_KNUDSEN_NUMBER_FAST 1
#else
#define AMC_PHYSICS_HAS_PARTICLE_KNUDSEN_NUMBER_FAST 0
#endif


/*!
 * \file ClassParameterizationPhysics.hxx
 * \brief File containing base physical parameterizations for air variables.
 * \author Edouard Debry
 */

namespace AMC
{
  class ClassParameterizationPhysics
  {
  public:

    typedef AMC_PARAMETERIZATION_REAL real;

    /*!< Compute pure water surface tension in N.m^{-1}.*/
    static real ComputeWaterSurfaceTension(const real &temperature);


    /*!< Compute air free mean path in µm.*/
    static real ComputeAirFreeMeanPath(const real &temperature, const real &pressure);


    /*!< Compute air dynamic viscosity in kg.m^{-1}.s^{-1}.*/
    static real ComputeAirDynamicViscosity(const real &temperature);


    /*!< Compute water saturation vapor pressure in Pascal.*/
    static real ComputeWaterSaturationVaporPressureClausiusClapeyron(const real &temperature);
    static real ComputeWaterSaturationVaporPressurePreining(const real &temperature);

    /*!< Compute sulfuric acid saturation vapor pressure in Pascal.*/
    static real ComputeSulfuricAcidSaturationVaporPressure(const real &temperature);

    /*!< Compute water dynamic viscosity in kg.m^{-1].s^{-1}.*/
    static real ComputeWaterDynamicViscosity(const real &temperature);

    
    /*!< Compute water air density in kg.m^{-3}.*/
    static real ComputeAirDensity(const real &temperature, const real &pressure, const real &relative_humidity);


    /*!< Compute saturation vapor pressure for a different temperature.*/
    static real ComputeSaturationVaporPressure(const real &temperature,
                                               const real saturation_vapor_pressure_reference,
                                               const real &vaporization_enthalpy);


    /*!< Compute saturation vapor concentration for a different temperature.*/
    static real ComputeSaturationVaporConcentration(const real &temperature,
                                                    const real saturation_vapor_concentration_reference,
                                                    const real &vaporization_enthalpy);


    /*!< Compute gas diffusivity.*/
    static real ComputeGasDiffusivity(const real &temperature,
                                      const real &pressure,
                                      const real &collision_factor,
                                      const real &molecular_diameter,
                                      const real &molar_mass);

    /*!< Compute gas quadratic mean velocity.*/
    static real ComputeGasQuadraticMeanVelocity(const real &temperature,
                                                const real &molar_mass);

    /*!< Compute particle slip flow correction.*/
    static real ComputeParticleCunninghamSlipFlowCorrectionAllen(const real &diameter,
                                                                 const real &air_free_mean_path);

    static real ComputeParticleCunninghamSlipFlowCorrectionKasten(const real &diameter,
                                                                  const real &air_free_mean_path);

    static real ComputeParticleCunninghamSlipFlowCorrectionMillikan(const real &diameter,
                                                                    const real &air_free_mean_path);

    static real ComputeParticleCunninghamSlipFlowCorrectionFuchs(const real &diameter,
                                                                 const real &air_free_mean_path);

    /*!< Compute particle diffusivity.*/
#ifndef SWIG
    template<real SlipFlowCorrection(const real &, const real &) >
    static real ComputeParticleDiffusivity(const real &diameter,
                                           const real &temperature,
                                           const real &pressure);
#endif

    static real ComputeParticleDiffusivityAllen(const real &diameter,
                                                const real &temperature,
                                                const real &pressure);

    static real ComputeParticleDiffusivityKasten(const real &diameter,
                                                 const real &temperature,
                                                 const real &pressure);

    static real ComputeParticleDiffusivityMillikan(const real &diameter,
                                                   const real &temperature,
                                                   const real &pressure);

    static real ComputeParticleDiffusivityFuchs(const real &diameter,
                                                const real &temperature,
                                                const real &pressure);


    /*!< Compute gas quadratic mean velocity.*/
    static real ComputeParticleQuadraticMeanVelocity(const real &diameter,
                                                     const real &temperature,
                                                     const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    /*!< Compute particle free mean path.*/
    template<real SlipFlowCorrection(const real &, const real &) >
    static real ComputeParticleFreeMeanPath(const real &diameter,
                                            const real &temperature,
                                            const real &pressure,
                                            const real &density);

    static real ComputeParticleFreeMeanPathAllen(const real &diameter,
                                                 const real &temperature,
                                                 const real &pressure,
                                                 const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleFreeMeanPathKasten(const real &diameter,
                                                  const real &temperature,
                                                  const real &pressure,
                                                  const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleFreeMeanPathMillikan(const real &diameter,
                                                    const real &temperature,
                                                    const real &pressure,
                                                    const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleFreeMeanPathFuchs(const real &diameter,
                                                 const real &temperature,
                                                 const real &pressure,
                                                 const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    /*!< Compute particle Knudsen number.*/
    template<real SlipFlowCorrection(const real &, const real &) >
    static real ComputeParticleKnudsenNumber(const real &diameter1,
                                             const real &diameter2,
                                             const real &temperature,
                                             const real &pressure,
                                             const real &density);

    static real ComputeParticleKnudsenNumberAllen(const real &diameter1,
                                                  const real &diameter2,
                                                  const real &temperature,
                                                  const real &pressure,
                                                  const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleKnudsenNumberKasten(const real &diameter1,
                                                   const real &diameter2,
                                                   const real &temperature,
                                                   const real &pressure,
                                                   const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleKnudsenNumberMillikan(const real &diameter1,
                                                     const real &diameter2,
                                                     const real &temperature,
                                                     const real &pressure,
                                                     const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleKnudsenNumberFuchs(const real &diameter1,
                                                  const real &diameter2,
                                                  const real &temperature,
                                                  const real &pressure,
                                                  const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    /*!< Compute particle terminal velocity.*/
    template<real SlipFlowCorrection(const real &, const real &) >
    static real ComputeParticleTerminalVelocity(const real &diameter,
                                                const real &temperature,
                                                const real &pressure,
                                                const real &density);

    static real ComputeParticleTerminalVelocityAllen(const real &diameter,
                                                     const real &temperature,
                                                     const real &pressure,
                                                     const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleTerminalVelocityKasten(const real &diameter,
                                                      const real &temperature,
                                                      const real &pressure,
                                                      const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleTerminalVelocityMillikan(const real &diameter,
                                                        const real &temperature,
                                                        const real &pressure,
                                                        const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    static real ComputeParticleTerminalVelocityFuchs(const real &diameter,
                                                     const real &temperature,
                                                     const real &pressure,
                                                     const real density = AMC_PHYSICS_PARTICLE_DENSITY);

    /*!< Compute particle dry diameter from wet one.*/
    static real ComputeParticleDryDiameterFromWetDiameter(const real &wet_diameter,
                                                          const real &temperature,
                                                          const real &relative_humidity,
                                                          const real &mole_solute,
                                                          const real &density_solute,
                                                          const real &molar_mass_solute,
                                                          const real water_activity_coefficient = real(1),
                                                          const real relative_tolerance = real(1.e-6),
                                                          const int number_loop = 100);
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_PHYSICS_HXX
#endif
