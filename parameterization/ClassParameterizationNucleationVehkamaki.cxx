// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VEHKAMAKI_CXX

#include "ClassParameterizationNucleationVehkamaki.hxx"

namespace AMC
{
  // Threshold parameters due to validity range of formula.
  inline real ClassParameterizationNucleationVehkamaki::threshold_relative_humidity(const real &relative_humidity) const
  {
    if (relative_humidity > AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MAX)
      return AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MAX;
    else if (relative_humidity < AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MIN)
      return AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MIN;
    else
      return relative_humidity;
  }


  inline real ClassParameterizationNucleationVehkamaki::threshold_temperature(const real &temperature) const
  {
    if (temperature > AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MAX)
      return AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MAX;
    else if (temperature < AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MIN)
      return AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MIN;
    else
      return temperature;
  }


  inline real ClassParameterizationNucleationVehkamaki::
  threshold_concentration_sulfate_molec_cm3(const real &concentration_sulfate_molec_cm3) const
  {
    if (concentration_sulfate_molec_cm3 > AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MAX)
      return AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MAX;
    else if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MIN)
      return AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MIN;
    return concentration_sulfate_molec_cm3;
  }


  // Nucleation rate.
  real ClassParameterizationNucleationVehkamaki::nucleation_rate(const real &concentration_sulfate_molec_cm3_log,
                                                                 const real &sulfuric_acid_mole_fraction_inv,
                                                                 const real &temperature,
                                                                 const real &relative_humidity_log) const
  {
    real a = VEHKAMAKI_POLYNOMIAL_FIT( 0.14309 ,  2.21956   , -0.0273911   ,  0.0000722811,  5.91822  );
    real b = VEHKAMAKI_POLYNOMIAL_FIT( 0.117489,  0.462532  , -0.0118059   ,  0.0000404196, 15.7963   );
    real c = VEHKAMAKI_POLYNOMIAL_FIT(-0.215554, -0.0810269 ,  0.00143581  , -4.7758e-6   , -2.91297  );
    real d = VEHKAMAKI_POLYNOMIAL_FIT(-3.58856 ,  0.049508  , -0.00021382  ,  3.10801e-7  , -0.0293333);
    real e = VEHKAMAKI_POLYNOMIAL_FIT( 1.14598 , -0.600796  ,  0.00864245  , -0.0000228947, -8.44985  );
    real f = VEHKAMAKI_POLYNOMIAL_FIT( 2.15855 ,  0.0808121 , -0.000407382 , -4.01957e-7  ,  0.721326 );
    real g = VEHKAMAKI_POLYNOMIAL_FIT( 1.6241  , -0.0160106 ,  0.0000377124,  3.21794e-8  , -0.0113255);
    real h = VEHKAMAKI_POLYNOMIAL_FIT( 9.71682 , -0.115048  ,  0.000157098 ,  4.00914e-7  ,  0.71186  );
    real i = VEHKAMAKI_POLYNOMIAL_FIT(-1.05611 ,  0.00903378, -0.0000198417,  2.46048e-8  , -0.0579087);
    real j = VEHKAMAKI_POLYNOMIAL_FIT(-0.148712,  0.00283508, -9.24619e-6  ,  5.00427e-9  , -0.0127081);

    return exp(a + relative_humidity_log * (b + relative_humidity_log * (c + relative_humidity_log * d))
               + concentration_sulfate_molec_cm3_log * (e + concentration_sulfate_molec_cm3_log
                                                        * (h + j * concentration_sulfate_molec_cm3_log))
               + relative_humidity_log * concentration_sulfate_molec_cm3_log *
               (f + relative_humidity_log * g + concentration_sulfate_molec_cm3_log * i));
  }


  // Sulfuric acid density.
  real ClassParameterizationNucleationVehkamaki::sulfuric_acid_density(const real &sulfuric_acid_mass_fraction,
                                                                       const real &temperature) const
  {
    const real &xm = sulfuric_acid_mass_fraction;
    real a = SULFATE_DENSITY( 0.7681724  ,  2.1847140  ,  7.163002   , -44.31447    , 88.75606    , -75.73729    , 23.43228   );
    real b = SULFATE_DENSITY( 1.808225e-3, -9.294656e-3, -0.03742148 ,   0.2565321  , -0.5362872  ,   0.4857736  , -0.1629592 );
    real c = SULFATE_DENSITY(-3.478524e-6,  1.335867e-5,  5.195706e-5,  -3.717636e-4,  7.990811e-4,  -7.458060e-4,  2.58139e-4);

    return a + temperature * (b + c * temperature);
  }


  // Compute sulfuric acid mol fraction.
  real ClassParameterizationNucleationVehkamaki::sulfuric_acid_mole_fraction(const real &concentration_sulfate_molec_cm3_log,
                                                                             const real &temperature,
                                                                             const real &relative_humidity_log) const
  {
    return real(0.740997) - real(0.00266379) * temperature
      + (real(-0.00349998) + real(0.0000504022) * temperature) * concentration_sulfate_molec_cm3_log
      + relative_humidity_log * (real(0.00201048) + real(-0.000183289) * temperature
                                 + relative_humidity_log * (real(0.00157407) + real(-0.0000179059) * temperature
                                                            + relative_humidity_log * (real(0.000184403)
                                                                                       + real(-1.50345e-6) * temperature)));
  }


  // Critical cluster radius.
  real ClassParameterizationNucleationVehkamaki::
  critical_cluster_radius(const real &critical_cluster_total_number_molecule,
                          const real &sulfuric_acid_mole_fraction) const
  {
    return exp(real(-1.6524245) + real(0.42316402) * sulfuric_acid_mole_fraction
               + 0.3346648 * log(critical_cluster_total_number_molecule));
  }


  // Total number of molecules of critical cluster.
  real ClassParameterizationNucleationVehkamaki::
  critical_cluster_total_number_molecule(const real &concentration_sulfate_molec_cm3_log,
                                         const real &temperature,
                                         const real &relative_humidity_log,
                                         const real &sulfuric_acid_mole_fraction_inv) const
  {
    real a = VEHKAMAKI_POLYNOMIAL_FIT(-0.00295413 , -0.0976834   ,  0.00102485  , -2.18646e-6 , -0.101717   );
    real b = VEHKAMAKI_POLYNOMIAL_FIT(-0.00205064 , -0.00758504  ,  0.000192654 , -6.7043e-7  , -0.255774   );
    real c = VEHKAMAKI_POLYNOMIAL_FIT( 0.00322308 ,  0.000852637 , -0.0000154757,  5.66661e-8 ,  0.0338444  );
    real d = VEHKAMAKI_POLYNOMIAL_FIT( 0.0474323  , -0.000625104 ,  2.65066e-6  , -3.67471e-9 , -0.000267251);
    real e = VEHKAMAKI_POLYNOMIAL_FIT(-0.0125211  ,  0.00580655  , -0.000101674 ,  2.88195e-7 ,  0.0942243  );
    real f = VEHKAMAKI_POLYNOMIAL_FIT(-0.038546   , -0.000672316 ,  2.60288e-6  ,  1.19416e-8 , -0.00851515 );
    real g = VEHKAMAKI_POLYNOMIAL_FIT(-0.0183749  ,  0.000172072 , -3.71766e-7  , -5.14875e-10,  0.00026866 );
    real h = VEHKAMAKI_POLYNOMIAL_FIT(-0.0619974  ,  0.000906958 , -9.11728e-7  , -5.36796e-9 , -0.00774234 );
    real i = VEHKAMAKI_POLYNOMIAL_FIT( 0.0121827  , -0.00010665  ,  2.5346e-7   , -3.63519e-10,  0.000610065);
    real j = VEHKAMAKI_POLYNOMIAL_FIT( 0.000320184, -0.0000174762,  6.06504e-8  , -1.42177e-11,  0.000135751);

    return exp(a + relative_humidity_log * (b + relative_humidity_log * (c + relative_humidity_log * d))
               + concentration_sulfate_molec_cm3_log * (e + concentration_sulfate_molec_cm3_log
                                                        * (h + j * concentration_sulfate_molec_cm3_log))
               + relative_humidity_log * concentration_sulfate_molec_cm3_log *
               (f + relative_humidity_log * g + concentration_sulfate_molec_cm3_log * i));
  }


  // Constructors.
  ClassParameterizationNucleationVehkamaki::ClassParameterizationNucleationVehkamaki()
    : ClassParameterizationNucleationBase("Vehkamaki")
  {
    // Add water.
    this->species_index_amc_.push_back(-1);
    this->gas_index_amc_.push_back(-1);

    return;
  }


  // Destructor.
  ClassParameterizationNucleationVehkamaki::~ClassParameterizationNucleationVehkamaki()
  {
    return;
  }


  // Clone.
  ClassParameterizationNucleationVehkamaki* ClassParameterizationNucleationVehkamaki::clone() const
  {
    return new ClassParameterizationNucleationVehkamaki(*this);
  }


  // Compute kernel.
  real ClassParameterizationNucleationVehkamaki::ComputeNumberMoleculeTotal(const real &temperature,
                                                                            const real &relative_humidity,
                                                                            const real &concentration_sulfate_molec_cm3) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
#endif

    if (xstar > real(0))
      {
        real sulfuric_acid_mole_fraction_inv = real(1) / xstar;

        return critical_cluster_total_number_molecule(concentration_sulfate_molec_cm3_log,
                                                      temperature_valid,
                                                      relative_humidity_log,
                                                      sulfuric_acid_mole_fraction_inv);
      }

    return real(0);
  }


  real ClassParameterizationNucleationVehkamaki::ComputeSulfuricAcidMoleFraction(const real &temperature,
                                                                            const real &relative_humidity,
                                                                            const real &concentration_sulfate_molec_cm3) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    return sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                       temperature_valid,
                                       relative_humidity_log);
  }


  real ClassParameterizationNucleationVehkamaki::ComputeSulfuricAcidDensity(const real &temperature,
                                                                            const real &relative_humidity,
                                                                            const real &concentration_sulfate_molec_cm3) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

    real sulfuric_acid_mass_fraction = xstar
      / (xstar + (real(1) - xstar) * AMC_NUCLEATION_VEHKAMAKI_RATIO_MOL_WEIGHT_WATER_SULFATE);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
    CBUG << "sulfuric_acid_mass_fraction = " << sulfuric_acid_mass_fraction << endl;
#endif

    return sulfuric_acid_density(sulfuric_acid_mass_fraction, temperature);
  }


  real ClassParameterizationNucleationVehkamaki::ComputeCriticalClusterRadius(const real &temperature,
                                                                              const real &relative_humidity,
                                                                              const real &concentration_sulfate_molec_cm3) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

    real sulfuric_acid_mass_fraction = xstar
      / (xstar + (real(1) - xstar) * AMC_NUCLEATION_VEHKAMAKI_RATIO_MOL_WEIGHT_WATER_SULFATE);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
    CBUG << "sulfuric_acid_mass_fraction = " << sulfuric_acid_mass_fraction << endl;
#endif

    real density_gcm3 = sulfuric_acid_density(sulfuric_acid_mass_fraction, temperature_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "density_gcm3 = " << density_gcm3 << endl;
#endif

    if (xstar > real(0))
      {
        real sulfuric_acid_mole_fraction_inv = real(1) / xstar;

        real ntot = critical_cluster_total_number_molecule(concentration_sulfate_molec_cm3_log,
                                                           temperature_valid,
                                                           relative_humidity_log,
                                                           sulfuric_acid_mole_fraction_inv);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "ntot = " << ntot << endl;
#endif

        if (ntot > AMC_NUCLEATION_VEHKAMAKI_CLUSTER_TOTAL_NUMBER_MOLECULE_MIN)
          return critical_cluster_radius(ntot, xstar);
      }

    return real(0);
  }


  real ClassParameterizationNucleationVehkamaki::ComputeKernel(const real &temperature,
                                                               const real &relative_humidity,
                                                               const real &concentration_sulfate_molec_cm3) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(relative_humidity);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(temperature);

    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
#endif

    if (xstar > real(0))
      {
        real sulfuric_acid_mole_fraction_inv = real(1) / xstar;

        real ntot = critical_cluster_total_number_molecule(concentration_sulfate_molec_cm3_log,
                                                           temperature_valid,
                                                           relative_humidity_log,
                                                           sulfuric_acid_mole_fraction_inv);
#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "ntot = " << ntot << endl;
#endif

        // If too few molecules in cluster, parameterization not valid.
        if (ntot > AMC_NUCLEATION_VEHKAMAKI_CLUSTER_TOTAL_NUMBER_MOLECULE_MIN)
          return nucleation_rate(concentration_sulfate_molec_cm3_log,
                                 sulfuric_acid_mole_fraction_inv,
                                 temperature_valid, relative_humidity_log);
      }

    return real(0);
  }


  void ClassParameterizationNucleationVehkamaki::ComputeKernel(const int &section_index,
                                                               const real *concentration_gas,
                                                               vector1r &rate_aer_number,
                                                               vector1r &rate_aer_mass) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(ClassMeteorologicalData::relative_humidity_);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(ClassMeteorologicalData::temperature_);

    real concentration_sulfate_molec_cm3 = concentration_gas[this->gas_index_amc_[AMC_NUCLEATION_SULFATE_INDEX]]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;
    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    // If gas concentrations are below the minimal value, we consider there is no nucleation
    // instead of clipping the value, which may lead to some overestimation.
    if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MIN) return;

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
#endif

    if (xstar > real(0))
      {
        real sulfuric_acid_mole_fraction_inv = real(1) / xstar;

        // Flux number.
        real flux_number_cm3s = nucleation_rate(concentration_sulfate_molec_cm3_log,
                                                sulfuric_acid_mole_fraction_inv,
                                                temperature_valid,
                                                relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "flux_number_cm3s = " << flux_number_cm3s << endl;
#endif

        // Flux mass.
        real sulfuric_acid_mass_fraction = xstar
          / (xstar + (real(1) - xstar) * AMC_NUCLEATION_VEHKAMAKI_RATIO_MOL_WEIGHT_WATER_SULFATE);

        real density_gcm3 = sulfuric_acid_density(sulfuric_acid_mass_fraction, temperature_valid);

        real ntot = critical_cluster_total_number_molecule(concentration_sulfate_molec_cm3_log,
                                                           temperature_valid,
                                                           relative_humidity_log,
                                                           sulfuric_acid_mole_fraction_inv);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "sulfuric_acid_mass_fraction = " << sulfuric_acid_mass_fraction << endl;
        CBUG << "density_gcm3 = " << density_gcm3 << endl;
        CBUG << "ntot = " << ntot << endl;
#endif

        // If too few molecules in cluster, parameterization not valid.
        if (ntot > AMC_NUCLEATION_VEHKAMAKI_CLUSTER_TOTAL_NUMBER_MOLECULE_MIN)
          {
            real radius_nm = critical_cluster_radius(ntot, xstar);

            // Update rates, do not forget we possibly add other things (coagulation, condensation).
            rate_aer_number[section_index] += flux_number_cm3s * real(1e6);

            // The 8.e-9 factor accounts for all necessary conversion to get µg.m^{-3}.s^{-1} mass flux.
            const int i = section_index * ClassSpecies::Nspecies_;
            rate_aer_mass[i + this->species_index_amc_[AMC_NUCLEATION_SULFATE_INDEX]] += density_gcm3
              * AMC_PI6 * radius_nm * radius_nm * radius_nm
              * sulfuric_acid_mass_fraction
              * flux_number_cm3s * real(8.e-9);
          }
      }
  }


#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  bool ClassParameterizationNucleationVehkamaki::ComputeKernel(const vector1r &concentration_gas,
                                                               real &diameter,
                                                               real &rate_aer_number,
                                                               vector1r &rate_aer_mass) const
  {
    // Threshold inputs due to formula validity range.
    real relative_humidity_valid = threshold_relative_humidity(NPF::ClassMeteoData::relative_humidity_);
    real relative_humidity_log = log(relative_humidity_valid);

    real temperature_valid = threshold_temperature(NPF::ClassMeteoData::temperature_);

    real concentration_sulfate_molec_cm3 = concentration_gas[AMC_NUCLEATION_SULFATE_INDEX]
      * AMC_NUCLEATION_CONVERT_SULFATE_FROM_UGM3_TO_MOLECULE_CM3;
    real concentration_sulfate_molec_cm3_valid = threshold_concentration_sulfate_molec_cm3(concentration_sulfate_molec_cm3);
    real concentration_sulfate_molec_cm3_log = log(concentration_sulfate_molec_cm3_valid);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "temperature_valid = " << temperature_valid << endl;
    CBUG << "relative_humidity_valid = " << relative_humidity_valid << endl;
    CBUG << "concentration_sulfate_molec_cm3_valid = " << concentration_sulfate_molec_cm3_valid << endl;
#endif

    // If gas concentrations are below the minimal value, we consider there is no nucleation
    // instead of clipping the value, which may lead to some overestimation.
    if (concentration_sulfate_molec_cm3 < AMC_NUCLEATION_VEHKAMAKI_CONCENTRATION_SULFATE_MOLEC_CM3_MIN) return false;

    real xstar = sulfuric_acid_mole_fraction(concentration_sulfate_molec_cm3_log,
                                             temperature_valid,
                                             relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
    CBUG << "xstar = " << xstar << endl;
#endif

    if (xstar > real(0))
      {
        real sulfuric_acid_mole_fraction_inv = real(1) / xstar;

        // Flux number.
        real flux_number_cm3s = nucleation_rate(concentration_sulfate_molec_cm3_log,
                                                sulfuric_acid_mole_fraction_inv,
                                                temperature_valid,
                                                relative_humidity_log);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "flux_number_cm3s = " << flux_number_cm3s << endl;
#endif

        // Flux mass.
        real sulfuric_acid_mass_fraction = xstar
          / (xstar + (real(1) - xstar) * AMC_NUCLEATION_VEHKAMAKI_RATIO_MOL_WEIGHT_WATER_SULFATE);

        real density_gcm3 = sulfuric_acid_density(sulfuric_acid_mass_fraction, temperature_valid);

        real ntot = critical_cluster_total_number_molecule(concentration_sulfate_molec_cm3_log,
                                                           temperature_valid,
                                                           relative_humidity_log,
                                                           sulfuric_acid_mole_fraction_inv);

#ifdef AMC_WITH_DEBUG_VEHKAMAKI
        CBUG << "sulfuric_acid_mass_fraction = " << sulfuric_acid_mass_fraction << endl;
        CBUG << "density_gcm3 = " << density_gcm3 << endl;
        CBUG << "ntot = " << ntot << endl;
#endif

        // If too few molecules in cluster, parameterization not valid.
        if (ntot > AMC_NUCLEATION_VEHKAMAKI_CLUSTER_TOTAL_NUMBER_MOLECULE_MIN)
          {
            real radius_nm = critical_cluster_radius(ntot, xstar);

            // Update rates.
            rate_aer_number = flux_number_cm3s * real(1e6);

            // The 1.e-9 factor accounts for all necessary conversion to get µg.m^{-3}.s^{-1} mass flux.
            real flux_mass = density_gcm3
              * AMC_PI6 * radius_nm * radius_nm * radius_nm
              * flux_number_cm3s * real(8.e-9);

            rate_aer_mass[AMC_NUCLEATION_SULFATE_INDEX] = flux_mass * sulfuric_acid_mass_fraction;

            real water_mass_fraction = real(1) - sulfuric_acid_mass_fraction;
            rate_aer_mass[this->index_npf_[ClassParameterizationNucleationBase::Nspecies_]] =
              flux_mass * (water_mass_fraction > real(0) ? water_mass_fraction : real(0));

            // Particle diameter.
            diameter = radius_nm * real(2.e-3);

            return true;
          }
      }

    return false;
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VEHKAMAKI_CXX
#endif
