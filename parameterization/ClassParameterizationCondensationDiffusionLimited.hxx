// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_HXX

#define AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MIN  0.01
#define AMC_CONDENSATION_DIFFUSION_LIMITED_KNUDSEN_MAX  10.0
#define AMC_CONDENSATION_DIFFUSION_LIMITED_PI_DIV4_Em12 0.7853981633974483e-12
#define AMC_CONDENSATION_DIFFUSION_LIMITED_2_PI_Em6     6.283185307179586e-6

namespace AMC
{
  template<class FC, class FW, class FD>
  class ClassParameterizationCondensationDiffusionLimited : public ClassParameterizationCondensationBase
  {

  protected:

    /*!< Knudsen minimum and maximum.*/
    real knudsen_min_, knudsen_max_;

    /*<! Correction factor.*/
    FC* correction_factor_;

    /*<! Flux correction when particle is liquid.*/
    FW* flux_correction_aqueous_;

    /*<! Flux correction when particle is dry.*/
    FD* flux_correction_dry_;

  public:

    /*!< Constructors.*/
    ClassParameterizationCondensationDiffusionLimited(Ops::Ops &ops, string name = "DiffusionLimited");

    /*!< Destructor.*/
    ~ClassParameterizationCondensationDiffusionLimited();

    /*!< Clone.*/
    ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>* clone() const;

    /*!< Get methods.*/
    FC* GetCorrectionFactor() const;
    FW* GetFluxCorrectionAqueous() const;
    FD* GetFluxCorrectionDry() const;
    

    /*!< Compute coefficient.*/
    real ComputeCoefficient(const real &diameter,
                            const real &velocity,
                            const real &diffusivity,
                            const real &accomodation) const;

    /*!< Compute coefficient.*/
    virtual void ComputeCoefficient(const int section_min,
                                    const int section_max,
                                    const vector1i &parameterization_section,
                                    const real *concentration_aer_number,
                                    vector1r &condensation_coefficient) const;

    /*!< Compute kernel.*/
    virtual void ComputeKernel(const int section_min,
                               const int section_max,
                               const vector1i &parameterization_section,
                               const real *concentration_aer_number,
                               const real *concentration_aer_mass,
                               const real *concentration_gas,
                               vector1r &condensation_coefficient,
                               vector1r &rate_aer_mass) const;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    /*!< Compute coefficient for NPF.*/
    virtual void ComputeCoefficient(vector2r &coefficient) const;
#endif

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    virtual void _TestCoefficient_(const real &temperature,
                                   const real &pressure,
                                   const real &diameter,
                                   vector<real> &condensation_coefficient) const;

    virtual void _Test_(const real &temperature,
                        const real &pressure,
                        const real &diameter,
                        const real &density,
                        const vector<real> &concentration_gas,
                        const vector<real> &concentration_aer_mass,
                        const vector<real> &equilibrium_aer_internal,
                        vector<real> &equilibrium_gas_surface,
                        vector<real> &rate_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_HXX
#endif
