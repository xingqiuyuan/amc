// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION2_CXX

#include "ClassCondensationFluxCorrectionAqueousVersion2.hxx"

#define H2SO4 0
#define NH3   1
#define HNO3  2
#define HCl   3

namespace AMC
{
  // Constructors.
  ClassCondensationFluxCorrectionAqueousVersion2::ClassCondensationFluxCorrectionAqueousVersion2(Ops::Ops &ops)
    : ClassCondensationFluxCorrectionAqueousBase(ops, "Version2")
  {
    return;
  }


  // Destructor.
  ClassCondensationFluxCorrectionAqueousVersion2::~ClassCondensationFluxCorrectionAqueousVersion2()
  {
    return;
  }


  // Compute equilibrium gas correction thanks to ion hydronium flux limitation.
  void ClassCondensationFluxCorrectionAqueousVersion2::ComputeFluxCorrection(const real *equilibrium_aer_internal,
                                                                             const real *concentration_gas,
                                                                             const vector1r &coefficient,
                                                                             real *equilibrium_gas_surface,
                                                                             vector1r &rate_aer_mass) const
  {
    vector1r coefficient_molar(AMC_FLUX_CORRECTION_AQUEOUS_NGAS);
    vector1r rate_aer_molar(AMC_FLUX_CORRECTION_AQUEOUS_NGAS);

    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      {
        const int &j = this->index_[i];
        real molar_mass_inverse = real(1) / this->molar_mass_[i];
        coefficient_molar[i] = coefficient[j] * molar_mass_inverse;
        rate_aer_molar[i] = rate_aer_mass[j] * molar_mass_inverse;
      }

    // Hydronium mol concentration.
    real hydronium_molar = equilibrium_aer_internal[this->index_hydronium_]
      / AMC_FLUX_CORRECTION_AQUEOUS_HYDRONIUM_MOLAR_MASS;

    real hydronium_molar_threshold = hydronium_molar * this->limitation_hydronium_;

    real electroneutrality = real(2) * rate_aer_molar[H2SO4]
      + rate_aer_molar[HNO3] + rate_aer_molar[HCl] - rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
    cout << "hydronium_molar = " << hydronium_molar << endl;
    cout << "hydronium_molar_threshold = " << hydronium_molar_threshold << endl;
    cout << "electroneutrality = " << electroneutrality << endl;
#endif

    // Perform correction directly on single rates.
    if (electroneutrality < - hydronium_molar_threshold)
      {
        // If electroneutrality exceeds the lower bound,
        // this can only be due to HNO3 or HCl evaporation.
        if (rate_aer_molar[HNO3] < real(0) &&
            rate_aer_molar[HCl] < real(0))
          {
            // Does the remaining flux already exceeds the limit ?
            real exceed = - hydronium_molar_threshold
              - real(2) * rate_aer_molar[H2SO4]
              + rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
            cout << "exceed1 = " << exceed << endl;
#endif

            if (exceed == real(0))
              {
                // If exactly the limit, then set HNO3 and HCl evaporation to = 0.
                rate_aer_molar[HNO3] = real(0);
                rate_aer_molar[HCl] = real(0);
              }
            else
              {
                // This correction intends to bring the electroneutrality to its limit.
                real correction = (rate_aer_molar[HNO3]
                                   + rate_aer_molar[HCl]) / exceed;

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
                cout << "correction1 = " << correction << endl;
#endif

                if (correction >= real(1))
                  {
                    // If correction is greater than 1, this means the remaining flux is
                    // inferior (in term of abs value) to the limit, so we let
                    // HNO3 and HCl evaporate up to this limit.
                    rate_aer_molar[HNO3] /= correction;
                    rate_aer_molar[HCl] /= correction;
                  }
                else
                  {
                    // If the remaining flux is superior (in term of abs value) to the limit,
                    // there is no hope to bring the hydronium flux to its limit.
                    rate_aer_molar[HNO3] = real(0);
                    rate_aer_molar[HCl] = real(0);
                  }
              }
          }
        else if (rate_aer_molar[HNO3] < real(0))
          {
            // We do here exactly the same things as above,
            // except that HCl now condenses and is included in the remaining flux.
            real exceed = - hydronium_molar_threshold
              - real(2) * rate_aer_molar[H2SO4]
              - rate_aer_molar[HCl] + rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
            cout << "exceed2 = " << exceed << endl;
#endif

            if (exceed == real(0))
              rate_aer_molar[HNO3] = real(0);
            else
              {
                // This correction intends to bring the electroneutrality to its limit.
                real correction = rate_aer_molar[HNO3] / exceed;

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
                cout << "correction2 = " << correction << endl;
#endif

                if (correction >= real(1))
                  rate_aer_molar[HNO3] /= correction;
                else
                  rate_aer_molar[HNO3] = real(0);
              }
          }
        else if (rate_aer_molar[HCl] < real(0))
          {
            // Again, same fault ... same sentence.

            real exceed = - hydronium_molar_threshold
              - real(2) * rate_aer_molar[H2SO4]
              - rate_aer_molar[HNO3] + rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
            cout << "exceed3 = " << exceed << endl;
#endif

            if (exceed == real(0))
              rate_aer_molar[HCl] = real(0);
            else
              {
                // This correction intends to bring the electroneutrality to its limit.
                real correction = rate_aer_molar[HCl] / exceed;

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
                cout << "correction3 = " << correction << endl;
#endif

                if (correction >= real(1))
                  rate_aer_molar[HCl] /= correction;
                else
                  rate_aer_molar[HCl] = real(0);
              }
          }
      }
    else if (electroneutrality > hydronium_molar_threshold &&
             rate_aer_molar[NH3] < real(0))
      {
        // If electroneutrality exceeds the upper bound,
        // it is a problem only if due to NH3 evaporation.

        real exceed = real(2) * rate_aer_molar[H2SO4]
          + rate_aer_molar[HNO3] + rate_aer_molar[HCl]
          - hydronium_molar_threshold;

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
        cout << "exceed4 = " << exceed << endl;
#endif

        if (exceed == real(0))
          rate_aer_molar[NH3] = real(0);
        else
          {
            // This correction intends to bring the electroneutrality to its limit.
            real correction = rate_aer_molar[NH3] / exceed;

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
            cout << "correction4 = " << correction << endl;
#endif

            if (correction >= real(1))
              rate_aer_molar[NH3] /= correction;
            else
              rate_aer_molar[NH3] = real(0);
          }
      }

    // Recompute electroneutrality and treat cases when correction < 1.
    electroneutrality = real(2) * rate_aer_molar[H2SO4] + rate_aer_molar[HNO3]
      + rate_aer_molar[HCl] - rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
    cout << "electroneutrality_new = " << electroneutrality << endl;
    cout << "hydronium_molar_threshold = " << hydronium_molar_threshold << endl;
#endif

    if (electroneutrality < - hydronium_molar_threshold)
      {
        // From this previous case, if the remaining flux is already superior (as absolute value)
        // to the threshold it may be due to NH3 condensation, so we limit it.

        // Maximum ammonia rate due to condensation.
        real rate_max_ammonia_molar1 = coefficient_molar[NH3]
          * concentration_gas[this->index_[NH3]];

        // Maximum ammonia rate allowed thanks to limitation.
        real rate_max_ammonia_molar2 =  real(2) * rate_aer_molar[H2SO4]
          + rate_aer_molar[HNO3] + rate_aer_molar[HCl]
          + hydronium_molar_threshold;

        // Set the ammonia rate to the min of previous max.
        rate_aer_molar[NH3] =
          rate_max_ammonia_molar1 <= rate_max_ammonia_molar2 ? rate_max_ammonia_molar1 : rate_max_ammonia_molar2;
      }
    else if (electroneutrality > hydronium_molar_threshold)
      {
        // Maximum nitrate and chloride rate due to condensation.
        real rate_max_nitrate_molar1 = coefficient_molar[HNO3]
          * concentration_gas[this->index_[HNO3]];
        real rate_max_chloride_molar1 = coefficient_molar[HCl]
          * concentration_gas[this->index_[HCl]];

        if (rate_aer_molar[HNO3] > real(0) &&
            rate_aer_molar[HCl] > real(0))
          {
            real flux_ratio = rate_aer_molar[HCl]
              / (rate_aer_molar[HCl] + rate_aer_molar[HNO3]);

            // Maximum nitrate and chloride rate allowed thanks to limitation.
            real rate_max_nitrate_molar2 = (real(1) - flux_ratio)
              * (hydronium_molar_threshold - real(2) * rate_aer_molar[H2SO4]
                 + rate_aer_molar[NH3]);

            real rate_max_chloride_molar2 = flux_ratio
              * (hydronium_molar_threshold - real(2) * rate_aer_molar[H2SO4]
                 + rate_aer_molar[NH3]);

            rate_aer_molar[HNO3] =
              rate_max_nitrate_molar1 <= rate_max_nitrate_molar2 ? rate_max_nitrate_molar1 : rate_max_nitrate_molar2;
            rate_aer_molar[HCl] =
              rate_max_chloride_molar1 <= rate_max_chloride_molar2 ? rate_max_chloride_molar1 : rate_max_chloride_molar2;
          }
        else if (rate_aer_molar[HNO3] > real(0))
          {
            // Maximum nitrate and chloride rate allowed thanks to limitation.
            real rate_max_nitrate_molar2 = hydronium_molar_threshold
              - real(2) * rate_aer_molar[H2SO4] + rate_aer_molar[NH3];

            rate_aer_molar[HNO3] =
              rate_max_nitrate_molar1 <= rate_max_nitrate_molar2 ? rate_max_nitrate_molar1 : rate_max_nitrate_molar2;
          }
        else if (rate_aer_molar[HCl] > real(0))
          {
            real rate_max_chloride_molar2 = hydronium_molar_threshold
              - real(2) * rate_aer_molar[H2SO4] + rate_aer_molar[NH3];

            rate_aer_molar[HCl] =
              rate_max_chloride_molar1 <= rate_max_chloride_molar2 ? rate_max_chloride_molar1 : rate_max_chloride_molar2;
          }
      }

    // Give back fluxes and correct the equilibrium gas surface according to the rate correction.
    int index = this->index_[NH3];
    rate_aer_mass[index] = rate_aer_molar[NH3] * this->molar_mass_[NH3];
    equilibrium_gas_surface[index] = concentration_gas[index] - rate_aer_mass[index] / coefficient[index];

    index = this->index_[HCl];
    rate_aer_mass[index] = rate_aer_molar[HCl] * this->molar_mass_[HCl];
    equilibrium_gas_surface[index] = concentration_gas[index] - rate_aer_mass[index] / coefficient[index];

    index = this->index_[HNO3];
    rate_aer_mass[index] = rate_aer_molar[HNO3] * this->molar_mass_[HNO3];
    equilibrium_gas_surface[index] = concentration_gas[index] - rate_aer_mass[index] / coefficient[index];
  }
}

#undef H2SO4
#undef NH3
#undef HNO3
#undef HCl

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION2_CXX
#endif
