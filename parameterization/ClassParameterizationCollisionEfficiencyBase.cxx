// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_BASE_CXX

#include "ClassParameterizationCollisionEfficiencyBase.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCollisionEfficiencyBase::ClassParameterizationCollisionEfficiencyBase(const string name)
    : name_(name)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Instantiate collision efficiency parameterization \""
                         << name_ << "\"." << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationCollisionEfficiencyBase::~ClassParameterizationCollisionEfficiencyBase()
  {
    return;
  }


  // Clone.
  ClassParameterizationCollisionEfficiencyBase* ClassParameterizationCollisionEfficiencyBase::clone() const
  {
    return new ClassParameterizationCollisionEfficiencyBase(*this);
  }


  // Get methods.
  string ClassParameterizationCollisionEfficiencyBase::GetName() const
  {
    return name_;
  }


  // Compute collision efficiency.
  real ClassParameterizationCollisionEfficiencyBase::Compute(const real &diameter1,
                                                             const real &diameter2,
                                                             const real &slip_flow_correction1,
                                                             const real &slip_flow_correction2,
                                                             const real &terminal_velocity1,
                                                             const real &terminal_velocity2,
                                                             const real &temperature,
                                                             const real &air_free_mean_path,
                                                             const real &air_density,
                                                             const real &air_dynamic_viscosity,
                                                             const real &water_dynamic_viscosity) const
  {
    return real(1);
  }


#ifdef AMC_WITH_TEST
  // Test.
  real ClassParameterizationCollisionEfficiencyBase::_Test_(const vector<real> &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];
    real relative_humidity = variable[4];

    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);
    real air_density = ClassParameterizationPhysics::ComputeAirDensity(temperature, pressure, relative_humidity);
    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);
    real water_dynamic_viscosity = ClassParameterizationPhysics::ComputeWaterDynamicViscosity(temperature);

    // Particle mass in µg.
    real mass1 = AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3 * AMC_PI6
      * AMC_COAGULATION_PARTICLE_DENSITY_DEFAULT * diameter1 * diameter1 * diameter1;
    real mass2 = AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3 * AMC_PI6
      * AMC_COAGULATION_PARTICLE_DENSITY_DEFAULT * diameter2 * diameter2 * diameter2;

    // Slip flow correction
    real slip_flow_correction1 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionFuchs(diameter1, air_free_mean_path);
    real slip_flow_correction2 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionFuchs(diameter2, air_free_mean_path);

    // Terminal velocity in m.s^{-1}. G / (3 * Pi) * 1.e-3
    real terminal_velocity1 = 1.0408733278209956e-3 * mass1 * slip_flow_correction1 / (diameter1 * air_dynamic_viscosity);
    real terminal_velocity2 = 1.0408733278209956e-3 * mass2 * slip_flow_correction2 / (diameter2 * air_dynamic_viscosity);

    return this->Compute(diameter1, diameter2,
                         slip_flow_correction1, slip_flow_correction2,
                         terminal_velocity1, terminal_velocity2,
                         temperature, air_free_mean_path,
                         air_density, air_dynamic_viscosity,
                         water_dynamic_viscosity);
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_BASE_CXX
#endif
