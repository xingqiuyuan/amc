power_law = {
   sihto_2006 = {
      activation = {K = 1.7e-6, P = 1.0},
      kinetic = {K = 6.e-13, P = 2.0}
   },

   erupe_2010 = {
      apr_23_2009 = {P = 1.0, logK = -5.4},
      apr_25_2009 = {P = 1.3, logK = -7.2},
      apr_26_2009 = {P = 0.6, logK = -2.8},
      may_3_2009 = {P = 1.9, logK = -11.6},
      may_9_2009 = {P = 1.3, logK = -7.9},
      aug_23_2008 = {P = 2.1, logK = -13.2},
      aug_24_2008 = {P = 1.4, logK = -8.7},
      aug_25_2009 = {P = 1.3, logK = -9.1},
      sep_3_2008 = {P = 2.3, logK = -15.7},
      nov_26_2009 = {P = 1.4, logK = -7.6},
      nov_28_2009 = {P = 0.8, logK = -5.6}
   },

   kuang_2008 = {
      -- Values for P fixed to 2.
      tecamac = {logK = -12.2, P = 2.0},
      atlanta = {logK = -13.8, P = 2.0},
      boulder = {logK = -13.4, P = 2.0},
      hyytiala = {logK = -12.4, P = 2.0},
      idaho_hill = {logK = -10.8, P = 2.0},
      mauna_loa = {logK = -12.3, P = 2.0},
      macquarie_island = {logK = -14.0, P = 2.0},

      -- Values for P unconstrained.
      tecamac_unconstrained = {logK = -12.2, P = 1.99},
      atlanta_unconstrained = {logK = -13.9, P = 2.01},
      boulder_unconstrained = {logK = -13.3, P = 1.98},
      hyytiala_unconstrained = {logK = -12.3, P = 1.99},
      idaho_hill_unconstrained = {logK = -11.0, P = 2.04},
      mauna_loa_unconstrained = {logK = -12.3, P = 2.0},
      --macquarie_island_unconstrained_max = {logK = -1.4, P = 0.06},
      --macquarie_island_unconstrained_mean = {logK = -14.0, P = 2.0},
      --macquarie_island_unconstrained_min = {logK = -26.6, P = 3.94}
   }
}