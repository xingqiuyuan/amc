// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VOID_CXX

#include "ClassParameterizationNucleationVoid.hxx"

namespace AMC
{
  // Constructor.
  ClassParameterizationNucleationVoid::ClassParameterizationNucleationVoid()
    : ClassParameterizationNucleationBase("Void")
  {
    return;
  }


  // Destructor.
  ClassParameterizationNucleationVoid::~ClassParameterizationNucleationVoid()
  {
    return;
  }


  // Clone.
  ClassParameterizationNucleationVoid* ClassParameterizationNucleationVoid::clone() const
  {
    return new ClassParameterizationNucleationVoid(*this);
  }


  // Compute kernel.
  void ClassParameterizationNucleationVoid::ComputeKernel(const int &section_index,
                                                          const real *concentration_gas,
                                                          vector1r &rate_aer_number,
                                                          vector1r &rate_aer_mass) const
  {
  }


  bool ClassParameterizationNucleationVoid::ComputeKernel(const vector1r &concentration_gas,
                                                          real &diameter,
                                                          real &rate_aer_number,
                                                          vector1r &rate_aer_mass) const
  {
    diameter = real(0);
    rate_aer_number = real(0);
    rate_aer_mass.assign(int(rate_aer_mass.size()), real(0));

    return false;
  }
}
#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_VOID_CXX
#endif
