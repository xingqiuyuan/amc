// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION1_CXX

#include "ClassCondensationFluxCorrectionAqueousVersion1.hxx"

#define H2SO4 0
#define NH3   1
#define HNO3  2
#define HCl   3

namespace AMC
{
  // Constructors.
  ClassCondensationFluxCorrectionAqueousVersion1::ClassCondensationFluxCorrectionAqueousVersion1(Ops::Ops &ops)
    : ClassCondensationFluxCorrectionAqueousBase(ops, "Version1")
  {
    return;
  }


  // Destructor.
  ClassCondensationFluxCorrectionAqueousVersion1::~ClassCondensationFluxCorrectionAqueousVersion1()
  {
    return;
  }


  // Compute equilibrium gas correction thanks to ion hydronium flux limitation.
  void ClassCondensationFluxCorrectionAqueousVersion1::ComputeFluxCorrection(const real *equilibrium_aer_internal,
                                                                             const real *concentration_gas,
                                                                             const vector1r &coefficient,
                                                                             real *equilibrium_gas_surface,
                                                                             vector1r &rate_aer_mass) const
  {
    vector1r coefficient_molar(AMC_FLUX_CORRECTION_AQUEOUS_NGAS);
    vector1r rate_aer_molar(AMC_FLUX_CORRECTION_AQUEOUS_NGAS);

    for (int i = 0; i < AMC_FLUX_CORRECTION_AQUEOUS_NGAS; i++)
      {
        const int &j = this->index_[i];
        real molar_mass_inverse = real(1) / this->molar_mass_[i];
        coefficient_molar[i] = coefficient[j] * molar_mass_inverse;
        rate_aer_molar[i] = rate_aer_mass[j] * molar_mass_inverse;
      }

    // Hydronium mol concentration.
    real hydronium_molar = equilibrium_aer_internal[this->index_hydronium_]
      / AMC_FLUX_CORRECTION_AQUEOUS_HYDRONIUM_MOLAR_MASS;

    real hydronium_molar_threshold = hydronium_molar * this->limitation_hydronium_;

    real electroneutrality = real(2) * rate_aer_molar[H2SO4]
      + rate_aer_molar[HNO3] + rate_aer_molar[HCl] - rate_aer_molar[NH3];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
    cout << "hydronium_molar = " << hydronium_molar << endl;
    cout << "hydronium_molar_threshold = " << hydronium_molar_threshold << endl;
    cout << "electroneutrality = " << electroneutrality << endl;
#endif

    // Equilibrium gas correction computation.
    // Default value, if nothing else returned before.
    real equilibrium_gas_correction(real(0));

    if (abs(electroneutrality) > hydronium_molar_threshold)
      {
        // Gives hydronium_threshold the sign of electroneutrality.
        if (electroneutrality < real(0))
          hydronium_molar_threshold *= real(-1);

        real a = coefficient_molar[HNO3]
          * equilibrium_gas_surface[this->index_[HNO3]]
          + coefficient_molar[HCl]
          * equilibrium_gas_surface[this->index_[HCl]];

        real b = hydronium_molar_threshold
          - real(2) * coefficient_molar[H2SO4]
          * concentration_gas[this->index_[H2SO4]]
          - coefficient_molar[HNO3] * concentration_gas[this->index_[HNO3]]
          - coefficient_molar[HCl] * concentration_gas[this->index_[HCl]]
          + coefficient_molar[NH3] * concentration_gas[this->index_[NH3]];

        real c = - coefficient_molar[NH3]
          * equilibrium_gas_surface[this->index_[NH3]];

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
        cout << "a = " << a << endl;
        cout << "b = " << b << endl;
        cout << "c = " << c << endl;
#endif

        // a is always positive but may be 0.
        if (a > real(0))
          {
            real delta = b * b - real(4) * a * c;
            real q = - real(0.5) * (b + (b >=0 ? real(1) : real(-1)) * sqrt(delta));

            // In this case, the equiibrium gas correction is the positive root.
            real root = q / a;
            if (root < real(0))
              root = c / q;
            equilibrium_gas_correction = root;
          }
        else // c is always negative, but may be null.
          if (c != real(0))
            equilibrium_gas_correction = - c / b;

        // The equilibrium gas correction is applied if only upper
        // calculation changed the correction to a strictly positive
        // value, otherwise it is considered as a non stiff case.

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_AQUEOUS
        cout << "equilibrium_gas_correction = " << equilibrium_gas_correction << endl;
#endif

        if (equilibrium_gas_correction > real(0))
          {
            int index = this->index_[NH3];
            equilibrium_gas_surface[index] /= equilibrium_gas_correction;
            rate_aer_mass[index] = coefficient[index] * (concentration_gas[index] - equilibrium_gas_surface[index]);

            index = this->index_[HNO3];
            equilibrium_gas_surface[index] *= equilibrium_gas_correction;
            rate_aer_mass[index] = coefficient[index] * (concentration_gas[index] - equilibrium_gas_surface[index]);

            index = this->index_[HCl];
            equilibrium_gas_surface[index] *= equilibrium_gas_correction;
            rate_aer_mass[index] = coefficient[index] * (concentration_gas[index] - equilibrium_gas_surface[index]);
          }
      }
  }
}

#undef H2SO4
#undef NH3
#undef HNO3
#undef HCl

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_AQUEOUS_VERSION1_CXX
#endif
