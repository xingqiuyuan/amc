// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_POWER_LAW_HXX

#define AMC_NUCLEATION_POWER_LAW_CONCENTRATION_SULFATE_MIN_MOLEC_CM3 1.e4
#define AMC_NUCLEATION_POWER_LAW_CONCENTRATION_SULFATE_MAX_MOLEC_CM3 1.e12

#define AMC_NUCLEATION_POWER_LAW_DIAMETER_DEFAULT 0.001
#define AMC_NUCLEATION_POWER_LAW_FRACTION_SULFATE_DEFAULT 0.7

namespace AMC
{
  class ClassParameterizationNucleationPowerLaw : public ClassParameterizationNucleationBase
  {
  private:

    /*!< Whether parameter P is an integer, caracteristic of activation (=1) or kinetic (=2).*/
    bool P_is_activation_, P_is_kinetic_;

    /*!< Nucleation diameter.*/
    real diameter_;

    /*!< Density of nucleated particle.*/
    real density_;

    /*!< Mass of nucleated particle : working variable.*/
    real mass_;

    /*!< K parameter.*/
    real K_;

    /*!< P parameter.*/
    real P_;

    /*!< Mass fraction of species in nucleated particle (mostly sulfate).*/
    vector1r species_fraction_;

    /*!< Configuration file.*/
    static string configuration_file_;

  public:

    /*!< Get available parameterization.*/
    static int GetAvailableParameter(vector<string> &name,
                                     vector<real> &P, vector<real> &K, vector<real> &logK,
                                     string configuration_file = "");

    /*!< Get configuration file.*/
    static string GetConfigurationFile();

    /*!< Constructors.*/
    ClassParameterizationNucleationPowerLaw();
    ClassParameterizationNucleationPowerLaw(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationNucleationPowerLaw();

    /*!< Clone.*/
    ClassParameterizationNucleationPowerLaw* clone() const;

    /*!< Get parameters.*/
    void GetGetSpeciesFraction(vector<real> &species_fraction) const;
    real GetDiameter() const;
    real GetDensity() const;
    real GetK() const;
    real GetLogK() const;
    real GetP() const;

    /*!< Compute kernel.*/
    real ComputeKernel(const real &concentration_sulfate_molec_cm3) const;

#ifndef SWIG
    void ComputeKernel(const int &section_index,
                       const real *concentration_gas,
                       vector1r &rate_aer_number,
                       vector1r &rate_aer_mass) const;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    bool ComputeKernel(const vector1r &concentration_gas,
                       real &diameter,
                       real &rate_aer_number,
                       vector1r &rate_aer_mass) const;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_POWER_LAW_HXX
#endif
