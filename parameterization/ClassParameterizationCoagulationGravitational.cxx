// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_GRAVITATIONAL_CXX

#include "ClassParameterizationCoagulationGravitational.hxx"

namespace AMC
{
  // Constructors.
  template<class C>
  ClassParameterizationCoagulationGravitational<C>::ClassParameterizationCoagulationGravitational()
    : ClassParameterizationCoagulationBase("Gravitational", 3)
  {
    // Collision efficiency parameterization.
    collision_efficiency_ = new collision_efficiency_parameterization();
    this->density_fixed_ *= AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

    this->name_ += collision_efficiency_->GetName();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Gravitational coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Collision efficiency parameterization = "
                         << collision_efficiency_->GetName() << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "density_fixed = " << this->density_fixed_ << " ug/µm3" << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";
    this->variable_name_[4] = "relative_humidity";

    return;
  }


  template<class C>
  ClassParameterizationCoagulationGravitational<C>::ClassParameterizationCoagulationGravitational(Ops::Ops &ops)
    : ClassParameterizationCoagulationBase("Gravitational", 3)
  {
    // Collision efficiency parameterization.
    collision_efficiency_ = new collision_efficiency_parameterization();
    this->density_fixed_ =  ops.Get<real>("density_fixed", "", this->density_fixed_)
      * AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

    this->name_ += collision_efficiency_->GetName();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Gravitational coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Collision efficiency parameterization = "
                         << collision_efficiency_->GetName() << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "density_fixed = " << this->density_fixed_ << " ug/µm3" << endl;
#endif

    this->variable_name_[2] = "temperature";
    this->variable_name_[3] = "pressure";
    this->variable_name_[4] = "relative_humidity";

    return;
  }


  // Destructor.
  template<class C>
  ClassParameterizationCoagulationGravitational<C>::~ClassParameterizationCoagulationGravitational()
  {
    delete collision_efficiency_;
    collision_efficiency_ = NULL;

    return;
  }


  // Clone.
  template<class C>
  ClassParameterizationCoagulationGravitational<C>* ClassParameterizationCoagulationGravitational<C>::clone() const
  {
    return new ClassParameterizationCoagulationGravitational<C>(*this);
  }


  // Copy.
  template<class C>
  void ClassParameterizationCoagulationGravitational<C>::Copy(const ClassParameterizationCoagulationBase *param)
  {
    collision_efficiency_ = reinterpret_cast<const ClassParameterizationCoagulationGravitational<C>*>(param)->collision_efficiency_->clone();
    ClassParameterizationCoagulationBase::Copy(*param);
  }


  // Get collision efficiency parameterization.
  template<class C>
  C* ClassParameterizationCoagulationGravitational<C>::GetCollisionEfficiencyParameterizationPtr()
  {
    return collision_efficiency_;
  }


  // Compute gravitational kernel.
  template<class C>
  real ClassParameterizationCoagulationGravitational<C>::Kernel(const real &diameter1, const real &diameter2,
                                                                const real &mass1, const real &mass2,
                                                                const real &temperature,
                                                                const real &air_free_mean_path,
                                                                const real &air_density,
                                                                const real &air_dynamic_viscosity,
                                                                const real &water_dynamic_viscosity) const
  {
#ifdef AMC_COAGULATION_GRAVITATIONAL_CUNNINGHAM_WITH_FUCHS
    real slip_flow_correction1 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionFuchs(diameter1, air_free_mean_path);
    real slip_flow_correction2 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionFuchs(diameter2, air_free_mean_path);
#elif AMC_COAGULATION_GRAVITATIONAL_CUNNINGHAM_WITH_ALLEN
    real slip_flow_correction1 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionAllen(diameter1, air_free_mean_path);
    real slip_flow_correction2 = ClassParameterizationPhysics::
      ComputeParticleCunninghamSlipFlowCorrectionAllend(diameter2, air_free_mean_path);
#else
    real slip_flow_correction1 = real(1);
    real slip_flow_correction2 = real(1);
#endif

    // G * 1.e-3 / (3 * Pi)
    real terminal_velocity1 = 1.0408733278209956e-3 * mass1 * slip_flow_correction1 / (diameter1 * air_dynamic_viscosity);
    real terminal_velocity2 = 1.0408733278209956e-3 * mass2 * slip_flow_correction2 / (diameter2 * air_dynamic_viscosity);

    real collision_efficiency =
      collision_efficiency_->Compute(diameter1, diameter2,
                                     slip_flow_correction1, slip_flow_correction2,
                                     terminal_velocity1, terminal_velocity2,
                                     temperature,
                                     air_free_mean_path,
                                     air_density,     
                                     air_dynamic_viscosity,
                                     water_dynamic_viscosity);

    real diameter_sum = diameter1 + diameter2;

    // Pi / 4 * 1.e-12
    return 7.853981633974483e-13 * collision_efficiency * diameter_sum
      * diameter_sum * abs(terminal_velocity1 - terminal_velocity2);
  }


  // Compute kernel for tabulation.
  template<class C>
  real ClassParameterizationCoagulationGravitational<C>::KernelTable(const vector1r &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];
    real relative_humidity = variable[4];

    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);
    real air_density = ClassParameterizationPhysics::ComputeAirDensity(temperature, pressure, relative_humidity);
    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);
    real water_dynamic_viscosity = ClassParameterizationPhysics::ComputeWaterDynamicViscosity(temperature);

    // Particle mass in µg.
    real mass1 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter1 * diameter1 * diameter1;
    real mass2 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter2 * diameter2 * diameter2;

    return Kernel(diameter1, diameter2, mass1, mass2, temperature,
                  air_free_mean_path, air_density,
                  air_dynamic_viscosity, water_dynamic_viscosity);
  }


  // Compute kernel for AMC.
  template<class C>
  void ClassParameterizationCoagulationGravitational<C>::ComputeKernel(const vector2i &couple,
                                                                       Array<real, 2> &kernel) const
  {
    int Ncouple = int(couple.size());
    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        kernel(g1, g2) += Kernel(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                 ClassAerosolData::mass_[g1], ClassAerosolData::mass_[g2],
                                 ClassMeteorologicalData::temperature_,
                                 ClassMeteorologicalData::air_free_mean_path_,
                                 ClassMeteorologicalData::air_density_,
                                 ClassMeteorologicalData::air_dynamic_viscosity_,
                                 ClassMeteorologicalData::water_dynamic_viscosity_);
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_GRAVITATIONAL_CXX
#endif
