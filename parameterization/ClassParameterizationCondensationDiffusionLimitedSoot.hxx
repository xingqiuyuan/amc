// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_SOOT_HXX

#define AMC_CONDENSATION_DIFFUSION_LIMITED_SOOT_PI_Em12 3.141592653589793e-12

namespace AMC
{
  template<class FC, class FW, class FD>
  class ClassParameterizationCondensationDiffusionLimitedSoot : public ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>
  {
  public:

    /*!< Constructors.*/
    ClassParameterizationCondensationDiffusionLimitedSoot(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationCondensationDiffusionLimitedSoot();

    /*!< Clone.*/
    ClassParameterizationCondensationDiffusionLimitedSoot* clone() const;

    /*!< Compute coefficient.*/
    real ComputeCoefficient(const real &diameter,
                            const real &surface,
                            const real &velocity,
                            const real &diffusivity,
                            const real &accomodation) const;

    /*!< Compute coefficient.*/
    void ComputeCoefficient(const int section_min,
                            const int section_max,
                            const vector1i &parameterization_section,
                            const real *concentration_aer_number,
                            vector1r &condensation_coefficient) const;

    /*!< Compute kernel.*/
    void ComputeKernel(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       const real *concentration_aer_mass,
                       const real *concentration_gas,
                       vector1r &condensation_coefficient,
                       vector1r &rate_aer_mass) const;
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_SOOT_HXX
#endif
