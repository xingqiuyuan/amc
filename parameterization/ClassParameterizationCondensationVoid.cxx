// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_VOID_CXX

#include "ClassParameterizationCondensationVoid.hxx"

namespace AMC
{
  // Constructor.
  ClassParameterizationCondensationVoid::ClassParameterizationCondensationVoid()
    : ClassParameterizationCondensationBase()
  {
    return;
  }


  // Destructor.
  ClassParameterizationCondensationVoid::~ClassParameterizationCondensationVoid()
  {
    return;
  }

  
  // Clone.
  ClassParameterizationCondensationVoid* ClassParameterizationCondensationVoid::clone() const
  {
    return new ClassParameterizationCondensationVoid(*this);
  }


  // Compute coefficient.
  void ClassParameterizationCondensationVoid::ComputeCoefficient(const int section_min,
                                                                 const int section_max,
                                                                 const vector1i &parameterization_section,
                                                                 const real *concentration_aer_number,
                                                                 vector1r &condensation_coefficient) const
  {
    // This method does ... nothing.
  }


  // Compute kernel.
  void ClassParameterizationCondensationVoid::ComputeKernel(const int section_min,
                                                            const int section_max,
                                                            const vector1i &parameterization_section,
                                                            const real *concentration_aer_number,
                                                            const real *concentration_aer_mass,
                                                            const real *concentration_gas,
                                                            vector1r &condensation_coefficient,
                                                            vector1r &rate_aer_mass) const
  {
    // This method does ... nothing.
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_VOID_CXX
#endif
