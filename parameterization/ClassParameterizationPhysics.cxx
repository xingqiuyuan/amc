// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_PHYSICS_CXX

#include "ClassParameterizationPhysics.hxx"

namespace AMC
{
  // Compute pure water surface tension in N.m^{-1}.
  real ClassParameterizationPhysics::ComputeWaterSurfaceTension(const real &temperature)
  {
    real temperature_degree = temperature - real(273.15);

    // Limit temperature to the formula valid range [-40°C, +40°C].
    if (temperature_degree < - real(40))
      temperature_degree = - real(40);
    else if (temperature_degree > real(40))
      temperature_degree = real(40);

    if (temperature_degree < real(0))
      return 7.593e-2 + temperature_degree
        * (1.15e-4 + temperature_degree
           * (6.818e-5 + temperature_degree
              * (6.511e-6 + temperature_degree
                 * (2.933e-7 + temperature_degree
                    * (6.283e-9 + temperature_degree * 5.285e-11)))));
    else
      return real(7.61e-2) - real(1.55e-4) *  temperature_degree;
  }


  // Compute free mean path of air molecules.
  real ClassParameterizationPhysics::ComputeAirFreeMeanPath(const real &temperature, const real &pressure)
  {
    return sqrt(AMC_PHYSICS_PI * AMC_PHYSICS_IDEAL_GAS_CONSTANT * temperature * real(0.5) / AMC_PHYSICS_MOLAR_MASS_AIR)
      * ComputeAirDynamicViscosity(temperature) * real(1e6) / pressure;
  }


  // Compute water saturation vapor pressure in Pascal.
  real ClassParameterizationPhysics::ComputeWaterSaturationVaporPressureClausiusClapeyron(const real &temperature)
  {
    return real(6.1078e2) * pow(10, (real(7.5) * temperature - real(2048.625)) / (temperature - real(35.85)));
  }

  real ClassParameterizationPhysics::ComputeWaterSaturationVaporPressurePreining(const real &temperature)
  {
    return exp(real(77.34491296) - real(7235.424651) / temperature -
               real(8.2) * log(temperature) + real(5.7113e-3) * temperature);
  }

  // Compute sulfuric acid saturation vapor pressure in Pascal.*/
  real ClassParameterizationPhysics::ComputeSulfuricAcidSaturationVaporPressure(const real &temperature)
  {
    real temperature_inv = real(1) / temperature;

    return real(101325) * exp(real(-11.695)
                              + real(10156)
                              * (real(0.0027766208524226017)
                                 - temperature_inv
                                 + real(0.0006972477064220183) * (real(1) + log(real(360.15) * temperature_inv)
                                                                  - real(360.15) * temperature_inv)));
  }


  // Compute water dynamic viscosity.
  real ClassParameterizationPhysics::ComputeWaterDynamicViscosity(const real &temperature)
  {
    return real(2.426306606254427e-5) * exp(real(578.919) / (temperature - real(137.546)));
  }


  // Compute water air density in kg.m^{-3}.
  real ClassParameterizationPhysics::ComputeAirDensity(const real &temperature,
                                                       const real &pressure,
                                                       const real &relative_humidity)
  {
    return (pressure * AMC_PHYSICS_MOLAR_MASS_AIR
            + relative_humidity * ComputeWaterSaturationVaporPressureClausiusClapeyron(temperature)
            * AMC_PHYSICS_MOLAR_MASS_WATER_MINUS_AIR)
      / (AMC_PHYSICS_IDEAL_GAS_CONSTANT * temperature);
  }


  // Compute air dynamic viscosity in kg.m^{-1}.s^{-1}.
  real ClassParameterizationPhysics::ComputeAirDynamicViscosity(const real &temperature)
  {
    real tmp = temperature / real(291.15);
    return real(1.827e-5) * (real(411.15) / ( temperature + real(120)) * tmp * sqrt(tmp));
  }


  // Compute saturation vapor pressure for a different temperature.
  real ClassParameterizationPhysics::ComputeSaturationVaporPressure(const real &temperature,
                                                                    const real saturation_vapor_pressure_reference,
                                                                    const real &vaporization_enthalpy)
  {
    return saturation_vapor_pressure_reference
      * exp(vaporization_enthalpy / AMC_PHYSICS_IDEAL_GAS_CONSTANT
            * (AMC_PHYSICS_INV_TEMPERATURE_REFERENCE - real(1) / temperature));
  }


  // Compute saturation vapor concentration for a different temperature.
  real ClassParameterizationPhysics::ComputeSaturationVaporConcentration(const real &temperature,
                                                                         const real saturation_vapor_concentration_reference,
                                                                         const real &vaporization_enthalpy)
  {
    return saturation_vapor_concentration_reference
      * exp(vaporization_enthalpy / AMC_PHYSICS_IDEAL_GAS_CONSTANT
            * (AMC_PHYSICS_INV_TEMPERATURE_REFERENCE - real(1) / temperature));
  }


  // Compute gas diffusivity.
  real ClassParameterizationPhysics::ComputeGasDiffusivity(const real &temperature,
                                                           const real &pressure,
                                                           const real &collision_factor,
                                                           const real &molecular_diameter,
                                                           const real &molar_mass)
  {
    static const real x_table[] = { 0.30, 0.35, 0.40, 0.45, 0.50, 0.55,
                                    0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90,
                                    0.95, 1.00, 1.05, 1.10, 1.15, 1.20, 1.25,
                                    1.30, 1.35, 1.40, 1.45, 1.50, 1.55, 1.60,
                                    1.65, 1.70, 1.75, 1.80, 1.85, 1.90, 1.95,
                                    2.00, 2.10, 2.20, 2.30, 2.40, 2.50, 2.60,
                                    2.70, 2.80, 2.90, 3.00, 3.10, 3.20, 3.30,
                                    3.40, 3.50, 3.60, 3.70, 3.80, 3.90, 4.00,
                                    4.10, 4.20, 4.30, 4.40, 4.50, 4.60, 4.70,
                                    4.80, 4.90, 5.00, 6.00, 7.00, 8.00, 9.00,
                                    10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0,
                                    80.0, 90.0, 100.0, 200.0, 400.0 };

    static const real omega_table[] = { 2.662, 2.476, 2.318, 2.184, 2.066,
                                        1.966, 1.877, 1.798, 1.729, 1.667, 1.612,
                                        1.562, 1.517, 1.476, 1.439, 1.406, 1.375,
                                        1.346, 1.320, 1.296, 1.273, 1.253, 1.233,
                                        1.215, 1.198, 1.182, 1.167, 1.153, 1.140,
                                        1.128, 1.116, 1.105, 1.094, 1.084, 1.075,
                                        1.057, 1.041, 1.026, 1.012, 9.996e-1, 9.878e-1,
                                        9.770e-1, 9.672e-1, 9.576e-1, 9.490e-1, 9.406e-1,
                                        9.328e-1, 9.256e-1, 9.186e-1, 9.120e-1, 9.058e-1,
                                        8.998e-1, 8.942e-1, 8.888e-1, 8.836e-1, 8.788e-1,
                                        8.740e-1, 8.694e-1, 8.652e-1, 8.610e-1, 8.568e-1,
                                        8.530e-1, 8.492e-1, 8.456e-1, 8.422e-1, 8.124e-1,
                                        7.896e-1, 7.712e-1, 7.556e-1, 7.424e-1, 6.640e-1,
                                        6.232e-1, 5.960e-1, 5.756e-1, 5.596e-1, 5.464e-1,
                                        5.352e-1, 5.256e-1, 5.130e-1, 4.644e-1, 4.170e-1 };

    // Search in table.
    real x = temperature / sqrt(collision_factor * real(97));

    // Default value is 1, which is true for hard sphere
    // and prevent 0 division error and subsequent computation.
    real omega(real(1));
    int imin(0);
    int imax(80);

    if (x < x_table[imin])
      omega = omega_table[imin];
    else if (x >= x_table[imax])
      omega = omega_table[imax];
    else
      {
        int imid = (imin + imax) / 2;

        while (imax >= imin)
          {
            imid = (imin + imax) / 2;

            if (x > x_table[imid])
              imin = imid + 1;
            else if (x < x_table[imid])
              imax = imid - 1;
            else
              break;
          }

        omega = omega_table[imid - 1] + (omega_table[imid] - omega_table[imid - 1])
          * (x - x_table[imid - 1]) / (x_table[imid] - x_table[imid - 1]);
      }

    const real sigma = 0.5 * (molecular_diameter + 3.617);

    // Molar mass from kg/mol to g/mol.
    const real molar_mass_tmp = molar_mass * real(1e3); 

    return 1.8829225e-2 * temperature
      * sqrt(temperature * (molar_mass_tmp + real(28.97)) / (molar_mass_tmp * real(28.97)))
      / (sigma * sigma * omega * pressure);
  }



  // Compute gas quadratic mean velocity.
  real ClassParameterizationPhysics::ComputeGasQuadraticMeanVelocity(const real &temperature,
                                                                     const real &molar_mass)
  {
    return sqrt(AMC_PHYSICS_8_DIV_PI * AMC_IDEAL_GAS_CONSTANT * temperature / molar_mass);
  }


  // Compute particle quadratic mean velocity.
  real ClassParameterizationPhysics::ComputeParticleQuadraticMeanVelocity(const real &diameter,
                                                                          const real &temperature,
                                                                          const real density)
  {
    // 4 / Pi * sqrt(3 * Kb * 1.e15)
    return 0.00025912681430716346 * sqrt(temperature / (density * diameter)) / diameter;
  }


  // Compute particle slip flow correction.
  real ClassParameterizationPhysics::ComputeParticleCunninghamSlipFlowCorrectionAllen(const real &diameter,
                                                                                      const real &air_free_mean_path)
  {
    real knudsen = real(2) * air_free_mean_path / diameter;
    return real(1) + knudsen * (AMC_PHYSICS_CUNNINGHAM_ALLEN_A + AMC_PHYSICS_CUNNINGHAM_ALLEN_B *
                                exp(- AMC_PHYSICS_CUNNINGHAM_ALLEN_C / knudsen));
  }

  real ClassParameterizationPhysics::ComputeParticleCunninghamSlipFlowCorrectionKasten(const real &diameter,
                                                                                       const real &air_free_mean_path)
  {
    real knudsen = real(2) * air_free_mean_path / diameter;
    return real(1) + knudsen * (AMC_PHYSICS_CUNNINGHAM_KASTEN_A + AMC_PHYSICS_CUNNINGHAM_KASTEN_B *
                                exp(- AMC_PHYSICS_CUNNINGHAM_KASTEN_C / knudsen));
  }

  real ClassParameterizationPhysics::ComputeParticleCunninghamSlipFlowCorrectionMillikan(const real &diameter,
                                                                                         const real &air_free_mean_path)
  {
    real knudsen = real(2) * air_free_mean_path / diameter;
    return real(1) + knudsen * (AMC_PHYSICS_CUNNINGHAM_MILLIKAN_A + AMC_PHYSICS_CUNNINGHAM_MILLIKAN_B *
                                exp(- AMC_PHYSICS_CUNNINGHAM_MILLIKAN_C / knudsen));
  }

  real ClassParameterizationPhysics::ComputeParticleCunninghamSlipFlowCorrectionFuchs(const real &diameter,
                                                                                      const real &air_free_mean_path)
  {
    real knudsen = real(2) * air_free_mean_path / diameter;
    return (real(5) + knudsen * (real(4) + knudsen * (real(6) + real(18) *knudsen)))
      / (real(5) + knudsen * (- real(1) + real(AMC_PHYSICS_8PLUSPI) * knudsen));
  }


  // Compute particle diffusivity.
  template<real SlipFlowCorrection(const real &, const real &)>
  inline real ClassParameterizationPhysics::ComputeParticleDiffusivity(const real &diameter,
                                                                       const real &temperature,
                                                                       const real &pressure)
  {
    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);

    // Kb / (3 * Pi) * 1.e6
    return 1.4649138746259574e-18 * temperature * SlipFlowCorrection(diameter, air_free_mean_path)
      / (diameter * ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature));
  }

  real ClassParameterizationPhysics::ComputeParticleDiffusivityAllen(const real &diameter,
                                                                     const real &temperature,
                                                                     const real &pressure)
  {
    return ComputeParticleDiffusivity<ComputeParticleCunninghamSlipFlowCorrectionAllen>(diameter, temperature, pressure);
  }

  real ClassParameterizationPhysics::ComputeParticleDiffusivityKasten(const real &diameter,
                                                                      const real &temperature,
                                                                      const real &pressure)
  {
    return ComputeParticleDiffusivity<ComputeParticleCunninghamSlipFlowCorrectionKasten>(diameter, temperature, pressure);
  }

  real ClassParameterizationPhysics::ComputeParticleDiffusivityMillikan(const real &diameter,
                                                                        const real &temperature,
                                                                        const real &pressure)
  {
    return ComputeParticleDiffusivity<ComputeParticleCunninghamSlipFlowCorrectionMillikan>(diameter, temperature, pressure);
  }

  real ClassParameterizationPhysics::ComputeParticleDiffusivityFuchs(const real &diameter,
                                                                     const real &temperature,
                                                                     const real &pressure)
  {
    return ComputeParticleDiffusivity<ComputeParticleCunninghamSlipFlowCorrectionFuchs>(diameter, temperature, pressure);
  }


  // Compute particle free mean path.
  template<real SlipFlowCorrection(const real &, const real &) >
  real ClassParameterizationPhysics::ComputeParticleFreeMeanPath(const real &diameter,
                                                                 const real &temperature,
                                                                 const real &pressure,
                                                                 const real &density)
  {
    return AMC_PHYSICS_8_DIV_PI * ComputeParticleDiffusivity<SlipFlowCorrection>(diameter, temperature, pressure)
      / ComputeParticleQuadraticMeanVelocity(diameter, temperature, density);
  }

  real ClassParameterizationPhysics::ComputeParticleFreeMeanPathAllen(const real &diameter,
                                                                      const real &temperature,
                                                                      const real &pressure,
                                                                      const real density)
  {
    return ComputeParticleFreeMeanPath<ComputeParticleCunninghamSlipFlowCorrectionAllen>(diameter, temperature, pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleFreeMeanPathKasten(const real &diameter,
                                                                       const real &temperature,
                                                                       const real &pressure,
                                                                       const real density)
  {
    return ComputeParticleFreeMeanPath<ComputeParticleCunninghamSlipFlowCorrectionKasten>(diameter, temperature, pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleFreeMeanPathMillikan(const real &diameter,
                                                                         const real &temperature,
                                                                         const real &pressure,
                                                                         const real density)
  {
    return ComputeParticleFreeMeanPath<ComputeParticleCunninghamSlipFlowCorrectionMillikan>(diameter, temperature, pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleFreeMeanPathFuchs(const real &diameter,
                                                                      const real &temperature,
                                                                      const real &pressure,
                                                                      const real density)
  {
    return ComputeParticleFreeMeanPath<ComputeParticleCunninghamSlipFlowCorrectionFuchs>(diameter, temperature, pressure, density);
  }


  // Compute particle Knudsen number.
  template<real SlipFlowCorrection(const real &, const real &) >
  real ClassParameterizationPhysics::ComputeParticleKnudsenNumber(const real &diameter1,
                                                                  const real &diameter2,
                                                                  const real &temperature,
                                                                  const real &pressure,
                                                                  const real &density)
  {
#ifdef AMC_PHYSICS_WITH_PARTICLE_KNUDSEN_NUMBER_FAST
    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);

    real particle_free_mean_path1 = SlipFlowCorrection(diameter1, air_free_mean_path);
    real particle_free_mean_path2 = SlipFlowCorrection(diameter2, air_free_mean_path);

    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);

    // 4 / (3 * Pi) * sqrt(Kb / 3000) * 1.e6
    return 2.8791868256351496e-08 *
      sqrt(density * temperature * (particle_free_mean_path1 * particle_free_mean_path1 * diameter1 +
                                    particle_free_mean_path2 * particle_free_mean_path2 * diameter2))
      / ((diameter1 + diameter2) * air_dynamic_viscosity);
#else
    // Particle free mean path.
    real particle_free_mean_path1 = ClassParameterizationPhysics::
      ComputeParticleFreeMeanPath<SlipFlowCorrection>(diameter1, temperature, pressure, density);

    real particle_free_mean_path2 = ClassParameterizationPhysics::
      ComputeParticleFreeMeanPath<SlipFlowCorrection>(diameter2, temperature, pressure, density);

    return real(2e6) * sqrt(particle_free_mean_path1 * particle_free_mean_path1 +
                            particle_free_mean_path2 * particle_free_mean_path2)
      / (diameter1 + diameter2);
#endif
  }

  real ClassParameterizationPhysics::ComputeParticleKnudsenNumberAllen(const real &diameter1,
                                                                       const real &diameter2,
                                                                       const real &temperature,
                                                                       const real &pressure,
                                                                       const real density)
  {
    return ComputeParticleKnudsenNumber<ComputeParticleCunninghamSlipFlowCorrectionAllen>(diameter1, diameter2,
                                                                                          temperature, pressure,
                                                                                          density);
  }

  real ClassParameterizationPhysics::ComputeParticleKnudsenNumberKasten(const real &diameter1,
                                                                        const real &diameter2,
                                                                        const real &temperature,
                                                                        const real &pressure,
                                                                        const real density)
  {
    return ComputeParticleKnudsenNumber<ComputeParticleCunninghamSlipFlowCorrectionKasten>(diameter1, diameter2,
                                                                                           temperature, pressure,
                                                                                           density);
  }

  real ClassParameterizationPhysics::ComputeParticleKnudsenNumberMillikan(const real &diameter1,
                                                                          const real &diameter2,
                                                                          const real &temperature,
                                                                          const real &pressure,
                                                                          const real density)
  {
    return ComputeParticleKnudsenNumber<ComputeParticleCunninghamSlipFlowCorrectionMillikan>(diameter1, diameter2,
                                                                                             temperature, pressure,
                                                                                             density);
  }

  real ClassParameterizationPhysics::ComputeParticleKnudsenNumberFuchs(const real &diameter1,
                                                                       const real &diameter2,
                                                                       const real &temperature,
                                                                       const real &pressure,
                                                                       const real density)
  {
    return ComputeParticleKnudsenNumber<ComputeParticleCunninghamSlipFlowCorrectionFuchs>(diameter1, diameter2,
                                                                                          temperature, pressure,
                                                                                          density);
  }


  template<real SlipFlowCorrection(const real &, const real &) >
  real ClassParameterizationPhysics::ComputeParticleTerminalVelocity(const real &diameter,
                                                                     const real &temperature,
                                                                     const real &pressure,
                                                                     const real &density)
  {
    // G / 18. * 1.e-9
    return 5.450000000000001e-10 * diameter * diameter * density
      * SlipFlowCorrection(diameter, ComputeAirFreeMeanPath(temperature, pressure))
      / ComputeAirDynamicViscosity(temperature);
  }

  real ClassParameterizationPhysics::ComputeParticleTerminalVelocityAllen(const real &diameter,
                                                                          const real &temperature,
                                                                          const real &pressure,
                                                                          const real density)
  {
    return ComputeParticleTerminalVelocity<ComputeParticleCunninghamSlipFlowCorrectionAllen>(diameter, temperature,
                                                                                             pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleTerminalVelocityKasten(const real &diameter,
                                                                           const real &temperature,
                                                                           const real &pressure,
                                                                           const real density)
  {
    return ComputeParticleTerminalVelocity<ComputeParticleCunninghamSlipFlowCorrectionKasten>(diameter, temperature,
                                                                                              pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleTerminalVelocityMillikan(const real &diameter,
                                                                             const real &temperature,
                                                                             const real &pressure,
                                                                             const real density)
  {
    return ComputeParticleTerminalVelocity<ComputeParticleCunninghamSlipFlowCorrectionMillikan>(diameter, temperature,
                                                                                                pressure, density);
  }

  real ClassParameterizationPhysics::ComputeParticleTerminalVelocityFuchs(const real &diameter,
                                                                          const real &temperature,
                                                                          const real &pressure,
                                                                          const real density)
  {
    return ComputeParticleTerminalVelocity<ComputeParticleCunninghamSlipFlowCorrectionFuchs>(diameter, temperature,
                                                                                             pressure, density);
  }


  // Compute particle dry diameter from wet one.
  real ClassParameterizationPhysics::ComputeParticleDryDiameterFromWetDiameter(const real &wet_diameter,
                                                                               const real &temperature,
                                                                               const real &relative_humidity,
                                                                               const real &mole_solute,
                                                                               const real &density_solute,
                                                                               const real &molar_mass_solute,
                                                                               const real water_activity_coefficient,
                                                                               const real relative_tolerance,
                                                                               const int number_loop)
  {
    // If no solute, there will be no water.
    if (mole_solute == real(0))
      return real(0);

    // Particle total volume in µm3.
    const real particle_volume = AMC_PI6 * wet_diameter * wet_diameter * wet_diameter;

#ifdef AMC_WITH_DEBUG_PHYSICS
    CBUG << "particle_volume = " << particle_volume << endl;
#endif

    // Water surface tension.
    const real water_surface_tension = ComputeWaterSurfaceTension(temperature);

    // Water molar mass.
    const real molar_mass_water = real(18);

    // Water initial mole fraction.
    real water_fraction = molar_mass_solute > molar_mass_water ? relative_humidity / water_activity_coefficient : real(0);

    for (int i = 0; i < number_loop; ++i)
      {
#ifdef AMC_WITH_DEBUG_PHYSICS
        CBUG << "i = " << i << endl;
        CBUG << "water_fraction = " << water_fraction << endl;
#endif
        const real solute_fraction = real(1) - water_fraction;

        const real molar_mass_average = molar_mass_water * water_fraction
          + molar_mass_solute * solute_fraction;

        const real particle_density = real(1) / (water_fraction + solute_fraction / density_solute);

#ifdef AMC_WITH_DEBUG_PHYSICS
        CBUG << "molar_mass_average = " << molar_mass_average << endl;
        CBUG << "particle_density = " << particle_density << endl;
#endif

        const real kelvin_effect = exp(real(4) * water_surface_tension * molar_mass_average
                                       / (particle_density * AMC_IDEAL_GAS_CONSTANT
                                          * temperature * wet_diameter));

#ifdef AMC_WITH_DEBUG_PHYSICS
        CBUG << "kelvin_effect = " << kelvin_effect << endl;
#endif

        // New water fraction value.
        const real water_fraction_old(water_fraction);

        water_fraction = relative_humidity / (water_activity_coefficient * kelvin_effect);
        water_fraction = water_fraction < real(1) ? water_fraction : real(0.999999);

        // Doest it converge ?
        if (water_fraction_old > real(0))
          if (abs(water_fraction - water_fraction_old) / water_fraction_old < relative_tolerance)
            break;
      }

#ifdef AMC_WITH_DEBUG_PHYSICS
    CBUG << "water_fraction = " << water_fraction << endl;
#endif

    // Water mole.
    const real mole_water = water_fraction / (real(1) - water_fraction) * mole_solute;

#ifdef AMC_WITH_DEBUG_PHYSICS
    CBUG << "mole_water = " << mole_water << endl;
#endif

    // Water volume in µm3.
    const real water_volume = AMC_WATER_MOLAR_MASS * mole_water / AMC_WATER_DENSITY;

#ifdef AMC_WITH_DEBUG_PHYSICS
    CBUG << "water_volume = " << water_volume << endl;
#endif

    // If no inconsistencies, return dry diameter.
    if (particle_volume < water_volume)
      return real(-1);
    else
      return pow((particle_volume - water_volume) * AMC_INV_PI6, AMC_FRAC3);
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_PHYSICS_CXX
#endif
