// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TURBULENT_HXX

#define AMC_COAGULATION_TURBULENT_PI_DIV_120  0.02617993877991494

namespace AMC
{
  class ClassParameterizationCoagulationTurbulent : public ClassParameterizationCoagulationBase
  {

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationTurbulent();
    ClassParameterizationCoagulationTurbulent(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationCoagulationTurbulent();

    /*!< Clone.*/
    ClassParameterizationCoagulationTurbulent* clone() const;

    /*!< Compute brownian kernel.*/
    real Kernel(const real &diameter1, const real &diameter2,
                const real &eddy_dissipation_rate,
                const real &air_dynamic_viscosity,
                const real &air_density) const;

    /*!< Compute kernel for tabulation.*/
    real KernelTable(const vector1r &variable) const;

#ifndef SWIG
    /*!< Compute kernel for AMC.*/
    void ComputeKernel(const vector2i &couple,
                       Array<real, 2> &kernel) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_TURBULENT_HXX
#endif
