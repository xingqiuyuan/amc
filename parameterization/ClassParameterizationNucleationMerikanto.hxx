// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_MERIKANTO_HXX

#ifndef SWIG
#define MERIKANTO_POLYNOMIAL_FIT(a0, a1, a2, a3) real(a0) + temperature * (real(a1) + temperature * (real(a2) + temperature * real(a3)))
#endif

#define AMC_NUCLEATION_MERIKANTO_AMMONIUM_INDEX 1
#define AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_WATER 18.0
#define AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_SULFATE 98.0
#define AMC_NUCLEATION_MERIKANTO_MOLAR_MASS_AMMONIUM 17.0
#define AMC_NUCLEATION_MERIKANTO_CONVERT_AMMONIUM_FROM_UGM3_TO_PPT 489.086010294
#define AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MIN 235.0
#define AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MAX 295.0
#define AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MIN 0.05
#define AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MAX 0.95
#define AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN 5.e4
#define AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MAX 5.e9
#define AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN 0.1
#define AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MAX 1000.0
#define AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_CM3_MIN 1.e-5
#define AMC_NUCLEATION_MERIKANTO_NUCLEATION_RATE_LOG_MIN -11.512925464970229
#define AMC_NUCLEATION_MERIKANTO_SULFATE_CONVERT_FROM_MOLEC_TO_UG 1.6273281426115462e-16
#define AMC_NUCLEATION_MERIKANTO_AMMONIUM_CONVERT_FROM_MOLEC_TO_UG 2.8229161657547227e-17
#define AMC_NUCLEATION_MERIKANTO_WATER_CONVERT_FROM_MOLEC_TO_UG 2.988970057857942e-17

namespace AMC
{
  class ClassParameterizationNucleationMerikanto : public ClassParameterizationNucleationBase
  {
  private:

    /*!< Threshold parameters due to validity range of formula.*/
    real threshold_relative_humidity(const real &relative_humidity) const;
    real threshold_temperature(const real &temperature) const;
    real threshold_concentration_ammonium_ppt(const real &concentration_ammonium_ppt) const;
    real threshold_concentration_sulfate_molec_cm3(const real &concentration_sulfate_molec_cm3) const;

    /*!< Logarithm of nucleation rate.*/
    real nucleation_rate_cm3s_log(const real &temperature,
                                  const real &relative_humidity,
                                  const real &relative_humidity_log,
                                  const real &concentration_sulfate_molec_cm3_log,
                                  const real &concentration_ammonium_ppt,
                                  const real &concentration_ammonium_ppt_log) const;

    /*!< Critical cluster radius in nanometer.*/
    real critical_cluster_radius_nm(const real &temperature,
                                    const real &nucleation_rate_log,
                                    const real &concentration_sulfate_molec_cm3_log,
                                    const real &concentration_ammonium_ppt_log) const;


    /*!< Number molecule total.*/
    real number_molecule_total(const real &temperature,
                               const real &nucleation_rate_log,
                               const real &concentration_sulfate_molec_cm3_log,
                               const real &concentration_ammonium_ppt_log) const;

    /*!< Number molecule ammonium.*/
    real number_molecule_ammonium(const real &temperature,
                                  const real &nucleation_rate_log,
                                  const real &concentration_sulfate_molec_cm3_log,
                                  const real &concentration_ammonium_ppt_log) const;

    /*!< Number molecule sulfate.*/
    real number_molecule_sulfate(const real &temperature,
                                 const real &nucleation_rate_log,
                                 const real &concentration_sulfate_molec_cm3_log,
                                 const real &concentration_ammonium_ppt_log) const;

    /*!< Onset temperature.*/
    real onset_temperature(const real &temperature,
                           const real &relative_humidity,
                           const real &concentration_sulfate_molec_cm3_log,
                           const real &concentration_ammonium_ppt_log) const;

  public:

    /*!< Constructors.*/
    ClassParameterizationNucleationMerikanto();

    /*!< Destructor.*/
    ~ClassParameterizationNucleationMerikanto();

    /*!< Clone.*/
    ClassParameterizationNucleationMerikanto* clone() const;

    /*!< Convert concentrations.*/
    static real ConvertAmmoniumFromUGM3toPPT(const real &concentration_ammonium_ugm3,
                                             const real temperature = real(273.15),
                                             const real pressure = real(1.01325e5));

    /*!< Compute onset temperature.*/
    real ComputeTemperatureOnset(const real &temperature,
                                 const real &relative_humidity,
                                 const real &concentration_sulfate_molec_cm3,
                                 const real &concentration_ammonium_ppt) const;

    /*!< Number of molecules in cluster.*/
    void ComputeNumberMolecule(const real &temperature,
                               const real &relative_humidity,
                               const real &concentration_sulfate_molec_cm3,
                               const real &concentration_ammonium_ppt,
                               vector<real> &number_molecule) const;

    /*!< Compute mole and mass fractions.*/
    void ComputeMoleFraction(const real &temperature,
                             const real &relative_humidity,
                             const real &concentration_sulfate_molec_cm3,
                             const real &concentration_ammonium_ppt,
                             vector<real> &mole_fraction) const;

    void ComputeMassFraction(const real &temperature,
                             const real &relative_humidity,
                             const real &concentration_sulfate_molec_cm3,
                             const real &concentration_ammonium_ppt,
                             vector<real> &mass_fraction) const;

    /*!< Critical cluster radius.*/
    real ComputeCriticalClusterRadius(const real &temperature,
                                      const real &relative_humidity,
                                      const real &concentration_sulfate_molec_cm3,
                                      const real &concentration_ammonium_ppt) const;

    /*!< Compute kernel.*/
    real ComputeKernel(const real &temperature,
                       const real &relative_humidity,
                       const real &concentration_sulfate_molec_cm3,
                       const real &concentration_ammonium_ppt) const;

#ifndef SWIG
    void ComputeKernel(const int &section_index,
                       const real *concentration_gas,
                       vector1r &rate_aer_number,
                       vector1r &rate_aer_mass) const;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    bool ComputeKernel(const vector1r &concentration_gas,
                       real &diameter,
                       real &rate_aer_number,
                       vector1r &rate_aer_mass) const;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_NUCLEATION_MERIKANTO_HXX
#endif
