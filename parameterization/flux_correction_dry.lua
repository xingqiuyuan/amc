flux_correction_dry = {
   -- Static data taken from Isoropia.
   XK3  = {reference = 1.971e6,   coefficient = {30.20, 19.910}},
   XK4  = {reference = 2.511e6,   coefficient = {29.17, 16.830}},
   XK6  = {reference = 1.086e-16, coefficient = {-71.00, 2.400}},
   XK8  = {reference = 37.661,    coefficient = {-1.56, 16.900}},
   XK9  = {reference = 11.977,    coefficient = {-8.22, 16.010}},
   XK10 = {reference = 5.746e-17, coefficient = {-74.38, 6.120}},

   -- Order of species is hardcoded, you may change their name (eg. H2SO4 -> sulfate)
   -- but do not alter the order.
   species = {"H2SO4", "NH3", "HNO3", "HCl"},
   molar_mass = {98.0, 17.0, 63.0, 36.5},

   -- Order of solid is hardcoded, you may change their name (eg. NH4NO3 -> ammonium-nitrate)
   -- but do not alter the order.
   solid = {"NH4NO3", "NH4Cl", "NaNO3", "NaCl"}
}