// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FIXED_HXX


namespace AMC
{
  class ClassParameterizationDiameterFixed : public ClassParameterizationDiameterBase
  {
  private:

    /*!< Section index.*/
    vector1i section_index_;

    /*!< Fixed diameter.*/
    vector1r diameter_;

    /*!< Fixed volume.*/
    vector1r volume_;

  public:

    /*!< Constructors.*/
    ClassParameterizationDiameterFixed(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationDiameterFixed();

    /*!< Get methods.*/
    void GetSectionIndexList(vector<int> &section_index) const;
    void GetDiameterList(vector<real> &diameter) const;

    /*!< Clone.*/
    ClassParameterizationDiameterFixed* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationDiameterBase *param);

#ifndef SWIG
    /*!< Compute diameter for AMC.*/
    void ComputeDiameter(const int section_min,
                         const int section_max,
                         const vector1i &parameterization_section,
                         const real *concentration_aer_number,
                         const real *concentration_aer_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_DIAMETER_FIXED_HXX
#endif
