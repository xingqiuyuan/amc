// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_DRY_INORGANIC_SALT_CXX

#include "ClassCondensationFluxCorrectionDryInorganicSalt.hxx"

#define NH4NO3 0
#define NH4Cl 1
#define NaNO3 2
#define NaCl 3

#define H2SO4 0
#define NH3   1
#define HNO3  2
#define HCl   3

namespace AMC
{
  // Init static data.
  void ClassCondensationFluxCorrectionDryInorganicSalt::Init(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << Reset()
                         << "Set static data for dry inorganic salt flux correction." << endl;
#endif

    string prefix_orig = ops.GetPrefix();

    is_initiated_ = false;

    // If no adequate section, exit.
    if (! ops.Exists("flux_correction_dry"))
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Warning(1) << Reset() << "Could not find section"
                             << " \"flux_correction_dry\" to initiate dry inorganic salt." << endl;
#endif
        return;
      }

    ops.SetPrefix("flux_correction_dry.");

    vector1r coefficient;

    // Constant reaction for  NH4NO3s <==> NH3g + HNO3g .
    XK10_reference_ = ops.Get<real>("XK10.reference");
    ops.Set("XK10.coefficient", "", coefficient);
    XK10_coefficient0_ = coefficient[0];
    XK10_coefficient1_ = coefficient[1];

    // Constant reaction for  NH4Cls <==> NH3g + HClg .
    XK6_reference_ = ops.Get<real>("XK6.reference");
    ops.Set("XK6.coefficient", "", coefficient);
    XK6_coefficient0_ = coefficient[0];
    XK6_coefficient1_ = coefficient[1];

    // Constant reaction for  NaCls + HNO3g <==> NaNO3s + HClg adim .
    XK3_reference_ = ops.Get<real>("XK3.reference");
    XK4_reference_ = ops.Get<real>("XK4.reference");
    XK8_reference_ = ops.Get<real>("XK8.reference");
    XK9_reference_ = ops.Get<real>("XK9.reference");

    XK3_ = XK3_reference_;
    XK4_ = XK4_reference_;
    XK6_ = XK6_reference_;
    XK8_ = XK8_reference_;
    XK9_ = XK9_reference_;
    XK10_ = XK10_reference_;

    ops.Set("XK3.coefficient", "", coefficient);
    XK3_coefficient0_ = coefficient[0];
    XK3_coefficient1_ = coefficient[1];

    ops.Set("XK4.coefficient", "", coefficient);
    XK4_coefficient0_ = coefficient[0];
    XK4_coefficient1_ = coefficient[1];

    ops.Set("XK8.coefficient", "", coefficient);
    XK8_coefficient0_ = coefficient[0];
    XK8_coefficient1_ = coefficient[1];

    ops.Set("XK9.coefficient", "", coefficient);
    XK9_coefficient0_ = coefficient[0];
    XK9_coefficient1_ = coefficient[1];

    ops.SetPrefix(prefix_orig);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK3 : reference = " << XK3_reference_
                         << ", coefficients = " << XK3_coefficient0_ << ", " << XK3_coefficient1_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK4 : reference = " << XK4_reference_
                         << ",coefficients = " << XK4_coefficient0_ << ", " << XK4_coefficient1_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK6 : reference = " << XK6_reference_
                         << ", coefficients = " << XK6_coefficient0_ << ", " << XK6_coefficient1_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK8 : reference = " << XK8_reference_
                         << ", coefficients = " << XK8_coefficient0_ << ", " << XK8_coefficient1_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK9 : reference = " << XK9_reference_
                         << ", coefficients = " << XK9_coefficient0_ << ", " << XK9_coefficient1_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "XK10: reference = " << XK10_reference_
                         << ", coefficients = " << XK10_coefficient0_ << ", " << XK10_coefficient1_ << endl;
#endif
  }


  // Init static data for one step.
  void ClassCondensationFluxCorrectionDryInorganicSalt::Update(const real &temperature)
  {
    real tmp0 = AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_TEMPERATURE_REFERENCE / temperature;
    real tmp1 = real(1) + log(tmp0) - tmp0;
    tmp0 -= real(1);

    XK3_ = XK3_reference_ * exp(XK3_coefficient0_ * tmp0 - XK3_coefficient1_ * tmp1);
    XK4_ = XK4_reference_ * exp(XK4_coefficient0_ * tmp0 - XK4_coefficient1_ * tmp1);
    XK6_ = XK6_reference_ * exp(XK6_coefficient0_ * tmp0 - XK6_coefficient1_ * tmp1);
    XK8_ = XK8_reference_ * exp(XK8_coefficient0_ * tmp0 - XK8_coefficient1_ * tmp1);
    XK9_ = XK9_reference_ * exp(XK9_coefficient0_ * tmp0 - XK9_coefficient1_ * tmp1);
    XK10_ = XK10_reference_ * exp(XK10_coefficient0_ * tmp0 - XK10_coefficient1_ * tmp1);
  }


  // Clear static data.
  void ClassCondensationFluxCorrectionDryInorganicSalt::Clear()
  {
    XK3_reference_ = real(0);
    XK4_reference_ = real(0);
    XK6_reference_ = real(0);
    XK8_reference_ = real(0);
    XK9_reference_ = real(0);
    XK10_reference_ = real(0);

    XK3_ = real(0);
    XK4_ = real(0);
    XK6_ = real(0);
    XK8_ = real(0);
    XK9_ = real(0);
    XK10_ = real(0);

    XK3_coefficient0_ = real(0);
    XK3_coefficient1_ = real(0);
    XK4_coefficient0_ = real(0);
    XK4_coefficient1_ = real(0);
    XK6_coefficient0_ = real(0);
    XK6_coefficient1_ = real(0);
    XK8_coefficient0_ = real(0);
    XK8_coefficient1_ = real(0);
    XK9_coefficient0_ = real(0);
    XK9_coefficient1_ = real(0);
    XK10_coefficient0_ = real(0);
    XK10_coefficient1_ = real(0);
  }


  // Constructors.
  ClassCondensationFluxCorrectionDryInorganicSalt::ClassCondensationFluxCorrectionDryInorganicSalt(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info() << Reset() << "Instantiating dry inorganic salt flux correction." << endl;
#endif

    // Abort if could not set static data.
    if (! is_initiated_)
      throw AMC::Error("Cannot instantiate dry inorganic salt because the setting of static data failed earlier.");

    // Read standalone configuration.
    string prefix_orig = ops.GetPrefix();

    ops.SetPrefix("flux_correction_dry.");

    vector1s species_name;
    ops.Set("species", "", species_name);

    ops.Set("molar_mass", "", molar_mass_);
    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS; i++)
      molar_mass_[i] *= real(1e6);

    vector1s solid_name;
    ops.Set("solid", "", solid_name);

    // Revert to AMC specific configuration.
    ops.SetPrefix(prefix_orig);

    index_gas_.assign(AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS, -1);
    index_solid_.assign(AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NSOLID, -1);

    if (ops.Exists("flux_correction.dry"))
      {
        ops.SetPrefix(prefix_orig + "flux_correction.dry.");

        index_gas_[H2SO4] = ops.Get<int>(species_name[H2SO4], "", -1);
        index_gas_[NH3] = ops.Get<int>(species_name[NH3], "", -1);
        index_gas_[HNO3] = ops.Get<int>(species_name[HNO3], "", -1);
        index_gas_[HCl] = ops.Get<int>(species_name[HCl], "", -1);

        index_solid_[NH4NO3] = ops.Get<int>(solid_name[NH4NO3], "", -1);
        index_solid_[NH4Cl] = ops.Get<int>(solid_name[NH4Cl], "", -1);
        index_solid_[NaNO3] = ops.Get<int>(solid_name[NaNO3], "", -1);
        index_solid_[NaCl] = ops.Get<int>(solid_name[NaCl], "", -1);

        ops.SetPrefix(prefix_orig);
      }

    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS; i++)
      if (index_gas_[i] < 0)
        for (int j = 0; j < ClassSpecies::Ngas_; j++)
          if (species_name[i] == ClassSpecies::name_[ClassSpecies::semivolatile_[j]])
            {
              index_gas_[i] = j;
              break;
            }

    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS; i++)
      if (index_gas_[i] < 0)
        throw AMC::Error("Unable to find AMC index of species \"" + species_name[i] + "\".");

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS; i++)
      *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "Gas species \"" << species_name[i]
                           << "\" at index " << index_gas_[i] << endl;
#endif

    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NSOLID; i++)
      if (index_solid_[i] < 0)
        for (int j = 0; j < ClassSpeciesEquilibrium::Nspecies_; j++)
          if (solid_name[i] == ClassSpeciesEquilibrium::name_[j])
            {
              index_solid_[i] = j;
              break;
            }

    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NSOLID; i++)
      if (index_solid_[i] < 0)
        throw AMC::Error("Unable to find AMC index of solid species \"" + solid_name[i] + "\".");

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NSOLID; i++)
      *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "Solid species \"" << solid_name[i]
                           << "\" at index " << index_solid_[i] << endl;
#endif

    return;
  }


  // Destructor.
  ClassCondensationFluxCorrectionDryInorganicSalt::~ClassCondensationFluxCorrectionDryInorganicSalt()
  {
    return;
  }


  // Get methods.
  string ClassCondensationFluxCorrectionDryInorganicSalt::GetName() const
  {
    return "InorganicSalt";
  }


  // Compute gas equilibrium concentration when particle is dry.
  void ClassCondensationFluxCorrectionDryInorganicSalt::ComputeFluxCorrection(const real &temperature,
                                                                              const vector1r &condensation_coefficient,
                                                                              const real *equilibrium_aer_internal,
                                                                              const real *concentration_gas,
                                                                              real *concentration_gas_equilibrium,
                                                                              vector1r &rate_aer_mass) const
  {
    //
    // Compute temperature dependent constant reactions.
    //

    real tmp = AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_PRESSURE_ATM
      / (temperature * AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_IDEAL_GAS_CONSTANT);
    tmp *= tmp;

    // Constant reaction for  NH4NO3s <==> NH3g + HNO3g in (µg.m-3)^2
    real constant_reaction_nh4no3 = XK10_ * tmp * molar_mass_[NH3] * molar_mass_[HNO3];

    // Constant reaction for  NH4Cls <==> NH3g + HClg in (µg.m-3)^2
    real constant_reaction_nh4cl = XK6_ * tmp * molar_mass_[NH3] * molar_mass_[HCl];

    // Constant reaction for  NaCls + HNO3g <==> NaNO3s + HClg adim
    real constant_reaction_nacl_nano3 = XK4_ * XK8_ / (XK3_ * XK9_);

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_DRY
    cout << "constant_reaction_nh4no3 = " << constant_reaction_nh4no3 << endl;
    cout << "constant_reaction_nh4cl = " << constant_reaction_nh4cl << endl;
    cout << "constant_reaction_nacl_nano3 = " << constant_reaction_nacl_nano3 << endl;
#endif

    //
    // Compute saturation related to previous reactions.
    //

    real saturation_nh4no3 = concentration_gas[index_gas_[NH3]] * concentration_gas[index_gas_[HNO3]];
    real saturation_nh4cl = concentration_gas[index_gas_[NH3]] * concentration_gas[index_gas_[HCl]];
    real saturation_nacl_nano3_a = concentration_gas[index_gas_[HCl]];
    real saturation_nacl_nano3_b = constant_reaction_nacl_nano3 * concentration_gas[index_gas_[HNO3]];

    vector1r condensation_coefficient_molar(AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS),
      rate_aer_mass_molar(AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS);

    for (int i = 0; i < AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS; i++)
      {
        int j = index_gas_[i];
        condensation_coefficient_molar[i] = condensation_coefficient[j] / molar_mass_[i];
        rate_aer_mass_molar[i] = rate_aer_mass[j] / molar_mass_[i];
      }

    real saturation_h2so4 = real(2) * rate_aer_mass_molar[H2SO4] - rate_aer_mass_molar[NH3];

    //
    // Determine which reaction is active.
    //

    bool is_nh4no3_active(false), is_nh4cl_active(false),
      is_nacl_nano3_active(false), is_nacl_nh4cl_h2so4_active(false),
      is_nano3_nh4no3_h2so4_active(false);

    if (equilibrium_aer_internal[index_solid_[NH4NO3]] > real(0) ||
        saturation_nh4no3 > constant_reaction_nh4no3)
      is_nh4no3_active = true;

    if (equilibrium_aer_internal[index_solid_[NH4Cl]] > real(0) ||
        saturation_nh4cl > constant_reaction_nh4cl)
      is_nh4cl_active = true;

    if (equilibrium_aer_internal[index_solid_[NaNO3]] > real(0) &&
        equilibrium_aer_internal[index_solid_[NaCl]] > real(0))
      is_nacl_nano3_active = true;
    else if (equilibrium_aer_internal[index_solid_[NaNO3]] > real(0) &&
             saturation_nacl_nano3_a > saturation_nacl_nano3_b)
      is_nacl_nano3_active = true;
    else if (equilibrium_aer_internal[index_solid_[NaCl]] > real(0) &&
             saturation_nacl_nano3_a < saturation_nacl_nano3_b)
      is_nacl_nano3_active = true;

    if (concentration_gas[index_gas_[H2SO4]] > real(0))
      {
        if (equilibrium_aer_internal[index_solid_[NaCl]] > real(0) ||
            equilibrium_aer_internal[index_solid_[NH4Cl]] > real(0))
          is_nacl_nh4cl_h2so4_active = true;

        if (equilibrium_aer_internal[index_solid_[NaNO3]] > real(0) ||
            equilibrium_aer_internal[index_solid_[NH4NO3]] > real(0))
          is_nano3_nh4no3_h2so4_active = true;
      }

    // Three first reactions cannot be active at the same time.
    // If this happens, sure it is a problem, in this case,
    // we put rates at zero and return, hoping the problem
    // will be solved on next iteration.
    if (is_nh4no3_active)
      if (is_nh4cl_active)
        if (is_nacl_nano3_active)
          {
            rate_aer_mass[index_gas_[NH3]] = real(0);
            rate_aer_mass[index_gas_[HNO3]] = real(0);
            rate_aer_mass[index_gas_[HCl]] = real(0);
            return;
          }

    //
    // Determine which case is relevant.
    //

    int icase;

    if (is_nh4cl_active && is_nacl_nano3_active)
      icase = 1; // R2 and R3 active
    else if (is_nh4no3_active && is_nh4cl_active)
      icase = 2; // R1 and R2 active
    else if (is_nh4no3_active && is_nacl_nano3_active)
      icase = 3; // R1 and R3 active
    else if (is_nh4no3_active)
      icase = 4; // only R1 active
    else if (is_nh4cl_active)
      icase = 5; // only R2 active
    else if (is_nacl_nano3_active)
      icase = 6; // only R3 active
    else
      if (is_nacl_nh4cl_h2so4_active)
        icase = 7; // R4 or R7 active
      else if (is_nano3_nh4no3_h2so4_active)
        icase = 8; // R5 or R6 active
      else // nothing active
        if (saturation_h2so4 < real(0))
          icase = 9; // enough nh3 to neutralize so4
        else
          icase = 10; // not enough nh3 to neutralize so4, in this case aerosol become acidic

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_DRY
    cout << "icase = " << icase << endl;
#endif

  //
  // Solve each case.
  //

    const int &index_hno3 = index_gas_[HNO3];
    const int &index_nh3 = index_gas_[NH3];
    const int &index_hcl = index_gas_[HCl];

    if (icase == 3)
      {
        // icase 3 is not physical but used 
        // to determine the real icase
        real a = (condensation_coefficient_molar[HNO3] + condensation_coefficient_molar[HCl] * constant_reaction_nacl_nano3);
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HNO3]
          + rate_aer_mass_molar[HCl] - rate_aer_mass_molar[NH3];
        real c = constant_reaction_nh4no3 * condensation_coefficient_molar[NH3];

        concentration_gas_equilibrium[index_hno3] = (b + sqrt(b * b + real(4) * a * c)) / (real(2) * a);

        /* 
           concentration_gas_equilibrium[index_hcl] =
           constant_reaction_nacl_nano3 * concentration_gas_equilibrium[index_hno3];
           concentration_gas_equilibrium[index_nh3] =
           constant_reaction_nh4no3 / concentration_gas_equilibrium[index_hno3];
        */
 
        // Test if HNO3 condenses.
        if (concentration_gas[index_hno3] > concentration_gas_equilibrium[index_hno3])
          icase = 2; // if NH4NO3 forms then real case = 2 
        else
          icase = 1; // if NACl forms then real case = 1
      }

    // Other cases.
    if (icase == 1)
      {
        real a = condensation_coefficient_molar[HNO3] + condensation_coefficient_molar[HCl] * constant_reaction_nacl_nano3;
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HNO3]
          + rate_aer_mass_molar[HCl] - rate_aer_mass_molar[NH3];
        real c = constant_reaction_nh4cl / constant_reaction_nacl_nano3
          * condensation_coefficient_molar[NH3];

        concentration_gas_equilibrium[index_hno3] = (b + sqrt(b * b + real(4) * a * c)) / (real(2) * a);
        concentration_gas_equilibrium[index_hcl] =
          constant_reaction_nacl_nano3 * concentration_gas_equilibrium[index_hno3];
        concentration_gas_equilibrium[index_nh3] =
          constant_reaction_nh4cl / constant_reaction_nacl_nano3 / concentration_gas_equilibrium[index_hno3];
      }
    else if (icase == 2)
      {
        real a = condensation_coefficient_molar[HNO3]
          + condensation_coefficient_molar[HCl] * constant_reaction_nh4cl / constant_reaction_nh4no3;
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HNO3]
          +rate_aer_mass_molar[HCl] - rate_aer_mass_molar[NH3];
        real c = constant_reaction_nh4no3 * condensation_coefficient_molar[NH3];

        concentration_gas_equilibrium[index_hno3] = (b + sqrt(b * b + real(4) * a * c)) / (real(2) * a);
        concentration_gas_equilibrium[index_nh3] =
          constant_reaction_nh4no3 / concentration_gas_equilibrium[index_hno3];
        concentration_gas_equilibrium[index_hcl] =
          constant_reaction_nh4cl / constant_reaction_nh4no3 * concentration_gas_equilibrium[index_hno3];
      }
    else if (icase == 4)
      {
        real a = condensation_coefficient_molar[HNO3];
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HNO3] - rate_aer_mass_molar[NH3];
        real c = constant_reaction_nh4no3 * condensation_coefficient_molar[NH3];

        concentration_gas_equilibrium[index_hno3] = (b + sqrt(b * b + real(4) * a * c)) / (real(2) * a);
        concentration_gas_equilibrium[index_nh3] = constant_reaction_nh4no3 / concentration_gas_equilibrium[index_hno3];
        concentration_gas_equilibrium[index_hcl] = concentration_gas[index_hcl];
      }
    else if (icase == 5)
      {
        real a = condensation_coefficient_molar[NH3];
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HCl] - rate_aer_mass_molar[NH3];
        real c = condensation_coefficient_molar[HCl] * constant_reaction_nh4cl;

        concentration_gas_equilibrium[index_nh3] = (b + sqrt(b * b + real(4) * a * c)) / (real(2) * a);
        concentration_gas_equilibrium[index_hcl] = constant_reaction_nh4cl / concentration_gas_equilibrium[index_nh3];
        concentration_gas_equilibrium[index_hno3] = concentration_gas[index_hno3];
      }
    else if (icase == 6)
      {
        real a = condensation_coefficient_molar[HNO3] + constant_reaction_nacl_nano3 * condensation_coefficient_molar[HCl];
        real b = real(2) * rate_aer_mass_molar[H2SO4] + rate_aer_mass_molar[HCl] + rate_aer_mass_molar[HNO3];

        concentration_gas_equilibrium[index_hno3] = b / a;
        concentration_gas_equilibrium[index_hcl] = constant_reaction_nacl_nano3
          * concentration_gas_equilibrium[index_hno3];
        concentration_gas_equilibrium[index_nh3] = concentration_gas[index_hcl];
      }
    else if (icase == 7)
      {
        concentration_gas_equilibrium[index_nh3] = concentration_gas[index_nh3];
        concentration_gas_equilibrium[index_hno3] = concentration_gas[index_hno3];

        rate_aer_mass[index_nh3] = real(0);
        rate_aer_mass[index_hno3] = real(0);

        rate_aer_mass[index_hcl] = - molar_mass_[HCl] * real(2) * rate_aer_mass_molar[H2SO4];

        concentration_gas_equilibrium[index_hcl] = concentration_gas[index_hcl]
          - rate_aer_mass[index_hcl] / condensation_coefficient[index_hcl];
      }
    else if (icase == 8)
      {
        concentration_gas_equilibrium[index_nh3] = concentration_gas[index_nh3];
        concentration_gas_equilibrium[index_hcl] = concentration_gas[index_hcl];

        rate_aer_mass[index_nh3] = real(0);
        rate_aer_mass[index_hcl] = real(0);

        rate_aer_mass[index_hno3] = - molar_mass_[HCl] * real(2) * rate_aer_mass_molar[H2SO4];

        concentration_gas_equilibrium[index_hno3] = concentration_gas[index_hno3]
          - rate_aer_mass[index_hno3] / condensation_coefficient[index_hno3];
      }
    else if (icase == 9)
      {
        concentration_gas_equilibrium[index_hno3] = concentration_gas[index_hno3];
        concentration_gas_equilibrium[index_hcl] = concentration_gas[index_hcl];

        rate_aer_mass[index_hno3] = real(0);
        rate_aer_mass[index_hcl] = real(0);

        rate_aer_mass[index_nh3] = molar_mass_[NH3] * real(2) * rate_aer_mass_molar[H2SO4];

        concentration_gas_equilibrium[index_nh3] = concentration_gas[index_nh3]
          - rate_aer_mass[index_nh3] / condensation_coefficient[index_nh3];
      }
    else if (icase == 10)
      {
        // No more electroneutrality in this case.
        concentration_gas_equilibrium[index_hno3] = concentration_gas[index_hno3];
        concentration_gas_equilibrium[index_hcl] = concentration_gas[index_hcl];

        rate_aer_mass[index_hno3] = real(0);
        rate_aer_mass[index_hcl] = real(0);
      }

#ifdef AMC_WITH_DEBUG_FLUX_CORRECTION_DRY
    cout << "concentration_gas_equilibrium_nh3 = " << concentration_gas_equilibrium[index_nh3] << endl;
    cout << "concentration_gas_equilibrium_hno3 = " << concentration_gas_equilibrium[index_hno3] << endl;
    cout << "concentration_gas_equilibrium_hcl = " << concentration_gas_equilibrium[index_hcl] << endl;
#endif

    //
    // Giving out the kernel for case <=6.
    //
    if (icase <= 6)
      for (int i = NH3; i < HCl; i++)
        {
          int j = index_gas_[i];
          rate_aer_mass[j] = condensation_coefficient[j]
            * (concentration_gas[j] - concentration_gas_equilibrium[j]);
        }
  }
}

#undef NH4NO3
#undef NH4Cl
#undef NaNO3
#undef NaCl

#undef H2SO4
#undef NH3
#undef HNO3
#undef HCl

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_DRY_INORGANIC_SALT_CXX
#endif
