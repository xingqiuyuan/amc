// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_SOOT_CXX

#include "ClassParameterizationCondensationDiffusionLimitedSoot.hxx"


namespace AMC
{
  // Constructors.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::ClassParameterizationCondensationDiffusionLimitedSoot(Ops::Ops &ops)
    : ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>(ops, "DiffusionLimitedSoot")
  {
    return;
  }


  // Destructor.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::~ClassParameterizationCondensationDiffusionLimitedSoot()
  {
    return;
  }


  // Clone.
  template<class FC, class FW, class FD>
  ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>* ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::clone() const
  {
    return new ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>(*this);
  }


  // Compute coefficient.
  template<class FC, class FW, class FD>
  real ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::ComputeCoefficient(const real &diameter,
                                                                                             const real &surface,
                                                                                             const real &velocity,
                                                                                             const real &diffusivity,
                                                                                             const real &accomodation) const
  {
    real knudsen = 2.e6 * (this->correction_factor_)->FreeMeanPath(diffusivity, velocity) / diameter;

    // Compute coefficient according to regime : continous, free molecular or transition.
    if (knudsen < this->knudsen_min_)
      return AMC_CONDENSATION_DIFFUSION_LIMITED_2_PI_Em6 * diffusivity * diameter;
    else if (knudsen >= this->knudsen_max_)
      return AMC_CONDENSATION_DIFFUSION_LIMITED_SOOT_PI_Em12
        * velocity * accomodation * surface;
    else
      return AMC_CONDENSATION_DIFFUSION_LIMITED_2_PI_Em6 * diffusivity * diameter *
        (this->correction_factor_)->CorrectionFactorSoot(knudsen, accomodation, diameter, surface);
  }


  // Compute coefficient.
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::ComputeCoefficient(const int section_min,
                                                                                             const int section_max,
                                                                                             const vector1i &parameterization_section,
                                                                                             const real *concentration_aer_number,
                                                                                             vector1r &condensation_coefficient) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        // Start index in rate mass.
        int k = j * ClassSpecies::Ngas_;

        for (int l = 0; l < ClassSpecies::Ngas_; l++)
          {
            int m = ClassSpecies::semivolatile_[l];

            condensation_coefficient[k++] = concentration_aer_number[j]
              * ComputeCoefficient(ClassAerosolData::diameter_[j],
                                   ClassAerosolData::surface_[j],
                                   ClassMeteorologicalData::gas_quadratic_mean_velocity_[m],
                                   ClassMeteorologicalData::gas_diffusivity_[m],
                                   ClassSpecies::accomodation_coefficient_[m]);
          }
      }
  }


  // Compute kernel.
  template<class FC, class FW, class FD>
  void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::ComputeKernel(const int section_min,
                                                                                        const int section_max,
                                                                                        const vector1i &parameterization_section,
                                                                                        const real *concentration_aer_number,
                                                                                        const real *concentration_aer_mass,
                                                                                        const real *concentration_gas,
                                                                                        vector1r &condensation_coefficient,
                                                                                        vector1r &rate_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        // Start index in rate mass.
        int n = j * ClassSpecies::Nspecies_;

        // Start index in equilibrium array.
        int o = j * ClassSpeciesEquilibrium::Nspecies_;

        // Start index in condensation specific arrays.
        int k = j * ClassSpecies::Ngas_;

        // If enough particles.
        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Compute kernel coefficient for this section.
            vector1r coefficient(ClassSpecies::Ngas_, real(0));
            vector1r rate_aer_mass_local(ClassSpecies::Ngas_, real(0));

            for (int l = 0; l < ClassSpecies::Ngas_; l++)
              {
                const int &m = ClassSpecies::semivolatile_[l];

                // The equilibrium gas surface concentration
                // is set to a negative value if it has no physical meaning
                // from previous thermodynamic computation (e.g. no absorbing material).
                if (ClassAerosolData::equilibrium_gas_surface_[k + l] >= real(0))
                  {
                    coefficient[l] = concentration_aer_number[j]
                      * ComputeCoefficient(ClassAerosolData::diameter_[j],
                                           ClassAerosolData::surface_[j],
                                           ClassMeteorologicalData::gas_quadratic_mean_velocity_[l],
                                           ClassMeteorologicalData::gas_diffusivity_[l],
                                           ClassSpecies::accomodation_coefficient_[m]);

                    // The phase index where lies the semivolatile species.
                    const int &phase_index = ClassAerosolData::phase_[n + m];

                    // Single particle mass rate. Kelvin effect is already accounted.
                    rate_aer_mass_local[l] = coefficient[l]
                      * (concentration_gas[l] - ClassAerosolData::equilibrium_gas_surface_[k + l]);

                    // Prevent non physical cases, if no species mass in particle, evaporation cannot take place.
                    if (concentration_aer_mass[n + m] == real(0))
                      if (rate_aer_mass_local[l] < real(0))
                        rate_aer_mass_local[l] = real(0);
                  }
              }

            // If particle is wet, eventually compute H+ flux correction for aqueous phase.
            // If it is dry, also correct rate and equilibrium gas concentrations for inorganic salts.
            if (ClassAerosolData::liquid_water_content_[j] > real(0))
              (this->flux_correction_aqueous_)->ComputeFluxCorrection(ClassAerosolData::equilibrium_aer_internal_.data() + o,
                                                                      concentration_gas,
                                                                      coefficient,
                                                                      ClassAerosolData::equilibrium_gas_surface_.data() + k,
                                                                      rate_aer_mass_local);
            else
              (this->flux_correction_dry_)->ComputeFluxCorrection(ClassMeteorologicalData::temperature_,
                                                                  coefficient,
                                                                  ClassAerosolData::equilibrium_aer_internal_.data() + o,
                                                                  concentration_gas,
                                                                  ClassAerosolData::equilibrium_gas_surface_.data() + k,
                                                                  rate_aer_mass_local);

            // Send back condensation coefficient and rate.
            for (int l = 0; l < ClassSpecies::Ngas_; l++)
              {
                condensation_coefficient[k + l] = coefficient[l];
                rate_aer_mass[n + ClassSpecies::semivolatile_[l]] = rate_aer_mass_local[l];
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_CONDENSATION_DIFFUSION_LIMITED_SOOT_CXX
#endif
