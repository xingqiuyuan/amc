// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_AVERAGE_CXX

#include "ClassParameterizationSurfaceTensionAverage.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationSurfaceTensionAverage::ClassParameterizationSurfaceTensionAverage(Ops::Ops &ops, const string name)
    : ClassParameterizationSurfaceTensionBase(ops, name)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate surface tension average parameterization." << endl; 
#endif

    // Proxy from ClassSpecies.
    surface_tension_ = ClassSpecies::surface_tension_;
    molar_mass_ = ClassSpecies::molar_mass_;

    return;
  }


  // Destructor.
  ClassParameterizationSurfaceTensionAverage::~ClassParameterizationSurfaceTensionAverage()
  {
    return;
  }


  // Clone.
  ClassParameterizationSurfaceTensionAverage* ClassParameterizationSurfaceTensionAverage::clone() const
  {
    return new ClassParameterizationSurfaceTensionAverage(*this);
  }


  // Copy.
  void ClassParameterizationSurfaceTensionAverage::Copy(const ClassParameterizationSurfaceTensionBase *param)
  {
    surface_tension_ = reinterpret_cast<const ClassParameterizationSurfaceTensionAverage*>(param)->surface_tension_;
    molar_mass_ = reinterpret_cast<const ClassParameterizationSurfaceTensionAverage*>(param)->molar_mass_;

    ClassParameterizationSurfaceTensionBase::Copy(*param);
  }


  // Compute surface tension for AMC.
  void ClassParameterizationSurfaceTensionAverage::ComputeSurfaceTension(const int &section_min,
                                                                         const int &section_max,
                                                                         const vector1i &parameterization_section,
                                                                         const real *concentration_aer_number,
                                                                         const real *concentration_aer_mass) const
  {
    const int Nsection = int(parameterization_section.size());

    // Whether parameterization manages all phases or only one.
    if (this->phase_index_ < 0)
      {
        // Average phase molar mass.
        vector1r surface_tension(ClassPhase::Nphase_, real(0)),
          mol_total(ClassPhase::Nphase_, real(0));

        for (int i = 0; i < Nsection; i++)
          {
            const int j = parameterization_section[i];

            // Avoid sections we do not want to compute.
            if (j < section_min || j >= section_max)
              continue;

            if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
              {
                // Index of concentration and phase vectors.
                const int k = j * ClassSpecies::Nspecies_;

                // Index of surface tension vector.
                const int l = j * ClassPhase::Nphase_;

                // If particle is solid, inorganic species may be solid salt
                // for which Kelvin effect has (as far as I know) little meaning,
                // salt are like polygons, i.e. flat surfaces ...
                for (int m = 0; m < ClassSpecies::Nspecies_; m++)
                  {
                    const int n = k + m;
                    const int phase_index = ClassAerosolData::phase_[n];

                    if (phase_index >= 0)
                      {
#ifdef AMC_WITH_LAYER
                        const real mol_fraction = concentration_aer_mass[n] / molar_mass_[m]
                          * ClassAerosolData::layer_repartition_[n].back();
#else
                        const real mol_fraction = concentration_aer_mass[n] / molar_mass_[m];
#endif
                        surface_tension[phase_index] += mol_fraction * surface_tension_[m];
                        mol_total[phase_index] += mol_fraction;
                      }
                  }

                // Add liquid water content to the aqueous phase, if accounted and if any.
                if (ClassPhase::aqueous_phase_index_ >= 0)
                  if (ClassAerosolData::liquid_water_content_[j] > real(0))
                    {
                      const real mol_fraction = ClassAerosolData::liquid_water_content_[j] / AMC_WATER_MOLAR_MASS;
                      surface_tension[ClassPhase::aqueous_phase_index_] +=
                        ClassMeteorologicalData::water_surface_tension_ * mol_fraction;
                      mol_total[ClassPhase::aqueous_phase_index_] += mol_fraction;
                    }

                // Beware of empty phases.
                for (int m = 0; m < ClassPhase::Nphase_; m++)
                  if (mol_total[m] > real(0))
                    ClassAerosolData::surface_tension_[l + m] = surface_tension[m] / mol_total[m];
              }
          }
      }
    else
      {
        // Average phase molar mass.
        real surface_tension(real(0)), mol_total(real(0));

        for (int i = 0; i < Nsection; i++)
          {
            const int j = parameterization_section[i];

            // Avoid sections we do not want to compute.
            if (j < section_min || j >= section_max)
              continue;

            if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
              {
                // Index of concentration and phase vectors.
                const int k = j * ClassSpecies::Nspecies_;

                // Index of surface tension vector.
                const int l = j * ClassPhase::Nphase_;

                // If particle is solid, inorganic species may be solid salt
                // for which Kelvin effect has (as far as I know) little meaning,
                // salt are like polygons, i.e. flat surfaces ...
                for (int m = 0; m < ClassSpecies::Nspecies_; m++)
                  {
                    const int n = k + m;
                    const int phase_index = ClassAerosolData::phase_[n];

                    // This filters to the only phase which is manages
                    // and also filters solids as this->phase_index_ >= 0.
                    if (phase_index == this->phase_index_)
                      {
#ifdef AMC_WITH_LAYER
                        const real mol_fraction = concentration_aer_mass[n] / molar_mass_[m]
                          * ClassAerosolData::layer_repartition_[n].back();
#else
                        const real mol_fraction = concentration_aer_mass[n] / molar_mass_[m];
#endif
                        surface_tension += mol_fraction * surface_tension_[m];
                        mol_total += mol_fraction;
                      }
                  }

                // Add liquid water content to the aqueous phase, if accounted and if any.
                // If the managed phase is aqueous then its index is equal to the aqueous
                // phase index, if theres is no aqueous phase in AMC (not to say LWC = 0)
                // then the aqueous phase index is < 0, which is also filtered.
                if (ClassPhase::aqueous_phase_index_ == this->phase_index_)
                  if (ClassAerosolData::liquid_water_content_[j] > real(0))
                    {
                      const real mol_fraction = ClassAerosolData::liquid_water_content_[j] / AMC_WATER_MOLAR_MASS;
                      surface_tension += ClassMeteorologicalData::water_surface_tension_ * mol_fraction;
                      mol_total += mol_fraction;
                    }

                // Beware of empty phases.
                if (mol_total > real(0))
                  ClassAerosolData::surface_tension_[l + this->phase_index_] = surface_tension / mol_total;
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_AVERAGE_CXX
#endif
