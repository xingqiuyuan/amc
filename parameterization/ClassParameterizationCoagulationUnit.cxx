// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_UNIT_CXX

#include "ClassParameterizationCoagulationUnit.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCoagulationUnit::ClassParameterizationCoagulationUnit()
    : ClassParameterizationCoagulationBase("Unit", 0), constant_(real(1))
  {
    return;
  }

  ClassParameterizationCoagulationUnit::ClassParameterizationCoagulationUnit(Ops::Ops &ops)
    : ClassParameterizationCoagulationBase("Unit", 0), constant_(real(1))
  {
    constant_ = ops.Get<real>("constant", "", constant_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Unit coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "constant = " << constant_ << endl;
#endif

    return;
  }

  // Destructor.
  ClassParameterizationCoagulationUnit::~ClassParameterizationCoagulationUnit()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationUnit* ClassParameterizationCoagulationUnit::clone() const
  {
    return new ClassParameterizationCoagulationUnit(*this);
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationUnit::KernelTable(const vector1r &variable) const
  {
    return constant_;
  }


  // Compute unit kernel.
  void ClassParameterizationCoagulationUnit::ComputeKernel(const vector2i &couple,
                                                           Array<real, 2> &kernel) const
  {
    int Ncouple = int(couple.size());
    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        kernel(g1, g2) = constant_;
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_UNIT_CXX
#endif
