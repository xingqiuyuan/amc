// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_SEINFELD_CXX

#include "ClassParameterizationCollisionEfficiencySeinfeld.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCollisionEfficiencySeinfeld::ClassParameterizationCollisionEfficiencySeinfeld()
    : ClassParameterizationCollisionEfficiencyBase("Seinfeld")
  {
    return;
  }


  // Destructor.
  ClassParameterizationCollisionEfficiencySeinfeld::~ClassParameterizationCollisionEfficiencySeinfeld()
  {
    return;
  }


  // Clone.
  ClassParameterizationCollisionEfficiencySeinfeld* ClassParameterizationCollisionEfficiencySeinfeld::clone() const
  {
    return new ClassParameterizationCollisionEfficiencySeinfeld(*this);
  }


  // Compute collision efficiency.
  real ClassParameterizationCollisionEfficiencySeinfeld::Compute(const real &diameter1,
                                                                 const real &diameter2,
                                                                 const real &slip_flow_correction1,
                                                                 const real &slip_flow_correction2,
                                                                 const real &terminal_velocity1,
                                                                 const real &terminal_velocity2,
                                                                 const real &temperature,
                                                                 const real &air_free_mean_path,
                                                                 const real &air_density,
                                                                 const real &air_dynamic_viscosity,
                                                                 const real &water_dynamic_viscosity) const
  {
    // Stokes number of smallest particle. 2 * 1.e6 / G
    real St = real(2.038735983690112e5) * abs(terminal_velocity1 - terminal_velocity2);

    // Reynolds number of largest particle. Based on radius.
    real Re = real(5.e-7) * air_density / air_dynamic_viscosity;

    // Schmidt number of smallest particle. (3 * Pi) / Kb * 1.e-6
    real Sc = 6.826339877867116e17 * air_dynamic_viscosity * air_dynamic_viscosity / (air_density * temperature);

    // Diameter ratio.
    real ratio_diameter;

    if (diameter2 >= diameter1)
      {
        ratio_diameter = diameter1 / diameter2;
        St *= terminal_velocity1 / diameter2;
        Re *= diameter2 * terminal_velocity2;
        Sc *= diameter1 / slip_flow_correction1;
      }
    else
      {
        ratio_diameter = diameter2 / diameter1;
        St *= terminal_velocity2 / diameter1;
        Re *= diameter1 * terminal_velocity1;
        Sc *= diameter2 / slip_flow_correction2;
      }

    // Ratio between water and air dynamic viscosity.
    real ratio_viscosity_inv = air_dynamic_viscosity / water_dynamic_viscosity;

    // Parameterization.
    real Re_log = log(real(1) + Re);
    real S_star = (real(1.2) + Re_log * 0.08333333333333333) / (real(1) + Re_log);
    real Re_square = sqrt(Re);

    real brownian_diffusion = (real(1) + Re_square * (real(0.4) * pow(Sc, 0.33333333333333333)
                                                      + real(0.16) * sqrt(Sc))) / (Re * Sc);

    real interception = ratio_diameter * (ratio_viscosity_inv + (real(1) + real(2) * Re_square) * ratio_diameter);

    real impaction = St - S_star + 0.666666666666666666;
    if (impaction > real(0))
      impaction = (St - S_star) / impaction;

    real collision_efficiency = real(4) * (brownian_diffusion + interception);

    if (impaction > real(0))
      collision_efficiency += impaction * sqrt(impaction);

    return collision_efficiency < real(1) ? collision_efficiency : real(1);
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_SEINFELD_CXX
#endif
