// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_VOID_CXX

#include "ClassParameterizationCoagulationVoid.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationCoagulationVoid::ClassParameterizationCoagulationVoid()
    : ClassParameterizationCoagulationBase("Void", 0)
  {
    return;
  }


  // Destructor.
  ClassParameterizationCoagulationVoid::~ClassParameterizationCoagulationVoid()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationVoid* ClassParameterizationCoagulationVoid::clone() const
  {
    return new ClassParameterizationCoagulationVoid(*this);
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationVoid::KernelTable(const vector1r &variable) const
  {
    return real(0);
  }


  // Compute void kernel.
  void ClassParameterizationCoagulationVoid::ComputeKernel(const vector2i &couple,
                                                           Array<real, 2> &kernel) const
  {
    int Ncouple = int(couple.size());
    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        kernel(g1, g2) = real(0);
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_VOID_CXX
#endif
