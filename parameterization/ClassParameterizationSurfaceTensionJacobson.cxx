// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_JACOBSON_CXX

#include "ClassParameterizationSurfaceTensionJacobson.hxx"

namespace AMC
{
  // Constructors.
  ClassParameterizationSurfaceTensionJacobson::ClassParameterizationSurfaceTensionJacobson(Ops::Ops &ops)
    : ClassParameterizationSurfaceTensionBase(ops, "Jacobson"), Norganic_aqueous_(0), Ninorganic_ionic_(0),
      logarithm_cutoff_(real(PARAMETERIZATION_SURFACE_TENSION_LOGARITHM_CUTOFF_DEFAULT))
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate surface tension parameterization"
                         << " for aqueous phase from Jacobson's book section 16.2.3.1 (around page 533)." << endl; 
#endif

    if (ClassPhase::aqueous_phase_index_ < 0)
      throw AMC::Error("There seems to have no aqueous phase in model configuration.");

    this->phase_index_ = ClassPhase::aqueous_phase_index_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Set phase index of Jacobson's parameterization"
                         << "to that of aqueous phase : phase_index = " << this->phase_index_ << endl;
#endif

    // Stores index of inorganic species which are in ionic form.
    for (int i = 0; i < ClassSpeciesEquilibrium::Nspecies_; i++)
      if (! ClassSpeciesEquilibrium::is_organic_[i] &&
          ClassSpeciesEquilibrium::electric_charge_[i] != 0)
        inorganic_ionic_.push_back(i);

    Ninorganic_ionic_ = int(inorganic_ionic_.size());

    molar_mass_inorganic_ionic_.resize(Ninorganic_ionic_);
    for (int i = 0; i < Ninorganic_ionic_; i++)
      molar_mass_inorganic_ionic_[i] = ClassSpeciesEquilibrium::molar_mass_[inorganic_ionic_[i]];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Ninorganic_ionic = " << Ninorganic_ionic_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "inorganic_ionic = " << inorganic_ionic_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset()
                         << "molar_mass_inorganic_ionic = " << molar_mass_inorganic_ionic_ << endl;
#endif

    // Stores index of species which are organics and may partition into aqueous phase.
    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (ClassSpecies::is_organic_[i])
        {
          bool in_aqueous_phase(false);

          for (int j = 0; j < int(ClassSpecies::phase_[i].size()); j++)
            if (ClassSpecies::phase_[i][j] == ClassPhase::aqueous_phase_index_)
              {
                in_aqueous_phase = true;
                break;
              }

          if (in_aqueous_phase)
            organic_aqueous_.push_back(i);
        }

    Norganic_aqueous_ = int(organic_aqueous_.size());

    factor_organic_aqueous_.resize(Norganic_aqueous_);
    for (int i = 0; i < Norganic_aqueous_; i++)
      factor_organic_aqueous_[i] = ClassSpecies::molar_mass_[organic_aqueous_[i]]
        / ClassSpecies::carbon_number_[organic_aqueous_[i]];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Norganic_aqueous = " << Norganic_aqueous_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "organic_aqueous = " << organic_aqueous_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "factor_organic_aqueous = " << factor_organic_aqueous_ << endl;
#endif

    // The value below which the logarithm of 1 + x is approximated as x - x^2 / 2 + x^3 / 3.
    logarithm_cutoff_ = ops.Get<real>("logarithm_cutoff", "", logarithm_cutoff_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "logarithm_cutoff = " << logarithm_cutoff_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationSurfaceTensionJacobson::~ClassParameterizationSurfaceTensionJacobson()
  {
    return;
  }


  // Clone.
  ClassParameterizationSurfaceTensionJacobson* ClassParameterizationSurfaceTensionJacobson::clone() const
  {
    return new ClassParameterizationSurfaceTensionJacobson(*this);
  }


  // Copy.
  void ClassParameterizationSurfaceTensionJacobson::Copy(const ClassParameterizationSurfaceTensionBase *param)
  {
    Ninorganic_ionic_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->Ninorganic_ionic_;
    inorganic_ionic_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->inorganic_ionic_;
    molar_mass_inorganic_ionic_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->molar_mass_inorganic_ionic_;
    Norganic_aqueous_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->Norganic_aqueous_;
    organic_aqueous_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->organic_aqueous_;
    factor_organic_aqueous_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->factor_organic_aqueous_;
    logarithm_cutoff_ = reinterpret_cast<const ClassParameterizationSurfaceTensionJacobson*>(param)->logarithm_cutoff_;

    ClassParameterizationSurfaceTensionBase::Copy(*param);
  }


  // Compute surface tension for AMC.
  void ClassParameterizationSurfaceTensionJacobson::ComputeSurfaceTension(const int &section_min,
                                                                          const int &section_max,
                                                                          const vector1i &parameterization_section,
                                                                          const real *concentration_aer_number,
                                                                          const real *concentration_aer_mass) const
  {
    int Nsection = int(parameterization_section.size());

    for (int i = 0; i < Nsection; i++)
      {
        const int j = parameterization_section[i];

        // Avoid sections we do not want to compute.
        if (j < section_min || j >= section_max)
          continue;

        if (concentration_aer_number[j] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Index of concentration and phase vectors.
            const int k = j * ClassSpecies::Nspecies_;

            // This parameterization is meaningless if no liquid water content.
            if (ClassAerosolData::liquid_water_content_[j] > real(0))
              {
                const real liquid_water_content_l_m3 = ClassAerosolData::liquid_water_content_[j]
                  * AMC_CONVERT_LWC_FROM_UGM3_TO_LM3;

                // If an aqueous phase is present, consider all aqueous organics
                // absorbed in, we do not check the ClassAerosolData::phase_ field.
                //
                // The factor_organic_aqueous_ vector contains also the translation
                // from mol of organics to mol of carbon.
                real carbon_molality(real(0));
#ifdef AMC_WITH_LAYER
                for (int l = 0; l < Norganic_aqueous_; l++)
                  {
                    const int m = k + organic_aqueous_[l];
                    carbon_molality += ClassAerosolData::layer_repartition_[m].back()
                      * concentration_aer_mass[m] / factor_organic_aqueous_[l];
                  }
#else
                for (int l = 0; l < Norganic_aqueous_; l++)
                  carbon_molality += concentration_aer_mass[k + organic_aqueous_[l]] / factor_organic_aqueous_[l];
#endif
                carbon_molality /= liquid_water_content_l_m3;

                real inorganic_ionic_molality(real(0));
#ifdef AMC_WITH_LAYER
                for (int l = 0; l < Ninorganic_ionic_; l++)
                  {
                    const int m = k + inorganic_ionic_[l];
                    inorganic_ionic_molality += ClassAerosolData::layer_repartition_[m].back()
                      * concentration_aer_mass[m] / molar_mass_inorganic_ionic_[l];
                  }
#else
                for (int l = 0; l < Ninorganic_ionic_; l++)
                  inorganic_ionic_molality += concentration_aer_mass[k + inorganic_ionic_[l]] / molar_mass_inorganic_ionic_[l];
#endif
                inorganic_ionic_molality /= liquid_water_content_l_m3;

                // Log factor for carbon surface tension formula effect.
                real carbon_molality_log = carbon_molality * real(628.14);

                // Compute logarithm only if is is worth.
                if (carbon_molality_log < logarithm_cutoff_)
                  carbon_molality_log *= (real(1) + carbon_molality_log * (carbon_molality_log * AMC_FRAC3 - real(0.5)));
                else
                  carbon_molality_log = log(real(1) + carbon_molality_log);

                // At last, make use of Jacobson's book formula, assuming carbon and inorganic_ionic effects are addtitive.
                ClassAerosolData::surface_tension_[j * ClassPhase::Nphase_ + ClassPhase::aqueous_phase_index_] =
                  ClassMeteorologicalData::water_surface_tension_ + real(1.7) * inorganic_ionic_molality
                  - real(0.0187) * ClassMeteorologicalData::temperature_ * carbon_molality_log;
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_PARAMETERIZATION_SURFACE_TENSION_JACOBSON_CXX
#endif
