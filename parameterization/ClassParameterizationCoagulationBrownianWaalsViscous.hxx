// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_WAALS_VISCOUS_HXX

#define AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_NUMBER_QUADRATURE_DEFAULT 50
#define AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_HAMAKER_CONSTANT_DEFAULT    2.e-19 // Default Hamaker constant J.

namespace AMC
{
  class ClassParameterizationCoagulationBrownianWaalsViscous : public ClassParameterizationCoagulationBrownian
  {
  private:

    /*!< Number of quadrature points.*/
    bool with_viscous_effect_;

    /*!< Number of quadrature points.*/
    int Nquadrature_;

    /*!< Hamaker constant.*/
    real hamaker_;

    /*!< Legendre quadrature points.*/
    vector1r zero_;

    /*!< Legendre quadrature weights.*/
    vector1r weight_;

    /*!< Working values for faster quadrature computation.*/
    vector1r zero_square_;
    vector1r weight_div_zero_square_;
    vector1r zero_inv_;
    vector1r y_, y1_, y1p_;

    /*!< Function computing the correction factor for the continuous regime with viscous forces.*/
    real compute_correction_factor_continuous_viscous(const real &diameter1, const real &diameter2, const real &temperature) const;

    /*!< Function computing the correction factor for the continuous regime.*/
    real compute_correction_factor_continuous(const real &diameter1, const real &diameter2, const real &temperature) const;

    /*!< Function computing the correction factor for the free molecular regime.*/
    real compute_correction_factor_free_molecular(const real &diameter1, const real &diameter2, const real &temperature) const;

  public:

    /*!< Constructors.*/
    ClassParameterizationCoagulationBrownianWaalsViscous();
    ClassParameterizationCoagulationBrownianWaalsViscous(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationCoagulationBrownianWaalsViscous();

    /*!< Clone.*/
    ClassParameterizationCoagulationBrownianWaalsViscous* clone() const;

    /*!< Copy.*/
    void Copy(const ClassParameterizationCoagulationBase *param);

    /*!< Get methods.*/
    real GetHamaker() const;
    real GetHamakerRelative(const real &temperature) const;
    int GetNquadrature() const;
    bool IsViscousEffect() const;

    /*!< Set methods.*/
    void SetHamaker(const real hamaker = AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_HAMAKER_CONSTANT_DEFAULT);
    void SetNquadrature(const int Nquadrature = AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_NUMBER_QUADRATURE_DEFAULT);
    void ToggleViscousEffect();

    /*!< Compute correction factor.*/
    real CorrectionFactorContinuous(const real &diameter1, const real &diameter2, const real &temperature) const;

    real CorrectionFactorFreeMolecular(const real &diameter1, const real &diameter2,
                                       const real &temperature) const;

    real CorrectionFactorTransition(const real &diameter1, const real &diameter2,
                                    const real &knudsen1, const real &knudsen2,
                                    const real &mass1, const real &mass2,
                                    const real &temperature,
                                    const real &air_dynamic_viscosity) const;

    real CorrectionFactorTable(const vector<real> &variable) const;

    /*!< Compute brownian kernel.*/
    real KernelContinuous(const real &diameter1, const real &diameter2,
                          const real &knudsen1, const real &knudsen2,
                          const real &temperature, const real &air_dynamic_viscosity) const;

    real KernelFreeMolecular(const real &diameter1, const real &diameter2,
                             const real &mass1, const real &mass2,
                             const real &temperature) const;

    real KernelTransition(const real &diameter1, const real &diameter2,
                          const real &knudsen1, const real &knudsen2,
                          const real &mass1, const real &mass2,
                          const real &temperature, const real &air_dynamic_viscosity) const;

    /*!< Compute kernel for tabulation.*/
    real KernelTable(const vector<real> &variable) const;

#ifndef SWIG
    /*!< Compute kernel for AMC.*/
    void ComputeKernel(const vector2i &couple,
                       Array<real, 2> &kernel) const;

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    /*!< Compute kernel for NPF.*/
    void ComputeKernel(vector2r &kernel) const;

    void KernelTable(const vector1r &diameter, vector2r &kernel) const;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_WAALS_VISCOUS_HXX
#endif
