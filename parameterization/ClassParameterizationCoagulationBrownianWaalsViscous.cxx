// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_WAALS_VISCOUS_CXX

#include "ClassParameterizationCoagulationBrownianWaalsViscous.hxx"

namespace AMC
{
  // Function computing the correction factor for the continuous regime with viscous forces.
  inline real ClassParameterizationCoagulationBrownianWaalsViscous::
  compute_correction_factor_continuous_viscous(const real &diameter1,
                                               const real &diameter2,
                                               const real &temperature) const
  {
    real radius1 = diameter1 * real(0.5);
    real radius2 = diameter2 * real(0.5);
    real radius_sum = radius1 + radius2;
    real radius_diff = radius1 - radius2;
    real u = radius1 * radius2 / (radius_sum * radius_sum);
    real u2 = real(2) * u;
    real a = real(2.6) * sqrt(u);
    real v = radius_diff / radius_sum;
    v *= v;

    real hamaker_div_boltzmann_energy = hamaker_
      / (AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * real(6));

    real factor_inv(real(0));
    for (int i = 0; i < Nquadrature_; i++)
      {
        const real &x = zero_[i];
        const real &x2 = zero_square_[i];
        const real &y = y_[i];
        const real &y1 = y1_[i];
        real y2 = x2 / (real(1) - v * x2);

        factor_inv += (real(1) + u * (a * sqrt(y) + y))
          * exp(- hamaker_div_boltzmann_energy
                * (u2 * (y1 + y2) + log(y2 / y1)))
          * weight_[i];
      }

    return real(1) / factor_inv;
  }


  inline real ClassParameterizationCoagulationBrownianWaalsViscous::
  compute_correction_factor_continuous(const real &diameter1,
                                       const real &diameter2,
                                       const real &temperature) const
  {
    real radius1 = diameter1 * real(0.5);
    real radius2 = diameter2 * real(0.5);
    real radius_sum = radius1 + radius2;
    real radius_diff = radius1 - radius2;
    real u = radius1 * radius2 / (radius_sum * radius_sum);
    real u2 = real(2) * u;
    real v = radius_diff / radius_sum;
    v *= v;

    real hamaker_div_boltzmann_energy = hamaker_
      / (AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * real(6));

    real factor_inv(real(0));
    for (int i = 0; i < Nquadrature_; i++)
      {
        const real &x = zero_[i];
        const real &x2 = zero_square_[i];
        const real &y1 = y1_[i];
        real y2 = x2 / (real(1) - v * x2);

        factor_inv += exp(- hamaker_div_boltzmann_energy
                          * (u2 * (y1 + y2) + log(y2 / y1)))
          * weight_[i];
      }

    return real(1) / factor_inv;
  }


  // Function computing the correction factor for the free molecular regime.
  inline real ClassParameterizationCoagulationBrownianWaalsViscous::
  compute_correction_factor_free_molecular(const real &diameter1,
                                           const real &diameter2,
                                           const real &temperature) const
  {
    real radius1 = diameter1 * real(0.5);
    real radius2 = diameter2 * real(0.5);
    real radius_sum = radius1 + radius2;
    real radius_diff = radius1 - radius2;
    real u = real(2) * radius1 * radius2 / (radius_sum * radius_sum);
    real v = radius_diff / radius_sum;
    v *= v;

    real hamaker_div_boltzmann_energy = hamaker_
      / (AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * real(6));

    real factor(real(0));
    for (int i = 0; i < Nquadrature_; i++)
      {
        const real &x = zero_[i];
        const real &x2 = zero_square_[i];
        const real &y1 = y1_[i];
        const real &y1p = y1p_[i];
        real y2 = x2 / (real(1) - v * x2);
        real y2p = y2 * (real(1) + v * y2) * zero_inv_[i];

        real g = u * (y1p * (real(1) + real(2) * y1) +
                      y2p * (real(1) + real(2) * v * y2))
          + v * y2p - y1p;
        real h = log(y2 / y1) - u * (y1 * y1 + v * y2 * y2) + y1 - v * y2;

        factor += g * exp(hamaker_div_boltzmann_energy * h) * weight_div_zero_square_[i];
      }

    return factor * hamaker_div_boltzmann_energy * real(2);
  }


  // Constructors.
  ClassParameterizationCoagulationBrownianWaalsViscous::ClassParameterizationCoagulationBrownianWaalsViscous()
    : ClassParameterizationCoagulationBrownian("Waals/Viscous brownian"), with_viscous_effect_(true),
      hamaker_(AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_HAMAKER_CONSTANT_DEFAULT),
      Nquadrature_(AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_NUMBER_QUADRATURE_DEFAULT)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Brownian Waals/Viscous coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "with viscous effect = "
                         << (with_viscous_effect_ ? "yes" : "no") << endl;
#endif

    // So called Hamaker constant.
    SetHamaker();

    // Quadrature needed to compute Van der Waals effects.
    SetNquadrature();

    return;
  }

  ClassParameterizationCoagulationBrownianWaalsViscous::ClassParameterizationCoagulationBrownianWaalsViscous(Ops::Ops &ops)
    : ClassParameterizationCoagulationBrownian(ops, "Waals/viscous brownian"), with_viscous_effect_(true),
      hamaker_(AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_HAMAKER_CONSTANT_DEFAULT),
      Nquadrature_(AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_NUMBER_QUADRATURE_DEFAULT)
  {
    // Whether to take into account Viscous forces.
    with_viscous_effect_ = ops.Get<bool>("with_viscous_effect", "", with_viscous_effect_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Instantiate Brownian Waals/Viscous coagulation." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "with viscous effect = "
                         << (with_viscous_effect_ ? "yes" : "no") << endl;
#endif

    // So called Hamaker constant.
    SetHamaker(ops.Get<real>("hamaker", "", AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_HAMAKER_CONSTANT_DEFAULT));

    // Quadrature needed to compute Van der Waals effects.
    SetNquadrature(ops.Get<int>("Nquadrature", "", AMC_COAGULATION_BROWNIAN_WAALS_VISCOUS_NUMBER_QUADRATURE_DEFAULT));

    return;
  }


  // Destructor.
  ClassParameterizationCoagulationBrownianWaalsViscous::~ClassParameterizationCoagulationBrownianWaalsViscous()
  {
    return;
  }


  // Clone.
  ClassParameterizationCoagulationBrownianWaalsViscous* ClassParameterizationCoagulationBrownianWaalsViscous::clone() const
  {
    return new ClassParameterizationCoagulationBrownianWaalsViscous(*this);
  }


  // Copy.
  void ClassParameterizationCoagulationBrownianWaalsViscous::Copy(const ClassParameterizationCoagulationBase *param)
  {
    ClassParameterizationCoagulationBrownian::Copy(param);

    with_viscous_effect_ = reinterpret_cast<const ClassParameterizationCoagulationBrownianWaalsViscous*>(param)->with_viscous_effect_;

    SetHamaker(reinterpret_cast<const ClassParameterizationCoagulationBrownianWaalsViscous*>(param)->hamaker_);
    SetNquadrature(reinterpret_cast<const ClassParameterizationCoagulationBrownianWaalsViscous*>(param)->Nquadrature_);
  }


  // Get methods.
  real ClassParameterizationCoagulationBrownianWaalsViscous::GetHamaker() const
  {
    return hamaker_;
  }


  real ClassParameterizationCoagulationBrownianWaalsViscous::GetHamakerRelative(const real &temperature) const
  {
    return hamaker_ / (AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature);
  }


  int ClassParameterizationCoagulationBrownianWaalsViscous::GetNquadrature() const
  {
    return Nquadrature_;
  }


  bool ClassParameterizationCoagulationBrownianWaalsViscous::IsViscousEffect() const
  {
    return with_viscous_effect_;
  }


  // Set methods.
  void ClassParameterizationCoagulationBrownianWaalsViscous::SetHamaker(const real hamaker)
  {
    hamaker_ = hamaker;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "hamaker = " << hamaker_ << endl;
#endif
  }


  void ClassParameterizationCoagulationBrownianWaalsViscous::SetNquadrature(const int Nquadrature)
  {
    Nquadrature_ = Nquadrature;

    zero_.assign(Nquadrature_, real(0));
    weight_.assign(Nquadrature_, real(0));

    // Compute zeros and weights for quadrature between 0 and 1.
    compute_gauss_legendre_zero_weight<real>(Nquadrature_, zero_.data() - 1, weight_.data() - 1, real(0), real(1));

    // Set working values.
    zero_square_.assign(Nquadrature_, real(0));
    weight_div_zero_square_.assign(Nquadrature_, real(0));
    zero_inv_.assign(Nquadrature_, real(0));
    y_.assign(Nquadrature_, real(0));
    y1_.assign(Nquadrature_, real(0));
    y1p_.assign(Nquadrature_, real(0));

    for (int i = 0; i < Nquadrature_; i++)
      {
        zero_square_[i] = zero_[i] * zero_[i];
        zero_inv_[i] = real(1) / zero_[i];
        weight_div_zero_square_[i] = weight_[i] / zero_square_[i];
        y_[i] = zero_[i] / (real(1) -  zero_[i]);
        y1_[i] = zero_square_[i] / (real(1) -  zero_square_[i]);
        y1p_[i] = y1_[i] * (real(1) + y1_[i]) * zero_inv_[i];
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Nquadrature = " << Nquadrature_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "zero = " << zero_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "weight = " << weight_ << endl;
#endif
  }


  void ClassParameterizationCoagulationBrownianWaalsViscous::ToggleViscousEffect()
  {
    with_viscous_effect_ = with_viscous_effect_ == false;
  }


  // Compute correction factor.
  real ClassParameterizationCoagulationBrownianWaalsViscous::CorrectionFactorContinuous(const real &diameter1,
                                                                                        const real &diameter2,
                                                                                        const real &temperature) const
  {
    if (with_viscous_effect_)
      return compute_correction_factor_continuous_viscous(diameter1, diameter2, temperature);
    else
      return compute_correction_factor_continuous(diameter1, diameter2, temperature);
  }


  real ClassParameterizationCoagulationBrownianWaalsViscous::CorrectionFactorFreeMolecular(const real &diameter1,
                                                                                           const real &diameter2,
                                                                                           const real &temperature) const
  {
    return compute_correction_factor_free_molecular(diameter1, diameter2, temperature);
  }


  real ClassParameterizationCoagulationBrownianWaalsViscous::CorrectionFactorTransition(const real &diameter1,
                                                                                        const real &diameter2,
                                                                                        const real &knudsen1,
                                                                                        const real &knudsen2,
                                                                                        const real &mass1,
                                                                                        const real &mass2,
                                                                                        const real &temperature,
                                                                                        const real &air_dynamic_viscosity) const
  {
    real correction_factor_continuous;
    if (with_viscous_effect_)
      correction_factor_continuous = compute_correction_factor_continuous_viscous(diameter1, diameter2, temperature);
    else
      correction_factor_continuous = compute_correction_factor_continuous(diameter1, diameter2, temperature);

    real correction_factor_free_molecular = compute_correction_factor_free_molecular(diameter1, diameter2, temperature);

    real f1 = (real(5) + knudsen1 * (real(4) + knudsen1 * (real(6) + real(18) *knudsen1)))
      / (real(5) + knudsen1 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen1));
    real f2 = (real(5) + knudsen2 * (real(4) + knudsen2 * (real(6) + real(18) *knudsen2)))
      / (real(5) + knudsen2 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen2));

    // The 1e6 factor account for conversion from µm to m.
    real diffusion_mutual = real(1e6) * AMC_COAGULATION_BROWNIAN_FRAC3
      * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature
      * (f1 / diameter1 + f2 / diameter2) / (air_dynamic_viscosity * AMC_COAGULATION_PI);

    // The 1e9 factor in 8e9 converts from the particle mass mass1 and mass2 from µg to kg.
    real velocity_mutual = real(8e9) * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT
      * temperature * (real(1) / mass1 + real(1) / mass2) / AMC_COAGULATION_PI;

    // The 1e6 factor account for conversion from µm to m.
    real ratio = real(8e6) * diffusion_mutual / (sqrt(velocity_mutual) * (diameter1 + diameter2));

    return correction_factor_continuous * (real(1) + ratio)
      / (real(1) + ratio * correction_factor_continuous / correction_factor_free_molecular);
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationBrownianWaalsViscous::CorrectionFactorTable(const vector<real> &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];

    // Air dynamic viscosity.
    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);

    // Air free mean path.
    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);

    // Knudsen number.
    real knudsen1 = real(2) * air_free_mean_path / diameter1;
    real knudsen2 = real(2) * air_free_mean_path / diameter2;

    // Particle mass.
    real mass1 = AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3
      * AMC_COAGULATION_PI6 * this->density_fixed_
      * diameter1 * diameter1 * diameter1;
    real mass2 = AMC_COAGULATION_CONVERT_FROM_GCM3_TO_MICROGMICROM3
      * AMC_COAGULATION_PI6 * this->density_fixed_
      * diameter2 * diameter2 * diameter2;

    return CorrectionFactorTransition(diameter1, diameter2,
                                      knudsen1, knudsen2,
                                      mass1, mass2, 
                                      temperature, air_dynamic_viscosity);
  }


  // Compute brownian kernel.
  real ClassParameterizationCoagulationBrownianWaalsViscous::KernelContinuous(const real &diameter1, const real &diameter2,
                                                                              const real &knudsen1, const real &knudsen2,
                                                                              const real &temperature,
                                                                              const real &air_dynamic_viscosity) const
  {
    throw AMC::Error("KernelContinuous() is not relevant for Waals/Viscous forces, use KernelTransition() instead.");
  }


  real ClassParameterizationCoagulationBrownianWaalsViscous::KernelFreeMolecular(const real &diameter1, const real &diameter2,
                                                                                 const real &mass1, const real &mass2,
                                                                                 const real &temperature) const
  {
    throw AMC::Error("KernelFreeMolecular() is not relevant for Waals/Viscous forces, use KernelTransition() instead.");
  }


  real ClassParameterizationCoagulationBrownianWaalsViscous::KernelTransition(const real &diameter1, const real &diameter2,
                                                                              const real &knudsen1, const real &knudsen2,
                                                                              const real &mass1, const real &mass2,
                                                                              const real &temperature,
                                                                              const real &air_dynamic_viscosity) const
  {
    real correction_factor_continuous(0);
    if (with_viscous_effect_)
      correction_factor_continuous = compute_correction_factor_continuous_viscous(diameter1, diameter2, temperature);
    else
      correction_factor_continuous = compute_correction_factor_continuous(diameter1, diameter2, temperature);

    real correction_factor_free_molecular = compute_correction_factor_free_molecular(diameter1, diameter2, temperature);

    real f1 = (real(5) + knudsen1 * (real(4) + knudsen1 * (real(6) + real(18) *knudsen1)))
      / (real(5) + knudsen1 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen1));
    real f2 = (real(5) + knudsen2 * (real(4) + knudsen2 * (real(6) + real(18) *knudsen2)))
      / (real(5) + knudsen2 * (- real(1) + real(AMC_COAGULATION_BROWNIAN_8PLUSPI) * knudsen2));

    // The 1e6 factor account for conversion from µm to m.
    real diffusion_mutual = real(1e6) * AMC_COAGULATION_BROWNIAN_FRAC3
      * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature
      * (f1 / diameter1 + f2 / diameter2) / (air_dynamic_viscosity * AMC_COAGULATION_PI);

    // The 1e9 factor in 8e9 converts from the particle mass mass1 and mass2 from µg to kg.
    real velocity_mutual = real(8e9) * AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT
      * temperature * (real(1) / mass1 + real(1) / mass2) / AMC_COAGULATION_PI;

    // The 1e6 factor account for conversion from µm to m.
    real ratio = real(8e6) * diffusion_mutual / (sqrt(velocity_mutual) * (diameter1 + diameter2));

    return correction_factor_continuous * (real(1) + ratio)
      / (real(1) + ratio * correction_factor_continuous / correction_factor_free_molecular)
      * ClassParameterizationCoagulationBrownian::KernelTransition(diameter1, diameter2,
                                                                   knudsen1, knudsen2,
                                                                   mass1, mass2,
                                                                   temperature, air_dynamic_viscosity);
  }


  // Compute kernel for tabulation.
  real ClassParameterizationCoagulationBrownianWaalsViscous::KernelTable(const vector<real> &variable) const
  {
    // Variables.
    real diameter1 = variable[0];
    real diameter2 = variable[1];
    real temperature = variable[2];
    real pressure = variable[3];

    // Air dynamic viscosity.
    real air_dynamic_viscosity = ClassParameterizationPhysics::ComputeAirDynamicViscosity(temperature);

    // Air free mean path.
    real air_free_mean_path = ClassParameterizationPhysics::ComputeAirFreeMeanPath(temperature, pressure);

    // Knudsen number.
    real knudsen1 = real(2) * air_free_mean_path / diameter1;
    real knudsen2 = real(2) * air_free_mean_path / diameter2;

    // Particle mass.
    real mass1 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter1 * diameter1 * diameter1;
    real mass2 = AMC_COAGULATION_PI6 * this->density_fixed_ * diameter2 * diameter2 * diameter2;

    return KernelTransition(diameter1, diameter2,
                            knudsen1, knudsen2,
                            mass1, mass2, 
                            temperature, air_dynamic_viscosity);
  }


  // Compute kernel for AMC.
  void ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(const vector2i &couple,
                                                                           Array<real, 2> &kernel) const
  {
    const int Ncouple = int(couple.size());

    for (int i = 0; i < Ncouple; i++)
      {
        int g1 = couple[i][0];
        int g2 = couple[i][1];

        const real knudsen1 = real(2) * ClassMeteorologicalData::air_free_mean_path_ / ClassAerosolData::diameter_[g1];
        const real knudsen2 = real(2) * ClassMeteorologicalData::air_free_mean_path_ / ClassAerosolData::diameter_[g2];

        kernel(g1, g2) += KernelTransition(ClassAerosolData::diameter_[g1], ClassAerosolData::diameter_[g2],
                                           knudsen1, knudsen2,
                                           ClassAerosolData::mass_[g1], ClassAerosolData::mass_[g2],
                                           ClassMeteorologicalData::temperature_,
                                           ClassMeteorologicalData::air_dynamic_viscosity_);
      }
  }


#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  // Compute kernel for NPF.
  void ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(vector2r &kernel) const
  {
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      for (int j = 0; j <= i; ++j)
        {
          const int k = NPF::ClassParticle::index_active_[i];
          const int l = NPF::ClassParticle::index_active_[j];

          const real knudsen1 = real(2) * NPF::ClassMeteoData::air_free_mean_path_ / NPF::ClassParticleData::diameter_[k];
          const real knudsen2 = real(2) * NPF::ClassMeteoData::air_free_mean_path_ / NPF::ClassParticleData::diameter_[l];

          // Return kernel in m^3.s^{-1}.
          kernel[i][j] = KernelTransition(NPF::ClassParticleData::diameter_[k],
                                          NPF::ClassParticleData::diameter_[l],
                                          knudsen1, knudsen2,
                                          NPF::ClassParticleData::mass_[k],
                                          NPF::ClassParticleData::mass_[l],
                                          NPF::ClassMeteoData::temperature_,
                                          NPF::ClassMeteoData::air_dynamic_viscosity_);
        }
  }

  void ClassParameterizationCoagulationBrownianWaalsViscous::KernelTable(const vector1r &diameter,
                                                                         vector2r &kernel) const
  {
    const int N = int(diameter.size());

    for (int i1 = 0; i1 < N; ++i1)
      for (int i2 = 0; i2 <= i1; ++i2)
        {
          const real &diameter1 = diameter[i1];
          const real &diameter2 = diameter[i2];

          const real knudsen1 = real(2) * NPF::ClassMeteoData::air_free_mean_path_ / diameter1;
          const real knudsen2 = real(2) * NPF::ClassMeteoData::air_free_mean_path_ / diameter2;

          const real mass1 = this->density_fixed_ * AMC_PI6 * diameter1 * diameter1 * diameter1;
          const real mass2 = this->density_fixed_ * AMC_PI6 * diameter2 * diameter2 * diameter2;

          // Return kernel in m^3.s^{-1}.
          kernel[i1][i2] = KernelTransition(diameter1, diameter2,
                                            knudsen1, knudsen2,
                                            mass1, mass2,
                                            NPF::ClassMeteoData::temperature_,
                                            NPF::ClassMeteoData::air_dynamic_viscosity_);
        }

    // Kernel is symetric.
    for (int i1 = 0; i1 < N; ++i1)
      for (int i2 = i1 + 1; i2 < N; ++i2)
        kernel[i1][i2] = kernel[i2][i1];
  }
#endif
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COAGULATION_BROWNIAN_WAALS_VISCOUS_CXX
#endif
