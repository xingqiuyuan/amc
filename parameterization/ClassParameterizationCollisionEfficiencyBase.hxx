// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_BASE_HXX

#ifndef AMC_WITH_COAGULATION
#define AMC_COAGULATION_PARTICLE_DENSITY_DEFAULT            1.0  // Default particle density in g.cm^{-3}
#endif

namespace AMC
{
  class ClassParameterizationCollisionEfficiencyBase
  {
  public:

    typedef AMC_PARAMETERIZATION_REAL real;

  protected:

    /*!< parameterization name.*/
    string name_;

  public:

    /*!< Constructors.*/
    ClassParameterizationCollisionEfficiencyBase(const string name = "Base");

    /*!< Destructor.*/
    virtual ~ClassParameterizationCollisionEfficiencyBase();

#ifndef SWIG
    /*!< Clone.*/
    virtual ClassParameterizationCollisionEfficiencyBase* clone() const;
#endif

    /*!< Get methods.*/
    string GetName() const;

    /*!< Compute collision efficiency.*/
    virtual real Compute(const real &diameter1,
                         const real &diameter2,
                         const real &slip_flow_correction1,
                         const real &slip_flow_correction2,
                         const real &terminal_velocity1,
                         const real &terminal_velocity2,
                         const real &temperature,
                         const real &air_free_mean_path,
                         const real &air_density,
                         const real &air_dynamic_viscosity,
                         const real &water_dynamic_viscosity) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    real _Test_(const vector<real> &variable) const;
#endif
  };
}

#define AMC_FILE_CLASS_PARAMETERIZATION_COLLISION_EFFICIENCY_BASE_HXX
#endif
