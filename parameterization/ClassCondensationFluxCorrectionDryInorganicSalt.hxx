// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_DRY_INORGANIC_SALT_HXX

#define AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_TEMPERATURE_REFERENCE 298.15
#define AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NGAS 4
#define AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_NSOLID 4
#define AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_PRESSURE_ATM 1.01325e5
#define AMC_FLUX_CORRECTION_DRY_INORGANIC_SALT_IDEAL_GAS_CONSTANT 8.314462175

namespace AMC
{
  class ClassCondensationFluxCorrectionDryInorganicSalt
  {
  public:
    typedef AMC_PARAMETERIZATION_REAL real;

  private:

    /*!< Is static part initiated ?.*/
    static bool is_initiated_;

    /*!< Constant reaction for  NH4NO3s <==> NH3g + HNO3g .*/
    static real XK10_reference_, XK10_;
    static real XK10_coefficient0_, XK10_coefficient1_;

    /*!< Constant reaction for  NH4Cls <==> NH3g + HClg .*/
    static real XK6_reference_, XK6_;
    static real XK6_coefficient0_, XK6_coefficient1_;

    /*!< Constant reaction for  NaCls + HNO3g <==> NaNO3s + HClg adim .*/
    static real XK3_reference_, XK3_, XK4_reference_, XK4_,
      XK8_reference_, XK8_, XK9_reference_, XK9_;
    static real XK3_coefficient0_, XK3_coefficient1_;
    static real XK4_coefficient0_, XK4_coefficient1_;
    static real XK8_coefficient0_, XK8_coefficient1_;
    static real XK9_coefficient0_, XK9_coefficient1_;

    /*!< solid indexes in AMC vector.*/
    vector1i index_solid_;

    /*!< gas indexes in AMC vector.*/
    vector1i index_gas_;

    /*!< The molar weight of gas species.*/
    vector1r molar_mass_;

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Init static data for one step.*/
    static void Update(const real &temperature);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Constructors.*/
    ClassCondensationFluxCorrectionDryInorganicSalt(Ops::Ops &ops);


    /*!< Destructor.*/
    ~ClassCondensationFluxCorrectionDryInorganicSalt();


    /*!< Get methods.*/
    string GetName() const;


    /*!< Compute flux correction when particle is dry.*/
    void ComputeFluxCorrection(const real &temperature,
                               const vector1r &condensation_coefficient,
                               const real *equilibrium_aer_internal,
                               const real *concentration_gas,
                               real *concentration_gas_equilibrium,
                               vector1r &rate_aer_mass) const;
  };
}

#define AMC_FILE_CLASS_CONDENSATION_FLUX_CORRECTION_DRY_INORGANIC_SALT_HXX
#endif
