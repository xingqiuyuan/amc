// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_ADSORPTION_PANKOW_HXX

// Various specific surface area typical values in cm^2.µg^{-1}.
#define MODEL_ADSORPTION_PANKOW_SPECIFIC_SURFACE_AREA_CORN_1971_MIN 0.019
#define MODEL_ADSORPTION_PANKOW_SPECIFIC_SURFACE_AREA_CORN_1971_MAX 0.031
#define MODEL_ADSORPTION_PANKOW_SPECIFIC_SURFACE_AREA_CORN_1971_AVG 0.025
#define MODEL_ADSORPTION_PANKOW_SPECIFIC_SURFACE_AREA_WHITBY_1972 0.05

namespace AMC
{
  class ClassModelAdsorptionPankow : public ClassModelAdsorptionBase
  {
  private:

    /*!< specific_surface_area.*/
    vector1r specific_surface_area_;

  public:

    /*!< Constructors.*/
    ClassModelAdsorptionPankow(Ops::Ops &ops);


    /*!< Destructor.*/
    ~ClassModelAdsorptionPankow();

    /*!< Clone.*/
    ClassModelAdsorptionPankow* clone() const;

    /*!< Get methods.*/
    void GetSpecificSurfaceArea(vector<real> &specific_surface_area) const;

    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;
  };
}

#define AMC_FILE_CLASS_MODEL_ADSORPTION_PANKOW_HXX
#endif
