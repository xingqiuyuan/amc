
isoropia = {
   species = {
      external = {"Na", "H2SO4", "NH3", "HNO3", "HCl"},
      liquid = {"Hp", "Nap", "NH4p", "Clm", "SO42m", "HSO4m", "NO3m", "H2O", "NH3aq", "HClaq", "HNO3aq", "OHm"},
      solid = {"NaNO3", "NH4NO3", "NaCl", "NH4Cl", "Na2SO4", "NH4_2_SO4", "NaHSO4", "NH4HSO4", "NH4_4HSO4_2"}
   },

   molar_mass = {
      external = {23., 98., 17., 63., 36.5},
      liquid = {1., 23., 18., 35.5, 96., 97., 63., 18., 17., 36.5, 63., 17.},
      solid = {85., 80., 58.5, 53.5, 142., 132., 120., 115., 247.}
   }
}