
pankow = {
   species_name = {"BiISO1", "BiISO2", "PYR", "NOND", "PTCA", "POA", "AnBlP", "AnBmP", "BiBlP", "BiBmP"},
   Nsemivolatile = 5,
   molar_mass = {g_mol = {118., 136., 202., 268., 354., 280., 167., 152., 298., 236.}},
   saturation_vapor_pressure = {Pa = {3.17e-3, 1.46e-5, 6.105e-4, 1.605e-3, 5.08e-8}},
   partition_coefficient = {m3_ug = {0.00862, 1.62, 2.009e-2, 5.76e-3, 1.378e2}},
   use_partition_coefficient = {true, true, true, true, true},
   vaporization_enthalpy = {J_mol = {42.e3, 42.e3, 81.35e3, 96.4e3, 127.e3}},
   Nloop = 10,
   epsilon = 1.e-4
}