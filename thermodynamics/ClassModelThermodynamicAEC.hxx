// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_AEC_HXX

extern "C"
{
  void unifac_(int*, int*, int*, double*, double*, double*, double*, double*, double*, double*);
}

#define AEC_WITH_OLIGOMERIZATION_DEFAULT true
#define AEC_WITH_UNIFAC_DEFAULT true
#define AEC_MOLAR_MASS_PRIMARY_FIXED_DEFAULT 280
#define AEC_OLIGOMERIZATION_PH_REFERENCE_DEFAULT 6
#define AEC_UNIFAC_Z_PARAMETER 10
#define AEC_NUMBER_LOOP_A 10
#define AEC_EPSILON_A 1.e-4
#define AEC_NUMBER_LOOP_B 10
#define AEC_EPSILON_B 1.e-4
#define AEC_NUMBER_LOOP_H 5
#define AEC_EPSILON_H 1.e-3
#define AEC_MOLAR_MASS_WATER 18.e6
#define AEC_TEMPERATURE_REFERENCE 298.0
#define AEC_FRACTION_DILUTE_DEFAULT 0.0001
#define AEC_CONVERT_HYDRONIUM_FROM_UGM3_TO_LWM3 1.e3
#define AEC_CONVERT_HYDRONIUM_FROM_LWM3_TO_UGM3 1.e-3
#define AEC_CONVERT_LWC_FROM_UGM3_TO_LM3 1.e-9
#define AEC_CONVERT_LWC_FROM_LM3_TO_UGM3 1.e9
#define AEC_OLIGOMERIZATION_POWER 1.91
#define AEC_HP_MOL_DEFAULT 1.e-6
#define AEC_OLIGOMERIZATION_HP_MOL_MINIMUM 1.e-14
#define AEC_OLIGOMERIZATION_HP_MOL_MAXIMUM 1.e-1
#define AEC_HYDRONIUM_MOLAR_MASS_UGMOL 1.e6
// Constant making all necessary unit conversion for the Henry constant
// from mol.L^{-1}.atm^{-1} to m^3.µg^{-1}.K^{-1} so that
// partition coefficient = (Henry constant) x (this constant) x temperature
// this constant = perfect gas constant (8.314462175) / 1.01325e14
#define AEC_HENRY_CONSTANT_UNIT_CONVERSION 8.21303e-14
#ifdef AEC_WITH_UNIFAC
#define AEC_MOLAR_FRACTION_TINY 1.e-8
#endif

namespace AMC
{
  class ClassModelThermodynamicAEC : virtual public ClassModelThermodynamicBase
  {
  protected:

    /*!< Number of loop for convergence of hydrophilic module.*/
    int Nloop_a_;

    /*!< Number of loop for convergence of hydrophobic module.*/
    int Nloop_b_;

    /*!< Number of loop for convergence of H+ concentration.*/
    int Nloop_h_;

    /*!< Convergence criteria for hydrophilic module.*/
    real epsilon_a_;

    /*!< Convergence criteria for hydrophobic module.*/
    real epsilon_b_;

    /*!< Convergence criteria for H+ concentrations.*/
    real epsilon_h_;

    /*!< Number of semivolatile species.*/
    int Nsemivolatile_;

    /*!< Number of AEC semivolatile hydrophilic species, excluding water.*/
    int Nhydrophilic_semivolatile_;

    /*!< Number of AEC semivolatile hydrophilic species, including water.*/
    int Nhydrophilic_semivolatile_water_;

    /*!< Total number of AEC hydrophilic species (semivolatile + ionic).*/
    int Nhydrophilic_;

    /*!< Number of AEC hydrophobic semivolatile species.*/
    int Nhydrophobic_semivolatile_;

    /*!< Number of AEC hydrophobic primary species.*/
    int Nhydrophobic_primary_;

    /*!< Number of AEC hydrophobic semivolatile and primary species.*/
    int Nhydrophobic_;

    /*!< Total number of AEC hydrophobic semivolatile
      and primary species plus hydrophilic semivolatile.*/
    int Nhydrophobic_total_;

    /*!< Name of hydrophilic AEC species.*/
    vector1s name_hydrophilic_;

    /*!< Name of hydrophobic AEC species.*/
    vector1s name_hydrophobic_;

    /*!< Number of internal species.*/
    int Nspecies_internal_;

    /*!< Index of internal species in AMC managed by the thermodynamic model.*/
    vector2i species_index_internal_;

    /*!< Hydrophilic AEC species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_hydrophilic_;

    /*!< Hydrophobic AEC species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_hydrophobic_;

    /*!< Primary AEC species fixed molar mass in µg.mol^{-1}.*/
    real molar_mass_primary_fixed_;

    /*!< Primary molar fraction : how to partition
      the AMC primary molar mass on AEC primary species.*/
    vector1r primary_fraction_molar_;

    /*!< Number of dissolution reactions for each hydrophilic species.*/
    vector1i Nreaction_dissolution_;

    /*!< Constant of dissolution reactions.*/
    vector2r reaction_dissolution_constant_;

    /*!< Species index of dissolution reactions.*/
    vector2i reaction_dissolution_species_index_;

    /*!< Henry constant of semivolatile hydrophilic AEC species.*/
    vector1r henry_constant_;

    /*!< Deliquescence relative humidity.*/
    vector1r drh_;

    /*!< Number of species undergoing oligomerization.*/
    int Noligomerization_;

    /*!< Index of hydrophilic species undergoin oligomerization.*/
    vector1i oligomerization_species_index_;

    /*!< Oligomerization constant.*/
    vector1r oligomerization_constant_;

    /*!< Oligomerization power (dependence to Hp).*/
    real oligomerization_power_;

    /*!< Reference pH for oligomerization constant.*/
    real oligomerization_pH_reference_;

    /*!< Reference Hp concentration for oligomerization constant.*/
    real oligomerization_hp_mol_reference_;

    /*!< Saturation vapor pressure in Pascal.*/
    vector1r saturation_vapor_pressure_hydrophilic_;

    /*!< Vaporization enthalpy in J.mol^{-1}.*/
    vector1r vaporization_enthalpy_hydrophilic_;

    /*!< Saturation vapor pressure in Pascal.*/
    vector1r saturation_vapor_pressure_hydrophobic_;

    /*!< Vaporization enthalpy in J.mol^{-1}.*/
    vector1r vaporization_enthalpy_hydrophobic_;

    /*! UNIFAC data.*/

    /*!< Infinite dilution gamma coefficient.*/
    vector<double> gamma_infinite_;

    /*!< fixed Unifac parameter.*/
    double Z_;

    int nfunc_a_, nfunc_b_;
    vector<double> rg_a_, rg_b_;
    vector<double> qg_a_, qg_b_;
    vector<int> nu_a_, nu_b_;
    vector<double> A_a_, A_b_;

    /*!< Whether to include oligomerization or not.*/
    bool with_oligomerization_;

    /*!< Whether to use unifac or not.*/
    bool with_unifac_;

    /*!< Number of AMC species viewed as primary by AEC.*/
    int Nhydrophobic_primary_amc_;

    /*!< H+ index : where AEC has to get protons from AMC.*/
    int index_hydronium_;

    /*!< Index of hydrophilic species in AMC, water is not accounted in.*/
    vector1i index_hydrophilic_aerosol_;
    vector1i index_hydrophilic_gas_;
    
    /*!< Index of hydrophobic species in AMC.*/
    vector1i index_hydrophobic_aerosol_;
    vector1i index_hydrophobic_gas_;

    /*!< Index of AMC species considered as primary by AEC.*/
    vector1i index_hydrophobic_primary_amc_;

    /*!< Hydrophilic module.*/
    bool module_hydrophilic_local_(const real &temp,
                                   const real &rh,
                                   vector1r &he,
                                   real &lwc, real &hp,
                                   vector1r &aero,
                                   vector1r &gas) const;

    bool module_hydrophilic_global_(const real &temp,
                                    const real &rh,
                                    vector1r &he,
                                    real &lwc, real &hp,
                                    vector1r &aero,
                                    vector1r &gas) const;

    /*!< Hydrophobic module.*/
    bool module_hydrophobic_local_(const real &temp,
                                   const real &prim_mass,
                                   const real &prim_mw,
                                   const vector1r &vpsat,
                                   const vector1r &aero,
                                   vector1r &gas) const;

    bool module_hydrophobic_global_(const real &temp,
                                    const real &prim_mass,
                                    const real &prim_mw,
                                    const vector1r &vpsat,
                                    vector1r &aero,
                                    vector1r &gas) const;

  public:

    /*!< Constructors.*/
    ClassModelThermodynamicAEC(Ops::Ops &ops, const bool standalone = true, const string name = "aec");

    /*!< Destructor.*/
    virtual ~ClassModelThermodynamicAEC();

    /*!< Get methods.*/
    virtual int GetNspeciesInternal() const;
    int GetNspeciesA() const;
    int GetNspeciesSemivolatileA() const;
    int GetNloopA() const;
    real GetEpsilonA() const;
    int GetNspeciesB() const;
    int GetNspeciesSemivolatileB() const;
    int GetNloopB() const;
    real GetEpsilonB() const;
    bool IsOligomerizationUsed() const;
    bool IsUnifacUsed() const;
    void GetGammaInfiniteDilution(vector<real> &gamma_infinite) const;

    /*!< Set methods.*/
    void SetNloopA(const int &Nloop);
    void SetEpsilonA(const real &epsilon);
    void SetNloopB(const int &Nloop);
    void SetEpsilonB(const real &epsilon);
    void ToggleOligomerization();
    void ToggleUnifac();


    /*!< Clone.*/
    ClassModelThermodynamicAEC* clone() const;


    /*!< Compute gamma coefficients.*/
    void ComputeGammaCoefficientHydrophilic(const real &temperature,
                                            const vector<real> &concentration_aer_mass,
                                            vector<real> &gamma_hydrophilic) const;

    void ComputeGammaCoefficientHydrophobic(const real &temperature,
                                            const vector<real> &concentration_aer_mass,
                                            vector<real> &gamma_hydrophobic) const;

    void ComputeGammaInfiniteDilution(const real fraction_dilute = AEC_FRACTION_DILUTE_DEFAULT,
                                      const real temperature_reference = AEC_TEMPERATURE_REFERENCE);


    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;

    /*!< Compute global equilibrium.*/
    void ComputeGlobalEquilibrium(const int &section_min,
                                  const int &section_max,
                                  const real *concentration_gas,
                                  const real *concentration_aer_mass,
                                  vector1r &concentration_aer_total,
                                  vector1r &equilibrium_aer_internal,
                                  real &liquid_water_content,
                                  vector1r &concentration_aer_equilibrium,
                                  vector1r &concentration_gas_equilibrium) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    void Test(const int &mode,
              const real &temperature,
              const real &relative_humidity,
              const map<string, real> &concentration,
              map<string, real> &output) const;


    void TestAMC(const real &temperature,
                 const real &relative_humidity,
                 const vector<real> &concentration_aer_mass,
                 vector<real> &equilibrium_aer_internal,
                 vector<real> &equilibrium_gas_surface) const;
#endif
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_AEC_HXX
#endif
