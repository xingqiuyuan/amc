// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_PANKOW_CXX

#include "ClassModelThermodynamicPankow.hxx"

namespace AMC
{
  // Local module.
  bool ClassModelThermodynamicPankow::module_local_(const real &temp,
                                                    const vector1r &mw,
                                                    const vector1r &kpart,
                                                    const vector1r &vpsat,
                                                    const vector1r &aero,
                                                    vector1r &gas) const
  {
    // Compute total suspended mass.
    real tsp(real(0));
    for (int i = 0; i < Nspecies_; i++)
      tsp += aero[i];

    if (tsp == real(0))
      return false;

    // Average mol weight.
    real mwa_inv(real(0));
    for (int i = 0; i < Nspecies_; i++)
      mwa_inv += aero[i] / mw[i];
    mwa_inv /= tsp;

    // If we use the saturation vapor pressure,
    // then the partition coefficient implicitely depend
    // on mass concentration through the average molar mass,
    // should not be very sensitive though ...

    // Compute gas equilibrium concentration.
    for (int i = 0; i <  Nspecies_semivolatile_; i++)
      if (use_partition_coefficient_[i])
        gas[i] = aero[i] / (tsp * kpart[i]);
      else
        {
          // Partition coefficient inverse in µg/m3.
          real Kinv = vpsat[i] / (THERMODYNAMIC_IDEAL_GAS_CONSTANT * temp * mwa_inv);

          // Local equilibrium gas correction.
          // If component does not exist, aero[i] = 0,
          // then equilibrium gas concentration is also null.
          // Perhaps need to take care about near 0 concentrations.
          gas[i] = aero[i] * Kinv / tsp;
        }

    return true;
  }


  // Global module.
  bool ClassModelThermodynamicPankow::module_global_(const real &temp,
                                                     const vector1r &mw,
                                                     const vector1r &kpart,
                                                     const vector1r &vpsat,
                                                     vector1r &aero,
                                                     vector1r &gas) const
  {
    // Mass conservation.
    vector1r mass_cons(Nspecies_semivolatile_);
    for (int i = 0; i < Nspecies_semivolatile_; i++)
      mass_cons[i] = aero[i] + gas[i];

    // Compute primary mass and its contribution to average inverse molar weight.
    real prim_mass(real(0));
    real mwa_inv_prim_mass(real(0));
    for (int i = Nspecies_semivolatile_; i < Nspecies_; i++)
      {
        prim_mass += aero[i];
        mwa_inv_prim_mass += aero[i] / mw[i];
      }

    // Compute total suspended mass.
    real tsp(real(0));
    for (int i = 0; i < Nspecies_; i++)
      tsp += aero[i];

    if (tsp == real(0))
      return false;

    //
    // Solving loop.
    //

    vector1r xold;
    vector1r x(Nspecies_semivolatile_);
    for (int i = 0; i <  Nspecies_semivolatile_; i++)
      x[i] = aero[i];

    for (int h = 0; h < Nloop_; h++)
      {
#ifdef AMC_WITH_DEBUG_PANKOW
        cout << "h = " << h << endl;
        cout << "tsp = " << tsp << endl;
#endif

        // Average mol weight.
        real mwa_inv(mwa_inv_prim_mass);
        for (int i = 0; i < Nspecies_semivolatile_; i++)
          mwa_inv += x[i] / mw[i];
        mwa_inv /= tsp;

#ifdef AMC_WITH_DEBUG_PANKOW
        cout << "mwa_inv = " << mwa_inv << endl;
#endif

        // Partition coefficient inverse in µg/m3.
        vector1r coef(Nspecies_semivolatile_, tsp);
        for (int i = 0; i <  Nspecies_semivolatile_; i++)
          if (use_partition_coefficient_[i])
            coef[i] *= kpart[i];
          else
            coef[i] *= (THERMODYNAMIC_IDEAL_GAS_CONSTANT * temp * mwa_inv) / vpsat[i];

#ifdef AMC_WITH_DEBUG_PANKOW
        cout << "coef = " << coef << endl;
#endif

        // Keep old concentrations and update aerosol concentrations.
        xold = x;

        // New value.
        for (int i = 0; i < Nspecies_semivolatile_; i++)
          x[i] = mass_cons[i] * coef[i] / (real(1) + coef[i]);

#ifdef AMC_WITH_DEBUG_PANKOW
        cout << "x = " << x << endl;
#endif

        // Does loop converges ?
        double sum(double(0)), abs_diff(double(0));
        for (int i = 0; i < Nspecies_semivolatile_; i++)
          {
            sum += xold[i];
            abs_diff += abs(xold[i] - x[i]);
          }

#ifdef AMC_WITH_DEBUG_PANKOW
        cout << "abs_diff = " << abs_diff << ", epsilon * sum = " << epsilon_ * sum << endl;
#endif

        if (abs_diff < epsilon_ * sum)
          break;

        // Compute total suspended mass.
        tsp = prim_mass;
        for (int i = 0; i < Nspecies_semivolatile_; i++)
          tsp += x[i];

        // If no more absorbing mass, exit loop.
        // It should be very rare.
        if (tsp == real(0))
          return false;
      }

    // Give back solution. Gas from mass conservation.
    for (int i = 0; i < Nspecies_semivolatile_; i++)
      {
        aero[i] = x[i];
        gas[i] = mass_cons[i] - aero[i];
      }

    return true;
  }


  // Constructors.
  ClassModelThermodynamicPankow::ClassModelThermodynamicPankow(Ops::Ops &ops, const bool standalone)
    : ClassModelThermodynamicBase(ops, standalone, "pankow"), Nspecies_primary_(0),
      Nloop_(PANKOW_NUMBER_LOOP), epsilon_(double(PANKOW_EPSILON)),
      Nspecies_semivolatile_(0), Nspecies_(0)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Instantiating Pankow Thermodyamic model." << endl;
#endif

    string prefix_orig = ops.GetPrefix();

    // First read Pankow specific data, independent of AMC.
    ops.ClearPrefix();
    ops.SetPrefix(this->prefix_);

    ops.Set("species_name", "", species_name_);
    Nspecies_ = int(species_name_.size());

    if (Nspecies_ == 0)
      throw AMC::Error("No species read in Pankow thermodynamic model.");

    ops.Set("molar_mass.g_mol", "", molar_mass_);
    ops.Set("saturation_vapor_pressure.Pa", "", saturation_vapor_pressure_);
    ops.Set("partition_coefficient.m3_ug", "", partition_coefficient_);
    ops.Set("vaporization_enthalpy.J_mol", "", vaporization_enthalpy_);
    ops.Set("use_partition_coefficient", "", use_partition_coefficient_);

    if ((molar_mass_.size()) != Nspecies_)
      throw AMC::Error("Size of molar_mass vector is != Nspecies (=" +
                       to_str(Nspecies_) + ") in Pankow thermodynamic model.");

    Nloop_ = ops.Get<int>("Nloop", "", Nloop_);
    epsilon_ = ops.Get<double>("epsilon", "", epsilon_);

    // Convert from g.mol^{-1} to µg.mol^{-1}.
    for (int i = 0; i < Nspecies_; i++)
      molar_mass_[i] *= real(1.e6);

    Nspecies_semivolatile_ = ops.Get<int>("Nsemivolatile");
    if (Nspecies_semivolatile_ == 0)
      throw AMC::Error("No semivolatile species in Pankow thermodynamic model.");

    if (int(saturation_vapor_pressure_.size()) != Nspecies_semivolatile_)
      throw AMC::Error("Size of saturation_vapor_pressure vector is != Nspecies (=" +
                       to_str(Nspecies_semivolatile_) + ") in Pankow thermodynamic model.");

    if (int(partition_coefficient_.size()) != Nspecies_semivolatile_)
      throw AMC::Error("Size of partition_coefficient vector is != Nspecies (=" +
                       to_str(Nspecies_semivolatile_) + ") in Pankow thermodynamic model.");

    if (int(vaporization_enthalpy_.size()) != Nspecies_semivolatile_)
      throw AMC::Error("Size of vaporization_enthalpy vector is != Nspecies (=" +
                       to_str(Nspecies_semivolatile_) + ") in Pankow thermodynamic model.");

    if (int(use_partition_coefficient_.size()) != Nspecies_semivolatile_)
      throw AMC::Error("Size of use_partition_coefficient vector is != Nspecies (=" +
                       to_str(Nspecies_semivolatile_) + ") in Pankow thermodynamic model.");

    Nspecies_primary_ = Nspecies_ - Nspecies_semivolatile_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "species_name = " << species_name_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Nspecies = " << Nspecies_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Nspecies_semivolatile = " << Nspecies_semivolatile_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Nspecies_primary = " << Nspecies_primary_ << endl;
#endif

    // Revert to local config.
    ops.SetPrefix(prefix_orig);

    if (! this->standalone_)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Make correspondance with AMC configuration." << endl;
#endif

        species_index_aerosol_.resize(Nspecies_);
        for (int i = 0; i < Nspecies_; i++)
          {
            species_index_aerosol_[i] = ClassSpecies::GetIndex(species_name_[i]);
            if (species_index_aerosol_[i] < 0)
              throw AMC::Error("Species \"" + species_name_[i] + "\" of Pankow thermodynamic model \"" +
                               prefix_orig + "\" not found in AMC species list.");
          }

        species_index_gas_.resize(Nspecies_semivolatile_);
        for (int i = 0; i < Nspecies_semivolatile_; i++)
          {
            species_index_gas_[i] = ClassSpecies::GetIndexGas(species_name_[i]);
            if (species_index_gas_[i] < 0)
              throw AMC::Error("Gas species \"" + species_name_[i] + "\" of Pankow thermodynamic model \"" +
                               prefix_orig + "\" not found in AMC gas species list.");
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Debug(3) << Reset() << "Species index with AMC : " << endl;
        *AMCLogger::GetLog() << Bred() << Debug(3) << Reset() << "\tAerosol : " << species_index_aerosol_ << endl;
        *AMCLogger::GetLog() << Bred() << Debug(3) << Reset() << "\tGas : " << species_index_gas_ << endl;
#endif

        for (int i = 0; i < Nspecies_; i++)
          molar_mass_[i] = ClassSpecies::molar_mass_[species_index_aerosol_[i]];

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          saturation_vapor_pressure_[i] = ClassSpecies::saturation_vapor_pressure_[species_index_aerosol_[i]];

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          partition_coefficient_[i] = ClassSpecies::partition_coefficient_[species_index_aerosol_[i]];

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          vaporization_enthalpy_[i] = ClassSpecies::vaporization_enthalpy_[species_index_aerosol_[i]];

        ops.Set("use_partition_coefficient", "", use_partition_coefficient_, use_partition_coefficient_);

        Nloop_ = ops.Get<int>("Nloop", "", Nloop_);
        epsilon_ = ops.Get<double>("epsilon", "", epsilon_);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "use_partition_coefficient = " << use_partition_coefficient_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "Nloop = " << Nloop_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "epsilon = " << epsilon_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "molar_mass = " << molar_mass_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "saturation_vapor_pressure = " << saturation_vapor_pressure_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "partition_coefficient = " << partition_coefficient_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "vaporization_enthalpy = " << vaporization_enthalpy_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassModelThermodynamicPankow::~ClassModelThermodynamicPankow()
  {
    return;
  }


  // Get methods.
  int ClassModelThermodynamicPankow::GetNspecies() const
  {
    return Nspecies_;
  }

  int ClassModelThermodynamicPankow::GetNspeciesSemivolatile() const
  {
    return Nspecies_semivolatile_;
  }

  bool ClassModelThermodynamicPankow::IsPartitionCoefficientUsed(const int &i) const
  {
    return use_partition_coefficient_[i];
  }

  int ClassModelThermodynamicPankow::GetNloop() const
  {
    return Nloop_;
  }

  real ClassModelThermodynamicPankow::GetEpsilon() const
  {
    return epsilon_;
  }


  // Set methods.
  void ClassModelThermodynamicPankow::TogglePartitionCoefficientUse(const int &i)
  {
    use_partition_coefficient_[i] = use_partition_coefficient_[i] == false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Toggle use of coefficient partition"
                         << " for species \"" << species_name_[i] << "\" : use = "
                         << (use_partition_coefficient_[i] ? "yes" : "no") << endl;
#endif
  }

  void ClassModelThermodynamicPankow::SetNloop(const int &Nloop)
  {
    Nloop_ = Nloop;
  }

  void ClassModelThermodynamicPankow::SetEpsilon(const real &epsilon)
  {
    epsilon_ = epsilon;
  }


  // Clone.
  ClassModelThermodynamicPankow* ClassModelThermodynamicPankow::clone() const
  {
    return new ClassModelThermodynamicPankow(*this);
  }


  // Compute local equilibrium.
  void ClassModelThermodynamicPankow::ComputeLocalEquilibrium(const int &section_min,
                                                              const int &section_max,
                                                              const vector1i &section_index,
                                                              const real *concentration_aer_number,
                                                              const real *concentration_aer_mass) const
  {
    // Whether to use the partition coefficient
    // or to recompute it from saturation vapor pressure.
    vector1r kpart(Nspecies_semivolatile_), vpsat(Nspecies_semivolatile_);

    for (int h = 0; h < Nspecies_semivolatile_; h++)
      if (use_partition_coefficient_[h])
        kpart[h] = ClassMeteorologicalData::partition_coefficient_[species_index_gas_[h]];
      else
        vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[species_index_gas_[h]];

    const vector1r &mw = ClassModelThermodynamicPankow::molar_mass_;

    int Nsection = int(section_index.size());

    for (int h = 0; h < Nsection; h++)
      {
        const int i = section_index[h];
        if (i < section_min || i >= section_max)
          continue;

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Aerosol is assumed to be thermically at equilibrium with air.
            //ClassAerosolData::temperature_[i] = ClassMeteorologicalData::temperature_;

            // Start index for gas concentration vector.
            const int j = i * ClassSpecies::Ngas_;

            // Start index for mass concentration vector.
            const int k = i * ClassSpecies::Nspecies_;

            vector1r aero(ClassModelThermodynamicPankow::Nspecies_),
              gas(ClassModelThermodynamicPankow::Nspecies_semivolatile_);

#ifdef AMC_WITH_LAYER
            for (int l = 0; l < ClassModelThermodynamicPankow::Nspecies_; l++)
              {
                const int m = k + species_index_aerosol_[l];
                aero[l] = ClassAerosolData::layer_repartition_[m].back() * concentration_aer_mass[m];
              }
#else
            for (int l = 0; l < ClassModelThermodynamicPankow::Nspecies_; l++)
              aero[l] = concentration_aer_mass[k + species_index_aerosol_[l]];
#endif

            if (module_local_(ClassMeteorologicalData::temperature_, mw, kpart, vpsat, aero, gas))
              for (int l = 0; l < Nspecies_semivolatile_; l++)
                ClassAerosolData::equilibrium_gas_surface_[j + species_index_gas_[l]] = gas[l];
          }
      }
  }


  // Compute global equilibrium.
  void ClassModelThermodynamicPankow::ComputeGlobalEquilibrium(const int &section_min,
                                                               const int &section_max,
                                                               const real *concentration_gas,
                                                               const real *concentration_aer_mass,
                                                               vector1r &concentration_aer_total,
                                                               vector1r &equilibrium_aer_internal,
                                                               real &liquid_water_content,
                                                               vector1r &concentration_aer_equilibrium,
                                                               vector1r &concentration_gas_equilibrium) const
  {
    // Whether to use the partition coefficient
    // or to recompute it from saturation vapor pressure.
    vector1r kpart(Nspecies_semivolatile_), vpsat(Nspecies_semivolatile_);

    for (int h = 0; h < Nspecies_semivolatile_; h++)
    if (use_partition_coefficient_[h])
      kpart[h] = ClassMeteorologicalData::partition_coefficient_[species_index_aerosol_[h]];
    else
      vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[species_index_aerosol_[h]];

    const vector1r &mw = ClassModelThermodynamicPankow::molar_mass_;

    // Compute bulk aerosol concentration for each Pankow species.
    for (int i = section_min; i < section_max; i++)
      {
        int j = i * ClassSpecies::Nspecies_;
        for (int k = 0; k < Nspecies_; k++)
          {
            int l = species_index_aerosol_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }
      }

    vector1r aero(ClassModelThermodynamicPankow::Nspecies_);
    for (int i = 0; i < ClassModelThermodynamicPankow::Nspecies_; i++)
      aero[i] = concentration_aer_total[species_index_aerosol_[i]];

    vector1r gas(Nspecies_semivolatile_);
    for (int i = 0; i < Nspecies_semivolatile_; i++)
      gas[i] = concentration_gas[species_index_gas_[i]];

    if (module_global_(ClassMeteorologicalData::temperature_, mw, kpart, vpsat, aero, gas))
      {
        // Give back solution to concentration vector.
        for (int i = 0; i < Nspecies_; i++)
          concentration_aer_equilibrium[species_index_aerosol_[i]] = aero[i];

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          concentration_gas_equilibrium[species_index_gas_[i]] = gas[i];
      }
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassModelThermodynamicPankow::Test(const int &mode,
                                           const real &temperature,
                                           const vector<real> &concentration,
                                           map<string, real> &output) const
  {
    // Update temperature dependent physical parameters.
    vector1r vpsat = saturation_vapor_pressure_;
    vector1r kpart = partition_coefficient_;

    for (int i = 0; i < Nspecies_semivolatile_; i++)
      {
        // Van't Hoff coefficient for saturation concentration or
        // partition coefficient change due to change in temperature.
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_[i]);

        // The partition coefficient is like the inverse of the saturation vapor concentration.
        if (use_partition_coefficient_[i])
          kpart[i] *= (temperature / AMC_TEMPERATURE_REFERENCE) / vant_hoff_coefficient;
        else
          vpsat[i] *= vant_hoff_coefficient;
      }

    const vector1r &mw = molar_mass_;

    // Everything in particle phase.
    vector1r aero = concentration;
    vector1r gas(Nspecies_semivolatile_, real(0));

    // Forward or reverse (global or local) mode.
    bool tsp_not_null;
    if (mode == 0)
      tsp_not_null = module_global_(temperature, mw, kpart, vpsat, aero, gas);
    else if (mode == 1)
      tsp_not_null = module_local_(temperature, mw, kpart, vpsat, aero, gas);
    else
      throw AMC::Error("Unkown mode " + to_str(mode) + ", valid values are 0=global and 1=local.");

    // Total suspended mass.
    output["TSP"] = real(0);
    for (int i = 0; i < Nspecies_; i++)
      output["TSP"] += aero[i];

    if (tsp_not_null)
      {
        // Outputs in µg.m^{-3}.
        for (int i = 0; i < Nspecies_; i++)
          output[species_name_[i]] = aero[i];

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          output[species_name_[i] + "g"] = gas[i];

        // Also give physical values.
        output["temperature"] = temperature;

        for (int i = 0; i < Nspecies_semivolatile_; i++)
          if (use_partition_coefficient_[i])
            output["KPART_" + species_name_[i]] = kpart[i];
          else
            output["VPSAT_" + species_name_[i]] = vpsat[i];
      }
  }


  void ClassModelThermodynamicPankow::TestAMC(const real &temperature,
                                              const real &relative_humidity,
                                              const vector<real> &concentration_aer_mass,
                                              vector<real> &equilibrium_aer_internal,
                                              vector<real> &equilibrium_gas_surface) const
  {
    if (this->standalone_)
      throw AMC::Error("Thermodyanmic model \"" + this->name_ + "\" runs in standalone mode.");

    // Update temperature dependent physical parameters.
    vector1r vpsat = saturation_vapor_pressure_;
    vector1r kpart = partition_coefficient_;

    for (int i = 0; i < Nspecies_semivolatile_; i++)
      {
        // Van't Hoff coefficient for saturation concentration or
        // partition coefficient change due to change in temperature.
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_[i]);

        // The partition coefficient is like the inverse of the saturation vapor concentration.
        if (use_partition_coefficient_[i])
          kpart[i] *= (temperature / AMC_TEMPERATURE_REFERENCE) / vant_hoff_coefficient;
        else
          vpsat[i] *= vant_hoff_coefficient;
      }

    const vector1r &mw = molar_mass_;

    vector1r aero(Nspecies_), gas(Nspecies_semivolatile_);

    for (int i = 0; i < Nspecies_; i++)
      aero[i] = concentration_aer_mass[species_index_aerosol_[i]];

    if (module_local_(temperature, mw, kpart, vpsat, aero, gas))
      for (int i = 0; i < Nspecies_semivolatile_; i++)
        equilibrium_gas_surface[species_index_gas_[i]] = gas[i];
  }
#endif
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_PANKOW_CXX
#endif
