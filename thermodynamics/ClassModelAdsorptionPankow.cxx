// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_ADSORPTION_PANKOW_CXX

#include "ClassModelAdsorptionPankow.hxx"

namespace AMC
{
  // Constructors.
  ClassModelAdsorptionPankow::ClassModelAdsorptionPankow(Ops::Ops &ops)
    : ClassModelAdsorptionBase(ops, "pankow")
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Instantiating Pankow adsorption model." << endl;
#endif

    // Set specific surface area for each particles.
    specific_surface_area_.assign(ClassAerosolData::GetNg(),
                                  MODEL_ADSORPTION_PANKOW_SPECIFIC_SURFACE_AREA_CORN_1971_AVG);

    const string prefix_orig = ops.GetPrefix();
    ops.SetPrefix(prefix_orig + "specific_surface_area.");

    vector1s section_name = ops.GetEntryList();

    // First look for a default value for all general sections.
    for (int i = 0; i < int(section_name.size()); i++)
      if (section_name[i] == "default")
        {
          real specific_surface_area_default = ops.Get<real>("default");

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fred() << Debug(2) << Reset()
                               << "specific_surface_area_default = " << specific_surface_area_default << endl;
#endif
          specific_surface_area_.assign(ClassAerosolData::GetNg(), specific_surface_area_default);
          break;
        }

    // Reverse general section index.
    vector<vector<int> > general_section_reverse;
    ClassAerosolData::GetGeneralSectionReverse(general_section_reverse);

    // Then look for particular settings.
    for (int i = 0; i < int(section_name.size()); i++)
      if (section_name[i] != "default")
        {
          int size_section = ops.Get<int>(section_name[i] + ".size");
          if (size_section < 0)
            size_section += ClassDiscretizationSize::GetNsection();

          vector1i class_composition;
          if (ops.Exists(section_name[i] + ".class"))
            ops.Set(section_name[i] + ".class", "", class_composition);
          else
            for (int j = 0; j < general_section_reverse[size_section].size(); j++)
              class_composition.push_back(j);

          real value = ops.Get<real>(section_name[i] + ".value");

          for (int j = 0; j < int(class_composition.size()); j++)
            specific_surface_area_[general_section_reverse[size_section][class_composition[j]]] = value;

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "For size section " << size_section
                               << " and its class compositions " << class_composition
                               << " assign specific surface area to " << value << "cm2/µg" << endl;
#endif
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "specific_surface_area = " << specific_surface_area_ << endl;
#endif

    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassModelAdsorptionPankow::~ClassModelAdsorptionPankow()
  {
    return;
  }


  // Clone.
  ClassModelAdsorptionPankow* ClassModelAdsorptionPankow::clone() const
  {
    return new ClassModelAdsorptionPankow(*this);
  }


  // Get methods.
  void ClassModelAdsorptionPankow::GetSpecificSurfaceArea(vector<real> &specific_surface_area) const
  {
    specific_surface_area = specific_surface_area_;
  }


  // Compute local equilibrium.
  void ClassModelAdsorptionPankow::ComputeLocalEquilibrium(const int &section_min,
                                                           const int &section_max,
                                                           const vector1i &section_index,
                                                           const real *concentration_aer_number,
                                                           const real *concentration_aer_mass) const
  {
    int Nsection = int(section_index.size());
    for (int h = 0; h < Nsection; h++)
      {
        const int i = section_index[h];
        if (i < section_min || i >= section_max)
          continue;

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Start index for mass concentration vector.
            const int j = i * ClassSpecies::Nspecies_;

            // Start index for gas concentration vector.
            const int k = i * ClassSpecies::Ngas_;

            // Total mass concentration. Beginning with liquid water content.
            real mass_total = ClassAerosolData::liquid_water_content_[i];
            for (int l = 0; l < ClassSpecies::Nspecies_; l++)
              mass_total += concentration_aer_mass[j + l];

            if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
              for (int l = 0; l < ClassSpecies::Ngas_; l++)
                {
                  int m = k + l;

                  const int n = ClassSpecies::semivolatile_[l];

                  // If species is organic and Thermodynamic models
                  // could not assign it one equilibrium gas surface concentrations,
                  // this means, aBsorption could not take place.
                  // We then let the possibility for organic species to aDsorb.
                  if (ClassSpecies::is_organic_[n])
                    if (ClassAerosolData::equilibrium_gas_surface_[m] < real(0))
                      {
                        // Compute aDsorption partitioning coefficient.
                        // sorption_site in mol/cm2
                        // specific_surface_area in cm2/µg
                        // AMC_IDEAL_GAS_CONSTANT in J/(K.mol)
                        // temperature in K
                        // saturation_vapor_pressure in Pascal (=J/m3)
                        //
                        // adsorpion_coefficient in
                        // (mol/cm2) * (cm2/µg) * J/(K.mol) * K * m3 / J = m3 / µg
                        // which is the usual partitioning coefficient unit.
                        ClassAerosolData::adsorption_coefficient_[m] =
                          ClassSpecies::sorption_site_[n] * specific_surface_area_[i]
                          * AMC_IDEAL_GAS_CONSTANT * ClassMeteorologicalData::temperature_
                          / ClassMeteorologicalData::saturation_vapor_pressure_[l];

#ifdef AMC_WITH_LAYER
                        ClassAerosolData::equilibrium_gas_surface_[m] = concentration_aer_mass[j + n]
                          * ClassAerosolData::layer_repartition_[j + n].back()
                          / (ClassAerosolData::adsorption_coefficient_[m] * mass_total);
#else
                        ClassAerosolData::equilibrium_gas_surface_[m] = concentration_aer_mass[j + n]
                          / (ClassAerosolData::adsorption_coefficient_[m] * mass_total);
#endif
                      }
                }
          }
      }
  }
}

#define AMC_FILE_CLASS_MODEL_ADSORPTION_PANKOW_CXX
#endif
