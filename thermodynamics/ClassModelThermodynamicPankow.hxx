// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_PANKOW_HXX

#define PANKOW_NUMBER_LOOP 10
#define PANKOW_EPSILON 1.e-4

namespace AMC
{
  class ClassModelThermodynamicPankow : public ClassModelThermodynamicBase
  {
  private:

    /*!< Number of loops.*/
    int Nloop_;

    /*!< Number of semivolatile species.*/
    int Nspecies_semivolatile_;

    /*!< Number of primary species.*/
    int Nspecies_primary_;

    /*!< Total number of species.*/
    int Nspecies_;

    /*!< Criteria for stopping loop.*/
    double epsilon_;

    /*!< Whether to use partition coefficient or saturation vapor pressure.*/
    vector1b use_partition_coefficient_;

    /*!< Index of aerosol species in AMC.*/
    vector1i species_index_aerosol_;

    /*!< Index of gas species in AMC.*/
    vector1i species_index_gas_;

    /*!< Species names.*/
    vector1s species_name_;

    /*!< Molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_;

    /*!< Saturation vapor pressure in Pascal.*/
    vector1r saturation_vapor_pressure_;

    /*!< Vaporization enthalpy in J.mol^{-1}.*/
    vector1r vaporization_enthalpy_;

    /*!< Partition coefficient m^{3}/µg.*/
    vector1r partition_coefficient_;

    /*!< Local module.*/
    bool module_local_(const real &temp,
                       const vector1r &mw,
                       const vector1r &kpart,
                       const vector1r &vpsat,
                       const vector1r &aero,
                       vector1r &gas) const;

    /*!< Global module.*/
    bool module_global_(const real &temp,
                        const vector1r &mw,
                        const vector1r &kpart,
                        const vector1r &vpsat,
                        vector1r &aero,
                        vector1r &gas) const;

  public:

    /*!< Constructors.*/
    ClassModelThermodynamicPankow(Ops::Ops &ops, const bool standalone = true);


    /*!< Destructor.*/
    ~ClassModelThermodynamicPankow();

    /*!< Get methods.*/
    int GetNspecies() const;
    int GetNspeciesSemivolatile() const;
    int GetNloop() const;
    real GetEpsilon() const;
    bool IsPartitionCoefficientUsed(const int &i) const;

    /*!< Set methods.*/
    void TogglePartitionCoefficientUse(const int &i);
    void SetNloop(const int &Nloop);
    void SetEpsilon(const real &epsilon);

    /*!< Clone.*/
    ClassModelThermodynamicPankow* clone() const;


    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;

    /*!< Compute global equilibrium.*/
    void ComputeGlobalEquilibrium(const int &section_min,
                                  const int &section_max,
                                  const real *concentration_gas,
                                  const real *concentration_aer_mass,
                                  vector1r &concentration_aer_total,
                                  vector1r &equilibrium_aer_internal,
                                  real &liquid_water_content,
                                  vector1r &concentration_aer_equilibrium,
                                  vector1r &concentration_gas_equilibrium) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    void Test(const int &mode,
              const real &temperature,
              const vector<real> &concentration,
              map<string, real> &output) const;

    void TestAMC(const real &temperature,
                 const real &relative_humidity,
                 const vector<real> &concentration_aer_mass,
                 vector<real> &equilibrium_aer_internal,
                 vector<real> &equilibrium_gas_surface) const;
#endif
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_PANKOW_HXX
#endif
