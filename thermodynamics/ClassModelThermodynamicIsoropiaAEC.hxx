// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_AEC_HXX


namespace AMC
{
  class ClassModelThermodynamicIsoropiaAEC : public ClassModelThermodynamicIsoropia, public ClassModelThermodynamicAEC
  {
  protected:

    /*!< Number of species.*/
    int Nspecies_;

    /*!< Number of semivolatile species.*/
    int Nsemivolatile_;

    /*!< Number of AMC internal species involved in thermodynamic model : this shadows that of parent class.*/
    int Nspecies_internal_;

  public:

    /*!< Constructors.*/
    ClassModelThermodynamicIsoropiaAEC(Ops::Ops &ops, const bool standalone = true, const string name = "aec");

    /*!< Destructor.*/
    ~ClassModelThermodynamicIsoropiaAEC();

    /*!< Get methods.*/
    int GetNspeciesInternal() const;

    /*!< Clone.*/
    ClassModelThermodynamicIsoropiaAEC* clone() const;


    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;


    /*!< Compute global equilibrium.*/
    void ComputeGlobalEquilibrium(const int &section_min,
                                  const int &section_max,
                                  const real *concentration_gas,
                                  const real *concentration_aer_mass,
                                  vector1r &concentration_aer_total,
                                  vector1r &equilibrium_aer_internal,
                                  real &liquid_water_content,
                                  vector1r &concentration_aer_equilibrium,
                                  vector1r &concentration_gas_equilibrium) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    void Test(const int &mode,
              const real &temperature,
              const real &relative_humidity,
              const vector<real> &concentration,
              map<string, real> &output,
              const bool concentration_in_mol = false) const;


    void TestAMC(const real &temperature,
                 const real &relative_humidity,
                 const vector<real> &concentration_aer_mass,
                 vector<real> &equilibrium_aer_internal,
                 vector<real> &equilibrium_gas_surface) const;
#endif
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_AEC_HXX
#endif
