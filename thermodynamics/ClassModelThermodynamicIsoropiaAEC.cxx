// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_AEC_CXX

#include "ClassModelThermodynamicIsoropiaAEC.hxx"

namespace AMC
{
  // Constructors.
  ClassModelThermodynamicIsoropiaAEC::ClassModelThermodynamicIsoropiaAEC(Ops::Ops &ops, const bool standalone, const string name)
    : ClassModelThermodynamicBase(ops, standalone, name),
      ClassModelThermodynamicIsoropia(ops, standalone), ClassModelThermodynamicAEC(ops, standalone, name),
      Nspecies_internal_(0), Nspecies_(0), Nsemivolatile_(0)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Instantiating IsoropiaAEC Thermodynamic model." << endl;
#endif

    ClassModelThermodynamicBase::name_ = "IsoropiaAEC";
    ClassModelThermodynamicBase::prefix_ = "";

    Nspecies_ = ISOROPIA_NUMBER_SPECIES_EXTERNAL + ClassModelThermodynamicAEC::Nsemivolatile_;
    Nsemivolatile_ = ISOROPIA_NUMBER_SPECIES_GAS + ClassModelThermodynamicAEC::Nsemivolatile_;
    Nspecies_internal_ = ClassModelThermodynamicIsoropia::Nspecies_internal_
      + ClassModelThermodynamicAEC::Nspecies_internal_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Debug(1) << Reset() << "Nspecies = " << Nspecies_ << endl;
    *AMCLogger::GetLog() << Byellow() << Debug(1) << Reset() << "Nsemivolatile = " << Nsemivolatile_ << endl;
    *AMCLogger::GetLog() << Byellow() << Debug(1) << Reset() << "Nspecies_internal = " << Nspecies_internal_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassModelThermodynamicIsoropiaAEC::~ClassModelThermodynamicIsoropiaAEC()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Debug(3) << Reset() << "Called destructor for IsoropiaAEC." << endl;
#endif

    return;
  }


  // Get methods.
  int ClassModelThermodynamicIsoropiaAEC::GetNspeciesInternal() const
  {
    return Nspecies_internal_;
  }


  // Clone.
  ClassModelThermodynamicIsoropiaAEC* ClassModelThermodynamicIsoropiaAEC::clone() const
  {
    return new ClassModelThermodynamicIsoropiaAEC(*this);
  }


  // Compute local equilibrium.
  void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                   const int &section_max,
                                                                   const vector1i &section_index,
                                                                   const real *concentration_aer_number,
                                                                   const real *concentration_aer_mass) const
  {
    // Meteorological variables.
    double temperature_isrpia = double(ClassMeteorologicalData::temperature_);
    double relative_humidity_isrpia = double(ClassMeteorologicalData::relative_humidity_);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    // Internal species molar concentration mol.m^{-3}.
    vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

    // AEC local arrays.
    vector1r aero_a(ClassModelThermodynamicAEC::Nhydrophilic_, real(0)),
      gas_a(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_, real(0));
    vector1r aero_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0)),
      gas_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0));

    // In this routine, we are in reverse mode.
    cntrl[0] = double(1);

    // Metastable option.
    cntrl[1] = double(ClassModelThermodynamicIsoropia::metastable_);

    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(ClassModelThermodynamicAEC::Nsemivolatile_);
    vector1r he(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_);

    for (int h = 0; h < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; h++)
      vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[ClassModelThermodynamicAEC::index_hydrophobic_gas_[h]];

    for (int h = 0; h < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; h++)
      {
        const int &i = ClassModelThermodynamicAEC::index_hydrophilic_gas_[h];
        vpsat[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + h] = ClassMeteorologicalData::saturation_vapor_pressure_[i];
        he[h] = ClassMeteorologicalData::henry_constant_[i];
      }

    int Nsection = int(section_index.size());

    for (int h = 0; h < Nsection; h++)
      {
        const int i = section_index[h];
        if (i < section_min || i >= section_max)
          continue;

        // We should perform a loop between Isoropia and AEC, namely through
        // H+ and LWC, but in view of 3D applications we just avoid it.
        // As a consequence, Isoropia may predict no water, but organics may bring
        // just enough water to enable inorganic dissolution. We deliberately omit
        // this case, which would anyway require some deep modifications in Isoropia code.
        // This should be rare and only appear if Isoropia is set not metastable, even more rare ...
        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Aerosol is assumed to be thermically at equilibrium with air.
            //ClassAerosolData::temperature_[i] = ClassMeteorologicalData::temperature_;

            // Start index of gas surface concentration.
            const int j = i * ClassSpecies::Ngas_;

            // Start index for mass concentration vector.
            const int k = i * ClassSpecies::Nspecies_;

            // Start index for mass concentration vector.
            const int l = i * ClassSpeciesEquilibrium::Nspecies_;

            // Input in mol.m^{-3}.
            double wi_total(double(0));
            for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_EXTERNAL; m++)
              {
#ifdef AMC_WITH_LAYER
                const int n = k + ClassModelThermodynamicIsoropia::species_index_external_[m];
                wi[m] = double(ClassAerosolData::layer_repartition_[n].back() *
                               concentration_aer_mass[n] / ClassModelThermodynamicIsoropia::molar_mass_external_[m]);
#else
                wi[m] = double(concentration_aer_mass[k + ClassModelThermodynamicIsoropia::species_index_external_[m]]
                               / ClassModelThermodynamicIsoropia::molar_mass_external_[m]);
#endif
                wi_total += wi[m];
              }

            // If no inorganic matter, set equilibrium gas concentration
            // to the current bulk gas concentration to prevent condensation,
            // as in theory, absorption cannot take place if no absorbing inorganic material.
            // This should be very rare...
            real hp(real(0)), lwc(real(0));

            if (wi_total > double(0))
              {
                isoropia_(wi.data(), &relative_humidity_isrpia, &temperature_isrpia, cntrl.data(), w.data(),
                          gas.data(), aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

                // Put Isoropia output into one vector for easier use.
                for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_LIQUID; m++)
                  concentration_molar_internal[m] = real(aerliq[m]);
                for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_SOLID; m++)
                  concentration_molar_internal[ISOROPIA_NUMBER_SPECIES_LIQUID + m] = real(aersld[m]);

                // Outputs required concentrations, turned into µg.m^{-3}.
                lwc = concentration_molar_internal[ISOROPIA_INDEX_LWC]
                  * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_LWC];

                // Grab H+ concentration, directly from Isoropia.
                hp = concentration_molar_internal[ISOROPIA_INDEX_HP]
                  * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_HP];

                // Avoid H+ ions which are later processed by AEC/H2O.
                // That is why Hp must be placed at beginning of vector.
                for (int m = 1; m < ClassModelThermodynamicIsoropia::Nspecies_internal_; m++)
                  {
                    // Isoropia index.
                    const int n = ClassModelThermodynamicIsoropia::species_index_internal_[m][1];

                    // Thermodynamic model species, e.g. hydronium ions (H+).
                    ClassAerosolData::equilibrium_aer_internal_
                      [l + ClassModelThermodynamicIsoropia::species_index_internal_[m][0]] = concentration_molar_internal[n]
                      * ClassModelThermodynamicIsoropia::molar_mass_internal_[n];
                  }

                // If no liquid water content, gas equilibrium concentrations
                // from Isoropia are believed to be meaningless.
                if (lwc > real(0))
                  {
                    for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_GAS; m++)
                      ClassAerosolData::equilibrium_gas_surface_[j + ClassModelThermodynamicIsoropia::species_index_gas_[m]]
                        = real(gas[m]) * ClassModelThermodynamicIsoropia::molar_mass_gas_[m];

                    // Say that all inorganic species are in aqueous phase.
                    for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_EXTERNAL; m++)
                      ClassAerosolData::phase_[k + ClassModelThermodynamicIsoropia::species_index_external_[m]]
                        = ClassPhase::aqueous_phase_index_;
                  }
                else
                  {
                    // If no liquid water content, we just have to say that all species are in a solid phase.
                    for (int m = 0; m < ISOROPIA_NUMBER_SPECIES_EXTERNAL; m++)
                      ClassAerosolData::phase_[k + ClassModelThermodynamicIsoropia::species_index_external_[m]] = -1;
                  }
              }

            //
            // Hydrophilic module.
            //

#ifdef AMC_WITH_LAYER
            for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
              {
                const int n = k + ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[m];
                aero_a[m] = ClassAerosolData::layer_repartition_[n].back() * concentration_aer_mass[n];
              }
#else
            for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
              aero_a[m] = concentration_aer_mass[k + ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[m]];
#endif
            bool aqueous_phase = ClassModelThermodynamicAEC::
              module_hydrophilic_local_(ClassMeteorologicalData::temperature_,
                                        ClassMeteorologicalData::relative_humidity_,
                                        he, lwc, hp, aero_a, gas_a);

            // If not aqueous phase, give concentrations to hydrophobic module.
            if (aqueous_phase)
              {
                for (int m = 0; m < Nhydrophilic_semivolatile_; m++)
                  ClassAerosolData::equilibrium_gas_surface_[j + ClassModelThermodynamicAEC::index_hydrophilic_gas_[m]] = gas_a[m];

                // Give back liquid wate content.
                ClassAerosolData::liquid_water_content_[i] = lwc;

                // The return value contain the possible modification due to organics, usually it is negligible.
                if (ClassModelThermodynamicAEC::index_hydronium_ >= 0)
                  ClassAerosolData::equilibrium_aer_internal_[l + index_hydronium_] = hp;

                // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
                for (int m = 0; m < ClassModelThermodynamicAEC::Nspecies_internal_; m++)
                  ClassAerosolData::equilibrium_aer_internal_[l + ClassModelThermodynamicAEC::species_index_internal_[m][0]]
                    = aero_a[ClassModelThermodynamicAEC::species_index_internal_[m][1]];

                // If in aqueous phase, say that all hydrophilic species are in it.
                for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
                  ClassAerosolData::phase_[k + ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[m]]
                    = ClassPhase::aqueous_phase_index_;
              }
            else
              {
                for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
                  aero_b[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + m] = aero_a[m];

                // If not in aqueous phase, say that all hydrophilic species absorb
                // in the anhydrous phase, which is the second (and last) one in ClassSpecies::phase_ field.
                for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
                  {
                    const int n = index_hydrophilic_aerosol_[m];
                    ClassAerosolData::phase_[k + n] = ClassSpecies::phase_[n].back();
                  }
              }

            //
            // Hydrophobic module.
            //

            // Compute primary mass and its average molar weight
            // from AMC species viewed by AMC as primary mass (Nhydrophobic_primary_amc_),
            // this is different than primary organic AEC species
            // (Nhydrophobic_primary_amc_) on which this primary mass will be mapped.
            real primary_mass(real(0)), primary_mass_mw(real(0));
            for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophobic_primary_amc_; m++)
              {

                const int n = ClassModelThermodynamicAEC::index_hydrophobic_primary_amc_[m];
                real mass_species = concentration_aer_mass[k + n];
#ifdef AMC_WITH_LAYER
                mass_species *= ClassAerosolData::layer_repartition_[k + n].back();
#endif
                primary_mass += mass_species;
                primary_mass_mw += ClassSpecies::molar_mass_[n] * mass_species;
              }

            if (primary_mass > real(0))
              primary_mass_mw /= primary_mass;
            else
              primary_mass_mw = molar_mass_primary_fixed_;

#ifdef AMC_WITH_LAYER
            for (int m = 0; m < Nhydrophobic_semivolatile_; m++)
              {
                const int n = k + ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[m];
                aero_b[m] = ClassAerosolData::layer_repartition_[n].back() * concentration_aer_mass[n];
              }
#else
            for (int m = 0; m < Nhydrophobic_semivolatile_; m++)
              aero_b[m] = concentration_aer_mass[k + ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[m]];
#endif

            if (ClassModelThermodynamicAEC::
                module_hydrophobic_local_(ClassMeteorologicalData::temperature_,
                                          primary_mass, primary_mass_mw,
                                          vpsat, aero_b, gas_b))
              {
                for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; m++)
                  ClassAerosolData::equilibrium_gas_surface_[j + ClassModelThermodynamicAEC::index_hydrophobic_gas_[m]] = gas_b[m];

                if (! aqueous_phase)
                  for (int m = 0; m < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; m++)
                    ClassAerosolData::equilibrium_gas_surface_[j + ClassModelThermodynamicAEC::index_hydrophilic_gas_[m]]
                      = gas_b[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + m];
              }
          }
      }
  }


  // Compute global equilibrium.
  void ClassModelThermodynamicIsoropiaAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const real *concentration_gas,
                                                                    const real *concentration_aer_mass,
                                                                    vector1r &concentration_aer_total,
                                                                    vector1r &equilibrium_aer_internal,
                                                                    real &liquid_water_content,
                                                                    vector1r &concentration_aer_equilibrium,
                                                                    vector1r &concentration_gas_equilibrium) const
  {
    // Meteorological variables.
    double temperature_isrpia = double(ClassMeteorologicalData::temperature_);
    double relative_humidity_isrpia = double(ClassMeteorologicalData::relative_humidity_);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    // In this routine, we are in forward mode.
    cntrl[0] = double(0);

    // Metastable option.
    cntrl[1] = double(ClassModelThermodynamicIsoropia::metastable_);

    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(ClassModelThermodynamicAEC::Nsemivolatile_);
    vector1r he(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_);

    for (int h = 0; h < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; h++)
      vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[ClassModelThermodynamicAEC::index_hydrophobic_gas_[h]];

    for (int h = 0; h < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; h++)
      {
        const int &i = ClassModelThermodynamicAEC::index_hydrophilic_gas_[h];
        vpsat[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + h]
          = ClassMeteorologicalData::saturation_vapor_pressure_[i];
        he[h] = ClassMeteorologicalData::henry_constant_[i];
      }

    // Compute aerosol concentration for each Isoropia species.
    for (int i = section_min; i < section_max; i++)
      {
        int j = i * ClassSpecies::Nspecies_;
        for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_EXTERNAL; k++)
          {
            const int l = ClassModelThermodynamicIsoropia::species_index_external_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }

        for (int k = 0; k < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; k++)
          {
            const int l = ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }

        for (int k = 0; k < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; k++)
          {
            const int l = ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }

        for (int k = 0; k < ClassModelThermodynamicAEC::Nhydrophobic_primary_amc_; k++)
          {
            const int l = ClassModelThermodynamicAEC::index_hydrophobic_primary_amc_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }
      }

    // Feed Isoropia inputs.
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      wi[i] = double(concentration_aer_total[ClassModelThermodynamicIsoropia::species_index_external_[i]]);

    int j(2);
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
      wi[j++] += double(concentration_gas[ClassModelThermodynamicIsoropia::species_index_gas_[i]]);

    // Input in mol.m^{-3}.
    double wi_total(double(0));
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      {
        wi[i] /= double(ClassModelThermodynamicIsoropia::molar_mass_external_[i]);
        wi_total += wi[i];
      }

    // Put Isoropia output into one vector for easier use.
    vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

    if (wi_total > double(0))
      {
        isoropia_(wi.data(), &relative_humidity_isrpia, &temperature_isrpia, cntrl.data(), w.data(),
                  gas.data(), aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

        // Remove gas from total concentration for semivolatile species.
        j = 2;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          wi[j++] -= gas[i];

        j = 0;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
          concentration_molar_internal[j++] = real(aerliq[i]);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
          concentration_molar_internal[j++] = real(aersld[i]);

        // Give back bulk equilibrium concentrations into µg.m^{-3}.
        for (int i = 2; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
          concentration_aer_equilibrium[species_index_external_[i]]
            = real(wi[i]) * ClassModelThermodynamicIsoropia::molar_mass_external_[i];

        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          concentration_gas_equilibrium[species_index_gas_[i]]
            = real(gas[i]) * ClassModelThermodynamicIsoropia::molar_mass_gas_[i];

        // Outputs required concentrations, turned into µg.m^{-3}.
        liquid_water_content = concentration_molar_internal[ISOROPIA_INDEX_LWC]
          * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_LWC];

        for (int i = 0; i < ClassModelThermodynamicIsoropia::Nspecies_internal_; i++)
          {
            // AMC and Isoropia index.
            const int &j = ClassModelThermodynamicIsoropia::species_index_internal_[i][0];
            const int &k = ClassModelThermodynamicIsoropia::species_index_internal_[i][1];

            equilibrium_aer_internal[j] = concentration_molar_internal[k]
              * ClassModelThermodynamicIsoropia::molar_mass_internal_[k];
          }
      }

    //
    // Hydrophilic module.
    //

    vector1r aero_a(ClassModelThermodynamicAEC::Nhydrophilic_, real(0)),
      gas_a(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_, real(0));
    vector1r aero_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0)),
      gas_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0));

    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
      {
        aero_a[i] = concentration_aer_total[ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[i]];
        gas_a[i] = concentration_gas[ClassModelThermodynamicAEC::index_hydrophilic_gas_[i]];
      }

    // Grab H+ concentration, directly from Isoropia.
    real hp = concentration_molar_internal[ISOROPIA_INDEX_HP]
      * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_HP];

    bool aqueous_phase = ClassModelThermodynamicAEC::module_hydrophilic_global_(ClassMeteorologicalData::temperature_,
                                                                                ClassMeteorologicalData::relative_humidity_,
                                                                                he, liquid_water_content, hp, aero_a, gas_a);

    // If not aqueous phase, give concentrations to hydrophobic module.
    if (aqueous_phase)
      {
        // Give back to concentration vector.
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            concentration_aer_equilibrium[ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[i]] = aero_a[i];
            concentration_gas_equilibrium[ClassModelThermodynamicAEC::index_hydrophilic_gas_[i]] = gas_a[i];
          }

        if (ClassModelThermodynamicAEC::index_hydronium_ >= 0)
          equilibrium_aer_internal[ClassModelThermodynamicAEC::index_hydronium_] = hp;

        // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
        for (int i = 0; i < ClassModelThermodynamicAEC::Nspecies_internal_; i++)
          equilibrium_aer_internal[ClassModelThermodynamicAEC::species_index_internal_[i][0]]
            = aero_a[ClassModelThermodynamicAEC::species_index_internal_[i][1]];
      }
    else
      for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
        {
          int j =  ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + i;
          aero_b[j] = aero_a[i];
          gas_b[j] = gas_a[i];
        }

    //
    // Hydrophobic module.
    //

    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
      {
        aero_b[i] = concentration_aer_total[ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[i]];
        gas_b[i] = concentration_gas[ClassModelThermodynamicAEC::index_hydrophobic_gas_[i]];
      }

    // Compute primary mass and its average molar weight.
    real primary_mass(real(0)), primary_mass_mw(real(0));
    for (int i = 0; i < Nhydrophobic_primary_amc_; i++)
      {
        const int &j = ClassModelThermodynamicAEC::index_hydrophobic_primary_amc_[i];
        primary_mass += concentration_aer_total[j];
        primary_mass_mw += ClassSpecies::molar_mass_[j] * concentration_aer_total[j];
      }

    if (primary_mass > real(0))
      primary_mass_mw /= primary_mass;
    else
      primary_mass_mw = molar_mass_primary_fixed_;

    if (ClassModelThermodynamicAEC::module_hydrophobic_global_(ClassMeteorologicalData::temperature_,
                                                               primary_mass, primary_mass_mw,

                                                               vpsat, aero_b, gas_b))
      {
        // Give back to concentration vector.
        for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
          {
            concentration_aer_equilibrium[ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[i]] = aero_b[i];
            concentration_gas_equilibrium[ClassModelThermodynamicAEC::index_hydrophobic_gas_[i]] = gas_b[i];
          }

        if (! aqueous_phase)
          for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
            {
              int j = ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + i;
              concentration_aer_equilibrium[ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[i]] = aero_b[j];
              concentration_gas_equilibrium[ClassModelThermodynamicAEC::index_hydrophilic_gas_[i]] = gas_b[j];
            }
      }
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassModelThermodynamicIsoropiaAEC::Test(const int &mode,
                                                const real &temperature,
                                                const real &relative_humidity,
                                                const vector<real> &concentration,
                                                map<string, real> &output,
                                                const bool concentration_in_mol) const
  {
  }


  void ClassModelThermodynamicIsoropiaAEC::TestAMC(const real &temperature,
                                                   const real &relative_humidity,
                                                   const vector<real> &concentration_aer_mass,
                                                   vector<real> &equilibrium_aer_internal,
                                                   vector<real> &equilibrium_gas_surface) const
  {
    if (this->standalone_)
      throw AMC::Error("Thermodyanmic model \"" + this->name_ + "\" runs in standalone mode.");

    real &liquid_water_content = equilibrium_aer_internal.back();

    // Meteorological variables.
    double temp = double(temperature);
    double rh = double(relative_humidity);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    /*!< Internal species molar concentration mol.m^{-3}.*/
    vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

    // In this routine, we are in reverse mode.
    cntrl[0] = double(1);

    // Metastable option.
    cntrl[1] = double(ClassModelThermodynamicIsoropia::metastable_);

    // Input in mol.m^{-3}.
    double wi_total(double(0));
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      {
        wi[i] = double(concentration_aer_mass[ClassModelThermodynamicIsoropia::species_index_external_[i]]
                       / ClassModelThermodynamicIsoropia::molar_mass_external_[i]);
        wi_total += wi[i];
      }

    // If nothing in inorganic phase, set equilibrium gas concentration
    // to the current bulk gas concentration to prevent condensation,
    // as in theory, absorption cannot take place if no absorbing inorganic material.
    // This should be very rare...
    if (wi_total > double(0))
      {
        isoropia_(wi.data(), &rh, &temp, cntrl.data(), w.data(), gas.data(),
                  aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

        // Put Isoropia output into one vector for easier use.
        int h = 0;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
          concentration_molar_internal[h++] = real(aerliq[i]);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
          concentration_molar_internal[h++] = real(aersld[i]);
 
        // Outputs required concentrations, turned into µg.m^{-3}.
        liquid_water_content = concentration_molar_internal[ISOROPIA_INDEX_LWC]
          * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_LWC];

        for (int i = 0; i < ClassModelThermodynamicIsoropia::Nspecies_internal_; i++)
          {
            // AMC index.
            const int &j = ClassModelThermodynamicIsoropia::species_index_internal_[i][0];

            // Isoropia index.
            const int &k = ClassModelThermodynamicIsoropia::species_index_internal_[i][1];

            // thermodynamic models, e.g. hydronium ions (H+).
            equilibrium_aer_internal[j] = concentration_molar_internal[k]
              * ClassModelThermodynamicIsoropia::molar_mass_internal_[k];
          }

        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          equilibrium_gas_surface[ClassModelThermodynamicIsoropia::species_index_gas_[i]]
            = real(gas[i]) * ClassModelThermodynamicIsoropia::molar_mass_gas_[i];
      }
    else
      {
        for (int i = 0; i < ClassModelThermodynamicIsoropia::Nspecies_internal_; i++)
          equilibrium_aer_internal[ClassModelThermodynamicIsoropia::species_index_internal_[i][0]] = real(0);

        // If no inorganic matter, set equilibrium gas concentrations to negative.
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          equilibrium_gas_surface[ClassModelThermodynamicIsoropia::species_index_gas_[i]] = real(-1);
      }

    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(ClassModelThermodynamicAEC::Nsemivolatile_);
    vector1r he(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_);

    // Update temperature dependent physical parameters.
    // Van't Hoff coefficient for saturation concentration or
    // partition coefficient change due to change in temperature.
    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1),
                                              ClassModelThermodynamicAEC::vaporization_enthalpy_hydrophilic_[i]);

        vpsat[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + i]
          = ClassModelThermodynamicAEC::saturation_vapor_pressure_hydrophilic_[i] * vant_hoff_coefficient;
        he[i] = ClassModelThermodynamicAEC::henry_constant_[i] * vant_hoff_coefficient;
      }

    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1),
                                              ClassModelThermodynamicAEC::vaporization_enthalpy_hydrophobic_[i]);

        vpsat[i] = ClassModelThermodynamicAEC::saturation_vapor_pressure_hydrophobic_[i] * vant_hoff_coefficient;
      }

    // Internal gas/particle concentration vectors for each phase.
    vector1r aero_a(ClassModelThermodynamicAEC::Nhydrophilic_, real(0)),
      aero_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0));
    vector1r gas_a(ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_, real(0)),
      gas_b(ClassModelThermodynamicAEC::Nsemivolatile_, real(0));

    //
    // Hydrophilic module.
    //

    // Grab H+ concentration, directly from Isoropia.
    real hp = concentration_molar_internal[ISOROPIA_INDEX_HP]
      * ClassModelThermodynamicIsoropia::molar_mass_internal_[ISOROPIA_INDEX_HP];

    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
      aero_a[i] = concentration_aer_mass[ClassModelThermodynamicAEC::index_hydrophilic_aerosol_[i]];

    bool aqueous_phase = module_hydrophilic_local_(temperature,
                                                   relative_humidity,
                                                   he, liquid_water_content,
                                                   hp, aero_a, gas_a);

    if (aqueous_phase)
      {
        for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
          equilibrium_gas_surface[ClassModelThermodynamicAEC::index_hydrophilic_gas_[i]] = gas_a[i];

        // The return value contain the possible modification due to organics, usually it is negligible.
        if (ClassModelThermodynamicAEC::index_hydronium_ >= 0)
          equilibrium_aer_internal[ClassModelThermodynamicAEC::index_hydronium_] = hp;

        // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
        for (int i = 0; i < ClassModelThermodynamicAEC::Nspecies_internal_; i++)
          equilibrium_aer_internal[ClassModelThermodynamicAEC::species_index_internal_[i][0]]
            = aero_a[ClassModelThermodynamicAEC::species_index_internal_[i][1]];
      }
    else
      for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
        aero_b[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + i] = aero_a[i];

    //
    // Hydrophobic module.
    //

    // Compute primary mass and its average molar weight
    // from AMC species viewed by AMC as primary mass (Nhydrophobic_primary_amc_),
    // this is different than primary organic AEC species
    // (Nhydrophobic_primary_amc_) on which this primary mass will be mapped.
    real primary_mass(real(0)), primary_mass_mw(real(0));
    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_primary_amc_; i++)
      {
        const int &j = ClassModelThermodynamicAEC::index_hydrophobic_primary_amc_[i];
        primary_mass += concentration_aer_mass[j];
        primary_mass_mw += ClassSpecies::molar_mass_[j] * concentration_aer_mass[j];
      }

    if (primary_mass > real(0))
      primary_mass_mw /= primary_mass;
    else
      primary_mass_mw = molar_mass_primary_fixed_;

    for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
      aero_b[i] = concentration_aer_mass[ClassModelThermodynamicAEC::index_hydrophobic_aerosol_[i]];

    if (module_hydrophobic_local_(temperature,
                                  primary_mass, primary_mass_mw,
                                  vpsat, aero_b, gas_b))
      {
        for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_; i++)
          equilibrium_gas_surface[ClassModelThermodynamicAEC::index_hydrophobic_gas_[i]] = gas_b[i];

        if (! aqueous_phase)
          for (int i = 0; i < ClassModelThermodynamicAEC::Nhydrophilic_semivolatile_; i++)
            equilibrium_gas_surface[ClassModelThermodynamicAEC::index_hydrophilic_gas_[i]]
              = gas_b[ClassModelThermodynamicAEC::Nhydrophobic_semivolatile_ + i];
      }
  }
#endif
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_AEC_CXX
#endif
