// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_ADSORPTION_BASE_CXX

#include "ClassModelAdsorptionBase.hxx"

namespace AMC
{
  // Constructors.
  ClassModelAdsorptionBase::ClassModelAdsorptionBase(Ops::Ops &ops, const string name)
    : name_(name)
  {
    return;
  }


  // Destructor.
  ClassModelAdsorptionBase::~ClassModelAdsorptionBase()
  {
    return;
  }


  // Get methods.
  string ClassModelAdsorptionBase::GetName() const
  {
    return name_;
  }
}

#define AMC_FILE_CLASS_MODEL_ADSORPTION_BASE_CXX
#endif
