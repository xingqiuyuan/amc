// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_ADSORPTION_BASE_HXX

namespace AMC
{
  class ClassModelAdsorptionBase
  {
  public:

    typedef AMC_THERMODYNAMIC_REAL real;
    typedef vector<int> vector1i;
    typedef vector<vector1i> vector2i;
    typedef vector<real> vector1r;
    typedef vector<string> vector1s;

  private:

    /*!< Parameterization name.*/
    string name_;

  public:

    /*!< Constructors.*/
    ClassModelAdsorptionBase(Ops::Ops &ops, const string name);

    /*!< Destructor.*/
    virtual ~ClassModelAdsorptionBase();

    /*!< Get methods.*/
    string GetName() const;

    /*!< Clone.*/
    virtual ClassModelAdsorptionBase* clone() const = 0;

    /*!< Compute local equilibrium.*/
    virtual void ComputeLocalEquilibrium(const int &section_min,
                                         const int &section_max,
                                         const vector1i &section_index,
                                         const real *concentration_aer_number,
                                         const real *concentration_aer_mass) const = 0;
  };
}

#define AMC_FILE_CLASS_MODEL_ADSORPTION_BASE_HXX
#endif
