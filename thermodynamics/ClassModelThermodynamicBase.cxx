// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_BASE_CXX

#include "ClassModelThermodynamicBase.hxx"

namespace AMC
{
  // Constructors.
  ClassModelThermodynamicBase::ClassModelThermodynamicBase(const bool standalone, const string name)
    : standalone_(standalone), name_(name), prefix_(name + ".")
  {
    return;
  }


  ClassModelThermodynamicBase::ClassModelThermodynamicBase(Ops::Ops &ops, const bool standalone, const string name)
    : standalone_(standalone), name_(name), prefix_(name)
  {
    // Prefix where to find configuration, the model name by default.
    prefix_ = ops.Get<string>("prefix", "", prefix_);

    // Check that prefix exists.
    string prefix_orig = ops.GetPrefix();

    ops.ClearPrefix();
    if (! ops.Exists(prefix_))
      throw AMC::Error("Cannot find section \"" + prefix_ + "\" for configuration of model \"" + name_ + "\".");

    prefix_ += ".";

    // Standalone mode.
    if (! standalone_)
      if (ClassSpecies::GetNspecies() == 0)
        throw AMC::Error("Thermodynamic model \"" + name_ + "\" is run in standalone mode, " +
                         "but AMC configuration seems not loaded (no species).");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "Read \"" << name_ << "\" thermodynamic model data"
                         << " from configuration section \"" << prefix_ << "\"." << endl;
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Thermodynamic model \"" << name_ << "\" runs in "
                         << "standalone mode ? " << (standalone_ ? "YES" : "NO") << endl;
#endif

    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassModelThermodynamicBase::~ClassModelThermodynamicBase()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Debug(3) << Reset() << "Called destructor for Base Thermodynamic." << endl;
#endif

    return;
  }


  // Get methods.
  string ClassModelThermodynamicBase::GetName() const
  {
    return name_;
  }

  string ClassModelThermodynamicBase::GetPrefix() const
  {
    return prefix_;
  }


  // Is model in standalone mode ?.
  bool ClassModelThermodynamicBase::IsStandAlone() const
  {
    return standalone_;
  }
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_BASE_CXX
#endif
