// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_VOID_HXX

namespace AMC
{
  class ClassModelThermodynamicVoid : public ClassModelThermodynamicBase
  {
  public:

    /*!< Constructors.*/
    ClassModelThermodynamicVoid(const bool standalone = true);

    /*!< Destructor.*/
    ~ClassModelThermodynamicVoid();

    /*!< Clone.*/
    ClassModelThermodynamicVoid* clone() const;


    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;

    /*!< Compute global equilibrium.*/
    void ComputeGlobalEquilibrium(const int &section_min,
                                  const int &section_max,
                                  const real *concentration_gas,
                                  const real *concentration_aer_mass,
                                  vector1r &concentration_aer_total,
                                  vector1r &equilibrium_aer_internal,
                                  real &liquid_water_content,
                                  vector1r &concentration_aer_equilibrium,
                                  vector1r &concentration_gas_equilibrium) const;
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_VOID_HXX
#endif
