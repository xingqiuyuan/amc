// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_AEC_CXX

#include "ClassModelThermodynamicAEC.hxx"
#include "AEC.cxx"

namespace AMC
{
  // Constructors.
  ClassModelThermodynamicAEC::ClassModelThermodynamicAEC(Ops::Ops &ops, const bool standalone, const string name)
    : ClassModelThermodynamicBase(ops, standalone, name), Nsemivolatile_(0),
      Nloop_a_(AEC_NUMBER_LOOP_A), Nloop_b_(AEC_NUMBER_LOOP_B),
      epsilon_a_(AEC_EPSILON_A), epsilon_b_(AEC_EPSILON_B),
      Nhydrophilic_semivolatile_(0), Nhydrophilic_semivolatile_water_(0),
      Nhydrophilic_(0), Nhydrophobic_semivolatile_(0), Nhydrophobic_total_(0),
      Nhydrophobic_primary_(0), Nhydrophobic_(0), Nspecies_internal_(0),
      molar_mass_primary_fixed_(real(AEC_MOLAR_MASS_PRIMARY_FIXED_DEFAULT)),
      nfunc_a_(0), nfunc_b_(0), Noligomerization_(0), Nloop_h_(AEC_NUMBER_LOOP_H),
      with_oligomerization_(AEC_WITH_OLIGOMERIZATION_DEFAULT), epsilon_h_(AEC_EPSILON_H),
      Nhydrophobic_primary_amc_(0), Nreaction_dissolution_(0), index_hydronium_(-1),
      oligomerization_pH_reference_(real(AEC_OLIGOMERIZATION_PH_REFERENCE_DEFAULT)),
      oligomerization_hp_mol_reference_(real(0)), Z_(real(AEC_UNIFAC_Z_PARAMETER)),
      with_unifac_(AEC_WITH_UNIFAC_DEFAULT), oligomerization_power_(AEC_OLIGOMERIZATION_POWER)
  {
    // If base class has not set the name and prefix, do it here.
    if (this->name_.empty())
      this->name_ = name;

    if (this->prefix_.empty())
      this->prefix_ = ops.Get<string>("prefix", "", this->name_) + ".";

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Instantiating " << this->name_
                         << " thermodyamic model with prefix \"" << this->prefix_ << "\"." << endl;
#endif

    string prefix_orig = ops.GetPrefix();

    //
    // First read AEC specific data, independent of AMC.
    //

    ops.ClearPrefix();

    // Read hydrophilic/phobic species names.
    ops.SetPrefix(this->prefix_ + "species.hydrophilic.");

    ops.Set("name", "", name_hydrophilic_);
    Nhydrophilic_ = int(name_hydrophilic_.size());

    Nhydrophilic_semivolatile_ = ops.Get<int>("Nsemivolatile");
    Nhydrophilic_semivolatile_water_ = Nhydrophilic_semivolatile_ + 1;

    ops.SetPrefix(this->prefix_ + "species.hydrophobic.");

    ops.Set("name", "", name_hydrophobic_);
    Nhydrophobic_ = int(name_hydrophobic_.size());

    Nhydrophobic_semivolatile_ = ops.Get<int>("Nsemivolatile");
    Nhydrophobic_primary_ = Nhydrophobic_ - Nhydrophobic_semivolatile_;

    if (Nhydrophobic_primary_ < 0)
      throw AMC::Error("Negative number of primary hydrophobic species.");

    Nsemivolatile_ = Nhydrophilic_semivolatile_ + Nhydrophobic_semivolatile_;
    Nhydrophobic_total_ = Nhydrophobic_ + Nhydrophilic_semivolatile_;

#ifdef AMC_WITH_LOGGER
    if (Nhydrophobic_primary_ == 0)
      *AMCLogger::GetLog() << Bred() << Warning() << Reset() << "There are no primary hydrophobic species." << endl;
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Nhydrophilic = " <<  Nhydrophilic_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Nhydrophobic = " <<  Nhydrophobic_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "name_hydrophilic = " <<  name_hydrophilic_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "name_hydrophobic = " <<  name_hydrophobic_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Nhydrophilic_semivolatile = " <<  Nhydrophilic_semivolatile_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Nhydrophobic_semivolatile = " <<  Nhydrophobic_semivolatile_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Nhydrophobic_primary = " <<  Nhydrophobic_primary_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Nhydrophobic_total = " <<  Nhydrophobic_total_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug() << Reset() << "Nsemivolatile = " <<  Nsemivolatile_ << endl;
#endif

    // Read molar mass and convert from g.mol^{-1} to µg.mol^{-1}.
    ops.SetPrefix(this->prefix_ + "molar_mass_g_mol.");
    ops.Set("hydrophilic", "", molar_mass_hydrophilic_);
    ops.Set("hydrophobic", "", molar_mass_hydrophobic_);
    molar_mass_primary_fixed_ = ops.Get<real>("primary", "", molar_mass_primary_fixed_) * real(1.e6);

    if (int(molar_mass_hydrophilic_.size()) != Nhydrophilic_)
      throw AMC::Error("Wrong number of molar mass for hydrophilic species.");

    if (int(molar_mass_hydrophobic_.size()) != Nhydrophobic_semivolatile_)
      throw AMC::Error("Wrong number of molar mass for hydrophobic semivolatile species.");

    for (int i = 0; i < Nhydrophilic_; i++)
      molar_mass_hydrophilic_[i] *= real(1.e6);

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      molar_mass_hydrophobic_[i] *= real(1.e6);

    // Read the molar partitioning or AMC primary mass into AEC primary species.
    ops.SetPrefix(this->prefix_);
    ops.Set("primary_fraction_molar", "", primary_fraction_molar_);

    // Read physical data.
    ops.SetPrefix(this->prefix_ + "physic.hydrophilic.");

    // Read henry, drh, ... constants.
    ops.Set("vaporization_enthalpy.J_mol", "", vaporization_enthalpy_hydrophilic_);
    ops.Set("saturation_vapor_pressure.Pa", "", saturation_vapor_pressure_hydrophilic_);
    if (ops.Exists("henry_constant.m3_ug"))
      {
        ops.Set("henry_constant.m3_ug", "", henry_constant_);

        // Convert to mol.L^{-1}.atm^{-1}.
        for (int i = 0; i < Nhydrophilic_semivolatile_; ++i)
          henry_constant_[i] *= real(AMC_PRESSURE_ATM) * real(1e9) / (AMC_IDEAL_GAS_CONSTANT * AMC_TEMPERATURE_REFERENCE);
      }
    else
        ops.Set("henry_constant.mol_L_atm", "", henry_constant_);

    ops.Set("drh", "", drh_);

    bool compute_gamma_infinite_dilution(false);
    if (ops.Exists("gamma_infinite_dilution"))
      ops.Set("gamma_infinite_dilution", "", gamma_infinite_);
    else
      compute_gamma_infinite_dilution = true;

    ops.SetPrefix(this->prefix_ + "physic.hydrophilic.dissolution.");

    Nreaction_dissolution_.assign(Nhydrophilic_semivolatile_, 0);
    reaction_dissolution_constant_.resize(Nhydrophilic_semivolatile_);
    reaction_dissolution_species_index_.resize(Nhydrophilic_semivolatile_);

    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      if (ops.Exists(name_hydrophilic_[i]))
        {
          ops.Set(name_hydrophilic_[i] + ".species", "", reaction_dissolution_species_index_[i]);

          if (ops.Exists(name_hydrophilic_[i] + ".pKa"))
            {
              vector1r pKa;
              ops.Set(name_hydrophilic_[i] + ".pKa", "", pKa);
              // I am not sure of this relationship, depends on units used for the constant in the AEC original code.
              for (int j = 0; j < int(pKa.size()); j++)
                reaction_dissolution_constant_[i][j] = pow(real(10), -pKa[j]);
            }
          else if (ops.Exists(name_hydrophilic_[i] + ".Ka"))
            ops.Set(name_hydrophilic_[i] + ".Ka", "", reaction_dissolution_constant_[i]);

          Nreaction_dissolution_[i] = int(reaction_dissolution_constant_[i].size());

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "For species \"" << name_hydrophilic_[i] << "\" :" << endl;
          *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "\tNreaction_dissolution = "
                               << Nreaction_dissolution_[i] << endl;
          *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "\treaction_dissolution_constant = "
                               << reaction_dissolution_constant_[i] << endl;
          *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "\treaction dissolution species = "
                               << name_hydrophilic_[reaction_dissolution_species_index_[i][0]];
          for (int j = 1; j < int(reaction_dissolution_species_index_[i].size()); j++)
            *AMCLogger::GetLog() << " <-> " << name_hydrophilic_[reaction_dissolution_species_index_[i][j]];
          *AMCLogger::GetLog() << endl;
#endif
        }

    ops.SetPrefix(this->prefix_ + "physic.hydrophobic.");
    ops.Set("vaporization_enthalpy.J_mol", "", vaporization_enthalpy_hydrophobic_);
    ops.Set("saturation_vapor_pressure.Pa", "", saturation_vapor_pressure_hydrophobic_);

    // Oligomerization.
    ops.SetPrefix(this->prefix_ + "physic.oligomerization.");
    with_oligomerization_ = ops.Get<bool>("use", "", with_oligomerization_);

    // Numeric data.
    ops.SetPrefix(this->prefix_ + "numeric.");
    Nloop_a_ = ops.Get<int>("hydrophilic.Nloop", "", Nloop_a_);
    epsilon_a_ = ops.Get<real>("hydrophilic.epsilon", "", epsilon_a_);

    Nloop_b_ = ops.Get<int>("hydrophobic.Nloop", "", Nloop_b_);
    epsilon_b_ = ops.Get<real>("hydrophobic.epsilon", "", epsilon_b_);

    Nloop_h_ = ops.Get<int>("hydronium.Nloop", "", Nloop_h_);
    epsilon_h_ = ops.Get<double>("hydronium.epsilon", "", epsilon_h_);

    // In AEC configuration, choose whether to use unifac or not.
    ops.SetPrefix(this->prefix_ + "unifac.");
    with_unifac_ = ops.Get<bool>("use", "", with_unifac_);

    //
    // Read AEC specific settings against AMC.
    //

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Done reading specific "
                         << this->name_ << ", now read configuration against AMC"
                         << " in section \"" << prefix_orig << "\" (if any)." << endl;
#endif

    ops.SetPrefix(prefix_orig);

    if (! this->standalone_)
      {

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Make correspondance with AMC species list." << endl;
#endif

        // By default, assume no H+ in aerosols. This is to prevent configuration omissions.
        index_hydronium_ = ops.Get<int>("index_hydronium", "", index_hydronium_);

        // Search for the index(es) of primary hydrophobic species. Default is none.
        vector1s hydrophobic_primary_amc;
        ops.Set("primary_hydrophobic_amc", "", vector1s(), hydrophobic_primary_amc);

        Nhydrophobic_primary_amc_ = int(hydrophobic_primary_amc.size());
        index_hydrophobic_primary_amc_.assign(Nhydrophobic_primary_amc_, -1);
        for (int i = 0; i < Nhydrophobic_primary_amc_; i++)
          {
            index_hydrophobic_primary_amc_[i] = ClassSpecies::GetIndex(hydrophobic_primary_amc[i]);
            if (index_hydrophobic_primary_amc_[i] < 0)
              throw AMC::Error("Unable to find species \"" + hydrophobic_primary_amc[i] + "\" in model" +
                               " species list for thermodynamic model \"" + prefix_orig + "\".");
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "H+ index = " << index_hydronium_
                             << ", a negative value means is does not exists or it is not used." << endl;
        *AMCLogger::GetLog() << Bgreen() << Info(1) << Reset() << "List of AMC species viewed as primary"
                             << " hydrophobic organic species = " << index_hydrophobic_primary_amc_ << endl;
        *AMCLogger::GetLog() << Bgreen() << Info(1) << Reset() << "Nhydrophobic_primary_amc = "
                             << Nhydrophobic_primary_amc_ << endl;
#endif

        // Make correspondance between thermodynamic and model species for hydrophilic.
        index_hydrophilic_aerosol_.assign(Nhydrophilic_semivolatile_, -1);
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            index_hydrophilic_aerosol_[i] = ClassSpecies::GetIndex(name_hydrophilic_[i]);
            if (index_hydrophilic_aerosol_[i] < 0)
              throw AMC::Error("Unable to find species \"" + name_hydrophilic_[i] + "\" in model" +
                               " species list for thermodynamic model \"" + prefix_orig + "\".");
          }

        index_hydrophilic_gas_.assign(Nhydrophilic_semivolatile_, -1);
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            index_hydrophilic_gas_[i] = ClassSpecies::GetIndexGas(name_hydrophilic_[i]);
            if (index_hydrophilic_gas_[i] < 0)
              throw AMC::Error("Unable to find species \"" + name_hydrophilic_[i] + "\" in model" +
                               " gas species list for thermodynamic model \"" + prefix_orig + "\".");
          }

        // Make correspondance between thermodynamic and model species for hydrophobic.
        index_hydrophobic_aerosol_.assign(Nhydrophobic_semivolatile_, -1);
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          {
            index_hydrophobic_aerosol_[i] = ClassSpecies::GetIndex(name_hydrophobic_[i]);
            if (index_hydrophobic_aerosol_[i] < 0)
              throw AMC::Error("Unable to find species \"" + name_hydrophobic_[i] + "\" in model" +
                               " species list for thermodynamic model \"" + prefix_orig + "\".");
          }

        index_hydrophobic_gas_.assign(Nhydrophobic_semivolatile_, -1);
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          {
            index_hydrophobic_gas_[i] = ClassSpecies::GetIndexGas(name_hydrophobic_[i]);
            if (index_hydrophobic_gas_[i] < 0)
              throw AMC::Error("Unable to find species \"" + name_hydrophobic_[i] + "\" in model" +
                               " gas species list for thermodynamic model \"" + prefix_orig + "\".");
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "AMC index of model species :" << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "\thydrophilic :" << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "\t\taerosol = " << index_hydrophilic_aerosol_ << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "\t\t    gas = " << index_hydrophilic_gas_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "\thydrophobic :" << endl;
        *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "\t\taerosol = " << index_hydrophobic_aerosol_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "\t\t    gas = " << index_hydrophobic_gas_ << endl;
#endif

        // Physics. Overwrite physical data with that of AMC.
        for (int i = 0; i< Nhydrophilic_semivolatile_; ++i)
          {
            molar_mass_hydrophilic_[i] = ClassSpecies::molar_mass_[index_hydrophilic_aerosol_[i]];
            saturation_vapor_pressure_hydrophilic_[i] = ClassSpecies::saturation_vapor_pressure_[index_hydrophilic_aerosol_[i]];
            vaporization_enthalpy_hydrophilic_[i] = ClassSpecies::vaporization_enthalpy_[index_hydrophilic_aerosol_[i]];
            henry_constant_[i] = ClassSpecies::henry_constant_[index_hydrophilic_aerosol_[i]];
            drh_[i] = ClassSpecies::drh_[index_hydrophilic_aerosol_[i]];
          }

        for (int i = 0; i < Nhydrophobic_semivolatile_; ++i)
          {
            molar_mass_hydrophobic_[i] = ClassSpecies::molar_mass_[index_hydrophobic_aerosol_[i]];
            saturation_vapor_pressure_hydrophobic_[i] = ClassSpecies::saturation_vapor_pressure_[index_hydrophobic_aerosol_[i]];
            vaporization_enthalpy_hydrophobic_[i] = ClassSpecies::vaporization_enthalpy_[index_hydrophobic_aerosol_[i]];
          }

        // Internal species, if any.
        if (ops.Exists("species_internal." + name))
          {
            vector1s species_list;
            ops.Set("species_internal." + name, "", species_list);
            Nspecies_internal_ = int(species_list.size());

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Bgreen() << Debug(3) << Reset() << "Nspecies_internal = " << Nspecies_internal_ << endl;
#endif

            species_index_internal_.assign(Nspecies_internal_, vector1i(2, -1));

            for (int i = 0; i < Nspecies_internal_; i++)
              {
                for (int j = 0; j < ClassSpeciesEquilibrium::Nspecies_; j++)
                  if (species_list[i] == ClassSpeciesEquilibrium::name_[j])
                    {
                      species_index_internal_[i][0] = j;
                      break;
                    }

                if (species_index_internal_[i][0] < 0)
                  throw AMC::Error("Unable to find species \"" + species_list[i] + "\" in AMC equilibrium species list.");

                for (int j = 0; j < Nhydrophilic_; j++)
                  if (species_list[i] == name_hydrophilic_[j])
                    {
                      species_index_internal_[i][1] = j;
                      break;
                    }

                if (species_index_internal_[i][1] < 0)
                  throw AMC::Error("Unable to find species \"" + species_list[i] + "\" in AEC species list.");

#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Species \""
                                     << ClassSpeciesEquilibrium::name_[species_index_internal_[i][0]]
                                     << "\" found at index " << species_index_internal_[i][0] << " in AMC"
                                     << " and index " << species_index_internal_[i][1] << " in AEC." << endl;
#endif
              }
          }

        // In AMC configuration, choose whether to use oligomerization or not.
        with_oligomerization_ = ops.Get<bool>("with_oligomerization", "", with_oligomerization_);

        // In AMC configuration, choose whether to use unifac or not.
        with_unifac_ = ops.Get<bool>("with_unifac", "", with_unifac_);

        // Numerics.
        Nloop_a_ = ops.Get<int>("Nloop.hydrophilic", "", Nloop_a_);
        epsilon_a_ = ops.Get<double>("epsilon.hydrophilic", "", epsilon_a_);

        Nloop_b_ = ops.Get<int>("Nloop.hydrophobic", "", Nloop_b_);
        epsilon_b_ = ops.Get<double>("epsilon.hydrophobic", "", epsilon_b_);

        Nloop_h_ = ops.Get<int>("Nloop.hydronium", "", Nloop_h_);
        epsilon_h_ = ops.Get<double>("epsilon.hydronium", "", epsilon_h_);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Hydrophilic physico/chemical data :" << endl; 
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\thenry_constant (mol.L^{-1}.atm^{-1}) = "
                         << henry_constant_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tvaporization_enthalpy (J.mol^{-1}) = "
                         << vaporization_enthalpy_hydrophilic_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tsaturation_vapor_pressure (Pa) = "
                         << saturation_vapor_pressure_hydrophilic_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tdrh = " << drh_ << endl;
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Hydrophobic physico/chemical data :" << endl; 
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tvaporization_enthalpy (J.mol^{-1}) = "
                         << vaporization_enthalpy_hydrophobic_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tsaturation_vapor_pressure (Pa) = "
                         << saturation_vapor_pressure_hydrophobic_ << endl;
#endif

    // Oligomerization.
    if (with_oligomerization_)
      {
        ops.SetPrefix(this->prefix_ + "physic.oligomerization.");
        ops.Set("species_index", "", oligomerization_species_index_);
        ops.Set("constant", "", oligomerization_constant_);
        Noligomerization_ = int(oligomerization_species_index_.size());
        oligomerization_pH_reference_ = ops.Get<real>("pH_reference", "", oligomerization_pH_reference_);
        oligomerization_power_ = ops.Get<real>("power", "", oligomerization_power_);
        oligomerization_hp_mol_reference_ = pow(real(10), - oligomerization_pH_reference_);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Oligomerization data :" << endl; 
        *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "\tNoligomerization = " << Noligomerization_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "\tpower = " << oligomerization_power_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "\tpH_refrerence = " << oligomerization_pH_reference_ << endl;
        for (int i = 0; i < Noligomerization_; i++)
          *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "\tspecies = "
                               << name_hydrophilic_[oligomerization_species_index_[i]]
                               << ", constant = " << oligomerization_constant_[i] << endl;
#endif
      }

    // Unifac data. "a" is hydrophilic.
    // Let you guess what is "b". Read only if used.
    if (with_unifac_)
      {
        ops.SetPrefix(this->prefix_ + "unifac.hydrophilic.");
        nfunc_a_ = ops.Get<int>("number_functional_group");
        ops.Set("rg", "", rg_a_);
        ops.Set("qg", "", qg_a_);

        if (int(rg_a_.size()) != nfunc_a_)
          throw AMC::Error("Wrong length for \"rg\" in section \"" + ops.GetPrefix() + "\".");
        if (int(qg_a_.size()) != nfunc_a_)
          throw AMC::Error("Wrong length for \"qg\" in section \"" + ops.GetPrefix() + "\".");

        nu_a_.resize(nfunc_a_ * Nhydrophilic_semivolatile_water_);
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            vector1i itmp;
            ops.Set("nu." + name_hydrophilic_[i], "", itmp);

            if (int(itmp.size()) != nfunc_a_)
              throw AMC::Error("Wrong length for row " + to_str(i) + " of nu in section \"" + ops.GetPrefix() + "\".");

            for (int j = 0; j < nfunc_a_; j++)
              nu_a_[j * Nhydrophilic_semivolatile_water_ + i] = itmp[j];
          }

        // Ensures water is at the end.
        {
          vector1i itmp;
          ops.Set("nu.WATER", "", itmp);
          for (int j = 0; j < nfunc_a_; j++)
            nu_a_[j * Nhydrophilic_semivolatile_water_ + Nhydrophilic_semivolatile_] = itmp[j];
        }

        A_a_.resize(nfunc_a_ * nfunc_a_);
        for (int i = 0; i < nfunc_a_; i++)
          {
            vector1r rtmp;
            ops.Set("A.r" + to_str(i), "", rtmp);

            if (int(rtmp.size()) != nfunc_a_)
              throw AMC::Error("Wrong length for row " + to_str(i) + " of A in section \"" + ops.GetPrefix() + "\".");

            for (int j = 0; j < nfunc_a_; j++)
              A_a_[j * nfunc_a_ + i] = rtmp[j];
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "<==== UNIFAC HYDROPHILIC DATA ====>" << endl; 
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "nfunc_a = " << nfunc_a_ << endl; 
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "rg_a = " << rg_a_ << endl; 
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "qg_a = " << qg_a_ << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "nu_a = " << nu_a_ << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "A_a = " << A_a_ << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "<==== UNIFAC HYDROPHILIC DATA ====>" << endl; 
#endif

        // Again for hydrophobic module.
        ops.SetPrefix(this->prefix_ + "unifac.hydrophobic.");
        nfunc_b_ = ops.Get<int>("number_functional_group");
        ops.Set("rg", "", rg_b_);
        ops.Set("qg", "", qg_b_);

        if (int(rg_b_.size()) != nfunc_b_)
          throw AMC::Error("Wrong length for \"rg\" in section \"" + ops.GetPrefix() + "\".");
        if (int(qg_b_.size()) != nfunc_b_)
          throw AMC::Error("Wrong length for \"qg\" in section \"" + ops.GetPrefix() + "\".");

        nu_b_.resize(nfunc_b_ * Nhydrophobic_);
        for (int i = 0; i < Nhydrophobic_; i++)
          {
            vector1i itmp;
            ops.Set("nu." + name_hydrophobic_[i], "", itmp);

            if (int(itmp.size()) != nfunc_b_)
              throw AMC::Error("Wrong length for row " + to_str(i) + " of nu in section \"" + ops.GetPrefix() + "\".");

            for (int j = 0; j < nfunc_b_; j++)
              nu_b_[j * Nhydrophobic_ + i] = itmp[j];
          }

        A_b_.resize(nfunc_b_ * nfunc_b_);
        for (int i = 0; i < nfunc_b_; i++)
          {
            vector1r rtmp;
            ops.Set("A.r" + to_str(i), "", rtmp);

            if (int(rtmp.size()) != nfunc_b_)
              throw AMC::Error("Wrong length for row " + to_str(i) + " of A in section \"" + ops.GetPrefix() + "\".");

            for (int j = 0; j < nfunc_b_; j++)
              A_b_[j * nfunc_b_ + i] = rtmp[j];
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "<==== UNIFAC HYDROPHOBIC DATA ====>" << endl; 
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "nfunc_b = " << nfunc_b_ << endl; 
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "rg_b = " << rg_b_ << endl; 
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "qg_b = " << qg_b_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "nu_b = " << nu_b_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "A_b = " << A_b_ << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "<==== UNIFAC HYDROPHOBIC DATA ====>" << endl; 
#endif

        // At last, when everything is loaded, compute gamma infinite dilution coefficients
        // if they were note read from configuration file.
        if (compute_gamma_infinite_dilution)
          ComputeGammaInfiniteDilution();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "gamma_infinite = " << gamma_infinite_ << endl;
#endif
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Use oligomerization ? "
                         << (with_oligomerization_ ? "YES" : "NO") << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Use UNIFAC ? "
                         << (with_unifac_ ? "YES" : "NO") << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "Some numeric data :" << endl; 
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "\thydrophilic :"
                         << " Nloop = " << Nloop_a_ << ", epsilon = " << epsilon_a_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "\thydrophobic :"
                         << " Nloop = " << Nloop_b_ << ", epsilon = " << epsilon_b_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "\thydronium :"
                         << " Nloop = " << Nloop_h_ << ", epsilon = " << epsilon_h_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassModelThermodynamicAEC::~ClassModelThermodynamicAEC()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Debug(3) << Reset() << "Called destructor for AEC." << endl;
#endif

    return;
  }


  // Get methods.
  int ClassModelThermodynamicAEC::GetNspeciesInternal() const
  {
    return Nspecies_internal_;
  }

  int ClassModelThermodynamicAEC::GetNspeciesA() const
  {
    return Nhydrophilic_;
  }

  int ClassModelThermodynamicAEC::GetNspeciesSemivolatileA() const
  {
    return Nhydrophilic_semivolatile_;
  }

  int ClassModelThermodynamicAEC::GetNloopA() const
  {
    return Nloop_a_;
  }

  real ClassModelThermodynamicAEC::GetEpsilonA() const
  {
    return epsilon_a_;
  }

  int ClassModelThermodynamicAEC::GetNspeciesB() const
  {
    return Nhydrophobic_;
  }

  int ClassModelThermodynamicAEC::GetNspeciesSemivolatileB() const
  {
    return Nhydrophobic_semivolatile_;
  }

  int ClassModelThermodynamicAEC::GetNloopB() const
  {
    return Nloop_b_;
  }

  real ClassModelThermodynamicAEC::GetEpsilonB() const
  {
    return epsilon_b_;
  }

  bool ClassModelThermodynamicAEC::IsOligomerizationUsed() const
  {
    return with_oligomerization_;
  }

  bool ClassModelThermodynamicAEC::IsUnifacUsed() const
  {
    return with_unifac_;
  }


  void ClassModelThermodynamicAEC::GetGammaInfiniteDilution(vector<real> &gamma_infinite) const
  {
    gamma_infinite = gamma_infinite_;
  }


  // Set methods.
  void ClassModelThermodynamicAEC::SetNloopA(const int &Nloop)
  {
    Nloop_a_ = Nloop;
  }

  void ClassModelThermodynamicAEC::SetEpsilonA(const real &epsilon)
  {
    epsilon_a_ = epsilon;
  }

  void ClassModelThermodynamicAEC::SetNloopB(const int &Nloop)
  {
    Nloop_b_ = Nloop;
  }

  void ClassModelThermodynamicAEC::SetEpsilonB(const real &epsilon)
  {
    epsilon_b_ = epsilon;
  }

  void ClassModelThermodynamicAEC::ToggleOligomerization()
  {
    with_oligomerization_ = with_oligomerization_ == false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Toggle use of oligomerization, "
                         << "use = " << (with_oligomerization_ ? "yes" : "no") << endl;
#endif
  }

  void ClassModelThermodynamicAEC::ToggleUnifac()
  {
    with_unifac_ = with_unifac_ == false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Toggle use of UNIFAC, "
                         << "use = " << (with_unifac_ ? "yes" : "no") << endl;
#endif
  }


  // Clone.
  ClassModelThermodynamicAEC* ClassModelThermodynamicAEC::clone() const
  {
    return new ClassModelThermodynamicAEC(*this);
  }


  // Compute gamma coefficients.
  void ClassModelThermodynamicAEC::ComputeGammaCoefficientHydrophilic(const real &temperature,
                                                                      const vector<real> &concentration_aer_mass,
                                                                      vector<real> &gamma_hydrophilic) const
  {
    gamma_hydrophilic.assign(Nhydrophilic_semivolatile_, real(1));

    const real &lwc = concentration_aer_mass.back();

#ifdef AEC_WITH_UNIFAC
    // Molar concentrations.
    vector<real> aero_molar(Nhydrophilic_semivolatile_water_);

    aero_molar.back() = lwc / AEC_MOLAR_MASS_WATER;

    real aero_molar_tot(aero_molar.back());
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        aero_molar[i] = concentration_aer_mass[i] / molar_mass_hydrophilic_[i];
        aero_molar_tot += aero_molar[i];
      }

#ifdef AMC_WITH_DEBUG_AEC
    cout << "aero_molar = " << aero_molar << endl;
#endif

    // Molar fractions for UNIFAC.
    vector<double> aero_molar_fraction(Nhydrophilic_semivolatile_water_);
    for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
      aero_molar_fraction[i] =  double(aero_molar[i] / aero_molar_tot);

    // Avoid null molar fraction.
    bool recompute(false);
    for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
      if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
        {
          aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
          recompute = true;
        }

    if (recompute)
      {
        double fraction_sum(double(0));
        for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
          fraction_sum += aero_molar_fraction[i];
        for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
          aero_molar_fraction[i] /= fraction_sum;
      }

#ifdef AMC_WITH_DEBUG_AEC
    cout << "aero_molar_fraction = " << aero_molar_fraction << endl;
#endif

    double temp = double(temperature);
    vector<double> gamma(Nhydrophilic_semivolatile_water_);

    unifac_(const_cast<int*>(&Nhydrophilic_semivolatile_water_), const_cast<int*>(&nfunc_a_),
            const_cast<int*>(nu_a_.data()), aero_molar_fraction.data(), const_cast<double*>(A_a_.data()),
            const_cast<double*>(rg_a_.data()), const_cast<double*>(qg_a_.data()),
            const_cast<double*>(&Z_), &temp, gamma.data());

#ifdef AMC_WITH_DEBUG_AEC
    cout << "gamma = " << gamma << endl;
    cout << "gamma_infinite = " << gamma_infinite_ << endl;
#endif

    // Divide by infinite dilution gamma coefficient.
    // If species not in aerosol, set its activity coefficient to 1.
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      if (aero_molar[i] > real(0))
        gamma_hydrophilic[i] = real(gamma[i] / gamma_infinite_[i]);

#ifdef AMC_WITH_DEBUG_AEC
    cout << "gamma_hydrophilic = " << gamma_hydrophilic << endl;
#endif
#endif
  }


  void ClassModelThermodynamicAEC::ComputeGammaCoefficientHydrophobic(const real &temperature,
                                                                      const vector<real> &concentration_aer_mass,
                                                                      vector<real> &gamma_hydrophobic) const
  {
    gamma_hydrophobic.assign(Nhydrophobic_semivolatile_, real(1));

    const real &prim_mass = concentration_aer_mass.back();

#ifdef AEC_WITH_UNIFAC
    // Molar aerosol concentrations.
    vector<real> aero_molar(Nhydrophobic_, double(0));

    // Partition the primary mass.
    real prim_molar = prim_mass / molar_mass_primary_fixed_;
    int j(Nhydrophobic_semivolatile_);
    for (int i = 0; i < Nhydrophobic_primary_; i++)
      aero_molar[j++] = double(prim_molar * primary_fraction_molar_[i]);

    double aero_molar_tot(prim_molar);
    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      {
        aero_molar[i] = double(concentration_aer_mass[i] / molar_mass_hydrophobic_[i]);
        aero_molar_tot += aero_molar[i];
      }

#ifdef AMC_WITH_DEBUG_AEC
    cout << "aero_molar = " << aero_molar << endl;
#endif

    vector<double> gamma(Nhydrophobic_);
    double temp = double(temperature);

    // Molar fractions for UNIFAC.
    vector<double> aero_molar_fraction(Nhydrophobic_);
    for (int i = 0; i < Nhydrophobic_; i++)
      aero_molar_fraction[i] =  double(aero_molar[i] / aero_molar_tot);

    // Avoid null molar fraction.
    bool recompute(false);
    for (int i = 0; i < Nhydrophobic_; i++)
      if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
        {
          aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
          recompute = true;
        }

    if (recompute)
      {
        double fraction_sum(double(0));
        for (int i = 0; i < Nhydrophobic_; i++)
          fraction_sum += aero_molar_fraction[i];
        for (int i = 0; i < Nhydrophobic_; i++)
          aero_molar_fraction[i] /= fraction_sum;
      }

#ifdef AMC_WITH_DEBUG_AEC
    cout << "aero_molar_fraction = " << aero_molar_fraction << endl;
#endif

    unifac_(const_cast<int*>(&Nhydrophobic_), const_cast<int*>(&nfunc_b_), const_cast<int*>(nu_b_.data()),
            aero_molar_fraction.data(), const_cast<double*>(A_b_.data()), const_cast<double*>(rg_b_.data()),
            const_cast<double*>(qg_b_.data()), const_cast<double*>(&Z_), &temp, gamma.data());

    // If species not in aerosol, set its activity coefficient to 1.
    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      if (aero_molar[i] > real(0))
        gamma_hydrophobic[i] = real(gamma[i]);

#ifdef AMC_WITH_DEBUG_AEC
    cout << "gamma_hydrophobic = " << gamma_hydrophobic << endl;
#endif
#endif
  }


  void ClassModelThermodynamicAEC::ComputeGammaInfiniteDilution(const real fraction_dilute,
                                                                const real temperature_reference)
  {
    gamma_infinite_.assign(Nhydrophilic_semivolatile_, real(1));

#ifdef AEC_WITH_UNIFAC
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        // Molar fractions for UNIFAC.
        vector<double> aero_molar_fraction(Nhydrophilic_semivolatile_water_, double(AEC_MOLAR_FRACTION_TINY));
        aero_molar_fraction[i] = double(fraction_dilute);
        aero_molar_fraction.back() = double(1) - double(fraction_dilute)
          - double(Nhydrophilic_semivolatile_ - 1) * double(AEC_MOLAR_FRACTION_TINY);

#ifdef AMC_WITH_DEBUG_AEC
        cout << "aero_molar_fraction = " << aero_molar_fraction << endl;
#endif

        double temp = double(temperature_reference);
        vector<double> gamma(Nhydrophilic_semivolatile_water_);

        unifac_(const_cast<int*>(&Nhydrophilic_semivolatile_water_), const_cast<int*>(&nfunc_a_),
                const_cast<int*>(nu_a_.data()), aero_molar_fraction.data(), const_cast<double*>(A_a_.data()),
                const_cast<double*>(rg_a_.data()), const_cast<double*>(qg_a_.data()),
                const_cast<double*>(&Z_), &temp, gamma.data());

        gamma_infinite_[i] = real(gamma[i]);
      }

#ifdef AMC_WITH_DEBUG_AEC
    cout << "gamma_infinite = " << gamma_infinite_ << endl;
#endif
#endif
  }


  // Compute local equilibrium.
  void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                           const int &section_max,
                                                           const vector1i &section_index,
                                                           const real *concentration_aer_number,
                                                           const real *concentration_aer_mass) const
  {
    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(Nsemivolatile_);
    vector1r he(Nhydrophilic_semivolatile_);

    for (int h = 0; h < Nhydrophobic_semivolatile_; h++)
      vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[index_hydrophobic_gas_[h]];

    for (int h = 0; h < Nhydrophilic_semivolatile_; h++)
      {
        const int &i = index_hydrophilic_gas_[h];
        vpsat[Nhydrophobic_semivolatile_ + h] = ClassMeteorologicalData::saturation_vapor_pressure_[i];
        he[h] = ClassMeteorologicalData::henry_constant_[i];
      }

    int Nsection = int(section_index.size());

    for (int h = 0; h < Nsection; h++)
      {
        const int i = section_index[h];
        if (i < section_min || i >= section_max)
          continue;

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            vector1r aero_a(Nhydrophilic_, real(0)), gas_a(Nhydrophilic_semivolatile_, real(0));
            vector1r aero_b(Nsemivolatile_, real(0)), gas_b(Nsemivolatile_, real(0));

            // Aerosol is assumed to be thermically at equilibrium with air.
            //ClassAerosolData::temperature_[i] = ClassMeteorologicalData::temperature_;

            // Start index of gas surface concentration.
            const int j = i * ClassSpecies::Ngas_;

            // Start index for mass concentration vector.
            const int k = i * ClassSpecies::Nspecies_;

            // Start index for mass concentration vector.
            const int m = i * ClassSpeciesEquilibrium::Nspecies_;

            //
            // Hydrophilic module.
            //

#ifdef AMC_WITH_LAYER
            for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
              {
                const int n = k + index_hydrophilic_aerosol_[l];
                aero_a[l] = ClassAerosolData::layer_repartition_[n].back() * concentration_aer_mass[n];
              }
#else
            for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
              aero_a[l] = concentration_aer_mass[k + index_hydrophilic_aerosol_[l]];
#endif

            // Grab H+ concentration, if any.
            real hp =  (index_hydronium_ >= 0) ? ClassAerosolData::equilibrium_aer_internal_[m + index_hydronium_] : real(0);
            real lwc = ClassAerosolData::liquid_water_content_[i];

            bool aqueous_phase = module_hydrophilic_local_(ClassMeteorologicalData::temperature_,
                                                           ClassMeteorologicalData::relative_humidity_,
                                                           he, lwc, hp, aero_a, gas_a);

            // If not aqueous phase, give concentrations to hydrophobic module.
            if (aqueous_phase)
              {
                for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
                  ClassAerosolData::equilibrium_gas_surface_[j + index_hydrophilic_gas_[l]] = gas_a[l];

                // Give back water content.
                ClassAerosolData::liquid_water_content_[i] = lwc;

                // The return value contain the possible modification due to organics, usually it is negligible.
                if (index_hydronium_ >= 0)
                  ClassAerosolData::equilibrium_aer_internal_[m + index_hydronium_] = hp;

                // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
                for (int l = 0; l < Nspecies_internal_; l++)
                  ClassAerosolData::equilibrium_aer_internal_[m + species_index_internal_[l][0]]
                    = aero_a[species_index_internal_[l][1]];

                // If in aqueous phase, say that all hydrophilic species are in it.
                for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
                  ClassAerosolData::phase_[k + index_hydrophilic_aerosol_[l]] = ClassPhase::aqueous_phase_index_;
              }
            else
              {
                for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
                  aero_b[Nhydrophobic_semivolatile_ + l] = aero_a[l];

                // If not in aqueous phase, say that all hydrophilic species absorb
                // in the anhydrous phase, which is the second (and last) one in ClassSpecies::phase_ field.
                for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
                  {
                    const int n = index_hydrophilic_aerosol_[l];
                    ClassAerosolData::phase_[k + n] = ClassSpecies::phase_[n].back();
                  }
              }

            //
            // Hydrophobic module.
            //

            // Compute primary mass and its average molar weight
            // from AMC species viewed by AMC as primary mass (Nhydrophobic_primary_amc_),
            // this is different than primary organic AEC species
            // (Nhydrophobic_primary_amc_) on which this primary mass will be mapped.
            real primary_mass(real(0)), primary_mass_mw(real(0));
            for (int l = 0; l < Nhydrophobic_primary_amc_; l++)
              {
                const int n = index_hydrophobic_primary_amc_[l];
                real mass_species = concentration_aer_mass[k + n];
#ifdef AMC_WITH_LAYER
                mass_species *= ClassAerosolData::layer_repartition_[k + n].back();
#endif
                primary_mass += mass_species;
                primary_mass_mw += ClassSpecies::molar_mass_[n] * mass_species;
              }

            if (primary_mass > real(0))
              primary_mass_mw /= primary_mass;
            else
              primary_mass_mw = molar_mass_primary_fixed_;

#ifdef AMC_WITH_LAYER
            for (int l = 0; l < Nhydrophobic_semivolatile_; l++)
              {
                const int n = k + index_hydrophobic_aerosol_[l];
                aero_b[l] = ClassAerosolData::layer_repartition_[n].back()
                  * concentration_aer_mass[n];
              }
#else
            for (int l = 0; l < Nhydrophobic_semivolatile_; l++)
              aero_b[l] = concentration_aer_mass[k + index_hydrophobic_aerosol_[l]];
#endif

            if (module_hydrophobic_local_(ClassMeteorologicalData::temperature_,
                                          primary_mass, primary_mass_mw,
                                          vpsat, aero_b, gas_b))
              {
                for (int l = 0; l < Nhydrophobic_semivolatile_; l++)
                  ClassAerosolData::equilibrium_gas_surface_[j + index_hydrophobic_gas_[l]] = gas_b[l];

                if (! aqueous_phase)
                  for (int l = 0; l < Nhydrophilic_semivolatile_; l++)
                    ClassAerosolData::equilibrium_gas_surface_[j + index_hydrophobic_gas_[l]]
                      = gas_b[Nhydrophobic_semivolatile_ + l];
              }
          }
      }
  }


  // Compute global equilibrium.
  void ClassModelThermodynamicAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                            const int &section_max,
                                                            const real *concentration_gas,
                                                            const real *concentration_aer_mass,
                                                            vector1r &concentration_aer_total,
                                                            vector1r &equilibrium_aer_internal,
                                                            real &liquid_water_content,
                                                            vector1r &concentration_aer_equilibrium,
                                                            vector1r &concentration_gas_equilibrium) const
  {
    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(Nsemivolatile_);
    vector1r he(Nhydrophilic_semivolatile_);

    for (int h = 0; h < Nhydrophobic_semivolatile_; h++)
      vpsat[h] = ClassMeteorologicalData::saturation_vapor_pressure_[index_hydrophobic_gas_[h]];

    for (int h = 0; h < Nhydrophilic_semivolatile_; h++)
      {
        const int &i = index_hydrophilic_gas_[h];
        vpsat[Nhydrophobic_semivolatile_ + h] = ClassMeteorologicalData::saturation_vapor_pressure_[i];
        he[h] = ClassMeteorologicalData::henry_constant_[i];
      }

    // Compute bulk aerosol concentration for each AEC species.
    for (int i = section_min; i < section_max; i++)
      {
        int j = i * ClassSpecies::Nspecies_;
        for (int k = 0; k < Nhydrophilic_semivolatile_; k++)
          {
            const int &l = index_hydrophilic_aerosol_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }

        for (int k = 0; k < Nhydrophobic_semivolatile_; k++)
          {
            const int &l = index_hydrophobic_aerosol_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }

        for (int k = 0; k < Nhydrophobic_primary_amc_; k++)
          {
            const int &l = index_hydrophobic_primary_amc_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }
      }

    //
    // Hydrophilic module.
    //

    vector1r aero_a(Nhydrophilic_, real(0)), gas_a(Nhydrophilic_semivolatile_, real(0));
    vector1r aero_b(Nsemivolatile_, real(0)), gas_b(Nsemivolatile_, real(0));

    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        aero_a[i] = concentration_aer_total[index_hydrophilic_aerosol_[i]];
        gas_a[i] = concentration_gas[index_hydrophilic_gas_[i]];
      }

    // Grab liquid water content and H+ concentration.
    real hp =  (index_hydronium_ >= 0) ? equilibrium_aer_internal[index_hydronium_] : real(0);

    bool aqueous_phase = module_hydrophilic_global_(ClassMeteorologicalData::temperature_,
                                                    ClassMeteorologicalData::relative_humidity_,
                                                    he, liquid_water_content, hp, aero_a, gas_a);

    // If not aqueous phase, give concentrations to hydrophobic module.
    if (aqueous_phase)
      {
        // Give back to concentration vector.
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            concentration_aer_equilibrium[index_hydrophilic_aerosol_[i]] = aero_a[i];
            concentration_gas_equilibrium[index_hydrophilic_gas_[i]] = gas_a[i];
          }

        if (index_hydronium_ >= 0) equilibrium_aer_internal[index_hydronium_] = hp;

        // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
        for (int i = 0; i < Nspecies_internal_; i++)
          equilibrium_aer_internal[species_index_internal_[i][0]] = aero_a[species_index_internal_[i][1]];
      }
    else
      for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
        {
          int j =  Nhydrophobic_semivolatile_ + i;
          aero_b[j] = aero_a[i];
          gas_b[j] = gas_a[i];
        }

    //
    // Hydrophobic module.
    //

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      {
        aero_b[i] = concentration_aer_total[index_hydrophobic_aerosol_[i]];
        gas_b[i] = concentration_gas[index_hydrophobic_gas_[i]];
      }

    // Compute primary mass and its average molar weight.
    real primary_mass(real(0)), primary_mass_mw(real(0));
    for (int i = 0; i < Nhydrophobic_primary_amc_; i++)
      {
        const int &j = index_hydrophobic_primary_amc_[i];
        primary_mass += concentration_aer_total[j];
        primary_mass_mw += ClassSpecies::molar_mass_[j] * concentration_aer_total[j];
      }

    if (primary_mass > real(0))
      primary_mass_mw /= primary_mass;
    else
      primary_mass_mw = molar_mass_primary_fixed_;

    if (module_hydrophobic_global_(ClassMeteorologicalData::temperature_,
                                   primary_mass, primary_mass_mw,
                                   vpsat, aero_b, gas_b))
      {
        // Give back to concentration vector.
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          {
            concentration_aer_equilibrium[index_hydrophobic_aerosol_[i]] = aero_b[i];
            concentration_gas_equilibrium[index_hydrophobic_gas_[i]] = gas_b[i];
          }

        if (! aqueous_phase)
          for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
            {
              int j = Nhydrophobic_semivolatile_ + i;
              concentration_aer_equilibrium[index_hydrophilic_aerosol_[i]] = aero_b[j];
              concentration_gas_equilibrium[index_hydrophilic_gas_[i]] = gas_b[j];
            }
      }
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassModelThermodynamicAEC::Test(const int &mode,
                                        const real &temperature,
                                        const real &relative_humidity,
                                        const map<string, real> &concentration,
                                        map<string, real> &output) const
  {
    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(Nsemivolatile_);
    vector1r he(Nhydrophilic_semivolatile_);

    // Update temperature dependent physical parameters.
    // Van't Hoff coefficient for saturation concentration or
    // partition coefficient change due to change in temperature.
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_hydrophilic_[i]);

        vpsat[Nhydrophobic_semivolatile_ + i] = saturation_vapor_pressure_hydrophilic_[i] * vant_hoff_coefficient;
        he[i] = henry_constant_[i] / vant_hoff_coefficient;
      }

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_hydrophobic_[i]);

        vpsat[i] = saturation_vapor_pressure_hydrophobic_[i] * vant_hoff_coefficient;
      }

    // Internal gas/particle concentration vectors for each phase.
    vector1r aero_a(Nhydrophilic_, real(0)), aero_b(Nsemivolatile_, real(0));
    vector1r gas_a(Nhydrophilic_semivolatile_, real(0)), gas_b(Nsemivolatile_, real(0));

    // Everything in particle phase.
    real hp(real(0)), pH(real(-1));
    real liquid_water_content(real(0));
    real primary_hydrophobic(real(0));

    for (map<string, real>::const_iterator it = concentration.begin(); it != concentration.end(); it++)
      {
        if (it->first == "LWC")
          liquid_water_content = it->second;
        else if (it->first == "pH")
          pH = it->second;
        else if (it->first == "POA")
          primary_hydrophobic = it->second;
        else
          {
            bool found(false);

            for (int i = 0; i < Nhydrophilic_semivolatile_; ++i)
              if (it->first == name_hydrophilic_[i])
                {
                  aero_a[i] = it->second;
                  found = true;
                  break;
                }

            if (! found)
              for (int i = 0; i < Nhydrophobic_semivolatile_; ++i)
                if (it->first == name_hydrophobic_[i])
                  {
                    aero_b[i] = it->second;
                    found = true;
                    break;
                  }

#ifdef AMC_WITH_LOGGER
            if (! found)
              *AMCLogger::GetLog() << Fred() << User(2, "Test") << Reset() << "Could not find species \""
                                   << it->first << "\" in AEC/H2O species list, probably not in model." << endl;
#endif
          }
      }

    // H+ concentration from pH and liquid water content in µg.m^{-3}.
    if (pH >= real(0) && liquid_water_content > real(0))
      hp = pow(real(10), - pH) * liquid_water_content / AEC_CONVERT_HYDRONIUM_FROM_UGM3_TO_LWM3;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << User(0, "Test") << Reset() << "Concentrations given in argument :" << endl;
    *AMCLogger::GetLog() << Fgreen() << User(0, "Test") << Reset() << "\taero_a = " << aero_a << endl;
    *AMCLogger::GetLog() << Fgreen() << User(0, "Test") << Reset() << "\taero_b = " << aero_b << endl;
    *AMCLogger::GetLog() << Fblue() << User(1, "Test") << Reset() << "\tliquid_water_content = " << liquid_water_content << endl;
    *AMCLogger::GetLog() << Fblue() << User(1, "Test") << Reset() << "\tpH = " << pH << ", hp = " << hp << endl;
    *AMCLogger::GetLog() << Fred() << User(1, "Test") << Reset() << "\tprimary_hydrophobic = " << primary_hydrophobic << endl;
#endif

    bool aqueous_phase;

    // Forward or reverse (global or local) mode.
    if (mode == 0)
      {
        aqueous_phase = module_hydrophilic_global_(temperature, relative_humidity,
                                                   he, liquid_water_content,
                                                   hp, aero_a, gas_a);

        if (! aqueous_phase)
          for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
            {
              int j = Nhydrophobic_semivolatile_ + i;
              aero_b[j] = aero_a[j];
              gas_b[j] = gas_a[j];
            }

       bool tsp = module_hydrophobic_global_(temperature,
                                             primary_hydrophobic,
                                             molar_mass_primary_fixed_,
                                             vpsat, aero_b, gas_b);
      }
    else if (mode == 1)
      {
        aqueous_phase = module_hydrophilic_local_(temperature, relative_humidity,
                                                  he, liquid_water_content,
                                                  hp, aero_a, gas_a);

        if (! aqueous_phase)
          for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
            aero_b[Nhydrophobic_semivolatile_ + i] = aero_a[i];

        bool tsp = module_hydrophobic_local_(temperature,
                                             primary_hydrophobic,
                                             molar_mass_primary_fixed_,
                                             vpsat, aero_b, gas_b);
      }
    else
      throw AMC::Error("Unkown mode " + to_str(mode) + ", valid values are 0=global and 1=local.");

    if (! aqueous_phase)
      for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
        {
          int j = Nhydrophobic_semivolatile_ + i;
          aero_a[i] = aero_b[j];
          gas_a[i] = gas_b[j];
        }

    // Outputs in µg.m^{-3}.
    for (int i = 0; i < Nhydrophilic_; i++)
      output[name_hydrophilic_[i]] = aero_a[i];

    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      output[name_hydrophilic_[i] + "g"] = gas_a[i];

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      output[name_hydrophobic_[i]] = aero_b[i];

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      output[name_hydrophobic_[i] + "g"] = gas_b[i];

    // Liquid water content.
    output["LWC"] = liquid_water_content;

    // pH.
    output["pH"] = -log10(hp / liquid_water_content * AEC_CONVERT_HYDRONIUM_FROM_UGM3_TO_LWM3);

    // Total suspended mass.
    output["TSPa"] = liquid_water_content;
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      output["TSPa"] += aero_a[i];

    output["TSPb"] = primary_hydrophobic;
    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      output["TSPb"] += aero_b[i];

    // Also give physical values.
    output["temperature"] = temperature;
    output["relative_humidity"] = relative_humidity;

    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        output["VPSAT_" + name_hydrophilic_[i]] = vpsat[Nhydrophobic_semivolatile_ + i];
        output["HE_" + name_hydrophilic_[i]] = he[i];
      }

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      output["VPSAT_" + name_hydrophobic_[i]] = vpsat[i];
  }


  void ClassModelThermodynamicAEC::TestAMC(const real &temperature,
                                           const real &relative_humidity,
                                           const vector<real> &concentration_aer_mass,
                                           vector<real> &equilibrium_aer_internal,
                                           vector<real> &equilibrium_gas_surface) const
  {
    if (this->standalone_)
      throw AMC::Error("Thermodyanmic model \"" + this->name_ + "\" runs in standalone mode.");

    real &liquid_water_content = equilibrium_aer_internal.back();

    // Saturation vapor pressure and Henry constant.
    vector1r vpsat(Nsemivolatile_);
    vector1r he(Nhydrophilic_semivolatile_);

    // Update temperature dependent physical parameters.
    // Van't Hoff coefficient for saturation concentration or
    // partition coefficient change due to change in temperature.
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_hydrophilic_[i]);

        vpsat[Nhydrophobic_semivolatile_ + i] = saturation_vapor_pressure_hydrophilic_[i] * vant_hoff_coefficient;
        he[i] = henry_constant_[i] / vant_hoff_coefficient;
      }

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      {
        real vant_hoff_coefficient = ClassParameterizationPhysics::
          ComputeSaturationVaporConcentration(temperature, real(1), vaporization_enthalpy_hydrophobic_[i]);

        vpsat[i] = saturation_vapor_pressure_hydrophobic_[i] * vant_hoff_coefficient;
      }

    // Internal gas/particle concentration vectors for each phase.
    vector1r aero_a(Nhydrophilic_, real(0)), aero_b(Nsemivolatile_, real(0));
    vector1r gas_a(Nhydrophilic_semivolatile_, real(0)), gas_b(Nsemivolatile_, real(0));

    //
    // Hydrophilic module.
    //

    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      aero_a[i] = concentration_aer_mass[index_hydrophilic_aerosol_[i]];

    // Grab H+ concentration, if any.
    real hp = (index_hydronium_ >= 0) ? equilibrium_aer_internal[index_hydronium_] : real(0);

    bool aqueous_phase = module_hydrophilic_local_(temperature,
                                                   relative_humidity,
                                                   he, liquid_water_content,
                                                   hp, aero_a, gas_a);

    if (aqueous_phase)
      {
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          equilibrium_gas_surface[index_hydrophilic_gas_[i]] = gas_a[i];

        // The return value contain the possible modification due to organics, usually it is negligible.
        if (index_hydronium_ >= 0) equilibrium_aer_internal[index_hydronium_] = hp;

        // AMC index, AEC index : species_index_internal_[l][0], species_index_internal_[l][1].
        for (int i = 0; i < Nspecies_internal_; i++)
          equilibrium_aer_internal[species_index_internal_[i][0]] = aero_a[species_index_internal_[i][1]];
      }
    else
      for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
        aero_b[Nhydrophobic_semivolatile_ + i] = aero_a[i];

    //
    // Hydrophobic module.
    //

    // Compute primary mass and its average molar weight
    // from AMC species viewed by AMC as primary mass (Nhydrophobic_primary_amc_),
    // this is different than primary organic AEC species
    // (Nhydrophobic_primary_amc_) on which this primary mass will be mapped.
    real primary_mass(real(0)), primary_mass_mw(real(0));
    for (int i = 0; i < Nhydrophobic_primary_amc_; i++)
      {
        const int &j = index_hydrophobic_primary_amc_[i];
        primary_mass += concentration_aer_mass[j];
        primary_mass_mw += ClassSpecies::molar_mass_[j] * concentration_aer_mass[j];
      }

    if (primary_mass > real(0))
      primary_mass_mw /= primary_mass;
    else
      primary_mass_mw = molar_mass_primary_fixed_;

    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      aero_b[i] = concentration_aer_mass[index_hydrophobic_aerosol_[i]];

    if (module_hydrophobic_local_(temperature,
                                  primary_mass, primary_mass_mw,
                                  vpsat, aero_b, gas_b))
      {
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          equilibrium_gas_surface[index_hydrophobic_gas_[i]] = gas_b[i];

        if (! aqueous_phase)
          for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
            equilibrium_gas_surface[index_hydrophilic_gas_[i]] = gas_b[Nhydrophobic_semivolatile_ + i];
      }
  }
#endif
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_AEC_CXX
#endif
