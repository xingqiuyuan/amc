// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AEC_CXX

namespace AMC
{
  // Hydrophilic module.
  bool ClassModelThermodynamicAEC::module_hydrophilic_local_(const real &temp,
                                                             const real &rh,
                                                             vector1r &he,
                                                             real &lwc, real &hp,
                                                             vector1r &aero,
                                                             vector1r &gas) const
  {
#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "lwc = " << lwc << endl;
    CBUG << "hp = " << hp << endl;
#endif

    // Partitioning coefficient.
    vector1r kpart(Nhydrophilic_semivolatile_);

    // Molar aerosol concentrations.
    vector1r aero_molar(Nhydrophilic_semivolatile_water_);

    // Total and organic for liquid water content.
    real lwc_total(lwc), lwc_organic(real(0));

    // Molar water content.
    aero_molar.back() = lwc_total / AEC_MOLAR_MASS_WATER;

    real aero_molar_tot(aero_molar.back());
    for (int i = 0; i < Nhydrophilic_semivolatile_; ++i)
      {
        aero_molar[i] = aero[i] / molar_mass_hydrophilic_[i];
        aero_molar_tot += aero_molar[i];
      }

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "aero_molar = " << aero_molar << endl;
#endif

    // If neither water nor organics, there is nothing to do.
    // No water but organics matter may bring some water content,
    // no organic matter but water would allow organics to absorb.
    if (aero_molar_tot == real(0))
      return false;

    // Compute activity coefficients.
    vector1r gamma(Nhydrophilic_semivolatile_water_, real(1));

    // Aerosol water fraction.
    real water_fraction = rh;

#ifdef AEC_WITH_UNIFAC
    if (with_unifac_)
      {
        vector<double> gamma2(Nhydrophilic_semivolatile_water_);
        double temp2 = double(temp);

        // Molar fractions for UNIFAC.
        vector<double> aero_molar_fraction(Nhydrophilic_semivolatile_water_);
        for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
          aero_molar_fraction[i] =  double(aero_molar[i] / aero_molar_tot);

        // Avoid null molar fraction.
        bool recompute(false);
        for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
          if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
            {
              aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
              recompute = true;
            }

        if (recompute)
          {
            double fraction_sum(double(0));
            for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
              fraction_sum += aero_molar_fraction[i];
            for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
              aero_molar_fraction[i] /= fraction_sum;
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "aero_molar_fractionA = " << aero_molar_fraction << endl;
#endif

        unifac_(const_cast<int*>(&Nhydrophilic_semivolatile_water_), const_cast<int*>(&nfunc_a_),
                const_cast<int*>(nu_a_.data()), aero_molar_fraction.data(), const_cast<double*>(A_a_.data()),
                const_cast<double*>(rg_a_.data()), const_cast<double*>(qg_a_.data()),
                const_cast<double*>(&Z_), &temp2, gamma2.data());

        // Divide by infinite dilution gamma coefficient.
        // If species not in aerosol, set its activity coefficient to 1.
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          if (aero_molar[i] > real(0))
            gamma[i] = real(gamma2[i] / gamma_infinite_[i]);

        // Water activity.
        gamma.back() = real(gamma2.back());

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "gammaA = " << gamma << endl;
#endif

        // Update the aerosol water fraction thanks to water activity coefficient.
        if (gamma.back() > real(0))
          water_fraction /= gamma.back();
      }
#endif

    // Liquid water content from organics.
    real mol_solute(real(0));
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      mol_solute += aero_molar[i];

    // If valid water fraction add water associated with organics to lwc, in µg.m^{-3}.
    if (water_fraction < real(1))
      lwc_organic = water_fraction / (real(1) - water_fraction) * mol_solute * AEC_MOLAR_MASS_WATER;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "lwc_organic = " << lwc_organic << endl;
#endif

    // Add water content from organics.
    lwc_total += lwc_organic;

    // If RH is below all DRH, hydrophilic organics will
    // not be in aqueous phase. In practice, considered DRH data,
    // this condition may always be true.
    bool rh_below_all_drh = true;
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      if (rh >= drh_[i])
        {
          rh_below_all_drh = false;
          break;
        }

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "rh_below_all_drh = " << rh_below_all_drh << endl;
#endif

    // The following is only relevant if there is water.
    // If the inorganic module has a metastable mode on,
    // there will always be a liquid water content,
    // so this first case below should be the dominant one.
    if (lwc_total == real(0) || rh_below_all_drh)
      return false;

    // H+ mol.L^{-1} concentration, which preexist from inorganics.
    // equivalent to mol.kg^{-1} as water density is 1 kg.L^{-1}
    real hp_mol_exist = AEC_HP_MOL_DEFAULT;
    if (hp > real(0)) hp_mol_exist = hp / lwc_total * AEC_CONVERT_HYDRONIUM_FROM_UGM3_TO_LWM3;

    // Convert lwc_total from µg.m^{-3} to liter per m3 (of air).
    lwc_total *= AEC_CONVERT_LWC_FROM_UGM3_TO_LM3;

    real hp_mol_old;
    real hp_mol(hp_mol_exist);
    for (int h = 0; h < Nloop_h_; h++)
      {
        // H+ concentration from following ionic dissociation.
        real hp_mol_from_dissociation(real(0));

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "h = " << h << endl;
        CBUG << "hp_mol = " << hp_mol << endl;
#endif

        // Compute partitioning coefficient from Henry law and dissociation reactions.
        for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
          {
            const vector1i &reaction_dissolution_species_index = reaction_dissolution_species_index_[i];
            const vector1r &reaction_dissolution_constant = reaction_dissolution_constant_[i];

            // Molar based concentrations of non dissociated form.
            // aero_molar in mol.m^{-3} (of air), lwc_total in L.m^{-3}, hence aero in mol.L^{-1}
            aero[reaction_dissolution_species_index[0]] = aero_molar[i] / lwc_total;

            // In the partitioning constant computation, we assumed
            // activity constant for ionic species equals to that of non dissociated form.
            // In case of very concentrated particle (low liquid water
            // content) this may be not right, notably it would
            // enhance ionic dissociation (as discussed with Florian Couvidat).

            if (Nreaction_dissolution_[i] > 0)
              {
                for (int j = 0; j < Nreaction_dissolution_[i]; j++)
                  {
                    // Dissolution constant in mol.L^{-1} of water. H+ concentration also in mol.L^{-1}.
                    const real b = reaction_dissolution_constant[j] + hp_mol;
                    const real c = reaction_dissolution_constant[j] * aero[reaction_dissolution_species_index[j]];
                    aero[reaction_dissolution_species_index[j + 1]] = (-b + sqrt(b * b + real(4) * c)) * real(0.5);

                    // Mass conservation for non dissociated form.
                    aero[reaction_dissolution_species_index[j]] -= aero[reaction_dissolution_species_index[j + 1]];
                  }

                // Grab new H+ concentration.
                for (int j = 1; j <= Nreaction_dissolution_[i]; j++)
                  hp_mol_from_dissociation += real(j) * aero[reaction_dissolution_species_index[j]];
              }
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "hp_mol_from_dissociation = " << hp_mol_from_dissociation << endl;
#endif

        // Keep track of old hp concentration.
        hp_mol_old = hp_mol;
        hp_mol = hp_mol_exist + hp_mol_from_dissociation;

        if (abs(hp_mol_old - hp_mol) < epsilon_h_ * hp_mol_old)
          break;
      }

    // Eventually correct Henry constant due to oligomerization.
    if (with_oligomerization_)
      {
        real hp_mol_oligo = hp_mol;

        // Limit H+ mol concentration to realistic values in case something wrong happened before.
        if (hp_mol_oligo > real(AEC_OLIGOMERIZATION_HP_MOL_MAXIMUM))
          hp_mol_oligo = real(AEC_OLIGOMERIZATION_HP_MOL_MAXIMUM);

        if (hp_mol_oligo < real(AEC_OLIGOMERIZATION_HP_MOL_MINIMUM))
          hp_mol_oligo = real(AEC_OLIGOMERIZATION_HP_MOL_MINIMUM);

        // Oligo correction due to pH.
        real ph_correc = pow(hp_mol_oligo / oligomerization_hp_mol_reference_, AEC_OLIGOMERIZATION_POWER);

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "pH_correc = " << ph_correc << endl;
#endif

        for (int i = 0; i < Noligomerization_; i++)
          {
            const int j = oligomerization_species_index_[i];
            he[j] *= (real(1) + oligomerization_constant_[i] * ph_correc);
          }
      }

    // Thanks to H+ concentration, now compute partitionining constant.
    // And turns dissociated concentrations from mol.L^{-1} to µg.m^{-3} of air.
    for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
      {
        kpart[i] = real(1);

        const vector1i &reaction_dissolution_species_index = reaction_dissolution_species_index_[i];
        const vector1r &reaction_dissolution_constant = reaction_dissolution_constant_[i];

        if (Nreaction_dissolution_[i] > 0)
          {
            for (int j = Nreaction_dissolution_[i] - 1; j >= 0; j--)
              kpart[i] = kpart[i] * reaction_dissolution_constant[j] / hp_mol + real(1);

            for (int j = 0; j <= Nreaction_dissolution_[i]; j++)
              {
                const int k = reaction_dissolution_species_index[j];

                // aero[k] in mol.L^{-1}, molar_mass_hydrophilic_[k] in µg.mol^{-1}
                // lwc_total in L.m^{-3}, hence aero[k] finally in µg.m^{-3}.
                aero[k] *= molar_mass_hydrophilic_[k] * lwc_total;
              }
          }
        else
          {
            const int k = reaction_dissolution_species_index[0];
            aero[k] *= molar_mass_hydrophilic_[k] * lwc_total;
          }
      }

    // Revert to usual concentrations in µg.m^{-3}
    lwc_total *= AEC_CONVERT_LWC_FROM_LM3_TO_UGM3;

    // Compute partitioning coefficient from Henry law and dissociation reactions.
    // Substract one because of previous loop.
    for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
      kpart[i] = (kpart[i] - real(1) + real(1) / gamma[i])
        * he[i] * AEC_HENRY_CONSTANT_UNIT_CONVERSION * temp;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "kpartA = " << kpart << endl;
#endif

    // We are now ready to compute gas equilibrium concentrations.
    for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
      gas[i] = aero[i] / (lwc_total * kpart[i]);

    // This module should in fact perform a loop, as modified lwc and pH
    // will affect Henry constants, fractions, hence activity coefficients (gamma), ...
    // But in view of 3D applications we just avoid it.

    // Overide liquid water content.
    lwc = lwc_total;

    // hp_mol in mol.L^{-1}, i.e. 1.e6 µg.L^{-3}
    // lwc_total in µg.m^{-3}, i.e. 1.e-9 L.m^{-3}
    hp = hp_mol * lwc_total * AEC_CONVERT_HYDRONIUM_FROM_LWM3_TO_UGM3;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "lwc = " << lwc << endl;
    CBUG << "hp = " << hp << endl;
#endif

    return true;
  }


  bool ClassModelThermodynamicAEC::module_hydrophilic_global_(const real &temp,
                                                              const real &rh,
                                                              vector1r &he,
                                                              real &lwc, real &hp,
                                                              vector1r &aero,
                                                              vector1r &gas) const
  {
    // Mass conservation.
    vector1r mass_cons(Nhydrophilic_semivolatile_);
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      mass_cons[i] = aero[i] + gas[i];

    // Compute total suspended mass, plus water.
    real tsp(lwc);
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      tsp += mass_cons[i];

    // If neither water nor organics (gas ans particle phase), there is nothing to do.
    // No water but organics matter may bring some water content,
    // no organic matter but water would allow organics to absorb.
    if (tsp == real(0))
      return false;

    bool rh_below_all_drh = true;
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      if (rh >= drh_[i])
        {
          rh_below_all_drh = false;
          break;
        }

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "rh_below_all_drh = " << rh_below_all_drh << endl;
#endif

    // Partitioning coefficient.
    vector1r coef(Nhydrophilic_semivolatile_);

    // Molar aerosol concentrations.
    vector1r aero_molar(Nhydrophilic_semivolatile_water_);

    // Activity coefficients.
    vector1r gamma(Nhydrophilic_semivolatile_water_, real(1));

    // Total and organic for liquid water content.
    real lwc_total(real(0)), lwc_organic(real(0));

    // H+ concentration from following ionic dissociation.
    real hp_mol_from_dissociation(real(0));

    // H+ mol.L^{-1} concentration.
    // equivalent to mol.kg^{-1} as water density is 1 kg.L^{-1}
    real hp_mol, hp_mol_exist;


    //
    // Solving loop.
    //


    vector1r aero_old;
    for (int h = 0; h < Nloop_a_; h++)
      {
#ifdef AMC_WITH_DEBUG_AEC
        CBUG << endl << "h = " << h << endl;
#endif
        // Update liquid water content with that of organics from previous loop. Null at first.
        lwc_total = lwc + lwc_organic;

        // Molar water content.
        aero_molar.back() = lwc_total / AEC_MOLAR_MASS_WATER;

        // Update semivolatile.
        real aero_molar_tot = aero_molar.back();
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            aero_molar[i] = aero[i] / molar_mass_hydrophilic_[i];
            aero_molar_tot += aero_molar[i];
          }

        // Compute activity coefficients.
        gamma.assign(Nhydrophilic_semivolatile_water_, real(1));

        // Aerosol water fraction.
        real water_fraction = rh;

#ifdef AEC_WITH_UNIFAC
        if (with_unifac_)
          {
            double temp2 = double(temp);
            vector<double> gamma2(Nhydrophilic_semivolatile_water_);
            vector<double> aero_molar_fraction(Nhydrophilic_semivolatile_water_);

            // Molar fractions for UNIFAC.
            for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
              aero_molar_fraction[i] =  double(aero_molar[i] / aero_molar_tot);

            // Avoid null molar fraction.
            bool recompute(false);
            for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
              if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
                {
                  aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
                  recompute = true;
                }

            if (recompute)
              {
                double fraction_sum(double(0));
                for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
                  fraction_sum += aero_molar_fraction[i];
                for (int i = 0; i < Nhydrophilic_semivolatile_water_; i++)
                  aero_molar_fraction[i] /= fraction_sum;
              }

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "aero_molar_fractionA = " << aero_molar_fraction << endl;
#endif

            unifac_(const_cast<int*>(&Nhydrophilic_semivolatile_water_), const_cast<int*>(&nfunc_a_),
                    const_cast<int*>(nu_a_.data()), aero_molar_fraction.data(), const_cast<double*>(A_a_.data()),
                    const_cast<double*>(rg_a_.data()), const_cast<double*>(qg_a_.data()),
                    const_cast<double*>(&Z_), &temp2, gamma2.data());

            // Divide by infinite dilution gamma coefficient.
            // If species not in aerosol, set its activity coefficient to 1.
            for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
              if (aero_molar[i] > real(0))
                gamma[i] = real(gamma2[i] / gamma_infinite_[i]);

            // Water activity.
            gamma.back() = real(gamma2.back());

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "gammaA = " << gamma << endl;
#endif

            // Update the aerosol water fraction thanks to water activity coefficient.
            if (gamma.back() > real(0))
              water_fraction /= gamma.back();
          }
#endif

        // Liquid water content from organics.
        real mol_solute(real(0));
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          mol_solute += aero_molar[i];

        // If valid water fraction compute lwc of organics, in µg.m^{-3}
        if (water_fraction < real(1))
          lwc_organic = water_fraction / (real(1) - water_fraction) * mol_solute * AEC_MOLAR_MASS_WATER;

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "lwc_organic = " << lwc_organic << endl;
#endif

        // Update liquid water content with that of organics from previous loop. Null at first.
        lwc_total = lwc + lwc_organic;

        if (lwc_total == real(0) || rh_below_all_drh)
          return false;

        // H+ mol.L^{-1} concentration, which preexist from inorganics.
        // equivalent to mol.kg^{-1} as water density is 1 kg.L^{-1}
        hp_mol_exist = AEC_HP_MOL_DEFAULT;
        if (hp > real(0)) hp_mol_exist = hp / lwc_total * AEC_CONVERT_HYDRONIUM_FROM_UGM3_TO_LWM3;

        // Convert lwc_total from µg.m^{-3} to liter per m3 (of air).
        lwc_total *= AEC_CONVERT_LWC_FROM_UGM3_TO_LM3;

        real hp_mol_old;
        if (hp_mol < hp_mol_exist)
          hp_mol = hp_mol_exist;

        for (int g = 0; g < Nloop_h_; g++)
          {
            // H+ concentration from following ionic dissociation.
            hp_mol_from_dissociation  = real(0);

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "g = " << g << endl;
            CBUG << "hp_mol = " << hp_mol << endl;
#endif

            // Compute partitioning coefficient from Henry law and dissociation reactions.
            for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
              {
                const vector1i &reaction_dissolution_species_index = reaction_dissolution_species_index_[i];
                const vector1r &reaction_dissolution_constant = reaction_dissolution_constant_[i];

                // Molar based concentrations of non dissociated form.
                // aero_molar in mol.m^{-3} (of air), lwc_total in L.m^{-3}
                aero[reaction_dissolution_species_index[0]] = aero_molar[i] / lwc_total;

                // In the partitioning constant computation, we assumed
                // activity constant for ionic species equals to that of non dissociated form.
                // In case of very concentrated particle (low liquid water
                // content) this may be not right, notably it would
                // enhance ionic dissociation (as discussed with Florian Couvidat).

                if (Nreaction_dissolution_[i] > 0)
                  {
                    for (int j = 0; j < Nreaction_dissolution_[i]; j++)
                      {
                        real b = reaction_dissolution_constant[j] + hp_mol;
                        real c = reaction_dissolution_constant[j] * aero[reaction_dissolution_species_index[j]];
                        aero[reaction_dissolution_species_index[j + 1]] = (-b + sqrt(b * b + real(4) * c)) * real(0.5);

                        // Mass conservation for non dissociated form.
                        aero[reaction_dissolution_species_index[j]] -= aero[reaction_dissolution_species_index[j + 1]];
                      }

                    // Grab new H+ concentration.
                    for (int j = 1; j <= Nreaction_dissolution_[i]; j++)
                      hp_mol_from_dissociation += real(j) * aero[reaction_dissolution_species_index[j]];
                  }
              }

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "hp_mol_from_dissociation = " << hp_mol_from_dissociation << endl;
#endif

            // Keep track of old hp concentration.
            hp_mol_old = hp_mol;
            hp_mol = hp_mol_exist + hp_mol_from_dissociation;

            if (abs(hp_mol_old - hp_mol) < epsilon_h_ * hp_mol_old)
              break;
          }

        // Eventually correct Henry constant due to oligomerization.
        if (with_oligomerization_)
          {
            real hp_mol_oligo = hp_mol;

            // Limit H+ mol concentration to realistic values in case something wrong happened before.
            if (hp_mol_oligo > real(AEC_OLIGOMERIZATION_HP_MOL_MAXIMUM))
              hp_mol_oligo = real(AEC_OLIGOMERIZATION_HP_MOL_MAXIMUM);

            if (hp_mol_oligo < real(AEC_OLIGOMERIZATION_HP_MOL_MINIMUM))
              hp_mol_oligo = real(AEC_OLIGOMERIZATION_HP_MOL_MINIMUM);

            // Oligo correction due to pH.
            real ph_correc = pow(hp_mol / oligomerization_hp_mol_reference_, AEC_OLIGOMERIZATION_POWER);

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "pH_correc = " << ph_correc << endl;
#endif

            for (int i = 0; i < Noligomerization_; i++)
              {
                int j = oligomerization_species_index_[i];
                he[j] *= (real(1) + oligomerization_constant_[i] * ph_correc);
              }
          }

        // Compute partitioning coefficient from Henry law and dissociation reactions.
        // Liquid water content has to be in µg.m^{-3}.
        coef.assign(Nhydrophilic_semivolatile_, lwc_total * AEC_CONVERT_LWC_FROM_LM3_TO_UGM3);

        // Thanks to H+ concentration, now compute partitionining constant.
        // And turns dissociated concentrations from mol.L^{-1} to µg.m^{-3} of air.
        for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
          {
            real kpart = real(1);

            if (Nreaction_dissolution_[i] > 0)
              {
                const vector1i &reaction_dissolution_species_index = reaction_dissolution_species_index_[i];
                const vector1r &reaction_dissolution_constant = reaction_dissolution_constant_[i];

                for (int j = Nreaction_dissolution_[i] - 1; j >= 0; j--)
                  kpart = kpart * reaction_dissolution_constant[j] / hp_mol + real(1);
              }

            // Substract one because of previous loop.
            kpart = (kpart - real(1) + real(1) / gamma[i])
              * he[i] * AEC_HENRY_CONSTANT_UNIT_CONVERSION * temp;

            coef[i] *= kpart;
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "coefA = " << coef << endl;
#endif

        // Keep old concentrations and update aerosol concentrations.
        aero_old = aero;

        for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
          aero[i] = double(coef[i] / (real(1) + coef[i]) * mass_cons[i]);

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "aeroA = " << aero << endl;
#endif

        // Does loop converges ?
        real sum(real(0)), abs_diff(real(0));
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          {
            sum += aero_old[i];
            abs_diff += abs(aero_old[i] - aero[i]);
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "abs_diff = " << abs_diff << ", epsilon_a_ * sum = " << epsilon_a_ * sum << endl;
#endif

        if (abs_diff < epsilon_a_ * sum)
          break;

        // Compute total suspended mass.
        tsp = real(0);
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          tsp += aero[i];

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "tspA = " << tsp << endl;
#endif
      }

    // Give back solution. Gas from mass conservation.
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      gas[i] = mass_cons[i] - aero[i];

    // H+ concentration from following ionic dissociation.
    hp_mol_from_dissociation = real(0);

    for (int i = 0; i <  Nhydrophilic_semivolatile_; i++)
      {
        const vector1i &reaction_dissolution_species_index = reaction_dissolution_species_index_[i];
        const vector1r &reaction_dissolution_constant = reaction_dissolution_constant_[i];

        // Molar based concentrations of non dissociated form.
        // aero_molar in mol.m^{-3} (of air), lwc_total in L.m^{-3}
        aero[reaction_dissolution_species_index[0]] = aero[i] / (lwc_total * molar_mass_hydrophilic_[i]);

        if (Nreaction_dissolution_[i] > 0)
          {
            for (int j = 0; j < Nreaction_dissolution_[i]; j++)
              {
                real b = reaction_dissolution_constant[j] + hp_mol;
                real c = reaction_dissolution_constant[j] * aero[reaction_dissolution_species_index[j]];
                aero[reaction_dissolution_species_index[j + 1]] = (-b + sqrt(b * b + real(4) * c)) * real(0.5);

                // Mass conservation for non dissociated form.
                aero[reaction_dissolution_species_index[j]] -= aero[reaction_dissolution_species_index[j + 1]];
              }

            // Grab new H+ concentration.
            for (int j = 1; j <= Nreaction_dissolution_[i]; j++)
              hp_mol_from_dissociation += real(j) * aero[reaction_dissolution_species_index[j]];

            for (int j = 0; j <= Nreaction_dissolution_[i]; j++)
              {
                const int &k = reaction_dissolution_species_index[j];

                // aero[k] in mol.L^{-1}, molar_mass_hydrophilic_[k] in µg.mol^{-1}
                // lwc_total in L.m^{-3}, hence aero[k] finally in µg.m^{-3}.
                aero[k] *= molar_mass_hydrophilic_[k] * lwc_total;
              }
          }
        else
          {
            const int &k = reaction_dissolution_species_index[0];
            aero[k] *= molar_mass_hydrophilic_[k] * lwc_total;
          }
      }

    // hp_mol_exist from last loop update.
    hp_mol = hp_mol_exist + hp_mol_from_dissociation;

    // Revert to usual concentrations.
    lwc_total *= AEC_CONVERT_LWC_FROM_LM3_TO_UGM3;

    // Overide liquid water content.
    lwc = lwc_total;

    // hp_mol in mol.L^{-1}, i.e. 1.e6 µg.L^{-3}
    // lwc_total in µg.m^{-3}, i.e. 1.e-9 L.m^{-3}
    hp = hp_mol * lwc_total * AEC_CONVERT_HYDRONIUM_FROM_LWM3_TO_UGM3;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "lwc = " << lwc << endl;
    CBUG << "hp = " << hp << endl;
#endif

    return true;
  }


  // Hydrophobic module.
  bool ClassModelThermodynamicAEC::module_hydrophobic_local_(const real &temp,
                                                             const real &prim_mass,
                                                             const real &prim_mw,
                                                             const vector1r &vpsat,
                                                             const vector1r &aero,
                                                             vector1r &gas) const
  {
    // Compute total suspended mass.
    real tsp(prim_mass);
    for (int i = 0; i < Nsemivolatile_; i++)
      tsp += aero[i];

    // If nothing in that phase, set equilibrium gas concentration
    // to the current bulk gas concentration to prevent condensation,
    // as in theory, absorption cannot take place if no absorbing material.
    if (tsp == real(0))
      return false;

    // Partition the primary mass.
    real prim_molar = prim_mass / prim_mw;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "prim_molar = " << prim_molar << endl;
#endif

    // Molar aerosol concentrations.
    vector1r aero_molar(Nhydrophobic_total_);
    for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
      aero_molar[i] = aero[i] / molar_mass_hydrophobic_[i];

    int j(Nhydrophobic_semivolatile_);
    for (int i = 0; i < Nhydrophobic_primary_; i++)
      aero_molar[j++] = prim_molar * primary_fraction_molar_[i];

    // Account for possible hydrophilic concentrations.
    int k(Nhydrophobic_semivolatile_);
    for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
      aero_molar[j++] = aero[k++] / molar_mass_hydrophilic_[i];

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "aero_molar = " << aero_molar << endl;
#endif

    real aero_molar_tot(prim_molar);
    for (int i = 0; i < Nhydrophobic_total_; i++)
      aero_molar_tot += aero_molar[i];

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << "aero_molar_tot = " << aero_molar_tot << endl;
#endif

    // Average mol weight.
    real mwa_inv = prim_molar;
    for (int i = 0; i < Nhydrophobic_total_; i++)
      mwa_inv += aero_molar[i];
    mwa_inv /= tsp;

    // Compute activity coefficients.
    vector1r gamma(Nsemivolatile_, real(1));

#ifdef AEC_WITH_UNIFAC
    if (with_unifac_)
      {
        double temp2 = double(temp);
        vector<double> aero_molar_fraction(Nhydrophobic_, double(0));
        vector<double> gamma2(Nhydrophobic_);

        // Molar fractions for UNIFAC.
        for (int i = 0; i < Nhydrophobic_; i++)
          aero_molar_fraction[i] =  double(aero_molar[i] / aero_molar_tot);

        // Avoid null molar fraction.
        bool recompute(false);
        for (int i = 0; i < Nhydrophobic_; i++)
          if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
            {
              aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
              recompute = true;
            }

        if (recompute)
          {
            double fraction_sum(double(0));
            for (int i = 0; i < Nhydrophobic_; i++)
              fraction_sum += aero_molar_fraction[i];
            for (int i = 0; i < Nhydrophobic_; i++)
              aero_molar_fraction[i] /= fraction_sum;
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "aero_molar_fractionB = " << aero_molar_fraction << endl;
#endif

        unifac_(const_cast<int*>(&Nhydrophobic_), const_cast<int*>(&nfunc_b_), const_cast<int*>(nu_b_.data()),
                aero_molar_fraction.data(), const_cast<double*>(A_b_.data()), const_cast<double*>(rg_b_.data()),
                const_cast<double*>(qg_b_.data()), const_cast<double*>(&Z_), &temp2, gamma2.data());

        // If species not in aerosol, set its activity coefficient to 1.
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          if (aero_molar[i] > real(0))
            gamma[i] = real(gamma2[i]);

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "gammaB = " << gamma << endl;
#endif
      }
#endif

    // Compute gas equilibrium concentration.
    for (int i = 0; i < Nsemivolatile_; i++)
      {
        // Partition coefficient in m3/µg
        real K = THERMODYNAMIC_IDEAL_GAS_CONSTANT * temp * mwa_inv / (vpsat[i] * gamma[i]);

        // Local equilibrium gas correction.
        // If component does not exist, aero[i] = 0,
        // then equilibrium gas concentration is also null.
        // Perhaps need to take care about near 0 concentrations.
        gas[i] = aero[i] / (tsp * K);
      }

    return true;
  }


  bool ClassModelThermodynamicAEC::module_hydrophobic_global_(const real &temp,
                                                              const real &prim_mass,
                                                              const real &prim_mw,
                                                              const vector1r &vpsat,
                                                              vector1r &aero,
                                                              vector1r &gas) const
  {
    // Mass conservation.
    vector1r mass_cons(Nsemivolatile_);
    for (int i = 0; i < Nsemivolatile_; i++)
      mass_cons[i] = aero[i] + gas[i];

    // Compute total suspended mass.
    real tsp(prim_mass);
    for (int i = 0; i < Nsemivolatile_; i++)
      tsp += aero[i];

    if (tsp == real(0))
      return false;

    // Partition the primary mass.
    real prim_molar = prim_mass / prim_mw;

#ifdef AMC_WITH_DEBUG_AEC
    CBUG << endl << "prim_molar = " << prim_molar << endl;
#endif


    //
    // Solving loop.
    //


    vector1r aero_old;
    for (int h = 0; h < Nloop_b_; h++)
      {
#ifdef AMC_WITH_DEBUG_AEC
        CBUG << endl << "h = " << h << endl;
#endif

        // Molar aerosol concentrations.
        // The primary mass partition has not changed.
        // Update only semivolatile.
        vector1r aero_molar(Nhydrophobic_total_, double(0));

        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          aero_molar[i] = aero[i] / molar_mass_hydrophobic_[i];

        int j(Nhydrophobic_semivolatile_);
        for (int i = 0; i < Nhydrophobic_primary_; i++)
          aero_molar[j++] = prim_molar * primary_fraction_molar_[i];

        // Account for possible hydrophilic concentrations.
        int k(Nhydrophobic_semivolatile_);
        for (int i = 0; i < Nhydrophilic_semivolatile_; i++)
          aero_molar[j++] = aero[k++] / molar_mass_hydrophilic_[i];

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "aero_molar = " << aero_molar << endl;
#endif

        // Total molar concentrations.
        real aero_molar_tot(prim_molar);
        for (int i = 0; i < Nhydrophobic_total_; i++)
          aero_molar_tot += aero_molar[i];

        // Compute activity coefficients.
        vector1r gamma(Nsemivolatile_, real(1));

#ifdef AEC_WITH_UNIFAC
        if (with_unifac_)
          {
            vector<double> aero_molar_fraction(Nhydrophobic_);
            vector<double> gamma2(Nhydrophobic_);
            double temp2 = double(temp);

            // Molar fractions for UNIFAC.
            for (int i = 0; i < Nhydrophobic_; i++)
              aero_molar_fraction[i] = double(aero_molar[i] / aero_molar_tot);

            // Avoid null molar fraction.
            bool recompute(false);
            for (int i = 0; i < Nhydrophobic_; i++)
              if (aero_molar_fraction[i] < double(AEC_MOLAR_FRACTION_TINY))
                {
                  aero_molar_fraction[i] = double(AEC_MOLAR_FRACTION_TINY);
                  recompute = true;
                }

            if (recompute)
              {
                double fraction_sum(double(0));
                for (int i = 0; i < Nhydrophobic_; i++)
                  fraction_sum += aero_molar_fraction[i];
                for (int i = 0; i < Nhydrophobic_; i++)
                  aero_molar_fraction[i] /= fraction_sum;
              }

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "aero_molar_fractionB = " << aero_molar_fraction << endl;
#endif

            unifac_(const_cast<int*>(&Nhydrophobic_), const_cast<int*>(&nfunc_b_),
                    const_cast<int*>(nu_b_.data()), aero_molar_fraction.data(),
                    const_cast<double*>(A_b_.data()), const_cast<double*>(rg_b_.data()),
                    const_cast<double*>(qg_b_.data()), const_cast<double*>(&Z_),
                    &temp2, gamma2.data());

            // If species not in aerosol, set its activity coefficient to 1.
            for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
              if (aero_molar[i] > real(0))
                gamma[i] = real(gamma2[i]);

#ifdef AMC_WITH_DEBUG_AEC
            CBUG << "gammaB = " << gamma << endl;
#endif
          }
#endif

        // Average mol weight.
        real mwa_inv(prim_molar);
        for (int i = 0; i < Nhydrophobic_total_; i++)
          mwa_inv += aero_molar[i];
        mwa_inv /= tsp;

        // Partition coefficient inverse in µg/m3.
        vector1r coef(Nsemivolatile_, tsp);
        for (int i = 0; i <  Nhydrophobic_semivolatile_; i++)
          coef[i] *= (THERMODYNAMIC_IDEAL_GAS_CONSTANT * temp * mwa_inv) / (vpsat[i] * gamma[i]);

        // Keep old concentrations and update aerosol concentrations.
        aero_old = aero;

        // New value.
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          aero[i] = mass_cons[i] * coef[i] / (real(1) + coef[i]);

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "aeroB = " << aero << endl;
#endif

        // Does loop converges ?
        real sum(real(0)), abs_diff(real(0));
        for (int i = 0; i < Nhydrophobic_semivolatile_; i++)
          {
            sum += aero_old[i];
            abs_diff += abs(aero_old[i] - aero[i]);
          }

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "abs_diff = " << abs_diff << ", epsilon_a_ * sum = " << epsilon_a_ * sum << endl;
#endif

        if (abs_diff < epsilon_b_ * sum)
          break;

        // Compute total suspended mass.
        tsp = prim_mass;
        for (int i = 0; i < Nsemivolatile_; i++)
          tsp += aero[i];

#ifdef AMC_WITH_DEBUG_AEC
        CBUG << "tspB = " << tsp << endl;
#endif

        // If no more absorbing mass, exit loop.
        // It should be very rare.
        if (tsp == real(0))
          return false;
      }

    // Give back solution. Gas from mass conservation.
    for (int i = 0; i < Nsemivolatile_; i++)
      gas[i] = mass_cons[i] - aero[i];

    return true;
  }
}

#define AMC_FILE_AEC_CXX
#endif
