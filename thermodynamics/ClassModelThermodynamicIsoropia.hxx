// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_HXX

#define ISOROPIA_NUMBER_SPECIES_EXTERNAL 5
#define ISOROPIA_NUMBER_SPECIES_LIQUID 12
#define ISOROPIA_NUMBER_SPECIES_SOLID   9
#define ISOROPIA_NUMBER_SPECIES_INTERNAL 21
#define ISOROPIA_NUMBER_SPECIES_GAS 3
#define ISOROPIA_METASTABLE_DEFAULT true
#define ISOROPIA_INDEX_LWC 7
#define ISOROPIA_INDEX_HP 0

#ifndef SWIG
extern "C"
{
  void isoropia_(double*, double*, double*, double*, double*,
                 double*, double*, double*, char*, double*);
}
#endif

namespace AMC
{
  class ClassModelThermodynamicIsoropia : virtual public ClassModelThermodynamicBase
  {
  protected:

    /*!< Whether Isorropia is run with metastable option.*/
    int metastable_;

    /*!< Number of AMC internal species involved in thermodynamic model.*/
    int Nspecies_internal_;

    /*!< Index of external species in AMC managed by the thermodynamic model.*/
    vector1i species_index_external_;

    /*!< Index of gas species in AMC managed by the thermodynamic model.*/
    vector1i species_index_gas_;

    /*!< Index of internal species in AMC managed by the thermodynamic model.*/
    vector2i species_index_internal_;

    /*!< Name of external species.*/
    vector1s name_external_;

    /*!< Name of liquid species.*/
    vector1s name_liquid_;

    /*!< Name of solid species.*/
    vector1s name_solid_;

    /*!< Liquid species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_liquid_;

    /*!< Solid species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_solid_;

    /*!< Species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_external_;

    /*!< Gas species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_gas_;

    /*!< Internal species molar mass in µg.mol^{-1}.*/
    vector1r molar_mass_internal_;

  public:

    /*!< Constructors.*/
    ClassModelThermodynamicIsoropia(Ops::Ops &ops, const bool standalone = true);


    /*!< Destructor.*/
    virtual ~ClassModelThermodynamicIsoropia();

    /*!< Get methods.*/
    virtual int GetNspeciesInternal() const;
    bool IsMetaStable() const;

    /*!< Set methods.*/
    void ToggleMetaStable();

    /*!< Clone.*/
    ClassModelThermodynamicIsoropia* clone() const;


    /*!< Compute local equilibrium.*/
    void ComputeLocalEquilibrium(const int &section_min,
                                 const int &section_max,
                                 const vector1i &section_index,
                                 const real *concentration_aer_number,
                                 const real *concentration_aer_mass) const;


    /*!< Compute global equilibrium.*/
    void ComputeGlobalEquilibrium(const int &section_min,
                                  const int &section_max,
                                  const real *concentration_gas,
                                  const real *concentration_aer_mass,
                                  vector1r &concentration_aer_total,
                                  vector1r &equilibrium_aer_internal,
                                  real &liquid_water_content,
                                  vector1r &concentration_aer_equilibrium,
                                  vector1r &concentration_gas_equilibrium) const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    void Test(const int &mode,
              const real &temperature,
              const real &relative_humidity,
              const vector<real> &concentration,
              map<string, real> &output,
              const bool concentration_in_mol = false) const;

    void TestAMC(const real &temperature,
                 const real &relative_humidity,
                 const vector<real> &concentration_aer_mass,
                 vector<real> &equilibrium_aer_internal,
                 vector<real> &equilibrium_gas_surface) const;
#endif
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_HXX
#endif
