// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_CXX

#include "ClassModelThermodynamicIsoropia.hxx"

namespace AMC
{
  // Constructors.
  ClassModelThermodynamicIsoropia::ClassModelThermodynamicIsoropia(Ops::Ops &ops, const bool standalone)
    : ClassModelThermodynamicBase(ops, standalone, "isoropia"), metastable_(ISOROPIA_METASTABLE_DEFAULT),
      Nspecies_internal_(0) 
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Instantiating Isoropia Thermodynamic model." << endl;
#endif

    string prefix_orig = ops.GetPrefix();

    // First read Isoropia specific data, independent of AMC.
    ops.ClearPrefix();
    if (! ops.Exists("isoropia"))
      throw AMC::Error("Isoropia own configuration not found, may be forgot to include \"isoropia.lua\".");

    ops.SetPrefix("isoropia.");

    metastable_ = ops.Get<bool>("metastable", "", metastable_) ? 1 : 0;

    ops.SetPrefix("isoropia.species.");
    ops.Set("external", "", name_external_);
    ops.Set("liquid", "", name_liquid_);
    ops.Set("solid", "", name_solid_);

    ops.SetPrefix("isoropia.molar_mass.");
    ops.Set("external", "", molar_mass_external_);
    ops.Set("liquid", "", molar_mass_liquid_);
    ops.Set("solid", "", molar_mass_solid_);

    // Convert from g.mol^{-1} to µg.mol^{-1}.
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      molar_mass_external_[i] *= real(1.e6);

    molar_mass_internal_.resize(ISOROPIA_NUMBER_SPECIES_INTERNAL);

    int j(0);
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
      {
        molar_mass_liquid_[i] *= real(1.e6);
        molar_mass_internal_[j++] = molar_mass_liquid_[i];
      }

    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
      {
        molar_mass_solid_[i] *= real(1.e6);
        molar_mass_internal_[j++] = molar_mass_solid_[i];
      }

    // We know that gas Isoropia species are the last three external ones.
    molar_mass_gas_.resize(ISOROPIA_NUMBER_SPECIES_GAS);
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
      molar_mass_gas_[i] = molar_mass_external_[i + 2];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Species name : " << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "\tExternal : " << name_external_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "\tLiquid : " << name_liquid_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "\tSolid : " << name_solid_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Molar mass : " << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "\tExternal : " << molar_mass_external_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "\tLiquid : " << molar_mass_liquid_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "\tSolid : " << molar_mass_solid_ << endl;
#endif

    // Revert to local config.
    ops.SetPrefix(prefix_orig);

    if (! this->standalone_)
      {
        // Metastable option of Isoropia.
        metastable_ = ops.Get<bool>("metastable", "", metastable_) ? 1 : 0;

        // species_index_external_[0] is the AMC index of first thermodynamic model species.
        species_index_external_.resize(ISOROPIA_NUMBER_SPECIES_EXTERNAL);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
          {
            species_index_external_[i] = ClassSpecies::GetIndex(name_external_[i]);
            if (species_index_external_[i] < 0)
              throw AMC::Error("Species \"" + name_external_[i] + "\" of Isoropia not found in AMC species list.");
          }

        // species_index_gas_[0] is the AMC gas index of first thermodynamic model gas species.
        species_index_gas_.resize(ISOROPIA_NUMBER_SPECIES_EXTERNAL - 2);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL - 2; i++)
          {
            species_index_gas_[i] = ClassSpecies::GetIndexGas(name_external_[i + 2]);
            if (species_index_gas_[i] < 0)
              throw AMC::Error("Gas species \"" + name_external_[i] + "\" of Isoropia not found in AMC gas species list.");
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "Species index with AMC (void if standalone use) : " << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "\tExternal : " << species_index_external_ << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "\tGas : " << species_index_gas_ << endl;
#endif

        // species_index_internal_[0][0] is the index of first AMC internal species
        // managed the thermodynamic model and  species_index_internal_[0][1] is
        // where to find it in the thermodynamic concentration vector.
        // As an example, for H+, if H+ is the first AMC internal species,
        // then species_index_internal_[0][0] = 0 and species_index_internal_[0][1] = 0
        // because H+ is the 0th species (starting from 0) in liquid concentration vector.

        if (ops.Exists("species_internal.isrpia"))
          {
            vector1s species_list;
            ops.Set("species_internal.isrpia", "", species_list);
            Nspecies_internal_ = int(species_list.size());

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Nspecies_internal = " << Nspecies_internal_ << endl;
#endif

            species_index_internal_.assign(Nspecies_internal_, vector1i(2, -1));

            // Check that hydronium ions are first in array if included.
            for (int i = 0; i < Nspecies_internal_; i++)
              if (species_list[i] == "Hp" && i > 0)
                throw AMC::Error("Hydronium ion species (\"Hp\") has to be placed at beginning of vector when included.");

            for (int i = 0; i < Nspecies_internal_; i++)
              {
                for (int j = 0; j < ClassSpeciesEquilibrium::Nspecies_; j++)
                  if (species_list[i] == ClassSpeciesEquilibrium::name_[j])
                    {
                      species_index_internal_[i][0] = j;
                      break;
                    }

                if (species_index_internal_[i][0] < 0)
                  throw AMC::Error("Unable to find species \"" + species_list[i] + "\" in AMC equilibrium species list.");

                for (int j = 0; j < ISOROPIA_NUMBER_SPECIES_LIQUID; j++)
                  if (species_list[i] == name_liquid_[j])
                    {
                      species_index_internal_[i][1] = j;
                      break;
                    }

                for (int j = 0; j < ISOROPIA_NUMBER_SPECIES_SOLID; j++)
                  if (species_list[i] == name_solid_[j])
                    {
                      species_index_internal_[i][1] = ISOROPIA_NUMBER_SPECIES_LIQUID + j;
                      break;
                    }

                if (species_index_internal_[i][1] < 0)
                  throw AMC::Error("Unable to find species \"" + species_list[i] + "\" in Isoropia species list.");

#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Species \""
                                     << ClassSpeciesEquilibrium::name_[species_index_internal_[i][0]]
                                     << "\" found at index " << species_index_internal_[i][0] << " in AMC"
                                     << " and index " << species_index_internal_[i][1] << " in Isoropia." << endl;
#endif
              }
          }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "metastable = " << metastable_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassModelThermodynamicIsoropia::~ClassModelThermodynamicIsoropia()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Debug(3) << Reset() << "Called destructor for Isoropia." << endl;
#endif

    return;
  }


  // Get methods.
  int ClassModelThermodynamicIsoropia::GetNspeciesInternal() const
  {
    return Nspecies_internal_;
  }


  bool ClassModelThermodynamicIsoropia::IsMetaStable() const
  {
    return metastable_ == 1;
  }


  // Set methods.
  void ClassModelThermodynamicIsoropia::ToggleMetaStable()
  {
    metastable_ = 1 - metastable_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Switch metastable option, now it is "
                         << (metastable_ == 1 ? "true" : "false") << endl;
#endif
  }


  // Clone.
  ClassModelThermodynamicIsoropia* ClassModelThermodynamicIsoropia::clone() const
  {
    return new ClassModelThermodynamicIsoropia(*this);
  }


  // Compute local equilibrium.
  void ClassModelThermodynamicIsoropia::ComputeLocalEquilibrium(const int &section_min,
                                                                const int &section_max,
                                                                const vector1i &section_index,
                                                                const real *concentration_aer_number,
                                                                const real *concentration_aer_mass) const
  {
    // Meteorological variables.
    double temperature = double(ClassMeteorologicalData::temperature_);
    double relative_humidity = double(ClassMeteorologicalData::relative_humidity_);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    /*!< Internal species molar concentration mol.m^{-3}.*/
    vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

    // In this routine, we are in reverse mode.
    cntrl[0] = double(1);

    // Metastable option.
    cntrl[1] = double(metastable_);

    int Nsection = int(section_index.size());

    for (int h = 0; h < Nsection; h++)
      {
        const int i = section_index[h];
        if (i < section_min || i >= section_max)
          continue;

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          {
            // Aerosol is assumed to be at thermic equilibrium with air.
            //ClassAerosolData::temperature_[i] = ClassMeteorologicalData::temperature_;
            //temperature = double(ClassAerosolData::temperature_[i]);

            // Start index for mass concentration vector.
            int j = i * ClassSpecies::Nspecies_;

            // Input in mol.m^{-3}.
            double wi_total(double(0));
            for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_EXTERNAL; k++)
              {
#ifdef AMC_WITH_LAYER
                const int l = j + species_index_external_[k];
                wi[k] = double(concentration_aer_mass[l] / molar_mass_external_[k] *
                               ClassAerosolData::layer_repartition_[l].back());
#else
                wi[k] = double(concentration_aer_mass[j + species_index_external_[k]] / molar_mass_external_[k]);
#endif
                wi_total += wi[k];
              }

            // If no inorganic matter, set equilibrium gas concentration
            // to the current bulk gas concentration to prevent condensation,
            // as in theory, absorption cannot take place if no absorbing inorganic material.
            // This should be very rare...
            if (wi_total > double(0))
              {
                isoropia_(wi.data(), &relative_humidity, &temperature, cntrl.data(), w.data(),
                          gas.data(), aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

                // Put Isoropia output into one vector for easier use.
                j = 0;
                for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_LIQUID; k++)
                  concentration_molar_internal[j++] = real(aerliq[k]);
                for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_SOLID; k++)
                  concentration_molar_internal[j++] = real(aersld[k]);
 
                // Outputs required concentrations, turned into µg.m^{-3}.
                ClassAerosolData::liquid_water_content_[i] = concentration_molar_internal[ISOROPIA_INDEX_LWC]
                  * molar_mass_internal_[ISOROPIA_INDEX_LWC];

                j = i * ClassSpeciesEquilibrium::Nspecies_;
                for (int k = 0; k < Nspecies_internal_; k++)
                  {
                    // Isoropia index.
                    const int l = species_index_internal_[k][1];

                    // Thermodynamic model internal species, e.g. hydronium ions (H+).
                    ClassAerosolData::equilibrium_aer_internal_[j + species_index_internal_[k][0]]
                      = concentration_molar_internal[l] * molar_mass_internal_[l];
                  }

                // If no liquid water content, gas equilibrium concentrations
                // from Isoropia are believed to be meaningless.
                if (concentration_molar_internal[ISOROPIA_INDEX_LWC] > real(0))
                  {
                    j = i * ClassSpecies::Ngas_;
                    for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_GAS; k++)
                      ClassAerosolData::equilibrium_gas_surface_[j + species_index_gas_[k]]
                        = real(gas[k]) * molar_mass_gas_[k];

                    // Say that all inorganic species are in aqueous phase.
                    j = i * ClassSpecies::Nspecies_;
                    for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_EXTERNAL; k++)
                      ClassAerosolData::phase_[j + species_index_external_[k]] = ClassPhase::aqueous_phase_index_;
                  }
                else
                  {
                    // If no liquid water content, we just have to say that all species are in a solid phase.
                    j = i * ClassSpecies::Nspecies_;
                    for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_EXTERNAL; k++)
                      ClassAerosolData::phase_[j + species_index_external_[k]] = -1;
                  }
              }
          }
      }
  }


  // Compute global equilibrium.
  void ClassModelThermodynamicIsoropia::ComputeGlobalEquilibrium(const int &section_min,
                                                                 const int &section_max,
                                                                 const real *concentration_gas,
                                                                 const real *concentration_aer_mass,
                                                                 vector1r &concentration_aer_total,
                                                                 vector1r &equilibrium_aer_internal,
                                                                 real &liquid_water_content,
                                                                 vector1r &concentration_aer_equilibrium,
                                                                 vector1r &concentration_gas_equilibrium) const
  {
    // Meteorological variables.
    double temperature = double(ClassMeteorologicalData::temperature_);
    double relative_humidity = double(ClassMeteorologicalData::relative_humidity_);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    // In this routine, we are in forward mode.
    cntrl[0] = double(0);

    // Metastable option.
    cntrl[1] = double(metastable_);

    // Compute aerosol concentration for each Isoropia species.
    for (int i = section_min; i < section_max; i++)
      {
        int j = i * ClassSpecies::Nspecies_;
        for (int k = 0; k < ISOROPIA_NUMBER_SPECIES_EXTERNAL; k++)
          {
            int l = species_index_external_[k];
            concentration_aer_total[l] += concentration_aer_mass[j + l];
          }
      }

    // Feed Isoropia inputs.
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      wi[i] = double(concentration_aer_total[species_index_external_[i]]);

    int j(2);
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
      wi[j++] += double(concentration_gas[species_index_gas_[i]]);

    // Input in mol.m^{-3}.
    double wi_total(double(0));
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      {
        wi[i] /= double(molar_mass_external_[i]);
        wi_total += wi[i];
      }

    if (wi_total > double(0))
      {
        isoropia_(wi.data(), &relative_humidity, &temperature, cntrl.data(), w.data(),
                  gas.data(), aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

        // Remove gas from total concentration for semivolatile species.
        j = 2;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          wi[j++] -= gas[i];

        // Put Isoropia output into one vector for easier use.
        vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

        j = 0;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
          concentration_molar_internal[j++] = real(aerliq[i]);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
          concentration_molar_internal[j++] = real(aersld[i]);

        // Give back bulk equilibrium concentrations into µg.m^{-3}.
        for (int i = 2; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
          concentration_aer_equilibrium[species_index_external_[i]] = real(wi[i]) * molar_mass_external_[i];

        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          concentration_gas_equilibrium[species_index_gas_[i]] = real(gas[i]) * molar_mass_gas_[i];

        // Outputs required concentrations, turned into µg.m^{-3}.
        liquid_water_content = concentration_molar_internal[ISOROPIA_INDEX_LWC]
          * molar_mass_internal_[ISOROPIA_INDEX_LWC];

        for (int i = 0; i < Nspecies_internal_; i++)
          {
            // AMC and Isoropia index.
            const int &j = species_index_internal_[i][0];
            const int &k = species_index_internal_[i][1];

            equilibrium_aer_internal[j] = concentration_molar_internal[k] * molar_mass_internal_[k];
          }
      }
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassModelThermodynamicIsoropia::Test(const int &mode,
                                             const real &temperature,
                                             const real &relative_humidity,
                                             const vector<real> &concentration,
                                             map<string, real> &output,
                                             const bool concentration_in_mol) const
  {
    // Meteorological variables.
    double temp = double(temperature);
    double rh = double(relative_humidity);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    // In this routine, we chose the mode.
    cntrl[0] = double(mode);

    // Metastable option.
    cntrl[1] = double(metastable_);

    // Input in mol.m^{-3}.
    if (concentration_in_mol)
      for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
        wi[i] = double(concentration[i] * 1.e-6);
    else
      for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
        wi[i] = double(concentration[i] / molar_mass_external_[i]);

    double wi_total(double(0));
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      wi_total += wi[i];

    if (wi_total > double(0))
      {
        isoropia_(wi.data(), &rh, &temp, cntrl.data(), w.data(), gas.data(), aerliq.data(),
                  aersld.data(), const_cast<char*>(scase.data()), other.data());

        // If forward mode, remove gas from total concentrations.
        if (mode == 0)
          for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
            w[i + 2] -= gas[i];

        // Outputs in µg.m^{-3} or µmol.m^{-3}.
        if (concentration_in_mol)
          {
            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
              output[name_external_[i + 2] + "g"] = real(gas[i]) * real(1e6);

            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
              output[name_external_[i]] = real(w[i]) * real(1e6);      

            // Solids and liquids.
            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
              output[name_liquid_[i]] = real(aerliq[i]) * real(1e6);

            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
              output[name_solid_[i]] = real(aersld[i]) * real(1e6);
          }
        else
          {
            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
              output[name_external_[i + 2] + "g"] = real(gas[i]) * molar_mass_external_[i + 2];

            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
              output[name_external_[i]] = real(w[i]) * molar_mass_external_[i];      

            // Solids and liquids.
            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
              output[name_liquid_[i]] = real(aerliq[i]) * molar_mass_liquid_[i];

            for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
              output[name_solid_[i]] = real(aersld[i]) * molar_mass_solid_[i];
          }
      }
  }


  void ClassModelThermodynamicIsoropia::TestAMC(const real &temperature,
                                                const real &relative_humidity,
                                                const vector<real> &concentration_aer_mass,
                                                vector<real> &equilibrium_aer_internal,
                                                vector<real> &equilibrium_gas_surface) const
  {
    if (this->standalone_)
      throw AMC::Error("Thermodyanmic model \"" + this->name_ + "\" runs in standalone mode.");

    real &liquid_water_content = equilibrium_aer_internal.back();

    // Meteorological variables.
    double temp = double(temperature);
    double rh = double(relative_humidity);

    // Isoropia inputs and ouputs.
    string scase(15, ' ');
    vector<double> wi(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      cntrl(2, double(0)), w(ISOROPIA_NUMBER_SPECIES_EXTERNAL, double(0)),
      gas(ISOROPIA_NUMBER_SPECIES_GAS, double(0)),
      aerliq(ISOROPIA_NUMBER_SPECIES_LIQUID, double(0)),
      aersld(ISOROPIA_NUMBER_SPECIES_SOLID, double(0)),
      other(6, double(0));

    /*!< Internal species molar concentration mol.m^{-3}.*/
    vector1r concentration_molar_internal(ISOROPIA_NUMBER_SPECIES_INTERNAL, real(0));

    // In this routine, we are in reverse mode.
    cntrl[0] = double(1);

    // Metastable option.
    cntrl[1] = double(metastable_);

    // Input in mol.m^{-3}.
    double wi_total(double(0));
    for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_EXTERNAL; i++)
      {
        wi[i] = double(concentration_aer_mass[species_index_external_[i]] / molar_mass_external_[i]);
        wi_total += wi[i];
      }

    // If nothing in inorganic phase, set equilibrium gas concentration
    // to the current bulk gas concentration to prevent condensation,
    // as in theory, absorption cannot take place if no absorbing inorganic material.
    // This should be very rare...
    if (wi_total > double(0))
      {
        isoropia_(wi.data(), &rh, &temp, cntrl.data(), w.data(), gas.data(),
                  aerliq.data(), aersld.data(), const_cast<char*>(scase.data()), other.data());

        // Put Isoropia output into one vector for easier use.
        int h = 0;
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_LIQUID; i++)
          concentration_molar_internal[h++] = real(aerliq[i]);
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_SOLID; i++)
          concentration_molar_internal[h++] = real(aersld[i]);
 
        // Outputs required concentrations, turned into µg.m^{-3}.
        liquid_water_content = concentration_molar_internal[ISOROPIA_INDEX_LWC]
          * molar_mass_internal_[ISOROPIA_INDEX_LWC];

        for (int i = 0; i < Nspecies_internal_; i++)
          {
            // AMC index.
            const int &j = species_index_internal_[i][0];

            // Isoropia index.
            const int &k = species_index_internal_[i][1];

            // thermodynamic models, e.g. hydronium ions (H+).
            equilibrium_aer_internal[j] = concentration_molar_internal[k] * molar_mass_internal_[k];
          }

        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          equilibrium_gas_surface[species_index_gas_[i]] = real(gas[i]) * molar_mass_gas_[i];
      }
    else
      {
        for (int i = 0; i < Nspecies_internal_; i++)
          equilibrium_aer_internal[species_index_internal_[i][0]] = real(0);

        // If no inorganic matter, set equilibrium gas concentrations to negative.
        for (int i = 0; i < ISOROPIA_NUMBER_SPECIES_GAS; i++)
          equilibrium_gas_surface[species_index_gas_[i]] = real(-1);
      }
  }
#endif
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_ISOROPIA_CXX
#endif
