// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODEL_THERMODYNAMIC_BASE_HXX

#define THERMODYNAMIC_IDEAL_GAS_CONSTANT 8.314462175

namespace AMC
{
  class ClassModelThermodynamicBase
  {

  public:

    typedef AMC_THERMODYNAMIC_REAL real;
    typedef vector<int> vector1i;
    typedef vector<vector1i> vector2i;
    typedef vector<real> vector1r;
    typedef vector<string> vector1s;

  protected:

    /*!< Thread mutex if threads used.*/
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
    static mutex mutex_thermodynamic_;
#endif

    /*!< Name of model.*/
    string name_;

    /*!< Configuration prefix.*/
    string prefix_;

    /*!< Whether the model is used in standalone mode or not.*/
    bool standalone_;

  public:

    /*!< Constructors.*/
    ClassModelThermodynamicBase(const bool standalone = true, const string name = "");
    ClassModelThermodynamicBase(Ops::Ops &ops, const bool standalone = true, const string name = "");

    /*!< Destructor.*/
    virtual ~ClassModelThermodynamicBase();

    /*!< Get methods.*/
    string GetName() const;
    string GetPrefix() const;

    /*!< Is model in standalone mode ?.*/
    bool IsStandAlone() const;

    /*!< Clone.*/
    virtual ClassModelThermodynamicBase* clone() const = 0;


    /*!< Compute local equilibrium.*/
    virtual void ComputeLocalEquilibrium(const int &section_min,
                                         const int &section_max,
                                         const vector1i &section_index,
                                         const real *concentration_aer_number,
                                         const real *concentration_aer_mass) const = 0;

    /*!< Compute global equilibrium.*/
    virtual void ComputeGlobalEquilibrium(const int &section_min,
                                          const int &section_max,
                                          const real *concentration_gas,
                                          const real *concentration_aer_mass,
                                          vector1r &concentration_aer_total,
                                          vector1r &equilibrium_aer_internal,
                                          real &liquid_water_content,
                                          vector1r &concentration_aer_equilibrium,
                                          vector1r &concentration_gas_equilibrium) const = 0;
  };
}

#define AMC_FILE_CLASS_MODEL_THERMODYNAMIC_BASE_HXX
#endif
