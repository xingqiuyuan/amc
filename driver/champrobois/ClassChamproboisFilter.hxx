// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_FILTER_HXX

namespace Driver
{
  class ClassChamproboisSource;

  /*! 
   * \class ClassChamproboisFilter
   */
  class ClassChamproboisFilter : public ClassChamproboisSink, public ClassChamproboisSpeciation
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  private:

    /*!< Number of gas species.*/
    int Ngas_;

    /*!< Number of purely volatile species.*/
    int Nvolatile_;

    /*!< Total number concentration.*/
    real concentration_number_;

    /*!< Gas index.*/
    vector1i gas_index_;

    /*!< Fraction section.*/
    vector1r fraction_section_;

    /*!< Concentration of purely volatile species (which are not part of speciation).*/
    vector1r concentration_volatile_;

  public:

    /*!< Constructor.*/
    ClassChamproboisFilter(const string name, const string species_name_total);

    /*!< Destructor.*/
    ~ClassChamproboisFilter();

    /*!< Get methods.*/
    virtual string GetName() const = 0;
    int GetNgas() const;
    int GetNvolatile() const;
    real GetConcentrationNumber() const;
    void GetConcentrationVolatile(vector<real> &concentration_volatile) const;
#ifndef SWIG
    void Init(const ClassChamproboisSource *source_ptr);
#endif

#ifndef SWIG
    /*!< Fill filter.*/
    void Fill(const real &t, const real z,
              const Array<real, 2> &concentration,
              const Array<real, 1> &concentration_total,
              const Array<real, 1> &temperature);
#endif

    /*!< Dump.*/
    void Dump(map<string, int> &argument) const;

#ifndef SWIG  
    /*!< End.*/
    void End();
#endif
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_FILTER_HXX
#endif
