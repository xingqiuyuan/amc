// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_ELPI_CXX

#include "ClassChamproboisELPI.hxx"


namespace Driver
{
  // Fill.
  inline void ClassChamproboisELPI::fill_(const real ratio, const real &z, const Array<real, 2> &concentration)
  {
    for (int i = 0; i < this->Nr_impaction_; ++i)
      {
        const int j = this->index_impaction_[i];
        const real volume_impaction = z * this->surface_impaction_[i] * ratio;

        for (int k = 0; k < this->Nsection_; ++k)
          {
            concentration_number_(Nsample_, k) += concentration(j, k) * volume_impaction;

            // Same species order between driver and sink.
            const int l = this->Nsection_ + k * this->Nspecies_;
            for (int m = 0; m < this->Nspecies_; ++m)
              concentration_mass_(Nsample_, k) += concentration(j, l + m) * volume_impaction;
          }

        ClassChamproboisELPI::air_volume_sampled_ += volume_impaction;
      }
  }


  // Constructor.
  ClassChamproboisELPI::ClassChamproboisELPI(const string name)
    : ClassChamproboisSink("ELPI"), Nspecies_(0),
      Nsample_max_(CHAMPROBOIS_ELPI_SAMPLE_NUMBER_MAX),
      sample_time_(CHAMPROBOIS_ELPI_SAMPLE_RATE_DEFAULT),
      sample_rate_(CHAMPROBOIS_ELPI_SAMPLE_RATE_DEFAULT),
      t_(real(0)), Nsample_(0), air_volume_sampled_(real(0))
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset()
                         << "Instantiate Champrobois ELPI \"" << name_ << "\"." << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset() << "sample_rate = " << sample_rate_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << LogReset() << "Nsample_max = " << Nsample_max_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassChamproboisELPI::~ClassChamproboisELPI()
  {
    return;
  }


  // Get methods.
  string ClassChamproboisELPI::GetName() const
  {
    return name_;
  }


  int ClassChamproboisELPI::GetNspecies() const
  {
    return Nspecies_;
  }


  int ClassChamproboisELPI::GetNsample() const
  {
    return Nsample_;
  }


  int ClassChamproboisELPI::GetNsampleMax() const
  {
    return Nsample_max_;
  }


  real ClassChamproboisELPI::GetSampleRate() const
  {
    return sample_rate_;
  }


  void ClassChamproboisELPI::GetConcentrationNumber(vector<vector<real> > &concentration_number) const
  {
    concentration_number.assign(Nsample_, vector1r(this->Nsection_, real(0)));
    for (int i = 0; i < Nsample_; ++i)
      for (int j = 0; j < this->Nsection_; ++j)
        concentration_number[i][j] = concentration_number_(i, j);
  }


  void ClassChamproboisELPI::GetConcentrationMass(vector<vector<real> > &concentration_mass) const
  {
    concentration_mass.assign(Nsample_, vector1r(this->Nsection_, real(0)));
    for (int i = 0; i < Nsample_; ++i)
      for (int j = 0; j < this->Nsection_; ++j)
        concentration_mass[i][j] = concentration_mass_(i, j);
  }


  // Set methods.
  void ClassChamproboisELPI::SetNsampleMax(const int Nsample_max)
  {
    Nsample_max_ = Nsample_max;
  }


  void ClassChamproboisELPI::SetSampleRate(const real sample_rate)
  {
    sample_rate_ = sample_rate;
    sample_time_ = sample_rate;
  }


  void ClassChamproboisELPI::Init(const int &Nspecies)
  {
    Nspecies_ = Nspecies;

    concentration_number_.resize(Nsample_max_, this->Nsection_);
    concentration_mass_.resize(Nsample_max_, this->Nsection_);

    concentration_number_ = real(0);
    concentration_mass_ = real(0);
  }


  // Fill sink.
  void ClassChamproboisELPI::Fill(const real &t, const real z,
                                  const Array<real, 2> &concentration,
                                  const Array<real, 1> &concentration_total,
                                  const Array<real, 1> &temperature)
  {
    ClassChamproboisSink::Fill(t, z, concentration, concentration_total, temperature);

    const real ratio = t >= sample_time_ ? (sample_time_ - t_) / (t - t_) : real(1);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_ELPI
    CBUG << "ratio = " << ratio << endl;
#endif

    fill_(ratio, z, concentration);

    // If beyond sample time.
    if (ratio < real(1))
      {
#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_ELPI
        CBUG << "Nsample = " << Nsample_ << endl;
        CBUG << "air_volume_sampled = " << ClassChamproboisELPI::air_volume_sampled_ << endl;
#endif
        if (ClassChamproboisELPI::air_volume_sampled_ <= real(0))
          throw AMC::Error("Sampled air volume in ELPI is <= 0.");

        const real air_volume_sampled_inv = real(1) / ClassChamproboisELPI::air_volume_sampled_;
        concentration_number_(Nsample_, Range::all()) *= air_volume_sampled_inv;
        concentration_mass_(Nsample_, Range::all()) *= air_volume_sampled_inv;

        // Reset ELPI sampled air volume.
        ClassChamproboisELPI::air_volume_sampled_ = real(0);

        // Increment number of sampling steps.
        Nsample_++;

        // If reach max number of samples.
        if (Nsample_ == Nsample_max_)
          throw AMC::Error("Current number of samples (" + to_str(Nsample_) +
                           ") exceeds max (" + to_str(Nsample_max_) + ").");
  
        // Save remaining.
        fill_(real(1) - ratio, z, concentration);

        // Increment sampling time.
        sample_time_ += sample_rate_;
      }

    t_ = t;
  }


  // Dump.
  void ClassChamproboisELPI::Dump(map<string, int> &argument) const
  {
    const int width = (argument.find("width") != argument.end()) ? argument["width"] : 15;
    const int precision = (argument.find("precision") != argument.end()) ? argument["precision"] : 6;

    ClassChamproboisSink::Dump();
    cout << string(20, '=') << " ELPI \"" << name_ << "\" " << string(20, '=') << endl
         << "Sample rate = " << sample_rate_ << endl
         << "Number of samples = " << Nsample_ << endl
         << "Maximum number of samples = " << Nsample_max_ << endl;

    // 1.e-6 ? : more convenient to present concentrations in #/cm3.
    for (int i = 0; i < Nsample_; ++i)
      {
        cout << setw(4) << left << i;
        for (int j = 0; j < this->Nsection_; ++j)
          cout << setw(width) << setprecision(precision) << left << concentration_number_(i, j) * real(1.e-6);
        cout << endl;
      }
  }


  // Compute average concentrations.
  void ClassChamproboisELPI::End()
  {
    ClassChamproboisSink::End();

    concentration_number_.resizeAndPreserve(Nsample_, this->Nsection_);
    concentration_mass_.resizeAndPreserve(Nsample_, this->Nsection_);
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_ELPI_CXX
#endif
