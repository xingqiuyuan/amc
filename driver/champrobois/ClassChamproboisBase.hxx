// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_BASE_HXX

#define CHAMPROBOIS_BASE_CHIMNEY_LENGTH_DEFAULT 15
#define CHAMPROBOIS_BASE_CHIMNEY_RADIUS_DEFAULT 1.6
#define CHAMPROBOIS_BASE_FIRE_PLACE_RADIUS_DEFAULT 0.1

namespace Driver
{
  /*! 
   * \class ClassChamproboisBase
   */
  class ClassChamproboisBase : public ClassDriverBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Raw data file.*/
    static string raw_data_file_;

    /*!< Length of Chimney.*/
    static real chimney_length_;

    /*!< Radius of Chimney.*/
    static real chimney_radius_;

    /*!< Radius of fire place.*/
    static real fire_place_radius_;

    /*!< List of source name.*/
    static vector1s source_list_;

    /*!< List of speciation name.*/
    static vector1s speciation_list_;

  public:

    /*!< Constructor.*/
    ClassChamproboisBase();

    /*!< Destructor.*/
    virtual ~ClassChamproboisBase();

    /*!< Init.*/
    static void Init(const string &configuration_file, const string prefix = "champrobois");

    /*!< Clear static data.*/
    static void Clear();

    /*!< Get methods.*/
    static string GetRawDataFile();
    static real GetChimneyLength();
    static real GetChimneyRadius();
    static real GetFirePlaceRadius();
    static vector<string> GetSourceList();
    static vector<string> GetSpeciationList();
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_BASE_HXX
#endif
