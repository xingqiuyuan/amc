// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_BASE_CXX

#include "ClassChamproboisBase.hxx"

namespace Driver
{
  // Constructor.
  ClassChamproboisBase::ClassChamproboisBase()
  {
    return;
  }


  // Destructor.
  ClassChamproboisBase::~ClassChamproboisBase()
  {
    return;
  }


  void ClassChamproboisBase::Init(const string &configuration_file, const string prefix)
  {
    const string type = "champrobois";
    ClassDriverBase::Init(configuration_file, prefix, type);

    Ops::Ops ops(ClassDriverBase::configuration_file_);
    ops.SetPrefix(ClassDriverBase::prefix_ + ".");

    chimney_length_  = ops.Get<real>("chimney.length", "", chimney_length_);
    chimney_radius_  = ops.Get<real>("chimney.radius", "", chimney_radius_);
    fire_place_radius_  = ops.Get<real>("fire_place.radius", "", fire_place_radius_);
    raw_data_file_ = ops.Get<string>("raw_data_file", "", raw_data_file_);
    source_list_ = ops.GetEntryList("source");
    ops.Set("speciation.list", "", speciation_list_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << LogReset() << "Length of chimney is "
                         << chimney_length_ << " meters." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << LogReset() << "Radius of chimney is "
                         << chimney_radius_ << " meters." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << LogReset() << "Radius of fire place is "
                         << fire_place_radius_ << " meters." << endl;
    *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset() << "Raw data file is \"" << raw_data_file_ << "\"." << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "List of available sources : " << source_list_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "List of available speciations : " << speciation_list_ << endl;
#endif

    ops.Close();
  }


  // Clear static data.
  void ClassChamproboisBase::Clear()
  {
    raw_data_file_ = "./raw_data_champrobois.bin";
    chimney_length_ = CHAMPROBOIS_BASE_CHIMNEY_LENGTH_DEFAULT;
    chimney_radius_ = CHAMPROBOIS_BASE_CHIMNEY_RADIUS_DEFAULT;
    fire_place_radius_ = CHAMPROBOIS_BASE_FIRE_PLACE_RADIUS_DEFAULT;
    source_list_.clear();
    speciation_list_.clear();

    ClassDriverBase::Clear();
  }


  // Get methods.
  string ClassChamproboisBase::GetRawDataFile()
  {
    return raw_data_file_;
  }


  real ClassChamproboisBase::GetChimneyLength()
  {
    return chimney_length_;
  }

  real ClassChamproboisBase::GetChimneyRadius()
  {
    return chimney_radius_;
  }

  real ClassChamproboisBase::GetFirePlaceRadius()
  {
    return fire_place_radius_;
  }

  vector<string> ClassChamproboisBase::GetSourceList()
  {
    return source_list_;
  }

  vector<string> ClassChamproboisBase::GetSpeciationList()
  {
    return speciation_list_;
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_BASE_CXX
#endif
