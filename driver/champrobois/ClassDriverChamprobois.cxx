// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_CHAMPROBOIS_CXX

#include "ClassDriverChamprobois.hxx"

namespace Driver
{
  // Constructor.
  ClassDriverChamprobois::ClassDriverChamprobois(const string name, ClassChamproboisSource *source_ptr)
    : source_ptr_(source_ptr), with_diffusion_(true), with_loss_(true), Nc_(0), NgasVolatile_(0),
      Nr_(0), Nz_(0), key_(name), dt_(real(0)), dr_(real(0)), dz_(real(0)), name_(name),
      path_(name + ".bin"), pressure_(AMC_PRESSURE_ATM), Nsink_(0), t_(real(0)),
      heat_specific_isobaric_average_(DRIVER_CHAMPROBOIS_HEAT_SPECIFIC_ISOBARIC_AVERAGE),
      thermal_conductance_average_(DRIVER_CHAMPROBOIS_THERMAL_CONDUCTANCE_AVERAGE),
      diffusivity_turbulent_gas_(real(0)), diffusivity_turbulent_particle_(real(0)),
    scale_factor_diffusion_turbulent_gas_(real(0)), scale_factor_diffusion_turbulent_particle_(real(0)),
    concentration_water_background_(real(0)), temperature_background_(DRIVER_TEMPERATURE_REFERENCE),
    display_frequency_(real(0)), save_frequency_(real(0)), dt_max_(numeric_limits<real>::max()),
    dt_ratio_(DRIVER_CHAMPROBOIS_DT_RATIO_DEFAULT), with_diffusion_molecular_(true), dt_fixed_(numeric_limits<real>::max())
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    , host_name_(DRIVER_CHAMPROBOIS_HOST_NAME), port_number_(DRIVER_CHAMPROBOIS_PORT_NUMBER), use_socket_(true)
#endif
  {
    if (ClassDriverChamprobois::prefix_.empty())
      throw AMC::Error("No prefix given for Champrobois drivers, set one with SetPrefix(\"your_prefered_prefix\").");

    if (source_ptr_ == NULL)
      throw AMC::Error("Champrobois driver needs one source to begin with.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset() << "Init Champrobois simulation \""
                         << name_ << "\" with source \"" << source_ptr_->GetName() << "\"." << endl;
#endif

    //
    // Read configuration.
    //

    Ops::Ops ops(ClassChamproboisBase::configuration_file_);

    //
    // First, read default settings.
    //
    ops.SetPrefix(ClassDriverChamprobois::prefix_ + ".general.");

    Nr_ = ops.Get<int>("Nr", "", Nr_);
    Nz_ = ops.Get<int>("Nz", "", Nz_);
    dt_ratio_ = ops.Get<real>("dt.ratio", "", dt_ratio_);
    dt_fixed_ = ops.Get<real>("dt.fixed", "", dt_fixed_);

    // Loss.
    with_loss_ = ops.Get<bool>("loss.use", "", with_loss_);

    // Whether diffusion or not.
    with_diffusion_ = ops.Get<bool>("diffusion.use", "", with_diffusion_);
    with_diffusion_molecular_ = ops.Get<bool>("diffusion.molecular", "", with_diffusion_molecular_);

    // Scale factor for turbulent diffusion.
    scale_factor_diffusion_turbulent_gas_ = ops.Get<real>("diffusion.turbulent.scale_factor.gas", "",
                                                            scale_factor_diffusion_turbulent_gas_);
    scale_factor_diffusion_turbulent_particle_ = ops.Get<real>("diffusion.turbulent.scale_factor.particle", "",
                                                                 scale_factor_diffusion_turbulent_particle_);

    if (scale_factor_diffusion_turbulent_gas_ <= real(0))
      diffusivity_turbulent_gas_ = ops.Get<real>("diffusion.turbulent.gas", "", diffusivity_turbulent_gas_);

    if (scale_factor_diffusion_turbulent_particle_ <= real(0))
      diffusivity_turbulent_particle_ = ops.Get<real>("diffusion.turbulent.particle", "", diffusivity_turbulent_particle_);

    // Thermal variables.
    heat_specific_isobaric_average_ =
      ops.Get<real>("heat_specific_isobaric_average.Joule_g_K", "", heat_specific_isobaric_average_)
      * DRIVER_CONVERT_FROM_G_TO_MICROG;

    thermal_conductance_average_ =
      ops.Get<real>("thermal_conductance_average.Joule_s_K", "", thermal_conductance_average_);

    // Display and save frequency.
    display_frequency_ = ops.Get<real>("display_frequency", "", display_frequency_);
    save_frequency_ = ops.Get<real>("save.frequency", "", save_frequency_);
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    use_socket_ = ops.Get<bool>("save.socket", "", use_socket_);
    if (use_socket_)
      port_number_ = ops.Get<int>("save.port_number", "", port_number_);
#endif

    //
    // Then, read some particular settings.
    //

    ops.SetPrefix(ClassDriverChamprobois::prefix_ + "." + name_ + ".");

    key_ = ops.Get<string>("key", "",key_);
    path_ = ops.Get<string>("path", "", path_);

    Nr_ = ops.Get<int>("Nr", "", Nr_);
    Nz_ = ops.Get<int>("Nz", "", Nz_);
    dt_ratio_ = ops.Get<real>("dt.ratio", "", dt_ratio_);
    dt_fixed_ = ops.Get<real>("dt.fixed", "", dt_fixed_);

    pressure_ = ops.Get<real>("pressure_pascal", "", pressure_);

    // Loss.
    with_loss_ = ops.Get<bool>("loss.use", "", with_loss_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << LogReset() << "Simulation with loss ? " << (with_loss_ ? "YES" : "NO") << endl;
#endif

    // Whether diffusion or not.
    with_diffusion_ = ops.Get<bool>("diffusion.use", "", with_diffusion_);
    with_diffusion_molecular_ = ops.Get<bool>("diffusion.molecular", "", with_diffusion_molecular_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << LogReset() << "Simulation with diffusion ? "
                         << (with_diffusion_ ? "YES" : "NO") << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info() << LogReset() << "Simulation with molecular diffusion ? "
                         << (with_diffusion_molecular_ ? "YES" : "NO") << endl;
#endif

    // Scale factor for turbulent diffusion.
    scale_factor_diffusion_turbulent_gas_ = ops.Get<real>("diffusion.turbulent.scale_factor.gas", "",
                                                            scale_factor_diffusion_turbulent_gas_);
    scale_factor_diffusion_turbulent_particle_ = ops.Get<real>("diffusion.turbulent.scale_factor.particle", "",
                                                                 scale_factor_diffusion_turbulent_particle_);

    if (scale_factor_diffusion_turbulent_gas_ <= real(0))
      {
        diffusivity_turbulent_gas_ = ops.Get<real>("diffusion.turbulent.gas", "", diffusivity_turbulent_gas_);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "diffusivity_turbulent_gas = "
                             << diffusivity_turbulent_gas_ << endl;
#endif
      }
#ifdef AMC_WITH_LOGGER
    else
      *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "scale_factor_diffusion_turbulent_gas = "
                           << scale_factor_diffusion_turbulent_gas_ << endl;
#endif

    if (scale_factor_diffusion_turbulent_gas_ <= real(0) && diffusivity_turbulent_gas_ <= real(0))
      throw AMC::Error("Both scale factor and diffusivity for gas turbulent diffusion are <= 0, one of them should be > 0");

    if (scale_factor_diffusion_turbulent_particle_ <= real(0))
      {
        diffusivity_turbulent_particle_ = ops.Get<real>("diffusion.turbulent.particle", "", diffusivity_turbulent_particle_);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "diffusivity_turbulent_particle = "
                             << diffusivity_turbulent_particle_ << endl;
#endif
      }
#ifdef AMC_WITH_LOGGER
    else
      *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "scale_factor_diffusion_turbulent_particle = "
                           << scale_factor_diffusion_turbulent_particle_ << endl;
#endif

    if (scale_factor_diffusion_turbulent_particle_ <= real(0) && diffusivity_turbulent_particle_ <= real(0))
      throw AMC::Error("Both scale factor and diffusivity for particle turbulent diffusion are <= 0, one of them should be > 0");

    // Thermal variables.
    heat_specific_isobaric_average_ =
      ops.Get<real>("heat_specific_isobaric_average.Joule_g_K", "", heat_specific_isobaric_average_)
      * DRIVER_CONVERT_FROM_G_TO_MICROG;

    thermal_conductance_average_ =
      ops.Get<real>("thermal_conductance_average.Joule_s_K", "", thermal_conductance_average_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "heat_specific_isobaric_average = "
                         << heat_specific_isobaric_average_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "thermal_conductance_average = "
                         << thermal_conductance_average_ << endl;
#endif

    // Display and save frequency.
    display_frequency_ = ops.Get<real>("display_frequency", "", display_frequency_);
    save_frequency_ = ops.Get<real>("save.frequency", "", save_frequency_);
#ifdef AMC_WITH_LOGGER
    if (save_frequency_ > real(0))
      *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "Will write output in path \"" << path_
                           << "\" every " << save_frequency_ << " seconds." << endl;
#endif

#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    use_socket_ = ops.Get<bool>("save.socket", "", use_socket_);
    if (use_socket_)
      port_number_ = ops.Get<int>("save.port_number", "", port_number_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(3) << LogReset() << "Use socket for writing ? "
                         << (use_socket_ ? "YES" : "NO") << endl;
#endif
#endif

    ops.Close();

    //
    // Check configuration.
    //

    if (Nz_ <= 0) throw AMC::Error("Number of Z-axis sections (Nz) <= 0.");
    if (Nr_ <= 0) throw AMC::Error("Number of radial sections (Nr) <= 0.");

    // Space steps.
    dz_ = ClassChamproboisBase::chimney_length_ / real(Nz_);
    dr_ = ClassChamproboisBase::chimney_radius_ / real(Nr_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << LogReset() << "Chimney discretized in ("
                         << Nr_ << "x" << Nz_ << ") sections of size (" << dr_ << ", " << dz_ << ")." << endl;
#endif

    // Time Step.
    if (dt_fixed_ <= real(0))
      throw AMC::Error("Fixed time step should be > 0, got " + to_str(dt_fixed_));

    if (dt_ratio_ <= real(0) || dt_ratio_ > real(1))
      throw AMC::Error("Time step ratio should be within ]0, 1[, got " + to_str(dt_ratio_));

#ifdef AMC_WITH_LOGGER
    if (dt_fixed_ < numeric_limits<real>::max())
      *AMCLogger::GetLog() << Bblue() << Info(1) << LogReset() << "Fixed time step is " << dt_fixed_
                           << " seconds, possibly never used if greater than CFL condition." << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << LogReset() << "dt_ratio = " << dt_ratio_ << endl;
#endif

    // Init advection and diffusion scheme.
    ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>::Init(Nz_, dz_);
    ClassNumericalSolverDiffusionLinear::Init(Nz_, dz_);
    ClassNumericalSolverDiffusionRadial::Init(Nr_, dr_);

    r_.resize(Range(-1, Nr_));
    r_ = real(0);
    for (int i = -1; i <= Nr_; ++i)
      r_(i) = dr_ * (real(i) + real(0.5));

    z_.resize(Range(-1, Nz_));
    z_ = real(0);
    for (int i = -1; i <= Nz_; ++i)
      z_(i) = dz_ * (real(i) + real(0.5));

    //
    // AMC and source.
    //

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "Ng = " << AMC::ClassAerosolData::Ng_ << endl;
#endif

    // Is AMC up ?
    if (AMC::ClassAerosolData::Ng_ == 0) throw AMC::Error("No general sections in AMC. Init AMC on the driver side first.");

    // Check if same number of species as source.
    if (source_ptr_->GetNspecies() != AMC::ClassSpecies::Nspecies_)
      throw AMC::Error("Number of species in source (" + to_str(source_ptr_->GetNspecies()) +
                       ") differs from AMC one (" + to_str(AMC::ClassSpecies::Nspecies_) + ").");

    // And check if same order.
    for (int i = 0; i < AMC::ClassSpecies::Nspecies_; ++i)
      if (source_ptr_->GetSpeciesName(i) != AMC::ClassSpecies::name_[i])
        throw AMC::Error("At index " + to_str(i) + ", species is \"" + source_ptr_->GetSpeciesName(i)
                         + " in source and \"" + to_str(AMC::ClassSpecies::name_[i]) + "\" in AMC.");

    // Number of gas plus volatile species.
    NgasVolatile_ = AMC::ClassSpecies::Ngas_ + source_ptr_->GetNvolatile();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset() << "NgasVolatile = " << NgasVolatile_ << endl;
#endif

    // Size of concentrations. Last is water.
    Nc_ = AMC::ClassAerosolData::Ng_ * (1 + AMC::ClassSpecies::Nspecies_) + NgasVolatile_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset() << "Nc = " << Nc_ << endl;
#endif

    // Loss term.
    loss_.resize(Range(0, Nc_));
    loss_ = real(0);

    // Some gas physical constants, needed outside of AMC.
    collision_factor_.assign(NgasVolatile_ + 1, real(0));
    molecular_diameter_.assign(NgasVolatile_ + 1, real(0));
    molar_mass_.assign(NgasVolatile_ + 1, real(0));

    // All species below Ngas are in AMC.
    for (int i = 0; i < AMC::ClassSpecies::Ngas_; ++i)
      {
        const int j = AMC::ClassSpecies::semivolatile_[i];
        collision_factor_[i] = AMC::ClassSpecies::GetCollisionFactor(j);
        molecular_diameter_[i] = AMC::ClassSpecies::GetMolecularDiameter(j);
        molar_mass_[i] = AMC::ClassSpecies::GetMolarMass(j) * 1.e-9;
      }

    // Remaining one are purely volatile.
    ops.Open(ClassChamproboisBase::configuration_file_);

    for (int i = 0; i < source_ptr_->GetNvolatile(); ++i)
      {
        const int j = AMC::ClassSpecies::Ngas_ + i;
        ops.SetPrefix(ClassChamproboisBase::prefix_ + ".species." + source_ptr_->GetSpeciesName(j) + ".");
        collision_factor_[j] = ops.Get<real>("collision_factor");
        molecular_diameter_[j] = ops.Get<real>("molecular_diameter.angstrom");
        molar_mass_[j] = ops.Get<real>("molar_mass.g_mol") * 1.e-9;
      }

    ops.Close();

    // Last is water.
    collision_factor_.back() = DRIVER_WATER_COLLISION_FACTOR;
    molecular_diameter_.back() = DRIVER_WATER_MOLECULAR_DIAMETER;
    molar_mass_.back() = DRIVER_WATER_MOLAR_MASS; // Already in kg.mol^{-1}

    // Range of concentrations.
    range_all_ = Range::all();
    range_z_ = Range(0, Nz_ - 1);
    range_r_ = Range(0, Nr_ - 1);
    range_number_ = Range(0, AMC::ClassAerosolData::Ng_ - 1);
    range_mass_aer_ = Range(AMC::ClassAerosolData::Ng_,
                            AMC::ClassAerosolData::Ng_ + AMC::ClassAerosolData::NgNspecies_ - 1);
    range_mass_gas_ = Range(AMC::ClassAerosolData::Ng_ + AMC::ClassAerosolData::NgNspecies_,
                            AMC::ClassAerosolData::Ng_ + AMC::ClassAerosolData::NgNspecies_ + AMC::ClassSpecies::Ngas_ - 1);
    range_mass_vol_ = Range(AMC::ClassAerosolData::Ng_ + AMC::ClassAerosolData::NgNspecies_ + AMC::ClassSpecies::Ngas_,
                            AMC::ClassAerosolData::Ng_ + AMC::ClassAerosolData::NgNspecies_ + NgasVolatile_ - 1);
    range_mass_all_ = Range(AMC::ClassAerosolData::Ng_, Nc_);

    // Concentration.
    concentration_.resize(Range(-1, Nz_), Range(-1, Nr_), Range(0, Nc_));
    concentration_total_.resize(Range(-1, Nz_), Range(-1, Nr_));
    concentration_ = real(0);
    concentration_total_ = real(0);

    // Source and background ratio.
    ratio_source_.resize(Nr_);
    ratio_source_ = real(0);

    ratio_background_.resize(Nr_);
    ratio_background_ = real(0);

    real rm(real(0)), rp(real(0));
    for (int i = 0; i < Nr_; ++i)
      {
        rm = rp;
        rp += dr_;
        const real surface = rp * rp - rm * rm;
        {
          const real rmin = rm;
          const real rmax = ClassChamproboisBase::fire_place_radius_ > rp ? rp : ClassChamproboisBase::fire_place_radius_;

          if (rmax > rmin)
            ratio_source_(i) = (rmax * rmax - rmin * rmin) / surface;
        }

        {
          const real rmin = ClassChamproboisBase::fire_place_radius_ > rm ? ClassChamproboisBase::fire_place_radius_ : rm;
          const real rmax = rp;

          if (rmax > rmin)
            ratio_background_(i) = (rmax * rmax - rmin * rmin) / surface;
        }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(2) << LogReset() << "ratio_source = " << ratio_source_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << LogReset() << "ratio_background = " << ratio_background_ << endl;
#endif

    // Speed.
    speed_.resize(Range(-1, Nz_));
    speed_ = real(DRIVER_CHAMPROBOIS_SPEED_DEFAULT);

    // Particle diameters.
    diameter_.resize(Range(-1, Nz_), Range(-1, Nr_), Range(0, AMC::ClassAerosolData::Ng_ - 1));
    diameter_ = real(0);

    for (int j = 0; j < Nz_; ++j)
      for (int k = 0; k < Nr_; ++k)
        for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
          diameter_(j, k, i) = AMC::ClassDiscretizationSize::diameter_mean_[AMC::ClassAerosolData::general_section_[i][0]];

    // Diffusivity of gas and particles.
    diffusivity_gas_.resize(Range(0, NgasVolatile_), Range(-1, Nz_), Range(-1, Nr_));
    diffusivity_particle_.resize(Range(0, AMC::ClassAerosolData::Ng_ - 1), Range(-1, Nz_), Range(-1, Nr_));
    diffusivity_gas_ = real(0);
    diffusivity_particle_ = real(0);

    diffusivity_.resize(Range(0, Nc_));
    diffusivity_ = NULL;

    int h(-1);
    for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
      diffusivity_(++h) = new Array<real, 2>(diffusivity_particle_(i, range_all_, range_all_));

    for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
      for (int j = 0; j < AMC::ClassSpecies::Nspecies_; ++j)
        diffusivity_(++h) = new Array<real, 2>(diffusivity_particle_(i, range_all_, range_all_));

    for (int i = 0; i <= NgasVolatile_; ++i)
      diffusivity_(++h) = new Array<real, 2>(diffusivity_gas_(i, range_all_, range_all_));

    // Temperature and enthalpy.
    temperature_.resize(Range(-1, Nz_), Range(-1, Nr_));
    temperature_ = real(DRIVER_TEMPERATURE_REFERENCE);
    enthalpy_total_.resize(Range(-1, Nz_), Range(-1, Nr_));
    enthalpy_total_ = real(0);

    // Relative humidity.
    relative_humidity_.resize(Nz_, Nr_);
    relative_humidity_ = real(DRIVER_RELATIVE_HUMIDITY_REFERENCE);

    int Nz_split(Nz_), Nr_split(Nr_);
    AMC::split_domain(ClassDriverBase::Nrank_, Nz_split, Nr_split);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "Nz_split = " << Nz_split << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "Nr_split = " << Nr_split << endl;
#endif

    Nz_rank_.assign(ClassDriverBase::Nrank_, Nz_ / Nz_split);
    Nr_rank_.assign(ClassDriverBase::Nrank_, Nr_ / Nr_split);

    for (int i = 0; i < ClassDriverBase::Nrank_; ++i)
      {
        if (i < Nz_ % Nz_split) Nz_rank_[i]++;
        if (i < Nr_ % Nr_split) Nr_rank_[i]++;
      }

    z_offset_rank_.assign(ClassDriverBase::Nrank_, 0);
    r_offset_rank_.assign(ClassDriverBase::Nrank_, 0);
    for (int i = 0; i < ClassDriverBase::Nrank_ - 1; ++i)
      {
        z_offset_rank_[i + 1] = z_offset_rank_[i] + Nz_rank_[i];
        r_offset_rank_[i + 1] = r_offset_rank_[i] + Nr_rank_[i];
      }

#ifdef AMC_WITH_LOGGER
    if (ClassDriverBase::rank_ == 0)
      {
        *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Parallel computation :" << endl;
          for (int i = 0; i < ClassDriverBase::Nrank_; i++)
            *AMCLogger::GetLog() << Fgreen() << Info(2) << LogReset() << "\trank " << i
                                 << " : z = [ " << z_offset_rank_[i] << ", "
                                 << z_offset_rank_[i] + Nz_rank_[i] << "], r = [ "
                                 << r_offset_rank_[i] << ", " << r_offset_rank_[i] + Nr_rank_[i] << "]" << endl;
      }
#endif

    if (AMC::compute_vector_sum(Nz_rank_) != Nz_ || AMC::compute_vector_sum(Nr_rank_) != Nr_)
      throw AMC::Error("Splitted domain does not cover whole domain.");

#ifdef DRIVER_WITH_MPI
    if (ClassDriverBase::Nrank_ > 1)
      MPI::COMM_WORLD.Barrier();
#endif

    return;
  }


  // Destructor.
  ClassDriverChamprobois::~ClassDriverChamprobois()
  {
    source_ptr_ = NULL;
    for (int i = 0; i < Nsink_; ++i)
      sink_ptr_[i] = NULL;

    for (int i = 0; i <= Nc_; ++i)
      delete diffusivity_(i);

    return;
  }


  // Get methods.
  int ClassDriverChamprobois::GetNc() const
  {
    return Nc_;
  }


  int ClassDriverChamprobois::GetNz() const
  {
    return Nz_;
  }


  int ClassDriverChamprobois::GetNr() const
  {
    return Nr_;
  }


  int ClassDriverChamprobois::GetNsink() const
  {
    return Nsink_;
  }


  real ClassDriverChamprobois::GetPressure() const
  {
    return pressure_;
  }


  string ClassDriverChamprobois::GetName() const
  {
    return name_;
  }


  string ClassDriverChamprobois::GetPath() const
  {
    return path_;
  }


  string ClassDriverChamprobois::GetKey() const
  {
    return key_;
  }


#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
  string ClassDriverChamprobois::GetHostName() const
  {
    return host_name_;
  }


  int ClassDriverChamprobois::GetPortNumber() const
  {
    return port_number_;
  }
#endif


  ClassChamproboisSource* ClassDriverChamprobois::GetSourcePtr()
  {
    return source_ptr_;
  }


  // Set methods.
  void ClassDriverChamprobois::SetPrefix(const string prefix)
  {
    ClassDriverChamprobois::prefix_ = prefix;
  }


  void ClassDriverChamprobois::SetTemperature(const real temperature_celsius)
  {
    temperature_background_ = temperature_celsius + DRIVER_TEMPERATURE_ZERO_DEGREE_CELSIUS;
    temperature_ = temperature_background_;
  }


  void ClassDriverChamprobois::SetPressure(const real pressure)
  {
    pressure_ = pressure;
  }


  void ClassDriverChamprobois::SetConcentrationWater(real volume_humidity_percentage)
  {
    volume_humidity_percentage *= real(0.01);
    const real specific_humidity = volume_humidity_percentage
      / (real(DRIVER_AIR_MOLAR_MASS / DRIVER_WATER_MOLAR_MASS)
         + volume_humidity_percentage);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "specific_humidity = " << specific_humidity << endl;
#endif

    concentration_water_background_ = ClassParameterizationPhysics::
      ComputeWaterVaporDensityFromSpecificHumidity(temperature_background_, pressure_, specific_humidity);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(2) << LogReset() << "Free air water concentration = "
                         << concentration_water_background_ << " µg.m^{-3}" << endl;
#endif

    concentration_(range_all_, range_all_, Nc_) = concentration_water_background_;
  }


  void ClassDriverChamprobois::SetSpeed(const real volumic_flow)
  {
    const real section_surface = AMC_PI * ClassChamproboisBase::chimney_radius_ * ClassChamproboisBase::chimney_radius_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "section_surface (m^2) = " << section_surface << endl;
#endif

    speed_ = volumic_flow / real(3600) / section_surface;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Set flow speed to " << speed_
                         << " m.s^{-1} from volumic flow " << volumic_flow << " m^3.h^{-1}" << endl;
#endif
  }


  void ClassDriverChamprobois::SetTurbulentDiffusion()
  {
    const real section_surface = AMC_PI * ClassChamproboisBase::chimney_radius_ * ClassChamproboisBase::chimney_radius_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "section_surface (m^2) = " << section_surface << endl;
#endif

    const real transport_time_average = ClassChamproboisBase::chimney_length_ / mean(speed_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "transport_time_average = " << transport_time_average << endl;
#endif

    const real diffusion_turbulent_scaled = section_surface / (real(2) * transport_time_average);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "diffusion_turbulent_scaled = "
                         << diffusion_turbulent_scaled << endl;
#endif

    if (scale_factor_diffusion_turbulent_gas_ > real(0))
      {
        diffusivity_turbulent_gas_ = scale_factor_diffusion_turbulent_gas_ * diffusion_turbulent_scaled;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Set turbulent diffusion for gases to "
                             << diffusivity_turbulent_gas_ << " m^2.s^{-1}" << endl;
#endif
      }

    if (scale_factor_diffusion_turbulent_particle_ > real(0))
      {
        diffusivity_turbulent_particle_ = scale_factor_diffusion_turbulent_particle_ * diffusion_turbulent_scaled;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Set turbulent diffusion for particles to "
                             << diffusivity_turbulent_particle_ << " m^2.s^{-1}" << endl;
#endif
      }
  }


  void ClassDriverChamprobois::SetPath(const string path)
  {
    path_ = path;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << LogReset() << "Set file path to \"" << path_ << "\"." << endl;
#endif
  }


#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
  void ClassDriverChamprobois::SetHostName(const string host_name)
  {
    host_name_ = host_name;
  }

  void ClassDriverChamprobois::SetPortNumber(const int port_number)
  {
    port_number_ = port_number;
  }
#endif


  // Add one sink.
  void ClassDriverChamprobois::AddSink(ClassChamproboisSink *sink_ptr)
  {
    sink_ptr_.push_back(sink_ptr);

    // Set surface impaction.
    sink_ptr->SetDiameterBound(AMC::ClassDiscretizationSize::diameter_bound_);
    sink_ptr->SetSurfaceImpaction(Nr_, dr_);

    const int sink_position_z = int(sink_ptr->GetPositionZ() / dz_ + real(1.e-6));
    sink_position_z_.push_back(sink_position_z < Nz_ ? sink_position_z : Nz_ - 1);

    Nsink_++;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << LogReset() << "Add sink \"" << sink_ptr->GetName()
                         << "\" of type \"" << sink_ptr->GetType() << "\" at position "
                         << sink_ptr->GetPositionZ() << " (index = " << sink_position_z_.back()
                         << "). Current number of sinks is " << Nsink_ << endl;
#endif
  }


  void ClassDriverChamprobois::AddFilter(ClassChamproboisFilter *filter_ptr)
  {
    ClassDriverChamprobois::AddSink(filter_ptr);

    // Init the speciation part of sink.
    for (int i = 0; i < AMC::ClassSpecies::Nspecies_; ++i)
      filter_ptr->AddSpecies(AMC::ClassSpecies::name_[i], (AMC::ClassSpecies::semivolatile_reverse_[i] >= 0 ? real(0) : real(-1)), real(0));

    // Init filter.
    filter_ptr->Init(source_ptr_);
  }


  void ClassDriverChamprobois::AddELPI(ClassChamproboisELPI *elpi_ptr)
  {
    ClassDriverChamprobois::AddSink(elpi_ptr);

    // Init ELPI.
    elpi_ptr->Init(AMC::ClassSpecies::Nspecies_);
  }


  // Init one step forward.
  void ClassDriverChamprobois::InitStep(const real &time_out)
  {
    // Init.
    diffusivity_gas_ = real(0);
    diffusivity_particle_ = real(0);

    // Update molecular diffusivity coefficients of gas and particles in air.
    // Last gas species is water vapor.
    if (with_diffusion_molecular_)
      for (int j = 0; j < Nz_; ++j)
        for (int k = 0; k < Nr_; ++k)
          {
            for (int i = 0; i <= AMC::ClassSpecies::Ngas_; ++i)
              diffusivity_gas_(i, j, k) = AMC::ClassParameterizationPhysics::
                ComputeGasDiffusivity(temperature_(j, k),
                                      pressure_,
                                      collision_factor_[i],
                                      molecular_diameter_[i],
                                      molar_mass_[i]);

            for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
              diffusivity_particle_(i, j, k) = AMC::ClassParameterizationPhysics::
                ComputeParticleDiffusivityFuchs(diameter_(j, k, i), temperature_(j, k), pressure_);
          }

    diffusivity_gas_ += diffusivity_turbulent_gas_;

    diffusivity_gas_(range_all_, -1, range_all_) = diffusivity_gas_(range_all_, 0, range_all_);
    diffusivity_gas_(range_all_, Nz_, range_all_) = diffusivity_gas_(range_all_, Nz_ - 1, range_all_);
    diffusivity_gas_(range_all_, range_all_, -1) = diffusivity_gas_(range_all_, range_all_, 0);
    diffusivity_gas_(range_all_, range_all_, Nr_) = diffusivity_gas_(range_all_, range_all_, Nr_ - 1);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
    CBUG << "diffusivity_gas = " << min(diffusivity_gas_) << ", " << mean(diffusivity_gas_)
         << ", " << max(diffusivity_gas_) << endl;
#endif

    diffusivity_particle_ += diffusivity_turbulent_particle_;

    diffusivity_particle_(range_all_, -1, range_all_) = diffusivity_particle_(range_all_, 0, range_all_);
    diffusivity_particle_(range_all_, Nz_, range_all_) = diffusivity_particle_(range_all_, Nz_ - 1, range_all_);
    diffusivity_particle_(range_all_, range_all_, -1) = diffusivity_particle_(range_all_, range_all_, 0);
    diffusivity_particle_(range_all_, range_all_, Nr_) = diffusivity_particle_(range_all_, range_all_, Nr_ - 1);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
    CBUG << "diffusivity_particle = " << min(diffusivity_particle_) << ", " << mean(diffusivity_particle_)
         << ", " << max(diffusivity_particle_) << endl;
#endif

    //
    // Time step.
    //

    // Max time step due to CFL.
    dt_max_ = dz_ / max(speed_) * dt_ratio_;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
    CBUG << "dt_max = " << dt_max_ << endl;
#endif

    dt_ = dt_fixed_ > dt_max_ ? dt_max_ : dt_fixed_;

    if (dt_ > time_out)
      dt_ = time_out;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
    CBUG << "dt = " << dt_ << endl;
#endif
  }


  // Forward.
  bool ClassDriverChamprobois::Forward(const vector<real> &source, const real &temperature)
  {
    Array<real, 1> source_arr(const_cast<real*>(source.data()), shape(source_ptr_->GetNs() + 1), neverDeleteData);
    return Forward(source_arr, temperature);
  }


  bool ClassDriverChamprobois::Forward(const Array<real, 1> &source, const real &temperature)
  {
    //
    // Total enthalpy.
    //

    enthalpy_total_ = concentration_total_ * temperature_;

    // Concentration and enthalpy fluxes.
    Array<real, 3> flux_concentration(Nz_, Nr_, Nc_ + 1);
    Array<real, 2> flux_enthalpy_total(Nz_, Nr_);

    flux_concentration = real(0);
    flux_enthalpy_total = real(0);


    //
    // Advection.
    //


    // Source term is directly incorporated to concentrations.
    // This should be changed in case of external mixing.
    concentration_(-1, range_all_, range_all_) = real(0);

    for (int i = 0; i < Nr_; ++i)
      if (ratio_source_(i) > real(0))
        concentration_(-1, i, range_all_) = source * ratio_source_(i);

    // Add water from background air. We should also add many other things ...
    //for (int i = 0; i < Nr_; ++i)
    //  if (ratio_background_(i) > real(0))
    //    {
    //      if (ratio_source_(i) > real(0))
    //        concentration_(-1, i, Nc_) += concentration_water_background_ * ratio_background_(i);
    //      else
    //        concentration_(-1, i, Nc_) = concentration_water_background_ * ratio_background_(i);
    //    }

    // We do not use the background ratio because we are not sure of what contain the source water concentration.
    for (int i = 0; i < Nr_; ++i)
      if (ratio_source_(i) > real(0))
        concentration_(-1, i, Nc_) += concentration_water_background_;
      else
        concentration_(-1, i, Nc_) = concentration_water_background_;

    for (int i = 0; i < Nr_; ++i)
      concentration_total_(-1, i) = sum(concentration_(-1, i, range_mass_all_));

    // The source temperature.
    temperature_(-1, 0) = temperature;

    // Total enthalpy.
    enthalpy_total_(-1, 0) = concentration_total_(-1, 0) * temperature;

    // Border concentrations.
    concentration_(Nz_, range_all_, range_all_) = real(0);
    enthalpy_total_(Nz_, range_all_) = real(0);

    Array<real, 1> flux_advection(Nz_);

    for (int i = 0; i <= Nc_; ++i)
      for (int j = 0; j < Nr_; ++j)
        {
          ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>::
            ExplicitScheme(speed_, concentration_(range_all_, j, i), flux_advection);
          flux_concentration(range_all_, j, i) = flux_advection;
        }

    for (int j = 0; j < Nr_; ++j)
      {
        ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>::
          ExplicitScheme(speed_, enthalpy_total_(range_all_, j), flux_advection);
        flux_enthalpy_total(range_all_, j) = flux_advection;
      }


    //
    // Diffusion.
    //


    if (with_diffusion_)
      {
        // Avoid count twice source term.
        concentration_(-1, range_all_, range_all_) = concentration_(0, range_all_, range_all_);

        // Border concentrations.
        concentration_(Nz_, range_all_, range_all_) = concentration_(Nz_ - 1, range_all_, range_all_);
        concentration_(range_all_, -1, range_all_) = - concentration_(range_all_, 0, range_all_);
        concentration_(range_all_, Nr_, range_all_) = concentration_(range_all_, Nr_ - 1, range_all_);

        // Border temperature.
        temperature_(range_all_, -1) = temperature_(range_all_, 0);
        temperature_(range_all_, Nr_) = temperature_(range_all_, Nr_ - 1);

        // Loss account.
        if (with_loss_)
          for (int i = 0; i <= Nc_; ++i)
            concentration_(range_all_, Nr_, i) -= concentration_(range_all_, Nr_ - 1, i)
              * loss_(i) / (*diffusivity_(i))(range_all_, Nr_);

        // Radial.
        Array<real, 1> flux_diffusion_radial(Nr_);
        Array<real, 1> thermal_diffusivity_radial(Range(-1, Nr_));

        for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
          for (int j = 0; j < Nz_; ++j)
            {
              ClassNumericalSolverDiffusionRadial::ExplicitScheme((*diffusivity_(i))(j, range_all_),
                                                                  concentration_(j, range_all_, i),
                                                                  flux_diffusion_radial);

              flux_concentration(j, range_all_, i) += flux_diffusion_radial;
            }

        for (int i = AMC::ClassAerosolData::Ng_; i <= Nc_; ++i)
          for (int j = 0; j < Nz_; ++j)
            {
              ClassNumericalSolverDiffusionRadial::ExplicitScheme((*diffusivity_(i))(j, range_all_),
                                                                  concentration_(j, range_all_, i),
                                                                  flux_diffusion_radial);

              flux_concentration(j, range_all_, i) += flux_diffusion_radial;

              thermal_diffusivity_radial = (*diffusivity_(i))(j, range_all_) * temperature_(j, range_all_);

              ClassNumericalSolverDiffusionRadial::ExplicitScheme(thermal_diffusivity_radial,
                                                                  concentration_(j, range_all_, i),
                                                                  flux_diffusion_radial);

              flux_enthalpy_total(j, range_all_) += flux_diffusion_radial;
            }

        // Axial.
        Array<real, 1> flux_diffusion_axial(Nz_);
        Array<real, 1> thermal_diffusivity_axial(Range(-1, Nz_));

        for (int i = 0; i < AMC::ClassAerosolData::Ng_; ++i)
          for (int j = 0; j < Nr_; ++j)
            {
              ClassNumericalSolverDiffusionLinear::ExplicitScheme((*diffusivity_(i))(range_all_, j),
                                                                  concentration_(range_all_, j, i),
                                                                  flux_diffusion_axial);

              flux_concentration(range_all_, j, i) += flux_diffusion_axial;
            }

        for (int i = AMC::ClassAerosolData::Ng_; i <= Nc_; ++i)
          for (int j = 0; j < Nr_; ++j)
            {
              ClassNumericalSolverDiffusionLinear::ExplicitScheme((*diffusivity_(i))(range_all_, j),
                                                                  concentration_(range_all_, j, i),
                                                                  flux_diffusion_axial);

              flux_concentration(range_all_, j, i) += flux_diffusion_axial;

              thermal_diffusivity_axial = (*diffusivity_(i))(range_all_, j) * temperature_(range_all_, j);

              ClassNumericalSolverDiffusionLinear::ExplicitScheme(thermal_diffusivity_axial,
                                                                  concentration_(range_all_, j, i),
                                                                  flux_diffusion_axial);

              flux_enthalpy_total(range_all_, j) += flux_diffusion_axial;
            }
      }

    //
    // Integrate advection/diffusion.
    //

    // Check if clipping.
    bool clipping(false);

    for (int i = 0; i < Nz_; ++i)
      for (int j = 0; j < Nr_; ++j)
        for (int k = 0; k <= Nc_; ++k)
          if (concentration_(i, j, k) > real(0))
            if (concentration_(i, j, k) + flux_concentration(i, j, k) < real(0))
              {
                const real dt_clipping = - concentration_(i, j, k) / flux_concentration(i, j, k) * dt_;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
                CBUG << "Clipping at (i, j, k) = (" << i << ", " << j << ", " << k << ")" << endl;
                CBUG << "\tconcentration = " << concentration_(i, j, k) << endl;
                CBUG << "\tflux_concentration = " << flux_concentration(i, j, k) << endl;
#endif
                clipping = true;

                if (dt_clipping < dt_max_)
                  dt_max_ = dt_clipping;
              }

    // If clipping we return, otherwise go on.
    if (clipping)
      {
        dt_ = dt_max_ * dt_ratio_;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
        CBUG << "Clipping, return with dt = " << dt_ << endl;
#endif

        return false;
      }

    concentration_(range_z_, range_r_, range_all_) += flux_concentration(range_all_, range_all_, range_all_);

    // Total concentration.
    Array<real, 3> concentration_mass_total = concentration_(range_all_, range_all_, range_mass_all_);
    concentration_total_ = sum(concentration_mass_total, blitz::tensor::k);

    //
    // Thermal exchange with background air.
    //

    const real thermal_diffusivity_average = thermal_conductance_average_ / heat_specific_isobaric_average_;

    for (int j = 0; j < Nz_; ++j)
      flux_enthalpy_total(j, range_all_) += dt_ * thermal_diffusivity_average * temperature_background_;

    // Integrate enthalpy ... No, directly integrate temperature instead.
    //enthalpy_total_(range_z_, range_r_) += flux_enthalpy_total(range_all_, range_all_);

    temperature_(range_z_, range_r_) =
      (enthalpy_total_(range_z_, range_r_) + flux_enthalpy_total(range_all_, range_all_))
      / (concentration_total_(range_z_, range_r_) + dt_ * thermal_diffusivity_average);

    // Temperature should not be less than background one.
    temperature_ = where(temperature_ < temperature_background_, temperature_background_, temperature_);

    // Temperature should not be greater than that of source.
    temperature_ = where(temperature_ > temperature, temperature, temperature_);

    // Border temperature.
    temperature_(range_all_, -1) = temperature_(range_all_, 0);
    temperature_(range_all_, Nr_) = temperature_(range_all_, Nr_ - 1);

    // Compute relative humidity.
    for (int i = 0; i < Nz_; ++i)
      for (int j = 0; j < Nr_; ++j)
        {
          const real specific_humidity = ClassParameterizationPhysics::
            ComputeSpecificHumidityFromWaterVaporDensity(temperature_(i, j), pressure_, concentration_(i, j, Nc_));

          relative_humidity_(i, j) = ClassParameterizationPhysics::
            ComputeRelativeHumidityFromSpecificHumidity(temperature_(i, j), pressure_, specific_humidity);
        }

    // Everything went well.
    return true;
  }


  template<class S, class R, class D, class C>
  void ClassDriverChamprobois::Run(real time_out, const bool passive)
  {
#ifdef AMC_WITH_TIMER
    const int time_index = AMC::AMCTimer<AMC::CPU>::Add("run_champrobois_" + name_);
    AMC::AMCTimer<AMC::CPU>::Start();
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info() << LogReset() << "Champrobois AMC simulation starts." << endl;
    *AMCLogger::GetLog() << Bred() << Info() << LogReset() << "Champrobois driver runs in "
                         << (passive ? "PASSIVE" : "AEROSOL") << "mode." << endl;
    if (! with_diffusion_)
      *AMCLogger::GetLog() << Bred() << Warning(0) << LogReset() << "Performing simulation WITHOUT diffusion." << endl;
#endif

    time_out = time_out > real(0) ? time_out : source_ptr_->GetTimeOut();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug() << LogReset() << "time_out = " << time_out << endl;
#endif

    // Record display and save times.
    real display_time(real(0)), save_time(real(0));

    // Source variables.
    Array<real, 1> source(Range(0, source_ptr_->GetNs()));
    source = real(0);

    real temperature(real(0));

    // Time loop.
    t_ = real(0);
    while (time_out > real(0))
      {
        // Init step. This also sets the time step.
        InitStep(time_out);

        // Solve advection diffusion source loss over one time step.
        if (ClassDriverBase::rank_ == 0)
          do
            {
              ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>::SetTimeStep(dt_);
              ClassNumericalSolverDiffusionLinear::SetTimeStep(dt_);
              ClassNumericalSolverDiffusionRadial::SetTimeStep(dt_);

              // Retrieve source.
              source_ptr_->Compute(t_, dt_, source);
              temperature = source_ptr_->GetTemperature(t_);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "source = " << source << endl;
              CBUG << "temperature = " << temperature << endl;
#endif
            }
          while (! Forward(source, temperature));

        // The aerosol part.
        if (! passive)
          {
#ifdef DRIVER_WITH_MPI
            if (ClassDriverBase::Nrank_ > 1)
              MPI::COMM_WORLD.Barrier();
#endif
            Forward<S, R, D, C>();

#ifdef DRIVER_WITH_MPI
            if (ClassDriverBase::Nrank_ > 1)
              MPI::COMM_WORLD.Barrier();
#endif
          }

        time_out -= dt_;
        t_ += dt_;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
        CBUG << "relative_humidity = " << relative_humidity_ << endl;
        CBUG << "temperature = " << temperature_(range_z_, range_r_) << endl;
        CBUG << "concentration_total = " << concentration_total_(range_z_, range_r_) << endl;
        CBUG << "sum(concentration_total) = " << sum(concentration_total_(range_z_, range_r_)) << endl;
        CBUG << "t = " << t_ << ", dt = " << dt_ << endl;
#endif

        if (ClassDriverBase::rank_ == 0)
          {
            // Save some data.
            if (save_frequency_ > real(0))
              if (t_ >= save_time || time_out == real(0))
                {
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
                  // Send everything elsewhere or write to file.
                  if (use_socket_)
                    Send(time_out);
                  else
#endif
                    Write();

                  save_time += save_frequency_;
                }

            // Fill sinks.
            for (int i = 0; i < Nsink_; ++i)
              sink_ptr_[i]->Fill(t_, speed_(sink_position_z_[i]) * dt_,
                                 concentration_(sink_position_z_[i], range_all_, range_all_),
                                 concentration_total_(sink_position_z_[i], range_all_),
                                 temperature_(sink_position_z_[i], range_all_));

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
            getchar();
#endif

            // Display some data.
            if (display_frequency_ > real(0))
              if (t_ >= display_time || time_out == real(0))
                {
                  cout << "relative_humidity = " << relative_humidity_ << endl;
                  cout << "temperature = " << temperature_(range_z_, range_r_) << endl;
                  cout << "concentration_total = " << concentration_total_(range_z_, range_r_) << endl;
                  cout << "sum(concentration_total) = " << sum(concentration_total_(range_z_, range_r_)) << endl;
                  cout << "time = " << t_ << endl;
                  display_time += display_frequency_;
                }
          }
      }


    // Finally, say to the sink this is the end.
    if (ClassDriverBase::rank_ == 0)
      for (int i = 0; i < Nsink_; ++i)
        sink_ptr_[i]->End();

#ifdef AMC_WITH_TIMER
    AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Performed Champrobois simulation \"" << name_
                         << "\" in " << setprecision(12) << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index)
                         << " seconds." << endl;
#endif
#endif

#ifdef DRIVER_WITH_MPI
    if (ClassDriverBase::Nrank_ > 1)
      MPI::COMM_WORLD.Barrier();
#endif
  }


  template<class S, class R, class D, class C>
  void ClassDriverChamprobois::Forward()
  {
    // Aerosol and gas related rates.
    vector1r rate_aer_number(AMC::ClassAerosolData::Ng_, real(0)),
      rate_aer_mass(AMC::ClassAerosolData::NgNspecies_, real(0)),
      rate_gas(AMC::ClassAerosolData::Ngas_, real(0));

    // Time step for AMC.
    int Nt;
    real dt(real(0));

    // Compute aerosol dynamics.
    for (int i = z_offset_rank_[ClassDriverBase::rank_]; i < Nz_rank_[ClassDriverBase::rank_]; ++i)
      for (int j = r_offset_rank_[ClassDriverBase::rank_]; j < Nr_rank_[ClassDriverBase::rank_]; ++j)
        {
          // We assume here internal mixing on the AMC side.
          Array<real, 1> concentration_aer_number = concentration_(i, j, range_number_);

          // Solve dynamic only if something.
          if (sum(concentration_aer_number) > AMC_NUMBER_CONCENTRATION_MINIMUM)
            {
#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "(i, j) = (" << i << ", " << j << ")" << endl;
#endif

              Array<real, 1> concentration_aer_mass = concentration_(i, j, range_mass_aer_);
              Array<real, 1> concentration_gas = concentration_(i, j, range_mass_gas_);

              AMC::ClassMeteorologicalData::SetTemperature(temperature_(i, j));
              AMC::ClassMeteorologicalData::SetPressure(pressure_);
              AMC::ClassMeteorologicalData::SetRelativeHumidity(relative_humidity_(i, j));

              AMC::ClassMeteorologicalData::UpdateCoagulation();
              AMC::ClassMeteorologicalData::UpdateCondensation();

              // Particle wet diameter.
              AMC::ClassParameterization::ComputeParticleDiameterGerber(0, AMC::ClassAerosolData::Ng_,
                                                                        concentration_aer_number.data(),
                                                                        concentration_aer_mass.data());

              //
              // Coagulation.
              //

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "sum(concentration_aer_number) = " << sum(concentration_aer_number) << endl;
              CBUG << "sum(concentration_aer_mass) = " << sum(concentration_aer_mass) << endl;
#endif

              AMC::ClassDynamicsCoagulation<C>::ComputeKernel();

              Nt = AMC::ClassNumericalSolverCoagulation<C>::GetNtMax();
              dt = dt_;

              while (dt > real(0) && Nt > 0)
                {
#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
                  CBUG << "dt = " << dt << endl;
#endif

                  rate_aer_number.assign(AMC::ClassAerosolData::Ng_, real(0));
                  rate_aer_mass.assign(AMC::ClassAerosolData::NgNspecies_, real(0));

                  AMC::ClassDynamicsCoagulation<C>::Rate(0, AMC::ClassAerosolData::Ng_,
                                                            concentration_aer_number.data(),
                                                            concentration_aer_mass.data(),
                                                            rate_aer_number,
                                                            rate_aer_mass);

                  dt = AMC::ClassNumericalSolverBase::Explicit(0, AMC::ClassAerosolData::Ng_,
                                                               dt,
                                                               rate_aer_number.data(),
                                                               rate_aer_mass.data(),
                                                               NULL,
                                                               concentration_aer_number.data(),
                                                               concentration_aer_mass.data(),
                                                               NULL);
                  Nt--;
                }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "sum(concentration_aer_number) = " << sum(concentration_aer_number) << endl;
              CBUG << "sum(concentration_aer_mass) = " << sum(concentration_aer_mass) << endl;
#endif

              //
              // Condensation.
              //

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "sum(concentration_aer_number) = " << sum(concentration_aer_number) << endl;
              CBUG << "sum(concentration_aer_mass) = " << sum(concentration_aer_mass) << endl;
              CBUG << "sum(concentration_gas) = " << sum(concentration_gas) << endl;
              CBUG << "sum(concentration_aer_mass) + sum(concentration_gas) = "
                   << sum(concentration_aer_mass) + sum(concentration_gas) << endl;
#endif

              Nt = AMC::ClassNumericalSolverCondensation::GetNtMax();
              dt = dt_;

              while (dt > real(0) && Nt > 0)
                {
#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
                  CBUG << "dt = " << dt << endl;
#endif

                  rate_aer_mass.assign(AMC::ClassAerosolData::NgNspecies_, real(0));
                  rate_gas.assign(AMC::ClassAerosolData::Ngas_, real(0));

                  // Thermodynamics.
                  AMC::ClassThermodynamics::Compute(0, AMC::ClassAerosolData::Ng_,
                                                    concentration_aer_number.data(),
                                                    concentration_aer_mass.data());

                  // Particle wet diameter.
                  AMC::ClassParameterization::ComputeParticleDiameter(0, AMC::ClassAerosolData::Ng_,
                                                                      concentration_aer_number.data(),
                                                                      concentration_aer_mass.data());

                  // Condensation.
                  AMC::ClassDynamicsCondensation::RateLagrangian(0, AMC::ClassAerosolData::Ng_,
                                                                 concentration_aer_number.data(),
                                                                 concentration_aer_mass.data(),
                                                                 concentration_gas.data(),
                                                                 rate_aer_mass,
                                                                 rate_gas);

                  dt = AMC::ClassNumericalSolverCondensation::LagrangianExplicit(0, AMC::ClassAerosolData::Ng_,
                                                                                 dt,
                                                                                 rate_aer_mass,
                                                                                 rate_gas,
                                                                                 concentration_aer_number.data(),
                                                                                 concentration_aer_mass.data(),
                                                                                 concentration_gas.data());
                  Nt--;
                }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "sum(concentration_aer_number) = " << sum(concentration_aer_number) << endl;
              CBUG << "sum(concentration_aer_mass) = " << sum(concentration_aer_mass) << endl;
              CBUG << "sum(concentration_gas) = " << sum(concentration_gas) << endl;
              CBUG << "sum(concentration_aer_mass) + sum(concentration_gas) = "
                   << sum(concentration_aer_mass) + sum(concentration_gas) << endl;
#endif

              // Once integrated, perform redistribution.
              AMC::ClassRedistribution<S, R, D>::Redistribute(0, AMC::ClassAerosolData::Ng_,
                                                              concentration_aer_number.data(),
                                                              concentration_aer_mass.data());

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
              CBUG << "sum(concentration_aer_number) = " << sum(concentration_aer_number) << endl;
              CBUG << "sum(concentration_aer_mass) = " << sum(concentration_aer_mass) << endl;
#endif

              // Particle wet diameter for next loop.
              AMC::ClassParameterization::ComputeParticleDiameterGerber(0, AMC::ClassAerosolData::Ng_,
                                                                        concentration_aer_number.data(),
                                                                        concentration_aer_mass.data());

              // Return new diameter to driver.
              for (int k = 0; k < AMC::ClassAerosolData::Ng_; ++k)
                diameter_(i, j, k) = AMC::ClassAerosolData::diameter_[k];
            }
        }

    // Get rid of any clipping.
    concentration_ = where(concentration_ > real(0), concentration_, real(0));

    // Total concentration.
    Array<real, 3> concentration_mass_total = concentration_(range_all_, range_all_, range_mass_all_);
    concentration_total_ = sum(concentration_mass_total, blitz::tensor::k);
  }


#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
  // Send.
  void ClassDriverChamprobois::Send(const real &time_out, string host_name, int port_number) const
  {
    host_name = host_name.empty() ? host_name_ : host_name;
    port_number = port_number == 0 ? port_number_ : port_number;

    const int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct hostent *server = gethostbyname(host_name.c_str());

    if (sockfd >= 0 && server != NULL)
      {
        struct sockaddr_in server_address;
        bzero((char *) &server_address, sizeof(server_address));
        server_address.sin_family = AF_INET;
        bcopy((char *)server->h_addr, (char *)&server_address.sin_addr.s_addr, server->h_length);
        server_address.sin_port = htons(port_number);

        if (connect(sockfd, (struct sockaddr *) &server_address, sizeof(server_address)) == 0)
          {
            const int t = t_ + source_ptr_->GetDateBegin();

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
            CBUG << "Send data for t = " << t << endl;
#else
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fyellow() << Info(3) << LogReset() << "Send data for t = " << t << endl;
#endif
#endif

            if (write(sockfd, reinterpret_cast<const char*>(&t), sizeof(int)) < 0)
              throw AMC::Error(strerror(errno));

            if (write(sockfd, reinterpret_cast<const char*>(temperature_.data()), temperature_.size() * sizeof(real)) < 0)
              throw AMC::Error(strerror(errno));

            if (write(sockfd, reinterpret_cast<const char*>(relative_humidity_.data()),
                      relative_humidity_.size() * sizeof(real)) < 0)
              throw AMC::Error(strerror(errno));

            if (write(sockfd, reinterpret_cast<const char*>(concentration_.data()), concentration_.size() * sizeof(real)) < 0)
              throw AMC::Error(strerror(errno));

            // If we are on end, tell the server.
            const bool end = time_out == real(0);

            if (write(sockfd, reinterpret_cast<const char*>(&end), sizeof(bool)) < 0)
              throw AMC::Error(strerror(errno));
          }
      }

    close(sockfd);
  }
#endif


  // Write concentrations.
  void ClassDriverChamprobois::Write(string path) const
  {

  }


  // Compute diameter.
  void ClassDriverChamprobois::ComputeDiameter()
  {
    const real density_fixed = source_ptr_->GetDensityFixed();

    for (int i = 0; i < Nz_; ++i)
      for (int j = 0; j < Nr_; ++j)
        {
          Array<real, 1> concentration_number = concentration_(i, j, range_number_);
          Array<real, 1> concentration_mass = concentration_(i, j, range_mass_aer_);

          int k(-1);
          for (int l = 0; l < AMC::ClassAerosolData::Ng_; ++l)
            {
              real mass_total(real(0));
              for (int m = 0; m < AMC::ClassSpecies::Nspecies_; ++m)
                mass_total += concentration_mass(++k);

              if (concentration_number(l) > AMC_NUMBER_CONCENTRATION_MINIMUM && mass_total > real(0))
                diameter_(i, j, l) = pow(AMC_INV_PI6 * mass_total /
                                         (concentration_number(l) * density_fixed), AMC_FRAC3);
              else
                diameter_(i, j, l) = AMC::ClassDiscretizationSize::diameter_mean_[AMC::ClassAerosolData::general_section_[l][0]];
            }
        }
  }
}

#define AMC_FILE_CLASS_DRIVER_CHAMPROBOIS_CXX
#endif
