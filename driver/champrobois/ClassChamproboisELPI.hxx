// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_ELPI_HXX

#define CHAMPROBOIS_ELPI_SAMPLE_RATE_DEFAULT   60
#define CHAMPROBOIS_ELPI_SAMPLE_NUMBER_MAX    180

namespace Driver
{
  /*! 
   * \class ClassChamproboisELPI
   */
  class ClassChamproboisELPI : public ClassChamproboisSink
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  private:

    /*!< Driver time.*/
    real t_;

    /*!< Number of species.*/
    int Nspecies_;

    /*!< Number of samples.*/
    int Nsample_;

    /*!< Max number of samples.*/
    int Nsample_max_;

    /*!< ELPI name.*/
    string name_;

    /*!< Sample rate.*/
    real sample_rate_;

    /*!< Sample time.*/
    real sample_time_;

    /*!< Air volume sampled.*/
    real air_volume_sampled_;

    /*!< Number concentration.*/
    Array<real, 2> concentration_number_;

    /*!< Mass concentration.*/
    Array<real, 2> concentration_mass_;

    /*!< Fill.*/
    void fill_(const real ratio, const real &z, const Array<real, 2> &concentration);

  public:

    /*!< Constructor.*/
    ClassChamproboisELPI(const string name);

    /*!< Destructor.*/
    ~ClassChamproboisELPI();

    /*!< Get methods.*/
    string GetName() const;
    int GetNspecies() const;
    int GetNsample() const;
    int GetNsampleMax() const;
    real GetSampleRate() const;
    void GetConcentrationNumber(vector<vector<real> > &concentration_number) const;
    void GetConcentrationMass(vector<vector<real> > &concentration_mass) const;

    /*!< Set methods.*/
    void SetNsampleMax(const int Nsample_max = CHAMPROBOIS_ELPI_SAMPLE_NUMBER_MAX);
    void SetSampleRate(const real sample_rate = real(CHAMPROBOIS_ELPI_SAMPLE_RATE_DEFAULT));

#ifndef SWIG
    /*!< Init.*/
    void Init(const int &Nspecies);
#endif

#ifndef SWIG
    /*!< Fill sink.*/
    void Fill(const real &t, const real z,
              const Array<real, 2> &concentration,
              const Array<real, 1> &concentration_total,
              const Array<real, 1> &temperature);
#endif

    /*!< Dump.*/
    void Dump(map<string, int> &argument) const;

#ifndef SWIG
    /*!< End.*/
    void End();
#endif
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_ELPI_HXX
#endif
