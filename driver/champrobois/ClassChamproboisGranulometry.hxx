// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_GRANULOMETRY_HXX

namespace Driver
{
  /*! 
   * \class ClassChamproboisGranulometry
   */
  class ClassChamproboisGranulometry : protected ClassChamproboisBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Number of aerosol sections and bounds.*/
    int Nsection_, Nbound_;

    /*!< Number of time steps.*/
    int Nt_;

    /*!< Beginning date.*/
    real date_begin_;

    /*!< Particle density.*/
    real density_fixed_;

    /*!< Name of source.*/
    string name_;

    /*!< Key of source.*/
    string key_;

    /*!< Instrument.*/
    string instrument_;

    /*!< Diameter bound.*/
    vector1r diameter_bound_;

    /*!< Diameter mean.*/
    vector1r diameter_mean_;

    /*!< Mass mean.*/
    vector1r mass_mean_;

    /*!< Time in seconds from beginning date.*/
    vector1r time_;

    /*!< Total mass concentration.*/
    Array<real, 2> concentration_aer_mass_total_;

    /*!< Total mass concentration fraction.*/
    vector1r concentration_aer_mass_total_fraction_;

    /*!< Species number concentration.*/
    Array<real, 2> concentration_aer_number_;

    /*!< Filter concentration number.*/
    void filter_concentration_number();

    /*!< Compute single mass mean.*/
    void compute_mass_mean();

    /*!< Compute total mass concentration.*/
    void compute_concentration_mass_total();

  public:

    /*!< Constructor.*/
    ClassChamproboisGranulometry(const string name);

    /*!< Destructor.*/
    virtual ~ClassChamproboisGranulometry();

    /*!< Get methods.*/
    int GetNsection() const;
    string GetName() const;
    string GetInstrument() const;
    string GetKey() const;
    real GetDensityFixed() const;
    real GetDateBegin() const;
    real GetTimeOut() const;
    void GetTime(vector<real> &time) const;
    vector<real> GetDiameterBound() const; 
    vector<real> GetDiameterMean() const; 
    void GetConcentrationAerNumber(vector<vector<real> > &concentration_aer_number) const;
    void GetConcentrationAerMassTotal(vector<vector<real> > &concentration_aer_mass_total) const;
    void GetConcentrationAerMassTotalFraction(vector<real> &concentration_aer_mass_total_fraction) const;

    /*!< Set methods.*/
    void SetDateBegin(const real date_begin);
    void SetTime(const vector<real> &time);
    void SetKey(const string &key);
    void SetInstrument(const string &instrument);
    void SetParticleDensity(const real &density_fixed);
    void SetParticleDiameter(const vector<real> &diameter_bound, const vector<real> &diameter_mean);
    void SetParticleNumberConcentration(const vector<vector<real> > &concentration_aer_number);

    /*!< Compute.*/
    real ComputeMassTotalAerFromGranulometry(int Nsection = -1) const;

    /*!< Dump.*/
    virtual void Dump(const int width = 12, const int precision = 6) const;
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_GRANULOMETRY_HXX
#endif
