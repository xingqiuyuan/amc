require("os")

champrobois_path = os.getenv("HOME") .. "/code/amc/driver/champrobois"
champrobois_work = os.getenv("HOME") .. "/work/champrobois"

champrobois_oc_to_om = 1.9

speciation = {
   PM10 = {
      diameter = {min = 0., max = 10.},

      total = {
         label = "Particulate matter < 10 µm",
         key = {"", ""},
         aer = 1.
      },

      species = {
         DUST = {
            label = "dust",
            key = {"", ""},
            aer = 1.
         }
      }
   },

   PM25 = {
      diameter = {min = 0., max = 2.5},

      total = {
         label = "Particulate matter < 2.5 µm",
         key = {"", "poussieres - fraction solide"}
      },

      species = {
         EC = {
            label = "elemental carbon",
            key = {"", "ec particulaire (filtre)"}
         },

         OM = {
            label = "organic matter",
            key = {"oc gazeux (sunset barboteur eau)", "oc particulaire (filtre)"},
            factor = champrobois_oc_to_om}
      }
   },

   OM_ctp = {
      diameter = {min = 0., max = 2.5},

      total = {
         label = "organic matter",
         key = {"oc gazeux (sunset barboteur eau)", "oc particulaire (filtre)"},
         factor = champrobois_oc_to_om,
      },

      species = {
         LVG = {label = "levoglucosan", key = {"&l gazeux (resine ou barboteur ipo)", "&l particulaire"}},
         ACEN = {label = "acenaphtene"},
         ANTH = {label = "anthracene"},
         BZAN = {label = "benzo (a) anthracene"},
         BZAP = {label = "benzo (a) pyrene"},
         BZBF = {label = "benzo (b) fluoranthene"},
         BZEP = {label = "benzo (e) phenantrene"},
         BZPE = {label = "benzo (g,h,i) perylene"},
         BZKF = {label = "benzo (k) fluoranthene"},
         ALK19 = {label = "nonadecane", key = "c19"},
         ALK33 = {label = "tritiacontane", key = "c33"},
         CHRY = {label = "chrysene"},
         CORO = {label = "coronene"},
         DBZA = {label = "dibenzo (a,h) anthracene"},
         FLRT = {label = "fluoranthene"},
         FLRE = {label = "fluorene"},
         INPY = {label = "indeno (1,2,3-c,d) pyrene"},
         NAPT = {label = "naphtalene"},
         PETN = {label = "phenantrene"},
         PTAN = {label = "phytane"},
         PYR = {label = "pyrene"},
         RETE = {label = "retene"},
      }
   },

   OM_cp = {
      diameter = {min = 0., max = 2.5},

      total = {
         label = "organic matter",
         key = {"oc gazeux (sunset barboteur eau)", "oc particulaire (filtre)"},
         factor = champrobois_oc_to_om,
      },

      species = {
         LVG = {label = "levoglucosan", key = {"&l gazeux (barboteur eau)", "&l particulaire"}},
         ACEN = {label = "acenaphtene"},
         ANTH = {label = "anthracene"},
         BZAN = {label = "benzo (a) anthracene"},
         BZAP = {label = "benzo (a) pyrene"},
         BZBF = {label = "benzo (b) fluoranthene"},
         BZEP = {label = "benzo (e) phenantrene"},
         BZPE = {label = "benzo (g,h,i) perylene"},
         BZKF = {label = "benzo (k) fluoranthene"},
         ALK19 = {label = "nonadecane", key = "c19"},
         ALK33 = {label = "tritiacontane", key = "c33"},
         CHRY = {label = "chrysene"},
         CORO = {label = "coronene"},
         DBZA = {label = "dibenzo (a,h) anthracene"},
         FLRT = {label = "fluoranthene"},
         FLRE = {label = "fluorene"},
         INPY = {label = "indeno (1,2,3-c,d) pyrene"},
         NAPT = {label = "naphtalene"},
         PETN = {label = "phenantrene"},
         PTAN = {label = "phytane"},
         PYR = {label = "pyrene"},
         RETE = {label = "retene"},
      }
   }
}

champrobois = {
   species = {
      CO = {
         molar_mass = {g_mol = 28.0},
         molecular_diameter = {angstrom = 1.128},
         collision_factor = 400.
      },

      CO2 = {
         molar_mass = {g_mol = 44.0},
         molecular_diameter = {angstrom = 2.326},
         collision_factor = 400.
      }
   },

   chimney = {length = 15, radius = 1.2},
   fire_place = {radius = 0.2},

   raw_data_file = champrobois_work .. "/raw_data_champrobois.bin",

   speciation = {
      list = {
         "essai_2.champ_tres_proche",
         "essai_3.champ_tres_proche",
         "essai_5.champ_tres_proche",
         "essai_6.champ_tres_proche",
         "essai_7.champ_tres_proche",
         "essai_9.champ_tres_proche",
         "essai_2.champ_proche",
         "essai_3.champ_proche",
         "essai_5.champ_proche",
         "essai_6.champ_proche",
         "essai_7.champ_proche",
         "essai_9.champ_proche"
      },

      default = {
         PM10 = speciation.PM10
      },

      essai_2 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },

         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      },

      essai_3 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },
         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      },

      essai_5 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },
         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      },

      essai_6 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },
         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      },

      essai_7 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },
         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      },

      essai_9 = {
         champ_tres_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_ctp
         },
         champ_proche = {
            PM25 = speciation.PM25,
            OM = speciation.OM_cp
         }
      }
   },

   source = {

      essai_2 = {
         temperature_celsius = 37
      },

      essai_3 = {
         temperature_celsius = 33
      },

      essai_5 = {
         temperature_celsius = 35
      },

      essai_6 = {
         temperature_celsius = 50
      },

      essai_7 = {
         temperature_celsius = 49
      },

      essai_9 = {
         temperature_celsius = 24
      }
   }
}