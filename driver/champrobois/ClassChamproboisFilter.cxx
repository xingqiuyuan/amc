// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_FILTER_CXX

#include "ClassChamproboisFilter.hxx"


namespace Driver
{
  // Constructor.
  ClassChamproboisFilter::ClassChamproboisFilter(const string name, const string species_name_total)
    : ClassChamproboisSink("Filter"), ClassChamproboisSpeciation(name, species_name_total, false),
      Ngas_(0), Nvolatile_(0), concentration_number_(real(0))
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset()
                         << "Instantiate Champrobois Filter \"" << this->name_ << "\"." << endl;
#endif

    return;
  }


  // Destructor.
  ClassChamproboisFilter::~ClassChamproboisFilter()
  {
    return;
  }


  // Get methods.
  int ClassChamproboisFilter::GetNgas() const
  {
    return Ngas_;
  }


  int ClassChamproboisFilter::GetNvolatile() const
  {
    return Nvolatile_;
  }


  real ClassChamproboisFilter::GetConcentrationNumber() const
  {
    return concentration_number_;
  }


  void ClassChamproboisFilter::GetConcentrationVolatile(vector<real> &concentration_volatile) const
  {
    concentration_volatile = concentration_volatile_;
  }


  // Set methods.
  void ClassChamproboisFilter::Init(const ClassChamproboisSource *source_ptr)
  {
    if (this->Nspecies_ <= 0)
      throw AMC::Error("Init for filter \"" + name_ + "\" doest not have any species.");

    // Number of gas species.
    Ngas_ = 0;
    for (int i = 0; i < this->Nspecies_; ++i)
      if (this->is_semi_volatile_[i]) Ngas_++;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "Ngas = " << Ngas_ << endl;
#endif

    // Volatile species.
    Nvolatile_ = source_ptr->GetNvolatile();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "Nvolatile = " << Nvolatile_ << endl;
#endif

    // Gas index.
    gas_index_ = source_ptr->GetGasIndex();

    if (int(gas_index_.size()) != (Ngas_ + Nvolatile_))
      throw AMC::Error("Size of gas index (" + to_str(gas_index_.size()) +
                       ") differs from number of gas plus volatile species (" +
                       to_str(Ngas_ + Nvolatile_) + ").");

    // Fraction sections.
    if (this->Nsection_ <= 0)
      throw AMC::Error("Number of sections is <= 0.");

    // Number of sections set in parent class.
    fraction_section_.assign(this->Nsection_, real(0));

    for (int i = 0; i < this->Nsection_; ++i)
      {
        const real diameter_min = diameter_bound_[i] > this->diameter_min_ ? diameter_bound_[i] : this->diameter_min_;
        const real diameter_max = diameter_bound_[i + 1] < this->diameter_max_ ? diameter_bound_[i + 1] : this->diameter_max_;

        if (diameter_max > diameter_min)
          fraction_section_[i] = log(diameter_max / diameter_min) / log(this->diameter_bound_[i + 1] / this->diameter_bound_[i]);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "fraction_section = " << fraction_section_ << endl;
#endif
  }


  // Fill sink.
  void ClassChamproboisFilter::Fill(const real &t, const real z,
                                    const Array<real, 2> &concentration,
                                    const Array<real, 1> &concentration_total,
                                    const Array<real, 1> &temperature)
  {
    ClassChamproboisSink::Fill(t, z, concentration, concentration_total, temperature);

    for (int i = 0; i < this->Nr_impaction_; ++i)
      {
        const int j = this->index_impaction_[i];
        const real volume_impaction = z * this->surface_impaction_[i];

        for (int k = 0; k < this->Nsection_; ++k)
          if (fraction_section_[k] > real(0))
            {
              const real volume_impaction_section = volume_impaction * fraction_section_[k];

              concentration_number_ += concentration(j, k) * volume_impaction_section;

              // Same species order between driver and sink.
              const int l = this->Nsection_ + k * this->Nspecies_;
              for (int m = 0; m < this->Nspecies_; ++m)
                this->concentration_aer_[m] += concentration(j, l + m) * volume_impaction_section;
            }

        // Pointer for gas species.
        int k = this->Nsection_ * (this->Nspecies_ + 1) - 1;
        for (int l = 0; l < Ngas_; ++l)
          this->concentration_gas_[gas_index_[l]] += concentration(j, ++k) * volume_impaction;

        k = this->Nsection_ * (this->Nspecies_ + 1) + Ngas_ - 1;
        for (int l = 0; l < Nvolatile_; ++l)
          concentration_volatile_[l] += concentration(j, ++k) * volume_impaction;
      }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_FILTER
    CBUG << "concentration_number = " << concentration_number_ << endl;
    CBUG << "concentration_gas = " << this->concentration_gas_ << endl;
    CBUG << "concentration_aer = " << this->concentration_aer_ << endl;
#endif
  }


  // Dump.
  void ClassChamproboisFilter::Dump(map<string, int> &argument) const
  {
    ClassChamproboisSink::Dump();
    ClassChamproboisSpeciation::Dump(argument);
    cout << "Number of gas species = " << Ngas_ << endl
         << "Number of volatile species = " << Nvolatile_ << endl
         << "Number concentration = " << concentration_number_ << endl
         << "Volatile concentration = " << concentration_volatile_ << endl;
  }


  // Compute average concentrations.
  void ClassChamproboisFilter::End()
  {
    ClassChamproboisSink::End();

    const real air_volume_sampled_inv = real(1) / this->air_volume_sampled_;

    concentration_number_ *= air_volume_sampled_inv;

    for (int i = 0; i < ClassChamproboisSpeciation::Nspecies_; ++i)
      {
        this->concentration_aer_[i] *= air_volume_sampled_inv;
        this->concentration_gas_[i] *= air_volume_sampled_inv;
      }

    for (int i = 0; i < Nvolatile_; ++i)
      concentration_volatile_[i] *= air_volume_sampled_inv;

    this->concentration_gas_total_ = this->ComputeConcentrationGasTotal();
    this->concentration_aer_total_ = this->ComputeConcentrationAerTotal();
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_FILTER_CXX
#endif
