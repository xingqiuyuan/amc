// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SPECIATION_CXX

#include "ClassChamproboisSpeciation.hxx"


namespace Driver
{
  ClassChamproboisSpeciation::ClassChamproboisSpeciation(const string name, const string species_name_total,
                                                         const bool read_configuration)
    : name_(name), Nspecies_(0), factor_total_(real(1)), species_name_total_(species_name_total),
      species_label_total_(""), diameter_min_(CHAMPROBOIS_SPECIATION_DIAMETER_MIN),
      diameter_max_(CHAMPROBOIS_SPECIATION_DIAMETER_MAX), concentration_aer_total_(real(0)),
      concentration_gas_total_(real(0)), key_(name)
  {
    if (! read_configuration)
      return;

    Ops::Ops ops(ClassChamproboisBase::configuration_file_);

    const string prefix = ClassChamproboisBase::prefix_
      + ".speciation." + name_ + "." + species_name_total_ + ".";
    ops.SetPrefix(prefix);

    key_ = ops.Get<string>("key", "", key_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset() << "Instantiate Champrobois speciation \""
                         << name << "\" and \"" << species_name_total_ << "\" with key \"" << key_ << "\"." << endl;
#endif

    diameter_min_ = ops.Get<real>("diameter.min", "", diameter_min_);
    diameter_max_ = ops.Get<real>("diameter.max", "", diameter_max_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(2) << LogReset() << "\tbetween diameters (µm) "
                         << diameter_min_ << " and " << diameter_max_ << endl;
#endif

    ops.SetPrefix(prefix + "total.");

    species_label_total_ = ops.Get<string>("label", "", species_name_total_);

    if (ops.Exists("key"))
      {
        if (ops.Is<string>("key"))
          {
            const string key = ops.Get<string>("key");
            species_key_total_ = {key + " gazeux", key + " particulaire"};                
          }
        else
          ops.Set("key", "", species_key_total_);
      }
    else
      species_key_total_ = {species_label_total_ + " gazeux", species_label_total_ + " particulaire"};

    factor_total_ = ops.Get<real>("factor", "", real(1));
    concentration_gas_total_ = ops.Get<real>("gas", "", real(0)) * factor_total_;
    concentration_aer_total_ = ops.Get<real>("aer", "", real(0)) * factor_total_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << LogReset() << "\ttotal is \"" << species_name_total_
                         << "\" with label \"" << species_label_total_ << "\", factor = " << factor_total_
                         << " and keys " << species_key_total_ << " ." << endl;
#endif

    ops.SetPrefix(prefix);
    species_name_ = ops.GetEntryList("species");
    Nspecies_ = int(species_name_.size());

    species_label_.resize(Nspecies_);
    species_key_.resize(Nspecies_);
    factor_.resize(Nspecies_);
    is_semi_volatile_.assign(Nspecies_, true);
    concentration_gas_.assign(Nspecies_, real(0));
    concentration_aer_.assign(Nspecies_, real(0));

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "\tnumber of species = " << Nspecies_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "\tspecies list = " << species_name_ << endl;
#endif

    for (int i = 0; i < Nspecies_; ++i)
      {
        ops.SetPrefix(prefix + "species." + species_name_[i] + ".");

        species_label_[i] = ops.Get<string>("label", "", species_name_[i]);

        if (ops.Exists("key"))
          {
            if (ops.Is<string>("key"))
              {
                const string key = ops.Get<string>("key");
                species_key_[i] = {key + " gazeux", key + " particulaire"};                
              }
            else
              {
                ops.Set("key", "", species_key_[i]);

                for (int j = 0; j < 2; ++j)
                  if (species_key_[i][j].find("&l") != string::npos)
                    species_key_[i][j].replace(species_key_[i][j].find("&l"), 2, species_label_[i]);
              }
          }
        else
          species_key_[i] = {species_label_[i] + " gazeux", species_label_[i] + " particulaire"};

        factor_[i] = ops.Get<real>("factor", "", real(1));
        concentration_aer_[i] = ops.Get<real>("aer", "", real(0)) * factor_[i];
        concentration_gas_[i] = ops.Get<real>("gas", "", real(-1));
        is_semi_volatile_[i] = concentration_gas_[i] >= real(0);
        concentration_gas_[i] = is_semi_volatile_[i] ? concentration_gas_[i] * factor_[i] : real(0);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "\t\tspecies \"" << species_name_[i]
                             << "\" with label \"" << species_label_[i] << "\", factor = " << factor_[i]
                             << " and keys " << species_key_[i] << " ." << endl;
#endif
      }

    ops.Close();

    return;
  }


  // Destructor.
  ClassChamproboisSpeciation::~ClassChamproboisSpeciation()
  {
    return;
  }


  // Get methods.
  bool ClassChamproboisSpeciation::IsGasComplete() const
  {
    return ComputeConcentrationGasTotal() == concentration_gas_total_;
  }


  bool ClassChamproboisSpeciation::IsAerComplete() const
  {
    return ComputeConcentrationAerTotal() == concentration_aer_total_;
  }


  int ClassChamproboisSpeciation::GetNspecies() const
  {
    return Nspecies_;
  }


  int ClassChamproboisSpeciation::GetSpeciesIndex(const string species_name) const
  {
    for (int i = 0; i < Nspecies_; ++i)
      if (species_name_[i] == species_name)
        return i;

    return -1;
  }


  string ClassChamproboisSpeciation::GetName() const
  {
    return name_;
  }


  string ClassChamproboisSpeciation::GetKey() const
  {
    return key_;
  }


  string ClassChamproboisSpeciation::GetSpeciesNameTotal() const
  {
    return species_name_total_;
  }


  string ClassChamproboisSpeciation::GetSpeciesLabelTotal() const
  {
    return species_label_total_;
  }


  void ClassChamproboisSpeciation::GetSpeciesKeyTotal(vector<string> &species_key_total) const
  {
    species_key_total = species_key_total_;
  }


  void ClassChamproboisSpeciation::GetSpeciesNameList(vector<string> &species_name) const
  {
    species_name = species_name_;
  }


  string ClassChamproboisSpeciation::GetSpeciesName(const int i) const
  {
    return species_name_[i];
  }


  string ClassChamproboisSpeciation::GetSpeciesLabel(const int i) const
  {
    return species_label_[i];
  }


  void ClassChamproboisSpeciation::GetSpeciesKey(const int i, vector<string> &species_key) const
  {
    species_key = species_key_[i];
  }


  void ClassChamproboisSpeciation::GetConcentration(vector<real> &gas, vector<real> &aer,
                                                    const vector<string> species_name) const
  {
    if (species_name.empty())
      {
        gas = concentration_gas_;
        aer = concentration_aer_;
      }
    else
      {
        const int Nspecies = int(species_name.size());
        gas.assign(Nspecies, real(0));
        aer.assign(Nspecies, real(0));

        for (int i = 0; i < Nspecies; ++i)
          {
            const int j = GetSpeciesIndex(species_name[i]);
            if (j >= 0)
              {
                gas[i] = concentration_gas_[j];
                aer[i] = concentration_aer_[j];
              }
          }
      }
  }


  real ClassChamproboisSpeciation::GetConcentrationGasTotal() const
  {
    return concentration_gas_total_;
  }


  real ClassChamproboisSpeciation::GetConcentrationAerTotal() const
  {
    return concentration_aer_total_;
  }


  // Set methods.
  void ClassChamproboisSpeciation::SetDiameterMin(const real diameter_min)
  {
    diameter_min_ = diameter_min;
  }


  void ClassChamproboisSpeciation::SetDiameterMax(const real diameter_max)
  {
    diameter_max_ = diameter_max;
  }


  void ClassChamproboisSpeciation::SetSpeciesTotal(const real gas_total, const real aer_total)
  {
    concentration_gas_total_ = gas_total * factor_total_;
    concentration_aer_total_ = aer_total * factor_total_;
  }


  void ClassChamproboisSpeciation::SetSpecies(const int i, const real gas, const real aer)
  {
    is_semi_volatile_[i] = gas >= real(0);
    concentration_gas_[i] = is_semi_volatile_[i] ? gas * factor_[i] : real(0);
    concentration_aer_[i] = aer * factor_[i];
  }


  void ClassChamproboisSpeciation::SetKey(const string key)
  {
    key_ = key;
  }


  // Add one species.
  void ClassChamproboisSpeciation::AddSpecies(const string name, const real gas, const real aer,
                                              const string label, const vector<string> key, const real factor)
  {
    if (GetSpeciesIndex(name) >= 0)
      return;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "Add species \"" << name
                         << "\" at index " << Nspecies_ << " to speciation \"" << name_ << "."
                         << species_name_total_ << "\"." << endl;
#endif

    species_name_.push_back(name);
    species_label_.push_back(label.empty() ? name : label);
    species_key_.push_back(key);
    factor_.push_back(factor);
    is_semi_volatile_.push_back(gas >= real(0));
    concentration_gas_.push_back(gas < real(0) ? real(0) : (gas * factor));
    concentration_aer_.push_back(aer * factor);
    Nspecies_++;
  }


  // Remove one species.
  void ClassChamproboisSpeciation::RemoveSpecies(const int i)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(2) << LogReset() << "Removing species \""
                         << species_name_[i] << "\" at index " << i << " from species list." << endl;
#endif

    species_name_[i] = species_name_.back();
    species_name_.pop_back();

    species_label_[i] = species_label_.back();
    species_label_.pop_back();

    species_key_[i] = species_key_.back();
    species_key_.pop_back();

    is_semi_volatile_[i] = is_semi_volatile_.back();
    is_semi_volatile_.pop_back();

    concentration_gas_total_ -= concentration_gas_[i];
    CLIP(concentration_gas_total_);

    concentration_gas_[i] = concentration_gas_.back();
    concentration_gas_.pop_back();

    concentration_aer_total_ -= concentration_aer_[i];
    CLIP(concentration_aer_total_);

    concentration_aer_[i] = concentration_aer_.back();
    concentration_aer_.pop_back();

    Nspecies_--;
  }


  // Extract.
  void ClassChamproboisSpeciation::Extract(ClassChamproboisSpeciation &speciation) const
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info() << LogReset() << "Extract speciation \""
                         << speciation.name_ << "\" from \"" << name_ << "\"." << endl;
#endif

    for (int i = 0; i < speciation.Nspecies_; ++i)
      {
        const int index = GetSpeciesIndex(speciation.species_name_[i]);
        if (index >= 0)
          speciation.SetSpecies(i, is_semi_volatile_[index] ? concentration_gas_[index] : real(-1),
                                concentration_aer_[index]);

#ifdef AMC_WITH_LOGGER
        if (index >= 0)
          *AMCLogger::GetLog() << Fgreen() << Debug(3) << LogReset() << "\tFound species \""
                               << speciation.species_name_[i] << "\" at index " << index << endl;
        else
          *AMCLogger::GetLog() << Fred() << Debug(3) << LogReset() << "\tCould not find species \""
                               << speciation.species_name_[i] << "\", ignoring ..." << endl;
#endif
      }
  }


  // Explode.
  void ClassChamproboisSpeciation::Explode(const ClassChamproboisSpeciation &speciation)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << LogReset() << "Explode speciation \""
                         << name_ << "." <<  species_name_total_ << "\" from \""
                         << speciation.name_ << "." << speciation.species_name_total_ << "\"." << endl;
#endif

    int index(-1);
    for (int i = 0; i < Nspecies_; ++i)
      if (species_name_[i] == speciation.species_name_total_)
        {
          index = i;
          break;
        }

    if (index >= 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "Found total species \""
                             << speciation.species_name_total_ << "\" at index " << index
                             << " of speciation \"" << name_ << "." << species_name_total_ << "\"." << endl;
#endif

        // Check if same gas and aer concentrations.
        if (concentration_gas_[index] != speciation.concentration_gas_total_)
          throw AMC::Error("Gas concentration of total species \"" + speciation.species_name_total_ +
                           "\" differs between speciations.");

        if (concentration_aer_[index] != speciation.concentration_aer_total_)
          throw AMC::Error("Aer concentration of total species \"" + speciation.species_name_total_ +
                           "\" differs between speciations.");

        RemoveSpecies(index);
        for (int i = 0; i < speciation.Nspecies_; ++i)
          AddSpecies(speciation.species_name_[i],
                     speciation.is_semi_volatile_[i] ? speciation.concentration_gas_[i] : real(-1),
                     speciation.concentration_aer_[i], speciation.species_label_[i],
                     speciation.species_key_[i], speciation.factor_[i]);
      }
#ifdef AMC_WITH_LOGGER
    else
      *AMCLogger::GetLog() << Bred() << Warning() << LogReset() << "Could not find total species \""
                           << speciation.species_name_total_ << "\" of speciation \""
                           << speciation.name_ << "\" in species list of speciation \""
                           << name_ << "." << species_name_total_ << "\"." << endl;
#endif
  }


  // Add to source.
  void ClassChamproboisSpeciation::AddToSource(const vector<real> &diameter_bound, vector<real> &fraction_section,
                                               vector1s &species_name, vector1i &gas_index,
                                               vector1r &concentration_gas_mass, vector2r &fraction_aer_mass) const
  {
    // A few proxy.
    const int Nspecies = int(species_name.size());
    const int Nsection = int(diameter_bound.size()) - 1;

    // If fraction sections are not defined.
    for (int i = 0; i < Nsection; ++i)
      if (fraction_section[i] < real(0))
        {
          const real diameter_min = diameter_bound[i] > diameter_min_ ? diameter_bound[i] : diameter_min_;
          const real diameter_max = diameter_bound[i + 1] < diameter_max_ ? diameter_bound[i + 1] : diameter_max_;

          if (diameter_max > diameter_min)
            fraction_section[i] = log(diameter_max / diameter_min) / log(diameter_bound[i + 1] / diameter_bound[i]);
          else
            fraction_section[i] = real(0);
        }

    // Add species.
    for (int i = 0; i < Nspecies_; ++i)
      {
        // Check if species not already in source.
        for (int j = 0; j < Nspecies; j++)
          if (species_name[j] == species_name_[i])
            throw AMC::Error("Species \"" + species_name_[i] + "\" is already in source (at index " + to_str(j) + ").");

        species_name.push_back(species_name_[i]);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset() << "Adding species \""
                             << species_name_[i] << "\" to source at index " << Nspecies + i << endl;
#endif

        if (is_semi_volatile_[i])
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(3) << LogReset() << "\tis semivolatile"
                                 << ", concentration_gas = " << concentration_gas_[i] << endl;
#endif
            concentration_gas_mass.push_back(concentration_gas_[i]);
            gas_index.push_back(Nspecies + i);
          }
      }

    const real concentration_aer_total = ComputeConcentrationAerTotal();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << LogReset() << "concentration_aer_total = "
                         << concentration_aer_total << endl;
#endif

    // Insert aerosol speciation into source.
    for (int i = 0; i < Nsection; ++i)
      {
        real fraction_remaining = real(1);
        for (int j = 0; j < Nspecies; ++j)
          fraction_remaining -= fraction_aer_mass[i][j];
        CLIP(fraction_remaining);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(3) << LogReset() << "Adding aerosol mass to section " << i
                             << " with fraction " << fraction_section[i] * fraction_remaining << endl;
#endif

        if (fraction_section[i] > real(0))
          for (int j = 0; j < Nspecies_; ++j)
            fraction_aer_mass[i].push_back(fraction_section[i] * fraction_remaining *
                                           concentration_aer_[j] / concentration_aer_total);
        else
          for (int j = 0; j < Nspecies_; ++j)
            fraction_aer_mass[i].push_back(real(0));
      }
  }


  // Compute.
  real ClassChamproboisSpeciation::ComputeConcentrationAerTotal() const
  {
    real concentration_aer_total(real(0));

    for (int i = 0; i < Nspecies_; ++i)
      concentration_aer_total += concentration_aer_[i];

    return concentration_aer_total;
  }


  real ClassChamproboisSpeciation::ComputeConcentrationGasTotal() const
  {
    real concentration_gas_total(real(0));

    for (int i = 0; i < Nspecies_; ++i)
      concentration_gas_total += concentration_gas_[i];

    return concentration_gas_total;
  }


  // Dump.
  void ClassChamproboisSpeciation::Dump(map<string, int> &argument) const
  {
    const int width = (argument.find("width") != argument.end()) ? argument["width"] : 15;
    const int precision = (argument.find("precision") != argument.end()) ? argument["precision"] : 6;
    const bool percentage_total = (argument.find("ptotal") != argument.end()) ? (argument["ptotal"] == 1) : false;
    const bool percentage_phase = (argument.find("pphase") != argument.end()) ? (argument["pphase"] == 1) : false;

    cout << string(20, '=') << " Speciation \"" << name_ << "\" " << string(20, '=') << endl;
    cout << "Number of species = " << Nspecies_ << endl;
    cout << "Diameter (min, max) = (" << diameter_min_ << ", " << diameter_max_ << ")" << endl;

    const real concentration_gas_total = ComputeConcentrationGasTotal();
    const real concentration_aer_total = ComputeConcentrationAerTotal();

    if (percentage_total)
      cout << "Display species fractions out of total concentration." << endl;
    else if (percentage_phase)
      cout << "Display phase fractions out of total (gas+aer) for each species." << endl;
    else
      cout << "Display concentrations in µg.m^{-3}." << endl;

    for (int i = 0; i < Nspecies_; ++i)
      {
        real value_gas(real(-1)), value_aer(real(-1));
        if (percentage_total)
          {
            if (concentration_gas_total > real(0))
              value_gas = concentration_gas_[i] / concentration_gas_total;

            if (concentration_aer_total > real(0))
              value_aer = concentration_aer_[i] / concentration_aer_total;
          }
        else if (percentage_phase)
          {
            const real value_total = concentration_gas_[i] + concentration_aer_[i];
            if (value_total > real(0))
              {
                value_gas = concentration_gas_[i] / value_total;
                value_aer = concentration_aer_[i] / value_total;
              }
          }
        else
          {
            value_gas = concentration_gas_[i];
            value_aer = concentration_aer_[i];
          }

        cout << setw(5) << right << i << " " << setw(30) << left << species_label_[i]
             << setw(width) << setprecision(precision) << left << value_aer
             << setw(width) << setprecision(precision) << left << value_gas
             << setw(5) << right << (is_semi_volatile_[i] ? "yes" : "no") << endl;
      }

    if (percentage_total)
      cout << setw(36) << left << "Total computed concentration"
           << setw(width) << setprecision(precision) << left << concentration_aer_total
           << setw(width) << setprecision(precision) << left << concentration_gas_total << endl;
    else
      cout << setw(36) << left << "Total measured concentration"
           << setw(width) << setprecision(precision) << left << concentration_aer_total_
           << setw(width) << setprecision(precision) << left << concentration_gas_total_ << endl;
  }


  // Complete.
  void ClassChamproboisSpeciation::Complete(const real temperature, const vector<string> &species_name)
  {
    const int Nspecies = int(species_name.size());

    if (Nspecies != 2)
      throw AMC::Error("For the moment, this algorithm is limited to 2 species.");

    real concentration_gas_remaining(concentration_gas_total_),
      concentration_aer_remaining(concentration_aer_total_);
    for (int i = 0; i < Nspecies_; ++i)
      {
        concentration_gas_remaining -= concentration_gas_[i];
        concentration_aer_remaining -= concentration_aer_[i];
        CLIP(concentration_gas_remaining);
        CLIP(concentration_aer_remaining);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "concentration_gas_remaining = "
                         << concentration_gas_remaining << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "concentration_aer_remaining = "
                         << concentration_aer_remaining << endl;

#endif

    if (concentration_gas_remaining <= real(0) && concentration_aer_remaining <= real(0))
      throw AMC::Error("Remaining gas and aerosol concentration <= 0, nothing to repair.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Complete speciation \"" << name_
                         << "\" with AMC species " << species_name << endl;
#endif


    map<string, real> variable = {{"temperature", temperature}};
    AMC::ClassMeteorologicalData::UpdateMeteo(variable);
    AMC::ClassMeteorologicalData::UpdateCondensation();

    vector1r saturation_vapor_concentration(Nspecies), molar_mass(Nspecies);
    vector1s species_label(Nspecies);

    for (int i = 0; i < Nspecies; ++i)
      {
        const int amc_index = AMC::ClassSpecies::GetIndex(species_name[i]);
        if (amc_index < 0)
          throw AMC::Error("Unable to find species \"" + species_name[i] + "\" in AMC model list.");

        molar_mass[i] = AMC::ClassSpecies::GetMolarMass(amc_index);
        species_label[i] = AMC::ClassSpecies::GetLongName(amc_index);
        saturation_vapor_concentration[i] = AMC::ClassMeteorologicalData::
          GetVariable1d("saturation vapor concentration", AMC::ClassSpecies::GetIndexGas(amc_index));
  }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(2) << LogReset() << "saturation_vapor_concentration = "
                         << saturation_vapor_concentration << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << LogReset() << "molar_mass = " << molar_mass << endl;
#endif

    real concentration_mole(real(0));
    for (int i = 0; i < Nspecies_; ++i)
      {
        const int amc_index = AMC::ClassSpecies::GetIndex(species_name_[i]);
        if (amc_index < 0)
          throw AMC::Error("Unable to find species \"" + species_name_[i] + "\" in AMC model list.");

        concentration_mole += concentration_aer_[i] / AMC::ClassSpecies::GetMolarMass(amc_index);
      }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SPECIATION
    CBUG << "concentration_mole = " << concentration_mole << endl;
#endif

    // Solve linear system.
    vector1r A = {(saturation_vapor_concentration[0] - concentration_gas_remaining) / molar_mass[0],
                  (saturation_vapor_concentration[1] - concentration_gas_remaining) / molar_mass[1]};

    const real concentration_ratio_remaining = concentration_gas_remaining / concentration_aer_remaining;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SPECIATION
    CBUG << "concentration_ratio_remaining = " << concentration_ratio_remaining << endl;
#endif

    vector1r x(2, real(-1)), y(2, real(-1));
    if (A[0] != A[1])
      {
        x[0] = (concentration_mole * concentration_ratio_remaining - A[1]) / A[0] - A[1];
        x[1] = real(1) - x[0];
        concentration_mole += (x[0] / molar_mass[0] + x[1] / molar_mass[1]) * concentration_aer_remaining;
        y[0] = saturation_vapor_concentration[0] * x[0] / (concentration_ratio_remaining * molar_mass[0] * concentration_mole);
        y[1] = real(1) - y[0];
      }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SPECIATION
    CBUG << "x = " << x << ", y = " << y << endl;
#endif

    if (x[0] < real(0) || x[0] > real(1) || x[1] < real(0) || x[1] > real(1))
      { 
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Logger::Error() << LogReset()
                             << "Unable to find suitable fractions with first method try safe one." << endl;
#endif

        vector1r concentration_ratio(2, real(1));
        if (concentration_ratio_remaining > real(0))
          {
            const real fraction_separator = real(1) / (real(1) + concentration_ratio_remaining);
            if (saturation_vapor_concentration[0] < saturation_vapor_concentration[1])
              {
                concentration_ratio[1] = real(0.5) * fraction_separator;
                concentration_ratio[0] = (real(1) + fraction_separator) * real(0.5);
              }
            else
              {
                concentration_ratio[0] = real(0.5) * fraction_separator;
                concentration_ratio[1] = (real(1) + fraction_separator) * real(0.5);
              }

            concentration_ratio[0] = (real(1) - concentration_ratio[0]) / concentration_ratio[0];
            concentration_ratio[1] = (real(1) - concentration_ratio[1]) / concentration_ratio[1];

            x[0] = (concentration_ratio_remaining - concentration_ratio[1])
              / (concentration_ratio[0] - concentration_ratio[1]);
            x[1] = real(1) - x[0];
            y[0] = x[0] * concentration_ratio[0] / concentration_ratio_remaining;
            y[1] = real(1) - y[0];
          }
        else
          {
            x[0] = saturation_vapor_concentration[1]
              / (saturation_vapor_concentration[0] + saturation_vapor_concentration[1]);
            x[1] = real(1) - x[0];
            y[0] = real(0);
            y[1] = real(0);
          }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Debug(2) << LogReset() << "x = " << x << ", y = " << y << endl;
#endif

    // Now, we add species to speciation.
    for (int i = 0; i < Nspecies; ++i)
      AddSpecies(species_name[i], concentration_gas_remaining * y[i],
                 concentration_aer_remaining * x[i], species_label[i]);
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SPECIATION_CXX
#endif
