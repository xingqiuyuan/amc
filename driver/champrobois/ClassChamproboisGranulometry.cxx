// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_GRANULOMETRY_CXX

#include "ClassChamproboisGranulometry.hxx"


namespace Driver
{
  // Filter concentration number.
  void ClassChamproboisGranulometry::filter_concentration_number()
  {
    for (int i = 0; i < Nsection_; ++i)
      {
        vector1r x(Nt_, real(0)), y(Nt_, real(0));

        int Nt(-1), tmin(-1);
        for (int h = 0; h < Nt_; ++h)
          if (concentration_aer_number_(h, i) > real(0))
            {
              if (tmin < 0) tmin = h;
              x[++Nt] = time_[h];
              y[Nt] = concentration_aer_number_(h, i);
            }

        int tmax(-1);
        for (int h = Nt_ - 1; h >= 0; --h)
          if (concentration_aer_number_(h, i) > real(0))
            {
              tmax = h;
              break;
            }

        Nt++;

        if (Nt > 0 && Nt < Nt_)
          {
            vector1r y2(Nt, real(0));
            AMC::compute_cubic_spline<real>(Nt, x.data(), y.data(), y2.data());

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "Filtering number concentration"
                                 << " of section " << i << " from times ]" << tmin << ", " << tmax << "[" << endl;
#endif

            for (int h = tmin; h < tmax; ++h)
              if (concentration_aer_number_(h, i) <= real(0))
                concentration_aer_number_(h, i) = AMC::apply_cubic_spline<real>(Nt, x.data(), y.data(), y2.data(), time_[h]);

            for (int h = tmin; h < tmax; ++h)
              if (concentration_aer_number_(h, i) <= real(0))
                {
                  int hmin(h);
                  while (hmin >= tmin)
                    if (concentration_aer_number_(hmin, i) > real(0))
                      break;
                    else
                      hmin--;

                  int hmax(h);
                  while (hmax <= tmax)
                    if (concentration_aer_number_(hmax, i) > real(0))
                      break;
                    else
                      hmax++;

                  if (hmin >= tmin && hmax <= tmax)
                    {
                      const real u = (time_[h] - time_[hmin]) / (time_[hmax] - time_[hmin]);
                      concentration_aer_number_(h, i) = (real(1) - u) * concentration_aer_number_(hmin, i)
                        + u * concentration_aer_number_(hmax, i);
                    }

                  if (concentration_aer_number_(h, i) <= real(0))
                    throw AMC::Error("At time " + to_str(h) + " and section " + to_str(i) +
                                     " number concentration is <= 0 : tmin = " + to_str(tmin) +
                                     ", tmax = " + to_str(tmax) + ", hmin = " + to_str(hmin) +
                                     ", hmax = " + to_str(hmax) + ", h = " + to_str(h) + ".");
                }
          }
      }
  }


  // Compute single mass mean.
  void ClassChamproboisGranulometry::compute_mass_mean()
  {
    mass_mean_.assign(Nsection_, real(0));
    for (int i = 0; i < Nsection_; ++i)
      mass_mean_[i] = AMC_PI6 * density_fixed_ * diameter_mean_[i] * diameter_mean_[i] * diameter_mean_[i];
  }


  // Compute total mass concentration.
  void ClassChamproboisGranulometry::compute_concentration_mass_total()
  {
    concentration_aer_mass_total_.resize(Nt_, Nsection_);
    concentration_aer_mass_total_ = real(0);

    concentration_aer_mass_total_fraction_.assign(Nt_, real(0));

    real mass_total(real(0));
    for (int h = 0; h < Nt_; ++h)
      {
        for (int i = 0; i < Nsection_; ++i)
          {
            concentration_aer_mass_total_(h, i) = mass_mean_[i] * concentration_aer_number_(h, i);
            concentration_aer_mass_total_fraction_[h] += concentration_aer_mass_total_(h, i);
          }

        mass_total += concentration_aer_mass_total_fraction_[h];
      }

    const real mass_total_inv = real(1) / mass_total;
    for (int h = 0; h < Nt_; ++h)
      concentration_aer_mass_total_fraction_[h] *= mass_total_inv;
  }


  // Constructor.
  ClassChamproboisGranulometry::ClassChamproboisGranulometry(const string name)
    : name_(name), Nsection_(0), Nbound_(0), Nt_(0), key_(name + ".granulometry"),
      date_begin_(real(0)), density_fixed_(AMC_PARTICLE_DENSITY_DEFAULT),
      instrument_("")
  {
    Ops::Ops ops(ClassChamproboisBase::configuration_file_);

    ops.SetPrefix(ClassChamproboisBase::prefix_ + ".source.granulometry.");

    density_fixed_ = ops.Get<real>("density_fixed", "", density_fixed_);
    key_ = ops.Get<string>("key", "", key_);
    instrument_ = ops.Get<string>("instrument", "", instrument_);

    ops.Close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Instantiate granulometry for source \""
                         << name_ << "\"." << endl;
#endif

    return;
  }


  // Destructor.
  ClassChamproboisGranulometry::~ClassChamproboisGranulometry()
  {
    return;
  }


  // Get methods.
  int ClassChamproboisGranulometry::GetNsection() const
  {
    return Nsection_;
  }


  real ClassChamproboisGranulometry::GetDensityFixed() const
  {
    return density_fixed_;
  }


  real ClassChamproboisGranulometry::GetDateBegin() const
  {
    return date_begin_;
  }


  string ClassChamproboisGranulometry::GetName() const
  {
    return name_;
  }


  string ClassChamproboisGranulometry::GetKey() const
  {
    return key_;
  }


  string ClassChamproboisGranulometry::GetInstrument() const
  {
    return instrument_;
  }


  real ClassChamproboisGranulometry::GetTimeOut() const
  {
    return time_.back();
  }


  void ClassChamproboisGranulometry::GetTime(vector<real> &time) const
  {
    time = time_;
  }


  vector<real> ClassChamproboisGranulometry::GetDiameterBound() const
  {
    return diameter_bound_;
  }


  vector<real> ClassChamproboisGranulometry::GetDiameterMean() const
  {
    return diameter_mean_;
  }


  void ClassChamproboisGranulometry::GetConcentrationAerNumber(vector<vector<real> > &concentration_aer_number) const
  {
    concentration_aer_number.assign(Nt_, vector1r(Nsection_, real(0)));
    for (int i = 0; i < Nt_; ++i)
      for (int j = 0; j < Nsection_; ++j)
        concentration_aer_number[i][j] = concentration_aer_number_(i, j);
  }


  void ClassChamproboisGranulometry::GetConcentrationAerMassTotal(vector<vector<real> > &concentration_aer_mass_total) const
  {
    concentration_aer_mass_total.assign(Nt_, vector1r(Nsection_, real(0)));
    for (int i = 0; i < Nt_; ++i)
      for (int j = 0; j < Nsection_; ++j)
        concentration_aer_mass_total[i][j] = concentration_aer_mass_total_(i, j);
  }


  void ClassChamproboisGranulometry::GetConcentrationAerMassTotalFraction(vector<real> &concentration_aer_mass_total_fraction) const
  {
    concentration_aer_mass_total_fraction = concentration_aer_mass_total_fraction_;
  }


  // Set methods.
  void ClassChamproboisGranulometry::SetDateBegin(const real date_begin)
  {
    date_begin_ = date_begin;
  }


  void ClassChamproboisGranulometry::SetKey(const string &key)
  {
    key_ = key;
  }


  void ClassChamproboisGranulometry::SetInstrument(const string &instrument)
  {
    instrument_ = instrument;
  }


  void ClassChamproboisGranulometry::SetTime(const vector<real> &time)
  {
    time_ = time;
    Nt_ = int(time_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "Nt = " << Nt_ << endl;
#endif
  }


  void ClassChamproboisGranulometry::SetParticleDensity(const real &density_fixed)
  {
    density_fixed_ = density_fixed * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;
  }


  void ClassChamproboisGranulometry::SetParticleDiameter(const vector<real> &diameter_bound, const vector<real> &diameter_mean)
  {
    diameter_mean_ = diameter_mean;
    diameter_bound_ = diameter_bound;

    Nbound_ = int(diameter_bound_.size());
    Nsection_ = int(diameter_mean_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "Nsection = " << Nsection_ << endl;
#endif

    if (Nbound_ != (Nsection_ + 1))
      throw AMC::Error("Inconsistent Nbound (" + to_str(Nbound_) + ") and Nsection (" + to_str(Nsection_) + ").");
  }


  void ClassChamproboisGranulometry::SetParticleNumberConcentration(const vector<vector<real> > &concentration_aer_number)
  {
    concentration_aer_number_.resize(Nt_, Nsection_);
    concentration_aer_number_ = real(0);

    if (int(concentration_aer_number.size()) != Nt_)
      throw AMC::Error("Number of time steps in number concentration (" + to_str(concentration_aer_number.size())
                       + ") is not equal to Nt (" + to_str(Nt_) + ").");

    for (int i = 0; i < Nt_; ++i)
      {
        if (int(concentration_aer_number[i].size()) != Nsection_)
          throw AMC::Error("Number of sections in number concentration (" + to_str(concentration_aer_number[i].size())
                           + ") at time step " + to_str(i) + " is not equal to Nsection (" + to_str(Nsection_) + ").");

        for (int j = 0; j < Nsection_; ++j)
          concentration_aer_number_(i, j) = concentration_aer_number[i][j];
      }

    // The ELPI sometimes gives some unusual values.
    filter_concentration_number();

    for (int i = 0; i < Nsection_; ++i)
      for (int h = 0; h < Nt_; ++h)
        if (concentration_aer_number_(h, i) < real(0))
          throw AMC::Error("At time " + to_str(h) + " and section " + to_str(i) + " number concentration is < 0.");

    compute_mass_mean();
    compute_concentration_mass_total();
  }


  // Compute.
  real ClassChamproboisGranulometry::ComputeMassTotalAerFromGranulometry(int Nsection) const
  {
    Nsection = Nsection < 0 ? Nsection_ : Nsection;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "Nsection = " << Nsection << endl;
#endif
 
    if (Nsection > Nsection_)
      throw AMC::Error("Nsection provided (" + to_str(Nsection) +
                       ") exceeds maximum number of sections (" + to_str(Nsection_) + ").");

    real mass_total(real(0));

    for (int h = 0; h < Nt_; ++h)
      for (int i = 0; i < Nsection; ++i)
        mass_total += concentration_aer_mass_total_(h, i);

    return mass_total / real(Nt_);
  }


  // Dump.
  void ClassChamproboisGranulometry::Dump(const int width, const int precision) const
  {
    cout << string(20, '=') << " Sections (" << Nsection_ << ") " << string(20, '=') << endl;
    for (int i = 0; i < Nsection_; ++i)
      cout << setw(5) << right << i << "   "
           << setw(10) << setprecision(6) << left << diameter_bound_[i]
           << setw(15) << setprecision(6) << left << diameter_mean_[i]
           << setw(10) << setprecision(6) << left << diameter_bound_[i + 1] << endl;

    cout << string(20, '=') << " Number concentration / mass fraction " << string(20, '=') << endl;

    time_t date_begin_t = time_t(date_begin_);
    struct tm *date_begin_tm = localtime(&date_begin_t);
    char date_begin_c[20];
    strftime(date_begin_c, 20, "%Y-%m-%d %H:%M:%S", date_begin_tm);

    cout << "Beginning date : " << date_begin_c << endl;

    // 1.e-6 ? : more convenient to present concentrations in #/cm3.
    for (int i = 0; i < Nt_; ++i)
      {
        cout << setw(5) << right << i << setw(8) << right << time_[i] << "   ";
        for (int j = 0; j < Nsection_; ++j)
          cout << setw(width) << setprecision(precision) << left << concentration_aer_number_(i, j) * real(1.e-6);
        cout << " " << setw(width) << setprecision(precision) << left
             << concentration_aer_mass_total_fraction_[i] * real(100) << endl;
      }
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_GRANULOMETRY_CXX
#endif
