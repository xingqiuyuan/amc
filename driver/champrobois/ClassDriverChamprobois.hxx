// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_CHAMPROBOIS_HXX

#define DRIVER_CHAMPROBOIS_SPEED_DEFAULT                       1.
#define DRIVER_CHAMPROBOIS_HOST_NAME                           "localhost"
#define DRIVER_CHAMPROBOIS_PORT_NUMBER                         11830
#define DRIVER_CHAMPROBOIS_HEAT_SPECIFIC_ISOBARIC_AVERAGE      2.0       // Average specific isobaric heat in J.g^{-1}.K^{-1}
#define DRIVER_CHAMPROBOIS_THERMAL_CONDUCTANCE_AVERAGE         0.5       // Average thermal conductance in J.s^{-1}.K^{-1}.
// Threshold mass total concentration in µg.m^{-3}, it corresponds
// roughly to the measurement threshold detection, a few nanograms.
#define DRIVER_CHAMPROBOIS_CONCENTRATION_TOTAL_THRESHOLD       1.e-5
#define DRIVER_CHAMPROBOIS_DT_RATIO_DEFAULT                    0.8

namespace Driver
{
  /*! 
   * \class ClassDriverChamprobois
   */
  class ClassDriverChamprobois : protected ClassChamproboisBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  private:

    /*!< Prefix of drivers.*/
    static string prefix_;

    /*!< MPI configuration.*/
    vector1i z_offset_rank_, r_offset_rank_;
    vector1i Nz_rank_, Nr_rank_;

    /*!< Display every given number of seconds.*/
    real display_frequency_;

    /*!< Save every given number of seconds.*/
    real save_frequency_;

#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    /*!< Use socket.*/
    bool use_socket_;

    /*!< Host name.*/
    string host_name_;

    /*!< Port number.*/
    int port_number_;
#endif

    /*!< Perform simulation with wall loss.*/
    bool with_loss_;

    /*!< Perform simulation with diffusion.*/
    bool with_diffusion_;

    /*!< Perform simulation with molecular diffusion.*/
    bool with_diffusion_molecular_;

    /*!< Number of gas plus volatile species.*/
    int NgasVolatile_;

    /*!< Size of concentrations.*/
    int Nc_;

    /*!< Number of Z-axis sections.*/
    int Nz_;

    /*!< Number of radial sections.*/
    int Nr_;

    /*!< Number of sinks.*/
    int Nsink_;

    /*!< Current time.*/
    real t_;

    /*!< Time step.*/
    real dt_;

    /*!< Fixed time step.*/
    real dt_fixed_;

    /*!< Max time step.*/
    real dt_max_;

    /*!< Time step ratio.*/
    real dt_ratio_;

    /*!< Z-axis step.*/
    real dz_;

    /*!< Radial step.*/
    real dr_;

    /*!< Name of simulation.*/
    string name_;

    /*!< Key of simulation.*/
    string key_;

    /*!< Path.*/
    string path_;

    /*!< Pressure.*/
    real pressure_;

    /*!< Water vapor concentration from background air, not smoke.*/
    real concentration_water_background_;

    /*!< temperature from background air, not smoke.*/
    real temperature_background_;

    /*!< Pointer to source term.*/
    ClassChamproboisSource* source_ptr_;

    /*!< Pointer to sink terms.*/
    vector<ClassChamproboisSink* > sink_ptr_;

    /*!< Z position of sink in Chimney.*/
    vector1i sink_position_z_;

    /*!< Collision factor.*/
    vector1r collision_factor_;

    /*!< Molecular diameter.*/
    vector1r molecular_diameter_;

    /*!< Molar mass.*/
    vector1r molar_mass_;

    /*!< Radius.*/
    Array<real, 1> r_;

    /*!< Z-axis.*/
    Array<real, 1> z_;

    /*!< Air flow speed.*/
    Array<real, 1> speed_;

    /*!< Average thermal conductance.*/
    real thermal_conductance_average_;

    /*!< Average specific isobaric heat.*/
    real heat_specific_isobaric_average_;

    /*!< Temperature and enthalpy.*/
    Array<real, 2> temperature_;
    Array<real, 2> enthalpy_total_;

    /*!< Relative humidity.*/
    Array<real, 2> relative_humidity_;

    /*!< Scale factor for turbulent diffusion.*/
    real scale_factor_diffusion_turbulent_gas_;
    real scale_factor_diffusion_turbulent_particle_;

    /*!< Turbulent diffusion coefficient.*/
    real diffusivity_turbulent_gas_;
    real diffusivity_turbulent_particle_;

    /*!< Diffusion coefficient in air.*/
    Array<real, 3> diffusivity_gas_;
    Array<real, 3> diffusivity_particle_;
    Array<Array<real, 2>*, 1> diffusivity_;

    /*!< Particle diameters.*/
    Array<real, 3> diameter_;

    /*!< Loss coefficient.*/
    Array<real, 1> loss_;

    /*!< Concentration ranges.*/
    Range range_all_;
    Range range_z_;
    Range range_r_;
    Range range_number_;
    Range range_mass_aer_;
    Range range_mass_gas_;
    Range range_mass_vol_;
    Range range_mass_all_;

    /*!< Source ratio.*/
    Array<real, 1> ratio_source_;

    /*!< Background air ratio.*/
    Array<real, 1> ratio_background_;

    /*!< Concentrations : aerosol number + aerosol mass + gas + water vapor.*/
    Array<real, 3> concentration_;
    Array<real, 2> concentration_total_;

  public:

    /*!< Constructor.*/
    ClassDriverChamprobois(const string name, ClassChamproboisSource *source_ptr = NULL);

    /*!< Destructor.*/
    ~ClassDriverChamprobois();

    /*!< Get methods.*/
    int GetNc() const;
    int GetNr() const;
    int GetNz() const;
    int GetNsink() const;
    real GetPressure() const;
    string GetName() const;
    string GetPath() const;
    string GetKey() const;
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    string GetHostName() const;
    int GetPortNumber() const;
#endif
    ClassChamproboisSource* GetSourcePtr();

    /*!< Set methods.*/
    static void SetPrefix(const string prefix);
    void SetTemperature(const real temperature_celsius);
    void SetPressure(const real pressure);
    void SetConcentrationWater(real volume_humidity_percentage);
    void SetSpeed(const real volumic_flow);
    void SetTurbulentDiffusion();
    void SetPath(const string path);
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    void SetHostName(const string host_name);
    void SetPortNumber(const int port_number);
#endif

    /*!< Add one sink.*/
#ifndef SWIG
    void AddSink(ClassChamproboisSink *sink_ptr);
#endif
    void AddFilter(ClassChamproboisFilter *filter_ptr);
    void AddELPI(ClassChamproboisELPI *elpi_ptr);

    /*!< Run.*/
    template<class S, class R, class D, class C>
    void Run(real time_out = real(0), const bool passive = false);

    /*!< Init one step forward.*/
    void InitStep(const real &time_out);

    /*!< Forward.*/
#ifndef SWIG
    bool Forward(const Array<real, 1> &source, const real &temperature);
#endif

    bool Forward(const vector<real> &source, const real &temperature);

    template<class S, class R, class D, class C>
    void Forward();


#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
    /*!< Send.*/
    void Send(const real &time_out, string host_name = "", int port_number = 0) const;
#endif

    /*!< Write.*/
    void Write(string path = "") const;

    /*!< Compute diameter.*/
    void ComputeDiameter();
  };
}

#define AMC_FILE_CLASS_DRIVER_CHAMPROBOIS_HXX
#endif
