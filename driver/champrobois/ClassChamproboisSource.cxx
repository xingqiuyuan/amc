// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SOURCE_CXX

#include "ClassChamproboisSource.hxx"


namespace Driver
{
  // Compute source.
  inline void ClassChamproboisSource::compute_source(const int h, const real ratio, Array<real, 1> &source) const
  {
    int k(-1);

    for (int i = 0; i < this->Nsection_; ++i)
      source(++k) += this->concentration_aer_number_(h, i) * ratio;

    for (int i = 0; i < this->Nsection_; ++i)
      {
        const real concentration_mass = this->concentration_aer_mass_total_(h, i) * ratio;
        for (int j = 0; j < Nspecies_; ++j)
          source(++k) += fraction_aer_mass_[i][j] * concentration_mass;
      }

    const real ratio_gas = ratio * this->concentration_aer_mass_total_fraction_[h];
    for (int i = 0; i < int(gas_index_.size()); ++i)
      source(++k) += concentration_gas_mass_[i] * ratio_gas;

    source(++k) += concentration_water_ * ratio_gas;
  }


  // Constructor.
  ClassChamproboisSource::ClassChamproboisSource(const string name)
    : ClassChamproboisGranulometry(name), Ns_(0), Nspecies_(0), Nvolatile_(0),
      Ngas_(0), comment_(""), Nspeciation_(0), is_speciation_complete_(false),
      temperature_(DRIVER_TEMPERATURE_REFERENCE), concentration_water_(real(0))
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset() << "Instantiate Champrobois source \""
                         << this->name_ << "\"." << endl;
#endif

    Ops::Ops ops(ClassChamproboisBase::configuration_file_);

    ops.SetPrefix(ClassChamproboisBase::prefix_ + ".source." + this->name_ + ".");

    SetTemperature(ops.Get<real>("temperature_celsius", "", temperature_));

    ops.Close();

    return;
  }


  // Destructor.
  ClassChamproboisSource::~ClassChamproboisSource()
  {
    return;
  }


  // Is speciation complete ?
  bool ClassChamproboisSource::IsSpeciationComplete() const
  {
    return is_speciation_complete_;
  }


  // Get methods.
  int ClassChamproboisSource::GetNs() const
  {
    return Ns_;
  }


  int ClassChamproboisSource::GetNspeciation() const
  {
    return Nspeciation_;
  }


  int ClassChamproboisSource::GetNspecies() const
  {
    return Nspecies_;
  }


  int ClassChamproboisSource::GetNgas() const
  {
    return Ngas_;
  }


  int ClassChamproboisSource::GetNvolatile() const
  {
    return Nvolatile_;
  }


  string ClassChamproboisSource::GetSpeciesName(const int i) const
  {
    return species_name_[i];
  }


  string ClassChamproboisSource::GetSpeciesLabel(const int i) const
  {
    for (int j = 0 ; j < Nspeciation_; ++j)
      if (speciation_ptr_[j] != NULL)
        {
          const int k = speciation_ptr_[j]->GetSpeciesIndex(species_name_[i]);
          if (k >= 0)
            return speciation_ptr_[j]->GetSpeciesLabel(k);
        }

    return "";
  }


  vector<string> ClassChamproboisSource::GetSpeciesNameList() const
  {
    return species_name_;
  }


  string ClassChamproboisSource::GetComment() const
  {
    return comment_;
  }


  vector<int> ClassChamproboisSource::GetGasIndex() const
  {
    return gas_index_;
  }


  void ClassChamproboisSource::GetFractionAerMass(vector<vector<real> > &fraction_aer_mass) const
  {
    fraction_aer_mass = fraction_aer_mass_;
  }


  void ClassChamproboisSource::GetConcentrationGasMass(vector<real> &concentration_gas_mass) const
  {
    concentration_gas_mass = concentration_gas_mass_;
  }


  real ClassChamproboisSource::GetTemperature(const real t) const
  {
    return temperature_;
  }


  real ClassChamproboisSource::GetConcentrationWater() const
  {
    return concentration_water_;
  }


  ClassChamproboisSpeciation* ClassChamproboisSource::GetSpeciationPtr(const int i)
  {
    return speciation_ptr_[i];
  }


  // Add volatile species.
  void ClassChamproboisSource::AddVolatile(const string name, const real concentration_mass)
  {
    gas_index_.push_back(int(species_name_.size()));
    species_name_.push_back(name);
    concentration_gas_mass_.push_back(concentration_mass);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Adding (pure) gas \"" << name
                         << " with concentration " << concentration_mass
                         << " µg.m^{-3}, at index " << gas_index_.back() << endl;
#endif

    // Do not increment the number of species, just the gas one.
    Nvolatile_++;

    // Update new source size.
    Ns_ = this->Nsection_ * (1 + Nspecies_) + Ngas_ + Nvolatile_;
  }


  // Add speciation.
  void ClassChamproboisSource::AddSpeciation(const ClassChamproboisSpeciation &speciation, vector<real> fraction_section)
  {
    // Need to add speciations before purely volatile species.
    if (Nvolatile_ > 0)
      throw AMC::Error("Need to add all speciations before adding purely volatile species.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(2) << LogReset() << "Adding speciation \""
                         << speciation.GetName() << "\" with \"" << speciation.GetSpeciesNameTotal()
                         << "\" to source \"" << this->name_ << "\"" << endl;
#endif

    if (fraction_section.empty())
      fraction_section.assign(this->Nsection_, real(-1));

    if (int(fraction_section.size()) != this->Nsection_)
      throw AMC::Error("Given number of fraction sections (" + to_str(fraction_section.size()) +
                       ") differs from source number of sections (" + to_str(this->Nsection_) + ").");

    speciation.AddToSource(this->diameter_bound_, fraction_section,
                           species_name_, gas_index_, concentration_gas_mass_, fraction_aer_mass_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << LogReset() << "Speciation distributed with fraction sections "
                         << fraction_section << endl;
#endif

    Nspecies_ = int(species_name_.size());
    Ngas_ = int(gas_index_.size());
    Ns_ = this->Nsection_ * (1 + Nspecies_) + Ngas_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "Nspecies = " << Nspecies_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "Ngas = " << Ngas_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "Ns = " << Ns_ << endl;
#endif

    Nspeciation_++;
    speciation_ptr_.push_back(const_cast<ClassChamproboisSpeciation* >(&speciation));

    is_speciation_complete_ = true;
    for (int i = 0; i < this->Nsection_; ++i)
      {
        real fraction_sum(real(0));
        for (int j = 0; j < Nspecies_; ++j)
          fraction_sum += fraction_aer_mass_[i][j];

        if (abs(fraction_sum - real(1)) > real(DRIVER_CHAMPROBOIS_SOURCE_EPSILON))
          {
            is_speciation_complete_ = false;
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bmagenta() << Warning(1) << LogReset() << "Speciation for size section " << i
                             << " is not complete (" << setprecision(4) << fraction_sum * real(100) << "%)." << endl;
#endif
          }
      }
  }


  // Set methods.
  void ClassChamproboisSource::SetComment(const string comment)
  {
    comment_ = comment;
  }


  void ClassChamproboisSource::SetFractionAerMass()
  {
    fraction_aer_mass_.clear();
    fraction_aer_mass_.resize(this->Nsection_);
  }


  void ClassChamproboisSource::SetTemperature(const real temperature_celsius)
  {
    temperature_ = temperature_celsius + DRIVER_TEMPERATURE_ZERO_DEGREE_CELSIUS;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(3) << LogReset() << "Setting source temperature to "
                         << temperature_ << " Kelvin." << endl;
#endif
  }


  void ClassChamproboisSource::SetConcentrationWater(real volume_humidity_percentage, const real pressure_pascal)
  {
    volume_humidity_percentage *= real(0.01);
    const real specific_humidity = volume_humidity_percentage
      / (real(DRIVER_AIR_MOLAR_MASS / DRIVER_WATER_MOLAR_MASS) + volume_humidity_percentage);

    /*
      SH = m_w / (m_a + m_w) = (n_w * M_w) / (n_a * M_a + n_w * M_w)
      = n_w / (n_a * R + n_w) -- R = M_a / M_w
      = V_w / (V_a * R + V_w) = x * V_a / (V_a * R + x * V_a) = x / (R + x) -- x = V_w / V_a
    */

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "specific_humidity = " << specific_humidity << endl;
#endif

    concentration_water_ = ClassParameterizationPhysics::
      ComputeWaterVaporDensityFromSpecificHumidity(temperature_, pressure_pascal, specific_humidity) * this->Nt_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(3) << LogReset() << "Setting source total water concentration "
                         << concentration_water_ << " µg.m^{-3}" << endl;
#endif
  }


  // Compute.
  void ClassChamproboisSource::Compute(const real &t, const real &time_step, vector<real> &source) const
  {
    source.assign(Ns_ + 1, real(0));
    Array<real, 1> source_arr(source.data(), shape(Ns_ + 1), neverDeleteData);
    Compute(t, time_step, source_arr);
  }


  void ClassChamproboisSource::Compute(const real &t, const real &time_step, Array<real, 1> &source) const
  {
    // Init source vector.
    source = real(0);

    // Get min time index.
    const real &tmin = t;
    int hmin(0);
    while (this->time_[hmin] <= t)
      if (hmin == this->Nt_ - 1)
        break;
      else
        ++hmin;

    // Get max time index.
    const real tmax = t + time_step;
    int hmax(hmin);
    while (this->time_[hmax] < tmax)
      if (hmax == this->Nt_ - 1)
        break;
      else
        ++hmax;
          
    --hmin;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SOURCE
    CBUG << "hmin = " << hmin << endl;
    CBUG << "hmax = " << hmax << endl;
#endif

    if (hmin == 0)
      compute_source(0, real(1), source);

    for (int h = hmin; h < hmax; ++h)
      {
        const real tmin_h = (this->time_[h] > tmin) ? this->time_[h] : tmin;
        const real tmax_h = (this->time_[h + 1] < tmax) ? this->time_[h + 1] : tmax;
        const real ratio_h = (tmax_h > tmin_h ? tmax_h - tmin_h : real(0)) / (this->time_[h + 1] - this->time_[h]);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SOURCE
        CBUG << "h = " << h << endl;
        CBUG << "ratio_h = " << ratio_h << endl;
#endif
        compute_source(h + 1, ratio_h, source);
      }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SOURCE
    int h(this->Nsection_ - 1);
    for (int i = 0; i < this->Nsection_; ++i)
      {
        real mass_total(real(0));
        for (int j = 0; j < Nspecies_; ++j)
          mass_total += source(++h);

        if (source(i) > AMC_NUMBER_CONCENTRATION_MINIMUM && mass_total > real(0))
          {
            const real diameter_mean = pow(AMC_INV_PI6 * mass_total / (source(i) * this->density_fixed_), AMC_FRAC3);

            if (diameter_mean < this->diameter_bound_[i] || diameter_mean >= this->diameter_bound_[i + 1])
              throw AMC::Error("Mean diameter (= " + to_str(diameter_mean) + ") of source section number " +
                               to_str(i) + " is out of bounds [" + to_str(this->diameter_bound_[i]) +
                               ", " + to_str(this->diameter_bound_[i + 1]) + "[");
          }
      }
#endif
  }


  // Order.
  void ClassChamproboisSource::Order(const vector<string> &species_name, const vector<int> &semivolatile)
  {
    const int Nspecies = int(species_name.size());
    if (Nspecies != Nspecies_)
      throw AMC::Error("Number of species given in argument (" + to_str(Nspecies) +
                       ") differs from source current number (" + to_str(Nspecies_) + ").");

    const int Ngas = int(semivolatile.size());
    if (Ngas != Ngas_)
      throw AMC::Error("Number of gas species given in argument (" + to_str(Ngas) +
                       ") differs from source current number (" + to_str(Ngas_) + ").");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Order species of source \""
                         << this->name_ << "\" with " << species_name << "." << endl;
#endif

    // Search for index species in list.
    vector1i index(Nspecies, -1);
    for (int i = 0; i < Nspecies; ++i)
      {
        for (int j = 0; j < Nspecies_; ++j)
          if (species_name[i] == species_name_[j])
            {
              index[i] = j;
              break;
            }

        if (index[i] < 0)
          throw AMC::Error("Could not find species \"" + species_name[i] + "\" in source species list.");
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "index = " << index << endl;
#endif

    // Aerosol fractions.
    vector2r fraction_aer_mass(this->Nsection_, vector1r(Nspecies_, real(0)));
    for (int i = 0; i < this->Nsection_; ++i)
      for (int j = 0; j < Nspecies; ++j)
        fraction_aer_mass[i][j] = fraction_aer_mass_[i][index[j]];

    fraction_aer_mass_ = fraction_aer_mass;

    // Gas mass concentration.
    vector1i gas_index;
    vector1r concentration_gas_mass;
    for (int i = 0; i < Ngas; ++i)
      {
        const int j = semivolatile[i];

        for (int k = 0; k < Ngas_; ++k)
          if (gas_index_[k] == index[j])
            {
              gas_index.push_back(j); // gas_index = semivolatile
              concentration_gas_mass.push_back(concentration_gas_mass_[k]);
              break;
            }
      }

    // Purely volatile species, if any.
    for (int i = 0; i < Nvolatile_; ++i)
      {
        const int j = Ngas + i;
        gas_index.push_back(gas_index_[j]);
        concentration_gas_mass.push_back(concentration_gas_mass_[j]);
      }

    gas_index_.assign(gas_index.begin(), gas_index.end());
    concentration_gas_mass_.assign(concentration_gas_mass.begin(), concentration_gas_mass.end());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "gas_index = " << gas_index << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "concentration_gas_mass = " << concentration_gas_mass_ << endl;
#endif

    // Finally change the list of species. That of volatile ones is not changed.
    for (int i = 0; i < Nspecies_; ++i)
      species_name_[i] = species_name[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(3) << LogReset() << "species_name = " << species_name_ << endl;
#endif
  }


  void ClassChamproboisSource::OrderAMC()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Info(1) << LogReset() << "Order species of source \""
                         << this->name_ << "\" with AMC species list." << endl;
#endif

    ClassChamproboisSource::Order(AMC::ClassSpecies::GetNameList(), AMC::ClassSpecies::GetSemivolatile());
  }


  // Dump.
  void ClassChamproboisSource::Dump(const bool with_granulometry, const int width, const int precision) const
  {
    cout << string(30, '=') << " Source \"" << this->name_ << "\" " << string(30, '=') << endl;
    cout << "Comment : " << comment_ << endl;
    cout << "Temperature : " << temperature_ << " Kelvin" << endl;
    cout << "Concentration water : " << concentration_water_ << " µg.m^{-3}" << endl;
    if (with_granulometry) ClassChamproboisGranulometry::Dump(width, precision);

    cout << string(20, '=') << " Aerosol species " << string(20, '=') << endl;
    for (int i = 0; i < Nspecies_; ++i)
      {
        cout << setw(5) << right << i << " " << setw(30) << left << species_name_[i];
        for (int j = 0; j < this->Nsection_; ++j)
          cout << setw(width) << setprecision(precision) << left << fraction_aer_mass_[j][i];
        cout << endl;
      }

    cout << string(20, '=') << " Gas species (" << Ngas_ << "/" << Nspecies_ << ") " << string(20, '=') << endl;
    for (int i = 0; i < Ngas_; ++i)
      cout << setw(5) << right << i << " " << setw(30) << left << species_name_[gas_index_[i]]
           << setw(6) << left << gas_index_[i] << setw(width) << setprecision(precision)
           << left << concentration_gas_mass_[i] << endl;
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SOURCE_CXX
#endif
