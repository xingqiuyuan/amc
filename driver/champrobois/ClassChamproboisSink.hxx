// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SINK_HXX

namespace Driver
{
  /*! 
   * \class ClassChamproboisSink
   */
  class ClassChamproboisSink
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Sink type.*/
    string type_;

    /*!< Count of time steps.*/
    int Nt_;

    /*!< Number of sections.*/
    int Nsection_;

    /*!< Axial sink position in Chimney.*/
    real z_;

    /*!< Radial sink range in Chimney.*/
    real rmin_, rmax_;

    /*!< Number of radius sections.*/
    int Nr_impaction_;

    /*!< Temperature.*/
    real temperature_;

    /*!< Enthalpy.*/
    real enthalpy_;

    /*!< Total mass concentration.*/
    real concentration_total_;

    /*!< Air volume sampled.*/
    real air_volume_sampled_;

    /*!< Index of impaction.*/
    vector1r index_impaction_;

    /*!< Surface of impaction.*/
    vector1r surface_impaction_;

    /*!< Diameter bounds.*/
    vector1r diameter_bound_;

  public:

    /*!< Constructor.*/
    ClassChamproboisSink(const string type);

    /*!< Destructor.*/
    virtual ~ClassChamproboisSink();

    /*!< Get methods.*/
    virtual string GetName() const = 0;
    string GetType() const;
    int GetNt() const;
    int GetNsection() const;
    void GetDiameterBound(vector<real> &diameter_bound) const;
    real GetAirVolumeSampled() const;
    real GetTemperature() const;
    real GetConcentrationTotal() const;
    real GetPositionZ() const;
    real GetPositionRmin() const;
    real GetPositionRmax() const;

    /*!< Set methods.*/
    void SetPositionZ(const real z = ClassChamproboisBase::GetChimneyLength());
    void SetPositionRmin(const real rmin = real(0));
    void SetPositionRmax(const real rmax = ClassChamproboisBase::GetChimneyRadius());
#ifndef SWIG
    void SetDiameterBound(const vector1r &diameter_bound);
    void SetSurfaceImpaction(const int &Nr, const real &dr);
#endif

#ifndef SWIG
    /*!< Fill filter.*/
    virtual void Fill(const real &t, const real z,
                      const Array<real, 2> &concentration,
                      const Array<real, 1> &concentration_total,
                      const Array<real, 1> &temperature);

    /*!< Dump.*/
    virtual void Dump() const;

    /*!< End.*/
    virtual void End();
#endif
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SINK_HXX
#endif
