// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SOURCE_HXX

#define DRIVER_CHAMPROBOIS_SOURCE_EPSILON 1.e-12

namespace Driver
{
  /*! 
   * \class ClassChamproboisSource
   */
  class ClassChamproboisSource : public ClassChamproboisGranulometry
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  private:

    /*!< Is speciation complete ?.*/
    bool is_speciation_complete_;

    /*!< Size of source.*/
    int Ns_;

    /*!< Number of species.*/
    int Nspecies_;

    /*!< Number of gaseous species.*/
    int Ngas_;

    /*!< Number of purely volatile species.*/
    int Nvolatile_;

    /*!< Number of speciation.*/
    int Nspeciation_;

    /*!< Temperature.*/
    real temperature_;

    /*!< Water concentration.*/
    real concentration_water_;

    /*!< Comment.*/
    string comment_;

    /*!< Speciation pointers.*/
    vector<ClassChamproboisSpeciation* > speciation_ptr_;

    /*!< Species name.*/
    vector1s species_name_;

    /*!< Gas species index.*/
    vector1i gas_index_;

    /*!< Gas species mass.*/
    vector1r concentration_gas_mass_;

    /*!< Aerosol mass fraction.*/
    vector2r fraction_aer_mass_;

    /*!< Compute source.*/
    void compute_source(const int h, const real ratio, Array<real, 1> &source) const;

  public:

    /*!< Constructor.*/
    ClassChamproboisSource(const string name);

    /*!< Destructor.*/
    ~ClassChamproboisSource();

    /*!< Is speciation complete ?.*/
    bool IsSpeciationComplete() const;

    /*!< Get methods.*/
    int GetNs() const;
    int GetNspeciation() const;
    int GetNspecies() const;
    int GetNgas() const;
    int GetNvolatile() const;
    string GetComment() const;
    string GetSpeciesName(const int i) const;
    string GetSpeciesLabel(const int i) const;
    vector<string> GetSpeciesNameList() const;
    vector<int> GetGasIndex() const;
    void GetFractionAerMass(vector<vector<real> > &fraction_aer_mass) const;
    void GetConcentrationGasMass(vector<real> &concentration_gas_mass) const;
    real GetTemperature(const real t = real(0)) const;
    real GetConcentrationWater() const;
    ClassChamproboisSpeciation* GetSpeciationPtr(const int i);

    /*!< Add volatile species.*/
    void AddVolatile(const string name, const real concentration_mass);

    /*!< Add speciation.*/
    void AddSpeciation(const ClassChamproboisSpeciation &speciation, vector<real> fraction_section = vector<real>());

    /*!< Set methods.*/
    void SetComment(const string comment);
    void SetTemperature(const real temperature_celsius);
    void SetConcentrationWater(real volume_humidity_percentage, const real pressure_pascal);
    void SetFractionAerMass();

    /*!< Compute.*/
#ifndef SWIG
    void Compute(const real &t, const real &time_step, Array<real, 1> &source) const;
#endif
    void Compute(const real &t, const real &time_step, vector<real> &source) const;

    /*!< Order.*/
    void Order(const vector<string> &species_name, const vector<int> &semivolatile);
    void OrderAMC();

    /*!< Dump.*/
    void Dump(const bool with_granulometry = false, const int width = 12, const int precision = 6) const;
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SOURCE_HXX
#endif
