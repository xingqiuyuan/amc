// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SPECIATION_HXX

#define CHAMPROBOIS_SPECIATION_DIAMETER_MIN 0.
#define CHAMPROBOIS_SPECIATION_DIAMETER_MAX 10.

namespace Driver
{
  /*! 
   * \class ClassChamproboisSpeciation
   */
  class ClassChamproboisSpeciation : protected ClassChamproboisBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Number of species.*/
    int Nspecies_;

    /*!< Name of speciation.*/
    string name_;

    /*!< Key of speciation.*/
    string key_;

    /*!< Species name total.*/
    string species_name_total_;

    /*!< Species total label.*/
    string species_label_total_;

    /*!< Species name total.*/
    vector1s species_key_total_;

    /*!< Factor total.*/
    real factor_total_;

    /*!< Min and max diameter.*/
    real diameter_min_, diameter_max_;

    /*!< Concentration gas total.*/
    real concentration_gas_total_;

    /*!< Concentration aerosol total.*/
    real concentration_aer_total_;

    /*!< Whether species is semivolatile or not.*/
    vector1b is_semi_volatile_;

    /*!< Species name.*/
    vector1s species_name_;

    /*!< Vector of label species.*/
    vector1s species_label_;

    /*!< Vector of key species.*/
    vector2s species_key_;

    /*!< Vector of factor.*/
    vector1r factor_;

    /*!< Gas concentration (not time dependent).*/
    vector1r concentration_gas_;

    /*!< Aerosol concentration (not time dependent).*/
    vector1r concentration_aer_;

  public:

    /*!< Constructor.*/
    ClassChamproboisSpeciation(const string name, const string species_name_total,
                               const bool read_configuration = true);

    /*!< Destructor.*/
    virtual ~ClassChamproboisSpeciation();

    /*!< Get methods.*/
    bool IsGasComplete() const;
    bool IsAerComplete() const;
    string GetName() const;
    string GetKey() const;
    int GetNspecies() const;
    int GetSpeciesIndex(const string species_name) const;
    string GetSpeciesNameTotal() const;
    string GetSpeciesLabelTotal() const;
    void GetSpeciesKeyTotal(vector<string> &species_key_total) const;
    string GetSpeciesName(const int i) const;
    void GetSpeciesNameList(vector<string> &species_name) const;
    string GetSpeciesLabel(const int i) const;
    void GetSpeciesKey(const int i, vector<string> &species_key) const;
    void GetConcentration(vector<real> &gas, vector<real> &aer, const vector<string> species_name = vector<string>()) const;
    real GetConcentrationGasTotal() const;
    real GetConcentrationAerTotal() const;

    /*!< Set methods.*/
    void SetDiameterMin(const real diameter_min = CHAMPROBOIS_SPECIATION_DIAMETER_MIN);
    void SetDiameterMax(const real diameter_max = CHAMPROBOIS_SPECIATION_DIAMETER_MAX);
    void SetSpeciesTotal(const real gas_total, const real aer_total);
    void SetSpecies(const int i, const real gas, const real aer);
    void SetKey(const string key);

    /*!< Add one species.*/
    void AddSpecies(const string name, real gas, const real aer, const string label = "",
                    const vector<string> key = vector<string>({"gazeux", "particulaire"}),
                    const real factor = real(1));

    /*!< Remove one species.*/
    void RemoveSpecies(const int i);

    /*!< Extract.*/
    void Extract(ClassChamproboisSpeciation &speciation) const;

    /*!< Explode.*/
    void Explode(const ClassChamproboisSpeciation &speciation);

#ifndef SWIG
    /*!< Add to source.*/
    void AddToSource(const vector<real> &diameter_bound, vector<real> &fraction_section,
                     vector1s &species_name, vector1i &gas_index,
                     vector1r &concentration_gas_mass, vector2r &fraction_aer_mass) const;
#endif

    /*!< Compute.*/
    real ComputeConcentrationAerTotal() const;
    real ComputeConcentrationGasTotal() const;

    /*!< Dump.*/
    virtual void Dump(map<string, int> &argument) const;

    /*!< Complete.*/
    void Complete(const real temperature, const vector<string> &species_name);
  };
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SPECIATION_HXX
#endif
