// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHAMPROBOIS_SINK_CXX

#include "ClassChamproboisSink.hxx"


namespace Driver
{
  // Constructor.
  ClassChamproboisSink::ClassChamproboisSink(const string type)
    : Nt_(0), Nsection_(0), z_(ClassChamproboisBase::GetChimneyLength()),
      rmin_(real(0)), rmax_(ClassChamproboisBase::GetChimneyRadius()),
      Nr_impaction_(0), air_volume_sampled_(real(0)), type_(type),
      temperature_(real(0)), enthalpy_(real(0)), concentration_total_(real(0))
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset()
                         << "Instantiate Champrobois Sink of type \"" << type_ << "\"." << endl;
#endif

    return;
  }


  // Destructor.
  ClassChamproboisSink::~ClassChamproboisSink()
  {
    return;
  }


  // Get methods.
  string ClassChamproboisSink::GetType() const
  {
    return type_;
  }


  int ClassChamproboisSink::GetNt() const
  {
    return Nt_;
  }


  int ClassChamproboisSink::GetNsection() const
  {
    return Nsection_;
  }


  void ClassChamproboisSink::GetDiameterBound(vector<real> &diameter_bound) const
  {
    diameter_bound = diameter_bound_;
  }


  real ClassChamproboisSink::GetAirVolumeSampled() const
  {
    return air_volume_sampled_;
  }


  real ClassChamproboisSink::GetTemperature() const
  {
    return temperature_;
  }


  real ClassChamproboisSink::GetConcentrationTotal() const
  {
    return concentration_total_;
  }


  real ClassChamproboisSink::GetPositionZ() const
  {
    return z_;
  }


  real ClassChamproboisSink::GetPositionRmin() const
  {
    return rmin_;
  }


  real ClassChamproboisSink::GetPositionRmax() const
  {
    return rmax_;
  }


  // Set methods.
  void ClassChamproboisSink::SetPositionZ(const real z)
  {
    z_ = z;
  }


  void ClassChamproboisSink::SetPositionRmin(const real rmin)
  {
    rmin_ = rmin;
  }


  void ClassChamproboisSink::SetPositionRmax(const real rmax)
  {
    rmax_ = rmax;
  }


  void ClassChamproboisSink::SetDiameterBound(const vector1r &diameter_bound)
  {
    diameter_bound_ = diameter_bound;
    Nsection_ = int(diameter_bound_.size()) - 1;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset() << "Nsection = " << Nsection_ << endl;
#endif
  }


  void ClassChamproboisSink::SetSurfaceImpaction(const int &Nr, const real &dr)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Set sink surface impaction." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "z = " << z_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "rmin = " << rmin_ << ", rmax = " << rmax_ << endl;
#endif

    Nr_impaction_ = 0;

    index_impaction_.clear(); 
    surface_impaction_.clear();

    real rm(real(0)), rp(real(0));
    for (int i = 0; i < Nr; ++i)
      {
        rm = rp;
        rp += dr;

        const real rmin = rm > rmin_ ? rm : rmin_;
        const real rmax = rp > rmax_ ? rmax_ : rp;

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SINK
        CBUG << "i = " << i << endl;
        CBUG << "rmin = " << rmin << ", rmax = " << rmax << endl;
#endif

        if (rmax > rmin)
          {
            const real surface = rmax * rmax - rmin * rmin;

            index_impaction_.push_back(i);
            surface_impaction_.push_back(surface);

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SINK
            CBUG << "Nr_impaction = " << Nr_impaction_ << endl;
            CBUG << "surface = " << surface_impaction_.back() << endl;
#endif

            Nr_impaction_++;
          }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Number of radius sections impacted is " << Nr_impaction_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "index_impaction = " << index_impaction_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "surface_impaction = " << surface_impaction_ << endl;
#endif
  }


  // Fill sink.
  void ClassChamproboisSink::Fill(const real &t, const real z,
                                  const Array<real, 2> &concentration,
                                  const Array<real, 1> &concentration_total,
                                  const Array<real, 1> &temperature)
  {
#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SINK
    CBUG << "t = " << t << ", z = " << z << endl;
#endif

    for (int i = 0; i < Nr_impaction_; ++i)
      {
        const int j = index_impaction_[i];
        const real volume_impaction = z * surface_impaction_[i];

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_SINK
        CBUG << "index_impaction = " << index_impaction_[i] << endl;
        CBUG << "volume_impaction = " << volume_impaction << endl;
#endif

        concentration_total_ += concentration_total(j) * volume_impaction;
        enthalpy_ += temperature(j) * concentration_total(j) * volume_impaction;
        air_volume_sampled_ += volume_impaction;
      }

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_FILTER
    CBUG << "Nt = " << Nt_ << endl;
    CBUG << "temperature = " << temperature_ << endl;
    CBUG << "air_volume_sampled = " << air_volume_sampled_ << endl;
    CBUG << "concentration_total = " << concentration_total_ << endl;
#endif

    Nt_++;
  }


  // Dump.
  void ClassChamproboisSink::Dump() const
  {
    cout << string(20, '=') << " Sink of type \"" << type_ << "\" " << string(20, '=') << endl
         << "Number of time steps = " << Nt_ << endl
         << "Number of sections = " << Nsection_ << endl
         << "Z position = " << z_ << endl
         << "Radius (min, max) = (" << rmin_ << ", " << rmax_ << ")" << endl
         << "Sampled air volume = " << air_volume_sampled_ << endl
         << "Temperature = " << temperature_ << endl
         << "Total concentration = " << concentration_total_ << endl
         << "Section bound diameters = " << diameter_bound_ << endl;
  }


  // Compute average concentrations.
  void ClassChamproboisSink::End()
  {
    if (Nt_ == 0)
      throw AMC::Error("No time steps in sink of \"" + type_ + "\".");

    if (air_volume_sampled_ <= real(0))
      throw AMC::Error("Sampled air volume is <= 0 in sink of \"" + type_ + "\".");

    // Average temperature.
    if (concentration_total_ > real(0))
      temperature_ = enthalpy_ / concentration_total_;

    concentration_total_ /= air_volume_sampled_;
  }
}

#define AMC_FILE_CLASS_CHAMPROBOIS_SINK_CXX
#endif
