// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_EMISSION_BIOGENIC_CXX

#include "ClassChimereEmissionBiogenic.hxx"

namespace Driver
{
  // Find tracer index.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::find_tracer_index()
  {
    tracer_index_.assign(ClassTracer::Nbiogenic_, -1);

    for (int i = 0; i < ClassTracer::Nbiogenic_; ++i)
      {
        // Index in Chimere.
        for (int j = 0; j < int(tracer_name_.size()); ++j)
          if (tracer_name_[j] == ClassTracer::name_[ClassTracer::Nanthropic_ + i])
            {
              tracer_index_[i] = j;
              break;
            }

        if (tracer_index_[i] < 0)
          throw AMC::Error("Could not find AMC emission tracer \"" +
                           ClassTracer::name_[ClassTracer::Nanthropic_ + i] +
                           "\" in Chimere anthropic emission species list.");
      }
  }


  // Allocate.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::Allocate()
  {
    // allocate(emisbloc (nemisb,nzonalmax,nmeridmax))
    // allocate(emipbloc (nkc,ms,nzonalmax,nmeridmax))
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info() << LogReset() << "Workers link fortran 90 Chimere"
                         << " emission fields to C++ blitz::Array<> : emisbloc, emipbloc." << endl;
#endif

    emission_.reference(Array<double, 3>(WORKER_COMMON(emisbloc),
                                         shape(WORKER_PARAMS(nmeridmax),
                                               WORKER_PARAMS(nzonalmax),
                                               WORKER_PARAMS(nemisb)),
                                         neverDeleteData));
#ifdef CHIMERE_WITH_AMC
    emission_particle_.reference(Array<double, 4>(WORKER_COMMON(emipbloc),
                                                  shape(WORKER_PARAMS(nmeridmax),
                                                        WORKER_PARAMS(nzonalmax),
                                                        CHIMERE_AMC.Ng,
                                                        CHIMERE_AMC.Nvariable),
                                                  neverDeleteData));
#else
    emission_particle_.reference(Array<double, 4>(WORKER_COMMON(emipbloc),
                                                  shape(WORKER_PARAMS(nmeridmax),
                                                        WORKER_PARAMS(nzonalmax),
                                                        WORKER_PARAMS(ms),
                                                        WORKER_PARAMS(nkc)),
                                                  neverDeleteData));
#endif
  }


  // Init.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::Init()
  {
    // Every MPI process has to allocate.
    tracer_index_.assign(ClassTracer::Nbiogenic_, -1);

    if (MPI::COMM_WORLD.Get_rank() == 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                             << "Init biogenic Chimere emission subsystem"
                             << " (only when Chimere runs with AMC)." << endl;
#endif

        // Chimere Biogenic emissions.
        ifstream fin(CHIMERE_COMMON(fnbiogen));

        if (! fin.good())
          throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnbiogen)) +
                           "\", probably wrong path.");

        string input;
        while (fin >> input)
          tracer_name_.push_back(input);

        fin.close();

        if (int(tracer_name_.size()) != CHIMERE_PARAMS(nemisb))
          throw AMC::Error("Read number of biogenic tracers (" + to_str(tracer_name_.size()) +
                           ") mismatches the internal Chimere value \"nemisb\" (" +
                           to_str(CHIMERE_PARAMS(nemisb)) +").");

        // Make the link between Chimere and AMC on the tracer level.
        find_tracer_index();
      }

    // Broadcast the tracer index to all workers, which have to
    // know it because they compute the biogenic particle emissions.
    MPI::COMM_WORLD.Bcast(tracer_index_.data(), int(tracer_index_.size()), MPI::INT, 0);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Rank() << Debug(2) << LogReset()
                         << "tracer_index = " << tracer_index_ << endl;
#endif

    // Allocate working arrays.
    emission_rate_mass_work_.assign(ClassTracer::Ntracer_, real(0));
    rate_aer_number_work_.assign(CHIMERE_AMC.Ng, real(0));
    rate_aer_mass_work_.assign(CHIMERE_AMC.Ng * CHIMERE_AMC.Nspecies, real(0));
  }


  // Get methods.
  template<class D>
  double ClassChimereEmissionBiogenic<D>::GetEmission(const int s, const int y, const int x)
  {
    return emission_(x, y, s);
  }


  template<class D>
  double ClassChimereEmissionBiogenic<D>::GetEmissionParticle(const int s, const int b, const int y, const int x)
  {
    return emission_particle_(x, y, b, s);
  }


  // Clear static data.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::Clear()
  {
    tracer_name_.clear();
    tracer_index_.clear();
    emission_rate_mass_work_.clear();
    rate_aer_number_work_.clear();
    rate_aer_mass_work_.clear();
    // Blitz arrays must NOT be freed.
  }


  // Compute source rate.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::ComputeSourceRate()
  {
    for (int y = 0; y < WORKER_PARAMS(nmeridmax); ++y)
      for (int x= 0; x < WORKER_PARAMS(nzonalmax); ++x)
        {
          Array<double, 1> emission_local = emission_(x, y, Range::all());
          Array<double, 2> emission_particle_local = emission_particle_(x, y, Range::all(), Range::all());

          int h(ClassTracer::Nanthropic_ - 1);
          for (int i = 0; i < ClassTracer::Nbiogenic_; ++i)
            emission_rate_mass_work_[++h] = double(emission_local(tracer_index_[i]));

          rate_aer_number_work_.assign(CHIMERE_AMC.Ng, real(0));
          rate_aer_mass_work_.assign(CHIMERE_AMC.Ng * CHIMERE_AMC.Nspecies, real(0));

          for (int i = 0; i < ClassSource::Nbiogenic_; ++i)
            ClassDynamicsEmission<D>::rate(ClassSource::Nanthropic_ + i,
                                           emission_rate_mass_work_,
                                           rate_aer_number_work_,
                                           rate_aer_mass_work_);

          h = -1;
          for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
            {
              for (int j = 0; j < CHIMERE_AMC.Nspecies; ++j)
                emission_particle_local(i, j) += rate_aer_mass_work_[++h];

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
              emission_particle_local(i, CHIMERE_AMC.Nspecies) += rate_aer_number_work_[i];
#endif
            }
        }
  }


#if defined(CHIMERE_AMC_WITH_PREPROCESSING) && defined(DRIVER_WITH_NETCDF)
  // Compute source general section index.
  template<class D>
  void ClassChimereEmissionBiogenic<D>::PreProcessSourceGeneralSection(const string netcdf_file,
                                                                       const string species_file)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Preprocess biogenic source AMC general section"
                         << " with emission file \"" << netcdf_file << "\"." << endl;
#endif

    if (! ClassDynamicsEmission<D>::source_count_general_section_)
      throw AMC::Error("Source general section counting not enabled in emission dynamics class.");

    //
    // Load emission file.
    //

    NcFile fnemission(netcdf_file.c_str(), NcFile::ReadOnly);

    if (! fnemission.is_valid())
      throw AMC::Error("Unable to open NetCDF file \"" + netcdf_file + "\".");

    // Grab dimensions.
    const int Nt = int(fnemission.get_dim("Time")->size());
    const int Ns =  int(fnemission.get_dim("biospecies")->size());
    const int Ny =  int(fnemission.get_dim("south_north")->size());
    const int Nx =  int(fnemission.get_dim("west_east")->size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << LogReset()
                         << "Dimensions from NetCDf file : (Nt, Ns, Ny, Nx) = (" << Nt
                         << ", " << Ns << ", " << Ny << ", " << Nx << ")" << endl;
#endif

    // Chimere Biogenic emission tracer names.
    {
      ifstream fin(species_file.c_str());

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + species_file + "\", probably wrong path.");

      tracer_name_.clear();

      string input;
      while (fin >> input)
        tracer_name_.push_back(input);

      fin.close();
    }

    if (int(tracer_name_.size()) != Ns)
      throw AMC::Error("Read number of biogenic tracers (" + to_str(tracer_name_.size()) +
                       ") mismatches the NetCDF file value \"biospecies\" (" + to_str(Ns) +").");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset()
                         << "Found " << Ns << " tracer species in NetCDF file : " << tracer_name_ << endl;
#endif

    // Make the link between Chimere and AMC on the tracer level.
    find_tracer_index();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Debug(2) << LogReset() << "tracer_index = " << tracer_index_ << endl;
#endif

    // From AMC.
    const int Ng = AMC::ClassAerosolData::GetNg();
    const int Nspecies = AMC::ClassSpecies::GetNspecies();
#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
    const int Nvariable = Nspecies + 1;
#else
    const int Nvariable = Nspecies;
#endif

    // Allocate arrays.
    emission_.resize(Nx, Ny, Ns);
    emission_ = double(0);

    emission_particle_.resize(Nx, Ny, Ng, Nvariable);
    emission_particle_ = double(0);

    // Allocate working arrays.
    emission_rate_mass_work_.assign(ClassTracer::Ntracer_, real(0));
    rate_aer_number_work_.assign(Ng, real(0));
    rate_aer_mass_work_.assign(Ng * Nspecies, real(0));

    //
    // Main loop.
    //

#ifdef AMC_WITH_TIMER
    const int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_emission_biogenic");
#endif

    // NetCDF species variables.
    vector<NcVar*> var(Ns, NULL);
    for (int s = 0; s < Ns; ++s)
      var[s] = fnemission.get_var(tracer_name_[s].c_str());

    // A buffer is necessary here to overcome non-contiguous memory fields.
    Array<double, 2> buffer(Ny, Nx);

    // Time loop.
    for (int t = 0; t < Nt; ++t)
      {
        // Grab emission data.
        for (int s = 0; s < Ns; ++s)
          {
            var[s]->set_cur(t, 0, 0);
            var[s]->get(buffer.data(), 1, Ny, Nx);

            for (int y = 0; y < Ny; ++y)
              for (int x = 0; x < Nx; ++x)
                emission_(x, y, s) = buffer(y, x);
          }

#ifdef AMC_WITH_TIMER
        AMC::AMCTimer<AMC::CPU>::Start();
#endif

        // Space loop.
        for (int y = 0; y < Ny; ++y)
          for (int x = 0; x < Nx; ++x)
            {
              Array<double, 1> emission_local = emission_(x, y, Range::all());
              Array<double, 2> emission_particle_local = emission_particle_(x, y, Range::all(), Range::all());

              int h(ClassTracer::Nanthropic_ - 1);
              for (int i = 0; i < ClassTracer::Nbiogenic_; ++i)
                emission_rate_mass_work_[++h] = double(emission_local(tracer_index_[i]));

              rate_aer_number_work_.assign(Ng, real(0));
              rate_aer_mass_work_.assign(Ng * Nspecies, real(0));

              for (int i = 0; i < ClassSource::Nbiogenic_; ++i)
                ClassDynamicsEmission<D>::rate(ClassSource::Nanthropic_ + i,
                                               emission_rate_mass_work_,
                                               rate_aer_number_work_,
                                               rate_aer_mass_work_);

              h = -1;
              for (int i = 0; i < Ng; ++i)
                {
                  for (int j = 0; j < Nspecies; ++j)
                    emission_particle_local(i, j) += rate_aer_mass_work_[++h];

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
                  emission_particle_local(i, Nspecies) += rate_aer_number_work_[i];
#endif
                }
            }

#ifdef AMC_WITH_TIMER
        AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#endif
      }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset()
                         << "Performed " << Nt * Ny * Nx << " emission calls in "
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds (" << setprecision(12)
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) / real(Nt * Ny * Nx)
                         << " seconds/call) with biogenic emission file \"" << netcdf_file
                         << "\" and species file \"" << species_file << "\"." << endl;
#endif
#endif

    // We no more need it.
    fnemission.close();

    // Deallocate.
    emission_.free();
    emission_particle_.free();
  }
#endif
}

#define AMC_FILE_CLASS_CHIMERE_EMISSION_BIOGENIC_CXX
#endif
