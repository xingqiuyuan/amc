// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CHIMERE_HEADER_HXX


//
// External Fortran 90 Chimere variables.
//


#ifdef __GNUC__
#define CHIMERE_PARAMS(var) __chimere_params_MOD_##var
#define CHIMERE_COMMON(var) __chimere_common_MOD_##var
#define WORKER_PARAMS(var) __worker_params_MOD_##var
#define WORKER_COMMON(var) __worker_common_MOD_##var
#endif

extern char CHIMERE_PARAMS(domain[10]);
extern int CHIMERE_PARAMS(aero);
extern int CHIMERE_PARAMS(bins);
extern int CHIMERE_PARAMS(carb);
extern double CHIMERE_PARAMS(dpmax);
extern double CHIMERE_PARAMS(dpmin);
extern int CHIMERE_PARAMS(dust);
extern int CHIMERE_PARAMS(dustout);
extern int CHIMERE_PARAMS(ibound);
extern int CHIMERE_PARAMS(ichemstep);
extern int CHIMERE_PARAMS(ideepconv);
extern int CHIMERE_PARAMS(ihoursu);
extern int CHIMERE_PARAMS(ms);
extern int CHIMERE_PARAMS(nemisa);
extern int CHIMERE_PARAMS(nemisb);
extern int CHIMERE_PARAMS(nequil);
extern int CHIMERE_PARAMS(nfam);
extern int CHIMERE_PARAMS(nitgs);
extern int CHIMERE_PARAMS(nitgssu);
extern int CHIMERE_PARAMS(nivout);
extern int CHIMERE_PARAMS(nkc);
extern int CHIMERE_PARAMS(nmdoms);
extern int CHIMERE_PARAMS(nmerid);
extern int CHIMERE_PARAMS(npeq);
extern int CHIMERE_PARAMS(nphour_ref);
extern int CHIMERE_PARAMS(nreac);
extern int CHIMERE_PARAMS(nsaveconcs);
extern int CHIMERE_PARAMS(nsavedepos);
extern int CHIMERE_PARAMS(nsho);
extern int CHIMERE_PARAMS(nspec);
extern int CHIMERE_PARAMS(nvert_raw);
extern int CHIMERE_PARAMS(nzdoms);
extern int CHIMERE_PARAMS(nzonal);
extern int CHIMERE_PARAMS(pops);
extern int CHIMERE_PARAMS(reactive);
extern int CHIMERE_PARAMS(seasalt);
extern int CHIMERE_PARAMS(soatyp);
extern int CHIMERE_PARAMS(trc);
extern int CHIMERE_PARAMS(nlevemis);
extern int CHIMERE_PARAMS(nverti);
extern int CHIMERE_PARAMS(ivsurf);
extern int CHIMERE_PARAMS(nspectot);

extern char WORKER_PARAMS(domain[10]);
extern int WORKER_PARAMS(aero);
extern int WORKER_PARAMS(bins);
extern int WORKER_PARAMS(carb);
extern double WORKER_PARAMS(dpmax);
extern double WORKER_PARAMS(dpmin);
extern int WORKER_PARAMS(dustout);
extern int WORKER_PARAMS(ibound);
extern int WORKER_PARAMS(ichemstep);
extern int WORKER_PARAMS(ideepconv);
extern int WORKER_PARAMS(ihoursu);
extern int WORKER_PARAMS(ms);
extern int WORKER_PARAMS(nemisa);
extern int WORKER_PARAMS(nemisb);
extern int WORKER_PARAMS(nequil);
extern int WORKER_PARAMS(nfam);
extern int WORKER_PARAMS(nitgs);
extern int WORKER_PARAMS(nitgssu);
extern int WORKER_PARAMS(nivout);
extern int WORKER_PARAMS(nkc);
extern int WORKER_PARAMS(nlevemis);
extern int WORKER_PARAMS(nmdoms);
extern int WORKER_PARAMS(nmeridmax);
extern int WORKER_PARAMS(npeq);
extern int WORKER_PARAMS(nphour_ref);
extern int WORKER_PARAMS(nreac);
extern int WORKER_PARAMS(nsaveconcs);
extern int WORKER_PARAMS(nsavedepos);
extern int WORKER_PARAMS(nsho);
extern int WORKER_PARAMS(nspec);
extern int WORKER_PARAMS(nvert_raw);
extern int WORKER_PARAMS(nzdoms);
extern int WORKER_PARAMS(nzonalmax);
extern int WORKER_PARAMS(pops);
extern int WORKER_PARAMS(reactive);
extern int WORKER_PARAMS(seasalt);
extern int WORKER_PARAMS(soatyp);
extern int WORKER_PARAMS(trc);
extern int WORKER_PARAMS(nverti);
extern int WORKER_PARAMS(ivsurf);
extern int WORKER_PARAMS(nspectot);

extern char CHIMERE_COMMON(version[16]);
extern char CHIMERE_COMMON(fnaeromin[132]);
extern char CHIMERE_COMMON(fnaeroorg[132]);
extern char CHIMERE_COMMON(fnaerosol[132]);
extern char CHIMERE_COMMON(fnanthro[132]);
extern char CHIMERE_COMMON(fnbiogen[132]);
extern char CHIMERE_COMMON(fnbounconc[132]);
extern char CHIMERE_COMMON(fnbounspec[132]);
extern char CHIMERE_COMMON(fnchem[132]);
extern char CHIMERE_COMMON(fnconcs[132]);
extern char CHIMERE_COMMON(fndepoespe[132]);
extern char CHIMERE_COMMON(fndepopars[132]);
extern char CHIMERE_COMMON(fndepos[132]);
extern char CHIMERE_COMMON(fnemisa[132]);
extern char CHIMERE_COMMON(fnemisb[132]);
extern char CHIMERE_COMMON(fnendex[132]);
extern char CHIMERE_COMMON(fnexc[132]);
extern char CHIMERE_COMMON(fnfamilies[132]);
extern char CHIMERE_COMMON(fniniex[132]);
extern char CHIMERE_COMMON(fninit[132]);
extern char CHIMERE_COMMON(fnlanduse[132]);
extern char CHIMERE_COMMON(fnmeteo[132]);
extern char CHIMERE_COMMON(fnothers[132]);
extern char CHIMERE_COMMON(fnout[132]);
extern char CHIMERE_COMMON(fnoutspec[132]);
extern char CHIMERE_COMMON(fnphot[132]);
extern char CHIMERE_COMMON(fnprim[132]);
extern char CHIMERE_COMMON(fnrates[132]);
extern char CHIMERE_COMMON(fnsea[132]);
extern char CHIMERE_COMMON(fnsemivol[132]);
extern char CHIMERE_COMMON(fnsoil[132]);
extern char CHIMERE_COMMON(fnspec[132]);
extern char CHIMERE_COMMON(fnstoi[132]);
extern char CHIMERE_COMMON(fnveget[132]);
extern char CHIMERE_COMMON(fnwetd[132]);
extern int CHIMERE_COMMON(iopinit);
extern int CHIMERE_COMMON(nhourrun);
extern int CHIMERE_COMMON(idatestart);

extern double* CHIMERE_COMMON(conc);
extern double* WORKER_COMMON(conc);
extern int* CHIMERE_COMMON(idaero);
extern int* CHIMERE_COMMON(nsecti);
extern int* CHIMERE_COMMON(ncompo);
extern int CHIMERE_COMMON(ngassp);

extern float* CHIMERE_COMMON(emisaloc);
extern double* CHIMERE_COMMON(emipaloc);
extern double* WORKER_COMMON(emisbloc);
extern double* WORKER_COMMON(emipbloc);

extern int CHIMERE_COMMON(nspecboun);
extern int CHIMERE_PARAMS(nhbound);
extern int CHIMERE_PARAMS(nlatbound);
extern int* CHIMERE_COMMON(isboun);
extern float* CHIMERE_COMMON(boundlat);
extern float* CHIMERE_COMMON(boundtop);

//
// External Chimere Fortran 90 functions.
//

extern "C"
{
  void chimere_init_();
  void chimere_run_();
  void chimere_end_();
}


#ifdef CHIMERE_WITH_AMC
//
// Chimere data module for C++ to Fortran 90 communication. Only useful when AMC compiled in.
//

#define CHIMERE_AMC __chimere_amc_MOD_amc

typedef struct chimere_amc {
  int Ng;
  int Nspecies;
  int Nvariable;
  int Nsection;
} chimere_amc;

extern chimere_amc __chimere_amc_MOD_amc;
#endif


#define AMC_FILE_CHIMERE_HEADER_HXX
#endif
