// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_EMISSION_ANTHROPIC_HXX


namespace Driver
{
  /*! 
   * \class ClassChimereEmissionAnthropic
   */
  template<class D>
  class ClassChimereEmissionAnthropic : protected ClassDynamicsEmission<D>
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  private:

    /*!< Tracer names in Chimere.*/
    static vector1s tracer_name_;

    /*!< Index of anthropic AMC tracers in emisaloc Chimere emission field.*/
    static vector1i tracer_index_;

    /*!< Working arrays.*/
    static vector1r emission_rate_mass_work_;
    static vector1r rate_aer_number_work_;
    static vector1r rate_aer_mass_work_;

    /*!< Emission fields.*/
    static Array<float, 4> emission_;
    static Array<double, 5> emission_particle_;

    /*! Find tracer index.*/
    static void find_tracer_index();

  public:

    /*!< Allocate.*/
    static void Allocate();

    /*!< Init.*/
    static void Init();

    /*!< Get methods.*/
    static float GetEmission(const int s, const int z, const int y, const int x);
    static double GetEmissionParticle(const int s, const int b, const int z, const int y, const int x);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Compute source rate.*/
    static void ComputeSourceRate();

#if defined(CHIMERE_AMC_WITH_PREPROCESSING) && defined(DRIVER_WITH_NETCDF)
    /*!< Preprocess source general section index.*/
    static void PreProcessSourceGeneralSection(const string netcdf_file);
#endif
  };
}

#define AMC_FILE_CLASS_CHIMERE_EMISSION_ANTHROPIC_HXX
#endif
