// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CHIMERE_CPP

// For standalone Chimere compilation, get rid of everything else (trajectory, geo_data, ...).
#include "driver/Driver.hxx"

using namespace std;
using namespace Driver;
//using namespace AMC;

#include "ChimereTemplate.hxx"
typedef Driver::ClassDriverChimere<redist_size, redist_comp, disc_comp, coag_coef_repart> Chimere;

#ifdef CHIMERE_WITH_AMC
template bool AMC::ClassConfiguration::IsInitialized<AMC::init_flag::amc>();
#endif

int main(int argc, char** argv)
{
  TRY;

  if (argc < 5)
    throw string("Chimere executable expects 4 arguments : configuration_file prefix date_begin number_of_days");

  string configuration_file(argv[1]);
  string prefix(argv[2]);
  string date_begin(argv[3]);

  int number_of_days(0);
  istringstream iss(argv[4]);
  iss >> number_of_days;

  // Init MPI.
  ClassDriverBase::MPI_Init();

  // Setup logging system, if compiled in.
#ifdef AMC_WITH_LOGGER
  AMC::AMCLogger::Init(configuration_file);
#endif

  // Chimere and AMC configuration.
  Chimere::Init(configuration_file, prefix, date_begin, number_of_days);

  // Run Chimere (with MPI).
  Chimere::Run();

  // Clear everything.
  Chimere::Clear();

  END;

  return 0;
}

#define AMC_FILE_CHIMERE_CPP
#endif
