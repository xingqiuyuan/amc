subroutine chimere_run

!  Main program for simulation with CHIMERE

  use chimere_common
  use master_message_subs
  implicit none

!  Run integration
  if(rank==0) then
     call integrun
  else
     call worker
  endif

end subroutine chimere_run
