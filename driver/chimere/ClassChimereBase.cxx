// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_BASE_CXX

#include "ClassChimereBase.hxx"

namespace Driver
{
  // Compute end date from beginning date and number of simulation days.
  string ClassChimereBase::compute_date_end(const string &date_begin, const int &number_of_days)
  {
    struct tm date_end_tm = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    strptime(date_begin.c_str(), "%Y%m%d", &date_end_tm);
    date_end_tm.tm_mday += number_of_days - 1; // Last date included, so substract one.
    mktime(&date_end_tm);

    char date_end_char[10];
    strftime(date_end_char, 10, "%Y%m%d", &date_end_tm);
    return string(date_end_char);
  }


  // Allocate arrays.
  void ClassChimereBase::Allocate()
  {
    // Once Chimere is initiated, we can link concentrations.
    // allocate(conc (nspectot,-2:nzonal+3,-2:nmerid+3,1:nverti+1))
    // allocate(conc (nspectot,-2:nzonalmax+3,-2:nmeridmax+3,1:nverti+1))

    if (ClassDriverBase::rank_ == 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bblue() << Info() << LogReset() << "Master links fortran 90 CHIMERE"
                             << " concentration fields to C++ blitz::Array<> : conc." << endl;
#endif

        concentration_chimere_.reference(Array<double, 4>(CHIMERE_COMMON(conc),
                                                          shape(CHIMERE_PARAMS(nverti) + 1,
                                                                CHIMERE_PARAMS(nmerid) + 6,
                                                                CHIMERE_PARAMS(nzonal) + 6,
                                                                CHIMERE_PARAMS(nspectot)),
                                                          neverDeleteData));
      }
    else
      concentration_worker_.reference(Array<double, 4>(WORKER_COMMON(conc),
                                                       shape(WORKER_PARAMS(nverti) + 1,
                                                             WORKER_PARAMS(nmeridmax) + 6,
                                                             WORKER_PARAMS(nzonalmax) + 6,
                                                             WORKER_PARAMS(nspectot)),
                                                       neverDeleteData));
  }


  // Read Chimere configuration.
  void ClassChimereBase::ReadConfiguration(const string &date_begin, const string &date_end)
  {
    Ops::Ops ops(ClassDriverBase::configuration_file_);

    ops.SetPrefix(ClassDriverBase::prefix_ + ".");

    // Root directory.
    const string chimere_root = ops.Get<string>("chimere_root");

    // Temporary directory.
    const string chimere_tmp = ops.Get<string>("chimere_tmp");

#ifdef AMC_WITH_LOGGER
    Rank master(0, "Master", 7);
    *AMCLogger::GetLog() << master << Fyellow() << Debug() << LogReset()
                         << "chimere_root = " << chimere_root << endl;
    *AMCLogger::GetLog() << master << Fyellow() << Debug() << LogReset()
                         << "chimere_tmp = " << chimere_tmp << endl;
#endif

    // Some flags.
    CHIMERE_PARAMS(aero) = ops.Get<int>("aero");
    CHIMERE_PARAMS(seasalt) =  ops.Get<int>("seasalt");
    CHIMERE_PARAMS(dust) = ops.Get<int>("dust");
    CHIMERE_PARAMS(pops) = ops.Get<int>("pops");
    CHIMERE_PARAMS(dustout) = ops.Get<int>("dustout");
    CHIMERE_PARAMS(carb) = ops.Get<int>("carb");

    CHIMERE_PARAMS(bins) = 0;
    CHIMERE_PARAMS(ms) = 0;
    if (CHIMERE_PARAMS(aero) == 1 || CHIMERE_PARAMS(dustout) == 1 ||
        CHIMERE_PARAMS(seasalt) == 1 || CHIMERE_PARAMS(carb) == 1 ||
        CHIMERE_PARAMS(dust) == 1)
      {
        CHIMERE_PARAMS(bins) = 1;
        CHIMERE_PARAMS(ms) = ops.Get<int>("nbins");
      }

    int mecachim = ops.Get<int>("mecachim");
    bool nested = ops.Get<bool>("nested");

    CHIMERE_PARAMS(reactive) = 0;
    if (mecachim > 0)
      CHIMERE_PARAMS(reactive) = 1;

    CHIMERE_PARAMS(trc) = ops.Get<int>("trc");
    CHIMERE_PARAMS(soatyp) = ops.Get<int>("soatyp");

    ostringstream schemprepdir;
    schemprepdir << "chemprep/inputdata." << mecachim << CHIMERE_PARAMS(aero) << CHIMERE_PARAMS(seasalt)
                 << CHIMERE_PARAMS(pops) << CHIMERE_PARAMS(dustout) << CHIMERE_PARAMS(carb)
                 << CHIMERE_PARAMS(trc) << CHIMERE_PARAMS(soatyp) << CHIMERE_PARAMS(dust)
                 << "." << CHIMERE_PARAMS(ms);

    string chemprepdir = ops.Get<string>("chemprepdir", "", schemprepdir.str());

    if (chemprepdir[0] != '/')
      chemprepdir = chimere_root + "/" + chemprepdir;

    util::check_directory(chemprepdir.c_str());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fgreen() << Debug() << LogReset() << "chemprepdir = " << chemprepdir << endl;
#endif

    CHIMERE_PARAMS(ichemstep) = ops.Get<int>("step");
    CHIMERE_PARAMS(ideepconv) = ops.Get<int>("ideepconv");
    CHIMERE_PARAMS(nphour_ref) = ops.Get<int>("phys");
    CHIMERE_PARAMS(npeq) = ops.Get<int>("npeq", "", 1);
    CHIMERE_PARAMS(nitgs) =  ops.Get<int>("ngs", "", 1);
    CHIMERE_PARAMS(nitgssu) = ops.Get<int>("nsu", "", 5);
    CHIMERE_PARAMS(ihoursu) = ops.Get<int>("irs", "", 1);
    CHIMERE_PARAMS(nsaveconcs) =ops.Get<int>("nsconcs", "", 24);
    CHIMERE_PARAMS(nsavedepos) =ops.Get<int>("nsdepos","", 12);

    CHIMERE_PARAMS(ibound) = 1;
    if (CHIMERE_PARAMS(reactive) == 0 && CHIMERE_PARAMS(bins) == 0)
      if (nested)
        CHIMERE_PARAMS(ibound) = 0;

    CHIMERE_PARAMS(nequil) = ops.Get<int>("nequil", "", 0);
    if (CHIMERE_PARAMS(seasalt) == 2)
      {
        CHIMERE_PARAMS(nequil) = 1;
        cout << "+++ Because of active sea salts, _nequil_ is forced to 1 : " << endl;
        cout << "+++ use of ISORROPIA on line" << endl;
      }

    // Model version.
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Bmagenta() << Info() << "Chimere version = "
                         << CHIMERE_COMMON(version) << LogReset().Str() << endl;
#endif

    // Domain directory.
    string domaindir = ops.Get<string>("domaindir", "", "domains");
    if (domaindir[0] != '/')
      domaindir = chimere_root + "/" + domaindir;

    // Vertical coordinates.
    int nlayer_chimere = ops.Get<int>("nlayer_chimere");
    real first_layer_pressure = ops.Get<real>("first_layer_pressure");
    real top_chimere_pressure = ops.Get<real>("top_chimere_pressure");

    ostringstream svcoord;
    svcoord << domaindir << "/VCOORD/VCOORD_" << nlayer_chimere
            << "_" << first_layer_pressure << "_" << top_chimere_pressure;

    const string vcoord = ops.Get<string>("vcoord", "", svcoord.str());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fblue() << Info() << LogReset()
                         << "File for vertical coordinates : " << vcoord << endl;
#endif

    {
      ifstream fin(vcoord.c_str());

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + vcoord + "\", probably wrong path.");

      CHIMERE_PARAMS(nvert_raw) = 0;

      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nvert_raw);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fblue() << Debug() << LogReset()
                         << "nvert_raw = " << CHIMERE_PARAMS(nvert_raw) << endl;
#endif

    ClassDriverDomainBase::Nz_ = CHIMERE_PARAMS(nvert_raw);
    ClassDriverDomainBase::z_top_.assign(ClassDriverDomainBase::Nz_, real(0));
    ClassDriverDomainBase::z_mid_.assign(ClassDriverDomainBase::Nz_, real(0));

    // Domain.
    strncpy(CHIMERE_PARAMS(domain), ops.Get<string>("domain").c_str(), 10);
    strncpy(WORKER_PARAMS(domain), ops.Get<string>("domain").c_str(), 10);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fmagenta() << Debug() << LogReset()
                         << "domain = " << CHIMERE_PARAMS(domain) << endl;
#endif

    // Horizontal coordinates.
    ostringstream shcoord;
    shcoord << domaindir << "/HCOORD/COORD_" << CHIMERE_PARAMS(domain);

    const string hcoord = ops.Get<string>("hcoord", "", shcoord.str());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fmagenta() << Info() << LogReset()
                         << "File for horizontal coordinates : " << hcoord << endl;
#endif

    {
      ifstream fin(hcoord.c_str());

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + hcoord + "\", probably wrong path.");

      CHIMERE_PARAMS(nzonal) = 1;
      CHIMERE_PARAMS(nmerid) = 1;

      float lon, lat;
      fin >> lon >> lat;

      ClassDriverDomainBase::x_.push_back(lon);
      ClassDriverDomainBase::y_.push_back(lat);

      float lon2, lat2;
      while (fin >> lon2 >> lat2)
        {
          if (lat2 > lat)
            {
              ++CHIMERE_PARAMS(nmerid);
              ClassDriverDomainBase::y_.push_back(lat2);
            }

          lat = lat2;

          if (lon2 > lon)
            {
              ++CHIMERE_PARAMS(nzonal);
              ClassDriverDomainBase::x_.push_back(lon2);
              lon = lon2;
            }
        }

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Bblue() << Info() << LogReset() << "Domain resolution : ("
                         << CHIMERE_PARAMS(nzonal) << ", " << CHIMERE_PARAMS(nmerid) << ")." << endl;
#endif

    ClassDriverDomainBase::Nx_ = int(ClassDriverDomainBase::x_.size());
    ClassDriverDomainBase::Ny_ = int(ClassDriverDomainBase::y_.size());

    // Output species.
    const string accur = ops.Get<string>("accur");
    strncpy(CHIMERE_COMMON(fnoutspec), ops.Get<string>("fnoutspec", "",
                                                       chemprepdir + "/OUTPUT_SPECIES." + accur).c_str(), 132);

    // Active species.
    strncpy(CHIMERE_COMMON(fnspec), ops.Get<string>("fnspec", "", chemprepdir + "/ACTIVE_SPECIES").c_str(), 132);

    {
      ifstream fin(CHIMERE_COMMON(fnspec));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnspec)) + "\", probably wrong path.");

      CHIMERE_PARAMS(nspec) = 0;
      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nspec);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fmagenta() << Info() << LogReset()
                         << "Number of active species (nspec) : " << CHIMERE_PARAMS(nspec) << endl;
#endif

    // Chemistry.
    strncpy(CHIMERE_COMMON(fnchem), ops.Get<string>("fnchem", "", chemprepdir + "/CHEMISTRY").c_str(), 132);

    {
      ifstream fin(CHIMERE_COMMON(fnchem));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnchem)) + "\", probably wrong path.");

      CHIMERE_PARAMS(nreac) = 0;
      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nreac);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Byellow() << Info() << LogReset()
                         << "Number of reactions (nreac) : " << CHIMERE_PARAMS(nreac) << endl;
#endif

    if (CHIMERE_PARAMS(reactive) == 1)
      {
        strncpy(CHIMERE_COMMON(fnstoi), ops.Get<string>("fnstoi", "", chemprepdir + "/STOICHIOMETRY").c_str(), 132);
        strncpy(CHIMERE_COMMON(fnphot), ops.Get<string>("fnphot", "", chemprepdir + "/PHOTO_PARAMETERS").c_str(), 132);
      }

    // Reaction rates.
    strncpy(CHIMERE_COMMON(fnrates), ops.Get<string>("fnrates", "", chemprepdir + "/REACTION_RATES").c_str(), 132);

    // Families.
    strncpy(CHIMERE_COMMON(fnfamilies), ops.Get<string>("fnfamilies", "", chemprepdir + "/FAMILIES").c_str(), 132);

    {
      ifstream fin(CHIMERE_COMMON(fnfamilies));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnfamilies)) +
                         "\", probably wrong path.");

      CHIMERE_PARAMS(nfam) = 0;
      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nfam);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Bgreen() << Info() << LogReset()
                         << "Number of families (nfam) : " << CHIMERE_PARAMS(nfam) << endl;
#endif

    if (CHIMERE_PARAMS(bins) == 1)
      {
        strncpy(CHIMERE_COMMON(fnaerosol), ops.Get<string>("fnaerosol", "", chemprepdir + "/AEROSOL").c_str(), 132);

        ifstream fin(CHIMERE_COMMON(fnaerosol));

        if (! fin.good())
          throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnaerosol)) +
                           "\", probably wrong path.");

        string line;
        getline(fin, line);
        fin >> CHIMERE_PARAMS(ms);

        diameter_bound_.assign(CHIMERE_PARAMS(ms) + 1, real(0));

        getline(fin, line);
        getline(fin, line);
        {
          istringstream iss(line);
          for (int i = 0; i <= CHIMERE_PARAMS(ms); i++)
            iss >> diameter_bound_[i];
        }

        CHIMERE_PARAMS(dpmin) = diameter_bound_.front();
        CHIMERE_PARAMS(dpmax) = diameter_bound_.back();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << master << Bcyan() << Info() << LogReset()
                             << "Number of aerosol sections (ms)  : " << CHIMERE_PARAMS(ms) << endl;
        *AMCLogger::GetLog() << master << Fcyan() << Debug(1) << LogReset()
                             << "diameter_bound = " << diameter_bound_ << endl;
#endif

        getline(fin, line);
        getline(fin, line);

        CHIMERE_PARAMS(nkc) = 0;
        while (getline(fin, line))
          {
            int type, origin;
            real molar_mass;
            string species_name;
            istringstream iss(line);
            iss >> species_name >> type >> molar_mass >> origin;
            species_name_.push_back(species_name);
            ++CHIMERE_PARAMS(nkc);
          }
        fin.close();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << master << Bmagenta() << Info() << LogReset()
                             << "Number of aerosol species (nkc)  : " << CHIMERE_PARAMS(nkc) << endl;
        *AMCLogger::GetLog() << master << Fmagenta() << Debug(1) << LogReset()
                             << "species_name = " << species_name_ << endl;
#endif

        strncpy(CHIMERE_COMMON(fnprim), ops.Get<string>("fnprim", "", chemprepdir + "/PRIMARY").c_str(), 132);
      }
    else
      {
        CHIMERE_PARAMS(ms) = 1;
        CHIMERE_PARAMS(nkc) = 1;
        CHIMERE_PARAMS(dpmin) = double(1.e-3);
        CHIMERE_PARAMS(dpmax) = double(10.);
      }

    if (CHIMERE_PARAMS(aero) == 1)
      {
        strncpy(CHIMERE_COMMON(fnaeromin), ops.Get<string>("fnaeromin", "", chemprepdir + "/AEROMIN.bin").c_str(), 132);
        strncpy(CHIMERE_COMMON(fnaeroorg), ops.Get<string>("fnaeroorg", "", chemprepdir + "/AEROORG.bin").c_str(), 132);
      }

    strncpy(CHIMERE_COMMON(fnsemivol), ops.Get<string>("fnsemivol", "", chemprepdir + "/SEMIVOL").c_str(), 132);

    if (CHIMERE_PARAMS(pops) == 1)
      {
        strncpy(CHIMERE_COMMON(fnveget),  ops.Get<string>("fnveget", "", chemprepdir + "/VEGETATION").c_str(), 132);
        strncpy(CHIMERE_COMMON(fnsea), ops.Get<string>("fnsea", "", chemprepdir + "/SEA").c_str(), 132);
        strncpy(CHIMERE_COMMON(fnsoil), ops.Get<string>("fnsoil", "", chemprepdir + "/SOIL").c_str(), 132);
      }

    // Anthropic emissions.
    strncpy(CHIMERE_COMMON(fnanthro), ops.Get<string>("fnanthro", "", chemprepdir + "/ANTHROPIC").c_str(), 132);

    {
      ifstream fin(CHIMERE_COMMON(fnanthro));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnanthro)) + "\", probably wrong path.");

      CHIMERE_PARAMS(nemisa) = 0;
      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nemisa);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fred() << Debug() << LogReset()
                         << "Number of anthropic emission species (nemisa) : " << CHIMERE_PARAMS(nemisa) << endl;
#endif

    // biogenic emissions.
    strncpy(CHIMERE_COMMON(fnbiogen), ops.Get<string>("fnbiogen", "", chemprepdir + "/BIOGENIC").c_str(), 132);

    {
      ifstream fin(CHIMERE_COMMON(fnbiogen));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnbiogen)) + "\", probably wrong path.");

      CHIMERE_PARAMS(nemisb) = 0;
      string line;
      while (getline(fin, line))
        ++CHIMERE_PARAMS(nemisb);

      fin.close();
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fgreen() << Debug() << LogReset()
                         << "Number of biogenic emission species (nemisb) : " << CHIMERE_PARAMS(nemisb) << endl;
#endif

    // Dry and wet deposition.
    strncpy(CHIMERE_COMMON(fndepoespe), ops.Get<string>("fndepoespe", "", chemprepdir + "/DEPO_SPEC").c_str(), 132);
    strncpy(CHIMERE_COMMON(fndepopars), ops.Get<string>("fndepopars", "", chemprepdir + "/DEPO_PARS").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnwetd), ops.Get<string>("fnwetd", "", chemprepdir + "/WETD_SPEC").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnlanduse),
            ops.Get<string>("fnlanduse", "", domaindir + "/" + to_str(CHIMERE_PARAMS(domain))
                            + "/LANDUSE_" + to_str(CHIMERE_PARAMS(domain))).c_str(), 132);

    // Emissions.
    strncpy(CHIMERE_COMMON(fnemisa), ops.Get<string>("fnemisa", "", chimere_tmp + "/AEMISSIONS.nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnemisb), ops.Get<string>("fnemisb", "", chimere_tmp + "/BEMISSIONS.nc").c_str(), 132);

    // Boundary conditions.
    strncpy(CHIMERE_COMMON(fnbounconc), ops.Get<string>("fnbounconc", "", chimere_tmp + "/BOUN_CONCS.nc").c_str(), 132);

    // Meteorology.
    strncpy(CHIMERE_COMMON(fnmeteo), ops.Get<string>("fnmeteo", "", chimere_tmp + "/METEO.nc").c_str(), 132);

    // Results.
    string simuldir = ops.Get<string>("simuldir");
    string label = ops.Get<string>("label", "", ClassDriverBase::prefix_);
    string sim = date_begin + "_" + date_end + "_" + label;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Bcyan() << Info() << LogReset()
                         << "Output of simulation in directory \"" << simuldir << "\"" << endl;
    *AMCLogger::GetLog() << master << Fcyan() << Debug() << LogReset() << "sim = " << sim << endl;
#endif

    CHIMERE_COMMON(iopinit) = 1;
    strncpy(CHIMERE_COMMON(fninit), ops.Get<string>("fninit", "", simuldir + "/ini." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fniniex), ops.Get<string>("fniniex", "", simuldir + "/iex." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnout), ops.Get<string>("fnout", "", simuldir + "/out." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnexc), ops.Get<string>("fnexc", "", simuldir + "/exc." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnothers), ops.Get<string>("fnothers", "", simuldir + "/par." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnconcs), ops.Get<string>("fnconcs", "", simuldir + "/end." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fnendex), ops.Get<string>("fnendex", "", simuldir + "/exe." + sim + ".nc").c_str(), 132);
    strncpy(CHIMERE_COMMON(fndepos), ops.Get<string>("fndepos", "", simuldir + "/dep." + sim + ".nc").c_str(), 132);

    //
    // Parallel configuration.
    //

    MPI::COMM_WORLD.Barrier();

    // Get MPI variables from configuration file, if any.
    CHIMERE_PARAMS(nzdoms) = ops.Get<int>("nzdoms", "", 0);
    CHIMERE_PARAMS(nmdoms) = ops.Get<int>("nmdoms", "", 0);

    // Get MPI variables from environment, if any,
    // they take precedence over those of configuration file.
    char *nzdoms = getenv("NZDOMS");
    if (nzdoms != NULL)
      {
        istringstream iss(nzdoms);
        iss >> CHIMERE_PARAMS(nzdoms);
      }

    char *nmdoms = getenv("NMDOMS");
    if (nmdoms != NULL)
      {
        istringstream iss(nmdoms);
        iss >> CHIMERE_PARAMS(nmdoms);
      }

    // If nothing was given try to guess.
    const int ndoms = ClassDriverBase::Nrank_ - 1;

    if (CHIMERE_PARAMS(nzdoms) * CHIMERE_PARAMS(nmdoms) > 0)
      {
        const int Nrank = CHIMERE_PARAMS(nzdoms) * CHIMERE_PARAMS(nmdoms) + 1;
        if (ClassDriverBase::Nrank_ != Nrank)
          throw AMC::Error("Chimere launched with : \"mpirun -np " + to_str(ClassDriverBase::Nrank_) +
                           " ...\", expected \"mpirun -np " + to_str(Nrank) + " ...\" according to configuration.");
      }
    else if (CHIMERE_PARAMS(nzdoms) > 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << master << Fmagenta() << Info() << LogReset()
                             << "Try to guess nmdoms from nzdoms (" << CHIMERE_PARAMS(nzdoms)
                             << ") given in configuration and -np option (" << ClassDriverBase::Nrank_ << ")." << endl;
#endif
        if (ndoms % CHIMERE_PARAMS(nzdoms) > 0)
          throw AMC::Error("Cannot find nmdoms with -np " + to_str(ClassDriverBase::Nrank_) +
                           " and nzdoms = " + to_str(CHIMERE_PARAMS(nzdoms)));
        else
          CHIMERE_PARAMS(nmdoms) = ndoms / CHIMERE_PARAMS(nzdoms);
      }
    else if (CHIMERE_PARAMS(nmdoms) > 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << master << Fmagenta() << Info() << LogReset()
                             << "Try to guess nzdoms from nmdoms (" << CHIMERE_PARAMS(nmdoms)
                             << ") given in configuration and -np option (" << ClassDriverBase::Nrank_ << ")." << endl;
#endif
        if (ndoms % CHIMERE_PARAMS(nmdoms) > 0)
          throw AMC::Error("Cannot find nzdoms with -np " + to_str(ClassDriverBase::Nrank_) +
                           " and nmdoms = " + to_str(CHIMERE_PARAMS(nmdoms)));
        else
          CHIMERE_PARAMS(nzdoms) = ndoms / CHIMERE_PARAMS(nmdoms);
      }
    else
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << master << Fmagenta() << Info() << LogReset() << "Neither nzdoms nor nmdoms given in"
                             << " configuration file, try go guess best values from -np option ("
                             << ClassDriverBase::Nrank_ << ")." << endl;
#endif
        // Split domain takes the domain size as inputs.
        CHIMERE_PARAMS(nzdoms) = CHIMERE_PARAMS(nzonal);
        CHIMERE_PARAMS(nmdoms) = CHIMERE_PARAMS(nmerid);

        // It returns the splitted domain in both variables.
        AMC::split_domain(ndoms, CHIMERE_PARAMS(nzdoms), CHIMERE_PARAMS(nmdoms));
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Byellow() << Info() << LogReset() << "Parallel configuration : ("
                         << CHIMERE_PARAMS(nzdoms) << ", " << CHIMERE_PARAMS(nmdoms) << ")." << endl;
#endif

    MPI::COMM_WORLD.Barrier();

    int nzonal_o = CHIMERE_PARAMS(nzonal) / CHIMERE_PARAMS(nzdoms);
    int nzonal_r = CHIMERE_PARAMS(nzonal) - nzonal_o * (CHIMERE_PARAMS(nzdoms) - 1);

    int nmerid_o = CHIMERE_PARAMS(nmerid) / CHIMERE_PARAMS(nmdoms);
    int nmerid_u = CHIMERE_PARAMS(nmerid) - nmerid_o * (CHIMERE_PARAMS(nmdoms) - 1);

    WORKER_PARAMS(nzonalmax) = nzonal_r > nzonal_o ? nzonal_r : nzonal_o;
    WORKER_PARAMS(nmeridmax) = nmerid_u > nmerid_o ? nmerid_u : nmerid_o;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << master << Fcyan() << Debug() << LogReset() << "nzonalmax = " << WORKER_PARAMS(nzonalmax) << endl;
    *AMCLogger::GetLog() << master << Fcyan() << Debug() << LogReset() << "nmeridmax = " << WORKER_PARAMS(nmeridmax) << endl;
#endif

    // Cell to show.
    CHIMERE_PARAMS(nsho) = CHIMERE_PARAMS(nzonal) * CHIMERE_PARAMS(nzonal) / 2 + CHIMERE_PARAMS(nzonal);

    // Number of levels in ouput file.
    CHIMERE_PARAMS(nivout) = ops.Get<int>("levout");

    // Number of emission levels.
    CHIMERE_PARAMS(nlevemis) = CHIMERE_PARAMS(nvert_raw);

    // ivsurf used to be Fortran parameter that we have to move to a variable,
    // because it has to be known ahead of Fortran, due to boundary conditions.
    // Its default value is the one found in chimere_params.f90 : "Emission sublayer (2) or not (1)".
    CHIMERE_PARAMS(ivsurf) = ops.Get<int>("ivsurf", "", 1);

    ops.Close();

    // Check file names.
    file_name_["fnoutspec"] = CHIMERE_COMMON(fnoutspec);
    file_name_["fnspec"] = CHIMERE_COMMON(fnspec);
    file_name_["fnchem"] = CHIMERE_COMMON(fnchem);
    file_name_["fnstoi"] = CHIMERE_COMMON(fnstoi);
    file_name_["fnphot"] = CHIMERE_COMMON(fnphot);
    file_name_["fnrates"] = CHIMERE_COMMON(fnrates);
    file_name_["fnfamilies"] = CHIMERE_COMMON(fnfamilies);
    file_name_["fnaerosol"] = CHIMERE_COMMON(fnaerosol);
    file_name_["fnprim"] = CHIMERE_COMMON(fnprim);
    file_name_["fnaeromin"] = CHIMERE_COMMON(fnaeromin);
    file_name_["fnaeroorg"] = CHIMERE_COMMON(fnaeroorg);
    file_name_["fnsemivol"] = CHIMERE_COMMON(fnsemivol);
    file_name_["fnveget"] = CHIMERE_COMMON(fnveget);
    file_name_["fnsea"] = CHIMERE_COMMON(fnsea);
    file_name_["fnsoil"] = CHIMERE_COMMON(fnsoil);
    file_name_["fnanthro"] = CHIMERE_COMMON(fnanthro);
    file_name_["fnbiogen"] = CHIMERE_COMMON(fnbiogen);
    file_name_["fndepoespe"] = CHIMERE_COMMON(fndepoespe);
    file_name_["fndepopars"] = CHIMERE_COMMON(fndepopars);
    file_name_["fnwetd"] = CHIMERE_COMMON(fnwetd);
    file_name_["fnlanduse"] = CHIMERE_COMMON(fnlanduse);
    file_name_["fnemisa"] = CHIMERE_COMMON(fnemisa);
    file_name_["fnemisb"] = CHIMERE_COMMON(fnemisb);
    file_name_["fnbounconc"] = CHIMERE_COMMON(fnbounconc);
    file_name_["fnmeteo"] = CHIMERE_COMMON(fnmeteo);
    file_name_["fninit"] = CHIMERE_COMMON(fninit);
    file_name_["fniniex"] = CHIMERE_COMMON(fniniex);
    file_name_["fnout"] = CHIMERE_COMMON(fnout);
    file_name_["fnexc"] = CHIMERE_COMMON(fnexc);
    file_name_["fnothers"] = CHIMERE_COMMON(fnothers);
    file_name_["fnconcs"] = CHIMERE_COMMON(fnconcs);
    file_name_["fnendex"] = CHIMERE_COMMON(fnendex);
    file_name_["fndepos"] = CHIMERE_COMMON(fndepos);

#ifdef AMC_WITH_LOGGER
    MPI::COMM_WORLD.Barrier();
    for (map<string, char*>::iterator it = file_name_.begin(); it != file_name_.end(); ++it)
      *AMCLogger::GetLog() << master << Fgreen() << Debug(3) << LogReset() << it->first << " = \"" << it->second << "\"" << endl;
    MPI::COMM_WORLD.Barrier();
#endif

    // Check some of the most compulsory files.
    util::check_file(string(CHIMERE_COMMON(fninit)));
    util::check_file(string(CHIMERE_COMMON(fnmeteo)));
    util::check_file(string(CHIMERE_COMMON(fnemisa)));
    util::check_file(string(CHIMERE_COMMON(fnemisb)));
    util::check_file(string(CHIMERE_COMMON(fnbounconc)));

    // Chimere parameters on master side.
    parameter_["aero"] = &CHIMERE_PARAMS(aero);
    parameter_["bins"] = &CHIMERE_PARAMS(bins);
    parameter_["carb"] = &CHIMERE_PARAMS(carb);
    parameter_["dust"] = &CHIMERE_PARAMS(dust);
    parameter_["dustout"] = &CHIMERE_PARAMS(dustout);
    parameter_["ibound"] = &CHIMERE_PARAMS(ibound);
    parameter_["ichemstep"] = &CHIMERE_PARAMS(ichemstep);
    parameter_["ideepconv"] = &CHIMERE_PARAMS(ideepconv);
    parameter_["ihoursu"] = &CHIMERE_PARAMS(ihoursu);
    parameter_["ms"] = &CHIMERE_PARAMS(ms);
    parameter_["nemisa"] = &CHIMERE_PARAMS(nemisa);
    parameter_["nemisb"] = &CHIMERE_PARAMS(nemisb);
    parameter_["nequil"] = &CHIMERE_PARAMS(nequil);
    parameter_["nfam"] = &CHIMERE_PARAMS(nfam);
    parameter_["nitgs"] = &CHIMERE_PARAMS(nitgs);
    parameter_["nitgssu"] = &CHIMERE_PARAMS(nitgssu);
    parameter_["nivout"] = &CHIMERE_PARAMS(nivout);
    parameter_["nlevemis"] = &CHIMERE_PARAMS(nlevemis);
    parameter_["nkc"] = &CHIMERE_PARAMS(nkc);
    parameter_["nmdoms"] = &CHIMERE_PARAMS(nmdoms);
    parameter_["nmerid"] = &CHIMERE_PARAMS(nmerid);
    parameter_["npeq"] = &CHIMERE_PARAMS(npeq);
    parameter_["nphour_ref"] = &CHIMERE_PARAMS(nphour_ref);
    parameter_["nreac"] = &CHIMERE_PARAMS(nreac);
    parameter_["nsaveconcs"] = &CHIMERE_PARAMS(nsaveconcs);
    parameter_["nsavedepos"] = &CHIMERE_PARAMS(nsavedepos);
    parameter_["nsho"] = &CHIMERE_PARAMS(nsho);
    parameter_["nspec"] = &CHIMERE_PARAMS(nspec);
    parameter_["nvert_raw"] = &CHIMERE_PARAMS(nvert_raw);
    parameter_["nzdoms"] = &CHIMERE_PARAMS(nzdoms);
    parameter_["nzonal"] = &CHIMERE_PARAMS(nzonal);
    parameter_["pops"] = &CHIMERE_PARAMS(pops);
    parameter_["reactive"] = &CHIMERE_PARAMS(reactive);
    parameter_["seasalt"] = &CHIMERE_PARAMS(seasalt);
    parameter_["soatyp"] = &CHIMERE_PARAMS(soatyp);
    parameter_["trc"] = &CHIMERE_PARAMS(trc);
    parameter_["ivsurf"] = &CHIMERE_PARAMS(ivsurf);

#ifdef AMC_WITH_LOGGER
    MPI::COMM_WORLD.Barrier();
    for (map<string, int*>::iterator it = parameter_.begin(); it != parameter_.end(); ++it)
      *AMCLogger::GetLog() << master << Fmagenta() << Debug(3) << LogReset()
                           << it->first << " = " << *(it->second) << endl;
    MPI::COMM_WORLD.Barrier();
#endif

    // Workers.
    WORKER_PARAMS(aero) = CHIMERE_PARAMS(aero);
    WORKER_PARAMS(bins) = CHIMERE_PARAMS(bins);
    WORKER_PARAMS(ms) = CHIMERE_PARAMS(ms);
    WORKER_PARAMS(nkc) = CHIMERE_PARAMS(nkc);
    WORKER_PARAMS(nspec) = CHIMERE_PARAMS(nspec);
    WORKER_PARAMS(nfam) = CHIMERE_PARAMS(nfam);
    WORKER_PARAMS(carb) = CHIMERE_PARAMS(carb);
    WORKER_PARAMS(dpmax) = CHIMERE_PARAMS(dpmax);
    WORKER_PARAMS(dpmin) = CHIMERE_PARAMS(dpmin);
    WORKER_PARAMS(dustout) = CHIMERE_PARAMS(dustout);
    WORKER_PARAMS(ibound) = CHIMERE_PARAMS(ibound);
    WORKER_PARAMS(ichemstep) = CHIMERE_PARAMS(ichemstep);
    WORKER_PARAMS(ideepconv) = CHIMERE_PARAMS(ideepconv);
    WORKER_PARAMS(ihoursu) = CHIMERE_PARAMS(ihoursu);
    WORKER_PARAMS(nemisa) = CHIMERE_PARAMS(nemisa);
    WORKER_PARAMS(nemisb) = CHIMERE_PARAMS(nemisb);
    WORKER_PARAMS(nequil) = CHIMERE_PARAMS(nequil);
    WORKER_PARAMS(nitgs) = CHIMERE_PARAMS(nitgs);
    WORKER_PARAMS(nitgssu) = CHIMERE_PARAMS(nitgssu);
    WORKER_PARAMS(nivout) = CHIMERE_PARAMS(nivout);
    WORKER_PARAMS(nlevemis) = CHIMERE_PARAMS(nlevemis);
    WORKER_PARAMS(nmdoms) = CHIMERE_PARAMS(nmdoms);
    WORKER_PARAMS(npeq) = CHIMERE_PARAMS(npeq);
    WORKER_PARAMS(nphour_ref) = CHIMERE_PARAMS(nphour_ref);
    WORKER_PARAMS(nreac) = CHIMERE_PARAMS(nreac);
    WORKER_PARAMS(nsaveconcs) = CHIMERE_PARAMS(nsaveconcs);
    WORKER_PARAMS(nsavedepos) = CHIMERE_PARAMS(nsavedepos);
    WORKER_PARAMS(nsho) = CHIMERE_PARAMS(nsho);
    WORKER_PARAMS(nvert_raw) = CHIMERE_PARAMS(nvert_raw);
    WORKER_PARAMS(nzdoms) = CHIMERE_PARAMS(nzdoms);
    WORKER_PARAMS(pops) = CHIMERE_PARAMS(pops);
    WORKER_PARAMS(reactive) = CHIMERE_PARAMS(reactive);
    WORKER_PARAMS(seasalt) = CHIMERE_PARAMS(seasalt);
    WORKER_PARAMS(soatyp) = CHIMERE_PARAMS(soatyp);
    WORKER_PARAMS(trc) = CHIMERE_PARAMS(trc);
    WORKER_PARAMS(ivsurf) = CHIMERE_PARAMS(ivsurf);
  }


#ifdef CHIMERE_WITH_AMC
  // Write Chimere configuration.
  void ClassChimereBase::WriteConfiguration(const string suffix)
  {
    //
    // Active species file.
    //

    {
      ifstream fin(CHIMERE_COMMON(fnspec));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnspec)) + "\", probably wrong path.");

      strncpy(CHIMERE_COMMON(fnspec), (string(CHIMERE_COMMON(fnspec)) + "." + suffix).c_str(), 132);

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset()
                           << "Write species list for AMC (to be read by Chimere)"
                           << " in file \"" << CHIMERE_COMMON(fnspec) << "\"." << endl;
#endif

      ofstream fout(CHIMERE_COMMON(fnspec));

      if (! fout.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnspec)) + "\", probably wrong path.");

      const int ngassp = CHIMERE_PARAMS(nspec) - CHIMERE_PARAMS(ms) * CHIMERE_PARAMS(nkc);
      for (int i = 0; i < ngassp; ++i)
        {
          int j, k;
          string name;
          
          fin >> j >> name >> k;
          fout << setw(7) << left << j << setw(15) << left << name << setw(2) << right << k << endl;
        }

      fin.close();

      int i(ngassp);

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
      // Only if we actually treat number concentration outside of AMC.
      for (int j = 1; j <= CHIMERE_AMC.Ng; ++j)
        fout << setw(7) << left << ++i << setw(15)
             << left << "p" << setw(2) << setfill('0') << j
             << "Number" << setw(2) << right << 2 << endl;
#endif

      for (int j = 1; j <= CHIMERE_AMC.Ng; ++j)
        {
          ostringstream sbin;
          sbin << "p" << setw(2) << setfill('0') << j;

          for (int k = 0; k < CHIMERE_AMC.Nspecies; ++k)
            fout << setw(7) << left << ++i << setw(15)
                 << left << sbin.str() << AMC::ClassSpecies::GetName(k)
                 << setw(2) << right << 2 << endl;
        }

#ifdef CHIMERE_AMC_WITH_WATER
      // Liquid water content explicitely treated by Chimere+AMC.
      for (int j = 1; j <= CHIMERE_AMC.Ng; ++j)
        fout << setw(7) << left << ++i << setw(15)
             << left << "p" << setw(2) << setfill('0') << j
             << "WATER" << setw(2) << right << 2 << endl;
#endif

      fout.close();
    }

    //
    // Output species file.
    //

    {
      ifstream fin(CHIMERE_COMMON(fnoutspec));

      if (! fin.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnoutspec)) + "\", probably wrong path.");

      strncpy(CHIMERE_COMMON(fnoutspec), (string(CHIMERE_COMMON(fnoutspec)) + "." + suffix).c_str(), 132);

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset()
                           << "Write output species list for AMC (to be read by Chimere)"
                           << " in file \"" << CHIMERE_COMMON(fnoutspec) << "\"." << endl;
      *AMCLogger::GetLog() << Bmagenta() << Warning() << "Species of the form \"p04XXXXX\" from previous file"
                           << " are believed to be aerosol variables and filtered out." << LogReset().Str() << endl;
#endif

      ofstream fout(CHIMERE_COMMON(fnoutspec));

      if (! fout.good())
        throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnoutspec)) + "\", probably wrong path.");

      regex_t regex;
      regcomp(&regex, "^p[0-9][0-9][a-zA-Z][0-9a-zA-Z_]*$", REG_EXTENDED);

      real xmass;
      string species;
      while (fin >> species >> xmass)
        if (regexec(&regex, species.c_str(), 0, NULL, 0) > 0)
          fout << setw(15) << left << species << setw(5) << right << xmass << endl;

      fin.close();

      regfree(&regex);

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
      // Only if we actually track number concentration outside of AMC.
      for (int i = 1; i <= CHIMERE_AMC.Ng; ++i)
        fout << setw(15) << left << "p" << setw(2) << setfill('0')
             << i << "Number" << setw(5) << right << 0. << endl;
#endif

      for (int i = 1; i <= CHIMERE_AMC.Ng; ++i)
        {
          ostringstream sbin;
          sbin << "p" << setw(2) << setfill('0') << i;

          for (int j = 0; j < CHIMERE_AMC.Nspecies; ++j)
            fout << setw(15) << left << sbin.str()
                 << AMC::ClassSpecies::GetName(j)
                 << setw(5) << right << 0. << endl;
        }

#ifdef CHIMERE_AMC_WITH_WATER
      // Liquid water content explicitely treated by Chimere+AMC.
      for (int i = 1; i <= CHIMERE_AMC.Ng; ++i)
        fout << setw(15) << left << "p" << setw(2) << setfill('0')
             << i << "WATER" << setw(5) << right << 0. << endl;
#endif

      fout.close();
    }
  }
#endif


  // Get methods.
  int ClassChimereBase::GetParameter(const string name)
  {
    auto it = parameter_.find(name);
    return it != parameter_.end() ? *(it->second) : -999;
  }


  string ClassChimereBase::GetFileName(const string name)
  {
    auto it = file_name_.find(name);
    return it != file_name_.end() ? string(it->second) : "Not found";
  }


  map<string, int> ClassChimereBase::GetParameter()
  {
    map<string, int> parameter;
    for (map<string, int*>::iterator it = parameter_.begin(); it != parameter_.end(); it++)
      parameter[it->first] = *(it->second);
    return parameter;
  }


  map<string, string> ClassChimereBase::GetFileName()
  {
    map<string, string> file_name;
    for (map<string, char*>::iterator it = file_name_.begin(); it != file_name_.end(); it++)
      file_name[it->first] = string(it->second);
    return file_name;
  }


  double ClassChimereBase::GetConcentration(const int s, const int z, const int y, const int x)
  {
    // The +3 in x and y concentrations accounts for the shift
    // in fortran arrays when allocation is made (see upper).
    // Then, x = 0 and y = 0 corresponds to the eastern
    // and southern most true grid cell in Chimere domain,
    // which in fortran corresponds to x = 1 and y = 1.
    if (ClassDriverBase::rank_ == 0)
      return concentration_chimere_(x + 3, y + 3, z, s);
    else
      return concentration_worker_(x + 3, y + 3, z, s);
  }


  // Clear static data.
  void ClassChimereBase::Clear()
  {
    is_running_ = false;
    file_name_.clear();
    parameter_.clear();
    diameter_bound_.clear();
    species_name_.clear();
    // Blitz arrays must NOT be freed.
  }


  // Is running ?
  bool ClassChimereBase::IsRunning()
  {
    return is_running_;
  }
}

#define AMC_FILE_CLASS_CHIMERE_BASE_CXX
#endif
