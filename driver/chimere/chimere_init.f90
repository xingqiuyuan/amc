subroutine chimere_init

  use chimere_common
  use master_message_subs
  implicit none

  ! initial time-step values
  dtr=3600d0/(nphour_ref*ichemstep)
  dtr2=2d0*dtr/3d0
  nphour=nphour_ref

  !  All readings and initializations
  call init_mpi

  if(rank==0) call inichimere

end subroutine chimere_init
