// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_EMISSION_ANTHROPIC_CXX

#include "ClassChimereEmissionAnthropic.hxx"

namespace Driver
{
  // Find tracer index.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::find_tracer_index()
  {
    tracer_index_.assign(ClassTracer::Nanthropic_, -1);

    for (int i = 0; i < ClassTracer::Nanthropic_; ++i)
      {
        // Index in Chimere.
        for (int j = 0; j < int(tracer_name_.size()); ++j)
          if (tracer_name_[j] == ClassTracer::name_[i])
            {
              tracer_index_[i] = j;
              break;
            }

        if (tracer_index_[i] < 0)
          throw AMC::Error("Could not find AMC emission tracer \"" + ClassTracer::name_[i] +
                           "\" in Chimere anthropic emission species list.");
      }
  }


  // Allocate.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::Allocate()
  {
    // allocate(emisaloc (nemisa,nzonal,nmerid,nlevemis))
    // allocate(emipaloc (nkc,ms,nzonal,nmerid,nlevemis))
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info() << LogReset() << "Master links fortran 90 Chimere"
                         << " emission fields to C++ blitz::Array<> : emisaloc, emipaloc." << endl;
#endif

    emission_.reference(Array<float, 4>(CHIMERE_COMMON(emisaloc),
                                        shape(CHIMERE_PARAMS(nlevemis),
                                              CHIMERE_PARAMS(nmerid),
                                              CHIMERE_PARAMS(nzonal),
                                              CHIMERE_PARAMS(nemisa)),
                                        neverDeleteData));
#ifdef CHIMERE_WITH_AMC
    emission_particle_.reference(Array<double, 5>(CHIMERE_COMMON(emipaloc),
                                                  shape(CHIMERE_PARAMS(nlevemis),
                                                        CHIMERE_PARAMS(nmerid),
                                                        CHIMERE_PARAMS(nzonal),
                                                        CHIMERE_AMC.Ng,
                                                        CHIMERE_AMC.Nvariable),
                                                  neverDeleteData));
#else
    emission_particle_.reference(Array<double, 5>(CHIMERE_COMMON(emipaloc),
                                                  shape(CHIMERE_PARAMS(nlevemis),
                                                        CHIMERE_PARAMS(nmerid),
                                                        CHIMERE_PARAMS(nzonal),
                                                        CHIMERE_PARAMS(ms),
                                                        CHIMERE_PARAMS(nkc)),
                                                  neverDeleteData));
#endif
  }


  // Init.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::Init()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Init anthropic Chimere emission subsystem"
                         << " (only when Chimere runs with AMC)." << endl;
#endif

    // Grab Chimere tracer names. Should be the same as AMC.
    ifstream fin(CHIMERE_COMMON(fnanthro));

    if (! fin.good())
      throw AMC::Error("Error opening file \"" + to_str(CHIMERE_COMMON(fnanthro)) +
                       "\", probably wrong path.");

    string input;
    while (fin >> input)
      tracer_name_.push_back(input);

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "tracer_name = " << tracer_name_ << endl;
#endif

    if (int(tracer_name_.size()) != CHIMERE_PARAMS(nemisa))
      throw AMC::Error("Read number of anthropic tracers (" + to_str(tracer_name_.size()) +
                       ") mismatches the internal Chimere value \"nemisa\" (" +
                       to_str(CHIMERE_PARAMS(nemisa)) +").");

    // Make the link between Chimere and AMC on the tracer level.
    find_tracer_index();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "tracer_index = " << tracer_index_ << endl;
#endif

    // Allocate working arrays.
    emission_rate_mass_work_.assign(ClassTracer::Ntracer_, real(0));
    rate_aer_number_work_.assign(CHIMERE_AMC.Ng, real(0));
    rate_aer_mass_work_.assign(CHIMERE_AMC.Ng * CHIMERE_AMC.Nspecies, real(0));
  }


  // Get methods.
  template<class D>
  float ClassChimereEmissionAnthropic<D>::GetEmission(const int s, const int z, const int y, const int x)
  {
    // This should only be done in the MPI master process.
    return emission_(x, y, z, s);
  }


  template<class D>
  double ClassChimereEmissionAnthropic<D>::GetEmissionParticle(const int s, const int b,
                                                               const int z, const int y, const int x)
  {
    // This should only be done in the MPI master process.
    return emission_particle_(x, y, z, b, s);
  }


  // Clear static data.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::Clear()
  {
    tracer_name_.clear();
    tracer_index_.clear();
    emission_rate_mass_work_.clear();
    rate_aer_number_work_.clear();
    rate_aer_mass_work_.clear();

    // Blitz arrays must NOT be freed.
  }


  // Compute source rate.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::ComputeSourceRate()
  {
    for (int z = 0; z < CHIMERE_PARAMS(nlevemis); ++z)
      for (int y = 0; y < CHIMERE_PARAMS(nmerid); ++y)
        for (int x= 0; x < CHIMERE_PARAMS(nzonal); ++x)
          {
            Array<float, 1> emission_local = emission_(x, y, z, Range::all());
            Array<double, 2> emission_particle_local = emission_particle_(x, y, z, Range::all(), Range::all());

            for (int i = 0; i < ClassTracer::Nanthropic_; ++i)
              emission_rate_mass_work_[i] = double(emission_local(tracer_index_[i]));

            rate_aer_number_work_.assign(CHIMERE_AMC.Ng, real(0));
            rate_aer_mass_work_.assign(CHIMERE_AMC.Ng * CHIMERE_AMC.Nspecies, real(0));

            for (int i = 0; i < ClassSource::Nanthropic_; ++i)
              ClassDynamicsEmission<D>::rate(i,
                                             emission_rate_mass_work_,
                                             rate_aer_number_work_,
                                             rate_aer_mass_work_);

            int h(-1);
            for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
              {
                for (int j = 0; j < CHIMERE_AMC.Nspecies; ++j)
                  emission_particle_local(i, j) += rate_aer_mass_work_[++h];

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
                emission_particle_local(i, CHIMERE_AMC.Nspecies) += rate_aer_number_work_[i];
#endif
              }
          }
  }


#if defined(CHIMERE_AMC_WITH_PREPROCESSING) && defined(DRIVER_WITH_NETCDF)
  // Preprocess source general section index.
  template<class D>
  void ClassChimereEmissionAnthropic<D>::PreProcessSourceGeneralSection(const string netcdf_file)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Preprocess anthropic source AMC general section"
                         << " with emission file \"" << netcdf_file << "\"." << endl;
#endif

    if (! ClassDynamicsEmission<D>::source_count_general_section_)
      throw AMC::Error("Source general section counting not enabled in emission dynamics class.");

    //
    // Load emission file.
    //

    NcFile fnemission(netcdf_file.c_str(), NcFile::ReadOnly);

    if (! fnemission.is_valid())
      throw AMC::Error("Unable to open NetCDF file \"" + netcdf_file + "\".");

    // Grab dimensions.
    const int Nt = int(fnemission.get_dim("Time")->size());
    const int Ns =  int(fnemission.get_dim("Species")->size());
    const int Nz =  int(fnemission.get_dim("bottom_top")->size());
    const int Ny =  int(fnemission.get_dim("south_north")->size());
    const int Nx =  int(fnemission.get_dim("west_east")->size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << LogReset()
                         << "Dimensions from NetCDf file : (Nt, Ns, Nz, Ny, Nx) = (" << Nt
                         << ", " << Ns << ", " << Nz << ", " << Ny << ", " << Nx << ")" << endl;
#endif

    // Retrieve species list.
    const int sp_str_len = int(fnemission.get_dim("SpStrLen")->size());

    tracer_name_.assign(Ns, string(sp_str_len, ' '));
    {
      NcVar *var = fnemission.get_var("species");
      for (int i = 0; i < Ns; ++i)
        {
          var->set_cur(i, 0);
          var->get(const_cast<char*>(tracer_name_[i].data()), 1, sp_str_len);
        }
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset()
                         << "Found " << Ns << " tracer species in NetCDF file : " << tracer_name_ << endl;
#endif

    // Build index.
    find_tracer_index();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "tracer_index = " << tracer_index_ << endl;
#endif

    // From AMC.
    const int Ng = AMC::ClassAerosolData::GetNg();
    const int Nspecies = AMC::ClassSpecies::GetNspecies();
#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
    const int Nvariable = Nspecies + 1;
#else
    const int Nvariable = Nspecies;
#endif

    // Allocate arrays.
    emission_.resize(Nx, Ny, Nz, Ns);
    emission_ = float(0);

    emission_particle_.resize(Nx, Ny, Nz, Ng, Nvariable);
    emission_particle_ = double(0);

    // Allocate working arrays.
    emission_rate_mass_work_.assign(ClassTracer::Ntracer_, real(0));
    rate_aer_number_work_.assign(Ng, real(0));
    rate_aer_mass_work_.assign(Ng * Nspecies, real(0));

    //
    // Main loop.
    //

#ifdef AMC_WITH_TIMER
    const int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_emission_anthropic");
#endif

    // NetCDF species variables.
    vector<NcVar*> var(Ns, NULL);
    for (int s = 0; s < Ns; ++s)
      var[s] = fnemission.get_var(tracer_name_[s].c_str());

    // A buffer is necessary here to overcome non-contiguous memory fields.
    Array<float, 3> buffer(Nz, Ny, Nx);

    // Time loop.
    for (int t = 0; t < Nt; ++t)
      {
        // Grab emission data.
        for (int s = 0; s < Ns; ++s)
          {
            var[s]->set_cur(t, 0, 0, 0);
            var[s]->get(buffer.data(), 1, Nz, Ny, Nx);

            for (int z = 0; z < Nz; ++z)
              for (int y = 0; y < Ny; ++y)
                for (int x = 0; x < Nx; ++x)
                  emission_(x, y, z, s) = buffer(z, y, x);
          }

#ifdef AMC_WITH_TIMER
        AMC::AMCTimer<AMC::CPU>::Start();
#endif

        // Space loop.
        for (int z = 0; z < Nz; ++z)
          for (int y = 0; y < Ny; ++y)
            for (int x = 0; x < Nx; ++x)
              {
                Array<float, 1> emission_local = emission_(x, y, z, Range::all());
                Array<double, 2> emission_particle_local = emission_particle_(x, y, z, Range::all(), Range::all());

                for (int i = 0; i < ClassTracer::Nanthropic_; ++i)
                  emission_rate_mass_work_[i] = double(emission_local(tracer_index_[i]));

                rate_aer_number_work_.assign(Ng, real(0));
                rate_aer_mass_work_.assign(Ng * Nspecies, real(0));

                for (int i = 0; i < ClassSource::Nanthropic_; ++i)
                  ClassDynamicsEmission<D>::rate(i,
                                                 emission_rate_mass_work_,
                                                 rate_aer_number_work_,
                                                 rate_aer_mass_work_);

                int h(-1);
                for (int i = 0; i < Ng; ++i)
                  {
                    for (int j = 0; j < Nspecies; ++j)
                      emission_particle_local(i, j) += rate_aer_mass_work_[++h];

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
                    emission_particle_local(i, Nspecies) += rate_aer_number_work_[i];
#endif
                  }
              }

#ifdef AMC_WITH_TIMER
        AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#endif
      }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset()
                         << "Performed " << Nt * Nz * Ny * Nx << " emission calls in "
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds (" << setprecision(12)
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) / real(Nt * Nz * Ny * Nx)
                         << " seconds/call) with anthropic emission file \"" << netcdf_file << "\"." << endl;
#endif
#endif

    // We no more need it.
    fnemission.close();

    // Deallocate.
    emission_.free();
    emission_particle_.free();
  }
#endif
}

#define AMC_FILE_CLASS_CHIMERE_EMISSION_ANTHROPIC_CXX
#endif
