// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
//
// This file is part of AMC.
//
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CHIMERE_AMC_HXX

//
// Functions to be called from the CHIMERE fortran 90 source.
// It is necessary to externalize them in order to avoid C++
// name mangling. It is also mandatory to append each function
// with one (and only one) underscore, which is achieved by
// making a weak alias of given function.
//

#pragma weak chimere_amc_set_molar_mass_ = chimere_amc_set_molar_mass
#pragma weak chimere_amc_emission_anthropic_ = chimere_amc_emission_anthropic
#pragma weak chimere_amc_emission_biogenic_ = chimere_amc_emission_biogenic
#pragma weak chimere_amc_boundary_condition_allocate_ = chimere_amc_boundary_condition_allocate
#pragma weak chimere_amc_boundary_condition_init_ = chimere_amc_boundary_condition_init
#pragma weak chimere_amc_boundary_condition_lateral_ = chimere_amc_boundary_condition_lateral
#pragma weak chimere_amc_boundary_condition_top_ = chimere_amc_boundary_condition_top


extern "C"
{
  void chimere_amc_set_molar_mass(double *molarm);
  void chimere_amc_emission_anthropic();
  void chimere_amc_emission_biogenic();
  void chimere_amc_boundary_condition_allocate();
  void chimere_amc_boundary_condition_init(char* usedval, int len_usedval, char* notusedval, int len_notusedval);
  void chimere_amc_boundary_condition_lateral(const int *ksens, const float *buf3);
  void chimere_amc_boundary_condition_top(const int *ksens, const float *buf3);
}

#include "util/util.hxx"
#include "../DriverHeader.hxx"

#include "../../base/ClassAMC.cxx"
#include "../../discretization/ClassDiscretizationComposition.cxx"
#include "../../discretization/ClassDiscretizationClass.cxx"
#include "../../discretization/ClassDiscretization.cxx"

#include "../emission/ClassDynamicsEmission.cxx"
#include "ClassChimereEmissionAnthropic.cxx"
#include "ClassChimereEmissionBiogenic.cxx"

#include "../mapping/ClassMappingParticle.cxx"
// We could instantiate particle mapping here, it is done in the driver instead.  However, it is left here
// commented to remind that some particular declaration could be needed :
//template<class D> bool Driver::ClassMappingParticle<D>::is_initialized_ = false;
//template<class D> Driver::real Driver::ClassMappingParticle<D>::AMCP::density_fixed_factor_ = real(0);
#include "ClassChimereBoundaryCondition.cxx"

namespace AMC
{
#include "../../typedef.hxx"
}
#include "ChimereTemplate.hxx"


#define AMC_FILE_CHIMERE_AMC_HXX
#endif
