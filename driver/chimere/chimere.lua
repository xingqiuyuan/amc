
require("os")

-- Global path.
home_path = os.getenv("HOME")

--
-- Logging
--

-- Logging section, can be removed or commented,
-- then nothing will be logged at all.
require("level")
logging = {
   -- We log everything to standard output.
   stdout = {level = LEVEL_ALL},
   -- Log all non debug messages.
   message = {file = "./test-chimere-message.log", level = LEVEL_DEFAULT},
   -- All debug messages goes into a special file, along with
   -- some user defined messages, which are generally a kind of debug.
   debug = {file = "./test-chimere-debug.log", level = {LEVEL_DEBUG, LEVEL_USER}}
}

--
-- The mapping required by boundary conditions
--

require("mapping")

--
-- In following configuration, not commented out fields are mandatory, other
-- ones are optional and can be left as is if you agree with default values.
--

default = {
   amc = {
      configuration_file = "./test_amc.lua",
      -- prefix = "default"
   },

   -- Root directory.
   chimere_root = home_path .. "/code/chimere/ccrt",

   -- Temporary directory.
   chimere_tmp = home_path .. "/data/amc",

   -- Simulation directory.
   simuldir = ".",

   -- Some flags.
   label = "01",
   nested = false,
   domain = "CONT5",
   phys = 6, -- nphour_ref
   step = 2,
   nlayer_chimere = 8,
   top_chimere_pressure = 500,
   first_layer_pressure = 997,
   mecachim = 2,
   accur = "full",
   aero = 1,
   nbins = 8,
   seasalt = 1,
   dust = 1,
   pops = 0,
   dustout = 0,
   carb = 0,
   trc = 0,
   soatyp = 2,
   ideepconv = 0,

   -- Number of levels in output file.
   levout = 8,

   -- Parallel configuration.
   -- nzdoms = 1,
   -- nmdoms = 1,

   -- Chemprep directory.
   -- chemprepdir = "chemprep/inputdata.211000021.8",

   -- Domain directory.
   -- domaindir = "domains",

   -- ngs = 1,
   -- nsu = 5,
   -- irs = 1,
   -- npeq = 1,
   -- nequil = 0,
   -- nsconcs = 24,
   -- nsdepos = 12,

   -- Vertical coordinates.
   -- vcoord = "domains/VCOORD/VCOORD_8_995_500",

   -- Horizontal coordinates.
   -- hcoord = "domains/HCOORD/COORD_CONT5",

   -- fnoutspec = chemprepdir .. "/OUTPUT_SPECIES.full",

   -- Input/output files.
   -- fnspec = chemprepdir .. "/ACTIVE_SPECIES",
   -- fnchem = chemprepdir .. "/CHEMISTRY",
   -- fnstoi = chemprepdir .. "/STOICHIOMETRY",
   -- fnphot = chemprepdir .. "/PHOTO_PARAMETERS",
   -- fnrates = chemprepdir .. "/REACTION_RATES",
   -- fnfamilies = chemprepdir .. "/FAMILIES",
   -- fnaerosol = chemprepdir .. "/AEROSOL",
   -- fnprim = chemprepdir .. "/PRIMARY",
   -- fnaeromin = chemprepdir .. "/AEROMIN.bin",
   -- fnaeroorg = chemprepdir .. "/AEROORG.bin",
   -- fnsemivol = chemprepdir .. "/SEMIVOL",
   -- fnveget = chemprepdir .. "/VEGETATION",
   -- fnsea = chemprepdir .. "/SEA",
   -- fnsoil = chemprepdir .. "/SOIL",
   -- fnanthro = chemprepdir .. "/ANTHROPIC",
   -- fnbiogen = chemprepdir .. "/BIOGENIC",
   -- fndepoespe = chemprepdir .. "/DEPO_SPEC",
   -- fndepopars = chemprepdir .. "/DEPO_PARS",
   -- fnwetd = chemprepdir .. "/WETD_SPEC",
   -- fnlanduse = domaindir .. "/" .. domain .. "/LANDUSE_" .. domain,
   -- fnemisa = chimere_tmp .. "/AEMISSIONS.nc",
   -- fnemisb = chimere_tmp .. "/BEMISSIONS.nc",
   -- fnbounconc = chimere_tmp .. "/BOUN_CONCS.nc",
   -- fnmeteo = chimere_tmp .. "/METEO.nc",
   -- fninit = simuldir .. "/ini." .. sim .. ".nc",
   -- fniniex = simuldir .. "/iex." .. sim .. ".nc",
   -- fnout = simuldir .. "/out." .. sim .. ".nc",
   -- fnexc = simuldir .. "/exc." .. sim .. ".nc",
   -- fnothers = simuldir .. "/par." .. sim .. ".nc",
   -- fnconcs = simuldir .. "/end." .. sim .. ".nc",
   -- fnendex = simuldir .. "/exe." .. sim .. ".nc",
   -- fndepos = simuldir .. "/dep." .. sim .. ".nc",

   -- For boundary conditions, the mapping.
   mapping = "default"
}