// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_BOUNDARY_CONDITION_CXX

#include "ClassChimereBoundaryCondition.hxx"

namespace Driver
{
  // Constructor.
  template<class D>
  ClassChimereBoundaryCondition<D>::ClassAMCA::ClassAMCA()
    : generated_(false), has_number_concentration_(false), Nspecies_(0), Ng_(0), Nsection_(0), Nbound_(0)
  {
    return;
  }


  // Destructor.
  template<class D>
  ClassChimereBoundaryCondition<D>::ClassAMCA::~ClassAMCA()
  {
    return;
  }


  // Read AMC attribute from a NetCDF file.
  template<class D>
  void ClassChimereBoundaryCondition<D>::ClassAMCA::FromNetCDF(NcFile &fn)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset() << "Search for AMC"
                         << " specific attributes in NetCDF file." << endl;
#endif

    // Retrieve number of species and their names.
    {
      const int str_len = int(fn.get_dim("AMCA::species_str_len")->size());

      ClassAMCA::Nspecies_ = int(fn.get_dim("AMCA::Nspecies")->size());
      ClassAMCA::species_name_.assign(ClassAMCA::Nspecies_, string(str_len, ' '));

      NcVar *var = fn.get_var("AMCA::species_name");

      for (int i = 0; i < ClassAMCA::Nspecies_; ++i)
        {
          string &species_name = ClassAMCA::species_name_[i];

          var->set_cur(i, 0);
          var->get(const_cast<char*>(species_name.data()), 1, str_len);

          // Remove trailing spaces.
          species_name = species_name.substr(0, species_name.find_last_not_of(' '));
        }
    }

    // Retrieve number of size section and bound diameters.
    {
      ClassAMCA::Nsection_ = int(fn.get_dim("AMCA::Nsection")->size());
      ClassAMCA::Nbound_ = ClassAMCA::Nsection_ + 1;

      ClassAMCA::diameter_bound_.assign(ClassAMCA::Nbound_, real(0));

      NcVar *var = fn.get_var("AMCA::diameter_bound");
      var->get(ClassAMCA::diameter_bound_.data(), ClassAMCA::Nbound_);
    }

    // Retrieve number of general sections.
    ClassAMCA::Ng_ = int(fn.get_dim("AMCA::Ng")->size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug() << LogReset() << "Ng = " << ClassAMCA::Ng_ << endl;
#endif

    // Retrieve general sections.
    {
      ClassAMCA::general_section_.assign(ClassAMCA::Ng_, vector1i(2, -1));

      NcVar *var = fn.get_var("AMCA::general_section");
      for (int i = 0; i < ClassAMCA::Ng_; ++i)
        {
          var->set_cur(i, 0);
          var->get(ClassAMCA::general_section_[i].data(), 1, 2);
        }

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fgreen() << Debug(3) << LogReset()
                           << "AMCA::general_section = "
                           << ClassAMCA::general_section_ << endl;
#endif
    }

    // Retrieve number of class compositions.
    {
      ClassAMCA::Nclass_.assign(Nsection_, 0);

      NcVar *var = fn.get_var("AMCA::Nclass");
      var->get(ClassAMCA::Nclass_.data(), ClassAMCA::Nsection_);

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset()
                           << "AMCA::Nclass = " << ClassAMCA::Nclass_ << endl;
#endif
    }

    // Retrieve class composition names per size sections.
    {
      int str_len = int(fn.get_dim("AMCA::class_str_len")->size());

      ClassAMCA::class_composition_name_.resize(ClassAMCA::Nsection_);
      for (int i = 0; i < ClassAMCA::Nsection_; ++i)
        ClassAMCA::class_composition_name_[i].assign(ClassAMCA::Nclass_[i],
                                                     string(str_len, ' '));

      NcVar *var = fn.get_var("AMCA::class_composition_name");

      for (int i = 0; i < ClassAMCA::Ng_; ++i)
        {
          string &class_composition_name = ClassAMCA::class_composition_name_
            [ClassAMCA::general_section_[i][0]][ClassAMCA::general_section_[i][1]];

          var->set_cur(i, 0);
          var->get(const_cast<char*>(class_composition_name.data()), 1, str_len);

          // Remove trailing spaces.
          class_composition_name =
            class_composition_name.substr(0, class_composition_name.find_last_not_of(' '));
        }

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset()
                           << "AMCA::class_composition_name = "
                           << ClassAMCA::class_composition_name_ << endl;
#endif
    }
  }


#ifdef CHIMERE_WITH_AMC
  // Write AMC attribute to a NetCDF file.
  template<class D>
  void ClassChimereBoundaryCondition<D>::ClassAMCA::ToNetCDF(NcFile &fn)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info(1) << LogReset() << "Write AMC Attributes in output NetCDF file ..." << endl;
#endif

    // Say that we generated the output file through AMC.
    fn.add_att("AMC", ("Generated by user \"" + util::get_user_name() + "\" at time \"" +
                       util::get_current_time("%a, %d %b %Y %H:%M:%S %z") + "\" on host/domain \"" +
                       util::get_host_name() + "/" + util::get_domain_name() + "\".").c_str());

    // Write number of species and their names.
    {
      int species_str_len(-1);
      for (int i = 0; i < Nspecies_; ++i)
        species_str_len = species_str_len < int(species_name_[i].size()) ? int(species_name_[i].size()) : species_str_len;

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fgreen() << Debug(3) << LogReset() << "AMCA::species_str_len = " << species_str_len << endl;
#endif

      NcDim *dim_species_str_len = fn.add_dim("AMCA::species_str_len", species_str_len);
      NcDim *dim_nspecies = fn.add_dim("AMCA::Nspecies", Nspecies_);

      NcVar *var_species_name = fn.add_var("AMCA::species_name", ncChar, dim_nspecies, dim_species_str_len);

      for (int i = 0; i < Nspecies_; ++i)
        {
          const string species_name = species_name_[i] + string(species_str_len - int(species_name_[i].size()), ' ');
          var_species_name->set_cur(i, 0);
          var_species_name->put(species_name.c_str(), 1, species_str_len);
        }
    }

    // Write number of size section and bound diameters.
    {
      NcDim *dim_nsection = fn.add_dim("AMCA::Nsection", Nsection_);
      NcDim *dim_nbound = fn.add_dim("AMCA::Nbound", Nbound_);
      NcVar *var_diameter_bound = fn.add_var("AMCA::diameter_bound", ncDouble, dim_nbound);
      var_diameter_bound->put(diameter_bound_.data(), Nbound_);
    }

    // Write general sections.
    {
      NcDim *dim_ng = fn.add_dim("AMCA::Ng", Ng_);
      NcDim *dim_2 = fn.add_dim("AMCA::2", 2);
      NcVar *var_general_section = fn.add_var("AMCA::general_section", ncInt, dim_ng, dim_2);

      for (int i = 0; i < Ng_; ++i)
        {
          var_general_section->set_cur(i, 0);
          var_general_section->put(general_section_[i].data(), 1, 2);
        }
    }

    // Write number of class compositions.
    {
      NcDim *dim_nsection = fn.get_dim("AMCA::Nsection");
      NcVar *var_nclass = fn.add_var("AMCA::Nclass", ncInt, dim_nsection);
      var_nclass->put(Nclass_.data(), Nsection_);
    }


    // Write class composition names per size sections.
    {
      int class_str_len(-1);
      for (int i = 0; i < Ng_; ++i)
        {
          const int len = int(class_composition_name_[general_section_[i][0]][general_section_[i][1]].size());
          class_str_len = class_str_len < len ? len : class_str_len;
        }

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "AMCA::class_str_len = " << class_str_len << endl;
#endif

      NcDim *dim_ng = fn.get_dim("AMCA::Ng");
      NcDim *dim_class_str_len = fn.add_dim("AMCA::class_str_len", class_str_len);

      NcVar *var_class_composition_name = fn.add_var("AMCA::class_composition_name", ncChar, dim_ng, dim_class_str_len);

      for (int i = 0; i < Ng_; ++i)
        {
          const string class_composition_name = class_composition_name_[general_section_[i][0]][general_section_[i][1]] +
            string(class_str_len - int(class_composition_name_[general_section_[i][0]][general_section_[i][1]].size()), ' ');
          var_class_composition_name->set_cur(i, 0);
          var_class_composition_name->put(class_composition_name.c_str(), 1, class_str_len);
        }
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info(1) << LogReset() << "... done" << endl;
#endif
  }


  // Get AMC attribute from AMC itself.
  template<class D>
  void ClassChimereBoundaryCondition<D>::ClassAMCA::FromAMC()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Read AMC Attributes at run time ..." << endl;
#endif

    // We need AMC for this to work.
    if (! AMC::ClassConfiguration::IsInitialized<AMC::init_flag::amc>())
      throw AMC::Error("AMC seems not yet initialized, cannot go further, call amc.AMC.Init(...) first.");

    // Species.
    Nspecies_ = AMC::ClassSpecies::GetNspecies();
    species_name_ = AMC::ClassSpecies::GetNameList();

    // Size discretization.
    Nsection_ = AMC::ClassDiscretizationSize::GetNsection();
    Nbound_ = AMC::ClassDiscretizationSize::GetNbound();
    AMC::ClassDiscretizationSize::GetDiameterBound(diameter_bound_);

    // General sections.
    Ng_ = AMC::ClassAerosolData::GetNg();
    AMC::ClassAerosolData::GetGeneralSection(general_section_);

    // Class composition, if any.
    Nclass_ = AMC::ClassDiscretizationClass<D>::GetNclass();
    AMC::ClassDiscretization<D>::GetClassCompositionName(class_composition_name_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "... done" << endl;
#endif
  }


  // Test it boundary conditions have same attributes as running AMC.
  template<class D>
  bool ClassChimereBoundaryCondition<D>::ClassAMCA::IsSameAsRunningAMC()
  {
    // We desperately need AMC for this to work.
    if (! AMC::ClassConfiguration::IsInitialized<AMC::init_flag::amc>())
      throw AMC::Error("AMC seems not yet initialized, cannot go further, call amc.AMC.Init(...) first.");

    // Now check with currently loaded AMC configuration.
    bool same_discretization(true);

    try
      {
        if (ClassAMCA::Nspecies_ != AMC::ClassSpecies::GetNspecies())
          throw string("Number of species between the NetCDF file (" + to_str(ClassAMCA::Nspecies_) +
                       ") and AMC (" + to_str(AMC::ClassSpecies::GetNspecies()) + ") differs.");

        if (ClassAMCA::Nsection_ != AMC::ClassDiscretizationSize::GetNsection())
          throw string("Number of size sections differs between the NetCDF file (" +
                       to_str(ClassAMCA::Nsection_) + ") and AMC (" +
                       to_str(AMC::ClassDiscretizationSize::GetNsection()) + ").");

        // Do not compare too roughly.
        for (int i = 0; i < AMC::ClassDiscretizationSize::GetNbound(); ++i)
          if (abs(ClassAMCA::diameter_bound_[i] /
                  AMC::ClassDiscretizationSize::GetDiameterBound()[i] - real(1))
              >= CHIMERE_BOUNDARY_CONDITION_DIAMETER_PRECISION)
            throw string("Bound diameter number " + to_str(i) + " does not correspond :" +
                         "got \"" + to_str(ClassAMCA::diameter_bound_[i]) + "\" from NetCDF file and \"" +
                         to_str(AMC::ClassDiscretizationSize::GetDiameterBound()[i]) + "\" from AMC.");

        for (int i = 0; i < ClassAMCA::Nsection_; ++i)
          {
            if (ClassAMCA::Nclass_[i] != AMC::ClassDiscretizationClass<D>::GetNclass(i))
              throw string("For size section " + to_str(i) + ", the class composition number" +
                           " differs between the NetCDF file (" + to_str(ClassAMCA::Nclass_[i]) +
                           ") and AMC (" + to_str(AMC::ClassDiscretizationClass<D>::GetNclass(i)) + ").");

            // We compare class composition names only if we are externally mixed, i.e. Nclass > 1.
            if (ClassAMCA::Nclass_[i] > 1)
              for (int j = 0; j < ClassAMCA::Nclass_[i]; ++j)
                if (ClassAMCA::class_composition_name_[i][j] != AMC::ClassDiscretization<D>::GetClassCompositionName()[i][j])
                  throw string("For size section number " + to_str(i) + " and class composition number " + to_str(j) +
                               ", the class composition name differs between the " + "NetCDF file (" +
                               ClassAMCA::class_composition_name_[i][j] + " and AMC (" +
                               AMC::ClassDiscretization<D>::GetClassCompositionName()[i][j] + ").");
          }
      }
    catch (std::string& str)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Warning() << LogReset() << str << endl;
#endif
        same_discretization = false;
      }

    return same_discretization;
  }
#endif


  // Allocate.
  template<class D>
  void ClassChimereBoundaryCondition<D>::Allocate()
  {
    if (allocated_)
      throw AMC::Error("Seems \"lateral\" and \"top\" Chimere fields are already allocated, call Clear() first.");

    //allocate(boundlat (nlatbound,nspec,2))
    //allocate(boundtop (nzonal,nmerid,nspec,2))
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info() << LogReset() << "Link fortran 90 Chimere boundary"
                         << " condition fields to C++ blitz::Array<> : boundlat, boundtop." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "CHIMERE_PARAMS(nlatbound) = "
                         << CHIMERE_PARAMS(nlatbound) << endl;
#endif

    if (ClassChimereBase::IsRunning())
      {
        lateral_.reference(Array<float, 3>(CHIMERE_COMMON(boundlat),
                                           shape(2, CHIMERE_PARAMS(nspec), CHIMERE_PARAMS(nlatbound)),
                                           neverDeleteData));

        top_.reference(Array<float, 4>(CHIMERE_COMMON(boundtop),
                                       shape(2, CHIMERE_PARAMS(nspec),
                                             CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal)),
                                       neverDeleteData));
      }
    else
      {
        lateral_.resize(2, CHIMERE_PARAMS(nspec), CHIMERE_PARAMS(nlatbound));
        top_.resize(2, CHIMERE_PARAMS(nspec), CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal));
      }

    // We are done.
    allocated_ = true;
  }


#ifdef CHIMERE_WITH_AMC
  // Init.
  template<class D>
  void ClassChimereBoundaryCondition<D>::Init(Ops::Ops &ops, string netcdf_file, string fnaerosol)
  {
    if (initiated_)
      throw AMC::Error("Seems boundary conditions are already initialized, call Clear() first.");

    // Never hurts.
    ClassChimereBoundaryCondition<D>::Clear();

    // Check if mapping static data is up.
    if (! ClassMappingParticle<D>::IsInitialized())
      throw AMC::Error("Particle mapping static data seems not initialized, call MappingParticle::Init()");

    // Creates AMC attributes.
    ClassAMCA *AMCA = new ClassAMCA();

    //
    // Configuration file.
    //

    const string prefix_orig = ops.GetPrefix();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info() << LogReset()
                         << "Init boundary_condition subsystem from section \""
                         << prefix_orig << "\"." << endl;
#endif

    //
    // NetCDF file.
    //

    netcdf_file = ops.Get<string>("fnbounconc", "", ClassChimereBase::IsRunning() ? string(CHIMERE_COMMON(fnbounconc)) : netcdf_file);

    if (netcdf_file.empty())
      throw AMC::Error("No NetCDF file provided for boundary conditions.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset() << "Use NetCDF file \""
                         << netcdf_file << "\" to initialize boundary conditions" << endl;
#endif

    NcFile fnboundary(netcdf_file.c_str(), NcFile::ReadOnly);

    if (! fnboundary.is_valid())
      throw AMC::Error("Unable to open NetCDF file \"" + netcdf_file + "\".");

    // Was it generated by AMC or Chimere ?
    AMCA->generated_ = fnboundary.get_att("AMC")->is_valid();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(2) << LogReset() << "NetCDF file generated by AMC ? "
                         << (AMCA->generated_ ? "YES" : "NO") << endl;
#endif

    //
    // Retrieve Chimere Attributes, if not running.
    //

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset() << "Are we currently running Chimere ? "
                         << (ClassChimereBase::IsRunning() ? "YES" : "NO") << endl;
#endif

    if (! ClassChimereBase::IsRunning())
      {
        // If Chimere is not running, this mainly means we are called for preprocessing purpose, that is to
        // say we intend to map boundary conditions from "fnboundary" NetCDF file to the running AMC
        // configuration.
        CHIMERE_PARAMS(nmerid) = int(fnboundary.get_dim("south_north")->size());
        CHIMERE_PARAMS(nzonal) = int(fnboundary.get_dim("west_east")->size());
        CHIMERE_PARAMS(nverti) = int(fnboundary.get_dim("bottom_top")->size());
        CHIMERE_COMMON(nspecboun) = int(fnboundary.get_dim("Species")->size());

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "CHIMERE_PARAMS(nmerid) = " << CHIMERE_PARAMS(nmerid) << endl;
        *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "CHIMERE_PARAMS(nzonal) = " << CHIMERE_PARAMS(nzonal) << endl;
        *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "CHIMERE_PARAMS(nverti) = " << CHIMERE_PARAMS(nverti) << endl;
        *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "CHIMERE_COMMON(nspecboun) = " << CHIMERE_COMMON(nspecboun) << endl;
#endif

        CHIMERE_PARAMS(nhbound) = 2 * (CHIMERE_PARAMS(nmerid) + CHIMERE_PARAMS(nzonal));
        CHIMERE_PARAMS(nlatbound) = CHIMERE_PARAMS(nhbound) * CHIMERE_PARAMS(nverti);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "nhbound = " << CHIMERE_PARAMS(nhbound) << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "nlatbound = " << CHIMERE_PARAMS(nlatbound) << endl;
#endif

        // In this case, we do not treat gas concentrations, they are just copied (see PreProcess).
        CHIMERE_COMMON(ngassp) = 0;
        // We always account for number concentration ... the +1.
        CHIMERE_PARAMS(nspec) = AMC::ClassAerosolData::GetNg() * (AMC::ClassSpecies::GetNspecies() + 1);

        // Instantiating fortran pointer from C++, probably dangerous.
        CHIMERE_COMMON(isboun) = new int[CHIMERE_COMMON(nspecboun)];
      }

    //
    // Retrieve species list. Not specifically an AMC attribute.
    //

    // These variables are used whether Chimere is running or not, they reflect the NetCDF file
    // configuration. Note that this later could have been generated from AMC before (went through the
    // preprocess), but this does not ensure the NetCDF is compliant with current running AMC configuration.
    Nspecboun_ = int(fnboundary.get_dim("Species")->size());
    Nspecboun_gas_ = 0;
    Nspecboun_aer_ = 0;

    {
      const int sp_str_len = int(fnboundary.get_dim("SpStrLen")->size());
      species_.assign(Nspecboun_, string(sp_str_len, ' '));

      NcVar *var = fnboundary.get_var("species");
      for (int i = 0; i < Nspecboun_; ++i)
        {
          var->set_cur(i, 0);
          var->get(const_cast<char*>(species_[i].data()), 1, sp_str_len);

          // Remove trailing spaces.
          species_[i] = species_[i].substr(0, species_[i].find_last_not_of(' '));

          regex_t regex;
          regcomp(&regex, "^p[0-9][0-9][a-zA-Z][0-9a-zA-Z_]*$", REG_EXTENDED);

          // If aerosol species, say to Chimere to leave it to AMC.
          if (regexec(&regex, species_[i].c_str(), 0, NULL, 0) > 0)
            {
#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "Boundary species \""
                                   << species_[i] << "\" seems a particle one." << endl;
#endif
              CHIMERE_COMMON(isboun)[i] = -1;

              Nspecboun_aer_++;
            }
          else
            Nspecboun_gas_++;
        }
    }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Debug() << LogReset() << "Nspecboun = " << Nspecboun_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "Nspecboun_gas = " << Nspecboun_gas_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "Nspecboun_aer = " << Nspecboun_aer_ << endl;
    *AMCLogger::GetLog() << Bcyan() << Debug(2) << LogReset() << "species = " << species_ << endl;
#endif

    //
    // If generated by AMC, read specific attributes from NetCDF file.
    //

    // Here, we check if the NetCDF was indeed generated by AMC or not. If so, this mainly means that
    // so-called sections now represents general sections as opposed to Chimere which only handles internal
    // mixing.
    if (AMCA->generated_)
      AMCA->FromNetCDF(fnboundary);
    else
      {
        // Display generating process.
        NcAtt *att = fnboundary.get_att("Generating_process");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "NetCDF file \""
                             << netcdf_file << "\" was generated by \""
                             << (att->is_valid() ? att->as_string(0) : "NULL") << "\"." << endl;
#endif

        // Not generated by AMC ! But we still need to set up AMC attributes for this NetCDF file.  This has
        // nothing to deal with the current running AMC configuration.

        //
        // Retrieve number of species and their names.
        //

        for (int i = 0; i < Nspecboun_; ++i)
          if (CHIMERE_COMMON(isboun)[i] < 0)
            {
              // We assume here that species_[i] has the syntax pXXaBcD where XX is a number from 00 to 99.
              const string species_name = species_[i].substr(3);

              // Avoid "Number" if there are number concentrations in boundary condition file.
              if (species_name.find("Number") == string::npos)
                {
                  bool not_in_list(true);
                  for (int j = 0; j < int(AMCA->species_name_.size()); ++j)
                    if (not_in_list = (species_name == AMCA->species_name_[j]))
                      break;

                  if (not_in_list) AMCA->species_name_.push_back(species_name);
                }
              else
                AMCA->has_number_concentration_ = true;
            }

        AMCA->Nspecies_ = int(AMCA->species_name_.size());

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bblue() << Debug(1) << LogReset() << "AMCA->Nspecies = " << AMCA->Nspecies_ << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(3) << LogReset() << "AMCA->species_name = " << AMCA->species_name_ << endl;
        *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "AMCA->has_number_concentration = "
                             << (AMCA->has_number_concentration_ ? "TRUE" : "FALSE") << endl;
#endif

        //
        // Retrieve diameter bounds. Not specifically an AMC attribute.
        //

        // If no provided file as argument, 
        if (fnaerosol.empty() && ClassChimereBase::IsRunning())
          fnaerosol = string(CHIMERE_COMMON(fnaerosol));

        // Again if nothing, last chance.
        if (fnaerosol.empty())
          fnaerosol = ops.Get<string>("fnaerosol", "", fnaerosol);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "fnaerosol = " << fnaerosol << endl;
#endif

        if (fnaerosol.empty())
          throw AMC::Error("File path \"fnaerosol\" was not provided, need it file path");
        else
          {
            ifstream fin(fnaerosol.c_str());

            if (! fin.good())
              throw AMC::Error("Error opening file \"" + fnaerosol + "\", probably wrong path.");

            string line;
            getline(fin, line);
            fin >> AMCA->Nsection_;

            AMCA->Nbound_ = AMCA->Nsection_ + 1;

            AMCA->diameter_bound_.assign(AMCA->Nbound_, real(0));

            getline(fin, line);
            getline(fin, line);

            istringstream iss(line);
            for (int i = 0; i < AMCA->Nbound_; ++i)
              iss >> AMCA->diameter_bound_[i];
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "AMCA->Nsection = " << AMCA->Nsection_ << endl;
        *AMCLogger::GetLog() << Fred() << Debug(3) << LogReset() << "AMCA->diameter_bound = " << AMCA->diameter_bound_ << endl;
#endif

        // If there are no number concentration in NetCDF, we then have to compute the input mean diameters.
        if (! AMCA->has_number_concentration_)
          {
            diameter_in_ = new real[AMCA->Nsection_];
            for (int i = 0; i < AMCA->Nsection_; ++i)
              diameter_in_[i] = sqrt(AMCA->diameter_bound_[i] * AMCA->diameter_bound_[i + 1]);
          }
        else
          diameter_in_ = NULL;

        // We then have to give a default value to remaining AMC attributes. The sequel means we are internally mixed.
        AMCA->Ng_ = AMCA->Nsection_;
        AMCA->Nclass_.assign(AMCA->Nsection_, 1);
        AMCA->general_section_.assign(AMCA->Ng_, {-1, -1});
        AMCA->class_composition_name_.assign(AMCA->Nsection_, vector1s(1, "internal_mixing"));
        for (int i = 0; i < AMCA->Ng_; ++i)
          AMCA->general_section_[i][0] = i;
      }

    fnboundary.close();

    //
    // Construct the mapping size and composition, actually doing the work.
    //

    // Say that NetCDF file was AMC generated, if the AMC NetCDF configuration is equal to the running AMC one
    // we do not have to map concentrations. Note that this could happen even if file was not AMC generated,
    // e.g. if AMC runs in internal mixing with same size discretization as the NetCDF one (though infered
    // from "fnaerosol" ...). In any other cases, we have to perform size and/or class composition and/or
    // species mapping. This latter mapping is not trivial, it has to be detailed in the configuration file.
    if (AMCA->IsSameAsRunningAMC())
      mapping_particle_ptr_ = NULL;
    else
      mapping_particle_ptr_ = new ClassMappingParticle<D>(&ops, ops.Get<string>("mapping"),
                                                          AMCA->general_section_,
                                                          AMCA->class_composition_name_,
                                                          AMCA->species_name_,
                                                          AMCA->diameter_bound_);

    // Input and output concentration array. We assume there may have particle number concentrations in
    // boundary conditions, but we do not take water into account. Only relevant if we go through particle mapping.
    if (mapping_particle_ptr_ != NULL)
      {
        concentration_in_.resize(AMCA->Ng_ * (AMCA->Nspecies_ + 1));
        concentration_in_ = real(0);

        concentration_out_.resize(CHIMERE_PARAMS(nspec));
        concentration_out_ = real(0);
      }

    //
    // Construct species index.
    //

    species_index_.assign(Nspecboun_aer_, -1);

    // Treat number and mass concentrations present in input.
    for (int i = Nspecboun_gas_; i < Nspecboun_; ++i)
      {
        // We assume here that species_[i] has the syntax pXXaBcD where XX is a number from 00 to 99.
        const string species_name = species_[i].substr(3);

        // We have to pay attention here to reach exact equality.
        if (species_name == "Number")
          species_index_[i - Nspecboun_gas_] = util::convert<int>(species_[i].substr(1, 2)) - 1;
        else
          {
            bool found(false);
            for (int j = 0; j < AMCA->Nspecies_; ++j)
              if (found = (species_[i] == AMCA->species_name_[j]))
                {
                  species_index_[i - Nspecboun_gas_] = AMCA->Ng_ +
                    AMCA->Nspecies_ * (util::convert<int>(species_[i].substr(1, 2)) - 1) + j;
                  break;
                }

            if (! found) throw AMC::Error("Unable to find NetCDF species \"" + species_name +
                                          "\" (from \"" + species_[i] + "\") in AMCA list.");
          }
      }

    //
    // Structure of species index.
    //
    // | Value in species_index   | type of variable    | general section | species number      |
    // |                          | Number of Species() |    from 1 to Ng | from 1 to Nspecies  |
    // |--------------------------+---------------------+-----------------+---------------------|
    // | 00                       | Number              |               1 | X (= not a species) |
    // | 01                       | Number              |               2 | X                   |
    // | ...                      |                     |                 |                     |
    // | Ng - 2                   | Number              |          Ng - 1 | X                   |
    // | Ng - 1                   | Number              |              Ng | X                   |
    // | Ng                       | Species(1)          |               1 | 1                   |
    // | Ng + 1                   | Species(2)          |               1 | 2                   |
    // | Ng + 2                   | Species(3)          |               1 | 3                   |
    // | ...                      |                     |                 |                     |
    // | Ng + Nspecies - 1        | Species(Nspecies)   |               1 | Nspecies            |
    // | Ng + Nspecies            | Species(1)          |               2 | 1                   |
    // | Ng + Nspecies + 1        | Species(2)          |               2 | 2                   |
    // | ...                      |                     |                 |                     |
    // | Ng + 2 * Nspecies        | Species(1)          |               3 | 1                   |
    // | ...                      |                     |                 |                     |
    // | Ng + (Ng - 1) * Nspecies | Species(1)          |              Ng | 1                   |
    // | ...                      |                     |                 |                     |
    // | Ng + Ng * Nspecies - 1   | Species(Nspecies)   |              Ng | Nspecies            |
    // |--------------------------+---------------------+-----------------+---------------------|

    // Check if no one is forgotten. All values should be >= 0, even if there is no number concentration in
    // NetCDF file. This is only to map boundary condition order on ClassMappingParticle order.
    for (int i = 0; i < Nspecboun_aer_; ++i)
      if (species_index_[i] < 0)
        throw AMC::Error("Chimere boundary condition species \"" + species_[i + Nspecboun_gas_] + "\" dit not receive any index.");

    // Setup arrays out ranges. Upper limit is inclusive with Range().
    range_in_ = Range(Nspecboun_gas_, Nspecboun_ - 1);
    range_out_ = Range(CHIMERE_COMMON(ngassp), CHIMERE_PARAMS(nspec) - 1);

    // range_in_ is valid everywhere as it depends only of NetCDF file, range_out is valid when Chimere is
    // running as we are picking out some of its parameters.

    //Delete AMC attributes.
    delete AMCA;

    // Delete isboun ... if we instantiated it from our own.
    if (! ClassChimereBase::IsRunning())
      delete[] CHIMERE_COMMON(isboun);

    // We are done.
    initiated_ = true;
  }


  // Clear.
  template<class D>
  void ClassChimereBoundaryCondition<D>::Clear()
  {
    range_in_ = Range(0);
    range_out_ = Range(0);

    if (mapping_particle_ptr_ != NULL)
      {
        concentration_in_.free();
        concentration_out_.free();

        if (diameter_in_ != NULL)
          delete[] diameter_in_;

        delete mapping_particle_ptr_;
        mapping_particle_ptr_ = NULL;
      }

    Nspecboun_ = 0;
    Nspecboun_gas_ = 0;
    Nspecboun_aer_ = 0;

    species_.clear();
    species_index_.clear();

    // Never ... never free blitz arrays here if Chimere is running, you have been warned.
    if (! ClassChimereBase::IsRunning())
      {
        lateral_.free();
        top_.free();
      }

    initiated_ = false;
    allocated_ = false;
  }


  // Compute.
  template<class D>
  void ClassChimereBoundaryCondition<D>::ComputeLateral(const int ksens, const float *buf3)
  {
    // allocate(buf3(nspecboun, nhbound, nverti))
    const Array<float, 3> buffer(const_cast<float*>(buf3),
                                 shape(CHIMERE_PARAMS(nverti), CHIMERE_PARAMS(nhbound),
                                       CHIMERE_COMMON(nspecboun)),
                                 neverDeleteData);

    int zh(-1);

    if (mapping_particle_ptr_ != NULL)
      for (int z = 0; z < CHIMERE_PARAMS(nverti); ++z)
        for (int h = 0; h < CHIMERE_PARAMS(nhbound); ++h)
          {
            // Not optimal. But we are stuck by Fortran.
            Array<float, 1> concentration_out = lateral_(ksens, range_out_, ++zh);

            // Much better.
            const Array<float, 1> concentration_in = buffer(z, h, range_in_);

            // Translate to input concentration array. Beware that if there is no number concentration in
            // NetCDF, particle mapping has to find it out by itself.
            concentration_in_ = real(0);
            for (int i = 0; i < Nspecboun_aer_; ++i)
              concentration_in_(species_index_[i]) = real(concentration_in(i));

            // Prepare output concentration.
            concentration_out_ = real(0);

            // Perform redistribution.
            mapping_particle_ptr_->Compute(concentration_in_.data(), concentration_out_.data(), diameter_in_);

            // Give back concentrations. Pay attention that you cannot replace "concentration_out_" by
            // "concentration_out" in latest assertion. Hence this equality.
            concentration_out = concentration_out_;
          }
    else
      for (int z = 0; z < CHIMERE_PARAMS(nverti); ++z)
        for (int h = 0; h < CHIMERE_PARAMS(nhbound); ++h)
          {
            // Not optimal. But we are stuck by Fortran.
            Array<float, 1> concentration_out = lateral_(ksens, range_out_, ++zh);

            // Much better.
            const Array<float, 1> concentration_in = buffer(z, h, range_in_);

            // Perform redistribution.
            for (int i = 0; i < Nspecboun_aer_; ++i)
              concentration_out(species_index_[i]) = concentration_in(i);
          }
  }


  template<class D>
  void ClassChimereBoundaryCondition<D>::ComputeTop(const int ksens, const float *buf3)
  {
    //allocate(buf3(nspecboun,nzonal,nmerid))
    const Array<float, 3> buffer(const_cast<float*>(buf3),
                                 shape(CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal),
                                       CHIMERE_COMMON(nspecboun)),
                                 neverDeleteData);

    if (mapping_particle_ptr_ != NULL)
      for (int y = 0; y < CHIMERE_PARAMS(nmerid); ++y)
        for (int x = 0; x < CHIMERE_PARAMS(nzonal); ++x)
          {
            // Not optimal. But we are stuck by Fortran.
            Array<float, 1> concentration_out = top_(ksens, range_out_, y, x);

            // Much better.
            const Array<float, 1> concentration_in = buffer(y, x, range_in_);

            // Translate to input concentration array. Beware that if there is no number concentration in
            // NetCDF, particle mapping has to find it out by itself.
            concentration_in_ = real(0);
            for (int i = 0; i < Nspecboun_aer_; ++i)
              concentration_in_(species_index_[i]) = real(concentration_in(i));

            // Prepare output concentration.
            concentration_out_ = real(0);

            // Perform redistribution.
            mapping_particle_ptr_->Compute(concentration_in_.data(), concentration_out_.data(), diameter_in_);

            // Give back concentrations. Pay attention that you cannot replace "concentration_out_" by
            // "concentration_out" in latest assertion. Hence this equality.
            concentration_out = concentration_out_;
          }
    else
      for (int y = 0; y < CHIMERE_PARAMS(nmerid); ++y)
        for (int x = 0; x < CHIMERE_PARAMS(nzonal); ++x)
          {
            // Not optimal. But we are stuck by Fortran.
            Array<float, 1> concentration_out = top_(ksens, range_out_, y, x);

            // Much better.
            const Array<float, 1> concentration_in = buffer(y, x, range_in_);

            // Perform redistribution.
            for (int i = 0; i < Nspecboun_aer_; ++i)
              concentration_out(species_index_[i]) = concentration_in(i);
          }
  }
#endif


#if defined(CHIMERE_AMC_WITH_PREPROCESSING) && defined(DRIVER_WITH_NETCDF)
  // Preprocess boundary conditions.
  template<class D>
  void ClassChimereBoundaryCondition<D>::PreProcess(const string netcdf_file_in, const string netcdf_file_out)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << "Performing Chimere Boundary condition mapping ..." << LogReset().Str() << endl;
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset() << "Mapping ? " << (mapping_particle_ptr_ != NULL ? "YES" : "NO") << endl;
#endif

    //
    // A few checks.
    //

    if (! initiated_)
      throw AMC::Error("Seems boundary conditions are not yet initialized, call Init(ops, \"path/to/your/input/netcdf/file\") first.");

    if (! allocated_)
      throw AMC::Error("Seems \"lateral\" and \"top\" Chimere fields are not yet allocated, call Allocate() first.");

    //
    // Open netcdf files.
    //

    NcFile fnboundary_in(netcdf_file_in.c_str(), NcFile::ReadOnly);

    if (! fnboundary_in.is_valid())
      throw AMC::Error("Unable to open input NetCDF file \"" + netcdf_file_in + "\" for reading.");

    // Create output file, overwriten if it already exists. But let's be user friendly before and ask whenever
    // the file already exists, it may be huge and time consuming to (re)generate.
    if (access(netcdf_file_out.c_str(), F_OK) == 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Warning() << LogReset() << " Output file \"" << netcdf_file_out
                             << "\" already exists, overwrite ? (Hit any key to go on, Ctrl-C otherwise)" << endl;
#else
        cerr << "\033[41m" << "WARNING:" << "\033[0m" << " Output file \"" << netcdf_file_out
             << "\" already exists, overwrite ? (Hit any key to go on, Ctrl-C otherwise)" << endl;
#endif
        getchar();
      }

    NcFile fnboundary_out(netcdf_file_out.c_str(), NcFile::Replace);

    if (! fnboundary_out.is_valid())
      throw AMC::Error("Unable to open output NetCDF file \"" + netcdf_file_out + "\" for writing.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Info(1) << LogReset() << "Mapping from \""
                         << netcdf_file_in << "\" to \"" << netcdf_file_out << "\"." << endl;
#endif

    //
    // Prepare output file.
    //

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << LogReset() << Debug(3) << "Nspecboun_gas = " << Nspecboun_gas_ << endl;
    *AMCLogger::GetLog() << Fcyan() << LogReset() << Debug(3) << "CHIMERE_PARAMS(nspec) = " << CHIMERE_PARAMS(nspec) << endl;
#endif

    // Preexisting dimensions.
    const int Nspecboun = Nspecboun_gas_ + CHIMERE_PARAMS(nspec);

    for (int i = 0; i < fnboundary_in.num_dims(); ++i)
      if (fnboundary_in.get_dim(i)->is_unlimited())
        fnboundary_out.add_dim(fnboundary_in.get_dim(i)->name());
      else if (string(fnboundary_in.get_dim(i)->name()) == "Species") // Modify species dimension.
        fnboundary_out.add_dim(fnboundary_in.get_dim(i)->name(), Nspecboun);
      else
        fnboundary_out.add_dim(fnboundary_in.get_dim(i)->name(), fnboundary_in.get_dim(i)->size());

    // Preexisting attributes.
    for (int i = 0; i < fnboundary_in.num_atts(); ++i)
      fnboundary_out.add_att(fnboundary_in.get_att(i)->name(), fnboundary_in.get_att(i)->values()->as_string(0));

    // New dimensions and attributes from AMC.
    ClassAMCA *AMCA = new ClassAMCA();
    AMCA->FromAMC();
    AMCA->ToNetCDF(fnboundary_out);
    delete AMCA;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << LogReset() << Debug(1) << "Dimensions in output file :" << endl;
    for (int i = 0; i < fnboundary_out.num_dims(); ++i)
      *AMCLogger::GetLog() << Fgreen() << LogReset() << Debug(1) << "\t" << fnboundary_out.get_dim(i)->name()
                           << " = " << fnboundary_out.get_dim(i)->size() << endl;
    *AMCLogger::GetLog() << Bblue() << LogReset() << Debug(3) << "Attributes in output file :" << endl;
    for (int i = 0; i < fnboundary_in.num_atts(); ++i)
      *AMCLogger::GetLog() << Fblue() << LogReset() << Debug(3) << "\t" << fnboundary_out.get_att(i)->name()
                           << " = " << fnboundary_out.get_att(i)->values() << endl;
#endif

    //
    // Species variables.
    //

    vector1s species;

    {
      const int SpStrLen = int(fnboundary_out.get_dim("SpStrLen")->size());
      species.assign(Nspecboun, string(SpStrLen, ' '));

      NcVar *var = fnboundary_out.add_var("species", ncChar, fnboundary_out.get_dim("Species"), fnboundary_out.get_dim("SpStrLen"));

      for (int i = 0; i < Nspecboun_gas_; ++i)
        species[i].replace(0, species_[i].size(), species_[i]);

      // We do not follow the disastrous Chimere order for particles.
      int h(-1);

      for (int i = 0; i < AMC::ClassAerosolData::GetNg(); ++i)
        {
          ostringstream oss;
          oss << "p" << setw(2) << setfill('0') << i + 1 << "Number";
          const string str = oss.str();
          species[++h].replace(0, str.size(), str);
        }

      for (int i = 0; i < AMC::ClassAerosolData::GetNg(); ++i)
        for (int j = 0; j < AMC::ClassSpecies::GetNspecies(); ++j)
          {
            ostringstream oss;
            oss << "p" << setw(2) << setfill('0') << i + 1 << AMC::ClassSpecies::GetName(j);
            const string str = oss.str();
            species[++h].replace(0, str.size(), str);
          }

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Bred() << LogReset() << Debug(1) << "Species in output file :" << species << endl;
      *AMCLogger::GetLog() << Fred() << LogReset() << Debug(1) << "Nspecboun = " << Nspecboun << endl;
#endif

      for (h = 0; h < Nspecboun; ++h)
        {
          var->set_cur(h, 0);
          var->put(const_cast<char*>(species[h].data()), 1, SpStrLen);
        }
    }

    //
    // Time variable.
    //

    const int Nt = int(fnboundary_in.get_dim("Time")->size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << LogReset() << Debug(2) << "Nt = " << Nt << endl;
#endif

    {
      const int date_str_len = fnboundary_out.get_dim("DateStrLen")->size();
      NcVar *time_in = fnboundary_in.get_var("Times");
      NcVar *time_out = fnboundary_out.add_var("Times", ncChar, fnboundary_out.get_dim("Time"), fnboundary_out.get_dim("DateStrLen"));

      string date(date_str_len, ' ');

      for (int t = 0; t < Nt; ++t)
        {
          time_in->set_cur(t, 0);
          time_in->get(const_cast<char*>(date.data()), 1, date_str_len);

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fmagenta() << LogReset() << Debug(2) << "t = " << setw(4)
                               << setfill('0') << t << ", date = \"" << date << "\"." << endl;
#endif

          time_out->set_cur(t, 0);
          time_out->put(const_cast<char*>(date.data()), 1, date_str_len);
        }
    }

    //
    // Concentration variables.
    //

    NcVar *conc_top_in = fnboundary_in.get_var("top_conc");
    NcVar *conc_lat_in = fnboundary_in.get_var("lat_conc");

    NcVar *conc_top_out = fnboundary_out.add_var("top_conc", ncFloat, fnboundary_out.get_dim("Time"),
                                                 fnboundary_out.get_dim("south_north"), fnboundary_out.get_dim("west_east"),
                                                 fnboundary_out.get_dim("Species"));
    NcVar *conc_lat_out = fnboundary_out.add_var("lat_conc", ncFloat, fnboundary_out.get_dim("Time"),
                                                 fnboundary_out.get_dim("bottom_top"), fnboundary_out.get_dim("h_boundary"),
                                                 fnboundary_out.get_dim("Species"));

    //
    // Core job.
    //

#ifdef AMC_WITH_TIMER
    const int time_index = AMC::AMCTimer<AMC::CPU>::Add("chimere_boundary_condition_mapping");
    AMC::AMCTimer<AMC::CPU>::Start();
#endif

    Array<float, 3> buf3_lat(CHIMERE_PARAMS(nverti), CHIMERE_PARAMS(nhbound), CHIMERE_COMMON(nspecboun));
    Array<float, 3> buf3_top(CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal) , CHIMERE_COMMON(nspecboun));

    for (int t = 0; t < Nt; ++t)
      {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << LogReset() << Debug(1) << "t = " << t << endl;
#endif

        // Retrieve all concentrations only.
        conc_lat_in->set_cur(t, 0, 0, 0);
        conc_top_in->set_cur(t, 0, 0, 0);

        conc_lat_in->get(buf3_lat.data(), 1, CHIMERE_PARAMS(nverti), CHIMERE_PARAMS(nhbound), CHIMERE_COMMON(nspecboun));
        conc_top_in->get(buf3_top.data(), 1, CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal) , CHIMERE_COMMON(nspecboun));

        // Write gas concentrations as is to the output file.
        conc_lat_out->set_cur(t, 0, 0, 0);
        conc_top_out->set_cur(t, 0, 0, 0);

        conc_lat_out->put(buf3_lat.data(), 1, CHIMERE_PARAMS(nverti), CHIMERE_PARAMS(nhbound), Nspecboun_gas_);
        conc_top_out->put(buf3_top.data(), 1, CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal) , Nspecboun_gas_);

        // Then (pre)process aerosol concentrations.  Results of computation is stored in top_ and lateral_
        // float Array<> members.
        ClassChimereBoundaryCondition<D>::ComputeLateral(0, buf3_lat.data());
        ClassChimereBoundaryCondition<D>::ComputeTop    (0, buf3_top.data());

        // Then write it to the output file.
        conc_lat_out->set_cur(t, 0, 0, Nspecboun_gas_);
        conc_top_out->set_cur(t, 0, 0, Nspecboun_gas_);

        // Arrays thus defined contains contiguous data, as in C++ the storing order is row major, the
        // rightmost index is also the fastest moving.
        const Array<float, 3> lateral = lateral_(0, Range::all(), Range::all(), Range::all());
        const Array<float, 3> top = top_(0, Range::all(), Range::all(), Range::all());

#ifdef AMC_WITH_LOGGER
        for (int i = 0; i < CHIMERE_PARAMS(nspec); ++i)
          {
            const Array<float, 2> lat1 = lateral(Range::all(), Range::all(), i);
            const Array<float, 2> top1 = top(Range::all(), Range::all(), i);

            *AMCLogger::GetLog() << Bcyan() << LogReset() << Debug(2) << "species = \"" << species[i] << "\" :" << endl;
            *AMCLogger::GetLog() << Fcyan() << LogReset() << Debug(2) << "\tlateral (min, mean, max) = (" << setprecision(6) << min(lat1)
                                 << ", " << setprecision(6) << mean(lat1) << ", " << setprecision(6) << max(lat1) << ")" << endl;
            *AMCLogger::GetLog() << Fcyan() << LogReset() << Debug(2) << "\t    top (min, mean, max) = (" << setprecision(6) << min(top1)
                                 << ", " << setprecision(6) << mean(top1) << ", " << setprecision(6) << max(top1) << ")" << endl;
          }
#endif

        conc_lat_out->put(lateral.data(), 1, CHIMERE_PARAMS(nverti), CHIMERE_PARAMS(nhbound), CHIMERE_PARAMS(nspec));
        conc_top_out->put(top.data(), 1, CHIMERE_PARAMS(nmerid), CHIMERE_PARAMS(nzonal) , CHIMERE_PARAMS(nspec));
      }

#ifdef AMC_WITH_LOGGER
#ifdef AMC_WITH_TIMER
    AMC::AMCTimer<AMC::CPU>::Stop(time_index);
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Performed Chimere Boundary condition mapping in "
                         << setprecision(12) << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
    *AMCLogger::GetLog() << Byellow() << Info() << "... Finished performing Chimere Boundary condition mapping." << LogReset().Str() << endl;
#endif

    //
    // Close files.
    //

    fnboundary_in.close();
    fnboundary_out.close();
  }
#endif
}

#define AMC_FILE_CLASS_CHIMERE_BOUNDARY_CONDITION_CXX
#endif
