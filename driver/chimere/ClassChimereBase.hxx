// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_CHIMERE_BASE_HXX


namespace Driver
{
  /*! 
   * \class ClassChimereBase
   */
  class ClassChimereBase : protected ClassDriverDomainBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  protected:

    /*!< Is running ?*/
    static bool is_running_;

    /*!< Original Chimere diameter bounds.*/
    static vector1r diameter_bound_;

    /*!< Original Chimere species name.*/
    static vector1s species_name_;

    /*!< Map of Chimere integer parameters.*/
    static map<string, int*> parameter_;

    /*!< Map of Chimere input/output file names.*/
    static map<string, char*> file_name_;

    /*!< Concentrations.*/
    static Array<double, 4> concentration_chimere_;
    static Array<double, 4> concentration_worker_;

    /*!< Compute end date from beginning date and number of simulation days.*/
    static string compute_date_end(const string &date_begin, const int &number_of_days);

  public:

    /*!< Allocate arrays.*/
    static void Allocate();

    /*!< Read Chimere configuration.*/
    static void ReadConfiguration(const string &date_begin, const string &date_end);

#ifdef CHIMERE_WITH_AMC
    /*!< Write Chimere configuration.*/
    static void WriteConfiguration(const string suffix = "amc");
#endif

    /*!< Is running ?*/
    static bool IsRunning();

    /*!< Get methods.*/
    static int GetParameter(const string name);
    static string GetFileName(const string name);
    static map<string, int> GetParameter();
    static map<string, string> GetFileName();
    static double GetConcentration(const int s, const int z, const int y, const int x);

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_CHIMERE_BASE_HXX
#endif
