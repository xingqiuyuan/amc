// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
//
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CHIMERE_AMC_CXX

#include "chimere_amc.hxx"

// Set molar mass array of Chimere.
void chimere_amc_set_molar_mass(double *molarm)
{
  // Avoid gas species.
  int k = CHIMERE_COMMON(ngassp) - 1;

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
  // Number concentration, if used. Not relevant for number of course, but if accidentally used in a loop,
  // will not alter concentrations.
  for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
    molarm[++k] = double(1);
#endif

  // Mass concentration, we do not follow the usual storage order of Chimere.
  // This converts back the AMC molar mass to g.mol^{-1} for each species.
  for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
    for (int j = 0; j < CHIMERE_AMC.Nspecies; ++j)
      molarm[++k] = double(AMC::ClassSpecies::GetMolarMass(j)) * double(1.e-6);

#ifdef CHIMERE_AMC_WITH_WATER
  // Liquid water content, if used.
  for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
    molarm[++k] = double(AMC_WATER_MOLAR_MASS) * double(1.e-6);
#endif
}


// Chimere anthropic emissions.
void chimere_amc_emission_anthropic()
{
  Driver::ClassChimereEmissionAnthropic<disc_comp>::ComputeSourceRate();
}


// Chimere biogenic emissions.
void chimere_amc_emission_biogenic()
{
  Driver::ClassChimereEmissionBiogenic<disc_comp>::ComputeSourceRate();
}


// Allocate boundary condition fields.
void chimere_amc_boundary_condition_allocate()
{
  Driver::ClassChimereBoundaryCondition<disc_comp>::Allocate();
}


// Init Chimere boundary conditions.
void chimere_amc_boundary_condition_init(char* usedval, int len_usedval,
                                         char* notusedval, int len_notusedval)
{
  // Particle mapping init is done in the driver instead. See chimere_amc.hxx for
  // explanations about why we leave this declaration commented.
  //
  //Driver::ClassMappingParticle<disc_comp>::Init();

  Ops::Ops ops(Driver::ClassDriverBase::GetConfigurationFile());
  ops.SetPrefix(Driver::ClassDriverBase::GetPrefix() + ".");
  Driver::ClassChimereBoundaryCondition<disc_comp>::Init(ops);
  ops.Close();
}


// Chimere lateral boundary conditions.
void chimere_amc_boundary_condition_lateral(const int *ksens, const float *buf3)
{
  // Beware that this function is called from fortran in which indices begin at 1.
  Driver::ClassChimereBoundaryCondition<disc_comp>::ComputeLateral(*ksens - 1, buf3);
}


// Chimere top boundary conditions.
void chimere_amc_boundary_condition_top(const int *ksens, const float *buf3)
{
  // Beware that this function is called from fortran in which indices begin at 1.
  Driver::ClassChimereBoundaryCondition<disc_comp>::ComputeTop(*ksens - 1, buf3);
}

#define AMC_FILE_CHIMERE_AMC_CXX
#endif
