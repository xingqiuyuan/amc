! Copyright (C) 2013-2015 INERIS
! Author(s) : Edouard Debry
!
! This file is part of AMC.
!
! AMC is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! AMC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with AMC.  If not, see <http://www.gnu.org/licenses/>.

module chimere_amc

  implicit none

  type amc_namespace
     integer :: Ng        ! Number of general sections.
     integer :: Nspecies  ! Number of species.
     integer :: Nvariable ! Number of species + 1 for number concentration.
     integer :: Nsection  ! Number of size sections.
  endtype amc_namespace

  type(amc_namespace) :: AMC

end module chimere_amc
