// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_CHIMERE_HXX

namespace Driver
{
  /*! 
   * \class ClassDriverChimere
   */
  template<class S, class R, class D, class C>
  class ClassDriverChimere : public ClassChimereBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  public:

    /*!< Init Chimere model.*/
    static void Init(const string &configuration_file, const string &prefix,
                     const string &date_begin, const int &number_of_days,
                     const string type = "chimere");

    /*!< Clear static data.*/
    static void Clear();

    /*!< Run.*/
    static void Run();
  };
}

#define AMC_FILE_CLASS_DRIVER_CHIMERE_HXX
#endif
