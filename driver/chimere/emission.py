#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from driver import amc, emission, chimere, core

root_directory = os.path.dirname(os.path.abspath(__file__))

if core.CHIMERE_AMC_HAS_PREPROCESSING == 0:
    raise Exception("Driver interface not compiled with Chimere AMC preprocessing.")


#
# Not necessary but helpful.
#

if core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

if core.DRIVER_HAS_TIMER > 0:
    from driver.util import TimerCPU, TimerWall

#
# Load AMC.
#

amc.AMC.Init("./test_amc.lua", "default")

#
# Load emission driver subsystem.
#

ops = Ops.Ops(root_directory + "/emission.lua")
emission.DynamicsEmissionBase.Init(ops)

# Set the general section count.
emission.DynamicsEmissionBase.ToggleCountGeneralSection()

#
# Load Chimere anthropic and biogenic emissions. No need to call initialization.
#


chimere.ChimereEmissionAnthropicBase.ComputeSourceGeneralSection(root_directory + "../../../../data/amc/AEMISSIONS.nc")

chimere.ChimereEmissionBiogenicBase.ComputeSourceGeneralSection(root_directory + "../../../../data/amc/BEMISSIONS.nc", root_directory + "../../../chimere/ccrt/chemprep/inputdata.211000021.8/BIOGENIC")
