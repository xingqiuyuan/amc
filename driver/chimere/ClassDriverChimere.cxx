// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_CHIMERE_CXX

#include "ClassDriverChimere.hxx"

namespace Driver
{
  // Init Chimere model.
  template<class S, class R, class D, class C>
  void ClassDriverChimere<S, R, D, C>::Init(const string &configuration_file, const string &prefix,
                                            const string &date_begin, const int &number_of_days,
                                            const string type)
  {
    // Abort if already running.
    if (ClassChimereBase::is_running_)
      throw AMC::Error("CHIMERE is already running, abort init here, call Clear() to start again.");

    // If MPI not initialized, trouble ahead.
    if (! MPI::Is_initialized())
      throw AMC::Error("MPI subsystem, which is definitely needed by Chimere, seems not initialized, call MPI_Init() first.");

    // Clear everything before init.
    ClassDriverChimere<S, R, D, C>::Clear();

    ClassChimereBase::is_running_ = true;

    ClassDriverBase::Init(configuration_file, prefix, type);
    ClassDriverDomainBase::PreInit();

    istringstream iss(date_begin);
    int date_begin_int;
    iss >> date_begin_int;

    // Starting date in YYYYMMDDHH format.
    CHIMERE_COMMON(idatestart) = date_begin_int * 100;
    CHIMERE_COMMON(nhourrun) = number_of_days * 24;

    const string date_end = ClassChimereBase::compute_date_end(date_begin, number_of_days);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << LogReset()
                         << "Init Chimere model with beginning date \"" << date_begin
                         << "\" and end date (included) \"" << date_end << "\"" << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug() << LogReset() << "idatestart = "
                         << CHIMERE_COMMON(idatestart) << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug() << LogReset() << "nhourrun = "
                         << CHIMERE_COMMON(nhourrun) << endl;
#endif

    // Read Chimere configuration.
    ClassChimereBase::ReadConfiguration(date_begin, date_end);

    ClassDriverDomainBase::PostInit();

#ifdef CHIMERE_WITH_AMC
    //
    // CHIMERE with AMC.
    //

    MPI::COMM_WORLD.Barrier();

    // Boot AMC.
    if (! AMC::ClassAMX<S, R, D, C>::IsInitialized())
      {
        Ops::Ops ops(ClassDriverBase::configuration_file_);
        ops.SetPrefix(ClassDriverBase::prefix_ + ".amc.");
        AMC::ClassAMX<S, R, D, C>::Initiate(ops.Get<string>("configuration_file", "", configuration_file),
                                            ops.Get<string>("prefix", "", prefix));
        ops.Close();
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << "AMC booted with configuration : " << LogReset().Str()
                         << AMC::ClassAMX<S, R, D, C>::Info() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug() << LogReset() << "general_section = "
                         << AMC::ClassAerosolData::GetGeneralSection() << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug() << LogReset() << "class_composition_name = "
                         << AMC::ClassDiscretization<D>::GetClassCompositionName() << endl;
#endif

    //
    // In the sequel, we switch between the Chimere aerosol original model
    // (Bessagnet's) to the AMC one, order of instructions matters.
    //

    // Turn on AMC in Chimere.
    CHIMERE_AMC.Ng = AMC::ClassAerosolData::GetNg();
    CHIMERE_AMC.Nsection = AMC::ClassDiscretizationSize::GetNsection();
    CHIMERE_AMC.Nspecies = AMC::ClassSpecies::GetNspecies();
    CHIMERE_AMC.Nvariable = CHIMERE_AMC.Nspecies;

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
    // We also track the particle number concentration (+1).
    CHIMERE_AMC.Nvariable++;
#endif

#ifdef CHIMERE_AMC_WITH_WATER
    // Chimere puts water, liquid water content (LWC) I guess, in the species list. Usually, it is not
    // necessary to track the LWC down the 3D domain as it is constantly diagnosed by the Aerosol
    // Model. However some 3D processes may need it, e.g. deposition or cloud chemistry. So we have to let
    // water in the concentration field for Chimere+AMC, though not at the very same place it used to be.
    CHIMERE_AMC.Nvariable++;
#endif

    // Then write new configuration for AMC, to be read by Chimere.
    if (ClassDriverBase::rank_ == 0)
      ClassChimereBase::WriteConfiguration();

    // Shutdown original Chimere aerosol model.
    CHIMERE_PARAMS(bins) = 0;
    CHIMERE_PARAMS(aero) = 0;
    CHIMERE_PARAMS(nspec) -= CHIMERE_PARAMS(ms) * CHIMERE_PARAMS(nkc);
    CHIMERE_PARAMS(ms) = 0;
    CHIMERE_PARAMS(nkc) = 0;
    CHIMERE_PARAMS(dpmin) = double(0);
    CHIMERE_PARAMS(dpmax) = double(0);

    // Also shutdown families, for that time.
    CHIMERE_PARAMS(nfam) = 0;

    // Set correctly ngassp. Then, Chimere uses MPI to send it to all workers.
    if (ClassDriverBase::rank_ == 0)
      CHIMERE_COMMON(ngassp) = CHIMERE_PARAMS(nspec);

    // Reset "nspec" to its AMC correct size.
    CHIMERE_PARAMS(nspec) += CHIMERE_AMC.Ng * CHIMERE_AMC.Nvariable;

    // Reflects changes in the workers.
    WORKER_PARAMS(aero) = CHIMERE_PARAMS(aero);
    WORKER_PARAMS(bins) = CHIMERE_PARAMS(bins);
    WORKER_PARAMS(ms) = CHIMERE_PARAMS(ms);
    WORKER_PARAMS(nkc) = CHIMERE_PARAMS(nkc);
    WORKER_PARAMS(nfam) = CHIMERE_PARAMS(nfam);
    WORKER_PARAMS(nspec) = CHIMERE_PARAMS(nspec);

    // Boundary condition subsystem will need this. We could do it also in chimere_amc.(h/c)xx, we leave it
    // here because the clear method is also called.
    if (ClassDriverBase::rank_ == 0)
      ClassMappingParticle<D>::Init();

    MPI::COMM_WORLD.Barrier();

    //
    // CHIMERE with AMC.
    //
#endif

#ifdef DRIVER_CHIMERE_WITH_DEBUG
    getchar();
#endif

    //
    // Init the fortran 90 side.
    //

    chimere_init_();

    //
    //
    //

#ifdef CHIMERE_WITH_AMC
    if (ClassDriverBase::rank_ == 0)
      {
        // Initialize.
        for (int i = 0; i < CHIMERE_PARAMS(nspec); ++i)
          {
            CHIMERE_COMMON(idaero)[i] = 0;
            CHIMERE_COMMON(nsecti)[i] = 0;
            CHIMERE_COMMON(ncompo)[i] = 0;
          }

        // How the concentration vector is organized in CHIMERE.
        int k = CHIMERE_COMMON(ngassp) - 1;

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
        // Number concentration.
        for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
          {
            CHIMERE_COMMON(idaero)[++k] = 1;
            CHIMERE_COMMON(nsecti)[k] = i + 1;
            CHIMERE_COMMON(ncompo)[k] = -1;
          }
#endif
        // Mass concentration, we do not follow
        // the usual storage order of Chimere.
        for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
          for (int j = 0; j < CHIMERE_AMC.Nspecies; ++j)
            {
              CHIMERE_COMMON(idaero)[++k] = 1;
              CHIMERE_COMMON(nsecti)[k] = i + 1;
              CHIMERE_COMMON(ncompo)[k] = j + 1;
            }

#ifdef CHIMERE_AMC_WITH_WATER
        // Liquid water content ... is placed at the end, so as not to perturb the AMC concentration order
        // management. When included, the LWC/water index is CHIMERE_AMC.Nspecies in C/C++ and
        // CHIMERE_AMC.Nspecies + 1.
        for (int i = 0; i < CHIMERE_AMC.Ng; ++i)
          {
            CHIMERE_COMMON(idaero)[++k] = 1;
            CHIMERE_COMMON(nsecti)[k] = i + 1;
            CHIMERE_COMMON(ncompo)[k] = CHIMERE_AMC.Nspecies + 1;
          }
#endif
      }

    MPI::COMM_WORLD.Barrier();
#endif

    // Allocate arrays.
    ClassChimereBase::Allocate();

#ifdef DRIVER_WITH_EMISSION
    // Allocate arrays.
    if (ClassDriverBase::rank_ == 0)
      ClassChimereEmissionAnthropic<D>::Allocate();
    else
      ClassChimereEmissionBiogenic<D>::Allocate();

    // Wait everyone to finish its allocation.
    MPI::COMM_WORLD.Barrier();

#ifdef CHIMERE_WITH_AMC
    // Initialize the AMC emission dynamics subsystem.
    // This has nothing to do with Chimere yet.
    Ops::Ops ops(ClassDriverBase::configuration_file_);
    ops.SetPrefix(ClassDriverBase::prefix_ + ".");
    ClassDynamicsEmission<D>::Init(ops);
    ops.Close();

    // Finally, the emission part specific to Chimere.
    if (ClassDriverBase::rank_ == 0)
      ClassChimereEmissionAnthropic<D>::Init();

    ClassChimereEmissionBiogenic<D>::Init();
#endif
#endif
  }


  // Clear static data.
  template<class S, class R, class D, class C>
  void ClassDriverChimere<S, R, D, C>::Clear()
  {
    ClassChimereBase::Clear();

#ifdef CHIMERE_WITH_AMC
    ClassChimereBoundaryCondition<D>::Clear();
    ClassMappingParticle<D>::Clear();

#ifdef DRIVER_WITH_EMISSION
    ClassChimereEmissionAnthropic<D>::Clear();
    ClassChimereEmissionBiogenic<D>::Clear();
    ClassDynamicsEmission<D>::Clear();
#endif

    AMC::ClassAMX<S, R, D, C>::Terminate();
#endif

    // Clear the fortran side.
    chimere_end_();

    ClassDriverDomainBase::Clear();
  }


  // Run.
  template<class S, class R, class D, class C>
  void ClassDriverChimere<S, R, D, C>::Run()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << LogReset()
                         << "Chimere starts at " << CHIMERE_COMMON(idatestart)
                         << " for " << CHIMERE_COMMON(nhourrun) << " hours." << endl;
#endif

    // Pass the hand to fortran 90.
    chimere_run_();
  }
}

#define AMC_FILE_CLASS_DRIVER_CHIMERE_CXX
#endif
