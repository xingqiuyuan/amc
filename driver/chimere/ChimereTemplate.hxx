// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CHIMERE_TEMPLATE_HXX

//
// AMC templates chosen at compile time with command line options.
//

#if defined CHIMERE_WITH_DISCRETIZATION_COMPOSITION_BASE
typedef AMC::disc_comp_base disc_comp;
#elif defined CHIMERE_WITH_DISCRETIZATION_COMPOSITION_TABLE
typedef AMC::disc_comp_table disc_comp;
#else
typedef int disc_comp;
#endif

#if defined CHIMERE_WITH_REDISTRIBUTION_COMPOSITION_BASE
typedef AMC::ClassRedistributionCompositionBase<disc_comp> redist_comp;
#elif defined CHIMERE_WITH_REIDSTRIBUTION_COMPOSITION_TABLE
typedef AMC::ClassRedistributionCompositionTable<disc_comp> redist_comp;
#else
typedef int redist_comp;
#endif

#if defined CHIMERE_WITH_REDISTRIBUTION_SIZE_EULER_MIXED
typedef AMC::redist_size_mixed redist_size;
#elif defined CHIMERE_WITH_REDISTRIBUTION_SIZE_EULER_HYBRID
typedef AMC::redist_size_hybrid redist_size;
#elif defined CHIMERE_WITH_REDISTRIBUTION_SIZE_MOVING_DIAMETER
typedef AMC::redist_size_moving redist_size;
#elif defined CHIMERE_WITH_REDISTRIBUTION_SIZE_TABLE_MIXED
typedef AMC::redist_size_mixed_table redist_size;
#elif defined CHIMERE_WITH_REDISTRIBUTION_SIZE_TABLE_HYBRID
typedef AMC::redist_size_hybrid_table redist_size;
#else
typedef int redist_size;
#endif

#if defined CHIMERE_WITH_COAGULATION_COEFFICIENT_REPARTITION_MOVING
typedef AMC::ClassCoefficientRepartitionCoagulationMoving<redist_comp, disc_comp> coag_coef_repart;
#elif defined CHIMERE_WITH_COAGULATION_COEFFICIENT_REPARTITION_STATIC
typedef AMC::ClassCoefficientRepartitionCoagulationStatic<disc_comp> coag_coef_repart;
#else
typedef int coag_coef_repart;
#endif


#define AMC_FILE_CHIMERE_TEMPLATE_HXX
#endif
