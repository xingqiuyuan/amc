// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_RADIAL_HXX

namespace Driver
{
  /*!
   * \class ClassNumericalSolverDiffusionRadial
   */
  class ClassNumericalSolverDiffusionRadial
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  private:

    /*!< Size discretization.*/
    static int n_;

    /*!< Space step.*/
    static real dr_;

    /*!< Square space step.*/
    static real dr2_;

    /*!< Ratio time step / (space step * space step).*/
    static real s_;

    /*!< Radius bounds.*/
    static Array<real, 1> rb_;

    /*!< Inverse of radius centers.*/
    static Array<real, 1> rc_inv_;

    /*!< Working vectors.*/
    static Array<real, 1> D2_;

  public:

    /*!< Init.*/
    static void Init(const int n, const real dr);

    /*!< Set methods.*/
    static void SetTimeStep(const real &dt);

#ifndef SWIG
    /*!< Diffusion scheme.*/
    static void ExplicitScheme(const Array<real, 1> &D, const Array<real, 1> &u, Array<real, 1> &F);
#endif

#ifdef DRIVER_WITH_TEST
    static void Test(const string scheme, const real concentration,
                     const real diffusion = real(0.1), const int n = 50, const real dt = real(0.1),
                     const real R = real(10));
#endif
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_RADIAL_HXX
#endif
