// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_ADVECTION_LAX_WENDROFF_HXX

namespace Driver
{
  /*!
   * \class ClassNumericalSolverAdvectionLaxWendroff
   */
  template<class FL>
  class ClassNumericalSolverAdvectionLaxWendroff
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  private:

    /*!< Size discretization.*/
    static int n_;

    /*!< Space step.*/
    static real dx_;

    /*!< Ratio time step / space step.*/
    static real s_;

    /*!< Working vectors.*/
    static Array<real, 1> f_, nu_, gamma_;

  public:

    /*!< Init.*/
    static void Init(const int n, const real dx);

    /*!< Set methods.*/
    static void SetTimeStep(const real &dt);

#ifndef SWIG
    /*!< Lax-Wendroff explicit advection scheme with Roe's Superbee flux limiter.*/
    static void ExplicitScheme(const Array<real, 1> &V, const Array<real, 1> &u, Array<real, 1> &F);
#endif

#ifdef DRIVER_WITH_TEST
    /*!< Test schemes.*/
    static void Test(const string scheme, const real concentration, const real speed = real(1),
                     const int n = 50, const real dt = real(0.01), const real l = real(1),
                     const real L = real(10), const real cutoff = real(1.e-10));
#endif
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_ADVECTION_LAX_WENDROFF_HXX
#endif
