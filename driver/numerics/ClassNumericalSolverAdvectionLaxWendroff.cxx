// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_ADVECTION_LAX_WENDROFF_CXX

namespace Driver
{
  // Init.
  template<class FL>
  void ClassNumericalSolverAdvectionLaxWendroff<FL>::Init(const int n, const real dx)
  {
    if (dx <= real(0)) throw AMC::Error("dx <= 0.");

    n_ = n;
    dx_ = dx;
    s_ = real(0);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Init advection." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "n = " << n_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "dx = " << dx_ << endl;
#endif

    f_.resize(Range(-1, n_ - 1));
    nu_.resize(Range(-1, n_ - 1));
    gamma_.resize(Range(-1, n_ - 1));

    f_ = real(0);
    nu_ = real(0);
    gamma_ = real(0);
  }


  // Set methods.
  template<class FL>
  void ClassNumericalSolverAdvectionLaxWendroff<FL>::SetTimeStep(const real &dt)
  {
    s_ = dt / dx_;

#ifdef DRIVER_WITH_DEBUG_ADVECTION
    CBUG << "s = " << s_ << endl;
#endif
  }


  // Lax-Wendroff advection scheme.
  template<class FL>
  void ClassNumericalSolverAdvectionLaxWendroff<FL>::
  ExplicitScheme(const Array<real, 1> &V, const Array<real, 1> &u, Array<real, 1> &F)
  {
    f_ = V * u;

    for (int i = -1; i < n_; ++i)
      {
        const real denom = u(i + 1) - u(i);

        if (abs(denom) > real(0))
          nu_(i) = (f_(i + 1) - f_(i)) / denom;
        else
          nu_(i) = V(i);
      }

    nu_ *= s_;

    FL::Compute(n_, u, nu_, gamma_);

    for (int i = 0; i < n_; ++i)
      {
        real a, b;

        if (nu_(i) >= real(0))
          {
            a = real(1) - real(0.5) * (real(1) - nu_(i - 1)) * gamma_(i - 1);
            b = - real(0.5) * (real(1) - nu_(i)) * gamma_(i);
          }
        else
          {
            a = real(0.5) * (real(1) + nu_(i - 1)) * gamma_(i - 1);
            b = real(0.5) * (real(1) + nu_(i)) * gamma_(i) - real(1);
          }

        F(i) = a * f_(i - 1) - (a + b) * f_(i) + b * f_(i + 1);
      }

    F *= s_;
  }


#ifdef DRIVER_WITH_TEST
  // Test schemes.
  template<class FL>
  void ClassNumericalSolverAdvectionLaxWendroff<FL>::Test(const string scheme, const real concentration, const real speed,
                                                          const int n, const real dt, const real l, const real L,
                                                          const real cutoff)
  {
#ifndef DRIVER_WITH_DEBUG_ADVECTION
    throw AMC::Error("Advection driver should be compiled with debugging for this test to work.");
#endif

    // Initial concentration.
    if (l >= L)
      throw AMC::Error("Initial concentration length (l = " + to_str(l) + ")" +
                       " must be < to advection length (L = " + to_str(L) + ").");

    // Space discretization.
    const real dx = L / real(n);

    // Init scheme.
    Init(n, dx);

    Array<real, 1> u(Range(-1, n));
    u = 0;
    u(Range(0, int(l / dx + real(1.e-6)))) = concentration;

    // Speed.
    if (speed < real(0))
      throw AMC::Error("Constant speed (= " + to_str(speed) + ") must be > 0");

    Array<real, 1> V(Range(-1, n));
    V = speed;

    // Maximum time step.
    const real dt_max = dx_ / max(V);

#ifdef DRIVER_WITH_DEBUG_ADVECTION
    CBUG << "dt_max = " << dt_max << endl;
#endif

    if (dt > dt_max)
      throw AMC::Error("Time step provided (= " + to_str(dt) + ") greater than maximum time step (= " +
                       to_str(dt_max) + "), CFL condition violated.");

    SetTimeStep(dt);

    // Advection flux.
    Array<real, 1> F(n_);

    Range all = Range::all();
    Range RangeX(0, n_ - 1);

    bool end(false);
    real t(0);

    if (scheme.find("explicit") != string::npos)
      while (! end)
        {
#ifdef DRIVER_WITH_DEBUG_ADVECTION
          CBUG << "t = " << t << endl;
          CBUG << "u = " << u << endl;
          CBUG << "sum(u) = " << sum(u) << endl;
          getchar();
#endif

          ExplicitScheme(V, u, F);

#ifdef DRIVER_WITH_DEBUG_ADVECTION
          CBUG << "F = " << F << endl;
#endif

          u(RangeX) += F(all);

          end = true;
          for (int i = 0; i < n_; ++i)
            if (u(i) > cutoff)
              {
                end = false;
                break;
              }

          t += dt;
        }
    else
      throw AMC::Error("Not implemented scheme \"" + scheme + "\".");
  }
#endif
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_ADVECTION_LAX_WENDROFF_CXX
#endif
