// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_FLUX_LIMITER_ROES_SUPERBEE_HXX

namespace Driver
{
  /*!
   * \class ClassNumericalSolverFluxLimiterRoesSuperbee
   */
  class ClassNumericalSolverFluxLimiterRoesSuperbee
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  public:

#ifndef SWIG
    /*!< Roe's Superbee flux limiter for advection scheme.*/
    static void Compute(const int n, const Array<real, 1> &u,
                        const Array<real, 1> &nu, Array<real, 1> &gamma);
#endif
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_FLUX_LIMITER_ROES_SUPERBEE_HXX
#endif
