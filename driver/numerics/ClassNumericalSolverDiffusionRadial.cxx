// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_RADIAL_CXX

namespace Driver
{
  // Init.
  void ClassNumericalSolverDiffusionRadial::Init(const int n, const real dr)
  {
    if (dr <= real(0)) throw AMC::Error("dr <= 0.");

    n_ = n;
    dr_ = dr;
    dr2_ = dr * dr;
    s_ = real(0);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "Init radial diffusion." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "n = " << n_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "dr = " << dr_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "dr2 = " << dr2_ << endl;
#endif

    D2_.resize(Range(-1, n_ - 1));
    D2_ = real(0);

    rb_.resize(Range(-1, n_ - 1));
    rb_ = (blitz::tensor::i + 1) * dr_;

    const real dr_inv = real(1) / dr_;
    rc_inv_.resize(n_);
    rc_inv_ = dr_inv / (blitz::tensor::i + real(0.5));

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    CBUG << "rb = " << rb_ << endl;
    CBUG << "rc_inv = " << rc_inv_ << endl;
#endif
  }


  // Set methods.
  void ClassNumericalSolverDiffusionRadial::SetTimeStep(const real &dt)
  {
    s_ = dt / dr2_;

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    CBUG << "s = " << s_ << endl;
#endif
  }


  // Diffusion scheme.
  void ClassNumericalSolverDiffusionRadial::ExplicitScheme(const Array<real, 1> &D, const Array<real, 1> &u, Array<real, 1> &F)
  {
    for (int i = -1; i < n_; ++i)
      D2_(i) = D(i) + D(i + 1);
    D2_ *= real(0.5);

    for (int i = 0; i < n_; ++i)
      F(i) = rc_inv_(i) * (rb_(i - 1) * D2_(i - 1) * u(i - 1) - (rb_(i - 1) * D2_(i - 1) + rb_(i) * D2_(i)) * u(i)
                           + rb_(i) * D2_(i) * u(i + 1));

    F *= s_;
  }

#ifdef DRIVER_WITH_TEST
  void ClassNumericalSolverDiffusionRadial::Test(const string scheme, const real concentration,
                                                 const real diffusion, const int n, const real dt,
                                                 const real R)
  {
#ifndef DRIVER_WITH_DEBUG_DIFFUSION
    throw AMC::Error("Diffusion driver should be compiled with debugging for this test to work.");
#endif

    // Space discretization.
    const real dr = R / real(n);

    // Init.
    Init(n, dr);

    // Average radius.
    Array<real, 1> r(n_);
    r = (blitz::tensor::i + real(0.5)) * dr;

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    CBUG << "r = " << r << endl;
#endif

    // Initial concentration.
    Array<real, 1> u(Range(-1, n_));
    u = 0;
    u(0) = concentration;

    // Diffusion coefficient.
    Array<real, 1> D(Range(-1, n_));
    D = diffusion;

    // Maximum time step.
    const real dt_max = dr2_ / max(D);

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    CBUG << "dt_max = " << dt_max << endl;
#endif

    if (dt > dt_max)
      throw AMC::Error("Time step provided (= " + to_str(dt) + ") greater than maximum time step (= " +
                       to_str(dt_max) + ").");

    SetTimeStep(dt);

    // Diffusion flux.
    Array<real, 1> F(n_);

    Range all = Range::all();
    Range RangeR(0, n_ - 1);

    real t(0);

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    Array<real> ru(n_);
#endif

    if (scheme.find("explicit") != string::npos)
      while (true)
        {
#ifdef DRIVER_WITH_DEBUG_DIFFUSION
          CBUG << "t = " << t << endl;
          CBUG << "u = " << u << endl;
          ru = r * u(RangeR);
          CBUG << "sum(r * u) = " << sum(ru) << endl;
          getchar();
#endif

          ExplicitScheme(D, u, F);

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
          CBUG << "F = " << F << endl;
#endif

          // Integrate.
          u(RangeR) += F(all);

          // No fluxes at bounds.
          u(n_) = u(n_ - 1);
          u(-1) = u(0);

          t += dt;
        }
    else
      throw AMC::Error("Not implemented scheme \"" + scheme + "\".");
  }
#endif
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_RADIAL_CXX
#endif
