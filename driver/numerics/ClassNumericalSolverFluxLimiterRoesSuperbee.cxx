// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_FLUX_LIMITER_ROES_SUPERBEE_CXX

namespace Driver
{
  // Roe's Superbee flux limiter for advection scheme.
  void ClassNumericalSolverFluxLimiterRoesSuperbee::
  Compute(const int n, const Array<real, 1> &u, const Array<real, 1> &nu, Array<real, 1> &gamma)
  {
    for (int i = -1; i < n; ++i)
      {
        const int j = nu(i) >= real(0) ? i - 1 : i + 1;
        const real num = u(j + 1) - (j < -1 ? real(0) : u(j));
        const real denom = u(i + 1) - u(i);
        gamma(i) = (abs(denom) > real(0)) ? num / denom : 3;

        if (gamma(i) < real(0))
          gamma(i) = real(0);
        else
          if (gamma(i) > real(2))
            gamma(i) = real(2);
          else if (gamma(i) > real(0.5) && gamma(i) < real(1))
            gamma(i) = real(1);
      }
  }
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_FLUX_LIMITER_ROES_SUPERBEE_CXX
#endif
