// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_LINEAR_CXX

namespace Driver
{
  // Init.
  void ClassNumericalSolverDiffusionLinear::Init(const int n, const real dx)
  {
    if (dx <= real(0)) throw AMC::Error("dx <= 0.");

    n_ = n;
    dx_ = dx;
    dx2_ = dx * dx;
    s_ = real(0);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "Init linear diffusion." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "n = " << n_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "dx = " << dx_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "dx2 = " << dx2_ << endl;
#endif

    D2_.resize(Range(-1, n_ - 1));
    D2_ = real(0);
  }


  // Set methods.
  void ClassNumericalSolverDiffusionLinear::SetTimeStep(const real &dt)
  {
    s_ = dt / dx2_;

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
    CBUG << "s = " << s_ << endl;
#endif
  }


  // Diffusion scheme.
  void ClassNumericalSolverDiffusionLinear::ExplicitScheme(const Array<real, 1> &D, const Array<real, 1> &u, Array<real, 1> &F)
  {
    for (int i = -1; i < n_; ++i)
      D2_(i) = D(i) + D(i + 1);
    D2_ *= real(0.5);

    for (int i = 0; i < n_; ++i)
      F(i) = D2_(i - 1) * u(i - 1) - (D2_(i - 1) + D2_(i)) * u(i) + D2_(i) * u(i + 1);

    F *= s_;
  }


#ifdef DRIVER_WITH_TEST
  void ClassNumericalSolverDiffusionLinear::Test(const string scheme, const real concentration,
                                                 const real diffusion, const int n, const real dt,
                                                 const real L)
  {
#ifndef DRIVER_WITH_DEBUG_DIFFUSION
    throw AMC::Error("Diffusion driver should be compiled with debugging for this test to work.");
#endif

    // Space discretization.
    const real dx = L / real(n);

    // Init.
    Init(n, dx);

    // Initial concentration.
    Array<real, 1> u(Range(-1, n_));
    u = 0;
    u(n_ / 2) = concentration;

    // Diffusion coefficient.
    Array<real, 1> D(Range(-1, n_));
    D = diffusion;

    // Maximum time step.
    const real dt_max = dx2_ / max(D);

#ifdef DRIVER_WITH_DEBUG_ADVECTION
    CBUG << "dt_max = " << dt_max << endl;
#endif

    if (dt > dt_max)
      throw AMC::Error("Time step provided (= " + to_str(dt) + ") greater than maximum time step (= " +
                       to_str(dt_max) + ").");

    SetTimeStep(dt);

    // Diffusion flux.
    Array<real, 1> F(n_);

    Range all = Range::all();
    Range RangeX(0, n_ - 1);

    real t(real(0));

    if (scheme.find("explicit") != string::npos)
      while (true)
        {
#ifdef DRIVER_WITH_DEBUG_DIFFUSION
          CBUG << "t = " << t << endl;
          CBUG << "u = " << u << endl;
          CBUG << "sum(u) = " << sum(u(Range(0, n - 1))) << endl;
          getchar();
#endif

          ExplicitScheme(D, u, F);

#ifdef DRIVER_WITH_DEBUG_DIFFUSION
          CBUG << "F = " << F << endl;
#endif

          // Integrate.
          u(RangeX) += F(all);

          // No fluxes at bounds.
          u(n_) = u(n_ - 1);
          u(-1) = u(0);

          t += dt;
        }
    else
      throw AMC::Error("Not implemented scheme \"" + scheme + "\".");
  }
#endif
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_DIFFUSION_LINEAR_CXX
#endif
