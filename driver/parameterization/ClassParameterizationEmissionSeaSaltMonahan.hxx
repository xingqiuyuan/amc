// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_PARAMETERIZATION_EMISSION_SEA_SALT_MONAHAN_HXX

#define DRIVER_EMISSION_SEA_SALT_MONAHAN_NQUADRATURE_DEFAULT 10000
#define DRIVER_EMISSION_SEA_SALT_MONAHAN_PI43     4.188790204786391  // 4 * Pi / 3
#define DRIVER_EMISSION_SEA_SALT_MONAHAN_INV_PI6  1.909859317102744   // 6 / Pi
#define DRIVER_EMISSION_SEA_SALT_MONAHAN_FRAC3    0.333333333333333

namespace Driver
{
  class ClassParameterizationEmissionSeaSaltMonahan
  {
  public:
    typedef DRIVER_PARAMETERIZATION_REAL real;
    typedef vector<int> vector1i;
    typedef vector<real> vector1r;

  private:

    /*!< Use direct spume mechanism.*/
    bool direct_spume_;

    /*!< Use indirect bubble mechanism.*/
    bool indirect_bubble_;

    /*!< Number of quadrature points.*/
    int Nquadrature_;

  public:

    /*!< Constructor.*/
    ClassParameterizationEmissionSeaSaltMonahan(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassParameterizationEmissionSeaSaltMonahan();

    /*!< Compute number density.*/
    real ComputeNumberDensity(const real &radius, const real &wind_speed_10m) const;

    /*!< Compute volume density.*/
    real ComputeVolumeDensity(const real &radius, const real &wind_speed_10m) const;

    /*!< Compute sectional distribution.*/
    void ComputeSectionalDistribution(const vector<real> &diameter,
                                      const real &wind_speed_10m,
                                      vector<real> &diameter_mean,
                                      vector<real> &distribution_number,
                                      vector<real> &distribution_volume,
                                      int Nquadrature = 0) const;
  };
}

#define DRIVER_FILE_CLASS_PARAMETERIZATION_EMISSION_SEA_SALT_MONAHAN_HXX
#endif
