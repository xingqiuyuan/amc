// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_DRIVER_FILE_CLASS_PARAMETERIZATION_PHYSICS_CXX

#include "ClassParameterizationPhysics.hxx"

namespace Driver
{
  // Compute atmospheric pressure from altitude above sea level.
  real ClassParameterizationPhysics::ComputeAtmosphericPressureFromAltitude(const real altitude)
  {
    return DRIVER_PHYSICS_SEA_LEVEL_PRESSURE
      * pow(real(1) - DRIVER_COMPUTE_PRESSURE_FROM_ALTITUDE_FACTOR_1 * altitude,
            DRIVER_COMPUTE_PRESSURE_FROM_ALTITUDE_FACTOR_2);
  }


  // Compute altitude above sea level from atmospheric pressure.
  real ClassParameterizationPhysics::ComputeAltitudeFromAtmosphericPressure(const real pressure)
  {
    return DRIVER_COMPUTE_ALTITUDE_FROM_PRESSURE_FACTOR_1
      * (pow(pressure / DRIVER_PHYSICS_SEA_LEVEL_PRESSURE,
             DRIVER_COMPUTE_ALTITUDE_FROM_PRESSURE_FACTOR_2) - real(1));
  }


  // Compute relative humidity from specific humidity.
  real ClassParameterizationPhysics::ComputeRelativeHumidityFromSpecificHumidity(const real &temperature, const real &pressure,
                                                                                 const real &specific_humidity)
  {
    return specific_humidity
      / (DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR * (real(1) - specific_humidity) + specific_humidity)
      * (pressure / AMC::ClassParameterizationPhysics::ComputeWaterSaturationVaporPressureClausiusClapeyron(temperature));
  }


  // Compute water vapor density from specific humidity.
  real ClassParameterizationPhysics::ComputeWaterVaporDensityFromSpecificHumidity(const real &temperature, const real &pressure,
                                                                                  const real &specific_humidity)
  {
    return specific_humidity / (DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR * (real(1) - specific_humidity) + specific_humidity)
      * (pressure * DRIVER_PHYSICS_WATER_MOLAR_MASS / (DRIVER_PHYSICS_UNIVERSAL_GAS_CONSTANT * temperature));
  }


  // Compute specific humidity from water vapor density.
  real ClassParameterizationPhysics::ComputeSpecificHumidityFromWaterVaporDensity(const real &temperature, const real &pressure,
                                                                                  const real &water_vapor_density)
  {
    return DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR * water_vapor_density
      / (pressure * DRIVER_PHYSICS_WATER_MOLAR_MASS / (DRIVER_PHYSICS_UNIVERSAL_GAS_CONSTANT * temperature)
         + (real(1) - DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR) * water_vapor_density);
  }


  // Compute specific humidity from relative humidity.
  real ClassParameterizationPhysics::ComputeSpecificHumidityFromRelativeHumidity(const real &temperature, const real &pressure,
                                                                                 const real &relative_humidity)
  {
    const real A = relative_humidity
      * AMC::ClassParameterizationPhysics::ComputeWaterSaturationVaporPressureClausiusClapeyron(temperature) / pressure;

    return A * DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR
      / (real(1) + A * (DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR - real(1)));
  }
}

#define AMC_DRIVER_FILE_CLASS_PARAMETERIZATION_PHYSICS_CXX
#endif
