// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_DRIVER_FILE_CLASS_PARAMETERIZATION_PHYSICS_HXX

#define DRIVER_PHYSICS_SEA_LEVEL_PRESSURE              101325.0    // Pascals
#define DRIVER_PHYSICS_TEMPERATURE_LAPSE_RATE          0.0065      // K.m^{-1}
#define DRIVER_PHYSICS_EARTH_SURFACE_GRAVITY           9.80665     // m.s^{-2}
#define DRIVER_PHYSICS_DRY_AIR_MOLAR_MASS              0.0289644   // kg.mol^{-1}
#define DRIVER_PHYSICS_WATER_MOLAR_MASS                0.018       // kg.mol^{-1}
#define DRIVER_PHYSICS_SEA_LEVEL_STANDARD_TEMPERATURE  288.15      // K
#define DRIVER_PHYSICS_UNIVERSAL_GAS_CONSTANT          8.314462175 // J.mol^{-1}.K^{-1}
#define DRIVER_PHYSICS_RATIO_MOLAR_MASS_WATER_AIR      0.6214525   // MM_water / MM_dry_air 

#define DRIVER_COMPUTE_PRESSURE_FROM_ALTITUDE_FACTOR_1  2.2557695644629534e-05
#define DRIVER_COMPUTE_PRESSURE_FROM_ALTITUDE_FACTOR_2  5.255781292873008
#define DRIVER_COMPUTE_ALTITUDE_FROM_PRESSURE_FACTOR_1  44330.76923076923
#define DRIVER_COMPUTE_ALTITUDE_FROM_PRESSURE_FACTOR_2 -0.19026666907849246

/*! 
 * \class ClassDriverParameterization
 */

namespace Driver
{
  class ClassParameterizationPhysics
  {
  public:

    typedef DRIVER_PARAMETERIZATION_REAL real;

    /*!< Compute atmospheric pressure from altitude above sea level.*/
    static real ComputeAtmosphericPressureFromAltitude(const real altitude = 0);

    /*!< Compute altitude above sea level from atmospheric pressure.*/
    static real ComputeAltitudeFromAtmosphericPressure(const real pressure = DRIVER_PHYSICS_SEA_LEVEL_PRESSURE);

    /*!< Compute relative humidity from specific humidity.*/
    static real ComputeRelativeHumidityFromSpecificHumidity(const real &temperature, const real &pressure,
                                                            const real &specific_humidity);

    /*!< Compute water vapor density from specific humidity.*/
    static real ComputeWaterVaporDensityFromSpecificHumidity(const real &temperature, const real &pressure,
                                                             const real &specific_humidity);

    /*!< Compute specific humidity from water vapor density.*/
    static real ComputeSpecificHumidityFromWaterVaporDensity(const real &temperature, const real &pressure,
                                                             const real &water_vapor_density);

    /*!< Compute specific humidity from relative humidity.*/
    static real ComputeSpecificHumidityFromRelativeHumidity(const real &temperature, const real &pressure,
                                                            const real &relative_humidity);
  };
}

#define AMC_DRIVER_FILE_CLASS_PARAMETERIZATION_PHYSICS_HXX
#endif
