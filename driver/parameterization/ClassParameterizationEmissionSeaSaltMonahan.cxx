// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_PARAMETERIZATION_EMISSION_SEA_SALT_MONAHAN_CXX

#include "ClassParameterizationEmissionSeaSaltMonahan.hxx"

namespace Driver
{
  // Constructor.
  ClassParameterizationEmissionSeaSaltMonahan::ClassParameterizationEmissionSeaSaltMonahan(Ops::Ops &ops)
    : direct_spume_(true), indirect_bubble_(true), Nquadrature_(DRIVER_EMISSION_SEA_SALT_MONAHAN_NQUADRATURE_DEFAULT)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << Reset()
                         << "Instantiate (Monahan, 1986) sea salt emission parameterization." << endl;
#endif

    direct_spume_ = ops.Get<bool>("direct_spume", "", direct_spume_);
    indirect_bubble_ = ops.Get<bool>("indirect_bubble", "", indirect_bubble_);
    Nquadrature_ = ops.Get<int>("Nquadrature", "", Nquadrature_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "direct_spume = " << direct_spume_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "indirect_bubble = " << indirect_bubble_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << LogReset() << "Nquadrature = " << Nquadrature_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassParameterizationEmissionSeaSaltMonahan::~ClassParameterizationEmissionSeaSaltMonahan()
  {
    return;
  }


  // Compute number density.
  real ClassParameterizationEmissionSeaSaltMonahan::ComputeNumberDensity(const real &radius, const real &wind_speed_10m) const
  {
    real number_density(real(0));

    if (indirect_bubble_)
      if (radius >= real(0.8) && radius <= real(10))
        {
          real B = (real(0.380) - log(radius)) / real(0.650);
          number_density += real(1.373) * pow(wind_speed_10m, real(3.41))
            * (real(1) + real(0.057) * pow(radius, real(1.05)))
            * pow(10, real(1.19) * exp(- B * B)) / (radius * radius * radius);
        }

    if (direct_spume_)
      {
        if (radius >= real(100))
          number_density += real(8.60e6) * exp(real(2.08) * wind_speed_10m)
            / (radius * radius * radius * radius * radius * radius * radius * radius);
        else if (radius >= real(75))
          number_density += real(4.83e-2) * exp(real(2.08) * wind_speed_10m)
            / (radius * radius * radius * radius);
        else if (radius >= real(10))
          number_density += real(8.60e-6) * exp(real(2.08) * wind_speed_10m) / (radius * radius);
      }

    return number_density;
  }


  // Compute volume density.
  real ClassParameterizationEmissionSeaSaltMonahan::ComputeVolumeDensity(const real &radius, const real &wind_speed_10m) const
  {
    return DRIVER_EMISSION_SEA_SALT_MONAHAN_PI43 * radius * radius *radius * ComputeNumberDensity(radius, wind_speed_10m);
  }


  // Compute sectional distribution.
  void ClassParameterizationEmissionSeaSaltMonahan::ComputeSectionalDistribution(const vector<real> &diameter,
                                                                                 const real &wind_speed_10m,
                                                                                 vector<real> &diameter_mean,
                                                                                 vector<real> &distribution_number,
                                                                                 vector<real> &distribution_volume,
                                                                                 int Nquadrature) const
  {
    int Nb = int(diameter.size()) - 1;

    if (Nb <= 0)
      throw AMC::Error("Need at least one size section (two diameters).");

    if (Nquadrature == 0)
      Nquadrature = Nquadrature_;

    if (Nquadrature <= 0)
      throw AMC::Error("Negative or null number of quadrature points.");

    for (int i = 0; i < Nb; i++)
      {
        const real &radius1 = diameter[i] * real(0.5);
        const real &radius2 = diameter[i + 1] * real(0.5);
        real log_radius1 = log(radius1);
        real log_radius2 = log(radius2);
        real log_difference = log_radius2 - log_radius1;
        real log_h = log_difference / real(Nquadrature);

        real num(real(0)), vol(real(0));
        num = (ComputeNumberDensity(radius1, wind_speed_10m) +
               ComputeNumberDensity(radius2, wind_speed_10m)) * real(0.5);
        vol = (ComputeVolumeDensity(radius1, wind_speed_10m) +
               ComputeVolumeDensity(radius2, wind_speed_10m)) * real(0.5);

        for (int j = 1; j < Nquadrature; j++)
          {
            real log_radius = log_radius1 + log_h * real(j);
            real radius = exp(log_radius);
            num += ComputeNumberDensity(radius, wind_speed_10m);
            vol += ComputeVolumeDensity(radius, wind_speed_10m);
          }

        distribution_number.push_back(num * log_h);
        distribution_volume.push_back(vol * log_h);
      }

    diameter_mean.resize(Nb);
    for (int i = 0; i < Nb; i++)
      {
        diameter_mean[i] = pow(distribution_volume[i] / distribution_number[i]
                               * DRIVER_EMISSION_SEA_SALT_MONAHAN_INV_PI6,
                               DRIVER_EMISSION_SEA_SALT_MONAHAN_FRAC3);

        if (diameter_mean[i] < diameter[i] || diameter_mean[i] >= diameter[i + 1])
          throw AMC::Error("Mean diameter of size section " + to_str(i) + " (" + to_str(diameter_mean[i])
                           + ") is outside of section bound diameters [" + to_str(diameter[i])
                           + ", " + to_str(diameter[i + 1]) + "[");
      }
  }
}
#define DRIVER_FILE_CLASS_PARAMETERIZATION_EMISSION_SEA_SALT_MONAHAN_CXX
#endif
