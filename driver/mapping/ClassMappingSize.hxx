// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SIZE_HXX

#define DRIVER_MAPPING_SIZE_EPSILON 1.e-8

namespace Driver
{
  /*! 
   * \class ClassMappingSize
   */
  class ClassMappingSize
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  protected:

    /*!< Whether static part is initialized.*/
    static bool initialized_;

    /*!< Nested class gathering AMC Proxies.*/
    class AMCP
    {
    public:

      /*!< Number of AMC size sections.*/
      static int Nsection_;

      /*!< Number of AMC size bounds.*/
      static int Nbound_;

      /*!< Proxy for bound AMC diameters.*/
      static vector1r diameter_bound_;

      /*!< Bound AMC volumes.*/
      static vector1r volume_bound_;

      /*!< Proxy for mean AMC diameters.*/
      static vector1r diameter_mean_;

      /*!< Size volume means for AMC.*/
      static vector1r volume_mean_;

#ifdef DRIVER_MAPPING_SIZE_WITH_TABLE
      /*!< Size volume mean table for AMC.*/
      static Array<real, 2> volume_mean_table_;
#endif

      /*!< Clear AMCP static data.*/
      static void Clear();
    };

    /*!< Whether coefficients are computed or not.*/
    bool computed_;

    /*!< Number of bounds.*/
    int Nbound_;

    /*!< Number of sections.*/
    int Nsection_;

    /*!< Name of size mapping.*/
    string name_;

    /*!< Size volume means.*/
    vector1r volume_mean_;

    /*!< Fraction indexes.*/
    Array<int, 2> fraction_index_;

    /*!< Number fractions.*/
    Array<real, 2> fraction_number_;

    /*!< Mass fractions.*/
    Array<real, 2> fraction_mass_;

    /*!< Compute indexes.*/
    int compute_index_(const real &diameter_min, const real &diameter_max, Array<int, 1> &fraction_index) const;

    /*!< Compute fractions.*/
    bool compute_fraction_(const real &volume_mean, const Array<int, 1> &fraction_index,
                           Array<real, 1> &fraction_number, Array<real, 1> &fraction_mass) const;

  protected:

    /*!< Get methods.*/
    int get_fraction_index_min_(const int i);
    int get_fraction_index_max_(const int i);
    real get_fraction_number_(const int i, const int j);
    real get_fraction_mass_(const int i, const int j);

  public:

    /*!< Init.*/
    static void Init();

    /*!< Clear.*/
    static void Clear();

    /*!< Constructor.*/
    ClassMappingSize(Ops::Ops *ops_ptr, const string name,
                     vector<real> diameter_bound = vector<real>(),
                     vector<real> diameter_mean = vector<real>(),
                     bool compute = false);

    /*!< Destructor.*/
    ~ClassMappingSize();

    /*!< Compute.*/
    void Compute(vector<real> *volume_mean_ptr = NULL);
    bool Compute(const int i, const real &volume_mean);

    /*!< Get methods.*/
    bool AreComputed() const;
    int GetNsection() const;
    int GetNbound() const;
    string GetName() const;
    void GetFractionIndex(vector<vector<int> > &fraction_index) const;
    void GetFractionNumber(vector<vector<real> > &fraction_number) const;
    void GetFractionMass(vector<vector<real> > &fraction_mass) const;
  };
}

#define AMC_FILE_CLASS_MAPPING_SIZE_HXX
#endif
