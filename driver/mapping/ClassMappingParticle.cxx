// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
//
// This file is part of AMC.
//
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAP_PARTICLE_CXX

#include "ClassMappingParticle.hxx"


namespace Driver
{
  // Init.
  template<class D>
  void ClassMappingParticle<D>::Init()
  {
    if (ClassMappingParticle<D>::is_initialized_)
      throw AMC::Error("Particle Mapping seems already initialized.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Init particle mapping static data." << endl;
#endif

    // Init species mapping static data.
    ClassMappingSpecies::Init();

    // Init size mapping static data.
    ClassMappingSize::Init();

    // Proper class data.
    is_initialized_ = true;

    AMCP::density_fixed_factor_ = AMC::ClassDiscretizationSize::GetDensityFixed() * (AMC_PI / real(6));
  }


  // Clear.
  template<class D>
  void ClassMappingParticle<D>::Clear()
  {
    // Clear size mapping static data.
    ClassMappingSize::Clear();

    // Clear species mapping static data.
    ClassMappingSpecies::Clear();

    is_initialized_ = false;
  }


  // Is static data initialized ?
  template<class D>
  bool ClassMappingParticle<D>::IsInitialized()
  {
    return is_initialized_;
  }


  // Constructor.
  template<class D>
  ClassMappingParticle<D>::ClassMappingParticle(Ops::Ops *ops_ptr, const string name,
                                                vector<vector<int> > general_section,
                                                vector<vector<string> > class_composition_name,
                                                vector<string> species_name,
                                                vector<real> diameter_bound,
                                                vector<real> diameter_mean,
                                                bool compute)
    : ClassMappingSpecies(ops_ptr, name + ".species", species_name),
      ClassMappingSize(ops_ptr, name + ".size", diameter_bound, diameter_mean, compute),
      general_section_(general_section), Ng_(int(general_section.size()))
  {
    if (! ClassMappingParticle<D>::is_initialized_)
      throw AMC::Error("Particle Mapping seems not yet initialized, call Init() first.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << LogReset() << "Instantiate particle mapping \""
                         << name << "\"." << endl;
#endif

    if (Ng_ == 0) throw AMC::Error("No data provided for general sections.");

    //
    // If configuration file exists.
    //

    const string prefix_orig = ops_ptr != NULL ? ops_ptr->GetPrefix() : string();

    // We will need this.
    vector1s entry_list;

    if (ops_ptr != NULL)
      if (ops_ptr->Exists(name + ".particle"))
        {
          ops_ptr->SetPrefix(prefix_orig + name + ".particle");
          entry_list = ops_ptr->GetEntryList();

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset()
                               << "Eventually read class composition data from section \""
                               << ops_ptr->GetPrefix() << "\"." << endl;
          *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "entry_list = " << entry_list << endl;
#endif
        }

    //
    // If we have internally mixed data, we need to know in
    // which class composition to put data for each size section.
    //

    for (int i = 0; i < Ng_; ++i)
      {
        string amc_class_composition_name;

        // If not internal mixing, the class composition to search in AMC is this one.
        if (! general_section_[i][1] < 0)
          amc_class_composition_name = class_composition_name[general_section_[i][0]][general_section_[i][1]];

        // If there is a setting in configuration file for this general section, it takes precedence.
        if (! entry_list.empty())
          for (vector1s::const_iterator it = entry_list.begin(); it != entry_list.end(); ++it)
            if (ops_ptr->Get<int>(*it + ".general_section") == i)
              {
                amc_class_composition_name = ops_ptr->Get<string>(*it + ".amc_class");

                // Stops at first match.
                break;
              }

        // If there is something to search.
        if (! amc_class_composition_name.empty())
          {
            bool error_occured(false);
            general_section_[i][1] = AMC::ClassDiscretizationClass<D>::
              FindClassCompositionIndexByName(general_section_[i][0], amc_class_composition_name, error_occured);

#ifdef AMC_WITH_LOGGER
              if (error_occured)
                *AMCLogger::GetLog() << Bred() << Logger::Error() << LogReset() << "Could not find"
                                     << " a valid index for class composition \"" << amc_class_composition_name
                                     << "\" at size section " << general_section_[i][0] << " for general section " << i << endl;
              else
                *AMCLogger::GetLog() << Fgreen() << Info(2) << LogReset() << "Found class composition \""
                                     << amc_class_composition_name << "\" at index " << general_section_[i][1]
                                     << " of size section " << general_section_[i][0] << " for general section " << i << endl;
#endif
        }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "Input general section " << i
                             << " of size section " << general_section_[i][0] << " goes into class composition \""
                             << (general_section_[i][1] < 0 ? "computed at run time" : "index " + to_str(general_section_[i][1])) << endl;
#endif
      }

    composition_in_work_.assign(ClassMappingSpecies::GetNspecies(), real(0));
    composition_out_work_.assign(AMC::ClassDiscretization<D>::Nspecies_, real(0));

    if (ops_ptr != NULL)
      ops_ptr->SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  template<class D>
  ClassMappingParticle<D>::~ClassMappingParticle()
  {
    return;
  }


  // Compute.
  template<class D>
  void ClassMappingParticle<D>::Compute(const real *concentration_in, real *concentration_out, const real *diameter_in)
  {
    // A few shortcuts.
    const real *concentration_in_mass_ptr = concentration_in + ClassMappingParticle<D>::Ng_;

    real *concentration_out_number = concentration_out;
    real *concentration_out_mass = concentration_out + AMC::ClassDiscretization<D>::Ng_;

    for (int h = 0; h < ClassMappingParticle<D>::Ng_; ++h)
      {
        const vector1i &general_section = ClassMappingParticle<D>::general_section_[h];

        const real &concentration_in_number = *(concentration_in + h);

        // Whether number concentration is available or we can reconstruct it with input diameters.
        if (concentration_in_number > DRIVER_MAPPING_PARTICLE_CONCENTRATION_NUMBER_THRESHOLD || diameter_in != NULL)
          {
            const real *concentration_in_mass = concentration_in_mass_ptr + ClassMappingParticle<D>::Ng_ * h;

            // Compute total mass, success if enough mass.
            real concentration_in_mass_total(real(0));

            if (ClassMappingSpecies::compute_total_mass_composition(concentration_in_mass, concentration_in_mass_total, composition_in_work_))
              {
                const real volume_mean = (diameter_in != NULL ? diameter_in[h] * diameter_in[h] * diameter_in[h] :
                                          concentration_in_mass_total / (concentration_in_number * AMCP::density_fixed_factor_));

                // The size section index of input concentrations.
                const int i = general_section[0];

                // Success if mapping size coefficients are consistent.
                if (ClassMappingSize::Compute(i, volume_mean))
                  for (int s = ClassMappingSize::get_fraction_index_min_(i); s < ClassMappingSize::get_fraction_index_max_(i); ++s)
                    {
                      // The running index "s" is the size section index of output concentrations, that of AMC
                      // discretization.

                      // Compute output (AMC) composition, eventually depending of the size section if some
                      // species are partitioned differently with respect to size sections, say a tracer which
                      // is considered as mostly organic at lowest sizes and mostly dust at greatest ones.
                      ClassMappingSpecies::Compute(s, composition_in_work_, composition_out_work_);

                      bool on_edge(false);
                      const int c = (general_section[1] < 0 ? AMC::ClassDiscretizationClass<D>::discretization_composition_[s]->
                                     FindClassCompositionIndex(composition_out_work_, on_edge) : general_section[1]);

                      const int g = AMC::ClassDiscretization<D>::general_section_reverse_[s][c];

                      concentration_out_number[g] += ClassMappingSize::get_fraction_number_(i, s) * concentration_in_number;

                      const real concentration_out_mass_total = concentration_in_mass_total * ClassMappingSize::get_fraction_mass_(i, s);

                      int j = g * AMC::ClassDiscretization<D>::Nspecies_ - 1;
                      for (int k = 0; k < AMC::ClassDiscretization<D>::Nspecies_; ++k)
                        concentration_out_mass[++j] += concentration_out_mass_total * composition_out_work_[k];
                    }
                else
                  {
                    // Something went wrong in the mapping size fraction computation.  Something potentially
                    // ugly. So we do not use them.
                    const int s = ClassMappingSize::get_fraction_index_min_(i);

                    ClassMappingSpecies::Compute(s, composition_in_work_, composition_out_work_);

                    bool on_edge(false);
                    const int c = (general_section[1] < 0 ? AMC::ClassDiscretizationClass<D>::discretization_composition_[s]->
                                   FindClassCompositionIndex(composition_out_work_, on_edge) : general_section[1]);

                    const int g = AMC::ClassDiscretization<D>::general_section_reverse_[s][c];

                    concentration_out_number[g] += concentration_in_number;

                    // This is the real difference with above.
                    const real concentration_out_mass_total = concentration_in_mass_total;

                    int j = g * AMC::ClassDiscretization<D>::Nspecies_ - 1;
                    for (int k = 0; k < AMC::ClassDiscretization<D>::Nspecies_; ++k)
                      concentration_out_mass[++j] += concentration_out_mass_total * composition_out_work_[k];                    
                  }
              }
          }
      }
  }
}

#define AMC_FILE_CLASS_MAPPING_PARTICLE_CXX
#endif
