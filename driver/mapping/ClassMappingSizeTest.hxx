// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SIZE_TEST_HXX

#define DRIVER_MAPPING_SIZE_TEST_NUMBER_DEFAULT 1e8
#define DRIVER_MAPPING_SIZE_TEST_SEED_DEFAULT "seed"

namespace Driver
{
  /*! 
   * \class ClassMappingSizeTest
   */
  class ClassMappingSizeTest : public ClassMappingSize
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  protected:

    /*!< Size diameter bounds.*/
    vector1r diameter_bound_;

    /*!< Size diameter means.*/
    vector1r diameter_mean_;

    /*!< Error count.*/
    static int error_count_;

    /*!< Error message.*/
    static ostringstream error_message_;

    /*!< Run test.*/
    inline void run(const real &diameter_min, const real &diameter_max, const real &diameter_mean,
                    Array<int, 1> &fraction_index, Array<real, 1> &fraction_number,
                    Array<real, 1> &fraction_mass) const;

  public:

    /*!< Constructor.*/
    ClassMappingSizeTest();
    ClassMappingSizeTest(const string name,
                         const vector<real> &diameter_bound,
                         const vector<real> &diameter_mean);

    /*!< Get methods.*/
    static int GetErrorCount();
    static string GetErrorMessage();

    /*!< Run.*/
    static void Run(ClassMappingSizeTest *mapping_size_ptr = NULL,
                    int N = DRIVER_MAPPING_SIZE_TEST_NUMBER_DEFAULT,
                    string seed = DRIVER_MAPPING_SIZE_TEST_SEED_DEFAULT);

    void RunSelf(const real diameter_min, const real diameter_max, const real diameter_mean,
                 vector<int> &fraction_index, vector<real> &fraction_number, vector<real> &fraction_mass) const;
  };
}

#define AMC_FILE_CLASS_MAPPING_SIZE_TEST_HXX
#endif
