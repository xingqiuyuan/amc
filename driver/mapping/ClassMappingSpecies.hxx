// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SPECIES_HXX

#define DRIVER_MAPPING_SPECIES_EPSILON 1.e-8
#define DRIVER_MAPPING_SPECIES_CONCENTRATION_MASS_THRESHOLD 1.e-16 // µg.m^{-3}

namespace Driver
{
  /*! 
   * \class ClassMappingSpecies
   */
  class ClassMappingSpecies
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  private:

    /*!< Number of input species.*/
    int Nspecies_;

    /*!< Nested class gathering AMC Proxies.*/
    class AMCP
    {
    public:

      /*!< Number of AMC size sections.*/
      static int Nsection_;

      /*!< Number of AMC species.*/
      static int Nspecies_;
    };

    /*!< Is it initialized ?*/
    static bool initialized_;

    /*!< Mapping name.*/
    string name_;

    /*!< Size of 3D arrays.*/
    Array<int, 2> Nsize_;

    /*!< Array of AMC species indexes on a per size section and per input species basis.*/
    Array<int, 3> index_;

    /*!< Array of AMC species fractions on a per size section and per input species basis.*/
    Array<real, 3> fraction_;

  protected:

    /*!< Compute total mass and composition.*/
    bool compute_total_mass_composition(const real *concentration_in, real &concentration_in_mass_total, vector<real> &composition_in) const;

  public:

    /*!< Init.*/
    static void Init();

    /*!< Clear.*/
    static void Clear();

    /*!< Constructor.*/
    ClassMappingSpecies(Ops::Ops *ops_ptr, const string name,
                        vector<string> species_name = vector<string>());

    /*!< Destructor.*/
    ~ClassMappingSpecies();

    /*!< Get methods.*/
    int GetNspecies() const;

    /*!< Compute.*/
    void Compute(const int size_index, const vector<real> &concentration_in, vector<real> &concentration_out) const;
  };
}

#define AMC_FILE_CLASS_MAPPING_SPECIES_HXX
#endif
