// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SIZE_CXX

#include "ClassMappingSize.hxx"

#define FMIN  0
#define FMAX  1
#define FSIZE 2
#define FLEN  3

#define WMIN  0
#define WMAX  1
#define WCUT  2
#define WSIZE 3
#define IWLEN 4

#define WMEAN 0
#define WNUM  1
#define WMASS 2
#define RWLEN 3

namespace Driver
{
  // Get methods.
  int ClassMappingSize::get_fraction_index_min_(const int i)
  {
    return fraction_index_(i, FMIN);
  }

  int ClassMappingSize::get_fraction_index_max_(const int i)
  {
    return fraction_index_(i, FMAX);
  }

  real ClassMappingSize::get_fraction_number_(const int i, const int j)
  {
    return fraction_number_(i, j);
  }

  real ClassMappingSize::get_fraction_mass_(const int i, const int j)
  {
    return fraction_mass_(i, j);
  }


  // Compute fraction indexes.
  inline int ClassMappingSize::compute_index_(const real &diameter_min, const real &diameter_max,
                                              Array<int, 1> &fraction_index) const
  {
    // Initialize to default values.
    fraction_index = -1;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "diameter_min = " << diameter_min << endl;
    CBUG << "diameter_max = " << diameter_max << endl;
    CBUG << "diameter_bound_amc = " << AMCP::diameter_bound_ << endl;
#endif

    // Return if no solution at all.
    if (diameter_min >= AMCP::diameter_bound_.back() || diameter_max < AMCP::diameter_bound_.front())
      return -1;

    // Effective number of fractions.
    if (diameter_min < AMCP::diameter_bound_.front())
      fraction_index(FMIN) = 0;
    else
      for (int i = 0; i < AMCP::Nbound_; ++i)
        if (AMCP::diameter_bound_[i] > diameter_min)
          {
            fraction_index(FMIN) = i - 1;
            break;
          }

    if (diameter_max > AMCP::diameter_bound_.back())
      fraction_index(FMAX) = AMCP::Nsection_;
    else
      for (int i = fraction_index(FMIN) + 1; i < AMCP::Nbound_; ++i)
        if (AMCP::diameter_bound_[i] >= diameter_max)
          {
            fraction_index(FMAX) = i; // Last excluded.
            break;
          }

    fraction_index(FSIZE) = fraction_index(FMAX) - fraction_index(FMIN);

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "fraction_index = " << fraction_index << endl;
#endif

    return fraction_index(FSIZE);
  }


  // Compute fractions.
  inline bool ClassMappingSize::compute_fraction_(const real &volume_mean,
                                                  const Array<int, 1> &fraction_index,
                                                  Array<real, 1> &fraction_number,
                                                  Array<real, 1> &fraction_mass) const
  {
    // Initialize to default values.
    fraction_number = real(0);
    fraction_mass = real(0);

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "volume_mean = " << volume_mean << endl;
    CBUG << "fraction_index = " << fraction_index << endl;
    CBUG << "AMCP::volume_mean = " << AMCP::volume_mean_ << endl;
#endif

    // Whether coefficients are effectively
    // computed or one unusual case arose.
    bool well_computed(false);

    // The following algorithm assumes valid fraction indexes.
    if (fraction_index(FSIZE) > 1) // If CHIMERE section is NOT all in one AMC section.
      {
        // First, filter some very unusual cases.
        if (volume_mean < AMCP::volume_mean_[fraction_index(FMIN)])
          {
            fraction_number(fraction_index(FMIN)) = real(1);
            fraction_mass(fraction_index(FMIN)) = real(1);
          }
        else if (volume_mean >= AMCP::volume_mean_[fraction_index(FMAX) - 1])
          {
            fraction_number(fraction_index(FMAX) - 1) = real(1);
            fraction_mass(fraction_index(FMAX) - 1) = real(1);
          }
        else
          {
            const Range fraction_range = Range(fraction_index(FMIN), fraction_index(FMAX) - 1);
            fraction_number(fraction_range) = real(1);
            fraction_mass(fraction_range) = real(1);

            const int nwork = 2 * fraction_index(FSIZE) + 1;
            Array<real, 2> iwork(nwork, IWLEN);
            Array<real, 2> rwork(nwork, RWLEN);

            iwork = -1;
            iwork(0, WMIN) = fraction_index(FMIN);
            iwork(0, WMAX) = fraction_index(FMAX);
            iwork(0, WSIZE) = fraction_index(FSIZE);

            rwork = real(1);
            rwork(0, WMEAN) = volume_mean;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
            CBUG << "iwork = " << iwork << endl;
            CBUG << "rwork = " << rwork << endl;
            CBUG << "fraction_number = " << fraction_number << endl;
            CBUG << "fraction_mass = " << fraction_mass << endl;
#endif

            int i(0), j(1);
            while (i < j)
              {
#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
                CBUG << "i = " << i << ", j = " << j << endl;
#endif
                if (iwork(i, WSIZE) > 1)
                  {
                    if (iwork(i, WSIZE) == 2)
                      iwork(i, WCUT) = iwork(i, WMAX) - 1;
                    else
                      for (int k = iwork(i, WMIN); k < iwork(i, WMAX); ++k)
                        if (AMCP::volume_mean_[k] >= rwork(i, WMEAN))
                          {
                            iwork(i, WCUT) = k;
                            break;
                          }

#ifndef DRIVER_MAPPING_SIZE_WITH_TABLE
                    for (int k = iwork(i, WMIN); k < iwork(i, WCUT); ++k)
                      rwork(j, WMEAN) *= AMCP::volume_mean_[k];
#endif
                    iwork(j, WMIN) = iwork(i, WMIN);
                    iwork(j, WMAX) = iwork(i, WCUT);
                    iwork(j, WSIZE) = iwork(i, WCUT) - iwork(i, WMIN);

#ifndef DRIVER_MAPPING_SIZE_WITH_TABLE
                    rwork(j, WMEAN) = pow(rwork(j, WMEAN), real(1) / real(iwork(j, WSIZE)));
#else
                    rwork(j, WMEAN) = AMCP::volume_mean_table_(iwork(i, WMIN), iwork(i, WCUT) - 1);
#endif
                    j++;

#ifndef DRIVER_MAPPING_SIZE_WITH_TABLE
                    for (int k = iwork(i, WCUT); k < iwork(i, WMAX); ++k)
                      rwork(j, WMEAN) *= AMCP::volume_mean_[k];
#endif
                    iwork(j, WMIN) = iwork(i, WCUT);
                    iwork(j, WMAX) = iwork(i, WMAX);
                    iwork(j, WSIZE) = iwork(i, WMAX) - iwork(i, WCUT);

#ifndef DRIVER_MAPPING_SIZE_WITH_TABLE
                    rwork(j, WMEAN) = pow(rwork(j, WMEAN), real(1) / real(iwork(j, WSIZE)));
#else
                    rwork(j, WMEAN) = AMCP::volume_mean_table_(iwork(i, WMCUT), iwork(i, WMAX) - 1);
#endif
                    rwork(j - 1, WNUM) = (rwork(j, WMEAN) - rwork(i, WMEAN))
                      / (rwork(j, WMEAN) - rwork(j - 1, WMEAN));
                    rwork(j, WNUM) -= rwork(j - 1, WNUM);

                    rwork(j, WMASS) = rwork(j, WNUM) * rwork(j, WMEAN) / rwork(i, WMEAN);
                    rwork(j - 1, WMASS) -= rwork(j, WMASS);

                    j++;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
                    CBUG << "iwork = " << iwork << endl;
                    CBUG << "rwork = " << rwork << endl;
#endif
                  }

                i++;

                for (int k = iwork(i, WMIN); k < iwork(i, WMAX); ++k)
                  {
                    fraction_number(k) *= rwork(i, WNUM);
                    fraction_mass(k) *= rwork(i, WMASS);
                  }

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
                CBUG << "fraction_number = " << fraction_number << endl;
                CBUG << "fraction_mass = " << fraction_mass << endl;
#endif
              }

            // If everything ok at this point.
            well_computed = true;
          }
      }
    else
      {
        fraction_number(fraction_index(FMIN)) = real(1);
        fraction_mass(fraction_index(FMIN)) = real(1);
      }

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "fraction_number = " << fraction_number << endl;
    CBUG << "fraction_mass = " << fraction_mass << endl;
#endif

    // Always return.
    return well_computed;
  }


  // Init.
  void ClassMappingSize::Init()
  {
    //
    // Check if AMC is up and retrieve proxy.
    //

    if (initialized_)
      throw AMC::Error("Mapping size seems already initialized.");

    if (AMC::ClassDiscretizationSize::GetNsection() == 0)
      throw AMC::Error("AMC seems not initialized : no size sections defined.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info() << LogReset() << "Init mapping size static data." << endl;
#endif

    AMCP::Nsection_ = AMC::ClassDiscretizationSize::GetNsection();
    AMCP::Nbound_ = AMC::ClassDiscretizationSize::GetNbound();

    // AMC size section diameter mean and bound procxies.
    AMCP::diameter_mean_ = AMC::ClassDiscretizationSize::GetDiameterMean();
    AMCP::diameter_bound_ = AMC::ClassDiscretizationSize::GetDiameterBound();

    // We need the equivalent of volume mean for AMC for further computing.
    AMCP::volume_mean_ = AMCP::diameter_mean_;
    for (int i = 0; i < AMCP::Nsection_; ++i)
      AMCP::volume_mean_[i] *= AMCP::volume_mean_[i] * AMCP::volume_mean_[i];

    // Bound volumes, essentially for checking purposes.
    AMCP::volume_bound_ = AMCP::diameter_bound_;
    for (int i = 0; i < AMCP::Nbound_; ++i)
      AMCP::volume_bound_[i] *= AMCP::volume_bound_[i] * AMCP::volume_bound_[i];

#ifdef DRIVER_MAPPING_SIZE_WITH_TABLE
    // Build table.
    AMCP::volume_mean_table_.resize(AMCP::Nsection_, AMCP::Nsection_);
    AMCP::volume_mean_table_ = real(0);
    for (int i = 0; i < AMCP::Nsection_; ++i)
      {
        AMCP::volume_mean_table_(i, i) = AMCP::volume_mean_[i];

        for (int j = i + 1; j < AMCP::Nsection_; ++j)
          AMCP::volume_mean_table_(i, j) = AMCP::volume_mean_table_(i, j - 1) * AMCP::volume_mean_[j];

        for (int j = i + 1; j < AMCP::Nsection_; ++j)
          AMCP::volume_mean_table_(i, j) = pow(AMCP::volume_mean_table_(i, j), real(1) / real(j - i + 1));
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Warning() << LogReset() << "The mean AMC volume is tabulated"
                         << " (see below). This prevents the costful pow() function, however it is not"
                         << " yet fully tested (2015-11-23)." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug() << LogReset() << "AMCP::volume_mean_table = "
                         << AMCP::volume_mean_table_ << endl;
#endif
#endif

    // At last, say we're up.
    initialized_ = true;
  }


  // Clear AMCP static data.
  void ClassMappingSize::AMCP::Clear()
  {
#ifdef DRIVER_MAPPING_SIZE_WITH_TABLE
    volume_mean_table_.free();
#endif

    volume_mean_.clear();
    diameter_mean_.clear();
    volume_bound_.clear();
    diameter_bound_.clear();

    Nbound_ = 0;
    Nsection_ = 0;
  }


  // Clear.
  void ClassMappingSize::Clear()
  {
    AMCP::Clear();

    initialized_ = false;
  }


  // Constructor.
  ClassMappingSize::ClassMappingSize(Ops::Ops *ops_ptr, const string name,
                                     vector<real> diameter_bound,
                                     vector<real> diameter_mean,
                                     bool compute)
    : Nsection_(int(diameter_mean.size())), Nbound_(int(diameter_bound.size())),
      name_(name), computed_(false)
  {
    if (! initialized_)
      throw AMC::Error("Mapping size static part seems not yet initialized, call Init() first.");

    const string prefix_orig = (ops_ptr != NULL ? ops_ptr->GetPrefix() : "");

    // Let's go to the right section, "name" is assumed to contain the "size" keyword.
    if (ops_ptr != NULL)
      ops_ptr->SetPrefix(prefix_orig + name_ + ".");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info() << LogReset()
                         << "Instantiating size mapping \"" << name_ << "\" from section \""
                         << (ops_ptr == NULL ? "NULL" : ops_ptr->GetPrefix()) << "\"." << endl;
#endif

    //
    // Mapping size discretization.
    //

    if (Nbound_ == 0 && ops_ptr != NULL)
      ops_ptr->Set(name_ + ".diameter_bound_um", "", diameter_bound);

    Nbound_ = int(diameter_bound.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << LogReset() << "Nbound = " << Nbound_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "diameter_bound = " << diameter_bound << endl;
#endif

    // Check diameter integrity.
    for (int i = 0; i < Nbound_ - 1; ++i)
      if (diameter_bound[i] >= diameter_bound[i + 1])
        throw AMC::Error("Diameter bound at index " + to_str(i) + " is >= to the following.");

    if (diameter_bound.front() < AMCP::diameter_bound_.front() ||
        diameter_bound.back() > AMCP::diameter_bound_.back())
      throw AMC::Error("Min or max diameters are out of AMC diameter bounds.");

    // Get mean diameters.
    if (Nsection_ == 0)
      if (ops_ptr != NULL)
        if (ops_ptr->Exists(name_ + ".diameter_mean_um"))
          {
            ops_ptr->Set(name_ + ".diameter_mean_um", "", diameter_mean);
            Nsection_ = int(diameter_mean.size());
          }

    if (Nsection_ == 0)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info(1) << LogReset() << "No mean diameter given,"
                             << " choose the geometric mean of diameter bounds." << endl;
#endif
        Nsection_ = Nbound_ - 1;
        diameter_mean.resize(Nsection_);
        for (int i = 0; i < Nsection_; ++i)
          diameter_mean[i] = sqrt(diameter_bound[i] * diameter_bound[i + 1]);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << LogReset() << "Nsection = " << Nsection_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "diameter_mean = " << diameter_mean << endl;
#endif

    if (Nsection_ != Nbound_ - 1)
      throw AMC::Error("Number of diameter means mismatches the number of diameter bounds.");

    // Perform a few checks.
    for (int i = 0; i < Nsection_; ++i)
      if (diameter_mean[i] < diameter_bound[i] || diameter_mean[i] >= diameter_bound[i + 1])
        throw AMC::Error("Mean diameter of section " + to_str(i) + " is out of bounds.");

    // Turn to volume equivalent, i.e. omitting constant factor pi / 6.
    volume_mean_ = diameter_mean;
    for (int i = 0; i < Nsection_; ++i)
      volume_mean_[i] *= volume_mean_[i] * volume_mean_[i];

    //
    // Compute coefficient indexes.
    //

    fraction_index_.resize(Nsection_, FLEN);
    fraction_index_ = -1;

    for (int i = 0; i < Nsection_; ++i)
      {
        Array<int, 1> fraction_index = fraction_index_(i, Range::all());

        if (compute_index_(diameter_bound[i], diameter_bound[i + 1], fraction_index) <= 0)
          throw AMC::Error("Size section number " + to_str(i) + " cannot be mapped on AMC size sections.");
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(3) << LogReset()
                         << "Mapping size coefficient indexes :" << fraction_index_ << endl;
#endif


    //
    // Compute number and mass coefficients.
    //

    fraction_number_.resize(Nsection_, AMCP::Nsection_);
    fraction_mass_.resize(Nsection_, AMCP::Nsection_);

    fraction_number_ = real(0);
    fraction_mass_ = real(0);

    if (compute = (compute || (ops_ptr != NULL ? ops_ptr->Get<bool>("compute", "", false) : false)))
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bcyan() << Info(3) << LogReset()
                             << "Compute mapping size number and mass coefficients :" << endl;
#endif
        Compute();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset()
                             << "\tfraction_number = " << fraction_number_ << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset()
                             << "\tfraction_mass = " << fraction_mass_ << endl;
#endif

        // We choose to break because we are at startup. It is then up to you to
        // decide what to do in case such unrecoverable errors occur at run time.
        if (! computed_)
          throw AMC::Error("Coefficients computation failed.");
      }

    if (ops_ptr != NULL)
      ops_ptr->SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassMappingSize::~ClassMappingSize()
  {
    Nsection_ = 0;
    Nbound_ = 0;
    fraction_index_.free();
    fraction_number_.free();
    fraction_mass_.free();

    return;
  }


  // Compute.
  void ClassMappingSize::Compute(vector<real> *volume_mean_ptr)
  {
    if (volume_mean_ptr == NULL)
      volume_mean_ptr = &volume_mean_;

    // Check if no unrecoverable error.
    bool unrecoverable_error(false);

    for (int i = 0; i < Nsection_; ++i)
      if (! Compute(i, (*volume_mean_ptr)[i]))
        break;
  }


  bool ClassMappingSize::Compute(const int i, const real &volume_mean)
  {
    // Check if no unrecoverable error.
    bool unrecoverable_error(false);

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "i = " << i << endl;
#endif

    const Array<int, 1> fraction_index = fraction_index_(i, Range::all());
    Array<real, 1> fraction_number = fraction_number_(i, Range::all());
    Array<real, 1> fraction_mass = fraction_mass_(i, Range::all());

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "volume_mean = " << volume_mean << endl;
    CBUG << "fraction_index = " << fraction_index << endl;
#endif

    // Actual computation. Return status not used here.
    const bool well_computed = compute_fraction_(volume_mean, fraction_index, fraction_number, fraction_mass);

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "well_computed = " << (well_computed ? "YES" : "NO") << endl;
    CBUG << "fraction_number = " << fraction_number << endl;
    CBUG << "fraction_mass = " << fraction_mass << endl;
#endif

    for (int j = 0; j < AMCP::Nsection_; ++j)
      if ((fraction_number(j) < real(0) || fraction_mass(j) < real(0)) && (! unrecoverable_error))
        unrecoverable_error = true;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    if (unrecoverable_error) throw AMC::Error("Negative number or/and mass coefficients.");
#endif

    if (! unrecoverable_error)
      if (abs(sum(fraction_number) - real(1)) > DRIVER_MAPPING_SIZE_EPSILON ||
          abs(sum(fraction_mass) - real(1)) > DRIVER_MAPPING_SIZE_EPSILON)
        unrecoverable_error = true;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    if (unrecoverable_error) throw AMC::Error("Sum of number or/and mass coefficients is not unity.");
#endif

    if (! unrecoverable_error)
      for (int j = fraction_index(FMIN); j < fraction_index(FMAX); ++j)
        if (fraction_number(j) > real(0) && (! unrecoverable_error))
          {
            const real volume_mean_test = volume_mean * fraction_mass(j) / fraction_number(j);

            if (volume_mean_test <AMCP:: volume_bound_[j] || volume_mean_test >= AMCP::volume_bound_[j + 1])
              unrecoverable_error = true;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
            if (unrecoverable_error) throw AMC::Error("Mapped volume mean out of bounds.");
#endif
          }

    // Return whether coefficients are usable or not.
    return (computed_ = ! unrecoverable_error);
  }


  // Get methods.
  bool ClassMappingSize::AreComputed() const
  {
    return computed_;
  }


  int ClassMappingSize::GetNsection() const
  {
    return Nsection_;
  }


  int ClassMappingSize::GetNbound() const
  {
    return Nbound_;
  }


  string ClassMappingSize::GetName() const
  {
    return name_;
  }


  void ClassMappingSize::GetFractionIndex(vector<vector<int> > &fraction_index) const
  {
    fraction_index.assign(Nsection_, vector<int>(FLEN, -1));

    for (int i = 0; i < Nsection_; ++i)
      for (int j = 0; j < FLEN; ++j)
        fraction_index[i][j] = fraction_index_(i, j);
  }


  void ClassMappingSize::GetFractionNumber(vector<vector<real> > &fraction_number) const
  {
    fraction_number.assign(Nsection_, vector<real>(AMCP::Nsection_, real(-1)));

    for (int i = 0; i < Nsection_; ++i)
      for (int j = 0; j < AMCP::Nsection_; ++j)
        fraction_number[i][j] = fraction_number_(i, j);
  }


  void ClassMappingSize::GetFractionMass(vector<vector<real> > &fraction_mass) const
  {
    fraction_mass.assign(Nsection_, vector<real>(AMCP::Nsection_, real(-1)));

    for (int i = 0; i < Nsection_; ++i)
      for (int j = 0; j < AMCP::Nsection_; ++j)
        fraction_mass[i][j] = fraction_mass_(i, j);
  }
}

#ifndef DRIVER_WITH_TEST
#undef FMIN
#undef FMAX
#undef FSIZE
#undef FLEN
#endif

#undef WMIN
#undef WMAX
#undef WCUT
#undef WSIZE
#undef WILEN

#undef WMEAN
#undef WNUM
#undef WMASS
#undef WRLEN

#define AMC_FILE_CLASS_MAPPING_SIZE_CXX
#endif
