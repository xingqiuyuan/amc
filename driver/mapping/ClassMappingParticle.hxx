// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
//
// This file is part of AMC.
//
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_PARTICLE_HXX

#define DRIVER_MAPPING_PARTICLE_CONCENTRATION_NUMBER_THRESHOLD 1. // #.m^{-3}

namespace Driver
{
  /*!
   * \class ClassMappingParticle
   */
  template<class D>
  class ClassMappingParticle : protected ClassMappingSpecies, protected ClassMappingSize,
                               protected AMC::ClassDiscretization<D>
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;
    typedef typename Driver::vector1s vector1s;

  private:

    /*!< Number of generalized sections.*/
    int Ng_;

    /*!< Size and composition index for each general section.*/
    vector2i general_section_;

    /*!< Working composition vectors.*/
    vector1r composition_in_work_, composition_out_work_;

    /*!< Whether static part is initialized.*/
    static bool is_initialized_;

    /*!< Nested class gathering AMC Proxies.*/
    class AMCP
    {
    public:

      /*!< Translation factor from mass to volume, omitting the 2pi factor.*/
      static real density_fixed_factor_;
    };

  public:

    /*!< Init static data.*/
    static void Init();

    /*!< Clear static data.*/
    static void Clear();

    /*!< Is static data initialized ?*/
    static bool IsInitialized();

    /*!< Constructor.*/
    ClassMappingParticle(Ops::Ops *ops_ptr, const string name,
                         vector<vector<int> > general_section = vector<vector<int> >(),
                         vector<vector<string> > class_composition_name = vector<vector<string> >(),
                         vector<string> species_name = vector<string>(),
                         vector<real> diameter_bound = vector<real>(),
                         vector<real> diameter_mean = vector<real>(),
                         bool compute = false);

    /*!< Destructor.*/
    ~ClassMappingParticle();

    /*!< Compute.*/
    void Compute(const real *concentration_in, real *concentration_out, const real *diameter_in = NULL);
  };
}

#define AMC_FILE_CLASS_MAPPING_PARTICLE_HXX
#endif
