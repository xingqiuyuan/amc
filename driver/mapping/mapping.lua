
mapping = {
   default = {
      particle = {
         --first_map = {general_section = 0, amc_class = "name.of.the.amc.class.composition"},
         --second_map = {general_section = 1, amc_class = "name.of.the.amc.class.composition"}
      },

      size = {
         diameter_bound_um = {}
      },

      species = {
         -- List of species associated with given format.
         list = {"H2SO4", "PPM_e", "AnBmP", "BiA1D", "BiBmP", "DUST", "SALT"},

         -- When mapping is trivial, no need to specify it.
         H2SO4 = {},
         AnBmP = {},
         BiA1D = {},
         BiBmP = {},
         DUST = {},

         -- Salt split between sodium (Na) and sulfuric acid (HCl) AMC species.
         -- Note the sum of fractions is not necessarily unity due to molar mass ratios.
         SALT = {
            Na = 0.393,
            HCl = 0.624,
         },

         -- PPM_e tracer is split between various AMC species depending on the section size.
         -- The section size indexes 0_3 (0, 1 and 2, 3 excluded) refers to the associated
         -- size discretization, not AMC one.
         PPM_e_0_3 = {
            DUST = 0.3, EC = 0.3, PTCA = 0.2, LVG = 0.2
         },

         -- PPM_e for size sections 3 up to 8 (last excluded) is given to DUST AMC species.
         PPM_e_3_8 = {
            DUST = 1.0
         }
      }
   }
}