// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SPECIES_CXX

#include "ClassMappingSpecies.hxx"


namespace Driver
{
  // Compute total mass and composition.
  bool ClassMappingSpecies::compute_total_mass_composition(const real *concentration_in,
                                                           real &concentration_in_mass_total,
                                                           vector<real> &composition_in) const
  {
    concentration_in_mass_total = real(0);
    for (int i = 0; i < Nspecies_; ++i)
      concentration_in_mass_total += concentration_in[i];

    if (concentration_in_mass_total > DRIVER_MAPPING_SPECIES_CONCENTRATION_MASS_THRESHOLD)
      {
        const real concentration_in_mass_total_inv = real(1) / concentration_in_mass_total;
        for (int i = 0; i < Nspecies_; ++i)
          composition_in[i] = concentration_in[i] * concentration_in_mass_total_inv;

        return true;
      }
    else
      composition_in.assign(Nspecies_, real(0));

    // Always return.
    return false;
  }


  // Static init.
  void ClassMappingSpecies::Init()
  {
    if ((AMCP::Nspecies_ = AMC::ClassSpecies::GetNspecies()) == 0)
      throw AMC::Error("AMC seems not initialized : no species defined.");

    if ((AMCP::Nsection_ = AMC::ClassDiscretizationSize::GetNsection()) == 0)
      throw AMC::Error("AMC seems not initialized : no size sections defined.");

    initialized_ = true;
  }


  // Clear.
  void ClassMappingSpecies::Clear()
  {
    AMCP::Nsection_ = 0;
    AMCP::Nspecies_ = 0;
    initialized_ = false;
  }


  // Constructor.
  ClassMappingSpecies::ClassMappingSpecies(Ops::Ops *ops_ptr, const string name,
                                           vector<string> species_name)
    : name_(name), Nspecies_(int(species_name.size()))
  {
    if (! initialized_)
      throw AMC::Error("Mapping species static part seems not yet initialized : call Init() first.");

    const string prefix_orig = (ops_ptr != NULL ? ops_ptr->GetPrefix() : "");

    // Let's go to the right section, "name" is assumed to contain the "species" keyword.
    if (ops_ptr != NULL)
      ops_ptr->SetPrefix(prefix_orig + name_ + ".");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Instantiate species mapping \"" << name_ << "\"." << endl;
    *AMCLogger::GetLog() << Bmagenta() << Info(1) << LogReset() << "Received species list : "
                         << (Nspecies_ > 0 ? to_str(species_name) : "NONE") << " in argument." << endl;
#endif

    // If no species yet, search in configuration file.
    if (Nspecies_ == 0)
      if (ops_ptr != NULL)
        if (ops_ptr->Exists("list"))
          {
            ops_ptr->Set("list", "", species_name);
            Nspecies_ = int(species_name.size());
          }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Read species list : "
                         << (Nspecies_ > 0 ? to_str(species_name) : "NONE") << " from section \""
                         << (ops_ptr != NULL ? ops_ptr->GetPrefix() : "NONE") << "\"." << endl;
#endif

    if (Nspecies_ == 0) throw AMC::Error("No species list given in argument or in configuration.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug() << LogReset() << "Nspecies = " << Nspecies_ << endl;
#endif

    //
    // Allocate arrays.
    //

    Nsize_.resize(AMCP::Nsection_, Nspecies_);
    index_.resize(AMCP::Nsection_, Nspecies_, AMCP::Nspecies_);
    fraction_.resize(AMCP::Nsection_, Nspecies_, AMCP::Nspecies_);

    Nsize_ = 0;
    index_ = -1;
    fraction_ = real(-1);

    //
    // Read species mapping on a per species basis.
    //

    // We will need this.
    vector1s entry_list = ops_ptr != NULL ? ops_ptr->GetEntryList(name_) : vector1s();

    for (int i = 0; i < int(entry_list.size()); ++i)
      if (entry_list[i] == "list")
        {
          entry_list[i] = entry_list.back();
          entry_list.pop_back();
          break;
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << LogReset() << "entry_list = " << entry_list << endl;
#endif

    for (int i = 0; i < Nspecies_; ++i)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bred() << Info(3) << LogReset() << "Search mapping(s) for species \"" << species_name[i] << "\" :" << endl;
#endif

        const int index_amc = AMC::ClassSpecies::GetIndex(species_name[i]);

        if (index_amc >= 0)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset()
                                 << "Found exact match in AMC species list at index " << index_amc << endl;
#endif
            Nsize_(Range::all(), i) = 1;
            index_(Range::all(), i, 0) = index_amc;
            fraction_(Range::all(), i, 0) = real(1);
          }
        else if (ops_ptr != NULL)
          {
            // The first number is min section, second is the max one.
            regex pieces_regex("^" + species_name[i] + "_?([0-9]*)_?([0-9]*)$");
            smatch pieces_match;

            for (vector1s::iterator it = entry_list.begin(); it != entry_list.end(); ++it)
              if (regex_match(*it, pieces_match, pieces_regex))
                {
#ifdef AMC_WITH_LOGGER
                  *AMCLogger::GetLog() << Bblue() << Debug(1) << LogReset() << "Found section \"" << pieces_match[0].str()
                                       << "\" matching with species \"" << species_name[i] << "\"." << endl;
#endif
                  // Min and max (excluded) AMC size sections to consider.
                  const int section_min = pieces_match[1].str().empty() ? 0 : convert<int>(pieces_match[1].str());
                  const int section_max = pieces_match[2].str().empty() ? AMCP::Nsection_ : convert<int>(pieces_match[2].str());

#ifdef AMC_WITH_LOGGER
                  *AMCLogger::GetLog() << Fblue() << Debug(2) << LogReset() << "\tsection_min = " << section_min << endl;
                  *AMCLogger::GetLog() << Fblue() << Debug(2) << LogReset() << "\tsection_max = " << section_max << endl;
#endif

                  // Proper species mapping to use between [section_min, section_max[.
                  vector1s species_map = ops_ptr->GetEntryList(*it);

#ifdef AMC_WITH_LOGGER
                  *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "\tspecies_map = " << species_map << endl;
#endif

                  for (vector1s::iterator jt = species_map.begin(); jt != species_map.end(); ++jt)
                    {
                      const int amc_index = AMC::ClassSpecies::GetIndex(*jt);
                      if (amc_index < 0)
                        throw AMC::Error("Could not find species \"" + *jt + "\" in AMC list.");

                      const real fraction = ops_ptr->Get<real>(*it + "." + *jt, "v >= 0 and v <= 1");

                      index_(Range(section_min, section_max - 1), i, std::distance(species_map.begin(), jt)) = amc_index;
                      fraction_(Range(section_min, section_max - 1), i, std::distance(species_map.begin(), jt)) = fraction;

#ifdef AMC_WITH_LOGGER
                      *AMCLogger::GetLog() << Fblue() << Debug(2) << LogReset() << "\tAMC species = \""
                                           << *jt << ", index = " << amc_index << ", fraction = " << fraction << endl;
#endif
                    }

                  Nsize_(Range(section_min, section_max - 1), i) = int(species_map.size());
                }
          }
        else
          throw AMC::Error("Input species \"" + species_name[i] + "\" is not an AMC one and has no configuration settings.");
      }

    // Performs a few checks.
    for (int i = 0; i < AMCP::Nsection_; ++i)
      for (int j = 0; j < Nspecies_; ++j)
        {
          const real fraction_sum = sum(fraction_(i, j, Range(0, Nsize_(i, j) - 1)));
          if (abs(fraction_sum - real(1)) > DRIVER_MAPPING_SPECIES_EPSILON)
            throw AMC::Error("For section " + to_str(i) + " and species \"" + species_name[j] +
                             "\", sum of fractions is not unity (" + to_str(fraction_sum) + ").");
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "Nsize = " << Nsize_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "index = " << index_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "fraction = " << fraction_ << endl;
#endif

    if (ops_ptr != NULL)
      ops_ptr->SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassMappingSpecies::~ClassMappingSpecies()
  {
    Nsize_.free();
    index_.free();
    fraction_.free();

    return;
  }


  // Get methods.
  int ClassMappingSpecies::GetNspecies() const
  {
    return Nspecies_;
  }


  // Compute.
  void ClassMappingSpecies::Compute(const int size_index, const vector<real> &concentration_in, vector<real> &concentration_out) const
  {
    const Array<int, 1> Nsize = Nsize_(size_index, Range::all());
    const Array<int, 2> index = index_(size_index, Range::all(), Range::all());
    const Array<real, 2> fraction = fraction_(size_index, Range::all(), Range::all());

    for (int i = 0; i < Nspecies_; ++i)
      for (int j = 0; j < Nsize(i); ++j)
        concentration_out[index(i, j)] += concentration_in[i] * fraction(i, j);
  }
}

#define AMC_FILE_CLASS_MAPPING_SPECIES_CXX
#endif
