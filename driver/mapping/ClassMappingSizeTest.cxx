// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_CLASS_MAPPING_SIZE_TEST_CXX

#include "ClassMappingSizeTest.hxx"


namespace Driver
{
  // Test.
  void ClassMappingSizeTest::run(const real &diameter_min, const real &diameter_max, const real &diameter_mean,
                                 Array<int, 1> &fraction_index, Array<real, 1> &fraction_number,
                                 Array<real, 1> &fraction_mass) const
  {
#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
    CBUG << "diameter_min = " << diameter_min << endl;
    CBUG << "diameter_max = " << diameter_max << endl;
    CBUG << "diameter_mean = " << diameter_mean << endl;
#endif

    // Compute fraction indexes.
    if (this->compute_index_(diameter_min, diameter_max, fraction_index) > 0)
      {
#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
        CBUG << "fraction_index = " << fraction_index << endl;
#endif

        // Volume equivalent.
        const real volume_mean = diameter_mean * diameter_mean * diameter_mean;

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
        CBUG << "volume_mean = " << volume_mean << endl;
#endif

        // Compute coefficients.
        const bool well_computed =
          this->compute_fraction_(volume_mean, fraction_index, fraction_number, fraction_mass);

#ifdef DRIVER_MAPPING_SIZE_WITH_DEBUG
        CBUG << "fraction_number = " << fraction_number << endl;
        CBUG << "fraction_mass = " << fraction_mass << endl;
#endif

        // Check coefficients consistency.
        for (int i = 0; i < AMCP::Nsection_; ++i)
          if (fraction_number(i) < real(0) || fraction_mass(i) < real(0))
            throw AMC::Error("Either number or mass fraction at index " + to_str(i) + " is < 0.");

        if (abs(sum(fraction_number) - real(1)) > DRIVER_MAPPING_SIZE_EPSILON)
          throw AMC::Error("Sum of number fraction is not unity (" + to_str(sum(fraction_number)) +
                           "). epsilon = " + to_str(DRIVER_MAPPING_SIZE_EPSILON));
        if (abs(sum(fraction_mass) - real(1)) > DRIVER_MAPPING_SIZE_EPSILON)
          throw AMC::Error("Sum of mass fraction is not unity (" + to_str(sum(fraction_mass)) +
                           "). epsilon = " + to_str(DRIVER_MAPPING_SIZE_EPSILON));

        // If coefficients were correctly computed, perform further tests.
        if (well_computed)
          for (int i = fraction_index(FMIN); i < fraction_index(FMAX); ++i)
            {
              const real volume_mean_test = volume_mean * fraction_mass(i) / fraction_number(i);

              try
                {
                  // This error is less critical than other ones, so we just keep track
                  // of them in order to check afterwards if we should really bother.
                  if (abs(volume_mean_test / AMCP::volume_mean_[i] - real(1)) > DRIVER_MAPPING_SIZE_EPSILON)
                    throw AMC::Error("For AMC section " + to_str(i) + ", mean volume after mapping (" +
                                     to_str(volume_mean_test) + ") differs from AMC mean fixed volume (" +
                                     to_str(AMCP::volume_mean_[i]) + "). epsilon = " +
                                     to_str(DRIVER_MAPPING_SIZE_EPSILON) + ", diff = " +
                                     to_str(abs(volume_mean_test / AMCP::volume_mean_[i] - real(1))));
                }
              catch(AMC::Error& e)
                {
                  error_count_++;
                  error_message_ << e.What() << endl;
                }
            }
        else
          for (int i = fraction_index(FMIN); i < fraction_index(FMAX); ++i)
            if (fraction_number(i) > real(0))
              {
                const real volume_mean_test = volume_mean * fraction_mass(i) / fraction_number(i);

                if (volume_mean_test < AMCP::volume_bound_[i] || volume_mean_test >= AMCP::volume_bound_[i + 1])
                  throw AMC::Error("For AMC section " + to_str(i) + ", mean volume after mapping (" +
                                   to_str(volume_mean_test) + ") is out of AMC bound volumes ([" +
                                   to_str(AMCP::volume_bound_[i]) + ", " +
                                   to_str(AMCP::volume_bound_[i + 1]) + "]).");
              }
      }
    else
      throw AMC::Error("Section [" + to_str(diameter_min) + ", " + to_str(diameter_max) +
                       "[ cannot be mapped on AMC size discretization : [" +
                       to_str(AMCP::diameter_bound_.front()) + ", " + to_str(AMCP::diameter_bound_.back()) +
                       "[, Nsection = " + to_str(AMCP::Nsection_) + " .");
  }


  // Constructor.
  ClassMappingSizeTest::ClassMappingSizeTest() : ClassMappingSize(NULL, "test")
  {
    return;
  }


  ClassMappingSizeTest::ClassMappingSizeTest(const string name,
                                             const vector<real> &diameter_bound,
                                             const vector<real> &diameter_mean)
    : ClassMappingSize(NULL, "test_" + name, diameter_bound, diameter_mean, true),
      diameter_bound_(diameter_bound), diameter_mean_(diameter_mean)
  {
    return;
  }


  // Get methods.
  int ClassMappingSizeTest::GetErrorCount()
  {
    return error_count_;
  }


  string ClassMappingSizeTest::GetErrorMessage()
  {
    return error_message_.str();
  }


  // Run.
  void ClassMappingSizeTest::Run(ClassMappingSizeTest *mapping_size_ptr, int N, string seed)
  {
    if (N == 0) return;

    error_count_ = 0;
    error_message_.str("");

    const bool external_mapping_size = (mapping_size_ptr != NULL);

    if (! external_mapping_size)
      mapping_size_ptr = new ClassMappingSizeTest();
    else
      if (mapping_size_ptr->Nsection_ <= 0)
        throw AMC::Error("Mapping size \"" + mapping_size_ptr->name_ + "\" given in argument" +
                         " seems not initialized (Nsection <= 0).");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << "Test mapping size \"" << mapping_size_ptr->name_
                         << "\" with N = " << N << " and seed = \"" << seed << "\"" << LogReset().Str() <<  endl;
#endif

    // Random number generator and its seed.
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(2) << LogReset() << "Using seed \""
                         << seed << "\" for random generator." <<  endl;
#endif

    std::seed_seq seed_sequence (seed.begin(), seed.end());

    // Standard, 64 bit based, Mersenne Twister random engine.
    std::mt19937_64 generator (seed_sequence);

    // Random number between [0, 1].
    std::uniform_real_distribution<real> random_unity(real(0), real(1));

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "random_unity(generator) = "
                         << random_unity(generator) <<  endl;
#endif

    Array<int, 1> fraction_index(FLEN);
    Array<real, 1> fraction_number(AMCP::Nsection_);
    Array<real, 1> fraction_mass(AMCP::Nsection_);

    fraction_index= 0;
    fraction_number = real(0);
    fraction_mass = real(0);

#ifdef AMC_WITH_TIMER
    std::replace(seed.begin(), seed.end(), ' ', '_');
    const int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_mapping_size_" + mapping_size_ptr->name_
                                                        + "_" + to_str(N) + "_" + seed);
#endif

    if (external_mapping_size)
      {
        // Random integer between [0, Nsection[.
        std::uniform_int_distribution<> random_section(0, mapping_size_ptr->Nsection_ - 1);

        for (int i = 0; i < N; ++i)
          {
            // Choose random section ...
            const int section_index = random_section(generator);
            const real diameter_min = mapping_size_ptr->diameter_bound_[section_index];
            const real diameter_max = mapping_size_ptr->diameter_bound_[section_index + 1];

            // ... and within this section one random mean diameter.
            const real diameter_mean = diameter_min * pow(diameter_max / diameter_min, random_unity(generator));

#ifdef AMC_WITH_TIMER
            AMC::AMCTimer<AMC::CPU>::Start();
#endif

            mapping_size_ptr->run(diameter_min, diameter_max, diameter_mean,
                                  fraction_index, fraction_number, fraction_mass);

#ifdef AMC_WITH_TIMER
            AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#endif
          }
      }
    else
      {
        const real &diameter_bound_min = AMCP::diameter_bound_.front();
        const real &diameter_bound_max = AMCP::diameter_bound_.back();

        for (int i = 0; i < N; ++i)
          {
            // Choose random section on a logarithmic scale.
            const real diameter_min = diameter_bound_min
              * pow(diameter_bound_max / diameter_bound_min, random_unity(generator));
            const real diameter_max = diameter_min
              * pow(diameter_bound_max / diameter_min, random_unity(generator));
            const real diameter_mean = diameter_min * pow(diameter_max / diameter_min, random_unity(generator));

#ifdef AMC_WITH_TIMER
            AMC::AMCTimer<AMC::CPU>::Start();
#endif

            mapping_size_ptr->run(diameter_min, diameter_max, diameter_mean,
                                  fraction_index, fraction_number, fraction_mass);

#ifdef AMC_WITH_TIMER
            AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#endif
          }
      }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset() << "Performed " << N << " mappings in "
                         << setprecision(12) << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds ("
                         << real(AMC::AMCTimer<AMC::CPU>::GetTimer(time_index)) / real(N) << " sec/test)" << endl;
#endif
#endif

#ifdef AMC_WITH_LOGGER
    if (error_count_ > 0)
      *AMCLogger::GetLog() << Bred() << Warning(1) << LogReset() << "There were " << error_count_ << " errors"
                           << " on the average mean volume after mapping (GetErrorMessage() to see them)." << endl;
#endif

    // If it was internal.
    if (! external_mapping_size)
      delete mapping_size_ptr;
  }


  void ClassMappingSizeTest::RunSelf(const real diameter_min, const real diameter_max, const real diameter_mean,
                                     vector<int> &fraction_index, vector<real> &fraction_number,
                                     vector<real> &fraction_mass) const
  {
    if (diameter_mean < diameter_min || diameter_mean >= diameter_max)
      throw AMC::Error("Provided section diameter mean (" + to_str(diameter_mean) +
                       ") is out of section diameter bounds ([" + to_str(diameter_min) +
                       ", " + to_str(diameter_max) + "[).");

    error_count_ = 0;
    error_message_.str("");

    // Working arrays.
    fraction_index.assign(FLEN, 0);
    fraction_number.assign(AMCP::Nsection_, real(0));
    fraction_mass.assign(AMCP::Nsection_, real(0));

    Array<int, 1> fraction_index_arr(fraction_index.data(), shape(FLEN), neverDeleteData);
    Array<real, 1> fraction_number_arr(fraction_number.data(), shape(AMCP::Nsection_), neverDeleteData);
    Array<real, 1> fraction_mass_arr(fraction_mass.data(), shape(AMCP::Nsection_), neverDeleteData);

    run(diameter_min, diameter_max, diameter_mean, fraction_index_arr, fraction_number_arr, fraction_mass_arr);
  }
}

#undef FMIN
#undef FMAX
#undef FSIZE
#undef FLEN

#define AMC_FILE_CLASS_MAPPING_SIZE_TEST_CXX
#endif
