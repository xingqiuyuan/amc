// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_DRIVER_CPP

#include "Driver.hxx"

#ifdef DRIVER_WITH_CHIMERE
#include "chimere/ChimereTemplate.hxx"
#endif

#ifdef AMC_WITH_LOGGER
#include "logger/Logger.cpp"
#endif

namespace AMC
{
#ifdef AMC_WITH_LOGGER
  class AMCLogger;
#endif

#ifdef AMC_WITH_TIMER
  template class AMCTimer<CPU>;
  template class AMCTimer<Wall>;
#endif

#ifdef AMC_WITH_STATISTICS
  class AMCStatistics;
#endif

#ifdef AMC_WITH_MODAL
  // Independent modal distribution class.
  template class ClassModalDistribution<Driver::real>;
#endif

  // Size discretization.
  class ClassDiscretizationSize;

  // Composition discretization.
  class ClassDiscretizationCompositionBase;
  template class ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;
  template class ClassDiscretizationClass<disc_comp_base>;

  // Aerosol Data.
  class ClassAerosolData;

  // AMC.
  template class ClassAMC<disc_comp_base>;

  // AMX.
  template class ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
}

namespace Driver
{
#ifdef DRIVER_WITH_NUMERICAL_SOLVER
  class ClassNumericalSolverFluxLimiterRoesSuperbee;
  template class ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>;
  class ClassNumericalSolverDiffusionLinear;
  class ClassNumericalSolverDiffusionRadial;
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
  class ClassParameterizationPhysics;
#endif

#ifdef DRIVER_WITH_GEO_DATA
  template class ClassGeoDataBase<float>;
  template class ClassGeoDataElevation<float>;
#endif

  // Monahan's sea salt emissions.
#ifdef DRIVER_WITH_MONAHAN
  class ClassParameterizationEmissionSeaSaltMonahan;
#endif

  // Emission.
#ifdef DRIVER_WITH_EMISSION
  class ClassTracer;
  class ClassSource;
  template class ClassDynamicsEmission<AMC::disc_comp_base>;
  template class ClassDynamicsEmission<AMC::disc_comp_table>;
#endif

  class ClassDriverBase;
  class ClassDriverDomainBase;

  // Concentration mapping.
#ifdef DRIVER_WITH_MAPPING
  class ClassMappingSize;
#ifdef DRIVER_WITH_TEST
  class ClassMappingSizeTest;
#endif
  class ClassMappingSpecies;
  template class ClassMappingParticle<AMC::disc_comp_base>;
  template class ClassMappingParticle<AMC::disc_comp_table>;
#endif

#ifdef DRIVER_WITH_TRAJECTORY
  class ClassDriverTrajectoryBase;

  template void ClassDriverTrajectoryBase::Run<AMC::redist_size_mixed, AMC::redist_comp_base_base,
                                               AMC::disc_comp_base, AMC::coag_coef_moving_base>
  (vector<vector<real> > &concentration_aer_num,
   vector<vector<real> > &concentration_aer_mass,
   vector<vector<real> > &concentration_gas) const;

#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
  class ClassDriverTrajectoryChimere;
#endif

#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
  class ClassDriverTrajectoryPolair3d;
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
  class ClassChimereBase;
#ifdef CHIMERE_WITH_AMC
  template class ClassDriverChimere<redist_size, redist_comp, disc_comp, coag_coef_repart>;
#else
  template class ClassDriverChimere<int, int, int, int>;
#endif
#ifdef DRIVER_WITH_EMISSION
  template class ClassChimereEmissionAnthropic<AMC::disc_comp_base>;
  template class ClassChimereEmissionAnthropic<AMC::disc_comp_table>;
  template class ClassChimereEmissionBiogenic<AMC::disc_comp_base>;
  template class ClassChimereEmissionBiogenic<AMC::disc_comp_table>;
#endif
  template class ClassChimereBoundaryCondition<AMC::disc_comp_base>;
  template class ClassChimereBoundaryCondition<AMC::disc_comp_table>;
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
  class ClassChamproboisBase;
  class ClassChamproboisGranulometry;
  class ClassChamproboisSpeciation;
  class ClassChamproboisSink;
  class ClassChamproboisFilter;
  class ClassChamproboisELPI;
  class ClassChamproboisSource;
  class ClassDriverChamprobois;

  template void ClassDriverChamprobois::Run<AMC::redist_size_mixed, AMC::redist_comp_base_base,
                                            AMC::disc_comp_base, AMC::coag_coef_moving_base>(real time_out, const bool passive);

  template void ClassDriverChamprobois::Forward<AMC::redist_size_mixed, AMC::redist_comp_base_base,
                                                AMC::disc_comp_base, AMC::coag_coef_moving_base>();
#endif
}

#define AMC_FILE_DRIVER_CPP
#endif
