// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_DRIVER_HXX

#include "DriverHeader.hxx"

// Always included, except if Chimere in standalone
// mode with original aerosol model (Bessagnet's).
#include "AMC.hxx"

#ifdef DRIVER_WITH_NUMERICAL_SOLVER
#include "numerics/ClassNumericalSolverAdvectionLaxWendroff.cxx"
#include "numerics/ClassNumericalSolverFluxLimiterRoesSuperbee.cxx"
#include "numerics/ClassNumericalSolverDiffusionLinear.cxx"
#include "numerics/ClassNumericalSolverDiffusionRadial.cxx"
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
#include "driver/parameterization/ClassParameterizationPhysics.cxx"
#endif

#ifdef DRIVER_WITH_GEO_DATA
#include "geo_data/ClassGeoDataBase.cxx"
#include "geo_data/ClassGeoDataElevation.cxx"
#endif

#ifdef DRIVER_WITH_MONAHAN
#include "parameterization/ClassParameterizationEmissionSeaSaltMonahan.cxx"
#endif

#ifdef DRIVER_WITH_EMISSION
#include "emission/ClassTracer.cxx"
#include "emission/ClassSource.cxx"
#include "emission/ClassDynamicsEmission.cxx"
#endif

#include "base/ClassDriverBase.cxx"
#include "base/ClassDriverDomainBase.cxx"

#ifdef DRIVER_WITH_MAPPING
#include "mapping/ClassMappingSize.cxx"
#ifdef DRIVER_WITH_TEST
#include "mapping/ClassMappingSizeTest.cxx"
#endif
#include "mapping/ClassMappingSpecies.cxx"
#include "mapping/ClassMappingParticle.cxx"
#endif

#ifdef DRIVER_WITH_TRAJECTORY
#include "trajectory/ClassDriverTrajectoryBase.cxx"
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
#include "trajectory/ClassDriverTrajectoryChimere.cxx"
#endif
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
#include "trajectory/ClassDriverTrajectoryPolair3d.cxx"
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
#ifdef DRIVER_WITH_EMISSION
#include "chimere/ClassChimereEmissionAnthropic.cxx"
#include "chimere/ClassChimereEmissionBiogenic.cxx"
#endif
#include "chimere/ClassChimereBoundaryCondition.cxx"
#include "chimere/ClassChimereBase.cxx"
#include "chimere/ClassDriverChimere.cxx"
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
#include "champrobois/ClassChamproboisBase.cxx"
#include "champrobois/ClassChamproboisGranulometry.cxx"
#include "champrobois/ClassChamproboisSpeciation.cxx"
#include "champrobois/ClassChamproboisSink.cxx"
#include "champrobois/ClassChamproboisFilter.cxx"
#include "champrobois/ClassChamproboisELPI.cxx"
#include "champrobois/ClassChamproboisSource.cxx"
#include "champrobois/ClassDriverChamprobois.cxx"
#endif

namespace Driver
{
  // Numerical solvers.
#ifdef DRIVER_WITH_NUMERICAL_SOLVER
  int ClassNumericalSolverDiffusionLinear::n_ = 0;
  real ClassNumericalSolverDiffusionLinear::s_ = real(0);
  real ClassNumericalSolverDiffusionLinear::dx_ = real(0);
  real ClassNumericalSolverDiffusionLinear::dx2_ = real(0);
  Array<real, 1> ClassNumericalSolverDiffusionLinear::D2_;

  int ClassNumericalSolverDiffusionRadial::n_ = 0;
  real ClassNumericalSolverDiffusionRadial::s_ = real(0);
  real ClassNumericalSolverDiffusionRadial::dr_ = real(0);
  real ClassNumericalSolverDiffusionRadial::dr2_ = real(0);
  Array<real, 1> ClassNumericalSolverDiffusionRadial::D2_;
  Array<real, 1> ClassNumericalSolverDiffusionRadial::rb_;
  Array<real, 1> ClassNumericalSolverDiffusionRadial::rc_inv_;

  template<class FL> int ClassNumericalSolverAdvectionLaxWendroff<FL>::n_ = 0;
  template<class FL> real ClassNumericalSolverAdvectionLaxWendroff<FL>::s_ = real(0);
  template<class FL> real ClassNumericalSolverAdvectionLaxWendroff<FL>::dx_ = real(0);
  template<class FL> Array<real, 1> ClassNumericalSolverAdvectionLaxWendroff<FL>::f_;
  template<class FL> Array<real, 1> ClassNumericalSolverAdvectionLaxWendroff<FL>::nu_;
  template<class FL> Array<real, 1> ClassNumericalSolverAdvectionLaxWendroff<FL>::gamma_;
#endif

  // Geo data.
#ifdef DRIVER_WITH_GEO_DATA
  template<class T> bool ClassGeoDataBase<T>::is_initiated_ = false;
  template<class T> string ClassGeoDataBase<T>::configuration_file_ = "";
  template<class T> string ClassGeoDataBase<T>::data_directory_ = "";
  template<class T> vector1s ClassGeoDataBase<T>::name_list_;
#endif

#ifdef DRIVER_WITH_EMISSION
  int ClassTracer::Ntracer_ = 0;
  int ClassTracer::Nanthropic_ = 0;
  int ClassTracer::Nbiogenic_ = 0;
  vector1s ClassTracer::name_;
  vector1b ClassTracer::is_anthropic_;
  vector1i ClassTracer::Nspecies_;
  vector1r ClassTracer::molar_mass_;
  vector1r ClassTracer::density_;
  vector2i ClassTracer::species_index_;
  vector2r ClassTracer::species_mass_fraction_;
  int ClassSource::Nsource_ = 0;
  int ClassSource::Nanthropic_= 0;
  int ClassSource::Nbiogenic_ = 0;
  real ClassSource::threshold_percentage_size_ = DRIVER_SOURCE_THRESHOLD_PERCENTAGE_SIZE_DEFAULT;
  vector1i ClassSource::Nsize_;
  vector2i ClassSource::size_index_;
  vector2r ClassSource::size_diameter_mean_;
  vector2r ClassSource::size_fraction_number_;
  vector2r ClassSource::size_fraction_mass_;
  vector2r ClassSource::size_volume_mean_inv_;
  vector1s ClassSource::name_;
  vector1b ClassSource::is_anthropic_;
  vector1i ClassSource::Ntracer_;
  vector2i ClassSource::tracer_index_;
  vector1r ClassSource::density_fixed_;
  vector1b ClassSource::use_density_fixed_;
  vector2r ClassSource::size_mass_mean_inv_;
#endif

  // Driver base.
  int ClassDriverBase::rank_ = 0;
  int ClassDriverBase::Nrank_ = 1;
  bool ClassDriverBase::is_initiated_ = false;
  string ClassDriverBase::configuration_file_ = "";
  string ClassDriverBase::prefix_ = "";
  string ClassDriverBase::type_ = "";

  // Driver domain base.
  int ClassDriverDomainBase::Nz_ = 0;
  int ClassDriverDomainBase::Ny_ = 0;
  int ClassDriverDomainBase::Nx_ = 0;
  vector1r ClassDriverDomainBase::x_;
  vector1r ClassDriverDomainBase::y_;
  real ClassDriverDomainBase::x_delta_ = real(0);
  real ClassDriverDomainBase::x_min_ = real(0);
  real ClassDriverDomainBase::x_max_ = real(0);
  real ClassDriverDomainBase::y_delta_ = real(0);
  real ClassDriverDomainBase::y_min_ = real(0);
  real ClassDriverDomainBase::y_max_ = real(0);
  bool ClassDriverDomainBase::domain_regular_ = true;
  vector1r ClassDriverDomainBase::z_top_;
  vector1r ClassDriverDomainBase::z_mid_;

#ifdef DRIVER_WITH_MAPPING
  bool ClassMappingSize::initialized_ = false;
  int ClassMappingSize::AMCP::Nsection_ = 0;
  int ClassMappingSize::AMCP::Nbound_ = 0;
  vector1r ClassMappingSize::AMCP::diameter_bound_;
  vector1r ClassMappingSize::AMCP::volume_bound_;
  vector1r ClassMappingSize::AMCP::diameter_mean_;
  vector1r ClassMappingSize::AMCP::volume_mean_;
#ifdef DRIVER_MAPPING_SIZE_WITH_TABLE
  Array<real, 2> ClassMappingSize::AMCP::volume_mean_table_;
#endif
#ifdef DRIVER_WITH_TEST
  int ClassMappingSizeTest::error_count_ = 0;
  ostringstream ClassMappingSizeTest::error_message_;
#endif
  bool ClassMappingSpecies::initialized_ = false;
  int ClassMappingSpecies::AMCP::Nspecies_ = 0;
  int ClassMappingSpecies::AMCP::Nsection_ = 0;
  template<class D> bool ClassMappingParticle<D>::is_initialized_ = false;
  template<class D> real ClassMappingParticle<D>::AMCP::density_fixed_factor_ = real(0);
#endif

#ifdef DRIVER_WITH_TRAJECTORY
  // Trajectory base.
  int ClassDriverTrajectoryBase::id_counter_ = 0;
  int ClassDriverTrajectoryBase::Nvariable_ = 0;
  vector1s ClassDriverTrajectoryBase::variable_name_;
  map<string, int> ClassDriverTrajectoryBase::variable_z_level_;
  map<string, string> ClassDriverTrajectoryBase::data_file_;
  map<string, real> ClassDriverTrajectoryBase::variable_min_;
  map<string, real> ClassDriverTrajectoryBase::variable_max_;
  real ClassDriverTrajectoryBase::data_time_step_ = real(0);
  string ClassDriverTrajectoryBase::data_date_begin_ = "";
  Array<real, 2> ClassDriverTrajectoryBase::horizontal_surface_;

#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
  // Trajectory Chimere.
  int ClassDriverTrajectoryChimere::Nemission_anthropic_ = 0;
  int ClassDriverTrajectoryChimere::Nemission_biogenic_ = 0;
  int ClassDriverTrajectoryChimere::Nemission_salt_ = 0;
  vector1s ClassDriverTrajectoryChimere::emission_anthropic_name_;
  vector1s ClassDriverTrajectoryChimere::emission_biogenic_name_;
  vector1i ClassDriverTrajectoryChimere::emission_biogenic_index_;
  vector1s ClassDriverTrajectoryChimere::emission_salt_name_;
  map<string, real> ClassDriverTrajectoryChimere::emission_convert_factor_;

  int ClassDriverTrajectoryChimere::Nmeteo_2d_ = 0;
  int ClassDriverTrajectoryChimere::Nmeteo_3d_ = 0;
  vector1s ClassDriverTrajectoryChimere::meteo_name_2d_;
  vector1s ClassDriverTrajectoryChimere::meteo_name_3d_;
  Array<real, 3> ClassDriverTrajectoryChimere::vertical_layer_top_;
  Array<real, 3> ClassDriverTrajectoryChimere::vertical_layer_mid_;
#endif
#endif

#ifdef DRIVER_WITH_EMISSION
  template<class D> bool ClassDynamicsEmission<D>::source_count_general_section_ = false;
  template<class D> vector2i ClassDynamicsEmission<D>::source_general_section_;
  template<class D> vector1r ClassDynamicsEmission<D>::composition_work_;
  template<class D> Array<int, 3> ClassDynamicsEmission<D>::source_general_section_count_;
#endif

#ifdef DRIVER_WITH_CHIMERE
#ifdef DRIVER_WITH_EMISSION
  template<class D> vector1s ClassChimereEmissionAnthropic<D>::tracer_name_;
  template<class D> vector1i ClassChimereEmissionAnthropic<D>::tracer_index_;
  template<class D> Array<double, 5> ClassChimereEmissionAnthropic<D>::emission_particle_;
  template<class D> Array<float, 4> ClassChimereEmissionAnthropic<D>::emission_;
  template<class D> vector1r ClassChimereEmissionAnthropic<D>::emission_rate_mass_work_;
  template<class D> vector1r ClassChimereEmissionAnthropic<D>::rate_aer_number_work_;
  template<class D> vector1r ClassChimereEmissionAnthropic<D>::rate_aer_mass_work_;

  template<class D> vector1s ClassChimereEmissionBiogenic<D>::tracer_name_;
  template<class D> vector1i ClassChimereEmissionBiogenic<D>::tracer_index_;
  template<class D> Array<double, 4> ClassChimereEmissionBiogenic<D>::emission_particle_;
  template<class D> Array<double, 3> ClassChimereEmissionBiogenic<D>::emission_;
  template<class D> vector1r ClassChimereEmissionBiogenic<D>::emission_rate_mass_work_;
  template<class D> vector1r ClassChimereEmissionBiogenic<D>::rate_aer_number_work_;
  template<class D> vector1r ClassChimereEmissionBiogenic<D>::rate_aer_mass_work_;
#endif
  // Boundary conditions.
  template<class D> bool ClassChimereBoundaryCondition<D>::initiated_ = false;
  template<class D> bool ClassChimereBoundaryCondition<D>::allocated_ = false;
#ifdef DRIVER_WITH_MAPPING
  template<class D> ClassMappingParticle<D> *ClassChimereBoundaryCondition<D>::mapping_particle_ptr_ = NULL;
#endif
  template<class D> int ClassChimereBoundaryCondition<D>::Nspecboun_ = 0;
  template<class D> int ClassChimereBoundaryCondition<D>::Nspecboun_gas_ = 0;
  template<class D> int ClassChimereBoundaryCondition<D>::Nspecboun_aer_ = 0;
  template<class D> vector1s ClassChimereBoundaryCondition<D>::species_;
  template<class D> vector1i ClassChimereBoundaryCondition<D>::species_index_;
  template<class D> real *ClassChimereBoundaryCondition<D>::diameter_in_ = NULL;
  template<class D> Array<real, 1> ClassChimereBoundaryCondition<D>::concentration_in_;
  template<class D> Array<real, 1> ClassChimereBoundaryCondition<D>::concentration_out_;
  template<class D> Range ClassChimereBoundaryCondition<D>::range_in_;
  template<class D> Range ClassChimereBoundaryCondition<D>::range_out_;
  template<class D> Array<float, 3> ClassChimereBoundaryCondition<D>::lateral_;
  template<class D> Array<float, 4> ClassChimereBoundaryCondition<D>::top_;

  // Base Chimere class.
  vector1r ClassChimereBase::diameter_bound_;
  vector1s ClassChimereBase::species_name_;
  map<string, int*> ClassChimereBase::parameter_;
  map<string, char*> ClassChimereBase::file_name_;
  Array<double, 4> ClassChimereBase::concentration_chimere_;
  Array<double, 4> ClassChimereBase::concentration_worker_;
  bool ClassChimereBase::is_running_ = false;
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
  vector1s ClassChamproboisBase::source_list_;
  vector1s ClassChamproboisBase::speciation_list_;
  string ClassChamproboisBase::raw_data_file_ = "./raw_data_champrobois.bin";
  real ClassChamproboisBase::chimney_length_ = CHAMPROBOIS_BASE_CHIMNEY_LENGTH_DEFAULT;
  real ClassChamproboisBase::chimney_radius_ = CHAMPROBOIS_BASE_CHIMNEY_RADIUS_DEFAULT;
  real ClassChamproboisBase::fire_place_radius_ = CHAMPROBOIS_BASE_FIRE_PLACE_RADIUS_DEFAULT;
  string ClassDriverChamprobois::prefix_ = "simulation";
#endif
}

#define AMC_FILE_DRIVER_HXX
#endif
