// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_DOMAIN_BASE_HXX

#define DRIVER_BASE_REGULAR_DOMAIN_THRESHOLD 1.e-8

namespace Driver
{
  /*! 
   * \class ClassDriverDomainBase
   */
  class ClassDriverDomainBase : public ClassDriverBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  protected:

    /*!< Domain size.*/
    static int Nz_, Ny_, Nx_;

    /*!< Is domain regular ?.*/
    static bool domain_regular_;

    /*!< X and Y of domain.*/
    static vector1r x_;
    static vector1r y_;

    /*!< Bounds of domain.*/
    static real x_delta_;
    static real x_min_;
    static real x_max_;
    static real y_delta_;
    static real y_min_;
    static real y_max_;

    /*!< Global vertical layers, may not be used if derived class has its own.*/
    static vector1r z_top_;
    static vector1r z_mid_;

    /*!< Find horizontal location in domain.*/
    static bool find_horizontal_location_(const real &y, const real &x,
                                          int &j0, int &j1, int &i0, int &i1);

    /*!< Compute bilinear horizontal weights.*/
    static void compute_bilinear_horizontal_weight_(const real &y, const real &x,
                                                    const int j0, const int j1,
                                                    const int i0, const int i1,
                                                    real &w00, real &w01, real &w10, real &w11);

    /*!< Find vertical location in domain.*/
    static bool find_vertical_location_(const real &z, int &k0, int &k1);

    /*!< Compute bilinear vertical weights.*/
    static void compute_vertical_weight_(const real &z, const int k0, const int k1, real &w0, real &w1);

  public:

    /*!< Constructor.*/
    ClassDriverDomainBase();

    /*!< Destructor.*/
    virtual ~ClassDriverDomainBase();

#ifndef SWIG
    /*!< Init driver base.*/
    static void PreInit();
    static void PostInit();
#endif

    /*!< Get methods.*/
    static int GetNz();
    static int GetNy();
    static int GetNx();
    static bool IsDomainRegular();
    static void GetX(vector<real> &x);
    static void GetY(vector<real> &y);
    static real GetXmin();
    static real GetYmin();
    static real GetXmax();
    static real GetYmax();
    static real GetDeltaX();
    static real GetDeltaY();
    static void GetTopZ(vector<real> &z_top);
    static void GetMidZ(vector<real> &z_mid);

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_DRIVER_DOMAIN_BASE_HXX
#endif
