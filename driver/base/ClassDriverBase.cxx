// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_BASE_CXX

#include "ClassDriverBase.hxx"

namespace Driver
{
  // Constructor.
  ClassDriverBase::ClassDriverBase()
  {
    return;
  }


  // Destructor.
  ClassDriverBase::~ClassDriverBase()
  {
    return;
  }


#ifdef DRIVER_WITH_MPI
  // MPI initialization.
  void ClassDriverBase::MPI_Init()
  {
    if (! MPI::Is_initialized())
      MPI::Init();

    rank_ = MPI::COMM_WORLD.Get_rank();
    Nrank_ = MPI::COMM_WORLD.Get_size();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << LogReset() << "Init MPI subsystem." << endl;
#endif
  }
#endif


  // Init driver base.
  void ClassDriverBase::Init(const string &configuration_file, const string &prefix, const string &type)
  {
    // Configuration file and prefix.
    configuration_file_ = configuration_file;
    prefix_ = prefix;
    type_ = type;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                         << "Init driver of type \"" << type_ << "\" from configuration file \""
                         << configuration_file_ << "\" and prefix \"" << prefix_ << "\"." << endl;
#endif
  }


  // Get methods.
  bool ClassDriverBase::IsInitiated()
  {
    return is_initiated_;
  }

  
  string ClassDriverBase::GetConfigurationFile()
  {
    return configuration_file_;
  }

  
  string ClassDriverBase::GetPrefix()
  {
    return prefix_;
  }

  
  string ClassDriverBase::GetType()
  {
    return type_;
  }


  int ClassDriverBase::GetRank()
  {
    return rank_;
  }


  int ClassDriverBase::GetNrank()
  {
    return Nrank_;
  }


  // Clear static data.
  void ClassDriverBase::Clear()
  {
    configuration_file_ = "";
    prefix_ = "";
    type_ = "";

    // No more initiated.
    is_initiated_ = false;
  }
}

#define AMC_FILE_CLASS_DRIVER_BASE_CXX
#endif
