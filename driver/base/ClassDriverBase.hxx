// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_BASE_HXX


namespace Driver
{
  /*! 
   * \class ClassDriverBase
   */
  class ClassDriverBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  protected:

    /*!< MPI variables.*/
    static int rank_;
    static int Nrank_;

    /*!< Is static data initiated.*/
    static bool is_initiated_;

    /*!< Driver configuration file.*/
    static string configuration_file_;

    /*!< Driver prefix.*/
    static string prefix_;

    /*!< Type of driver.*/
    static string type_;

  public:

    /*!< Constructor.*/
    ClassDriverBase();

    /*!< Destructor.*/
    virtual ~ClassDriverBase();

#ifdef DRIVER_WITH_MPI
    /*!< MPI initialization.*/
    static void MPI_Init();
#endif

#ifndef SWIG
    /*!< Init driver base.*/
    static void Init(const string &configuration_file, const string &prefix, const string &type);
#endif

    /*!< Get methods.*/
    static bool IsInitiated();
    static string GetConfigurationFile();
    static string GetPrefix();
    static string GetType();
    static int GetRank();
    static int GetNrank();

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_DRIVER_BASE_HXX
#endif
