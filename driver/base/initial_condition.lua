
initial_concentration = {
   chimere_sa = {
      gas = {
         SO2 = {H2SO4 = 1.53125},
         HNO3 = {},
         NH3 = {},
         AnBmP = {},
         BiA1D = {},
         BiBmP = {}
      },

     aer = {
         BCAR_S2_nw = {EC = 1.},
         BCAR_S2_w = {EC = 1.},
         BCAR_S7 = {EC = 1.},
         DUST = {},
         OCAR_S2_nw = {C25 = 1.},
         OCAR_S2_w = {LVG = 1.},
         OCAR_S7 = {C25 = 1.},
         PPM_e = {DUST = 0.3, EC = 0.3, PTCA = 0.2, LVG = 0.2},
         PPM_o = {DUST = 0.3, EC = 0.3, PTCA = 0.2, LVG = 0.2},
         SALT = {Na = 0.3865546218487395,
                 HCl = 0.6134453781512605},
         AnA1D = {AnBlP = 1.},
         AnBmP = {},
         BiA1D = {},
         BiBmP = {},
         H2SO4 = {},
         HNO3 = {},
         NH3 = {}
     },

     -- Fixed aerosol density in g.cm^{-3}.
     -- density_fixe_aer = 1.2
   }
}