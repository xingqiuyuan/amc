// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_DOMAIN_BASE_CXX

#include "ClassDriverDomainBase.hxx"

namespace Driver
{
  // Find horizontal location in domain.
  inline bool ClassDriverDomainBase::find_horizontal_location_(const real &y, const real &x,
                                                         int &j0, int &j1, int &i0, int &i1)
  {
    if (y < y_min_ || y >= y_max_ || x < x_min_ || x >= x_max_)
      return false;

    if (domain_regular_)
      {
        j0 = abs(int((y - y_[0]) / y_delta_ + real(1.e-6)));
        i0 = abs(int((x - x_[0]) / x_delta_ + real(1.e-6)));
      }
    else
      {
        j0 = AMC::search_index(y_, y);
        i0 = AMC::search_index(x_, x);
      }

    j1 = (y >= y_[0] && y < y_.back()) ? j0 + 1 : j0;
    i1 = (x >= x_[0] && x < x_.back()) ? i0 + 1 : i0;

    return true;
  }


  // Compute bilinear horizontal weights.
  inline void ClassDriverDomainBase::compute_bilinear_horizontal_weight_(const real &y, const real &x,
                                                                   const int j0, const int j1,
                                                                   const int i0, const int i1,
                                                                   real &w00, real &w01, real &w10, real &w11)
  {
    real y_pos = j1 > j0 ? (y - y_[j0]) / (y_[j1] - y_[j0]) : real(0);
    real x_pos = i1 > i0 ? (x - x_[i0]) / (x_[i1] - x_[i0]) : real(0);

    w00 = (real(1) - y_pos) * (real(1) - x_pos);
    w01 = y_pos * (real(1) - x_pos);
    w10 = (real(1) - y_pos) * x_pos;
    w11 = y_pos * x_pos;
  }


  // Find vertical location in domain.
  inline bool ClassDriverDomainBase::find_vertical_location_(const real &z, int &k0, int &k1)
  {
    if (z < real(0) || z >= z_top_.back())
      return false;

    k0 = AMC::search_index(z_mid_, z);
    k1 = (z >= z_mid_[0] && z < z_mid_.back()) ? k0 + 1 : k0;

    return true;    
  }


  // Compute bilinear vertical weights.
  inline void ClassDriverDomainBase::compute_vertical_weight_(const real &z,
                                                        const int k0, const int k1,
                                                        real &w0, real &w1)
  {
    w1 = (k1 > k0) ? (z - z_mid_[k0]) / (z_mid_[k1] - z_mid_[k0]) : real(0);
    w0 = real(1) - w1;
  }


  // Constructor.
  ClassDriverDomainBase::ClassDriverDomainBase() : ClassDriverBase()
  {
    return;
  }


  // Destructor.
  ClassDriverDomainBase::~ClassDriverDomainBase()
  {
    return;
  }


  // Init driver base.
  void ClassDriverDomainBase::PreInit()
  {
    // Domain.
    domain_regular_ = false;

    Ops::Ops ops(ClassDriverBase::configuration_file_);
    ops.SetPrefix(ClassDriverBase::prefix_ + ".");

    if (ops.Exists("domain"))
      {
        ops.SetPrefix(ClassDriverBase::prefix_ + ".domain.");

        domain_regular_ = ops.Exists("x_min") && ops.Exists("y_min")
          && ops.Exists("Delta_x") && ops.Exists("Delta_y")
          && ops.Exists("Nx") && ops.Exists("Ny");

        if (domain_regular_)
          {
            x_min_ = ops.Get<real>("x_min");
            y_min_ = ops.Get<real>("y_min");
            x_delta_ = ops.Get<real>("Delta_x");
            y_delta_ = ops.Get<real>("Delta_y");
            Nx_ = ops.Get<int>("Nx");
            Ny_ = ops.Get<int>("Ny");
          }

        x_.resize(Nx_);
        for (int i = 0; i < Nx_; ++i)
          x_[i] = x_min_ + real(i) * x_delta_;

        y_.resize(Ny_);
        for (int i = 0; i < Ny_; ++i)
          y_[i] = y_min_ + real(i) * y_delta_;

        x_max_ = x_min_ + x_delta_ * (real(Nx_) + real(0.5));
        y_max_ = y_min_ + y_delta_ * (real(Ny_) + real(0.5));
        x_min_ -= x_delta_ * real(0.5);
        y_min_ -= y_delta_ * real(0.5);

        ops.Set("z_top", "", z_top_);
        Nz_ = int(z_top_.size());

        z_mid_.resize(Nz_);
        z_mid_[0] = z_top_[0] * real(0.5);
        for (int i = 1; i < Nz_; ++i)
          z_mid_[i] = (z_top_[i - 1] + z_top_[i]) * real(0.5);
      }

    ops.Close();
  }


  void ClassDriverDomainBase::PostInit()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Rank() << Fred() << Debug(2) << LogReset() << "Nx = " << Nx_ << endl;
    *AMCLogger::GetLog() << Rank() << Fred() << Debug(2) << LogReset() << "Ny = " << Ny_ << endl;
#endif

    if (Nx_ == 0 || Ny_ == 0)
      throw AMC::Error("Horizontal domain seems empty.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Rank() << Bblue() << Debug(2) << LogReset() << "Nz = " << Nz_ << endl;
    *AMCLogger::GetLog() << Rank() << Fblue() << Debug(2) << LogReset() << "z_top = " << z_top_ << endl;
    *AMCLogger::GetLog() << Rank() << Fcyan() << Debug(3) << LogReset() << "z_mid = " << z_mid_ << endl;
#endif

    if (Nz_ == 0)
      throw AMC::Error("Vertical domain seems empty.");

    // Check if domain is regular, compute delta if it is.
    domain_regular_ = true;

    for (int i = 1; i < Nx_ - 1; ++i)
      if (abs(x_[i + 1] + x_[i - 1] - real(2) * x_[i]) > real(DRIVER_BASE_REGULAR_DOMAIN_THRESHOLD))
        {
          domain_regular_ = false;
          break;
        }

    for (int i = 1; i < Ny_ - 1; ++i)
      if (abs(y_[i + 1] + y_[i - 1] - real(2) * y_[i]) > real(DRIVER_BASE_REGULAR_DOMAIN_THRESHOLD))
        {
          domain_regular_ = false;
          break;
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Rank() << Fgreen() << Debug(2) << LogReset() << "domain_regular = "
                         << (domain_regular_ ? "yes" : "no") << endl;
#endif

    if (domain_regular_)
      {
        x_delta_ = (x_.back() - x_.front()) / real(Nx_ > 1 ? Nx_ - 1 : 1);
        y_delta_ = (y_.back() - y_.front()) / real(Ny_ > 1 ? Ny_ - 1 : 1);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Rank() << Bgreen() << Debug(3) << LogReset() << "x_delta = " << x_delta_ << endl;
        *AMCLogger::GetLog() << Rank() << Bgreen() << Debug(3) << LogReset() << "y_delta = " << y_delta_ << endl;
#endif
      }
    else
      {
        x_min_ = real(1.5) * x_[0] - real(0.5) * x_[1];
        x_max_ = real(1.5) * x_[Nx_ - 1] - real(0.5) * x_[Nx_ - 2];
        y_min_ = real(1.5) * y_[0] - real(0.5) * y_[1];
        y_max_ = real(1.5) * y_[Ny_ - 1] - real(0.5) * y_[Ny_ - 2];

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Rank() << Byellow() << Debug(3) << LogReset() << "x_min = " << x_min_ << endl;
        *AMCLogger::GetLog() << Rank() << Byellow() << Debug(3) << LogReset() << "y_min = " << y_min_ << endl;
        *AMCLogger::GetLog() << Rank() << Bmagenta() << Debug(3) << LogReset() << "x_max = " << x_max_ << endl;
        *AMCLogger::GetLog() << Rank() << Bmagenta() << Debug(3) << LogReset() << "y_max = " << y_max_ << endl;
#endif
      }
  }


  // Get methods.
  int ClassDriverDomainBase::GetNz()
  {
    return Nz_;
  }

  
  int ClassDriverDomainBase::GetNy()
  {
    return Ny_;
  }

  
  int ClassDriverDomainBase::GetNx()
  {
    return Nx_;
  }

  
  bool ClassDriverDomainBase::IsDomainRegular()
  {
    return domain_regular_;
  }

  
  void ClassDriverDomainBase::GetX(vector<real> &x)
  {
    x = x_;
  }

  
  void ClassDriverDomainBase::GetY(vector<real> &y)
  {
    y = y_;
  }

  
  real ClassDriverDomainBase::GetXmin()
  {
    return x_min_;
  }

  
  real ClassDriverDomainBase::GetYmin()
  {
    return y_min_;
  }

  
  real ClassDriverDomainBase::GetXmax()
  {
    return x_max_;
  }

  
  real ClassDriverDomainBase::GetYmax()
  {
    return y_max_;
  }

  
  real ClassDriverDomainBase::GetDeltaX()
  {
    return x_delta_;
  }

  
  real ClassDriverDomainBase::GetDeltaY()
  {
    return y_delta_;
  }

  
  void ClassDriverDomainBase::GetTopZ(vector<real> &z_top)
  {
    z_top = z_top_;
  }

  
  void ClassDriverDomainBase::GetMidZ(vector<real> &z_mid)
  {
    z_mid = z_mid_;
  }


  // Clear static data.
  void ClassDriverDomainBase::Clear()
  {
    Nz_ = 0;
    Ny_ = 0;
    Nx_ = 0;

    x_.clear();
    y_.clear();
    x_delta_ = real(0);
    x_min_ = real(0);
    x_max_ = real(0);
    y_delta_ = real(0);
    y_min_ = real(0);
    y_max_ = real(0);
    domain_regular_ = true;
    z_top_.clear();
    z_mid_.clear();

    ClassDriverBase::Clear();
  }
}

#define AMC_FILE_CLASS_DRIVER_DOMAIN_BASE_CXX
#endif
