// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_GEO_DATA_ELEVATION_CXX

#include "ClassGeoDataElevation.hxx"

namespace Driver
{
  // Constructor.
  template<class T>
  ClassGeoDataElevation<T>::ClassGeoDataElevation(const string name)
    : ClassGeoDataBase<T>(name)
  {
    return;
  }


  // Destructor.
  template<class T>
  ClassGeoDataElevation<T>::~ClassGeoDataElevation()
  {
    return;
  }

  // Get methods.
  template<class T>
  T ClassGeoDataElevation<T>::GetValue(const T &latitude, const T &longitude) const
  {
    vector<T> x = {longitude, latitude};
    return ClassGeoDataBase<T>::GetValue(x);
  }

  // Extract.
  template<class T>
  void ClassGeoDataElevation<T>::Extract(const T &latitude_min, const T &latitude_max,
                                         const T &longitude_min, const T &longitude_max)
  {
    vector<T> Xmin = {longitude_min, latitude_min};
    vector<T> Xmax = {longitude_max, latitude_max};
    ClassGeoDataBase<T>::Extract(Xmin, Xmax);
  }


  // Interpolate.
  template<class T>
  void ClassGeoDataElevation<T>::Interpolate(const vector<T> &latitude, const vector<T> &longitude)
  {
    vector<vector<T> > X(this->D_);

    if (int(longitude.size()) == 3)
      {
        int n = int((longitude[1] - longitude[0]) / longitude[2] + T(1.e-6));
        X[0].resize(n);
        for (int i = 0; i < n; ++i)
          X[0][i] = longitude[0] + longitude[2] * T(i);
      }
    else
      X[0] = longitude;

    if (int(latitude.size()) == 3)
      {
        int n = int((latitude[1] - latitude[0]) / latitude[2] + T(1.e-6));
        X[1].resize(n);
        for (int i = 0; i < n; ++i)
          X[1][i] = latitude[0] + latitude[2] * T(i);
      }
    else
      X[1] = latitude;

    ClassGeoDataBase<T>::Interpolate(X);    
  }
}

#define AMC_FILE_CLASS_GEO_DATA_ELEVATION_CXX
#endif
