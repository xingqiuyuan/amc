// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_GEO_DATA_BASE_CXX

#include "ClassGeoDataBase.hxx"

namespace Driver
{
  // Init.
  template<class T>
  void ClassGeoDataBase<T>::Init(const string &configuration_file)
  {
    configuration_file_ = configuration_file;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info() << LogReset() << "Init Geo Data with configuration file \""
                         << configuration_file_ << "\"." << endl;
#endif

    Ops::Ops ops(configuration_file_);
    ops.SetPrefix("geo_data.");

    data_directory_ = ops.Get<string>("data_directory", "", "");
    name_list_ = ops.GetEntryList("variable");

    ops.Close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "List of variables : " << name_list_ << endl;
#endif

    is_initiated_ = true;
  }


  // Constructor.
  template<class T>
  ClassGeoDataBase<T>::ClassGeoDataBase(const string name)
    : name_(name), P_(0), S_(0), D_(0),
      value_min_(GEO_DATA_MIN_VALUE),
      value_max_(GEO_DATA_MAX_VALUE),
      data_file_("")
  {
    if (! is_initiated_)
      throw AMC::Error("GeoDataBase is not yet initiated, call Init() first.");

    Ops::Ops ops(configuration_file_);
    ops.SetPrefix("geo_data.variable." + name_ + ".");
    data_file_ = ops.Get<string>("file", "", name_ + ".bin");
    if (data_file_[0] != '/')
      data_file_ = data_directory_ + "/" + data_file_;

    value_min_ = ops.Get<T>("min", "", value_min_);
    value_max_ = ops.Get<T>("max", "", value_max_);

    ops.Close();

    util::check_file(data_file_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "Instantiate Geo Data \"" << name_
                         << "\" with data file \"" << data_file_ << "\"." << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "For Geo Data \"" << name_
                         << "\" : value_min = " << value_min_ << ", value_max = " << value_max_ << endl;
#endif

    ifstream fin(data_file_.c_str(), ifstream::binary);
    fin.read(reinterpret_cast<char*>(&D_), sizeof(int));

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "D = " << D_ << endl;
#endif

    // Number of edges of hypercube.
    P_ = 1;
    for (int i = 0; i < D_; ++i)
      P_ *= 2;

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "P = " << P_ << endl;
#endif

    N_.resize(D_);
    fin.read(reinterpret_cast<char*>(N_.data()), D_ * sizeof(int));

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "N = " << N_ << endl;
#endif

    // Data size.
    S_ = 1;
    for (int i = 0; i < D_; ++i)
      S_ *= N_[i];

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "S = " << S_ << endl;
#endif

    X_.resize(D_);
    for (int i = 0; i < D_; ++i)
      {
        X_[i].resize(N_[i]);

        fin.read(reinterpret_cast<char*>(X_[i].data()), N_[i] * sizeof(T));

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
        CBUG << "X[" << i << "] = " << X_[i].front() << " " << X_[i].back() << endl;
#endif
      }

    data_.resize(S_);
    fin.read(reinterpret_cast<char*>(data_.data()), S_ * sizeof(T));

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "data[0]  = " << data_.front() << ", data[-1] = " << data_.back() << endl;
#endif

    fin.close();

    return;
  }


  // Destructor.
  template<class T>
  ClassGeoDataBase<T>::~ClassGeoDataBase()
  {
    return;
  }


  // Get methods.
  template<class T>
  string ClassGeoDataBase<T>::GetName() const
  {
    return name_;
  }


  template<class T>
  int ClassGeoDataBase<T>::GetN(const int i) const
  {
    return N_[i];
  }


  template<class T>
  int ClassGeoDataBase<T>::GetP() const
  {
    return P_;
  }


  template<class T>
  int ClassGeoDataBase<T>::GetD() const
  {
    return D_;
  }


  template<class T>
  int ClassGeoDataBase<T>::GetS() const
  {
    return S_;
  }

  
  template<class T>
  void ClassGeoDataBase<T>::GetX(const int &i, vector<T> &x) const
  {
    x = X_[i];
  }


  template<class T>
  void ClassGeoDataBase<T>::GetData(vector<T> &data) const
  {
    data = data_;
  }


  template<class T>
  string ClassGeoDataBase<T>::GetDataFile() const
  {
    return data_file_;
  }


  template<class T>
  T ClassGeoDataBase<T>::GetValue(const vector<T> &x) const
  {
    vector<int> ipos(D_);
    vector<T> rpos(D_);

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "x = " << x << endl;
#endif

    for (int i = 0; i < D_; ++i)
      {
        const vector<T> &X = X_[i];

        // Limit search to (X[0], ..., X[D_-2]), X[D_-1].
        // If greater thant X[D_-2], returns D_-2
        // and following algorithms remain safe.
        ipos[i] = AMC::search_index(X, x[i], 0, -2);
        rpos[i] = (x[i] - X[ipos[i]]) / (X[ipos[i] + 1] - X[ipos[i]]);          

        // Keep relative position inside of domain.
        rpos[i] = rpos[i] < T(0) ? T(0) : rpos[i];
        rpos[i] = rpos[i] > T(1) ? T(1) : rpos[i];
      }

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "ipos = " << ipos << endl;
    CBUG << "rpos = " << rpos << endl;
#endif

    vector<int> index(D_, 0);
    T data(T(0));

    for (int i = 0; i < P_; ++i)
      {
        int pos = ipos[D_ - 1] + index[D_ - 1];
        for (int j = D_ - 2; j >= 0; --j)
          pos = ipos[j] + index[j] + pos * N_[j];

        T weight(T(1));
        for (int j = 0; j < D_; ++j)
          weight *= (T(1) - rpos[j]) * T(1 - index[j]) + rpos[j] * T(index[j]);

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
        CBUG << "index = " << index << endl;
        CBUG << "pos = " << pos << endl;
        CBUG << "weight = " << weight << endl;
#endif

        index[0]++;
        for (int j = 0; j < D_ - 1; ++j)
          if (index[j] == 2)
            {
              index[j] = 0;
              index[j + 1]++;
            }

        data += weight * data_[pos];
      }

    // At last filter.
    data = data > value_max_ ? value_max_ : data;
    data = data < value_min_ ? value_min_ : data;

    return data;
  }


  template<class T>
  T ClassGeoDataBase<T>::Min() const
  {
    T data_min = data_[0];
    for (int i = 1; i < S_; ++i)
      if (data_[i] < data_min)
        data_min = data_[i];
    return data_min;
  }


  template<class T>
  T ClassGeoDataBase<T>::Max() const
  {
    T data_max = data_[0];
    for (int i = 1; i < S_; ++i)
      if (data_[i] > data_max)
        data_max = data_[i];
    return data_max;
  }


  // Extract.
  template<class T>
  void ClassGeoDataBase<T>::Extract(const vector<T> &Xmin, const vector<T> &Xmax)
  {
#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "Xmin = " << Xmin << endl;
    CBUG << "Xmax = " << Xmax << endl;
#endif

    vector<int> index(D_, 0);

    int h(-1);
    for (int i = 0; i < S_; ++i)
      {
        int pos = index[D_ - 1];
        for (int j = D_ - 2; j >= 0; --j)
          pos = index[j] + pos * N_[j];

        bool outside(false);
        for (int j = 0; j < D_; ++j)
          {
            const int k = index[j];
            outside = X_[j][k] < Xmin[j] || X_[j][k] >= Xmax[j];
            if (outside)
              break;
          }

        if (! outside)
          data_[++h] = data_[pos];

        index[0]++;
        for (int j = 0; j < D_ - 1; ++j)
          if (index[j] == N_[j])
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

    if (++h < S_)
      {
        S_ = h;
        data_.resize(S_);
#if __cplusplus > 199711L
        data_.shrink_to_fit();
#endif
      }

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "S = " << S_ << endl;
#endif

    for (int i = 0; i < D_; ++i)
      {
        int j(-1);
        for (int k = 0; k < N_[i]; ++k)
          if (X_[i][k] >= Xmin[i] && X_[i][k] < Xmax[i])
            X_[i][++j] = X_[i][k];

        if (++j < N_[i])
          {
            X_[i].resize(j);
#if __cplusplus > 199711L
            X_[i].shrink_to_fit();
#endif
            N_[i] = j;
          }
      }

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "N = " << N_ << endl;
#endif
  }


  // Interpolate.
  template<class T>
  void ClassGeoDataBase<T>::Interpolate(const vector<vector<T> > &X)
  {
#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "X = " << X << endl;
#endif

    vector<int> N(D_);
    int S(1);
    for (int i = 0; i < D_; ++i)
      {
        N[i] = int(X[i].size());
        S *= N[i];
      }

#ifdef DRIVER_GEO_DATA_WITH_DEBUG
    CBUG << "S = " << S << endl;
    CBUG << "N = " << N << endl;
#endif

    vector<T> data(S);
    vector<int> index(D_, 0);

    for (int i = 0; i < S; ++i)
      {
        vector<T> x(D_);
        for (int j = 0; j < D_; ++j)
          x[j] = X[j][index[j]];

        data[i] = GetValue(x);

        index[0]++;
        for (int j = 0; j < D_ - 1; ++j)
          if (index[j] == N[j])
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

    // At last, give to object.
    S_ = S;
    data_ = data;
    N_ = N;
    X_ = X;
  }


  template<class T>
  void ClassGeoDataBase<T>::GetNameList(vector<string> &name_list)
  {
    name_list = name_list_;
  }


  template<class T>
  string ClassGeoDataBase<T>::GetDataDirectory()
  {
    return data_directory_;
  }


  template<class T>
  string ClassGeoDataBase<T>::GetConfigurationFile()
  {
    return configuration_file_;
  }


  // Clear data.
  template<class T>
  void ClassGeoDataBase<T>::Clear()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(2) << LogReset() << "Clearing all static data, need to call Init() again." << endl;
#endif

    configuration_file_ = "";
    data_directory_ = "";
    name_list_.clear();
    is_initiated_ = false;
  }
}

#define AMC_FILE_CLASS_GEO_DATA_BASE_CXX
#endif
