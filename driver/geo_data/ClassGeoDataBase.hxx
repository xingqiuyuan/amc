// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_GEO_DATA_BASE_HXX

#define GEO_DATA_BAD_VALUE -9999.
#define GEO_DATA_MIN_VALUE -1.e9
#define GEO_DATA_MAX_VALUE  1.e9

namespace Driver
{
  /*! 
   * \class ClassGeoDataBase
   */
  template<class T>
  class ClassGeoDataBase
  {
  protected:

    /*!< Number of dimensions.*/
    int D_;

    /*!< Data size.*/
    int S_;

    /*!< Hypercube edges.*/
    int P_;

    /*!< Name of data.*/
    string name_;

    /*!< Data size.*/
    vector<int> N_;

    /*!< Coordinates.*/
    vector<vector<T> > X_;

    /*!< Data.*/
    vector<T> data_;

    /*!< Min and max value with which to filter.*/
    real value_min_, value_max_;

    /*!< Data file.*/
    string data_file_;

    /*!< is it initialized ?.*/
    static bool is_initiated_;

    /*!< Configuration file.*/
    static string configuration_file_;

    /*!< Configuration file.*/
    static string data_directory_;

    /*!< List of geo data variable names.*/
    static vector1s name_list_;

  public:

    /*!< Init.*/
    static void Init(const string &configuration_file);

    /*!< Constructor.*/
    ClassGeoDataBase(const string name);

    /*!< Destructor.*/
    ~ClassGeoDataBase();

    /*!< Get methods.*/
    string GetName() const;
    int GetN(const int i) const;
    int GetD() const;
    int GetP() const;
    int GetS() const;
    void GetX(const int &i, vector<T> &x) const;
    void GetData(vector<T> &data) const;
    string GetDataFile() const;
    virtual T GetValue(const vector<T> &x) const;
    T Min() const;
    T Max() const;

    /*!< Extract.*/
    virtual void Extract(const vector<T> &Xmin, const vector<T> &Xmax);

    /*!< Interpolate.*/
    virtual void Interpolate(const vector<vector<T> > &X);

    static void GetNameList(vector<string> &name_list);
    static string GetDataDirectory();
    static string GetConfigurationFile();

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_GEO_DATA_BASE_HXX
#endif
