// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_GEO_DATA_ELEVATION_HXX

namespace Driver
{
  /*! 
   * \class ClassGeoDataElevation
   */
  template<class T>
  class ClassGeoDataElevation : public ClassGeoDataBase<T>
  {
  public:

    /*!< Constructor.*/
    ClassGeoDataElevation(const string name = "elevation");

    /*!< Destructor.*/
    ~ClassGeoDataElevation();

    /*!< Get methods.*/
    T GetValue(const T &latitude, const T &longitude) const;

    /*!< Extract.*/
    void Extract(const T &latitude_min, const T &latitude_max,
                 const T &longitude_min, const T &longitude_max);

    /*!< Interpolate.*/
    void Interpolate(const vector<T> &latitude, const vector<T> &longitude);
  };
}

#define AMC_FILE_CLASS_GEO_DATA_ELEVATION_HXX
#endif
