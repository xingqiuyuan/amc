// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_DRIVER_HEADER_HXX

/*!
 * \file DriverTrajectoryHeader.hxx
 * \brief Main Trajectory driver header file.
 * \author Edouard Debry
 */

#include <ctime>
#include <regex.h>
#include <errno.h>
#include <sys/types.h>
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#endif

#ifdef DRIVER_WITH_TEST
#define DRIVER_HAS_TEST 1
#else
#define DRIVER_HAS_TEST 0
#endif

#ifdef DRIVER_WITH_MPI
#define DRIVER_HAS_MPI 1
#define MPI_INCLUDED
#include <mpi.h>
#else
#define DRIVER_HAS_MPI 0
#endif

// NetCDF library.
#ifdef DRIVER_WITH_NETCDF
#define DRIVER_HAS_NETCDF 1
#include <netcdfcpp.h>
#else
#define DRIVER_HAS_NETCDF 0
#endif

// Logger.
#ifdef AMC_WITH_LOGGER
#define DRIVER_HAS_LOGGER 1
#ifdef DRIVER_WITH_MPI
#define LOGGER_WITH_MPI
#endif
#else
#define DRIVER_HAS_LOGGER 0
#endif

#ifdef AMC_WITH_TIMER
#define DRIVER_HAS_TIMER 1
#else
#define DRIVER_HAS_TIMER 0
#endif

#ifdef AMC_WITH_STATISTICS
#define DRIVER_HAS_STATISTICS 1
#else
#define DRIVER_HAS_STATISTICS 0
#endif

#include "AMCHeader.hxx"

// Parameterization Physics.
#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
#define DRIVER_HAS_PARAMETERIZATION_PHYSICS 1
#else
#define DRIVER_HAS_PARAMETERIZATION_PHYSICS 0
#endif

// Initial condition.
#ifdef DRIVER_WITH_MAPPING
#define DRIVER_HAS_MAPPING 1
#else
#define DRIVER_HAS_MAPPING 0
#endif

// Emission.
#ifdef DRIVER_WITH_EMISSION
#define DRIVER_HAS_EMISSION 1
#else
#define DRIVER_HAS_EMISSION 0
#endif

// Monahan's sea salt emissions.
#ifdef DRIVER_WITH_MONAHAN
#define DRIVER_HAS_MONAHAN 1
#else
#define DRIVER_HAS_MONAHAN 0
#endif

// Geo data.
#ifdef DRIVER_WITH_GEO_DATA
#define DRIVER_HAS_GEO_DATA 1
#else
#define DRIVER_HAS_GEO_DATA 0
#endif

// Trajectory model.
#ifdef DRIVER_WITH_TRAJECTORY
#define DRIVER_HAS_TRAJECTORY 1
#else
#define DRIVER_HAS_TRAJECTORY 0
#endif

// Chimere trajectory.
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
#define DRIVER_HAS_TRAJECTORY_CHIMERE 1
#else
#define DRIVER_HAS_TRAJECTORY_CHIMERE 0
#endif

// Polair3d trajectory.
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
#define DRIVER_HAS_TRAJECTORY_POLAIR3D 1
#else
#define DRIVER_HAS_TRAJECTORY_POLAIR3D 0
#endif

// Chimere model.
#ifdef DRIVER_WITH_CHIMERE
#define DRIVER_HAS_CHIMERE 1
#else
#define DRIVER_HAS_CHIMERE 0
#endif

// Chimere and AMC.
#ifdef CHIMERE_WITH_AMC
#define CHIMERE_HAS_AMC 1
#else
#define CHIMERE_HAS_AMC 0
#endif

#ifdef CHIMERE_AMC_WITH_NUMBER_CONCENTRATION
#define CHIMERE_AMC_HAS_NUMBER_CONCENTRATION 1
#else
#define CHIMERE_AMC_HAS_NUMBER_CONCENTRATION 0
#endif

#ifdef CHIMERE_AMC_WITH_PREPROCESSING
#define CHIMERE_AMC_HAS_PREPROCESSING 1
#else
#define CHIMERE_AMC_HAS_PREPROCESSING 0
#endif

// Champrobois model.
#ifdef DRIVER_WITH_CHAMPROBOIS
#define DRIVER_HAS_CHAMPROBOIS 1
#ifdef DRIVER_CHAMPROBOIS_WITH_SOCKET
#define DRIVER_CHAMPROBOIS_HAS_SOCKET 1
#else
#define DRIVER_CHAMPROBOIS_HAS_SOCKET 0
#endif
#else
#define DRIVER_HAS_CHAMPROBOIS 0
#define DRIVER_CHAMPROBOIS_HAS_SOCKET 0
#endif

#ifdef DRIVER_WITH_DEBUG_CHAMPROBOIS_DRIVER
#define DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER 1
#else
#define DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER 0
#endif

// Numerical solvers.
#ifdef DRIVER_WITH_NUMERICAL_SOLVER
#define DRIVER_HAS_NUMERICAL_SOLVER 1
#else
#define DRIVER_HAS_NUMERICAL_SOLVER 0
#endif

// Blitz++.
#include <blitz/array.h>

#ifndef SWIG
extern "C"

{
  extern void dgttrf_(const int*, double*, double*, double*, double*, int*, int*);
  extern void dgttrs_(const char*, const int*, const int*, const double*, const double*, const double*,
                      const double*, const int*, double*, const int*, int*);
}
#endif

// Physical constants.
#define DRIVER_AIR_MOLAR_MASS                      2.897e-2  // Air molar mass kg.mol^{-1}.
#define DRIVER_WATER_MOLAR_MASS                    1.8e-2    // Water molar mass kg.mol^{-1}.
#define DRIVER_TEMPERATURE_ZERO_DEGREE_CELSIUS     273.15    // O°C in Kelvin.
#define DRIVER_TEMPERATURE_REFERENCE               298.0     // Standard reference temperature Kelvin.
#define DRIVER_RELATIVE_HUMIDITY_REFERENCE         0.7       // Reference relative humidity.
#define DRIVER_SPECIFIC_HUMIDITY_REFERENCE         0.0136    // Specific humidity reference for RH = 0.7.
#define DRIVER_WATER_COLLISION_FACTOR              356.      // Lennard-Jones collision factor. 
#define DRIVER_WATER_MOLECULAR_DIAMETER            2.649     // Water molecular diameter in pm.
#define DRIVER_CONVERT_FROM_G_TO_MICROG            1.e-6     // Convert factor to multiply per gram constants.

/*! \namespace Driver
 * 
 * Driver for AMC.
 */
namespace Driver
{
  using namespace std;
#if __cplusplus > 199711L
  using namespace std::placeholders;
#endif

#ifdef AMC_WITH_LOGGER
  using namespace Logger;
  typedef Logger::Reset LogReset;
  using AMC::AMCLogger;
#endif

#ifndef SWIG
  using blitz::Array;
  using blitz::Range;
  using blitz::shape;
  using blitz::neverDeleteData;
#endif

  typedef double real;
  typedef float real_io;

  typedef vector<string>   vector1s;
  typedef vector<vector1s> vector2s;
  typedef vector<bool>     vector1b;
  typedef vector<int>      vector1i;
  typedef vector<vector1i> vector2i;
  typedef vector<real>     vector1r;
  typedef vector<vector1r> vector2r;

#define DRIVER_PARAMETERIZATION_REAL Driver::real

  using namespace util;
#ifndef SWIG
  using AMC::ClassModalDistribution;
#endif
  //using namespace AMC;
}

#ifdef DRIVER_WITH_NUMERICAL_SOLVER
#include "numerics/ClassNumericalSolverAdvectionLaxWendroff.hxx"
#include "numerics/ClassNumericalSolverFluxLimiterRoesSuperbee.hxx"
#include "numerics/ClassNumericalSolverDiffusionLinear.hxx"
#include "numerics/ClassNumericalSolverDiffusionRadial.hxx"
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
#include "driver/parameterization/ClassParameterizationPhysics.hxx"
#endif

#ifdef DRIVER_WITH_GEO_DATA
#include "geo_data/ClassGeoDataBase.hxx"
#include "geo_data/ClassGeoDataElevation.hxx"
#endif

#ifdef DRIVER_WITH_MONAHAN
#include "parameterization/ClassParameterizationEmissionSeaSaltMonahan.hxx"
#endif

#ifdef DRIVER_WITH_EMISSION
#include "emission/ClassTracer.hxx"
#include "emission/ClassSource.hxx"
#include "emission/ClassDynamicsEmission.hxx"
#endif

#include "base/ClassDriverBase.hxx"
#include "base/ClassDriverDomainBase.hxx"

#ifdef DRIVER_WITH_MAPPING
#include "mapping/ClassMappingSize.hxx"
#ifdef DRIVER_WITH_TEST
#include "mapping/ClassMappingSizeTest.hxx"
#endif
#include "mapping/ClassMappingSpecies.hxx"
#include "mapping/ClassMappingParticle.hxx"
#endif

#ifdef DRIVER_WITH_TRAJECTORY
#include "trajectory/ClassDriverTrajectoryBase.hxx"
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
#include "trajectory/ClassDriverTrajectoryChimere.hxx"
#endif
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
#include "trajectory/ClassDriverTrajectoryPolair3d.hxx"
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
#ifndef SWIG
#include "chimere/ChimereHeader.hxx"
#endif
#ifdef DRIVER_WITH_EMISSION
#include "chimere/ClassChimereEmissionAnthropic.hxx"
#include "chimere/ClassChimereEmissionBiogenic.hxx"
#endif
#include "chimere/ClassChimereBoundaryCondition.hxx"
#include "chimere/ClassChimereBase.hxx"
#include "chimere/ClassDriverChimere.hxx"
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
#include "champrobois/ClassChamproboisBase.hxx"
#include "champrobois/ClassChamproboisGranulometry.hxx"
#include "champrobois/ClassChamproboisSpeciation.hxx"
#include "champrobois/ClassChamproboisSink.hxx"
#include "champrobois/ClassChamproboisFilter.hxx"
#include "champrobois/ClassChamproboisELPI.hxx"
#include "champrobois/ClassChamproboisSource.hxx"
#include "champrobois/ClassDriverChamprobois.hxx"
#endif

#define AMC_FILE_DRIVER_HEADER_HXX
#endif
