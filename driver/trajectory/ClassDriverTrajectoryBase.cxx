// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_BASE_CXX

#include "ClassDriverTrajectoryBase.hxx"

namespace Driver
{
  // Adjust latitude and longitude.
  inline void ClassDriverTrajectoryBase::adjust_latitude_longitude_(real &latitude, real &longitude)
  {
    // Keep latitude within [-90, 90] and longitude within [-180, 180].
    if (abs(latitude) > real(90))
      {
        if (latitude > real(0))
          latitude = real(180) - latitude;
        else
          latitude = latitude - real(180);
        longitude += real(180);
      }

    if (longitude > real(180))
      longitude -= real(360);
    else if (longitude < - real(180))
      longitude += real(360);
  }


  // Filter variables.
  void ClassDriverTrajectoryBase::apply_filter_variable_(const string &name)
  {
    map<string, real>::iterator imin = variable_min_.find(name);
    map<string, real>::iterator imax = variable_max_.find(name);

    map<string, vector2r*>::iterator ivar = variable_.find(name);

    if (ivar != variable_.end())
      {
        int Nz = variable_z_level_[name];

        if (imin != variable_min_.end())
          for (int i = 0; i < Nt_; ++i)
            for (int j = 0; j < Nz; ++j)
              if ((*ivar->second)[i][j] < imin->second)
                (*ivar->second)[i][j] = imin->second;

        if (imax != variable_max_.end())
          for (int i = 0; i < Nt_; ++i)
            for (int j = 0; j < Nz; ++j)
              if ((*ivar->second)[i][j] > imax->second)
                (*ivar->second)[i][j] = imax->second;
      }
    else
      throw AMC::Error("Variable \"" + name + "\" not found in variable map.");
  }


  // Filter variables.
  void ClassDriverTrajectoryBase::set_filter_variable_(Ops::Ops &ops)
  {
    vector1s variable_name = ops.GetEntryList();

    for (int i = 0; i < int(variable_name.size()); ++i)
      if (ops.Exists(variable_name[i] + ".min"))
        {
          variable_min_[variable_name[i]] = ops.Get<real>(variable_name[i] + ".min");

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "Variable \"" << variable_name[i]
                               << "\" will be filtered below " << variable_min_[variable_name[i]] << endl;
#endif
      }

    for (int i = 0; i < int(variable_name.size()); ++i)
      if (ops.Exists(variable_name[i] + ".max"))
        {
          variable_max_[variable_name[i]] = ops.Get<real>(variable_name[i] + ".max");

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fmagenta() << Info(2) << LogReset() << "Variable \"" << variable_name[i]
                               << "\" will be filtered above " << variable_max_[variable_name[i]] << endl;
#endif
        }
  }


  // Constructor.
  ClassDriverTrajectoryBase::ClassDriverTrajectoryBase()
    : ClassDriverDomainBase(), id_(id_counter_++), Nt_(0), output_file_(""),
      is_loaded_(false), time_step_(real(0)), date_begin_(real(0))
  {
    for (int i = 0; i < Nvariable_; ++i)
      variable_[variable_name_[i]] = new vector2r();

    ostringstream fout;
    fout << "./trajectory_" << this->type_.substr(this->type_.find(":") + 1)
         << "_" << setw(3) << setfill('0') << id_ << ".bin";
    output_file_ = fout.str();

    return;
  }


  // Destructor.
  ClassDriverTrajectoryBase::~ClassDriverTrajectoryBase()
  {
    for (int i = 0; i < Nvariable_; ++i)
      delete variable_[variable_name_[i]];

    return;
  }


  // Init trajectory base.
  void ClassDriverTrajectoryBase::PreInit(const string &configuration_file, const string &prefix, string type)
  {
    // Base driver initialization.
    type = "trajectory:" + type;
    ClassDriverBase::Init(configuration_file, prefix, type);
    ClassDriverDomainBase::PreInit();

#ifdef DRIVER_WITH_GEO_DATA
    ClassGeoDataBase<float>::Init(configuration_file);
#endif

    // Hard coded variables, always needed.
    variable_name_ = {"temperature", "pressure", "relative_humidity", "Kz"};
    variable_z_level_["temperature"] = 0;
    variable_z_level_["pressure"] = 0;
    variable_z_level_["relative_humidity"] = 0;
    variable_z_level_["Kz"] = 0;

    Ops::Ops ops(configuration_file);

    // Loop on prefix sections for filters, from most general to specific ones. 
    istringstream iss(prefix);
    string prefix_filter, prefix_filter_old("");
    while (std::getline(iss, prefix_filter, '.'))
      {
        if (! prefix_filter_old.empty())
          prefix_filter = prefix_filter_old + "." + prefix_filter;

        if (ops.Exists(prefix_filter + ".filter"))
          {
            ops.SetPrefix(prefix_filter + ".filter.");
            set_filter_variable_(ops);
          }

        prefix_filter_old = prefix_filter;
      }

    // Initialize specific data to type.
    ops.SetPrefix(prefix + ".");

    vector1s data_type = ops.GetEntryList("file");
    for (int i = 0; i < int(data_type.size()); i++)
      {
        string data_file = ops.Get<string>("file." + data_type[i]);
        util::check_file(data_file);
        data_file_[data_type[i]] = data_file;
      }

    ops.Close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset() << "Input data files for type \""
                         << type << "\" = " << data_file_ << endl;
#endif
  }


  void ClassDriverTrajectoryBase::PostInit()
  {
    ClassDriverDomainBase::PostInit();

    // Compute surface.
    horizontal_surface_.resize(ClassDriverDomainBase::Ny_, ClassDriverDomainBase::Nx_);

    int j_last = ClassDriverDomainBase::Ny_ - 1;
    int i_last = ClassDriverDomainBase::Nx_ - 1;

    for (int j = 0; j < ClassDriverDomainBase::Ny_; ++j)
      for (int i = 0; i < ClassDriverDomainBase::Nx_; ++i)
        {
          real y_delta = ClassDriverDomainBase::y_delta_;
          real x_delta = ClassDriverDomainBase::x_delta_;

          if (! domain_regular_)
            {
              if (j == 0)
                y_delta = (ClassDriverDomainBase::y_[1] - ClassDriverDomainBase::y_[0]);
              if (j == j_last)
                y_delta = (ClassDriverDomainBase::y_[j_last] - ClassDriverDomainBase::y_[j_last - 1]);
              else
                y_delta = (ClassDriverDomainBase::y_[j + 1] - ClassDriverDomainBase::y_[j - 1]) * real(0.5);

              if (i == 0)
                x_delta = (ClassDriverDomainBase::x_[1] - ClassDriverDomainBase::x_[0]);
              if (i == i_last)
                x_delta = (ClassDriverDomainBase::x_[i_last] - ClassDriverDomainBase::x_[i_last - 1]);
              else
                x_delta = (ClassDriverDomainBase::x_[i + 1] - ClassDriverDomainBase::x_[i - 1]) * real(0.5);
            }

          horizontal_surface_(j, i) = DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER_SQUARE
            * DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_DEGREE_TO_RADIUS_SQUARE
            * y_delta * x_delta * cos(ClassDriverDomainBase::y_[j] * DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_DEGREE_TO_RADIUS);
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << LogReset() << "Computed horizontal surface :"
                         << " min = " << min(horizontal_surface_) << ", mean = " << mean(horizontal_surface_)
                         << ", max = " << max(horizontal_surface_) << endl;
#endif

    // List of variables.
    Nvariable_ = int(variable_name_.size());
    for (map<string, int>::iterator it = variable_z_level_.begin(); it != variable_z_level_.end(); ++it)
      if (it->second == 0)
        it->second = ClassDriverDomainBase::Nz_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "Number of variables = " << Nvariable_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(1) << LogReset() << "List of variables = " << variable_name_ << endl;
    *AMCLogger::GetLog() << Bcyan() << Debug(1) << LogReset() << "variable_z_level_ = " << variable_z_level_ << endl;
#endif
  }


  // Load data, common work.
  void ClassDriverTrajectoryBase::Load()
  {
    // Coordinates.
    latitude_.resize(Nt_);
    longitude_.resize(Nt_);

    // Variables.
    for (int i = 0; i < Nvariable_; i++)
      {
        variable_[variable_name_[i]]->resize(Nt_);
        apply_filter_variable_(variable_name_[i]);
      }

    // Altitude.
    altitude_.resize(Nt_);

    // Say that we have loaded.
    is_loaded_ = true;
  }


  // Get methods.
  int ClassDriverTrajectoryBase::GetId() const
  {
    return id_;
  }

  
  int ClassDriverTrajectoryBase::GetNt() const
  {
    return Nt_;
  }


  void ClassDriverTrajectoryBase::GetVariable(const string name, vector<vector<real> > &variable) const
  {
    map<string, vector2r*>::const_iterator it = variable_.find(name);

    if (it != variable_.end())
      variable = *(it->second);
    else
      throw AMC::Error("Variable \"" + name + "\" does not exist.");
  }


  void ClassDriverTrajectoryBase::GetLatitude(vector<real> &latitude) const
  {
    latitude = latitude_;
  }


  void ClassDriverTrajectoryBase::GetLongitude(vector<real> &longitude) const
  {
    longitude = longitude_;
  }


  void ClassDriverTrajectoryBase::GetAltitude(vector<real> &altitude) const
  {
    altitude = altitude_;
  }


  real ClassDriverTrajectoryBase::GetTimeStep() const
  {
    return time_step_;
  }

  real ClassDriverTrajectoryBase::GetDateBegin() const
  {
    return date_begin_;
  }


  string ClassDriverTrajectoryBase::GetOutputFile() const
  {
    return output_file_;
  }


  bool ClassDriverTrajectoryBase::ReadConcentration(const int t, const int z,
                                                    vector<real> &concentration_aer_num,
                                                    vector<real> &concentration_aer_mass,
                                                    vector<real> &concentration_gas) const
  {
    // Resize concentrations.
    concentration_aer_num.assign(AMC::ClassAerosolData::GetNg(), real(0));
    concentration_aer_mass.assign(AMC::ClassAerosolData::GetNgNspecies(), real(0));
    concentration_gas.assign(AMC::ClassSpecies::GetNgas(), real(0));

    return AMC::ClassAerosolData::ReadConcentration(output_file_,
                                                    concentration_aer_num.data(),
                                                    concentration_aer_mass.data(),
                                                    concentration_gas.data(),
                                                    t * this->Nz_ + z);
  }


  int ClassDriverTrajectoryBase::GetIdCounter()
  {
    return id_counter_;
  }


  int ClassDriverTrajectoryBase::GetNvariable()
  {
    return Nvariable_;
  }

  
  real ClassDriverTrajectoryBase::GetDataTimeStep()
  {
    return data_time_step_;
  }

  
  string ClassDriverTrajectoryBase::GetDataDateBegin()
  {
    return data_date_begin_;
  }


  vector<string> ClassDriverTrajectoryBase::GetVariableName()
  {
    return variable_name_;
  }


  int ClassDriverTrajectoryBase::GetVariableLevelZ(const string name)
  {
    if (variable_z_level_.find(name) != variable_z_level_.end())
      return variable_z_level_[name];
    else
      throw AMC::Error("Variable \"" + name + "\" is not in \"variable_z_level\" map.");
  }

  
  map<string, string> ClassDriverTrajectoryBase::GetDataFile()
  {
    return data_file_;
  }


  // Set methods.
  void ClassDriverTrajectoryBase::SetOutputFile(const string output_file)
  {
    output_file_ = output_file;
  }


  // Run from one given space position.
  template<class S, class R, class D, class C>
  void ClassDriverTrajectoryBase::Run(vector<vector<real> > &concentration_aer_num,
                                      vector<vector<real> > &concentration_aer_mass,
                                      vector<vector<real> > &concentration_gas) const
  {
    // Break if not loaded.
    if (! is_loaded_)
      throw AMC::Error("Trajectory not loaded, call Load first.");

    // Also break if AMC is not loaded.
    if (! AMC::ClassAMX<S, R, D, C>::IsInitialized())
      throw AMC::Error("AMC model seems not loaded, call AMX::Initiate() first.");

    // Prevent from overwriting already existing output file.
    if (! output_file_.empty())
      if (access(output_file_.c_str(), F_OK) == 0)
        throw AMC::Error("Output file \"" + output_file_ + "\" for trajectory n°" +
                         to_str(id_) + " of type \"" + this->type_ + "\" already exists.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << LogReset() << "Run trajectory id n°" << id_ << " starting at "
                         << "position (" << abs(latitude_[0]) << "°" << (latitude_[0] >= real(0) ? "N" : "S")
                         << ", " << longitude_[0] << "°" << (longitude_[0] >= real(0) ? "W" : "E")
                         << ") and " << date_begin_ << " seconds after data beginning date (" << data_date_begin_
                         << ")." << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << LogReset() << "time_step = " << time_step_ << endl;
#endif

    const int Nz = ClassDriverDomainBase::Nz_;
    const int Ntracer = ClassTracer::GetNtracer();
    const int Nsource = ClassSource::GetNsource();

    // Constants from AMC.
    const int Ng = AMC::ClassAerosolData::GetNg();
    const int Nspecies = AMC::ClassSpecies::GetNspecies();
    const int Ngas = AMC::ClassSpecies::GetNgas();
    const int NgNspecies = Ng * Nspecies;

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
    CBUG << "Nz = " << Nz << endl;
    CBUG << "Nsource = " << Nsource << endl;
    CBUG << "Ntracer = " << Ntracer << endl;
    CBUG << "Ng = " << Ng << endl;
    CBUG << "Nspecies = " << Nspecies << endl;
    CBUG << "Ngas = " << Ngas << endl;
    CBUG << "NgNspecies = " << NgNspecies << endl;
#endif

    // Where is each tracer ?
    vector<map<string, vector2r*>::const_iterator> itracer(Ntracer);
    for (int i = 0; i < Ntracer; ++i)
      itracer[i] = variable_.find(ClassTracer::GetName(i));

    vector1i tracer_z_level(Ntracer, 0);
    for (int i = 0; i < Ntracer; ++i)
      tracer_z_level[i] = variable_z_level_[itracer[i]->first];

    // Where is each source ?
    vector<map<string, vector2r*>::const_iterator> isource(Nsource);
    for (int i = 0; i < Nsource; ++i)
      isource[i] = variable_.find("number_" + ClassSource::GetName(i));

    vector1i source_z_level(Nsource, 0);
    for (int i = 0; i < Nsource; ++i)
      source_z_level[i] = variable_z_level_[isource[i]->first];

    // Where are meteo variables ?
    map<string, vector2r*>::const_iterator ikz = variable_.find("Kz");
    map<string, vector2r*>::const_iterator iz_mid = variable_.find("z_mid");
    map<string, vector2r*>::const_iterator iz_top = variable_.find("z_top");

    // Some declarations throughout the loop.
    vector1r z_delta_top(Nz), z_delta_mid(Nz);
    vector2r emission_rate_mass(Nz, vector1r(Ntracer, real(0)));
    vector2r emission_rate_number(Nz, vector1r(Nsource, real(0)));
    vector2r rate_aer_number(Nz, vector1r(Ng));
    vector2r rate_aer_mass(Nz, vector1r(NgNspecies));

    // Coefficients of matrix diffusion (tridiagonal).
    vector<double> dl(Nz - 1), d(Nz), du(Nz - 1), du2(Nz - 2),
      b(Nz * (Ngas + Ng + NgNspecies), real(0));
    vector1i ipiv(Nz);

    // Time loop.
    for (int t = 0; t < Nt_; ++t)
      {
#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
        CBUG << "t = " << t << endl;
#endif

        // Save concentrations. Write all levels.
        if (! output_file_.empty())
          for (int i = 0; i < Nz; ++i)
            AMC::ClassAerosolData::WriteConcentration(concentration_aer_num[i].data(),
                                                      concentration_aer_mass[i].data(),
                                                      concentration_gas[i].data(),
                                                      output_file_);

        // Levels top and mid, diffusion.
        const vector1r &kz = (*ikz->second)[t];
        const vector1r &z_mid = (*iz_mid->second)[t];
        const vector1r &z_top = (*iz_top->second)[t];

        // Thickness of each vertical layer.
        z_delta_top.front() = z_top.front();
        for (int i = 1; i < Nz; ++i)
          z_delta_top[i] = z_top[i] - z_top[i - 1];

        // Delta between each vertical mid points.
        z_delta_mid.front() = z_mid.front();
        for (int i = 1; i < Nz; ++i)
          z_delta_mid[i] = z_mid[i] - z_mid[i - 1];

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
        CBUG << "kz = " << kz << endl;
        CBUG << "z_delta_top = " << z_delta_top << endl;
        CBUG << "z_delta_mid = " << z_delta_mid << endl;
#endif

        // Low, mid and Up diagonal of tridiagonal matrix.
        for (int i = 0; i < Nz - 1; ++i)
          {
            real tmp = - (kz[i + 1] + kz[i]) * real(0.5) * time_step_ / z_delta_mid[i];
            dl[i] = tmp / z_delta_top[i + 1];
            du[i] = tmp / z_delta_top[i];
          }

        d.front() = real(1) - du.front();
        for (int i = 1; i < Nz - 1; ++i)
          d[i] = real(1) - (dl[i - 1] + du[i]);
        d.back() = real(1) - dl.back();

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
        CBUG << "d = " << d << endl;
        CBUG << "dl = " << dl << endl;
        CBUG << "du = " << du << endl;
#endif

        // LU decomposition for tridiagonal matrix, once for all.
        int info(0);

        dgttrf_(&Nz, dl.data(), d.data(), du.data(), du2.data(), ipiv.data(), &info);

        if (info != 0)
          throw AMC::Error("\"dgttrf\" failed with info = " + to_str(info) + ".");

        // Retrieve source number emissions.
        for (int i = 0; i < Nsource; ++i)
          for (int j = 0; j < source_z_level[i]; ++j)
            emission_rate_number[j][i] = (*isource[i]->second)[t][j] / z_delta_top[j];

        // Retrieve tracer emissions.
        for (int i = 0; i < Ntracer; ++i)
          for (int j = 0; j < tracer_z_level[i]; ++j)
            emission_rate_mass[j][i] = (*itracer[i]->second)[t][j] / z_delta_top[j];

        for (int i = 0; i < Nz; ++i)
          {
            rate_aer_number[i].assign(Ng, real(0));
            rate_aer_mass[i].assign(NgNspecies, real(0));

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
            CBUG << "emission_rate_number[" << i << "] = " << emission_rate_number[i] << endl;
            CBUG << "emission_rate_mass[" << i << "] = " << emission_rate_mass[i] << endl;
#endif

            ClassDynamicsEmission<D>::Rate(0, Ng,
                                           emission_rate_mass[i],
                                           emission_rate_number[i],
                                           rate_aer_number[i],
                                           rate_aer_mass[i]);

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
            CBUG << "rate_aer_number[" << i << "] = " << rate_aer_number[i] << endl;
            CBUG << "rate_aer_mass[" << i << "] = " << rate_aer_mass[i] << endl;
#endif
          }

        // Right term of linear system.
        int h(-1);
        for (int i = 0; i < Ngas; ++i)
          for (int j = 0; j < Nz; ++j)
            b[++h] = concentration_gas[j][i];

        for (int i = 0; i < Ng; ++i)
          for (int j = 0; j < Nz; ++j)
            b[++h] = concentration_aer_num[j][i] + time_step_ * rate_aer_number[j][i] / z_delta_top[j];

        for (int i = 0; i < NgNspecies; ++i)
          for (int j = 0; j < Nz; ++j)
            b[++h] = concentration_aer_mass[j][i] + time_step_ * rate_aer_mass[j][i] / z_delta_top[j];

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
        CBUG << "b = " << b << endl;
#endif

        // Solve system for each gas/aerosol size/species.
        const int nrhs(Ngas + Ng + NgNspecies), ldb(Nz);
        const string trans("N");

        dgttrs_((char*)trans.c_str(), &Nz, &nrhs,
                const_cast<double*>(dl.data()), const_cast<double*>(d.data()),
                const_cast<double*>(du.data()), const_cast<double*>(du2.data()),
                const_cast<int*>(ipiv.data()), b.data(), &ldb, &info);

        if (info != 0)
          throw AMC::Error("\"dgttrs\" failed with info = " + to_str(info) + ".");

        // Give back result.
        h = -1;
        for (int i = 0; i < Ngas; ++i)
          for (int j = 0; j < Nz; ++j)
            concentration_gas[j][i] = b[++h];

        for (int i = 0; i < Ng; ++i)
          for (int j = 0; j < Nz; ++j)
            concentration_aer_num[j][i] = b[++h];

        for (int i = 0; i < NgNspecies; ++i)
          for (int j = 0; j < Nz; ++j)
            concentration_aer_mass[j][i] = b[++h];

        // At last, we solve the General Aerosol Dynamics.
        for (int i = 0; i < Nz; ++i)
          AMC::ClassAMX<S, R, D, C>::Forward(concentration_aer_num[i].data(),
                                             concentration_aer_mass[i].data(),
                                             concentration_gas[i].data(),
                                             time_step_);

#ifdef DRIVER_TRAJECTORY_BASE_WITH_DEBUG
        getchar();
#endif
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << LogReset() << "Finished trajectory id n°" << id_ << " ending at "
                         << "position (" << abs(latitude_.back()) << "°" << (latitude_.back() >= real(0) ? "N" : "S")
                         << ", " << longitude_.back() << "°" << (longitude_.back() >= real(0) ? "W" : "E")
                         << ") and " << date_begin_ + time_step_ * Nt_
                         << " seconds after data beginning date (" << data_date_begin_ << ")." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << LogReset() << "Output save in file \"" << output_file_ << "\"." << endl;
#endif
  }


  // Reset trajectory data.
  void ClassDriverTrajectoryBase::Reset()
  {
    // Space position.
    latitude_.clear();
    longitude_.clear();

    // Altitude.
    altitude_.clear();

    // Variable pointers.
    variable_.clear();

    // Nullify number of time steps.
    Nt_ = 0;
    date_begin_ = real(0);
    time_step_ = real(0);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(2) << LogReset() << "Erased local data of trajectory id " << id_
                         << ", you have to call Load() again." << endl;
#endif

    // No more loaded.
    is_loaded_ = false;
  }


  // Clear static data.
  void ClassDriverTrajectoryBase::Clear()
  {
    id_counter_ = 0;

    data_date_begin_ = "";
    data_time_step_ = real(0);

    data_file_.clear();

    variable_name_.clear();
    variable_z_level_.clear();
    Nvariable_ = 0;

    variable_min_.clear();
    variable_max_.clear();

    horizontal_surface_.free();

    // Base driver clearing.
    ClassDriverDomainBase::Clear();
  }
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_BASE_CXX
#endif
