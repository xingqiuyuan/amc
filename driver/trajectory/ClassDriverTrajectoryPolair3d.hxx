// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_POLAIR3D_HXX

namespace Driver
{
  /*! 
   * \class ClassDriverTrajectoryPolair3d
   */
  class ClassDriverTrajectoryPolair3d : public ClassDriverTrajectoryBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;

  private:


  public:

    /*!< Constructor.*/
    ClassDriverTrajectoryPolair3d();

    /*!< Destructor.*/
    ~ClassDriverTrajectoryPolair3d();

    /*!< Init trajectory Polair3d.*/
    static void Init(const string &configuration_file, const string &prefix, const string type = "polair3d");

    /*!< Load data from Polair3d files.*/
    void Load(real latitude, real longitude, real t, real time_step);
  };
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_POLAIR3D_HXX
#endif
