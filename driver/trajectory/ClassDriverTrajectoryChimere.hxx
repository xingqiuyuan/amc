// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_CHIMERE_HXX

namespace Driver
{
  /*! 
   * \class ClassDriverTrajectoryChimere
   */
  class ClassDriverTrajectoryChimere : public ClassDriverTrajectoryBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  private:

    /*!< Chimere anthropic and biogenic emission variable names.*/
    static int Nemission_anthropic_;
    static int Nemission_biogenic_;
    static int Nemission_salt_;
    static vector1s emission_anthropic_name_;
    static vector1s emission_biogenic_name_;
    static vector1i emission_biogenic_index_;
    static vector1s emission_salt_name_;
    static map<string, real> emission_convert_factor_;

    /*!< Chimere meteo variable names.*/
    static int Nmeteo_2d_;
    static int Nmeteo_3d_;

    static vector1s meteo_name_2d_;
    static vector1s meteo_name_3d_;

    /*!< Vertical layers for Chimere.*/
    static Array<real, 3> vertical_layer_top_;
    static Array<real, 3> vertical_layer_mid_;

    /*!< Find vertical location in domain.*/
    static bool find_vertical_location_(const vector1r &z_top, const vector1r &z_mid,
                                        const real &z, int &k0, int &k1);

    /*!< Compute bilinear horizontal weights.*/
    static void compute_vertical_weight_(const vector1r &z_mid,
                                         const real &z, const int k0, const int k1,
                                         real &w0, real &w1);

    /*!< Interpolate arrays.*/
    static real interpolate_array_(const Array<float, 3>& array,
                                   const real &wt0, const real &wt1, const int t0, const int t1,
                                   const real &wh00, const real &wh10, const real &wh01, const real &wh11,
                                   const int i0, const int i1, const int j0, const int j1);

  public:

    /*!< Constructor.*/
    ClassDriverTrajectoryChimere();

    /*!< Destructor.*/
    ~ClassDriverTrajectoryChimere();

    /*!< Init trajectory Chimere.*/
    static void Init(const string &configuration_file, const string &prefix, string type = "chimere");

    /*!< Load data from NetCDF Chimere files.*/
    real Load(real latitude, real longitude, real t, real time_step);

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_CHIMERE_HXX
#endif
