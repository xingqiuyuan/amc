// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_CHIMERE_CXX

#include "ClassDriverTrajectoryChimere.hxx"

namespace Driver
{
  // Find vertical location in domain.
  inline bool ClassDriverTrajectoryChimere::find_vertical_location_(const vector1r &z_top, const vector1r &z_mid,
                                                                    const real &z, int &k0, int &k1)
  {
    if (z < real(0) || z >= z_top.back())
      return false;

    k0 = AMC::search_index(z_mid, z);
    k1 = (z >= z_mid[0] && z < z_mid.back()) ? k0 + 1 : k0;

    return true;    
  }


  // Compute bilinear horizontal weights.
  inline void ClassDriverTrajectoryChimere::compute_vertical_weight_(const vector1r &z_mid,
                                                                     const real &z, const int k0, const int k1,
                                                                     real &w0, real &w1)
  {
    w1 = (k1 > k0) ? (z - z_mid[k0]) / (z_mid[k1] - z_mid[k0]) : real(0);
    w0 = real(1) - w1;
  }


  // Interpolate arrays.
  inline real ClassDriverTrajectoryChimere::interpolate_array_(const Array<float, 3>& array,
                                                               const real &wt0, const real &wt1,
                                                               const int t0, const int t1,
                                                               const real &wh00, const real &wh10,
                                                               const real &wh01, const real &wh11,
                                                               const int i0, const int i1, const int j0, const int j1)
  {
    return wt0 * (wh00 * array(t0, j0, i0) + wh10 * array(t0, j0, i1) +
                  wh01 * array(t0, j1, i0) + wh11 * array(t0, j1, i1))
      + wt1 * (wh00 * array(t1, j0, i0) + wh10 * array(t1, j0, i1) +
               wh01 * array(t1, j1, i0) + wh11 * array(t1, j1, i1));
  }


  // Constructor.
  ClassDriverTrajectoryChimere::ClassDriverTrajectoryChimere()
    : ClassDriverTrajectoryBase()
  {
    return;
  }


  // Destructor.
  ClassDriverTrajectoryChimere::~ClassDriverTrajectoryChimere()
  {
    return;
  }


  // Init.
  void ClassDriverTrajectoryChimere::Init(const string &configuration_file, const string &prefix, string type)
  {
    // At first clear.
    ClassDriverTrajectoryChimere::Clear();

    // Base trajectory initialization.
    ClassDriverTrajectoryBase::PreInit(configuration_file, prefix, type);

    // Some hard coded variables.
    ClassDriverTrajectoryBase::variable_z_level_["pbl_height"] = 1;
    ClassDriverTrajectoryBase::variable_z_level_["z_top"] = 0;
    ClassDriverTrajectoryBase::variable_z_level_["z_mid"] = 0;

    ClassDriverTrajectoryBase::variable_name_.push_back("pbl_height");
    ClassDriverTrajectoryBase::variable_name_.push_back("z_top");
    ClassDriverTrajectoryBase::variable_name_.push_back("z_mid");

    // Anthropic and biogenic emissions.
    Ops::Ops ops(configuration_file);
    ops.SetPrefix(prefix + ".chimere.emission.");

    // Call list of emission tracers from AMC.
    vector1s tracer_name = ClassTracer::GetName();

    for (int i = 0; i < ClassTracer::GetNtracer(); ++i)
      if (ClassTracer::IsAnthropic(i))
        {
          Nemission_anthropic_++;
          emission_anthropic_name_.push_back(tracer_name[i]);
        }
      else
        {
          Nemission_biogenic_++;
          emission_biogenic_name_.push_back(tracer_name[i]);
          emission_biogenic_index_.push_back(ops.Get<int>("biogenic_index." + tracer_name[i]));
        }

    // Convert factor from molecule/cm2/s to µg/m2/s.
    for (int i = 0; i < ClassTracer::GetNtracer(); ++i)
      emission_convert_factor_[tracer_name[i]] = ClassTracer::GetMolarMass(i) * real(1e4) / AMC_AVOGADRO_NUMBER;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset() << "emission_convert_factor = "
                         << emission_convert_factor_ << endl;
#endif

    // Salt emission are a specific case.
    ops.Set("salt", "", emission_salt_name_);
    Nemission_salt_ = int(emission_salt_name_.size());

    if (Nemission_salt_ > 0)
      {
        Nemission_biogenic_++;
        emission_biogenic_name_.push_back(emission_salt_name_.back());
        emission_biogenic_index_.push_back(ops.Get<int>("biogenic_index." + emission_salt_name_.back()));
      }

    ops.Close();

    // Add emissions to variables.
    for (int i = 0; i < Nemission_anthropic_; ++i)
      {
        variable_z_level_[emission_anthropic_name_[i]] = 0;
        variable_name_.push_back(emission_anthropic_name_[i]);
      }

    for (int i = 0; i < Nemission_biogenic_; ++i)
      {
        variable_z_level_[emission_biogenic_name_[i]] = 1;
        variable_name_.push_back(emission_biogenic_name_[i]);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "Nemission_anthropic = " << Nemission_anthropic_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "emission_anthropic_name = " << emission_anthropic_name_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "Nemission_biogenic = " << Nemission_biogenic_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "emission_biogenic_name = " << emission_biogenic_name_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "emission_biogenic_index = " << emission_biogenic_index_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "Nemission_salt = " << Nemission_salt_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "emission_salt_name = " << emission_salt_name_ << endl;
#endif

    // Source names. For number emission, if any.
    vector1s source_name = ClassSource::GetName();

    for (int i = 0; i < ClassSource::GetNsource(); ++i)
      {
        variable_z_level_["number_" + source_name[i]] = 0;
        variable_name_.push_back("number_" + source_name[i]);
      }

    // Hardcoded meteo variables.
    meteo_name_2d_ = {"hght"};
    meteo_name_3d_ = {"winz", "winm", "airm", "temp", "sphu", "kzzz"};
    Nmeteo_2d_ = int(meteo_name_2d_.size());
    Nmeteo_3d_ = int(meteo_name_3d_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "Nmeteo_2d = " << Nmeteo_2d_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "meteo_name_2d = " << meteo_name_2d_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "Nmeteo_3d = " << Nmeteo_3d_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "meteo_name_3d = " << meteo_name_3d_ << endl;
#endif

    // Time step of CHIMERE files is always the hour.
    ClassDriverTrajectoryBase::data_time_step_ = real(3600);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug() << LogReset() << "data_time_step = "
                         << ClassDriverTrajectoryBase::data_time_step_ << endl;
#endif

    // Load meteorological file.
    NcFile fnmeteo(ClassDriverTrajectoryBase::data_file_["meteo"].c_str(), NcFile::ReadOnly);

    // CHIMERE begining date.
    {
      NcDim *DateStrLenDim = fnmeteo.get_dim("DateStrLen");
      int date_str_len = int(DateStrLenDim->size());

      string data_date_begin(date_str_len, ' ');
      NcVar *var_time = fnmeteo.get_var("Times");
      var_time->get(const_cast<char*>(data_date_begin.data()), 1, date_str_len);

      ClassDriverTrajectoryBase::data_date_begin_ = data_date_begin.substr(0, 4) +
        data_date_begin.substr(5, 2) + data_date_begin.substr(8, 2) + "00";

#ifdef AMC_WITH_LOGGER
      *AMCLogger::GetLog() << Fblue() << Debug() << LogReset() << "data_date_begin = "
                           << ClassDriverTrajectoryBase::data_date_begin_ << endl;
#endif
    }

    // Domain size.
    int &Nz = ClassDriverDomainBase::Nz_;
    int &Ny = ClassDriverDomainBase::Ny_;
    int &Nx = ClassDriverDomainBase::Nx_;

    Nz =  int(fnmeteo.get_dim("bottom_top")->size());
    Ny =  int(fnmeteo.get_dim("south_north")->size());
    Nx =  int(fnmeteo.get_dim("west_east")->size());

    // Retrieve longitude and latitudes.
    vector1r &x = ClassDriverDomainBase::x_;
    vector1r &y = ClassDriverDomainBase::y_;

    {
      Array<float, 2> tmp(Ny, Nx);
      NcVar *var = fnmeteo.get_var("lon");
      var->get(tmp.data(), Ny, Nx);
      x.resize(Nx);
      for (int i = 0; i < Nx; ++i)
        x[i] = real(tmp(0, i));
    }

    {
      Array<float, 2> tmp(Ny, Nx);
      NcVar *var = fnmeteo.get_var("lat");
      var->get(tmp.data(), Ny, Nx);
      y.resize(Ny);
      for (int i = 0; i < Ny; ++i)
        y[i] = real(tmp(i, 0));
    }

    // Space dependent vertical layers.
    {
      vertical_layer_top_.resize(Nz, Ny, Nx);
      vertical_layer_top_ = real(0);

      int Nt =  int(fnmeteo.get_dim("Time")->size());
      NcVar *var = fnmeteo.get_var("alti");
      Array<float, 3> tmp(Nz, Ny, Nx);

      for (int t = 0; t < Nt; ++t)
        {
          var->set_cur(t, 0, 0, 0);
          var->get(tmp.data(), 1, Nz, Ny, Nx);
          vertical_layer_top_ += tmp;
        }

      real Nt_inv = (Nt > 0) ? real(1) / real(Nt) : real(1);
      vertical_layer_top_ *= Nt_inv;

      vertical_layer_mid_.resize(Nz, Ny, Nx);
      vertical_layer_mid_(0, Range::all(), Range::all()) = real(0.5) * vertical_layer_top_(0, Range::all(), Range::all());
      for (int k = 1; k < Nz; ++k)
        vertical_layer_mid_(k, Range::all(), Range::all()) = real(0.5)
          * (vertical_layer_top_(k - 1, Range::all(), Range::all()) +
             vertical_layer_top_(k, Range::all(), Range::all()));
    }

    fnmeteo.close();

    // For Chimere, we compute global vertical layers but do not use them.
    vector1r &z_top = ClassDriverDomainBase::z_top_;
    vector1r &z_mid = ClassDriverDomainBase::z_mid_;

    real Nyx_inv = real(1) / real(Ny * Nx);
    z_top.resize(Nz);
    z_mid.resize(Nz);

    for (int k = 0; k < Nz; ++k)
      {
        z_top[k] = real(0);
        z_mid[k] = real(0);

        for (int j = 0; j < Ny; ++j)
          for (int i = 0; i < Nx; ++i)
            {
              z_top[k] += vertical_layer_top_(k, j, i);
              z_mid[k] += vertical_layer_mid_(k, j, i);
            }

        z_top[k] *= Nyx_inv;
        z_mid[k] *= Nyx_inv;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Debug(3) << LogReset() << "z_top = " << z_top_ << endl;
    *AMCLogger::GetLog() << Byellow() << Debug(3) << LogReset() << "z_mid = " << z_mid_ << endl;
#endif

    // Set domain from arrays.
    ClassDriverTrajectoryBase::PostInit();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "Note : z_top and z_mid are not used with CHIMERE,"
                         << " which has its own horizontal space dependent vertical grid." << endl;
#endif
  }


  // Load data from NetCDF Chimere files.
  real ClassDriverTrajectoryChimere::Load(real latitude, real longitude, real t, real time_step)
  {
    // Break if not initiated.
    if (! this->is_initiated_)
      throw AMC::Error("CHIMERE trajectory model not initiated, call Init() first.");

    this->time_step_ = time_step;
    if (this->time_step_ > this->data_time_step_)
      throw AMC::Error("Trajectory time step is > than that of data, should be <=.");

    this->date_begin_ = t;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info() << LogReset() << "Load Chimere trajectory starting at "
                         << "latitude = " << latitude << "°, longitude = " << longitude << "° and "
                         << t << " seconds after CHIMERE data beginning date, "
                         << "with a time step equal to " << time_step << " seconds." << endl;
#endif

    // Proxy to field dimensions.
    const int Nz = ClassDriverDomainBase::Nz_;
    const int Ny = ClassDriverDomainBase::Ny_;
    const int Nx = ClassDriverDomainBase::Nx_;
    const real Nz_inv = real(1) / real(Nz);

    // Load meteorological file.
    NcFile fnmeteo(ClassDriverTrajectoryBase::data_file_["meteo"].c_str(), NcFile::ReadOnly);
    NcFile fnaemission(ClassDriverTrajectoryBase::data_file_["emission_anthropic"].c_str(), NcFile::ReadOnly);
    NcFile fnbemission(ClassDriverTrajectoryBase::data_file_["emission_biogenic"].c_str(), NcFile::ReadOnly);

    int Nt_data =  int(fnmeteo.get_dim("Time")->size());
    int h_data = int(t / this->data_time_step_ + real(1.e-6));

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "Nt_data = " << Nt_data << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << LogReset() << "h_data = " << h_data << endl;
#endif

    // Time indexes with which interpolate.
    int t0(1), t1(0);

    map<string, NcVar*> ncvar_2d;
    map<string, Array<float, 3>*> array_2d;
    map<string, NcVar*> ncvar_3d;
    map<string, Array<float, 4>*> array_3d;

    for (int i = 0; i < Nmeteo_2d_; ++i)
      {
        ncvar_2d[meteo_name_2d_[i]] = fnmeteo.get_var(meteo_name_2d_[i].c_str());
        ncvar_2d[meteo_name_2d_[i]]->set_cur(h_data, 0, 0);
        array_2d[meteo_name_2d_[i]] = new Array<float, 3>(2, Ny, Nx);
        ncvar_2d[meteo_name_2d_[i]]->get((*array_2d[meteo_name_2d_[i]])
                                         (t1, Range::all(), Range::all()).data(), 1, Ny, Nx);
      }

    for (int i = 0; i < Nmeteo_3d_; ++i)
      {
        ncvar_3d[meteo_name_3d_[i]] = fnmeteo.get_var(meteo_name_3d_[i].c_str());
        ncvar_3d[meteo_name_3d_[i]]->set_cur(h_data, 0, 0, 0);
        array_3d[meteo_name_3d_[i]] = new Array<float, 4>(2, Nz, Ny, Nx);
        ncvar_3d[meteo_name_3d_[i]]->get((*array_3d[meteo_name_3d_[i]])
                                         (t1, Range::all(), Range::all(), Range::all()).data(), 1, Nz, Ny, Nx);
      }

    // Anthropic and biogenic emissions.
    for (int i = 0; i < Nemission_anthropic_; ++i)
      {
        ncvar_3d[emission_anthropic_name_[i]] = fnaemission.get_var(emission_anthropic_name_[i].c_str());
        ncvar_3d[emission_anthropic_name_[i]]->set_cur(h_data, 0, 0, 0);
        array_3d[emission_anthropic_name_[i]] = new Array<float, 4>(2, Nz, Ny, Nx);
        ncvar_3d[emission_anthropic_name_[i]]->get((*array_3d[emission_anthropic_name_[i]])
                                                   (t1, Range::all(), Range::all(), Range::all()).data(), 1, Nz, Ny, Nx);
      }

    NcVar *ncvar_emisb = fnbemission.get_var("emisb");

    for (int i = 0; i < Nemission_biogenic_; ++i)
      {
        ncvar_emisb->set_cur(h_data, 0, 0, emission_biogenic_index_[i]);
        array_2d[emission_biogenic_name_[i]] = new Array<float, 3>(2, Ny, Nx);
        ncvar_emisb->get((*array_2d[emission_biogenic_name_[i]])(t1, Range::all(), Range::all()).data(), 1, Ny, Nx, 1);
      }


    // Data time.
    const real data_time_max(Nt_data * this->data_time_step_);
    real data_t(h_data * this->data_time_step_);

    // Space indexes with which interpolate.
    int i0, i1, j0, j1;

    // Current time and bilinear horizontal and vertical weights.
    real wt0, wt1, wh00, wh01, wh10, wh11;

    // Local values for zonal and meridional winds.
    real winz_loc, winm_loc;

    // Old values for longitude and latitude.
    real longitude_old(longitude), latitude_old(latitude);

    // Is it the ETR first pass.
    bool etr_first_pass(true);

    // Resize lagrangian arrays.
    int Nt_max = int(ceil(data_time_max / this->time_step_));

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset() << "Nt_max = " << Nt_max << endl;
#endif

#ifdef DRIVER_WITH_GEO_DATA
    ClassGeoDataElevation<float> elevation;
#endif

    this->altitude_.resize(Nt_max);
    this->latitude_.resize(Nt_max);
    this->longitude_.resize(Nt_max);

    for (map<string, vector2r*>::iterator it = this->variable_.begin(); it != this->variable_.end(); it++)
      (it->second)->resize(Nt_max);

    // No number emission source at present.
    for (map<string, vector2r*>::iterator it = this->variable_.begin(); it != this->variable_.end(); it++)
      if ((it->first).find("number_") != string::npos)
        for (int i = 0; i < Nt_max; ++i)
          (*it->second)[i].assign(Nz, real(0));

    this->Nt_ = 0;
    while (t <= data_time_max)
      {
#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "t = " << t << ", data_t = " << data_t << ", data_time_max = " << data_time_max << endl;
#endif

        // Read data at instant enclosing the current time.
        if (t >= data_t)
          {
            h_data++;

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
            CBUG << "h_data = " << h_data << ", Nt_data = " << Nt_data << endl;
#endif

            if (h_data < Nt_data)
              {
                t0 = t0 == 1 ? 0 : 1;
                t1 = t1 == 1 ? 0 : 1;

                // 2d arrays.
                map<string, Array<float, 3>*>::iterator it2 = array_2d.begin();
                for (map<string, NcVar*>::iterator jt2 = ncvar_2d.begin(); jt2 != ncvar_2d.end(); ++jt2)
                  {
                    (jt2->second)->set_cur(h_data, 0, 0);
                    (jt2->second)->get((*it2->second)(t1, Range::all(), Range::all()).data(), 1, Ny, Nx);
                    it2++;
                  }

                // 3d arrays.
                map<string, Array<float, 4>*>::iterator it3 = array_3d.begin();
                for (map<string, NcVar*>::iterator jt3 = ncvar_3d.begin(); jt3 != ncvar_3d.end(); ++jt3)
                  {
                    (jt3->second)->set_cur(h_data, 0, 0, 0);
                    (jt3->second)->get((*it3->second)(t1, Range::all(), Range::all(), Range::all()).data(), 1, Nz, Ny, Nx);
                    it3++;
                  }

                // Biogenic emissions.
                for (int i = 0; i < Nemission_biogenic_; ++i)
                  {
                    ncvar_emisb->set_cur(h_data, 0, 0, emission_biogenic_index_[i]);
                    ncvar_emisb->get((*array_2d[emission_biogenic_name_[i]])(t1, Range::all(), Range::all()).data(), 1, Ny, Nx, 1);
                  }

                // Special case for salt emissions, some tracers are just fractions.
                for (int i = 0; i < Nemission_salt_; ++i)
                  (*array_2d[emission_salt_name_[i]])(t1, Range::all(), Range::all()) *=
                    (*array_2d[emission_salt_name_.back()])(t1, Range::all(), Range::all());
              }

            data_t += this->data_time_step_;
          }

        // Time weights.
        wt0 = (data_t - t) / this->data_time_step_;
        wt1 = real(1) - wt0;

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "wt0 = " << wt0 << ", wt1 = " << wt1 << endl;
#endif

        // Find our horizontal location. Exit loop if definitly out of domain.
        if (! ClassDriverTrajectoryBase::find_horizontal_location_(latitude, longitude, j0, j1, i0, i1))
          break;

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "j0 = " << j0 << ", j1 = " << j1 << ", i0 = " << i0 << ", i1 = " << i1 << endl;
#endif
        // Horizontal bilinear weighting coefficients.
        ClassDriverTrajectoryBase::compute_bilinear_horizontal_weight_(latitude, longitude,
                                                                       j0, j1, i0, i1,
                                                                       wh00, wh01, wh10, wh11);

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "wh00 = " << wh00 << ", wh01 = " << wh01 << ", wh10 = " << wh10 << ", wh11 = " << wh11 << endl;
#endif

        // If first pass, record trajectory.
        if (etr_first_pass)
          {
            for (map<string, vector2r*>::iterator it = this->variable_.begin(); it != this->variable_.end(); ++it)
              (*it->second)[this->Nt_].resize(this->variable_z_level_[it->first]);

            // Compute current layer altitude.
            for (int k = 0; k < Nz; ++k)
              (*this->variable_["z_top"])[this->Nt_][k] =
                wh00 * vertical_layer_top_(k, j0, i0)
                + wh01 * vertical_layer_top_(k, j1, i0) +
                + wh10 * vertical_layer_top_(k, j0, i1) +
                + wh11 * vertical_layer_top_(k, j1, i1);

            (*this->variable_["z_mid"])[this->Nt_][0] = real(0.5) * (*this->variable_["z_top"])[this->Nt_][0];
            for (int k = 1; k < Nz; ++k)
              (*this->variable_["z_mid"])[this->Nt_][k] = real(0.5)
                * ((*this->variable_["z_top"])[this->Nt_][k - 1] + (*this->variable_["z_top"])[this->Nt_][k]);

            this->latitude_[this->Nt_] = latitude;
            this->longitude_[this->Nt_] = longitude;

            real pbl_height = interpolate_array_(*array_2d["hght"], wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);
            (*this->variable_["pbl_height"])[this->Nt_].front() = pbl_height;

            // Biogenic emissions.
            for (int l = 0; l < Nemission_biogenic_; ++l)
              (*this->variable_[emission_biogenic_name_[l]])[this->Nt_].front() =
                emission_convert_factor_[emission_biogenic_name_[l]] *
                interpolate_array_(*array_2d[emission_biogenic_name_[l]], wt0, wt1, t0, t1,
                                   wh00, wh10, wh01, wh11, i0, i1, j0, j1);

            for (int k = 0; k < Nz; ++k)
              {
                // Anthropic emissions.
                for (int l = 0; l < Nemission_anthropic_; ++l)
                  (*this->variable_[emission_anthropic_name_[l]])[this->Nt_][k] =
                    emission_convert_factor_[emission_anthropic_name_[l]] *
                    interpolate_array_((*array_3d[emission_anthropic_name_[l]])(Range::all(), k, Range::all(), Range::all()),
                                       wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);

                // Temperature in Kelvin.
                real temp_tmp = interpolate_array_((*array_3d["temp"])(Range::all(), k, Range::all(), Range::all()),
                                                   wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);

                // Air density in molecule per cm3.
                real airm_tmp = interpolate_array_((*array_3d["airm"])(Range::all(), k, Range::all(), Range::all()),
                                                   wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);

                // Specific humidity kg/kg.
                real sphu_tmp = interpolate_array_((*array_3d["sphu"])(Range::all(), k, Range::all(), Range::all()),
                                                   wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);

                // Kz m2.s^{-1}.
                real kz_tmp = interpolate_array_((*array_3d["kzzz"])(Range::all(), k, Range::all(), Range::all()),
                                                 wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);

                (*this->variable_["temperature"])[this->Nt_][k] = temp_tmp;
                (*this->variable_["Kz"])[this->Nt_][k] = kz_tmp;

                // Pressure according to perfect gas law in Pascal. 1e6 convert molec.cm^{-3} to molec.m^{-3}
                real pres_tmp = AMC_BOLTZMANN_CONSTANT * airm_tmp * temp_tmp * real(1e6);
                (*this->variable_["pressure"])[this->Nt_][k] = pres_tmp;

                (*this->variable_["relative_humidity"])[this->Nt_][k] = Driver::ClassParameterizationPhysics::
                  ComputeRelativeHumidityFromSpecificHumidity(temp_tmp, pres_tmp, sphu_tmp);
              }

            // Limit diffusion to below PBL.
            vector1r &kz = (*this->variable_["Kz"])[this->Nt_];
            vector1r &z_top = (*this->variable_["z_top"])[this->Nt_];

            for (int k = 0; k < Nz; ++k)
              if (z_top[k] > pbl_height)
                {
                  for (int l = k; l < Nz; ++l)
                    kz[l] = real(0); 
                  break;
                }

#ifdef DRIVER_WITH_GEO_DATA
            this->altitude_[this->Nt_] = real(elevation.GetValue(latitude, longitude));
#else
            this->altitude_[this->Nt_] = Driver::ClassParameterizationPhysics::
              ComputeAltitudeFromAtmosphericPressure(this->pressure_[this->Nt_][0]);
#endif
            this->Nt_++;
          }

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "longitude = " << longitude << ", latitude = " << latitude << ", Nt = " << this->Nt_ << endl;
        // Wait until any keyboard key.
        getchar();
#endif

        // Wind.
        real winz_tmp(real(0)), winm_tmp(real(0));
        for (int k = 0; k < Nz; ++k)
          {
            winz_tmp += interpolate_array_((*array_3d["winz"])(Range::all(), k, Range::all(), Range::all()),
                                           wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);
            winm_tmp += interpolate_array_((*array_3d["winm"])(Range::all(), k, Range::all(), Range::all()),
                                           wt0, wt1, t0, t1, wh00, wh10, wh01, wh11, i0, i1, j0, j1);
          }

        winz_tmp *= Nz_inv;
        winm_tmp *= Nz_inv;

        if (etr_first_pass)
          {
            winz_loc = winz_tmp;
            winm_loc = winm_tmp;
            longitude_old = longitude;
            latitude_old = latitude;
            t += this->time_step_;
          }
        else
          {
            winz_loc += winz_tmp;
            winm_loc += winm_tmp;
            winz_loc *= real(0.5);
            winm_loc *= real(0.5);
            longitude = longitude_old;
            latitude = latitude_old;
          }

#ifdef DRIVER_TRAJECTORY_CHIMERE_WITH_DEBUG
        CBUG << "etr_first_pass = " << (etr_first_pass ? "yes" : "no") << endl;
        CBUG << "winz = " << winz_loc << ", winm = " << winm_loc << endl;
#endif

        longitude += winz_loc * this->time_step_ * DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER_INV
          / cos(latitude_old * DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_DEGREE_TO_RADIUS)
          * DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_RADIUS_TO_DEGREE;
        latitude -= winm_loc * this->time_step_ * DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER_INV
          * DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_RADIUS_TO_DEGREE;

        // Keep latitude within [-90, 90] and longitude within [-180, 180].
        ClassDriverTrajectoryBase::adjust_latitude_longitude_(latitude, longitude);

        // Toggle ETR flag.
        etr_first_pass = etr_first_pass == false;
      }

    // Clear local variables.
    ncvar_2d.clear();
    array_2d.clear();
    ncvar_3d.clear();
    array_3d.clear();

    // Once time loop done, there reamain to convert into proper units.

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info() << LogReset() << "Ended Chimere trajectory at "
                         << "latitude = " << latitude << "°, longitude = "
                         << longitude << "° and " << t << " seconds after first Chimere date (Nt = "
                         << this->Nt_ << ")." << endl;
#endif

    // Base work.
    ClassDriverTrajectoryBase::Load();

    // Return the last time we reached before going out of domain.
    return t;
  }


  // Clear static data.
  void ClassDriverTrajectoryChimere::Clear()
  {
    // Just return if not initiated.
    if (! ClassDriverBase::is_initiated_)
      return;

    Nmeteo_2d_ = 0;
    Nmeteo_3d_ = 0;
    meteo_name_2d_.clear();
    meteo_name_3d_.clear();
    Nemission_anthropic_ = 0;
    Nemission_biogenic_ = 0;
    Nemission_salt_ = 0;
    emission_anthropic_name_.clear();
    emission_biogenic_name_.clear();
    emission_salt_name_.clear();
    emission_biogenic_index_.clear();
    vertical_layer_top_.free();
    vertical_layer_mid_.free();
    emission_convert_factor_.clear();

    ClassDriverTrajectoryBase::Clear();
  }
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_CHIMERE_CXX
#endif
