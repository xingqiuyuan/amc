// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_POLAIR3D_CXX

#include "ClassDriverTrajectoryPolair3d.hxx"

namespace Driver
{
  // Constructor.
  ClassDriverTrajectoryPolair3d::ClassDriverTrajectoryPolair3d()
    : ClassDriverTrajectoryBase()
  {
    return;
  }


  // Destructor.
  ClassDriverTrajectoryPolair3d::~ClassDriverTrajectoryPolair3d()
  {
    return;
  }


  // Init.
  void ClassDriverTrajectoryPolair3d::Init(const string &configuration_file, const string &prefix, const string type)
  {
    // Base trajectory initialization.
    ClassDriverTrajectoryBase::PreInit(configuration_file, prefix, type);
  }


  // Load data from Polair3d files.
  void  ClassDriverTrajectoryPolair3d::Load(real latitude, real longitude, real t, real time_step)
  {
  }
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_POLAIR3D_CXX
#endif
