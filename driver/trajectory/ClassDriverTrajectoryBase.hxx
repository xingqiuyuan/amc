// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DRIVER_TRAJECTORY_BASE_HXX

// Physical constants.
#define DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER                   6378000.0
#define DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER_SQUARE            4.0678884e+13
#define DRIVER_TRAJECTORY_BASE_EARTH_RADIUS_METER_INV               1.5678896205707118e-07
#define DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_RADIUS_TO_DEGREE  57.29577951308232
#define DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_DEGREE_TO_RADIUS  0.017453292519943295
#define DRIVER_TRAJECTORY_BASE_CONVERT_ANGLE_FROM_DEGREE_TO_RADIUS_SQUARE  3.04617419786709e-04


namespace Driver
{
  /*! 
   * \class ClassDriverTrajectoryBase
   */
  class ClassDriverTrajectoryBase : public ClassDriverDomainBase
  {
  public:

    typedef Driver::real real;
    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Is trajectory loaded.*/
    bool is_loaded_;

    /*!< Trajectory id.*/
    int id_;

    /*!< Number of time steps in trajectory.*/
    int Nt_;

    /*!< Trajectory beginning date.*/
    real date_begin_;

    /*!< Trajectory time step.*/
    real time_step_;

    /*!< Altitude above sea level.*/
    vector1r altitude_;

    /*!< Horizontal space position.*/
    vector1r latitude_, longitude_;

    /*!< Map of variables.*/
    map<string, vector2r*> variable_;

    /*!< Output file for concentrations.*/
    string output_file_;

    /*!< Trajectory id counter.*/
    static int id_counter_;

    /*!< Number of variables.*/
    static int Nvariable_;

    /*!< Data beginning date.*/
    static string data_date_begin_;

    /*!< Data time step.*/
    static real data_time_step_;

    /*!< Data files (meteo, emissions, gas concentrations, ....*/
    static map<string, string> data_file_;

    /*!< List of variable names.*/
    static vector1s variable_name_;

    /*!< List of variable names.*/
    static map<string, int> variable_z_level_;

    /*!< Min and max values for data.*/
    static map<string, real> variable_min_;
    static map<string, real> variable_max_;

    /*!< Horizontal surface of each cell in m2.*/
    static Array<real, 2> horizontal_surface_;

    /*!< Adjust latitude and longitude.*/
    static void adjust_latitude_longitude_(real &latitude, real &longitude);

    /*!< Filter variables.*/
    void apply_filter_variable_(const string &name);

    /*!< Filter variables.*/
    static void set_filter_variable_(Ops::Ops &ops);

  public:

    /*!< Constructor.*/
    ClassDriverTrajectoryBase();

    /*!< Destructor.*/
    virtual ~ClassDriverTrajectoryBase();

#ifndef SWIG
    /*!< Init trajectory base.*/
    static void PreInit(const string &configuration_file, const string &prefix, string type);
    static void PostInit();
#endif

    /*!< Load data, common work.*/
    virtual void Load();

    /*!< Get methods.*/
    int GetId() const;
    int GetNt() const;
    void GetVariable(const string name, vector<vector<real> > &variable) const;
    void GetLatitude(vector<real> &latitude) const;
    void GetLongitude(vector<real> &longitude) const;
    void GetAltitude(vector<real> &altitude) const;
    real GetTimeStep() const;
    real GetDateBegin() const;
    string GetOutputFile() const;

    bool ReadConcentration(const int t, const int z,
                           vector<real> &concentration_aer_num,
                           vector<real> &concentration_aer_mass,
                           vector<real> &concentration_gas) const;

    static int GetIdCounter();
    static int GetNvariable();
    static real GetDataTimeStep();
    static string GetDataDateBegin();
    static vector<string> GetVariableName();
    static int GetVariableLevelZ(const string name);
    static map<string, string> GetDataFile();

    /*!< Set methods.*/
    void SetOutputFile(const string output_file);


    /*!< Run from one given space position.*/
    template<class S, class R, class D, class C>
    void Run(vector<vector<real> > &concentration_aer_num,
             vector<vector<real> > &concentration_aer_mass,
             vector<vector<real> > &concentration_gas) const;

    /*!< Reset trajectory.*/
    void Reset();

    /*!< Clear static data.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_DRIVER_TRAJECTORY_BASE_HXX
#endif
