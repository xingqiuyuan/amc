// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_DYNAMICS_EMISSION_CXX

#include "ClassDynamicsEmission.hxx"

namespace Driver
{
  // Rate of a given source.
  template<class D>
  inline void ClassDynamicsEmission<D>::rate(const int i,
                                             const vector1r &emission_rate_mass,
                                             vector1r &rate_aer_number,
                                             vector1r &rate_aer_mass,
                                             real *emission_rate_number,
                                             int section_min, int section_max)
  {
#ifdef DRIVER_WITH_DEBUG_EMISSION
    CBUG << "ClassSource::name = " << ClassSource::name_[i] << endl;
#endif

    // Determine source composition.
    composition_work_.assign(AMC::ClassDiscretization<D>::Nspecies_, real(0));

    for (int j = 0; j < ClassSource::Ntracer_[i]; ++j)
      {
        const int k = ClassSource::tracer_index_[i][j];

#ifdef DRIVER_WITH_DEBUG_EMISSION
        CBUG << "ClassTracer::name = " << ClassTracer::name_[k] << endl;
#endif

        for (int l = 0; l < ClassTracer::Nspecies_[k]; ++l)
          composition_work_[ClassTracer::species_index_[k][l]] +=
            emission_rate_mass[k] * ClassTracer::species_mass_fraction_[k][l];
      }

    // Total mass emitted.
    real emission_mass_total(real(0));
    for (int k = 0; k < AMC::ClassDiscretization<D>::Nspecies_; ++k)
      emission_mass_total += composition_work_[k];

#ifdef DRIVER_WITH_DEBUG_EMISSION
    CBUG << "emission_mass_total = " << emission_mass_total << endl;
#endif

    // Of course ...
    if (emission_mass_total > real(0))
      {
        const real emission_mass_total_inv = real(1) / emission_mass_total;
        for (int j = 0; j < AMC::ClassDiscretization<D>::Nspecies_; ++j)
          composition_work_[j] *= emission_mass_total_inv;

#ifdef DRIVER_WITH_DEBUG_EMISSION
        CBUG << "composition = " << composition_work_ << endl;
#endif

        // The source composition is not size section dependent.
        for (int j = 0; j < ClassSource::Nsize_[i]; ++j)
          {
#ifdef DRIVER_WITH_DEBUG_EMISSION
            CBUG << "ClassSource::size_index = " << ClassSource::size_index_[i][j] << endl;
            CBUG << "ClassSource::size_fraction_mass = " << ClassSource::size_fraction_mass_[i][j] << endl;
#endif
            // Real size section index.
            const int s = ClassSource::size_index_[i][j];

            // Emission mass in this size section.
            const real emission_mass = emission_mass_total * ClassSource::size_fraction_mass_[i][j];

            // The general section index for emission.
            int g(-1);

            // Whether the source has a relatively fixed composition or we deal only with internal
            // mixing, in both cases there is no need to find the class composition at run time.
            if (source_general_section_[i][j] < 0)
              {
                // The class composition index in this section, 0 if internal mixing.
                bool on_edge(false);
                const int c = AMC::ClassDiscretizationClass<D>::discretization_composition_[s]->
                  FindClassCompositionIndex(composition_work_, on_edge);

#ifdef DRIVER_WITH_DEBUG_EMISSION
                CBUG << "s = " << s << endl;
                CBUG << "c = " << c << endl;
#endif

                g = AMC::ClassDiscretization<D>::general_section_reverse_[s][c];

                if (source_count_general_section_) source_general_section_count_(i, s, g)++;
              }
            else
              g = source_general_section_[i][j];

#ifdef DRIVER_WITH_DEBUG_EMISSION
            CBUG << "g = " << g << endl;
#endif

            // This test ensures no trouble ahead, provided section_min and section_max are correct.
            if (g >= section_min && g < section_max)
              {
                // If number emissions are provided, use them, otherwise recompute them from mass.
                if (emission_rate_number != NULL && *emission_rate_number > real(0))
                  rate_aer_number[g] += *emission_rate_number * ClassSource::size_fraction_number_[i][j];
                else
                  {
                    // Inverse of mean mass : either from fixed density or depending on composition.
                    real size_mass_mean_inv;
                    if (ClassSource::use_density_fixed_[i])
                      size_mass_mean_inv = ClassSource::size_mass_mean_inv_[i][j];
                    else
                      {
                        real density_inv(real(0));
                        for (int k = 0; k < AMC::ClassDiscretization<D>::Nspecies_; ++k)
                          density_inv += composition_work_[k] / AMC::ClassSpecies::density_[k];
                        size_mass_mean_inv = size_volume_mean_inv_[i][j] * density_inv;

#ifdef DRIVER_WITH_DEBUG_EMISSION
                        CBUG << "density = " << real(1) / density_inv << endl;
#endif
                      }

                    const real emission_number = emission_mass * size_mass_mean_inv;

#ifdef DRIVER_WITH_DEBUG_EMISSION
                    CBUG << "size_mass_mean = " << real(1) / size_mass_mean_inv << endl;
                    CBUG << "emission_number = " << emission_number << endl;
#endif

                    rate_aer_number[g] += emission_number;
                    if (emission_rate_number != NULL) *emission_rate_number += emission_number;
                  }

                int k = g * AMC::ClassDiscretization<D>::Nspecies_ - 1;
                for (int l = 0; l < AMC::ClassDiscretization<D>::Nspecies_; ++l)
                  rate_aer_mass[++k] += composition_work_[l] * emission_mass;
              }
          }
      }
  }


  // Init static data.
  template<class D>
  void ClassDynamicsEmission<D>::Init(Ops::Ops &ops)
  {
    const string prefix_orig = ops.GetPrefix();

    // Clear everything.
    ClassDynamicsEmission<D>::Clear();

    if (! ops.Exists("emission"))
      throw AMC::Error("No emission section found within prefix \"" + ops.GetPrefix() +"\".");

    string prefix_emission = prefix_orig + "emission.";
    if (ops.Is<string>("emission"))
      prefix_emission = ops.Get<string>("emission") + ".";

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Info() << LogReset()
                         << "Init emission subsystem from section \"" << prefix_emission << "\"." << endl;
#endif

    ops.SetPrefix(prefix_emission + "tracer.");
    ClassTracer::Init(ops);

    ops.SetPrefix(prefix_emission + "source.");
    ClassSource::Init(ops);

    //
    // General section index for each source as a function of size.
    //

    // Default (< 0) is to compute the class composition at run time.
    source_general_section_.resize(ClassSource::Nsource_);
    for (int i = 0; i < ClassSource::Nsource_; ++i)
      source_general_section_[i].assign(ClassSource::Nsize_[i], -1);

    // Eventually read the general section index for each source from configuration file.
    if (ops.Exists("class_composition"))
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Byellow() << Info() << LogReset()
                             << "Found one \"class_composition\" section in \""
                             << ops.GetPrefix() << "\". Use it to determine in which"
                             << " general section to put source resolved emissions." << endl;
        *AMCLogger::GetLog() << Bred() << Warning() << LogReset()
                             << "If empty, general sections will be determined at run time, "
                             << "this may imply a slight additional CPU cost, depending on "
                             << "your configuration." << endl;
#endif

        ops.SetPrefix(prefix_emission + "source.class_composition.");

        for (int i = 0; i < ClassSource::Nsource_; ++i)
          if (ops.Exists(ClassSource::name_[i]))
            {
              ops.SetPrefix(prefix_emission + "source.class_composition." + ClassSource::name_[i] + ".");

#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset()
                                   << "Manage emission source \"" << ClassSource::name_[i] << "\" :" << endl;
#endif

              // Default values.
              const string class_name_default = ops.Get<string>("default", "", "");

#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Fcyan() << Info(2) << LogReset()
                                   << "\tdefault class name for all size sections is \""
                                   << class_name_default << "\". Can be overriden." << endl;
#endif

              // Size dependence.
              for (int j = 0; j < ClassSource::Nsize_[i]; ++j)
                {
                  // The AMC size section index.
                  const int size_section = ClassSource::size_index_[i][j];

#ifdef AMC_WITH_LOGGER
                  *AMCLogger::GetLog() << Fmagenta() << Debug(1) << LogReset()
                                       << "\t\tsize_section = " << size_section << endl;
#endif

                  int class_index(-1);
                  string class_name(class_name_default);

                  // If internal mixing, no need to proceed further.
                  if (AMC::ClassDiscretizationClass<D>::Nclass_[size_section] > 1)
                    {
                      if (ops.Exists("S" + to_str(size_section)))
                        {
                          if (ops.Is<string>("S" + to_str(size_section)))
                            class_name = ops.Get<string>("S" + to_str(size_section));
                          else if (ops.Is<int>("S" + to_str(size_section)))
                            class_index = ops.Get<int>("S" + to_str(size_section), " >= 0 ");
                          else
                            throw AMC::Error("Key \"S" + to_str(size_section) + "\" in section \"" +
                                             ops.GetPrefix() + "\" is neither a string nor an integer.");
                        }

#ifdef AMC_WITH_LOGGER
                      *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset()
                                           << "\t\tclass_index = " << class_index << endl;
                      *AMCLogger::GetLog() << Fgreen() << Debug(2) << LogReset()
                                           << "\t\tclass_name = " << class_name << endl;
#endif

                      // If class index was not provided but its name was given.
                      if (class_index < 0 && ! class_name.empty())
                        for (int k = 0; k < AMC::ClassDiscretizationClass<D>::Nclass_[size_section]; ++k)
                          {
                            const string name = AMC::ClassDiscretizationClass<D>::
                              discretization_composition_[size_section]->GetClassInternalMixing(k)->GetName();

                            if (name.find(class_name) != string::npos)
                              {
                                class_index = k;
                                break;
                              }
                          }
                    }
                  else
                    class_index = 0;

#ifdef AMC_WITH_LOGGER
                  *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset()
                                       << "\t\tclass_index = " << class_index << endl;
#endif

                  // At last, the desired general section index.
                  if (class_index >= 0)
                    source_general_section_[i][j] =
                      AMC::ClassDiscretization<D>::general_section_reverse_[i][class_index];
                }

#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Byellow() << Debug(1) << LogReset() << "\tsource_general_section = "
                                   << source_general_section_[i] << endl;
#endif
            }
      }
#ifdef AMC_WITH_LOGGER
    else
      *AMCLogger::GetLog() << Bred() << Warning() << LogReset()
                           << "Could not find one \"class_composition\" section in \""
                           << ops.GetPrefix() << "\". General sections will be determined "
                           << "at run time, this may imply a slight additional CPU cost, "
                           << "depending on your configuration." << endl;
#endif

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < ClassSource::Nsource_; ++i)
      {
        *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Emissions source \""
                             << ClassSource::name_[i] << "\" :" << endl;
        for (int j = 0; j < ClassSource::Nsize_[i]; ++j)
          {
            const int size_section = ClassSource::size_index_[i][j];

            *AMCLogger::GetLog() << Fblue() << Info(1) << LogReset() << "\tat size section "
                                 << size_section << " will go in AMC general section : " << endl;

            if (source_general_section_[i][j] < 0)
              *AMCLogger::GetLog() << Fred() << Info(1) << LogReset() << "\t\t\"computed at run time\"." << endl;
            else
              {
                const int class_index = AMC::ClassDiscretization<D>::
                  general_section_[source_general_section_[i][j]][1];
                const string class_name = AMC::ClassDiscretizationClass<D>::
                  discretization_composition_[size_section]->GetClassInternalMixing(class_index)->GetName();

                *AMCLogger::GetLog() << Fgreen() << Info(1) << LogReset() << "\t\tnumber "
                                     << source_general_section_[i][j] << ", which is the class composition \""
                                     << class_name << "\" (" << class_index << ")." << endl;
              }
          }
      }
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << User(0, "ADVICE")
                         << "Most of the time, general sections for each source emission "
                         << "can be precomputed because the sources chemical compositions "
                         << "remain relatively constant over time and space." << LogReset().Str() << endl;
#endif

    // Allocate working arrays.
    composition_work_.assign(AMC::ClassDiscretization<D>::Nspecies_, real(0));

    // Allocate source general section counter.
    ops.SetPrefix(prefix_emission + "source.");
    if ((source_count_general_section_ =
         ops.Get<bool>("count_general_section", "", source_count_general_section_)))
      {
        source_general_section_count_(ClassSource::Nsource_,
                                      AMC::ClassDiscretizationSize::Nsection_,
                                      AMC::ClassDiscretization<D>::Ng_);
        source_general_section_count_ = 0;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bmagenta() << Info(1) << LogReset()
                             << "Will count the general section for each emission source. This "
                             << "applies only to those which have to be determined at run time." << endl;
#endif
      }

    // Give back old position.
    ops.SetPrefix(prefix_orig);
  }


  // Clear static data.
  template<class D>
  void ClassDynamicsEmission<D>::Clear()
  {
    if (source_count_general_section_)
      {
        source_general_section_count_.free();
        source_count_general_section_ = false;
      }

    composition_work_.clear();
    source_general_section_.clear();
    ClassSource::Clear();
    ClassTracer::Clear();
  }


  // Get methods.
  template<class D>
  void ClassDynamicsEmission<D>::GetSourceGeneralSectionIndex(vector<vector<int> > &source_general_section)
  {
    source_general_section = source_general_section_;
  }

  template<class D>
  void ClassDynamicsEmission<D>::
  GetSourceGeneralSectionCount(vector<vector<int> > &source_general_section_count)
  {
    if (! source_count_general_section_)
      return;

    source_general_section_count.clear();

    for (int i = 0; i < ClassSource::Nsource_; ++i)
      for (int j = 0; j < ClassSource::Nsize_[i]; ++j)
        {
          const int s = ClassSource::size_index_[i][j];

          source_general_section_count.push_back({i, s});
          for (int g = 0; g < AMC::ClassDiscretization<D>::Ng_; ++g)
            source_general_section_count.back().push_back(source_general_section_count_(i, s, g));
        }
  }


  // Set methods.
  template<class D>
  void ClassDynamicsEmission<D>::ToggleSourceCountGeneralSection()
  {
    source_count_general_section_ = ! source_count_general_section_;
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(1) << LogReset()
                         << "Toggle count of source general section, it is now ["
                         << Bred().Str() << (source_count_general_section_ ? "ON" : "OFF")
                         << LogReset().Str() << "]" << endl;
#endif
  }

  // Compute rate for given general sections.
  template<class D>
  void ClassDynamicsEmission<D>::Rate(const int section_min,
                                      const int section_max,
                                      const vector<real> &emission_rate_mass,
                                      vector<real> &emission_rate_number,
                                      vector<real> &rate_aer_number,
                                      vector<real> &rate_aer_mass)
  {
    // Treat emissions on a per source basis.
    for (int i = 0; i < ClassSource::Nsource_; ++i)
      rate(i,
           emission_rate_mass,
           rate_aer_number,
           rate_aer_mass,
           emission_rate_number.data() + i,
           section_min, section_max);
  }
}

#define DRIVER_FILE_CLASS_DYNAMICS_EMISSION_CXX
#endif
