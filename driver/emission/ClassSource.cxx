// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_SOURCE_CXX

#include "ClassSource.hxx"

namespace Driver
{
  // Init static data.
  void ClassSource::Init(Ops::Ops &ops)
  {
    string prefix_orig = ops.GetPrefix();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << LogReset() << "Instantiate sources for emission"
                         << " from configuration section \"" << prefix_orig << "\"." << endl;
#endif

    // Source threshold percentage.
    threshold_percentage_size_ = ops.Get<real>("threshold_percentage_size", "",
                                               DRIVER_SOURCE_THRESHOLD_PERCENTAGE_SIZE_DEFAULT);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug() << LogReset() << "threshold_percentage_size = "
                         << threshold_percentage_size_ << endl;
#endif

    //
    // Source.
    //

    { // Anthropic.
      vector1s source_name = ops.GetEntryList("anthropic");
      Nanthropic_ = int(source_name.size());

      for (int i = 0; i < Nanthropic_; ++i)
        {
          name_.push_back(source_name[i]);
          is_anthropic_.push_back(true);
        }
    }

    { // Biogenic.
      vector1s source_name = ops.GetEntryList("biogenic");
      Nbiogenic_ = int(source_name.size());

      for (int i = 0; i < Nbiogenic_; ++i)
      {
        name_.push_back(source_name[i]);
        is_anthropic_.push_back(false);
      }
    }

    Nsource_ = int(name_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info(3) << LogReset() << "Nsource = " << Nsource_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(3) << LogReset() << "Nanthropic = " << Nanthropic_ << endl;
    *AMCLogger::GetLog() << Bcyan() << Info(3) << LogReset() << "Nbiogenic = " << Nbiogenic_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "name = " << name_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "is_anthropic = " << is_anthropic_ << endl;
#endif

    //
    // Read source data.
    //

    Ntracer_.assign(Nsource_, 0);
    tracer_index_.resize(Nsource_);

    Nsize_.assign(Nsource_, 0);
    size_index_.resize(Nsource_);
    size_diameter_mean_.resize(Nsource_);
    size_fraction_number_.resize(Nsource_);
    size_fraction_mass_.resize(Nsource_);
    size_mass_mean_inv_.resize(Nsource_);
    size_volume_mean_inv_.resize(Nsource_);

    density_fixed_.assign(Nsource_, real(-1));
    use_density_fixed_.assign(Nsource_, false);

    const int Nsection_amc = AMC::ClassDiscretizationSize::GetNsection();
    const vector1r diameter_bound_amc = AMC::ClassDiscretizationSize::GetDiameterBound();

    for (int i = 0; i < Nsource_; ++i)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bcyan() << Info(1) << LogReset() << "Source \"" << name_[i] << "\" :" << endl;
#endif

        if (is_anthropic_[i])
          ops.SetPrefix(prefix_orig + "anthropic." + name_[i] + ".");
        else
          ops.SetPrefix(prefix_orig + "biogenic." + name_[i] + ".");

        // Whether to use or not fixed density.
        if (use_density_fixed_[i] = ops.Exists("density_fixed"))
          density_fixed_[i] = ops.Get<real>("density_fixed") * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Byellow() << Debug(2) << LogReset() << "\tuse_density_fixed = "
                             << (use_density_fixed_[i] ? "yes" : "no") << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(2) << LogReset() << "\tdensity_fixed = "
                             << density_fixed_[i] << endl;
#endif

        vector1s source_tracer;
        ops.Set("tracer", "", source_tracer);

        Ntracer_[i] = int(source_tracer.size());
        tracer_index_[i].resize(Ntracer_[i]);

        for (int j = 0; j < Ntracer_[i]; ++j)
          {
            bool found(false);
            for (int k = 0; k < ClassTracer::Ntracer_; ++k)
              if (source_tracer[j] == ClassTracer::name_[k])
                {
                  tracer_index_[i][j] = k;

                  if ((ClassSource::is_anthropic_[i] && (! ClassTracer::is_anthropic_[k])) ||
                      ((! ClassSource::is_anthropic_[i]) && ClassTracer::is_anthropic_[k]))
                    throw AMC::Error("Inconsistent anthropic/biogenic origin between source \"" +
                                     name_[i] + "\" and its tracer \"" + ClassTracer::name_[k] + "\".");

                  found = true;
                  break;
                }

            if (! found)
              throw AMC::Error("Unable to find \"" + source_tracer[j] +
                               "\" in tracer list for source \"" + name_[i] + "\".");
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Byellow() << Debug(1) << LogReset() << "\tNtracer = " << Ntracer_[i] << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << LogReset()
                             << "\ttracer_index = " << tracer_index_[i] << endl;
        *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "\tis_anthropic ? "
                             << (ClassSource::is_anthropic_[i] ? "YES" : "NO") << endl;
#endif

        ops.SetPrefix(prefix_orig + "list." + name_[i] + ".size.");

        if (ops.Exists("modal") || ops.Exists("monahan"))
          {
            vector<real> diameter_mean;
            vector<real> distribution_number;
            vector<real> distribution_volume;

            if (ops.Exists("modal"))
              {
                if (ops.Is<string>("modal"))
                  ops.SetPrefix(ops.Get<string>("modal") + ".");
                else
                  ops.SetPrefix(prefix_orig + "list." + name_[i] + ".size.modal.");

                ClassModalDistribution<real> modal(ops);

                modal.ComputeSectionalDistribution(diameter_bound_amc,
                                                   diameter_mean,
                                                   distribution_number,
                                                   distribution_volume);
              }
            else if (ops.Exists("monahan"))
              {
                // Fake 10m wind speed as we only need ratios.
                real wind_speed_10m = real(1);

                ops.SetPrefix(prefix_orig + "list." + name_[i] + ".size.monahan.");
                ClassParameterizationEmissionSeaSaltMonahan monahan(ops);

                monahan.ComputeSectionalDistribution(diameter_bound_amc,
                                                     wind_speed_10m,
                                                     diameter_mean,
                                                     distribution_number,
                                                     distribution_volume);
              }

            real distribution_number_total(real(0));
            real distribution_volume_total(real(0));
            for (int j = 0; j < Nsection_amc; ++j)
              {
                distribution_number_total += distribution_number[j];
                distribution_volume_total += distribution_volume[j];
              }

            for (int j = 0; j < Nsection_amc; ++j)
              {
                distribution_number[j] /= distribution_number_total;
                distribution_volume[j] /= distribution_volume_total;
              }

            size_index_[i].resize(Nsection_amc);
            for (int j = 0; j < Nsection_amc; ++j)
              size_index_[i][j] = j;

            size_fraction_number_[i] = distribution_number;
            size_fraction_mass_[i] = distribution_volume;
            size_diameter_mean_[i] = diameter_mean;
            Nsize_[i] = Nsection_amc;
          }
        else if (ops.Exists("index"))
          {
            ops.Set("index", "", size_index_[i]);
            Nsize_[i] = int(size_index_[i].size());

            for (int j = 0; j < Nsize_[i]; ++j)
              if (size_index_[i][j] < 0 || size_index_[i][j] >= Nsection_amc)
                throw AMC::Error("For emission source \"" + name_[i] + "\", the " + to_str(i) +
                                 "th size index (=" + to_str(size_index_[i][j]) +
                                 ") is < 0 or >= the number of size sections.");

            ops.Set("fraction.number", "", vector1r(1, real(1)), size_fraction_number_[i]);
            ops.Set("fraction.mass", "", vector1r(1, real(1)), size_fraction_mass_[i]);

            ops.Set("diameter", "", vector1r(1, real(1)), size_diameter_mean_[i]);
            for (int j = 0; j < Nsize_[i]; ++j)
              if (size_diameter_mean_[i][j] < diameter_bound_amc[size_index_[i][j]] ||
                  size_diameter_mean_[i][j] >= diameter_bound_amc[size_index_[i][j] + 1])
                throw AMC::Error("Size emission diameter mean n° " + to_str(j) +
                                 " is outside of the size section n° " + to_str(size_index_[i][j]) + ".");
          }
        else
          throw AMC::Error("Must provide size repartition for emission source \"" + name_[i] +
                           "\", possibilities are \"modal\", \"monahan\" or \"index\".");

        // Performs a few checks on size fractions.
        if (int(size_fraction_number_[i].size()) != Nsize_[i])
          throw AMC::Error("For emission source \"" + name_[i] + "\", the number of fraction number" +
                           " differs from the number of size section index.");

        if (int(size_fraction_mass_[i].size()) != Nsize_[i])
          throw AMC::Error("For emission source \"" + name_[i] + "\", the number of fraction mass" +
                           " differs from the number of size section index.");

        if (int(size_diameter_mean_[i].size()) != Nsize_[i])
          throw AMC::Error("For emission source \"" + name_[i] + "\", the number of diameter mean" +
                           " differs from the number of size section index.");

        // Avoid very low fractions.
        int j(0);
        for (int k = 0; k < Nsize_[i]; ++k)
          if (size_fraction_number_[i][k] > threshold_percentage_size_ &&
              size_fraction_mass_[i][k] > threshold_percentage_size_)
            {
              size_index_[i][j] = size_index_[i][k];
              size_fraction_number_[i][j] = size_fraction_number_[i][k];
              size_fraction_mass_[i][j] = size_fraction_mass_[i][k];
              size_diameter_mean_[i][j] = size_diameter_mean_[i][k];
              j++;
            }

        if (j < Nsize_[i])
          {
            size_index_[i].resize(j);
            size_fraction_number_[i].resize(j);
            size_fraction_mass_[i].resize(j);
            size_diameter_mean_[i].resize(j);
#if __cplusplus > 199711L
            size_index_[i].shrink_to_fit();
            size_fraction_number_[i].shrink_to_fit();
            size_fraction_mass_[i].shrink_to_fit();
            size_diameter_mean_[i].shrink_to_fit();
#endif
            Nsize_[i] = j;

            // Renormalize.
            real fraction_number_sum(real(0));
            real fraction_mass_sum(real(0));
            for (int k = 0; k < Nsize_[i]; ++k)
              {
                fraction_number_sum += size_fraction_number_[i][k];
                fraction_mass_sum += size_fraction_mass_[i][k];
              }

            for (int k = 0; k < Nsize_[i]; ++k)
              {
                size_fraction_number_[i][k] /= fraction_number_sum;
                size_fraction_mass_[i][k] /= fraction_mass_sum;
              }
          }

        // Inverse of mean volume.
        size_volume_mean_inv_[i].resize(Nsize_[i]);
        for (int k = 0; k < Nsize_[i]; ++k)
          size_volume_mean_inv_[i][k] = real(1) / (AMC_PI6 * size_diameter_mean_[i][k]
                                                   * size_diameter_mean_[i][k] * size_diameter_mean_[i][k]);

        // inverse of mass mean. Only if fixed densiy is used.
        if (use_density_fixed_[i])
          {
            size_mass_mean_inv_[i].resize(Nsize_[i]);
            for (int k = 0; k < Nsize_[i]; ++k)
              size_mass_mean_inv_[i][k] = size_volume_mean_inv_[i][k] / density_fixed_[i];
          }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bgreen() << Debug(1) << LogReset() << "\tNsize = " << Nsize_[i] << endl;
        *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "\tsize_fraction_index = " << size_index_[i] << endl;
        *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "\tsize_fraction_number = " << size_fraction_number_[i] << endl;
        *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "\tsize_fraction_mass = " << size_fraction_mass_[i] << endl;
        *AMCLogger::GetLog() << Fred() << Debug(1) << LogReset() << "\tsize_diameter_mean = " << size_diameter_mean_[i] << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "\tsize_volume_mean_inv = " << size_volume_mean_inv_[i] << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(3) << LogReset() << "\tsize_mass_mean_inv = " << size_mass_mean_inv_[i] << endl;
#endif

        if (Nsize_[i] == 0)
          throw AMC::Error("For emission source \"" + name_[i] + "\", there is no section size indexes.");
      }

    // Return to original prefix.
    ops.SetPrefix(prefix_orig);
  }


  // Clear static data.
  void ClassSource::Clear()
  {
    threshold_percentage_size_ = DRIVER_SOURCE_THRESHOLD_PERCENTAGE_SIZE_DEFAULT;

    Nsource_ = 0;
    Nanthropic_ = 0;
    Nbiogenic_ = 0;
    name_.clear();
    Ntracer_.clear();
    tracer_index_.clear();
    is_anthropic_.clear();

    Nsize_.clear();
    size_index_.clear();
    size_diameter_mean_.clear();
    size_fraction_number_.clear();
    size_fraction_mass_.clear();

    density_fixed_.clear();
    use_density_fixed_.clear();

    size_volume_mean_inv_.clear();
    size_mass_mean_inv_.clear();
  }


  // Get methods.
  int ClassSource::GetNsource()
  {
    return Nsource_;
  }

  int ClassSource::GetNanthropic()
  {
    return Nanthropic_;
  }

  int ClassSource::GetNbiogenic()
  {
    return Nbiogenic_;
  }

  int ClassSource::GetNsize(const int i)
  {
    return Nsize_[i];
  }

  string ClassSource::GetName(const int i)
  {
    return name_[i];
  }

  bool ClassSource::IsAnthropic(const int i)
  {
    return is_anthropic_[i];
  }

  vector<string> ClassSource::GetName()
  {
    return name_;
  }

  real ClassSource::GetDensityFixed(const int i)
  {
    return use_density_fixed_[i] ? density_fixed_[i] : real(-1);
  }

  void ClassSource::GetTracerIndex(const int i, vector<int> &tracer_index)
  {
    tracer_index = tracer_index_[i];
  }

  void ClassSource::GetSizeIndex(const int i, vector<int> &size_index)
  {
    size_index = size_index_[i];
  }

  void ClassSource::GetSizeDiameterMean(const int i, vector<real> &size_diameter_mean)
  {
    size_diameter_mean = size_diameter_mean_[i];
  }

  void ClassSource::GetSizeFractionNumber(const int i, vector<real> &size_fraction_number)
  {
    size_fraction_number = size_fraction_number_[i];
  }

  void ClassSource::GetSizeFractionMass(const int i, vector<real> &size_fraction_mass)
  {
    size_fraction_mass = size_fraction_mass_[i];
  }
}

#define DRIVER_FILE_CLASS_SOURCE_CXX
#endif
