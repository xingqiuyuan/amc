// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_DYNAMICS_EMISSION_HXX

#define DRIVER_EMISSION_THRESHOLD_PERCENTAGE_SIZE_DEFAULT 1.e-4

namespace Driver
{
  /*! 
   * \class ClassDynamicsEmission
   */
  template<class D>
  class ClassDynamicsEmission : protected ClassSource, protected AMC::ClassSpecies,
                                protected AMC::ClassDiscretization<D>
  {
  public:
    typedef Driver::real real;

    typedef typename Driver::vector1b vector1b;
    typedef typename Driver::vector1s vector1s;

    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;

    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Whether to count the source general section.
      Only applies if it is to be determined at run time.*/
    static bool source_count_general_section_;

    /*!< In which AMC general section to put each source emissions on a per size basis.*/
    static vector2i source_general_section_;

    /*!< The source general section counter.*/
    static Array<int, 3> source_general_section_count_;

    /*!< Working array.*/
    static vector1r composition_work_;

    /*!< Rate of a given source.*/
    static void rate(const int i,
                     const vector1r &emission_rate_mass,
                     vector1r &rate_aer_number,
                     vector1r &rate_aer_mass,
                     real *emission_rate_number = NULL,
                     int section_min = 0,
                     int section_max = AMC::ClassDiscretization<D>::Ng_);

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Get methods.*/
    static void GetSourceGeneralSectionIndex(vector<vector<int> > &source_general_section);
    static void GetSourceGeneralSectionCount(vector<vector<int> > &source_general_section_count);

    /*!< Set methods.*/
    static void ToggleSourceCountGeneralSection();

    /*!< Compute rate for given general sections.*/
    static void Rate(const int section_min,
                     const int section_max,
                     const vector<real> &emission_rate_mass,
                     vector<real> &emission_rate_number,
                     vector<real> &rate_aer_number,
                     vector<real> &rate_aer_mass);
  };
}

#define DRIVER_FILE_CLASS_DYNAMICS_EMISSION_HXX
#endif
