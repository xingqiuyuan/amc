// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_SOURCE_HXX

#define DRIVER_SOURCE_THRESHOLD_PERCENTAGE_SIZE_DEFAULT 1.e-4

namespace Driver
{
  /*! 
   * \class ClassSource
   */
  class ClassSource : protected ClassTracer
  {
  public:
    typedef Driver::real real;

    typedef typename Driver::vector1b vector1b;
    typedef typename Driver::vector1s vector1s;

    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;

    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Source threshold size percentage.*/
    static real threshold_percentage_size_;

    /*!< Number of emission sources.*/
    static int Nsource_;
    static int Nanthropic_;
    static int Nbiogenic_;

    /*!< Name of emission source.*/
    static vector1s name_;

    /*!< Is source anthropic ?*/
    static vector1b is_anthropic_;

    /*!< Index of tracers attached to each source.*/
    static vector1i Ntracer_;
    static vector2i tracer_index_;

    /*!< Size repartition for each source.*/
    static vector1i Nsize_;
    static vector2i size_index_;
    static vector2r size_diameter_mean_;
    static vector2r size_volume_mean_inv_;
    static vector2r size_fraction_number_;
    static vector2r size_fraction_mass_;

    /*!< Particle density for each source.*/
    static vector1r density_fixed_;
    static vector1b use_density_fixed_;

    /*!< Inverse of mean mass.*/
    static vector2r size_mass_mean_inv_;

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNsource();
    static int GetNanthropic();
    static int GetNbiogenic();
    static int GetNsize(const int i);
    static string GetName(const int i);
    static bool IsAnthropic(const int i);
#ifndef SWIG
    static vector<string> GetName();
#endif
    static real GetDensityFixed(const int i);
    static void GetTracerIndex(const int i, vector<int> &tracer_index);
    static void GetSizeIndex(const int i, vector<int> &size_index);
    static void GetSizeDiameterMean(const int i, vector<real> &size_diameter_mean);
    static void GetSizeFractionNumber(const int i, vector<real> &size_fraction_number);
    static void GetSizeFractionMass(const int i, vector<real> &size_fraction_mass);
  };
}

#define DRIVER_FILE_CLASS_SOURCE_HXX
#endif
