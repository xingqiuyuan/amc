// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_TRACER_HXX

namespace Driver
{
  /*! 
   * \class ClassTracer
   */
  class ClassTracer
  {
  public:
    typedef Driver::real real;

    typedef typename Driver::vector1b vector1b;
    typedef typename Driver::vector1s vector1s;

    typedef typename Driver::vector1i vector1i;
    typedef typename Driver::vector2i vector2i;

    typedef typename Driver::vector1r vector1r;
    typedef typename Driver::vector2r vector2r;

  protected:

    /*!< Number of tracers.*/
    static int Ntracer_;
    static int Nanthropic_;
    static int Nbiogenic_;

    /*!< Is tracer anthropic or not ?*/
    static vector1b is_anthropic_;

    /*!< Tracer names.*/
    static vector1s name_;

    /*!< Speciation of each tracers against AMC species.*/
    static vector1i Nspecies_;
    static vector2i species_index_;
    static vector2r species_mass_fraction_;

    /*!< Molar mass of tracers.*/
    static vector1r molar_mass_;

    /*!< Density of tracers.*/
    static vector1r density_;

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNtracer();
    static int GetNanthropic();
    static int GetNbiogenic();
    static string GetName(const int i);
#ifndef SWIG
    static vector<string> GetName();
#endif
    static bool IsAnthropic(const int i);
    static int GetNspecies(const int i);
    static real GetMolarMass(const int i);
    static real GetDensity(const int i);
    static void GetSpeciesIndex(const int i, vector<int> &species_index);
    static void GetSpeciesMassFraction(const int i, vector<real> &species_mass_fraction);
  };
}

#define DRIVER_FILE_CLASS_TRACER_HXX
#endif
