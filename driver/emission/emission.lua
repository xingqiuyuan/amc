
emission = {
   modal_fin = {
      volume = {1.},
      diameter = {0.11},
      std = {1.6}
   },

   modal_coa = {
      volume = {1.},
      diameter = {4.},
      std = {1.1}
   },

   modal_big = {
      volume = {1.},
      diameter = {25.},
      std = {1.3}
   },

   chimere_sa = {
      tracer = {
         anthropic = {
            H2SO4_fin = {H2SO4 = 1.},
            PPM_o_big = {DUST = 1.},
            PPM_o_coa = {DUST = 1.},
            PPM_o_fin = {DUST = 0.3, EC = 0.3, PTCA = 0.2, LVG = 0.2},
            PPM_e_big = {DUST = 1.},
            PPM_e_coa = {DUST = 1.},
            PPM_e_fin = {DUST = 0.3, EC = 0.3, PTCA = 0.2, LVG = 0.2},
            OCAR_S7_fin = {PTCA = 1.},
            BCAR_S7_fin = {EC = 1.},
            OCAR_S2_w_fin = {LVG = 1.},
            BCAR_S2_w_fin = {EC = 1.},
            OCAR_S2_nw_fin = {PTCA = 1.},
            BCAR_S2_nw_fin = {EC = 1.}
         },

         biogenic = {
            DUST_big = {DUST = 1.},
            DUST_coa = {DUST = 1.},
            DUST_fin = {DUST = 1.},
            NA_coa = {Na = 1.},
            HCL_coa = {HCl = 1.},
            H2SO4_coa = {H2SO4 = 1.}
         }
      },

      source = {
         threshold_percentage_size = 1.e-4,

         anthropic = {
            s7 = {
               tracer = {"OCAR_S7_fin", "BCAR_S7_fin"},
               size = {modal = "emission.modal_fin"}
            },

            s2_w = {
               tracer = {"OCAR_S2_w_fin", "BCAR_S2_w_fin"},
               size = {modal = "emission.modal_fin"}
            },

            s2_nw = {
               tracer = {"OCAR_S2_nw_fin", "BCAR_S2_nw_fin"},
               size = {modal = "emission.modal_fin"}
            }
         },

         biogenic = {
            dust_big = {
               tracer = {"DUST_big"},
               size = {modal = "emission.modal_big"}
            },

            dust_coa = {
               tracer = {"DUST_coa"},
               size = {modal = "emission.modal_coa"}
            },

            dust_fin = {
               tracer = {"DUST_fin"},
               size = {modal = "emission.modal_fin"}
            },

            salt = {
               tracer = {"NA_coa", "HCL_coa", "H2SO4_coa"},
               size = {monahan = {}}
            },
         },

         -- Where to put source resolved emissions.
         -- If empty, will be determined at run time.
         class_composition = {
            s7 = {},
            s2_w = {},
            s2_nw = {},
            dust_big = {},
            dust_coa = {},
            dust_fin = {},
            salt = {}
         },

         -- Whether to count the general section. Only applies
         -- if it is to be determined at run time. Default to false.
         --count_general_section = true
      }
   }
}