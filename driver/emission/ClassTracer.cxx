// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef DRIVER_FILE_CLASS_TRACER_CXX

#include "ClassTracer.hxx"

namespace Driver
{
  // Init static data.
  void ClassTracer::Init(Ops::Ops &ops)
  {
    string prefix_orig = ops.GetPrefix();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << LogReset() << "Instantiate tracers for emission"
                         << " from configuration section \"" << prefix_orig << "\"." << endl;
#endif

    //
    // Tracers.
    //

    { // Anthropic.
      vector1s tracer_name = ops.GetEntryList("anthropic");
      Nanthropic_ = int(tracer_name.size());

      for (int i = 0; i < Nanthropic_; ++i)
        {
          name_.push_back(tracer_name[i]);
          is_anthropic_.push_back(true);
        }
    }

    { // Biogenic.
      vector1s tracer_name = ops.GetEntryList("biogenic");
      Nbiogenic_ = int(tracer_name.size());

      for (int i = 0; i < Nbiogenic_; ++i)
      {
        name_.push_back(tracer_name[i]);
        is_anthropic_.push_back(false);
      }
    }

    Ntracer_ = int(name_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(3) << LogReset() << "Ntracer_ = " << Ntracer_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(3) << LogReset() << "Nanthropic = " << Nanthropic_ << endl;
    *AMCLogger::GetLog() << Bmagenta() << Info(3) << LogReset() << "Nbiogenic = " << Nbiogenic_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "name = " << name_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << LogReset() << "is_anthropic = " << is_anthropic_ << endl;
#endif

    Nspecies_.assign(Ntracer_, 0);
    species_index_.resize(Ntracer_);
    species_mass_fraction_.resize(Ntracer_);
    molar_mass_.assign(Ntracer_, real(0));
    density_.assign(Ntracer_, real(0));

    for (int i = 0; i < Ntracer_; ++i)
      {
        if (is_anthropic_[i])
          ops.SetPrefix(prefix_orig + "anthropic." + name_[i] + ".");
        else
          ops.SetPrefix(prefix_orig + "biogenic." + name_[i] + ".");

        vector1s tracer_species = ops.GetEntryList();

        Nspecies_[i] = int(tracer_species.size());

        species_index_[i].resize(Nspecies_[i]);
        species_mass_fraction_[i].resize(Nspecies_[i]);

        real fraction_sum(real(0));
        for (int j = 0; j < Nspecies_[i]; ++j)
          {
            species_index_[i][j] = AMC::ClassSpecies::GetIndex(tracer_species[j]);
            if (species_index_[i][j] < 0)
              throw AMC::Error("Unable to find species \"" + tracer_species[j] + "\" for tracer \"" + name_[i] + "\".");

            species_mass_fraction_[i][j] = ops.Get<real>(tracer_species[j]);
            fraction_sum += species_mass_fraction_[i][j];
          }

        if (fraction_sum != real(1))
          throw AMC::Error("Fraction sum for tracer \"" + tracer_species[i] + "\" is not unity.");

        // Compute molar mass.
        for (int j = 0; j < Nspecies_[i]; ++j)
          molar_mass_[i] += species_mass_fraction_[i][j] / AMC::ClassSpecies::GetMolarMass(species_index_[i][j]);
        molar_mass_[i] = real(1) / molar_mass_[i];

        // Compute density.
        for (int j = 0; j < Nspecies_[i]; ++j)
          density_[i] += species_mass_fraction_[i][j] / AMC::ClassSpecies::GetDensity(species_index_[i][j]);
        density_[i] = real(1) / density_[i];

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bblue() << Debug(1) << LogReset() << "Tracer \"" << name_[i] << "\" :" << endl;
        *AMCLogger::GetLog() << Bblue() << Debug(1) << LogReset() << "\tmolar_mass = " << molar_mass_[i] << endl;
        *AMCLogger::GetLog() << Bblue() << Debug(1) << LogReset() << "\tdensity = " << density_[i] << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "\tspecies_index = " << species_index_[i] << endl;
        *AMCLogger::GetLog() << Fblue() << Debug(1) << LogReset() << "\tspecies_mass_fraction = " << species_mass_fraction_[i] << endl;
#endif
      }

    // Return to original prefix.
    ops.SetPrefix(prefix_orig);
  }


  // Clear static data.
  void ClassTracer::Clear()
  {
    Ntracer_ = 0;
    Nanthropic_ = 0;
    Nbiogenic_ = 0;
    name_.clear();
    is_anthropic_.clear();

    Nspecies_.clear();
    species_index_.clear();
    species_mass_fraction_.clear();

    molar_mass_.clear();
    density_.clear();
  }


  // Get methods.
  int ClassTracer::GetNtracer()
  {
    return Ntracer_;
  }

  int ClassTracer::GetNanthropic()
  {
    return Nanthropic_;
  }

  int ClassTracer::GetNbiogenic()
  {
    return Nbiogenic_;
  }

  string ClassTracer::GetName(const int i)
  {
    return name_[i];
  }

  vector<string> ClassTracer::GetName()
  {
    return name_;
  }

  bool ClassTracer::IsAnthropic(const int i)
  {
    return is_anthropic_[i];
  }

  int ClassTracer::GetNspecies(const int i)
  {
    return Nspecies_[i];
  }

  real ClassTracer::GetMolarMass(const int i)
  {
    return molar_mass_[i];
  }

  real ClassTracer::GetDensity(const int i)
  {
    return density_[i];
  }

  void ClassTracer::GetSpeciesIndex(const int i, vector<int> &species_index)
  {
    species_index = species_index_[i];
  }

  void ClassTracer::GetSpeciesMassFraction(const int i, vector<real> &species_mass_fraction)
  {
    species_mass_fraction = species_mass_fraction_[i];
  }
}

#define DRIVER_FILE_CLASS_TRACER_CXX
#endif
