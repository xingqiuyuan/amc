#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core

import numpy as _numpy
import os as _os


class DriverBase(_core.DriverBase):
    pass


class DriverDomainBase(_core.DriverDomainBase):

    @staticmethod
    def GetY():
        y = _core.vector1r()
        _core.DriverDomainBase.GetY(y)
        return _numpy.array(y, dtype = _numpy.float)

    @staticmethod
    def GetX():
        x = _core.vector1r()
        _core.DriverDomainBase.GetX(x)
        return _numpy.array(x, dtype = _numpy.float)

    @staticmethod
    def GetTopZ():
        z_top = _core.vector1r()
        _core.DriverDomainBase.GetTopZ(z_top)
        return _numpy.array(z_top, dtype = _numpy.float)

    @staticmethod
    def GetMidZ():
        z_mid = _core.vector1r()
        _core.DriverDomainBase.GetMidZ(z_mid)
        return _numpy.array(z_mid, dtype = _numpy.float)
