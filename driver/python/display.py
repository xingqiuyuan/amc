#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core
import datetime as _datetime
import numpy as _numpy
import pylab as _pylab
import copy as _copy
import os as _os
from inspect import currentframe as _cf
from matplotlib.colors import LogNorm as _LogNorm
from matplotlib.colors import Normalize as _Normalize
import shelve as _shelve
from util import string_to_dictionary as _string_to_dictionary


try:
    from matplotlib.toolkits.basemap import Basemap as _Basemap
except:
    pass

# From version 0.98.0 of Matplotlib.
try:
    from mpl_toolkits.basemap import Basemap as _Basemap
except:
    pass

if _core.DRIVER_HAS_GEO_DATA > 0:
    from geo_data import GeoDataElevation as _GeoDataElevation


def GetChamproboisSpeciesList(**kwargs):
    if kwargs.has_key("path"):
        fs = _shelve.open(kwargs["path"])
    else:
        fs = kwargs["fs"]

    species_name = _copy.copy(fs["species_name"])
    fs.close()

    return species_name


def GetChamproboisGasList(**kwargs):
    if kwargs.has_key("path"):
        fs = _shelve.open(kwargs["path"])
    else:
        fs = kwargs["fs"]

    gas_name = [fs["species_name"][i] for i in fs["gas_index"]]
    fs.close()

    return gas_name


def _expand_string_(s, n, m = None):
    if "-" in s:
        ls = s.split("-")
        if len(ls[0]) == 0: ls[0] = 0
        if len(ls[1]) == 0: ls[1] = n
        ls = range(int(ls[0]), int(ls[1]))
    elif "," in s:
        ls = [m.index(x) for x in ls.split(",")]
    elif s.isalnum():
        ls = [m.index(s)]
    else:
        raise ValueError("Could not expand string \"" + s + "\".")

    return ls


def GetChamproboisField(fields, **kwargs):
    if kwargs.has_key("path"):
        fs = _shelve.open(kwargs["path"])
    else:
        fs = kwargs["fs"]

    Nsection = fs["Nsection"]
    Nspecies = fs["Nspecies"]
    Ngas = fs["Ngas"]
    Nz = fs["Nz"]
    Nr = fs["Nr"]
    Nt = fs["Nt"]
    Ns = fs["Ns"]

    gas_name = [fs["species_name"][i] for i in fs["gas_index"]]

    concentration_gas = fs["concentration"][ :, :, :, Nsection * (Nspecies + 1):Ns]
    concentration_gas_water = fs["concentration"][ :, :, :, Ns]
    concentration_aer_num = fs["concentration"][ :, :, :, 0:Nsection]
    concentration_aer_mass_1 = fs["concentration"][ :, :, :, Nsection:Nsection * (Nspecies + 1)]
    concentration_aer_mass_2 = concentration_aer_mass_1[ :, :, :, :]
    concentration_aer_mass_2.shape = (Nt, Nz, Nr, Nsection, Nspecies)

    data = _numpy.zeros([Nt, Nz, Nr], dtype = _numpy.float)

    for field in fields.split("+"): 
        if "num" in field:
            if ":" in field:
                for i in _expand_string_(field.split(":")[1], Nsection):
                    data += concentration_aer_num[ :, :, :, i]
            else:
                data += concentration_aer_num.sum(3)
        elif "gas" in field:
            if "water" in field:
                data += _copy.copy(concentration_gas_water)
            elif ":" in field:
                for i in _expand_string_(field.split(":")[1], Ngas, gas_name):
                    data += concentration_gas[ :, :, :, i]
            else:
                data += concentration_gas.sum(3)
        elif "aer" in field:
            if ":" in field:
                for i in _expand_string_(field.split(":")[1], Nsection):
                    for j in _expand_string_(field.split(":")[2], Nspecies, fs["species_name"]):
                        data += concentration_aer_mass_2[ :, :, :, i, j]
            else:
                data += concentration_aer_mass_1.sum(3)
        else:
            data = _copy.copy(fs[field])

    fs.close()

    return data


def PlotChamproboisMap(field, **kwargs):
    if kwargs.has_key("path"):
        fs = _shelve.open(kwargs["path"])
    else:
        fs = kwargs["fs"]

    chimney_length = fs["chimney"]["length"]
    chimney_radius = fs["chimney"]["radius"]
    Nsection = fs["Nsection"]
    Nspecies = fs["Nspecies"]
    Ngas = fs["Ngas"]
    Ns = fs["Ns"]
    Nz = fs["Nz"]
    Nr = fs["Nr"]
    Nt = fs["Nt"]
    date = fs["date"]
    name = fs["name"]

    data = GetChamproboisField(field, fs = fs)

    fs.close()

    z = _numpy.linspace(0, chimney_length, Nz + 1)
    r = _numpy.linspace(0, chimney_radius, Nr + 1)

    # Put in matrix form for pcolor/pcolormesh
    z = _numpy.array([z for i in range(Nr + 1)])
    r = _numpy.array([r for i in range(Nz + 1)]).transpose()

    width = 24 / 2.54
    _pylab.matplotlib.rcParams["figure.figsize"] = (2 * width, .75 * width)

    margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
    _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
    _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
    _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
    _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

    font_size = 12
    _pylab.matplotlib.rcParams["font.size"] = font_size
    _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
    _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
    _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
    _pylab.matplotlib.rcParams["xtick.major.size"] = 2
    _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
    _pylab.matplotlib.rcParams["ytick.major.size"] = 2
    _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

    if not kwargs.has_key("color"):
        kwargs["color"] = "jet"

    if isinstance(kwargs["color"], str):
        cmap = _pylab.get_cmap(kwargs["color"])
    else:
        cmap = _ListedColormap(kwargs["color"])

    for t in range(Nt):
        _pylab.clf()

        fig = _pylab.figure()
        ax = fig.add_subplot(111)

        data_t = data[t].transpose()

        log_scale = False
        dmin = data_t.min()
        dmax = data_t.max()
        if kwargs.has_key("scale"):
            scale = _string_to_dictionary(kwargs["scale"])
            if scale.has_key("min"): dmin = scale["min"]
            if scale.has_key("max"): dmax = scale["max"]
            log_scale = scale.has_key("log")

        if log_scale and dmin > 0.:
            norm = _LogNorm(vmin = dmin, vmax = dmax)
        else:
            norm = _Normalize(vmin = dmin, vmax = dmax)

        if _core.DRIVER_HAS_LOGGER > 0:
            _core.DriverLogger_GetLog().DEBUG1("t = %d, data (min, mean, max) = (%3.2f %3.2f %3.2f)"
                                               % (t, data_t.min(), data_t.mean(), data_t.max()),
                                               _os.path.basename(_cf().f_code.co_filename),
                                               _cf().f_lineno, _cf().f_code.co_name)        

        cs = ax.pcolormesh(z, r, data_t, norm = norm, cmap = cmap)

        ax.set_xlim(z.min(), z.max())
        ax.set_xlabel(ur"Chimney axis [m]")

        ax.set_ylabel(ur"Chimney radius [m]")
        ax.set_ylim(r.min(), r.max())
        ax.set_yticks(r[:, 0])
        ax.set_yticklabels(["%1.2g" % x for x in r[:, 0]])

        cbar = _pylab.colorbar(cs)
        if kwargs.has_key("clabel"):
            cbar.ax.set_ylabel(kwargs["clabel"])

        if kwargs.has_key("title"):
            stitle = kwargs["title"]
        else:
            stitle = "%s %s &t" % (name, field)
        stitle = stitle.replace("&t", date[t].strftime("%Y-%m-%d_%H:%M"))
        ax.set_title(stitle)

        if kwargs.has_key("output_file"):
            output_file = kwargs["output_file"]
        else:
            output_file = "%s_%s_%03d.png" % (name, field, t)

        if _core.DRIVER_HAS_LOGGER > 0:
            _core.DriverLogger_GetLog().DEBUG1("Save field \"%s\" of essai \"%s\" at time %d to file \"%s\""
                                               % (field, name, t, output_file),
                                               _os.path.basename(_cf().f_code.co_filename),
                                               _cf().f_lineno, _cf().f_code.co_name)

        _pylab.savefig(output_file)


def PlotTrajectory(trajectory, fig = None, **kwargs):

    if kwargs.has_key("width"):
        width = kwargs["width"]
    else:
        width = 24
    width /= 2.54

    if kwargs.has_key("offset"):
        offset = kwargs["offset"]
    else:
        offset = 1

    _pylab.matplotlib.rcParams["figure.figsize"] = (width * offset, .75 * width)

    margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
    if kwargs.has_key("margins"):
        for k,v in kwargs["margins"].iteritems():
            margins[k] = v

    _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
    _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
    _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
    _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

    if kwargs.has_key("font_size"):
        font_size = kwargs["font_size"]
    else:
        font_size = 12

    _pylab.matplotlib.rcParams["font.size"] = font_size
    _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
    _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
    _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
    _pylab.matplotlib.rcParams["xtick.major.size"] = 2
    _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
    _pylab.matplotlib.rcParams["ytick.major.size"] = 2
    _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

    if kwargs.has_key("resolution"):
        resolution = kwargs["resolution"]
    else:
        resolution = "i"

    if kwargs.has_key("lw"):
        lw = kwargs["lw"]
    else:
        lw = "2"

    if fig == None:
        _pylab.clf()
        fig = _pylab.figure()
        ax = fig.add_subplot(111)

        m = _Basemap(projection = 'cyl',
                     llcrnrlon = trajectory.GetXmin(), llcrnrlat = trajectory.GetYmin(),
                     urcrnrlon = trajectory.GetXmax(), urcrnrlat = trajectory.GetYmax(),
                     resolution = resolution, suppress_ticks = False,
                     area_thresh = 1000)

        if not kwargs.has_key("nocountries"):
            m.drawcountries()

        if not kwargs.has_key("nocoastlines"):
            m.drawcoastlines()

        if kwargs.has_key("rivers"):
            m.drawrivers()

        if not kwargs.has_key("norelief"):
            m.etopo()
        else:
            m.drawmapboundary(fill_color = 'aqua')
            m.fillcontinents(color = 'coral', lake_color = 'aqua')
    else:
        ax = fig.gca()

    longitude = trajectory.GetLongitude()
    latitude = trajectory.GetLatitude()

    lon = "W"
    if longitude[0] < 0.: lon = "E"

    lat = "N"
    if latitude[0] < 0.: lat = "S"

    label = trajectory.GetDateBegin().strftime("%Y-%m-%d_%H") + ", " \
            + u"%04.1f°%s" % (abs(longitude[0]), lon) + ", " \
            + u"%04.1f°%s" % (abs(latitude[0]), lat)

    if kwargs.has_key("label"):
        label = kwargs["label"]

    if kwargs.has_key("fmt"):
        ax.plot(longitude, latitude, kwargs["fmt"], lw = lw, label = label)
    else:
        ax.plot(longitude, latitude, lw = lw, label = label)

    if kwargs.has_key("output_file") or kwargs.has_key("show"):
        if kwargs.has_key("legend"):
            if isinstance(kwargs["legend"], int):
                ax.legend(loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("Legend keyword must be an integer or a list.")

    ax.scatter(longitude[0], latitude[0], 60, color = "r", marker = "s")

    frequency = 24
    if kwargs.has_key("frequency"):
        frequency = kwargs["frequency"]

    frequency *= int(3600. / trajectory.GetTimeStep() + 1.e-6)

    for t in range(frequency, trajectory.GetNt(), frequency):
        ax.scatter(longitude[t], latitude[t], 60, color = "k", marker = "o", zorder = 9)
        
    ax.scatter(longitude[-1], latitude[-1], 60, color = "g", marker = "s")

    ax.set_xlabel(u"longitude")
    ax.set_ylabel(u"latitude")

    if kwargs.has_key("aspect_ratio"):
        _pylab.axes().set_aspect(kwargs["aspect_ratio"], "box")
        # Should be 1.4 for regional domain.

    if kwargs.has_key("output_file") or kwargs.has_key("show"):
        title = "Trajectory of type \"" + trajectory.GetType().split(":")[1] + "\""
        if kwargs.has_key("title"):
            title = kwargs["title"]

        ax.set_title(title)

    if kwargs.has_key("output_file"):
        _pylab.savefig(kwargs["output_file"])
        _pylab.clf()
    elif kwargs.has_key("show"):
        _pylab.show()

    return fig


def PlotVariable(trajectory, variable_name, **kwargs):
    variable_z_level = trajectory.GetVariableLevelZ(variable_name)

    if kwargs.has_key("z"):
        if kwargs["z"] >= variable_z_level:
            raise Exception("Variable \"" + variable_name + "\" has only " +
                            str(variable_z_level) + " levels, you asked the " + str(kwargs["z"]) + "th.")
        _plot_variable1_(trajectory, variable_name, kwargs["z"], **kwargs)
    else:
        if variable_z_level == 1:
            raise Exception("Variable \"" + variable_name + "\" has only 1 level.")
        _plot_variable2_(trajectory, variable_name, **kwargs)


def _plot_variable1_(trajectory, variable_name, z, **kwargs):

    if kwargs.has_key("width"):
        width = kwargs["width"]
    else:
        width = 24
    width /= 2.54

    _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

    margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.15, "top" : 0.8}
    if kwargs.has_key("margins"):
        for k,v in kwargs["margins"].iteritems():
            margins[k] = v

    _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
    _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
    _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
    _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

    if kwargs.has_key("font_size"):
        font_size = kwargs["font_size"]
    else:
        font_size = 12

    _pylab.matplotlib.rcParams["font.size"] = font_size
    _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
    _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
    _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
    _pylab.matplotlib.rcParams["xtick.major.size"] = 2
    _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
    _pylab.matplotlib.rcParams["ytick.major.size"] = 2
    _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

    if kwargs.has_key("lw"):
        lw = kwargs["lw"]
    else:
        lw = "2"

    _pylab.clf()
    fig = _pylab.figure()
    ax = fig.add_subplot(111)

    variable_data = trajectory.GetVariable(variable_name)

    t = range(trajectory.GetNt())

    fmt = "k-"
    if kwargs.has_key("fmt"):
        fmt = kwargs["fmt"]

    if kwargs.has_key("label"):
        ax.plot(t, variable_data[:,z], fmt, lw = lw, label = label)
    else:
        ax.plot(t, variable_data[:,z], fmt, lw = lw)

    if kwargs.has_key("ylabel"):
        ax.set_ylabel(kwargs["ylabel"])
    else:
        ax.set_ylabel(variable_name)

    if kwargs.has_key("xticks_frequency"):
        xticks_frequency = kwargs["xticks_frequency"]
    else:
        xticks_frequency = 8
    xticks_frequency *= int(3600. / trajectory.GetTimeStep() + 1.e-6)

    if  kwargs.has_key("xticks_date_format"):
        xticks_date_format = kwargs["xticks_date_format"]
    else:
        xticks_date_format = "%Y-%m-%d_%H"

    xticks_loc = []
    xticks_label = []
    for h in range(0, trajectory.GetNt(), xticks_frequency):
        xticks_loc.append(h)
        xticks_label.append(trajectory.GetDate(h).strftime(xticks_date_format))

    if  kwargs.has_key("xticks_rotation"):
        xticks_rotation = kwargs["xticks_rotation"]
    else:
        xticks_rotation = 45

    _pylab.xticks(xticks_loc, xticks_label, rotation = xticks_rotation)

    ax.set_xlim(xmin = 0, xmax = trajectory.GetNt())
    if kwargs.has_key("output_file") or kwargs.has_key("show"):
        title = "Variable \"" + variable_name + "\" for trajectory number " + str(trajectory.GetId()) \
                + " of type \"" + trajectory.GetType().split(":")[1] + "\"."

        if kwargs.has_key("title"):
            title = kwargs["title"]

        ax.set_title(title)

    if kwargs.has_key("output_file"):
        _pylab.savefig(kwargs["output_file"])
        _pylab.clf()
    elif kwargs.has_key("show"):
        _pylab.show()

    return fig


def _plot_variable2_(trajectory, variable_name, **kwargs):

    if kwargs.has_key("width"):
        width = kwargs["width"]
    else:
        width = 24
    width /= 2.54

    _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

    margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.15, "top" : 0.1}
    if kwargs.has_key("margins"):
        for k,v in kwargs["margins"].iteritems():
            margins[k] = v

    _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
    _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
    _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
    _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

    if kwargs.has_key("font_size"):
        font_size = kwargs["font_size"]
    else:
        font_size = 12

    _pylab.matplotlib.rcParams["font.size"] = font_size
    _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
    _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
    _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
    _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
    _pylab.matplotlib.rcParams["xtick.major.size"] = 2
    _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
    _pylab.matplotlib.rcParams["ytick.major.size"] = 2
    _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

    if kwargs.has_key("lw"):
        lw = kwargs["lw"]
    else:
        lw = "2"

    _pylab.clf()
    fig = _pylab.figure()
    ax = fig.add_subplot(111)

    variable_data = trajectory.GetVariable(variable_name)

    V = 30
    if kwargs.has_key("V"):
        V = kwargs["V"]

    z = range(trajectory.GetNz())
    t = range(trajectory.GetNt())

    if kwargs.has_key("log_norm"):
        norm =  _LogNorm(vmin = variable_data.min(), vmax = variable_data.max())
        if kwargs.has_key("levels"):
            cs = ax.contourf(t, z, variable_data.transpose(), levels = kwargs["levels"], norm = norm, extent = "both", **kwargs)
        else:
            cs = ax.contourf(t, z, variable_data.transpose(), V, norm = norm, extent = "both", **kwargs)
    else:
        if kwargs.has_key("levels"):
            cs = ax.contourf(t, z, variable_data.transpose(), levels = kwargs["levels"], extent = "both", **kwargs)
        else:
            cs = ax.contourf(t, z, variable_data.transpose(), V, extent = "both", **kwargs)

    cbar = _pylab.colorbar(cs)

    ax.set_xlabel(u"date")
    ax.set_ylabel(u"vertical levels [m]")

    if kwargs.has_key("xticks_frequency"):
        xticks_frequency = kwargs["xticks_frequency"]
    else:
        xticks_frequency = 8
    xticks_frequency *= int(3600. / trajectory.GetTimeStep() + 1.e-6)

    if  kwargs.has_key("xticks_date_format"):
        xticks_date_format = kwargs["xticks_date_format"]
    else:
        xticks_date_format = "%Y-%m-%d_%H"

    xticks_loc = []
    xticks_label = []
    for h in range(0, trajectory.GetNt(), xticks_frequency):
        xticks_loc.append(h)
        xticks_label.append(trajectory.GetDate(h).strftime(xticks_date_format))

    if  kwargs.has_key("xticks_rotation"):
        xticks_rotation = kwargs["xticks_rotation"]
    else:
        xticks_rotation = 45

    _pylab.xticks(xticks_loc, xticks_label, rotation = xticks_rotation)
    _pylab.yticks(range(trajectory.GetNz()), ["%3.1f" % z for z in trajectory.GetMidZ()])

    if kwargs.has_key("aspect_ratio"):
        _pylab.axes().set_aspect(kwargs["aspect_ratio"], "box")
        # Should be 1.4 for regional domain.

    if kwargs.has_key("output_file") or kwargs.has_key("show"):
        title = "Variable \"" + variable_name + "\" for trajectory number " + str(trajectory.GetId()) \
                + " of type \"" + trajectory.GetType().split(":")[1] + "\"."

        if kwargs.has_key("title"):
            title = kwargs["title"]

        ax.set_title(title)

    if kwargs.has_key("output_file"):
        _pylab.savefig(kwargs["output_file"])
        _pylab.clf()
    elif kwargs.has_key("show"):
        _pylab.show()

    return fig
