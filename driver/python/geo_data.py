#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core

import numpy as _numpy
import pylab as _pylab

try:
    from matplotlib.toolkits.basemap import Basemap as _Basemap
except:
    pass

# From version 0.98.0 of Matplotlib.
try:
    from mpl_toolkits.basemap import Basemap as _Basemap
except:
    pass


class GeoDataBase(_core.GeoDataBase):

    @staticmethod
    def GetNameList():
        name_list = _core.vector1s()
        _core.GeoDataBase.GetNameList(name_list)
        return list(name_list)


    def GetX(self, i):
        x = _core.vector1f()
        _core.GeoDataBase.GetX(self, i, x)
        return _numpy.array(x, dtype = _numpy.float)


    def GetData(self):
        data = _core.vector1f()
        _core.GeoDataBase.GetData(self, data)
        shape = [self.GetN(i) for i in range(self.GetD() - 1, -1, -1)]
        data = _numpy.array(data, dtype = _numpy.float)
        data.shape = shape
        return data


    def GetValue(self, x):
        x_vect = _core.vector1f(x)
        return _core.GeoDataBase.GetValue(self, x_vect)


    def Plot(self, **kwargs):
        if self.GetD() != 2:
            raise Exception("Map is only available for 2D data.")

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 4
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 2
        _pylab.matplotlib.rcParams["ytick.major.size"] = 4
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 2

        if kwargs.has_key("colormap"):
            colormap = kwargs["colormap"]
        else:
            colormap = None

        if kwargs.has_key("levelmap"):
            levelmap = kwargs["levelmap"]
        else:
            levelmap = None

        _pylab.clf()
        fig = _pylab.figure()
        ax = fig.add_subplot(111)

        X0 = self.GetX(0)
        X1 = self.GetX(1)

        if kwargs.has_key("resolution"):
            resolution = kwargs["resolution"]
        else:
            resolution = "i"

        m = _Basemap(projection = 'cyl',
                    llcrnrlon = X0[0], llcrnrlat = X1[0],
                     urcrnrlon = X0[-1], urcrnrlat = X1[-1],
                     resolution = resolution, suppress_ticks = False,
                     area_thresh = 1000)

        if not kwargs.has_key("nocountries"):
            m.drawcountries()

        if not kwargs.has_key("nocoastlines"):
            m.drawcoastlines()

        if kwargs.has_key("rivers"):
            m.drawrivers()

        data = self.GetData()

        V = 30
        if kwargs.has_key("V"):
            V = kwargs["V"]

        x_range, y_range = m.makegrid(self.GetN(0), self.GetN(1))

        if colormap != None and levelmap != None:
            cs = m.contourf(x_range, y_range, data, levelmap,
                            colors = colormap, extent = "both", **kwargs)
            cbar = _pylab.colorbar(cs, ticks = levelmap, drawedges = True)
        else:
            cs = m.contourf(x_range, y_range, data, V, extent = "both", **kwargs)
            cbar = _pylab.colorbar(cs)

        if kwargs.has_key("barlabel"):
            cbar.ax.set_ylabel(kwargs["barlabel"])
        cs.cmap.set_under("white")

        ax.set_xlabel(ur"longitude")
        ax.set_ylabel(ur"latitude")

        if kwargs.has_key("title"):
            title = kwargs["title"]
        else:
            title = self.GetName()

        ax.set_title(title)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()
        return fig


class GeoDataElevation(_core.GeoDataElevation, GeoDataBase):
    pass
