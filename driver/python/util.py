#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core
from driver_core import vector1i, vector1r, vector1s, vector1f, vector2r, vector2s, ArrayReal
import numpy as _numpy


if _core.DRIVER_HAS_LOGGER == 1:
    class LoggerDriver(_core.DriverLogger):
        pass


if _core.DRIVER_HAS_TIMER == 1:
    class TimerCPU(_core.TimerCPU):
        @staticmethod
        def Display(i = -1):
            return dict(_core.TimerCPU.Display(i))

    class TimerWall(_core.TimerWall):
        @staticmethod
        def Display(i = -1):
            return dict(_core.TimerWall.Display(i))


if _core.DRIVER_HAS_STATISTICS == 1:
    class StatisticsDriver(_core.DriverStatistics):
        @staticmethod
        def GetNameList():
            list(_core.DriverStatistics.GetNamelist())

        @staticmethod
        def Display(i = - 1, precision = 6):
            return dict(_core.DriverStatistics.Display(i, precision))


# Get shelve descriptor.
def get_shelve_descriptor(fs, key):
    fd = fs[key[0]]
    for k in key[1:]:
        fd = fd[k]
    return fd


# Change one string into a dictionary.
def string_to_dictionary(string, sep1 = ":", sep2 = "="):
    dictionary = {}
    for s in string.split(sep1):
        if sep2 in s:
            key, val = s.split(sep2)
            try:
                float(val)
            except ValueError:
                pass
            else:
                val = float(val)
            dictionary[key] = val
        else:
            dictionary[s] = True
    return dictionary


#
# Helper functions for standard nested vectors.
#


def to_array(v, dtype = None):
    if dtype == None:
        if isinstance(v, _core.vector2i):
            dtype = _numpy.int
        else:
            dtype = _numpy.float

    try:
        return _numpy.array(v, dtype = dtype)
    except Exception:
        return [_numpy.array(v[i], dtype = dtype) for i in range(len(v))]
