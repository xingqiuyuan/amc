#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core
import numpy as _numpy
import datetime as _datetime
from base import DriverBase as _DriverBase


class TrajectoryBase(_core.TrajectoryBase, _DriverBase):

    @staticmethod
    def GetDataFile():
        return dict(_core.TrajectoryBase.GetDataFile())

    @staticmethod
    def GetDataDateBegin():
        date_date_begin = _core.TrajectoryBase.GetDataDateBegin()
        return _datetime.datetime(int(date_date_begin[0:4]),
                                  int(date_date_begin[4:6]),
                                  int(date_date_begin[6:8])) \
            + _datetime.timedelta(seconds = 3600 * int(date_date_begin[8:10]))

    @staticmethod
    def GetVariableName():
        return list(_core.TrajectoryBase.GetVariableName())

    def GetDate(self, i):
        return TrajectoryBase.GetDataDateBegin() + \
            _datetime.timedelta(seconds = i * self.GetTimeStep())

    def GetDateBegin(self):
        return self.GetDate(0)

    def GetDateEnd(self):
        return self.GetDate(self.GetNt())

    def GetVariable(self, name):
        variable = _core.vector2r()
        _core.TrajectoryBase.GetVariable(self, name, variable)
        return _numpy.array(variable, dtype = _numpy.float)

    def GetLongitude(self):
        longitude = _core.vector1r()
        _core.TrajectoryBase.GetLongitude(self, longitude)
        return _numpy.array(longitude, dtype = _numpy.float)

    def GetLatitude(self):
        latitude = _core.vector1r()
        _core.TrajectoryBase.GetLatitude(self, latitude)
        return _numpy.array(latitude, dtype = _numpy.float)

    def GetAltitude(self):
        altitude = _core.vector1r()
        _core.TrajectoryBase.GetAltitude(self, altitude)
        return _numpy.array(altitude, dtype = _numpy.float)

    def ReadConcentration(self, t, z = 0):
        concentration_aer_num = _core.vector1r()
        concentration_aer_mass = _core.vector1r()
        concentration_gas = _core.vector1r()
        _core.TrajectoryBase.ReadConcentration(self, t, z,
                                               concentration_aer_num,
                                               concentration_aer_mass,
                                               concentration_gas)

        return _numpy.array(concentration_aer_num, dtype = _numpy.float), \
            _numpy.array(concentration_aer_mass, dtype = _numpy.float), \
            _numpy.array(concentration_gas, dtype = _numpy.float)

    def Run(self,
            concentration_aer_num,
            concentration_aer_mass,
            concentration_gas,
            **kwargs):

        concentration_aer_num_vect = _core.vector2r(concentration_aer_num)
        concentration_aer_mass_vect = _core.vector2r(concentration_aer_mass)
        concentration_gas_vect = _core.vector2r(concentration_gas)

        if not kwargs.has_key("amx"):
            kwargs["amx"] = "EulerMixedBaseMoving"

        if kwargs["amx"] == "EulerMixedBaseMoving":
            _core.TrajectoryBase.Run_AMX_EulerMixedBaseMoving(self,
                                                              concentration_aer_num_vect,
                                                              concentration_aer_mass_vect,
                                                              concentration_gas_vect)
        else:
            raise NotImplementedError("Not implemented \"" + kwargs["amx"] + "\".")

        return _numpy.array(concentration_aer_num_vect, dtype = _numpy.float), \
            _numpy.array(concentration_aer_mass_vect, dtype = _numpy.float), \
            _numpy.array(concentration_gas_vect, dtype = _numpy.float)


if _core.DRIVER_HAS_TRAJECTORY_CHIMERE > 0:
    class TrajectoryChimere(_core.TrajectoryChimere, TrajectoryBase):
        @staticmethod
        def Run(**kwargs):
            if not kwargs.has_key("amx"):
                kwargs["amx"] = "EulerMixedBaseMoving"

            if kwargs["amx"] == "EulerMixedBaseMoving":
                _core.TrajectoryBase.Run_AMX_EulerMixedBaseMoving()
            else:
                raise NotImplementedError("Not implemented \"" + kwargs["amx"] + "\".")


if _core.DRIVER_HAS_TRAJECTORY_POLAIR3D > 0:
    class TrajectoryPolair3d(_core.TrajectoryPolair3d, TrajectoryBase):
        pass
