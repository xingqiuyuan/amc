#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core
import numpy as _numpy


class DiscretizationSize(_core.DiscretizationSize):
    @staticmethod
    def GetDiameterBound():
        diameter_bound = _core.vector1r()
        _core.DiscretizationSize__GetDiameterBound_(diameter_bound)
        return _numpy.array(diameter_bound, dtype = _numpy.float)


class DiscretizationClassBase(_core.DiscretizationClassBase):
    pass


class AerosolData(_core.AerosolData):
    @staticmethod
    def GetGeneralSection():
        general_section = _core.vector2i()
        _core.AerosolData.GetGeneralSection(general_section)
        return _to_array(general_section)


class AMC(_core.AMC_Base):
    pass


class AMX_EulerMixedBaseMoving(_core.AMX_EulerMixedBaseMoving):
    pass
