#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core


if _core.DRIVER_HAS_PARAMETERIZATION_PHYSICS > 0:
    class ParameterizationPhysics(_core.ClassParameterizationPhysics):
        pass

if _core.DRIVER_HAS_MONAHAN > 0:
    class ParameterizationEmissionSeaSaltMonahan(_core.ParameterizationEmissionSeaSaltMonahan):
        pass
