#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core

import numpy as _numpy
import datetime as _datetime
from base import DriverBase as _DriverBase


class ChimereBase(_core.ChimereBase):
    @staticmethod
    def GetParameter():
        return dict(_core.ChimereBase.GetParameter())

    @staticmethod
    def GetFileName():
        return dict(_core.ChimereBase.GetFileName())


if _core.DRIVER_HAS_EMISSION > 0:
    class ChimereEmissionAnthropicBase(_core.ChimereEmissionAnthropicBase):
        pass

    class ChimereEmissionAnthropicTable(_core.ChimereEmissionAnthropicTable):
        pass

    class ChimereEmissionBiogenicBase(_core.ChimereEmissionBiogenicBase):
        pass

    class ChimereEmissionBiogenicTable(_core.ChimereEmissionBiogenicTable):
        pass


if _core.CHIMERE_HAS_AMC > 0:
    class CHIMERE_AMX_EulerMixedBaseMoving(_core.CHIMERE_AMX_EulerMixedBaseMoving, ChimereBase):
        pass
else:
    class CHIMERE(_core.CHIMERE, ChimereBase):
        pass


class ChimereBoundaryConditionBase(_core.ChimereBoundaryConditionBase):
    pass


class ChimereBoundaryConditionTable(_core.ChimereBoundaryConditionTable):
    pass
