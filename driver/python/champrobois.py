#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core
import datetime as _datetime
import numpy as _numpy
import time as _time
import os as _os
import sys as _sys
from util import to_array as _to_array
from util import get_shelve_descriptor as _get_shelve_descriptor
from util import LoggerDriver as _LoggerDriver
import shelve as _shelve
import copy as _copy
from inspect import currentframe as _cf
import struct as _struct


if _core.DRIVER_CHAMPROBOIS_HAS_SOCKET > 0:
    import socket as _socket


class ChamproboisBase(_core.ChamproboisBase):
    @staticmethod
    def GetSourceList():
        return list(_core.ChamproboisBase.GetSourceList())

    @staticmethod
    def GetSpeciationList():
        return list(_core.ChamproboisBase.GetSpeciationList())


class ChamproboisGranulometry(_core.ChamproboisGranulometry):

    def GetKey(self):
        return _core.ChamproboisGranulometry.GetKey(self).split(".")

    def SetTime(self, time):
        time_vec = _core.vector1r(time)
        _core.ChamproboisGranulometry.SetTime(self, time)

    def SetParticleDiameter(self, diameter_bound, diameter_mean):
        diameter_bound_vec = _core.vector1r(diameter_bound)
        diameter_mean_vec = _core.vector1r(diameter_mean)
        _core.ChamproboisGranulometry.SetParticleDiameter(self, diameter_bound_vec, diameter_mean_vec)

    def SetParticleNumberConcentration(self, concentration_aer_number):
        concentration_aer_number_vec = _core.vector2r(concentration_aer_number)
        _core.ChamproboisGranulometry.SetParticleNumberConcentration(self, concentration_aer_number_vec)

    def Load(self, key = None):
        if key == None: key = self.GetKey()

        fs = _shelve.open(_core.ChamproboisBase.GetRawDataFile())
        fvalue = _get_shelve_descriptor(fs, key)
 
        dates = [_time.mktime(d.timetuple()) for d in fvalue["dates"]]
        date_begin = dates[0]
        times = _numpy.array([dates[i] - dates[0] for i in range(len(dates))], dtype = _numpy.float)

        # Number concentration in #.m^{-3}
        number = _copy.copy(fvalue["number"])
        if "cm3" in fs["units"]["number"]:
            number *= 1.e6

        diameter_bound = _copy.copy(fvalue["D50"])
        diameter_mean = _copy.copy(fvalue["Di"])
        density_fixed = fvalue["rho"]
        instrument = fvalue["instrument"]

        fs.close()

        self.SetDateBegin(date_begin)
        self.SetTime(times)
        self.SetParticleDensity(density_fixed)
        self.SetInstrument(instrument)
        self.SetParticleDiameter(diameter_bound, diameter_mean)
        self.SetParticleNumberConcentration(number)

    def GetDateBegin(self):
        date_begin = _core.ChamproboisGranulometry.GetDateBegin(self)
        return _datetime.datetime.fromtimestamp(date_begin)

    def GetDate(self):
        date_begin = _core.ChamproboisGranulometry.GetDateBegin(self)
        return [_datetime.datetime.fromtimestamp(date_begin + d) for d in self.GetTime()]

    def GetTime(self):
        time_vec = _core.vector1r()
        _core.ChamproboisGranulometry.GetTime(self, time_vec)
        return _numpy.array(time_vec, dtype = _numpy.float)

    def GetDiameterBound(self):
        return _numpy.array(_core.ChamproboisGranulometry.GetDiameterBound(self), dtype = _numpy.float)

    def GetDiameterMean(self):
        return _numpy.array(_core.ChamproboisGranulometry.GetDiameterMean(self), dtype = _numpy.float)

    def GetConcentrationAerNumber(self):
        concentration_aer_number = _core.vector2r()
        _core.ChamproboisGranulometry.GetConcentrationAerNumber(self, concentration_aer_number)
        return _to_array(concentration_aer_number)

    def GetConcentrationAerMassTotal(self):
        concentration_aer_mass_total = _core.vector2r()
        _core.ChamproboisGranulometry.GetConcentrationAerMassTotal(self, concentration_aer_mass_total)
        return _to_array(concentration_aer_mass_total)

    def GetConcentrationAerMassTotalFraction(self):
        concentration_aer_mass_total_fraction = _core.vector1r()
        _core.ChamproboisGranulometry.GetConcentrationAerMassTotalFraction(self, concentration_aer_mass_total_fraction)
        return _numpy.array(concentration_aer_mass_total_fraction, dtype = _numpy.float)


class ChamproboisSpeciation(_core.ChamproboisSpeciation):

    def GetKey(self):
        return _core.ChamproboisSpeciation.GetKey(self).split(".")

    def GetSpeciesNameList(self):
        species_name = _core.vector1s()
        _core.ChamproboisSpeciation.GetSpeciesNameList(self, species_name)
        return list(species_name)

    def GetSpeciesKeyTotal(self):
        species_key = _core.vector1s()
        _core.ChamproboisSpeciation.GetSpeciesKeyTotal(self, species_key)
        return list(species_key)

    def GetSpeciesKey(self, i):
        species_key = _core.vector1s()
        _core.ChamproboisSpeciation.GetSpeciesKey(self, i, species_key)
        return list(species_key)

    def GetConcentration(self, species_name = []):
        gas = _core.vector1r()
        aer = _core.vector1r()
        _core.ChamproboisSpeciation.GetConcentration(self, gas, aer, species_name)

        return {"gas" : _numpy.array(gas, dtype = _numpy.float),
                "aer" : _numpy.array(aer, dtype = _numpy.float)}

    def _load_species_total_(self, funits, fvalue):
        species_key_total = self.GetSpeciesKeyTotal()
        species_total = [-1., -1.]

        for j in range(2):
            if len(species_key_total[j]) > 0:
                if fvalue.has_key(species_key_total[j]):
                    species_total[j] = fvalue[species_key_total[j]]

                    if "mg" in funits[species_key_total[j]]:
                        species_total[j] *= 1.e3
                    elif "ng" in funits[species_key_total[j]]:
                        species_total[j] *= 1.e-3

                    if "gaz secs" in funits[species_key_total[j]]:
                        species_total[j] *= fvalue["ratio_from_mo3_to_m3"]
                else:
                    msg = "Unable to find key \"%s\" for species \"%s\" and speciation \"%s\"." \
                    % (species_key[j], self.GetSpeciesName(i), self.GetName())
                    if _core.DRIVER_HAS_LOGGER > 0: _LoggerDriver.GetLog().ERROR(msg)
                    raise ValueError(msg)

        return species_total


    def _load_species_(self, i, funits, fvalue):
        species_key = self.GetSpeciesKey(i)
        species = [-1., -1.]

        for j in range(2):
            if len(species_key[j]) > 0:
                if fvalue.has_key(species_key[j]):
                    species[j] = fvalue[species_key[j]]

                    if "mg" in funits[species_key[j]]:
                        species[j] *= 1.e3
                    elif "ng" in funits[species_key[j]]:
                        species[j] *= 1.e-3

                    if "gaz secs" in funits[species_key[j]]:
                        species[j] *= fvalue["ratio_from_mo3_to_m3"]
                else:
                    msg = "Unable to find key \"%s\" for species \"%s\" and speciation \"%s\"." \
                    % (species_key[j], self.GetSpeciesName(i), self.GetName())
                    if _core.DRIVER_HAS_LOGGER > 0: _LoggerDriver.GetLog().ERROR(msg)
                    raise ValueError(msg)

        return species


    def Load(self, key = None):
        if key == None: key = self.GetKey()

        fs = _shelve.open(_core.ChamproboisBase.GetRawDataFile())

        funits = fs["units"]
        fvalue = _get_shelve_descriptor(fs, key)

        species_total = self._load_species_total_(funits, fvalue)
        _core.ChamproboisSpeciation.SetSpeciesTotal(self, species_total[0], species_total[1])

        for i in range(self.GetNspecies()):
            species = self._load_species_(i, funits, fvalue)
            _core.ChamproboisSpeciation.SetSpecies(self, i, species[0], species[1])

        fs.close()


    def Dump(self, **kwargs):
        argument = _core.map_string_int(kwargs)
        _core.ChamproboisSpeciation.Dump(self, argument)


class ChamproboisSink(_core.ChamproboisSink):
    def GetDiameterBound(self):
        diameter_bound = _core.vector1r()
        _core.ChamproboisSink.GetDiameterBound(self, diameter_bound)
        return _numpy.array(diameter_bound, dtype = _numpy.float)


class ChamproboisFilter(_core.ChamproboisFilter, ChamproboisSpeciation, ChamproboisSink):
    def Dump(self, **kwargs):
        argument = _core.map_string_int(kwargs)
        _core.ChamproboisFilter.Dump(self, argument)


    def GetConcentrationVolatile(self):
        concentration_volatile = _core.vector1r()
        _core.ChamproboisFilter.GetConcentrationVolatile(self, concentration_volatile)
        return _numpy.array(concentration_volatile, dtype = _numpy.float)


class ChamproboisELPI(_core.ChamproboisELPI):
    def Dump(self, **kwargs):
        argument = _core.map_string_int(kwargs)
        _core.ChamproboisELPI.Dump(self, argument)

    def GetConcentrationNumber(self):
        concentration_number = _core.vector2r()
        _core.ChamproboisELPI.GetConcentrationNumber(self, concentration_number)
        return _to_array(concentration_number)


    def GetConcentrationMass(self):
        concentration_mass = _core.vector2r()
        _core.ChamproboisELPI.GetConcentrationMass(self, concentration_mass)
        return _to_array(concentration_mass)


    def Write(self, path):
        fs = _shelve.open(path)
        fs["Nsample"] = self.GetNsample()
        fs["Nsection"] = self.GetNsection()
        fs["sample_rate"] = self.GetSampleRate()
        fs["diameter_bound"] = self.GetDiameterBound()
        fs["concentration"] = {"number" : self.GetConcentrationNumber(), "mass" : self.GetConcentrationMass()}
        fs.close()
        

class ChamproboisSource(_core.ChamproboisSource, ChamproboisGranulometry):

    def GetConcentrationGasMass(self):
        concentration_gas_mass = _core.vector1r()
        _core.ChamproboisSource.GetConcentrationGasMass(self, concentration_gas_mass)
        return _numpy.array(concentration_gas_mass, dtype = _numpy.float)


    def GetSpeciesNameList(self):
        return list(_core.ChamproboisSource.GetSpeciesNameList(self))


    def GetFractionAerMass(self):
        fraction_aer_mass = _core.vector2r()
        _core.ChamproboisSource.GetFractionAerMass(self, fraction_aer_mass)
        return _to_array(fraction_aer_mass)


    def GetGasIndex(self):
        return _numpy.array(_core.ChamproboisSource.GetGasIndex(self), dtype = _numpy.int)


    def Compute(self, t, time_step):
        source = _core.vector1r()
        _core.ChamproboisSource.Compute(self, t, time_step, source)
        source = _numpy.array(source, dtype = _numpy.float)

        i = self.GetNsection()
        j = i + self.GetNsection() * self.GetNspecies()
        k = j + self.GetNgas()

        return {"aer" : {"number" : source[0:i], "mass" : source[i:j]},
                "gas" : source[j:k], "water" : source[self.GetNs()]}


    def Load(self):
        ChamproboisGranulometry.Load(self)
        fs = _shelve.open(_core.ChamproboisBase.GetRawDataFile())
        key = self.GetName()
        if fs[key].has_key("comment") : self.SetComment(fs[key]["comment"])
        self.SetTemperature(fs[key]["temperature"]["champ_tres_proche"])
        self.SetConcentrationWater(fs[key]["humidity"]["champ_tres_proche"], fs[key]["pressure"])
        self.SetFractionAerMass()
        fs.close()


    def AddGas(self, name, key = []):
        fs = _shelve.open(_core.ChamproboisBase.GetRawDataFile())
        funits = fs["units"]
        fvalue = _get_shelve_descriptor(fs, self.GetKey() + key)

        concentration_gas_mass = fvalues[name.lower()]

        if "mg" in funits[name.lower()]:
            concentration_gas_mass *= 1.e3
        elif "ng" in funits[name.lower()]:
            concentration_gas_mass *= 1.e-3

        if "gaz secs" in funits[name.lower()]:
            concentration_gas_mass *= fvalue["ratio_from_mo3_to_m3"]

        _core.ChamproboisSource.AddGas(self, name, concentration_gas_mass)
        fs.close()


    def Order(self, species_name, semivolatile):
        species_name_vec = _core.vector1s(species_name)
        semivolatile_vec = _core.vector1i(semivolatile)
        _core.ChamproboisSource.Order(self, species_name_vec, semivolatile_vec)


    def AddSpeciation(self, speciation, fraction_section):
        fraction_section_vec = _core.vector1r(fraction_section)
        _core.ChamproboisSource.AddSpeciation(self, speciation, fraction_section_vec)


class DriverChamprobois(_core.DriverChamprobois):

    def Load(self):
        fs = _shelve.open(_core.ChamproboisBase.GetRawDataFile())
        key = self.GetKey()
        self.SetTemperature(fs[key]["temperature"]["champ_proche"])
        self.SetConcentrationWater(fs[key]["humidity"]["champ_proche"])
        self.SetPressure(fs[key]["pressure"])
        self.SetSpeed(fs[key]["speed"])
        self.SetTurbulentDiffusion()
        fs.close()


    def Run(self, time_out = 0., passive = False, **kwargs):
        if not kwargs.has_key("amx"):
            kwargs["amx"] = "EulerMixedBaseMoving"

        if kwargs["amx"] == "EulerMixedBaseMoving":
            _core.DriverChamprobois.Run_AMX_EulerMixedBaseMoving(self, time_out, passive)
        else:
            raise NotImplementedError("Not implemented \"" + kwargs["amx"] + "\".")


    def Forward(self, source, temperature, passive = False, **kwargs):
        if not kwargs.has_key("amx"):
            kwargs["amx"] == "EulerMixedBaseMoving"

        source_vec = _core.vector1r(source)
        _core.DriverChamprobois.Forward(self, source, temperature)

        if not passive:
            if kwargs["amx"] == "EulerMixedBaseMoving":
                _core.DriverChamprobois.Forward_AMX_EulerMixedBaseMoving(self)
            else:
                raise NotImplementedError("Not implemented \"" + kwargs["amx"] + "\".")


    if _core.DRIVER_CHAMPROBOIS_HAS_SOCKET > 0:
        def _receive_data_(self, connexion, datasize, bufsize = 4096, datalen = 8):
            buf = ""
            buflen = datasize * datalen
            while len(buf) < buflen:
                bufrem = buflen - len(buf)
                if bufrem < bufsize:
                    bufsize = bufrem
                buf += connexion.recv(bufsize)
            return _numpy.fromstring(buf, dtype = _numpy.float)


        def Receive(self, path = ""):
            if _core.DRIVER_HAS_LOGGER > 0 or _core.DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER > 0:
                filename = _os.path.basename(_cf().f_code.co_filename)
                function = _cf().f_code.co_name

            if len(path) > 0:
                self.SetPath(path)

            # Try to open one connection, if fail, just silently exit.
            try:
                sock = _socket.socket(_socket.AF_INET, _socket.SOCK_STREAM)
                sock.setsockopt(_socket.SOL_SOCKET, _socket.SO_REUSEADDR, 1)
                sock.bind((self.GetHostName(), self.GetPortNumber()))
            except Exception:
                sock.close()
                return
            else:
                sock.listen(5)

            if _core.DRIVER_HAS_LOGGER > 0:
                _core.DriverLogger_GetLog().INFO0("Listen at \"%s:%d\" for simulation \"%s\"."
                                                  % (self.GetHostName(), self.GetPortNumber(), self.GetName()),
                                                  filename, _cf().f_lineno, function)

            source = self.GetSourcePtr()

            Nz = self.GetNz()
            Nr = self.GetNr()
            Nsection = source.GetNsection()
            Nspecies = source.GetNspecies()
            Ns = source.GetNs()

            temperature = _numpy.zeros([Nz + 2, Nr + 2], dtype = _numpy.float)
            relative_humidity = _numpy.zeros([Nz, Nr], dtype = _numpy.float)
            concentration = _numpy.zeros([Nz + 2, Nr + 2, Ns + 1], dtype = _numpy.float);

            fs = _shelve.open(self.GetPath(), flag = "n", writeback = True)
            if _core.DRIVER_HAS_LOGGER > 0:
                _core.DriverLogger_GetLog().INFO1("Open path \"" + self.GetPath() + "\" for (shelve) writing.",
                                                  filename, _cf().f_lineno, function)

            fs["chimney"] = {"length" : _core.ChamproboisBase.GetChimneyLength(),
                             "radius" : _core.ChamproboisBase.GetChimneyRadius()}

            fs["name"] = self.GetName()
            fs["Nsection"] = Nsection
            fs["Nspecies"] = Nspecies
            fs["diameter_bound"] = _numpy.array(source.GetDiameterBound(), dtype = _numpy.float)
            fs["species_name"] = list(source.GetSpeciesNameList())
            fs["species_label"] = [source.GetSpeciesLabel(i) for i in range(source.GetNspecies())]
            fs["gas_index"] = _numpy.array(source.GetGasIndex(), dtype = _numpy.int)
            fs["Ngas"] = source.GetNgas()
            fs["Ns"] = Ns
            fs["Nz"] = Nz
            fs["Nr"] = Nr

            fs["date"] = []
            fs["temperature"] = []
            fs["relative_humidity"] = []
            fs["concentration"] = []

            Nt = 0
            end = False
            while not end:
                if _core.DRIVER_HAS_LOGGER > 0:
                    _core.DriverLogger_GetLog().DEBUG1("Accepting connexions ...", filename,
                                                       _cf().f_lineno, _cf().f_code.co_name)

                connexion, address = sock.accept()

                t = connexion.recv(4)
                t = _struct.unpack("<i", t)[0]
                date = _datetime.datetime.fromtimestamp(t)

                if _core.DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER > 0:
                    _sys.stderr.write("%s:%s:%s:Receiving data for t = %d, date = \"%s\".\n" %
                                      (filename, _cf().f_lineno, function, t, date_str))
                elif _core.DRIVER_HAS_LOGGER > 0:
                    _core.DriverLogger_GetLog().INFO3("Receiving data for t = %d, date = \"%s\"." %
                                                      (t, date.strftime("%H:%M:%S")),
                                                      filename, _cf().f_lineno, _cf().f_code.co_name)

                temperature = self._receive_data_(connexion, (Nz + 2) * (Nr + 2))
                temperature.shape = [Nz + 2, Nr + 2]

                if _core.DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER > 0:
                    _sys.stderr.write("%s:%s:%s:temperature (min, mean, max) = (%3.2f, %3.2f, %3.2f).\n" %
                                      (filename, _cf().f_lineno, function, temperature.min(),
                                       temperature.mean(), temperature.max()))

                relative_humidity = self._receive_data_(connexion, Nz * Nr)
                relative_humidity.shape = [Nz, Nr]

                if _core.DRIVER_HAS_DEBUG_CHAMPROBOIS_DRIVER > 0:
                    _sys.stderr.write("%s:%s:%s:relative_humidity (min, mean, max) = (%3.2f, %3.2f, %3.2f).\n" %
                                      (filename, _cf().f_lineno, function, relative_humidity.min(),
                                       relative_humidity.mean(), relative_humidity.max()))

                concentration = self._receive_data_(connexion, (Nz + 2) * (Nr + 2) * (Ns + 1))
                concentration.shape = [Nz + 2, Nr + 2, Ns + 1]

                end = connexion.recv(1)
                end = _struct.unpack("b", end)[0]

                connexion.close()

                fs["date"].append(date)
                fs["temperature"].append(temperature[1:Nz + 1, 1:Nr + 1])
                fs["relative_humidity"].append(relative_humidity)
                fs["concentration"].append(concentration[1:Nz + 1, 1:Nr + 1, 0:Ns + 1])
                Nt += 1

            fs["Nt"] = Nt
            fs["temperature"] = _numpy.array(fs["temperature"])
            fs["relative_humidity"] = _numpy.array(fs["relative_humidity"])
            fs["concentration"] = _numpy.array(fs["concentration"])

            if _core.DRIVER_HAS_LOGGER > 0:
                _core.DriverLogger_GetLog().INFO1("Received end, closing files.", filename,
                                                  _cf().f_lineno, _cf().f_code.co_name)

            sock.close()
            fs.close()
