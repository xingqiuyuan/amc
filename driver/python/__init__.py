#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as core

if core.DRIVER_HAS_MPI == 1:
    from mpi4py import MPI
else:
    print "Driver built without MPI support."

import parameterization

if core.DRIVER_HAS_EMISSION > 0:
    import emission

if core.DRIVER_HAS_TRAJECTORY > 0:
    import trajectory

if core.DRIVER_HAS_CHIMERE > 0:
    import chimere

if core.DRIVER_HAS_GEO_DATA > 0:
    import geo_data

if core.DRIVER_HAS_MAPPING > 0:
    import mapping

if core.DRIVER_HAS_CHAMPROBOIS > 0:
    import champrobois

if core.DRIVER_HAS_NUMERICAL_SOLVER > 0:
    import numerics

import display
import util
import amc
