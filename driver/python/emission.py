#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import driver_core as _core

import numpy as _numpy
import pylab as _pylab
import copy as _copy
import os as _os
from util import to_array as _to_array
from amc import AerosolData as _AerosolData


if _os.path.isfile(_os.path.dirname(_os.path.realpath(__file__)) + "/../emission/README"):
    README = open(_os.path.dirname(_os.path.realpath(__file__)) + "/../emission/README").read()


class Tracer(_core.Tracer):

    @staticmethod
    def GetSpeciesIndex(i):
        species_index = _core.vector1i()
        _core.Tracer.GetSpeciesIndex(i, species_index)
        return _numpy.array(species_index, dtype = _numpy.int)

    @staticmethod
    def GetSpeciesMassFraction(i):
        species_fraction = _core.vector1r()
        _core.Tracer.GetSpeciesMassFraction(i, species_mass_fraction)
        return _numpy.array(species_mass_fraction, dtype = _numpy.float)


class Source(_core.Source):

    @staticmethod
    def GetTracerIndex(i):
        tracer_index = _core.vector1i()
        _core.Source.GetTracerIndex(i, tracer_index)
        return _numpy.array(tracer_index, dtype = _numpy.int)

    @staticmethod
    def GetSizeIndex(i):
        size_index = _core.vector1i()
        _core.Source.GetSizeIndex(i, size_index)
        return _numpy.array(size_index, dtype = _numpy.int)

    @staticmethod
    def GetSizeDiameterMean(i):
        size_diameter_mean = _core.vector1r()
        _core.Source.GetSizeDiameterMean(i, size_diameter_mean)
        return _numpy.array(size_diameter_mean, dtype = _numpy.float)

    @staticmethod
    def GetSizeFractionNumber(i):
        size_fraction_number = _core.vector1r()
        _core.Source.GetSizeFractionNumber(i, size_fraction_number)
        return _numpy.array(size_fraction_number, dtype = _numpy.float)

    @staticmethod
    def GetSizeFractionMass(i):
        size_fraction_mass = _core.vector1r()
        _core.Source.GetSizeFractionMass(i, size_fraction_mass)
        return _numpy.array(size_fraction_mass, dtype = _numpy.float)


class DynamicsEmissionBase(_core.DynamicsEmissionBase):

    @staticmethod
    def GetSourceGeneralSection():
        source_general_section = _core.vector2i()
        _core.DynamicsEmissionBase.GetSourceGeneralSection(source_general_section)
        return _to_array(source_general_section)


    @staticmethod
    def GetSourceGeneralSectionCount(**kwargs):
        source_general_section_count_raw = _core.vector2i()
        _core.DynamicsEmissionBase.GetSourceGeneralSectionCount(source_general_section_count_raw)
        source_general_section_count_raw = _to_array(source_general_section_count_raw)

        if kwargs.get("raw", False):
            return source_general_section_count_raw

        general_section = _AerosolData.GetGeneralSection()

        source_general_section_count = {}
        for i in range(len(source_general_section_count_raw)):
            source_index = source_general_section_count_raw[i, 0]
            source_name = Source.GetName(source_index)

            if not source_general_section_count.has_key(source_name):
                source_general_section_count[source_name] = {}

            size_index = source_general_section_count_raw[i, 1]                
            if not source_general_section_count[source_name].has_key(size_index):
                source_general_section_count[source_name][size_index] = {}

            count_sum = source_general_section_count_raw[i, 2:].sum()
            for g,count in enumerate(source_general_section_count_raw[i, 2:]):
                if count > 0:
                    composition_index = general_section[g, 1]
                    composition_name = _core.DiscretizationClassBase. \
                                       GetDiscretizationCompositionBase(size_index). \
                                       GetClassInternalMixing(composition_index).GetName()
                    if kwargs.get("percentage", True):
                        source_general_section_count[source_name][size_index][composition_name] \
                            = float(count) / float(count_sum)
                    else:
                        source_general_section_count[source_name][size_index][composition_name] = count

        return source_general_section_count


    @staticmethod
    def Rate(emission_rate_mass, emission_rate_number,
             section_min = 0, section_max = 0):

        if section_max <= 0:
            section_max += _core.AerosolData.GetNg()

        emission_rate_mass_vect = _core.vector1r(emission_rate_mass)
        emission_rate_number_vect = _core.vector1r(emission_rate_number)
        rate_aer_number = _core.vector1r(_core.AerosolData.GetNg(), float(0))
        rate_aer_mass = _core.vector1r(_core.AerosolData.GetNgNspecies(), float(0))

        _core.DynamicsEmissionBase.Rate(section_min, section_max,
                                        emission_rate_mass_vect,
                                        emission_rate_number_vect,
                                        rate_aer_number,
                                        rate_aer_mass)

        return _numpy.array(rate_aer_number, dtype = _numpy.float), \
            _numpy.array(rate_aer_mass, dtype = _numpy.float), \
            _numpy.array(emission_rate_number_vect, dtype = _numpy.float)


class DynamicsEmissionTable(_core.DynamicsEmissionTable):
    pass
