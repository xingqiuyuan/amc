#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import driver_core as _core

import numpy as _numpy
import pylab as _pylab
import os as _os
import amc as _amc
from util import to_array as _to_array


class MappingSize(_core.MappingSize):

    def GetFractionIndex(self):
        fraction_index_vec = _core.vector2i()
        _core.MappingSize.GetFractionIndex(self, fraction_index_vec)
        return _to_array(fraction_index_vec)


    def GetFractionNumber(self):
        fraction_number_vec = _core.vector2r()
        _core.MappingSize.GetFractionNumber(self, fraction_number_vec)
        return _to_array(fraction_number_vec)


    def GetFractionMass(self):
        fraction_mass_vec = _core.vector2r()
        _core.MappingSize.GetFractionMass(self, fraction_mass_vec)
        return _to_array(fraction_mass_vec)


if _core.DRIVER_HAS_TEST > 0:

    class MappingSizeTest(MappingSize, _core.MappingSizeTest):

        def RunSelf(self, diameter_min, diameter_max, diameter_mean):
            fraction_index = _core.vector1i()
            fraction_number = _core.vector1r()
            fraction_mass = _core.vector1r()
            diameter_bound_amc = _core.vector1r()
            _core.MappingSizeTest.RunSelf(self, diameter_min, diameter_max, diameter_mean,
                                          fraction_index, fraction_number, fraction_mass)
            return {"index" : _numpy.array(fraction_index, dtype = _numpy.int), \
                    "number" : _numpy.array(fraction_number, dtype = _numpy.float), \
                    "mass" : _numpy.array(fraction_mass, dtype = _numpy.float)}


        def Plot(self, diameter_min, diameter_max, diameter_mean, **kwargs):
            width = kwargs.get("width", 29)
            width /= 2.54

            _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

            margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
            if kwargs.has_key("margins"):
                for k,v in kwargs["margins"].iteritems():
                    margins[k] = v

            _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
            _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
            _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
            _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

            font_size = kwargs.get("font_size", 12)

            _pylab.matplotlib.rcParams["font.size"] = font_size
            _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
            _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
            _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
            _pylab.matplotlib.rcParams["xtick.major.size"] = 2
            _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
            _pylab.matplotlib.rcParams["ytick.major.size"] = 2
            _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

            bar_dark_max = kwargs.get("bar_dark_max", 0.8)

            _pylab.clf()
            fig, (ax1, ax2) = _pylab.subplots(nrows = 2)

            data = self.RunSelf(diameter_min, diameter_max, diameter_mean)
            diameter_bound_amc = _amc.DiscretizationSize.GetDiameterBound()

            xticks_location = list(diameter_bound_amc)
            xticks_label = ["%1.2g" % x for x in xticks_location]

            for x in [diameter_min, diameter_max, diameter_mean]:
                xticks_location.append(x)
                xticks_label.append("\n%1.2g" % x)

            # Make bar plots.
            ax1.set_xscale("log")
            ax1.bar(diameter_min, 1, width = diameter_max - diameter_min, linewidth = 0, color = "c")
            ax1.axvline(diameter_mean, lw = 3, color = "m")
            ax1.axvline(diameter_min, lw = 2, color = "k", ls = "--")
            ax1.axvline(diameter_max, lw = 2, color = "k", ls = "--")

            for i in range(_amc.DiscretizationSize.GetNsection()):
                ax1.bar(diameter_bound_amc[i], data["number"][i],
                        width = diameter_bound_amc[i + 1] - diameter_bound_amc[i],
                        linewidth = 1, color = str(1.0 - data["number"][i] * bar_dark_max))

            ax1.set_ylabel("Number fraction []")
            ax1.set_xticks(xticks_location)
            ax1.set_xticklabels(xticks_label)
            ax1.get_xticklabels()[-1].set_color("m")
            ax1.get_xticklabels()[-2].set_color("#009999")
            ax1.get_xticklabels()[-3].set_color("#009999")
            ax3 = ax1.twinx()

            ax2.set_xscale("log")
            ax2.bar(diameter_min, 1, width = diameter_max - diameter_min, linewidth = 0, color = "c")
            ax2.axvline(diameter_mean, lw = 3, color = "m")
            ax2.axvline(diameter_min, lw = 2, color = "k", ls = "--")
            ax2.axvline(diameter_max, lw = 2, color = "k", ls = "--")

            for i in range(_amc.DiscretizationSize.GetNsection()):
                ax2.bar(diameter_bound_amc[i], data["mass"][i],
                        width = diameter_bound_amc[i + 1] - diameter_bound_amc[i],
                        linewidth = 1, color = str(1.0 - data["mass"][i] * bar_dark_max))

            ax2.set_ylabel("Mass fraction []")
            ax2.set_xticks(xticks_location)
            ax2.set_xticklabels(xticks_label)
            ax2.get_xticklabels()[-1].set_color("m")
            ax2.get_xticklabels()[-2].set_color("#009999")
            ax2.get_xticklabels()[-3].set_color("#009999")
            ax3 = ax2.twinx()

            ax1.set_title(r"Mapping of section [%1.3g, %1.3g] (centered on %1.3g $\mu m$) on AMC size discretization."
                          % (diameter_min, diameter_max, diameter_mean), x = 0.5, y = 1.1)
            ax2.set_xlabel(r"AMC diameters [$\mu m$]")

            if kwargs.has_key("output_file"):
                _pylab.savefig(kwargs["output_file"])
                _pylab.clf()
            elif kwargs.has_key("show"):
                _pylab.show()

            return fig


class MappingSpecies(_core.MappingSpecies):
    pass


class MappingParticleBase(_core.MappingParticleBase):
    pass


class MappingParticleTable(_core.MappingParticleTable):
    pass
