// Copyright (C) 2013-2015 Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

%module(directors="1") driver_core
%{
#include "DriverHeader.hxx"
#define SWIG_PYTHON_EXTRA_NATIVE_CONTAINERS
  typedef double real;

namespace AMC
{
#include "typedef.hxx"
}
#ifdef DRIVER_WITH_CHIMERE
#include "chimere/ChimereTemplate.hxx"
#endif
  %}

%include <typemaps.i>
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

namespace std
{
  %template(vector1i)  vector<int>;
  %template(vector1s)  vector<string>;
  %template(vector1r)  vector<double>;
  %template(vector2i)  vector<vector<int> >;
  %template(vector2r)  vector<vector<double> >;
  %template(vector2s)  vector<vector<string> >;
  %template(vector1f)  vector<float>;
  %template(map_string_int)    map<string, int>;
  %template(map_string_real)   map<string, double>;
  %template(map_string_string) map<string, string>;

  %extend vector<vector<int> >
  {
    int __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<int>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, int value)
    {
      (*self)[i][j] = value;
    }
  }

  %extend vector<vector<double> >
  {
    double __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<double>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, double value)
    {
      (*self)[i][j] = value;
    }
  }

  %extend vector<vector<string> >
  {
    string __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<string>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, string value)
    {
      (*self)[i][j] = value;
    }
  }
}

using namespace std;

%include "carrays.i"
%array_class(double, ArrayReal);

#ifdef AMC_WITH_LOGGER
#define LOGGER_WITH_OPS
#define LOGGER_WITH_TIME
#define LOGGER_WITH_FORMAT
#ifdef DRIVER_WITH_MPI
#define LOGGER_WITH_MPI
#endif
%include "logger.i"
using namespace Logger;
#endif

%exception
{
  try
    {
      $action
        }
  catch(AMC::Error& e)
    {
      PyErr_SetString(PyExc_Exception, e.What().c_str());
      return NULL;
    }
  catch(Ops::Error& e)
    {
      PyErr_SetString(PyExc_Exception, e.What().c_str());
      return NULL;
    }
  catch(std::exception& e)
    {
      PyErr_SetString(PyExc_Exception, e.what());
      return NULL;
    }
  catch(string& s)
    {
      PyErr_SetString(PyExc_Exception, s.c_str());
      return NULL;
    }
  catch(const char* s)
    {
      PyErr_SetString(PyExc_Exception, s);
      return NULL;
    }
  catch(...)
    {
      PyErr_SetString(PyExc_Exception, "Unknown exception...");
      return NULL;
    }
}


namespace AMC
{
#ifdef AMC_WITH_LOGGER
  %rename(DriverLogger) AMCLogger;
#endif
#ifdef AMC_WITH_STATISTICS
  %rename(DriverStatistics) AMCStatistics;
#endif

  // Size discretization.
  %rename(DiscretizationSize) ClassDiscretizationSize;

  // Aerosol data.
  %rename(AerosolData) ClassAerosolData;
}

#ifdef AMC_WITH_LOGGER
%include "share/AMCLogger.hxx"
#endif
#ifdef AMC_WITH_TIMER
%include "share/AMCTimer.hxx"
#endif
#ifdef AMC_WITH_STATISTICS
%include "share/AMCStatistics.hxx"
#endif
#ifdef AMC_WITH_MODAL
%include "share/ClassModalDistribution.hxx"
#endif

%include "discretization/ClassDiscretizationSize.hxx"
%include "discretization/ClassDiscretizationCompositionBase.hxx"
%include "discretization/ClassDiscretizationComposition.hxx"
%include "discretization/ClassDiscretizationClass.hxx"
%include "base/ClassAerosolData.hxx"
%include "base/ClassAMC.hxx"
%include "base/ClassAMX.hxx"

namespace AMC
{
#ifdef AMC_WITH_TIMER
  %template(TimerCPU)  AMCTimer<CPU>;
  %template(TimerWall) AMCTimer<Wall>;
#endif

#ifdef AMC_WITH_LOGGER
  %feature("director") AMCLogger;
#endif
#ifdef AMC_WITH_TIMER
  %feature("director") AMCTime<CPU>;
  %feature("director") AMCTime<Wall>;
#endif
#ifdef AMC_WITH_STATISTICS
  %feature("director") AMCStatistics;
#endif

#ifdef AMC_WITH_MODAL
  %template(ModalDistribution) ClassModalDistribution<Driver::real>;
  %feature("director") ClassModalDistribution<real>;
#endif

  // Size discretization.
  %extend ClassDiscretizationSize
  {
    // Annoyed with (re)naming issues.
    static void _GetDiameterBound_(vector<Driver::real> &diameter_bound)
    {
      AMC::ClassDiscretizationSize::GetDiameterBound(diameter_bound);
    }
  }

  %feature("director") ClassDiscretizationSize;

  // Composition discretization.
  %template(DiscretizationCompositionBase)  ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;
  %feature("director") ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;

  %template(DiscretizationClassBase) ClassDiscretizationClass<AMC::disc_comp_base>;
  %feature("director") ClassDiscretizationClass<AMC::disc_comp_base>;

  // Aerosol data.
  %feature("director") ClassAerosolData;

  // AMC.
  %template(AMC_Base) ClassAMC<AMC::disc_comp_base>;
  %feature("director") ClassAMC<AMC::disc_comp_base>;

  // AMX.
  %template(AMX_EulerMixedBaseMoving) ClassAMX<AMC::redist_size_mixed, AMC::redist_comp_base_base, AMC::disc_comp_base, AMC::coag_coef_moving_base>;
  %feature("director") ClassAMX<AMC::redist_size_mixed, AMC::redist_comp_base_base, AMC::disc_comp_base, AMC::coag_coef_moving_base>;
}


namespace Driver
{
#ifdef DRIVER_WITH_NUMERICAL_SOLVER
  %rename(NumericalSolverFluxLimiterRoesSuperbee) ClassNumericalSolverFluxLimiterRoesSuperbee;
  %rename(NumericalSolverDiffusionLinear) ClassNumericalSolverDiffusionLinear;
  %rename(NumericalSolverDiffusionRadial) ClassNumericalSolverDiffusionRadial;
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
  %rename(ParameterizationPhysics) Driver::ClassParameterizationPhysics;
#endif

  %rename(DriverBase) ClassDriverBase;
  %rename(DriverDomainBase) ClassDriverDomainBase;

#ifdef DRIVER_WITH_MAPPING
  %rename(MappingSize)    ClassMappingSize;
#ifdef DRIVER_WITH_TEST
  %rename(MappingSizeTest)    ClassMappingSizeTest;
#endif
  %rename(MappingSpecies)    ClassMappingSpecies;
#endif

#ifdef DRIVER_WITH_MONAHAN
  %rename(ParameterizationEmissionSeaSaltMonahan) ClassParameterizationEmissionSeaSaltMonahan;
#endif

#ifdef DRIVER_WITH_EMISSION
  %rename(Tracer) ClassTracer;
  %rename(Source) ClassSource;
#endif

#ifdef DRIVER_WITH_TRAJECTORY
  %rename(TrajectoryBase) ClassDriverTrajectoryBase;
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
  %rename(TrajectoryChimere) ClassDriverTrajectoryChimere;
#endif
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
  %rename(TrajectoryPolair3d) ClassDriverTrajectoryPolair3d;
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
  %rename(ChimereBase) ClassChimereBase;
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
  %rename(ChamproboisBase)         ClassChamproboisBase;
  %rename(ChamproboisGranulometry) ClassChamproboisGranulometry;
  %rename(ChamproboisSpeciation)   ClassChamproboisSpeciation;
  %rename(ChamproboisSink)         ClassChamproboisSink;
  %rename(ChamproboisFilter)       ClassChamproboisFilter;
  %rename(ChamproboisELPI)         ClassChamproboisELPI;
  %rename(ChamproboisSource)       ClassChamproboisSource;
  %rename(DriverChamprobois)       ClassDriverChamprobois;
#endif
}

%include "DriverHeader.hxx"

#ifdef DRIVER_WITH_NUMERICAL_SOLVER
%include "driver/numerics/ClassNumericalSolverFluxLimiterRoesSuperbee.hxx"
%include "driver/numerics/ClassNumericalSolverAdvectionLaxWendroff.hxx"
%include "driver/numerics/ClassNumericalSolverDiffusionRadial.hxx"
%include "driver/numerics/ClassNumericalSolverDiffusionLinear.hxx"
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
%include "driver/parameterization/ClassParameterizationPhysics.hxx"
#endif

#ifdef DRIVER_WITH_GEO_DATA
%include "geo_data/ClassGeoDataBase.hxx"
%include "geo_data/ClassGeoDataElevation.hxx"
#endif

#ifdef DRIVER_WITH_MONAHAN
%include "parameterization/ClassParameterizationEmissionSeaSaltMonahan.hxx"
#endif

#ifdef DRIVER_WITH_EMISSION
%include "emission/ClassTracer.hxx"
%include "emission/ClassSource.hxx"
%include "emission/ClassDynamicsEmission.hxx"
#endif

%include "base/ClassDriverBase.hxx"
%include "base/ClassDriverDomainBase.hxx"

#ifdef DRIVER_WITH_MAPPING
%include "mapping/ClassMappingSize.hxx"
#ifdef DRIVER_WITH_TEST
%include "mapping/ClassMappingSizeTest.hxx"
#endif
%include "mapping/ClassMappingSpecies.hxx"
%include "mapping/ClassMappingParticle.hxx"
#endif

#ifdef DRIVER_WITH_TRAJECTORY
%include "trajectory/ClassDriverTrajectoryBase.hxx"
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
%include "trajectory/ClassDriverTrajectoryChimere.hxx"
#endif
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
%include "trajectory/ClassDriverTrajectoryPolair3d.hxx"
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
%include "chimere/ClassChimereBase.hxx"
#ifdef DRIVER_WITH_EMISSION
%include "chimere/ClassChimereEmissionAnthropic.hxx"
%include "chimere/ClassChimereEmissionBiogenic.hxx"
#endif
%include "chimere/ClassChimereBoundaryCondition.hxx"
%include "chimere/ClassDriverChimere.hxx"
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
%include "champrobois/ClassChamproboisBase.hxx"
%include "champrobois/ClassChamproboisGranulometry.hxx"
%include "champrobois/ClassChamproboisSpeciation.hxx"
%include "champrobois/ClassChamproboisSink.hxx"
%include "champrobois/ClassChamproboisFilter.hxx"
%include "champrobois/ClassChamproboisELPI.hxx"
%include "champrobois/ClassChamproboisSource.hxx"
%include "champrobois/ClassDriverChamprobois.hxx"
#endif

namespace Driver
{
#ifdef DRIVER_WITH_NUMERICAL_SOLVER
  %template(NumericalSolverAdvectionLaxWendroffFluxLimiterRoesSuperbee) ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>;
  %feature("director") ClassNumericalSolverAdvectionLaxWendroff<ClassNumericalSolverFluxLimiterRoesSuperbee>;
  %feature("director") ClassNumericalSolverFluxLimiterRoesSuperbee;
  %feature("director") ClassNumericalSolverDiffusion;
#endif

#ifdef DRIVER_WITH_PARAMETERIZATION_PHYSICS
  %feature("director") Driver::ClassParameterizationPhysics;
#endif

#ifdef DRIVER_WITH_GEO_DATA
  %template(GeoDataBase) ClassGeoDataBase<float>;
  %template(GeoDataElevation) ClassGeoDataElevation<float>;
  %feature("director") ClassGeoDataBase<float>;
  %feature("director") ClassGeoDataElevation<float>;
#endif

#ifdef DRIVER_WITH_MONAHAN
  %feature("director") ClassParameterizationEmissionSeaSaltMonahan;
#endif

#ifdef DRIVER_WITH_EMISSION
  %template(DynamicsEmissionBase)  ClassDynamicsEmission<AMC::disc_comp_base>;
  %template(DynamicsEmissionTable) ClassDynamicsEmission<AMC::disc_comp_table>;
  %feature("director") ClassTracer;
  %feature("director") ClassSource;
  %feature("director") ClassDynamicsEmission<AMC::disc_comp_base>;
  %feature("director") ClassDynamicsEmission<AMC::disc_comp_table>;
#endif

  %feature("director") ClassDriverBase;
  %feature("director") ClassDriverDomainBase;

#ifdef DRIVER_WITH_MAPPING
  %feature("director") ClassMappingSize;
#ifdef DRIVER_WITH_TEST
  %feature("director") ClassMappingSizeTest;
#endif
  %feature("director") ClassMappingSpecies;
  %template(MappingParticleBase) ClassMappingParticle<AMC::disc_comp_base>;
  %template(MappingParticleTable) ClassMappingParticle<AMC::disc_comp_table>;
  %feature("director") ClassMappingParticle<AMC::disc_comp_base>;
  %feature("director") ClassMappingParticle<AMC::disc_comp_table>;
#endif

#ifdef DRIVER_WITH_TRAJECTORY
  %extend ClassDriverTrajectoryBase
  {
    %template(Run_AMX_EulerMixedBaseMoving) Run<AMC::redist_size_mixed, AMC::redist_comp_base_base, AMC::disc_comp_base, AMC::coag_coef_moving_base>;
  }

  %feature("director") ClassDriverTrajectoryBase;
#ifdef DRIVER_WITH_TRAJECTORY_CHIMERE
  %feature("director") ClassDriverTrajectoryChimere;
#endif
#ifdef DRIVER_WITH_TRAJECTORY_POLAIR3D
  %feature("director") ClassDriverTrajectoryPolair3d;
#endif
#endif

#ifdef DRIVER_WITH_CHIMERE
  %feature("director") ClassChimereBase;
#ifdef CHIMERE_WITH_AMC
  %template(CHIMERE_AMX_EulerMixedBaseMoving) ClassDriverChimere<redist_size, redist_comp, disc_comp, coag_coef_repart>;
  %feature("director") ClassDriverChimere<redist_size, redist_comp, disc_comp, coag_coef_repart>;
#else
  %template(CHIMERE) ClassDriverChimere<int, int, int, int>;
  %feature("director") ClassDriverChimere<int, int, int, int>;
#endif
#ifdef DRIVER_WITH_EMISSION
  %template(ChimereEmissionAnthropicBase) ClassChimereEmissionAnthropic<AMC::disc_comp_base>;
  %template(ChimereEmissionAnthropicTable) ClassChimereEmissionAnthropic<AMC::disc_comp_table>;
  %template(ChimereEmissionBiogenicBase) ClassChimereEmissionBiogenic<AMC::disc_comp_base>;
  %template(ChimereEmissionBiogenicTable) ClassChimereEmissionBiogenic<AMC::disc_comp_table>;
  %feature("director") ClassChimereEmissionAnthropic<AMC::disc_comp_base>;
  %feature("director") ClassChimereEmissionAnthropic<AMC::disc_comp_table>;
  %feature("director") ClassChimereEmissionBiogenic<AMC::disc_comp_base>;
  %feature("director") ClassChimereEmissionBiogenic<AMC::disc_comp_table>;
#endif
  %template(ChimereBoundaryConditionBase) ClassChimereBoundaryCondition<AMC::disc_comp_base>;
  %template(ChimereBoundaryConditionTable) ClassChimereBoundaryCondition<AMC::disc_comp_table>;
  %feature("director") ClassChimereBoundaryCondition<AMC::disc_comp_base>;
  %feature("director") ClassChimereBoundaryCondition<AMC::disc_comp_table>;
#endif

#ifdef DRIVER_WITH_CHAMPROBOIS
  %extend ClassChamproboisSpeciation
  {
    %pythoncode %{
        %}
  }

  %extend ClassDriverChamprobois
  {
    %template(Run_AMX_EulerMixedBaseMoving) Run<AMC::redist_size_mixed, AMC::redist_comp_base_base, AMC::disc_comp_base, AMC::coag_coef_moving_base>;
    %template(Forward_AMX_EulerMixedBaseMoving) Forward<AMC::redist_size_mixed, AMC::redist_comp_base_base, AMC::disc_comp_base, AMC::coag_coef_moving_base>;
  }
  %feature("director") ClassChamproboisBase;
  %feature("director") ClassChamproboisGranulometry;
  %feature("director") ClassChamproboisSpeciation;
  %feature("director") ClassChamproboisSink;
  %feature("director") ClassChamproboisFilter;
  %feature("director") ClassChamproboisELPI;
  %feature("director") ClassChamproboisSource;
  %feature("director") ClassDriverChamprobois;
#endif
}
