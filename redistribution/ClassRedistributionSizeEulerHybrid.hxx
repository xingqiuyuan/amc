// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_HYBRID_HXX

#define AMC_REDISTRIBUTION_SIZE_EULER_HYBRID_RATIO_FRACTION_THRESHOLD_DEFAULT 0.5

namespace AMC
{
  /*! 
   * \class ClassRedistributionSizeEulerHybrid
   */
  class ClassRedistributionSizeEulerHybrid : public ClassRedistributionSizeBase
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Fraction ratio (mass / (mass + number) threshold.*/
    real ratio_fraction_threshold_;

    /*!< Compute redistribution coefficients.*/
    void compute_coefficient(const real &mass_moving,
                             const real &mass_moving_log,
                             const real &ratio_fraction,
                             int &section_redistribute,
                             vector1r &coefficient_number,
                             vector1r &coefficient_mass) const;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
    ClassRedistributionSizeEulerHybrid(Ops::Ops &ops, int Ng = ClassDiscretizationSize::GetNsection());

    /*!< Destructor.*/
    ~ClassRedistributionSizeEulerHybrid();
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_HYBRID_HXX
#endif
