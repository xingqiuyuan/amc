// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_TABLE_CXX

#include "ClassRedistributionSizeTable.hxx"

namespace AMC
{
  // Type.
  template<class M>
  int ClassRedistributionSizeTable<M>::Type()
  {
    return AMC_REDISTRIBUTION_SIZE_TYPE_TABLE + M::Type();
  }


  // Set delta and delta_inv.
  template<class M>
  void ClassRedistributionSizeTable<M>::set_delta()
  {
    delta_inv_.resize(2);
    delta_inv_[0] = real(Ndisc_[0]) / (ClassDiscretizationSize::mass_bound_log_.back() -
                                       ClassDiscretizationSize::mass_bound_log_.front());
    delta_inv_[1] = real(Ndisc_[1]);

    delta_.resize(2);
    delta_[0] = real(1) / delta_inv_[0];
    delta_[1] = real(1) / delta_inv_[1];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "delta = " << delta_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "delta_inv = " << delta_inv_ << endl;
#endif
  }


  // Constructors.
  template<class M>
  ClassRedistributionSizeTable<M>::ClassRedistributionSizeTable(Ops::Ops &ops, int Ng)
    : redistribution_method_type(ops, Ng), Ntable_(0), is_table_computed_(false), path_("")
  {
    // Change type.
    this->type_ = "Table" + this->type_;

    // Lookup table for redistribution.
    Ndisc_.assign(2, 0);
    Ndisc_[0] = ops.Get<int>("table.Ndisc.size", "", AMC_REDISTRIBUTION_SIZE_TABLE_NUMBER_DISCRETIZATION_SIZE_DEFAULT);
    Ndisc_[1] = ops.Get<int>("table.Ndisc.fraction", "", AMC_REDISTRIBUTION_SIZE_TABLE_NUMBER_DISCRETIZATION_FRACTION_DEFAULT);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Debug() << Reset() << "Ndisc = " << Ndisc_
                         << ", may be overwritten if path file provided." << endl;
#endif

    path_ = ops.Get<string>("table.path", "", path_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Debug(1) << Reset() << "Provided path file = \"" << path_ << "\"" << endl;
#endif

    // If real path, read directly in constructor.
    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset() << "file_size = " << file_size << endl;
#endif

    if (file_size > 0)
      Read();

    return;
  }


  // Destructor.
  template<class M>
  ClassRedistributionSizeTable<M>::~ClassRedistributionSizeTable()
  {
    return;
  }


  // Get methods.
  template<class M>
  unsigned long ClassRedistributionSizeTable<M>::GetNtable() const
  {
    return Ntable_;
  }

  template<class M>
  void ClassRedistributionSizeTable<M>::_GetNdisc_(vector<int> &Ndisc) const
  {
    Ndisc = Ndisc_;
  }

  template<class M>
  void ClassRedistributionSizeTable<M>::_GetTable_(vector<vector<int> > &table_section_index,
                                                   vector<vector<real> > &table_coefficient_number,
                                                   vector<vector<real> > &table_coefficient_mass) const
  {
    table_section_index = table_section_index_;
    table_coefficient_number = table_coefficient_number_;
    table_coefficient_mass = table_coefficient_mass_;
  }


  // Set methods.
  template<class M>
  void ClassRedistributionSizeTable<M>::_SetNdisc_(const vector<int> &Ndisc)
  {
    Ndisc_ = Ndisc;

    // Table discretization step.
    set_delta();
  }


  // Table management.
  template<class M>
  void ClassRedistributionSizeTable<M>::Compute()
  {
#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    int time_index = AMCTimer<CPU>::Add("compute_table_redistribution_size_" + M::type_);
    AMCTimer<CPU>::Start();
#endif

    // Table length.
    Ntable_ = Ndisc_[0] * Ndisc_[1];

    // Table discretization step.
    set_delta();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "Compute table of size " << Ntable_ << endl;
#endif

    // Max number of section in which redistribute.
    int Nsection_max(0);
    for (int i = 0; i < ClassRedistributionSizeBase::Nsection_; i++)
      if (Nsection_max < this->Nredistribute_[i])
        Nsection_max = this->Nredistribute_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "Nsection_max = " << Nsection_max << endl;
#endif

    table_section_number_.assign(Ntable_, 0);
    table_section_index_.assign(Ntable_, vector1i(Nsection_max, 0));
    table_coefficient_number_.assign(Ntable_, vector1r(Nsection_max, real(0)));
    table_coefficient_mass_.assign(Ntable_, vector1r(Nsection_max, real(0)));

    int i(0);
    for (int j = 0; j < Ndisc_[0]; j++)
      for (int k = 0; k < Ndisc_[1]; k++)
        {
          int &Nsection = table_section_number_[i];
          vector1i &section_index = table_section_index_[i];
          vector1r &coefficient_number = table_coefficient_number_[i];
          vector1r &coefficient_mass = table_coefficient_mass_[i];

          // Moving mass logarithm.
          real mass_moving_log = ClassDiscretizationSize::mass_bound_log_[0] + delta_[0] * (real(j) + 0.5);
          real mass_moving = exp(mass_moving_log);

          // Fraction of mass concentration to redistribute.
          real mass_fraction = (real(k) + 0.5) * delta_[1];

          // Compute redistribution coefficients.
          int section_index_local;
          redistribution_method_type::compute_coefficient(mass_moving, mass_moving_log,
                                                          mass_fraction, section_index_local,
                                                          coefficient_number, coefficient_mass);
          // Reset counter.
          Nsection = 0;

          // Now redistribute number and mass.
          const int l = ClassRedistributionSizeBase::redistribute_index_[section_index_local];
          for (int m = 0; m < ClassRedistributionSizeBase::Nredistribute_[section_index_local]; m++)
            if (coefficient_number[m] > real(0))
              {
                section_index[Nsection] = l + m;
                coefficient_number[Nsection] = coefficient_number[m];
                coefficient_mass[Nsection] = coefficient_mass[m];
                Nsection++;
              }

          // Resize table, eventually.
          if (Nsection < Nsection_max)
            { 
              section_index.resize(Nsection);
              coefficient_number.resize(Nsection);
              coefficient_mass.resize(Nsection);

#if __cplusplus > 199711L
              section_index.shrink_to_fit();
              coefficient_number.shrink_to_fit();
              coefficient_mass.shrink_to_fit();
#endif
            }

          // Increment table counter.
          i++;
        }

#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Computed table for size discretization \""
                         << M::type_ << "\" in " << AMCTimer<CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif

    is_table_computed_ = true;
  }


  template<class M>
  void ClassRedistributionSizeTable<M>::Clear()
  {
    Ndisc_.clear();
    delta_.clear();
    delta_inv_.clear();
    Ntable_ = 0;
    table_section_number_.clear();
    table_section_index_.clear();
    table_coefficient_number_.clear();
    table_coefficient_mass_.clear();
    is_table_computed_ = false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Cleared table of type \"" << this->type_ << "\"." << endl;
#endif
  }


  template<class M>
  void ClassRedistributionSizeTable<M>::Read()
  {
    check_file(path_);

    ifstream fin(path_.c_str(), ifstream::binary);

    Ndisc_.resize(2);
    fin.read(reinterpret_cast<char*>(Ndisc_.data()), 2 * sizeof(int));
    Ntable_ = Ndisc_[0] * Ndisc_[1];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Ndisc = " << Ndisc_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "Ntable = " << Ntable_ << endl;
#endif

    table_section_number_.resize(Ntable_);
    table_section_index_.resize(Ntable_);
    table_coefficient_number_.resize(Ntable_);
    table_coefficient_mass_.resize(Ntable_);

    fin.read(reinterpret_cast<char*>(table_section_number_.data()), Ntable_ * sizeof(int));

    for (int i = 0; i < Ntable_; i++)
      {
        const int &Nsection = table_section_number_[i];

        table_section_index_[i].resize(Nsection);
        table_coefficient_number_[i].resize(Nsection);
        table_coefficient_mass_[i].resize(Nsection);

        fin.read(reinterpret_cast<char*>(table_section_index_[i].data()), Nsection * sizeof(real));
        fin.read(reinterpret_cast<char*>(table_coefficient_number_[i].data()), Nsection * sizeof(real));
        fin.read(reinterpret_cast<char*>(table_coefficient_mass_[i].data()), Nsection * sizeof(real));
      }

    fin.close();

    // Table discretization step.
    set_delta();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Read file \"" << path_ << "\"." << endl;
#endif

    is_table_computed_ = true;
  }


  template<class M>
  void ClassRedistributionSizeTable<M>::Write() const
  {
    if (! is_table_computed_)
      throw AMC::Error("Table not yet computed.");

    ofstream fout(path_.c_str(), ofstream::binary);
    fout.write(reinterpret_cast<const char*>(Ndisc_.data()), 2 * sizeof(int));
    fout.write(reinterpret_cast<const char*>(table_section_number_.data()), Ntable_ * sizeof(int));

    for (int i = 0; i < Ntable_; i++)
      {
        const int &Nsection = table_section_number_[i];
        fout.write(reinterpret_cast<const char*>(table_section_index_[i].data()), Nsection * sizeof(int));
        fout.write(reinterpret_cast<const char*>(table_coefficient_number_[i].data()), Nsection * sizeof(real));
        fout.write(reinterpret_cast<const char*>(table_coefficient_mass_[i].data()), Nsection * sizeof(real));
      }
    fout.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "Write table of type \"" << this->type_
                         << "\" and size " << Ntable_ << " in file \"" << path_ << "\"." << endl;
#endif
  }


  // Redistribute one section over existing size sections, does not modify composition.
  template<class M>
  inline void ClassRedistributionSizeTable<M>::Redistribute(const real &number_total,
                                                            const real &mass_total,
                                                            const real &concentration_aer_number,
                                                            const real &concentration_aer_mass_total,
                                                            int &Nsection,
                                                            vector1i &section_index,
                                                            vector1r &section_number,
                                                            vector1r &section_mass) const
  {
    // Number and mass fractions out of total number and mass for this section.
    real number_fraction = concentration_aer_number / number_total;
    real mass_fraction = concentration_aer_mass_total / mass_total;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "number_fraction = " << number_fraction << endl;
    CBUG << "mass_fraction = " << mass_fraction << endl;
#endif

    // Ratio between fractions : the greater means this section
    // contains more mass than number and conversely.
    real ratio_fraction = mass_fraction / (number_fraction + mass_fraction);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "ratio_fraction = " << ratio_fraction << endl;
#endif

    // Compute current moving mass.
    real mass_moving = concentration_aer_mass_total / concentration_aer_number;
    real mass_moving_log = log(mass_moving);

    // Compute table index.
    int table_index = int((mass_moving_log - ClassDiscretizationSize::mass_bound_log_[0])
                          * delta_inv_[0] + AMC_REDISTRIBUTION_SIZE_TABLE_ROUNDOFF_EPSILON) * Ndisc_[1]
      + int(mass_fraction * delta_inv_[1] + AMC_REDISTRIBUTION_SIZE_TABLE_ROUNDOFF_EPSILON);

    // Read table.
    Nsection = table_section_number_[table_index];
    const vector1i &section_index_local = table_section_index_[table_index];
    const vector1r &coefficient_number = table_coefficient_number_[table_index];
    const vector1r &coefficient_mass = table_coefficient_mass_[table_index];

    for (int i = 0; i < Nsection; ++i)
      section_index[i] = section_index_local[i];

    // Now redistribute number and mass.
    for (int i = 0; i < Nsection; ++i)
      {
        section_number[i] = coefficient_number[i] * concentration_aer_number;
        section_mass[i] = coefficient_mass[i] * concentration_aer_mass_total;
      }
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_TABLE_CXX
#endif
