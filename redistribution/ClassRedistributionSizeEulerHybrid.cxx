// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_HYBRID_CXX

#include "ClassRedistributionSizeEulerHybrid.hxx"

namespace AMC
{
  // Type.
  int ClassRedistributionSizeEulerHybrid::Type()
  {
    return AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID;
  }


  // Compute redistribution coefficients.
  void ClassRedistributionSizeEulerHybrid::compute_coefficient(const real &mass_moving,
                                                               const real &mass_moving_log,
                                                               const real &ratio_fraction,
                                                               int &section_redistribute,
                                                               vector1r &coefficient_number,
                                                               vector1r &coefficient_mass) const
  {
    // Size redistribute section in which is located the moving mass.
    section_redistribute = search_index(ClassRedistributionSizeBase::mass_bound_log_, mass_moving_log);

    // Proxy.
    const int n = ClassRedistributionSizeBase::Nredistribute_[section_redistribute];

    // Last element.
    coefficient_number[n - 1] = real(1);

    // The first real size section where to redistribute.
    const int i = ClassRedistributionSizeBase::redistribute_index_[section_redistribute];

    if (n > 1)
      {
        coefficient_number[0] = (ClassDiscretizationSize::mass_mean_log_[i + 1] - mass_moving_log)
          / (ClassDiscretizationSize::mass_mean_log_[i + 1] - ClassDiscretizationSize::mass_mean_log_[i]);
        coefficient_number[1] = real(1) - coefficient_number[0];
      }

    for (int j = 0; j < n; ++j)
      {
        real ratio_mass(real(1)), ratio_number(real(1));

        if (ratio_fraction > ratio_fraction_threshold_)
          ratio_number = mass_moving / ClassDiscretizationSize::mass_mean_[i + j];
        else
          ratio_mass = ClassDiscretizationSize::mass_mean_[i + j] / mass_moving;

        coefficient_mass[j] = coefficient_number[j] * ratio_mass;
        coefficient_number[j] *= ratio_number;
      }
  }
  

  // Constructors.
  ClassRedistributionSizeEulerHybrid::ClassRedistributionSizeEulerHybrid(Ops::Ops &ops, int Ng)
    : ClassRedistributionSizeBase(ops, Ng, "EulerHybrid"),
      ratio_fraction_threshold_(real(AMC_REDISTRIBUTION_SIZE_EULER_HYBRID_RATIO_FRACTION_THRESHOLD_DEFAULT))
  {
    // Get specific parameters for this redistribution scheme.
    ratio_fraction_threshold_ = ops.Get<real>("ratio_fraction_threshold", "", ratio_fraction_threshold_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset()
                         << "EulerHybrid ratio fraction threshold = " << ratio_fraction_threshold_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassRedistributionSizeEulerHybrid::~ClassRedistributionSizeEulerHybrid()
  {
    return;
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_HYBRID_CXX
#endif
