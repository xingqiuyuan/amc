// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_MOVING_DIAMETER_CXX

#include "ClassRedistributionSizeMovingDiameter.hxx"

namespace AMC
{
  // Type.
  int ClassRedistributionSizeMovingDiameter::Type()
  {
    return AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER;
  }


  // Compute redistribution coefficients.
  void ClassRedistributionSizeMovingDiameter::compute_coefficient(const real &mass_moving,
                                                                  const real &mass_moving_log,
                                                                  const real &ratio_fraction,
                                                                  int &section_redistribute,
                                                                  vector1r &coefficient_number,
                                                                  vector1r &coefficient_mass) const
  {
    // Proxy, n = 1 for Moving diameter.
    //const int &n = ClassRedistributionSizeBase::Nredistribute_[section_redistribute];
    //const int n = 1;

    // Size redistribute section in which is located the moving mass :
    // for moving diameter this is also the real size section in which redistribute.
    section_redistribute = search_index(ClassRedistributionSizeBase::mass_bound_log_, mass_moving_log);

    // Last element. n - 1 = 0
    //coefficient_number[n - 1] = real(1);
    coefficient_number[0] = real(1);

    // The first real section where to redistribute : for MovingDiameter this is the redistribute section.
    //const int i = ClassRedistributionSizeBase::redistribute_index_[section_redistribute];
    const int i = section_redistribute;

    // All mass is redistributed in given section.
    coefficient_mass[0] = real(1);
  }


  // Constructors.
  ClassRedistributionSizeMovingDiameter::ClassRedistributionSizeMovingDiameter(Ops::Ops &ops, int Ng)
    : ClassRedistributionSizeBase(ops, Ng, "MovingDiameter")
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "MovingDiameter size redistribution scheme :"
                         << " override some variables of base class." << endl;
#endif

    // Override the redistribution size grid.
    Nsection_ = ClassDiscretizationSize::Nsection_;
    Nbound_ = ClassDiscretizationSize::Nbound_;
    Nredistribute_.resize(Nsection_);
    redistribute_index_.resize(Nsection_);

    for (int i = 0; i < Nsection_; i++)
      {
        Nredistribute_[i] = 1;
        redistribute_index_[i] = i;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "MovingDiameter size redistribution scheme :" << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "\tNredistribute = " << Nredistribute_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "\tredistribute_index = " << redistribute_index_ << endl;
#endif

    diameter_bound_ = ClassDiscretizationSize::diameter_bound_;
    mass_bound_log_ = ClassDiscretizationSize::mass_bound_log_;
    mass_bound_ = ClassDiscretizationSize::mass_bound_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Warning() << Reset() << "With MovingDiameter size redistribution scheme,"
                         << " diameters are allowed to move within size sections, hence are no more centered." << endl;
#endif

    return;
  }


  // Destructor.
  ClassRedistributionSizeMovingDiameter::~ClassRedistributionSizeMovingDiameter()
  {
    return;
  }


  // Normalize concentration to get number and mass conservation.
  void ClassRedistributionSizeMovingDiameter::Normalize(const real &number_total_initial,
                                                        const real &mass_total_initial,
                                                        real *concentration_aer_number,
                                                        real *concentration_aer_mass,
                                                        const int Nspecies) const
  {
    // Do nothing function because MovingDiameter
    // algorithm is number and mass conservative.
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_MOVING_DIAMETER_CXX
#endif
