// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_MOVING_DIAMETER_HXX

namespace AMC
{
  /*! 
   * \class ClassRedistributionSizeMovingDiameter
   */
  class ClassRedistributionSizeMovingDiameter : public ClassRedistributionSizeBase
  {
  public:
    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;
   
  protected:

    /*!< Compute redistribution coefficients..*/
    void compute_coefficient(const real &mass_moving,
                             const real &mass_moving_log,
                             const real &ratio_fraction,
                             int &section_redistribute,
                             vector1r &coefficient_number,
                             vector1r &coefficient_mass) const;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
    ClassRedistributionSizeMovingDiameter(Ops::Ops &ops, int Ng = ClassDiscretizationSize::GetNsection());

    /*!< Destructor.*/
    ~ClassRedistributionSizeMovingDiameter();

    /*!< Normalize concentration to get number and mass conservation.*/
    void Normalize(const real &number_total_initial,
                   const real &mass_total_initial,
                   real *concentration_aer_number,
                   real *concentration_aer_mass,
                   const int Nspecies = 1) const;
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_MOVING_DIAMETER_HXX
#endif
