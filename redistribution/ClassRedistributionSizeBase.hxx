// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_BASE_HXX

#define AMC_REDISTRIBUTION_SIZE_TYPE_EULER_HYBRID     1
#define AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED      2
#define AMC_REDISTRIBUTION_SIZE_TYPE_MOVING_DIAMETER  3
#define AMC_REDISTRIBUTION_SIZE_TYPE_TABLE           10

namespace AMC
{
  /*! 
   * \class ClassRedistributionSizeBase
   */
  class ClassRedistributionSizeBase : protected ClassDiscretizationSize
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of general sections : proxy from ClassAMC.*/
    int Ng_;

    /*!< Number of sections over which redistribute.*/
    int Nsection_;

    /*!< Number of bounds of redistribute sections.*/
    int Nbound_;

    /*!< Number of sections over which redistribute.*/
    vector1i Nredistribute_;

    /*!< Index of first section over which redistribute.*/
    vector1i redistribute_index_;

    /*!< Redistribute section diameter bound (µm).*/
    vector1r diameter_bound_;

    /*!< Redistribute section mass bound (µg).*/
    vector1r mass_bound_;

    /*!< Redistribute section mass bound logarithm (adim).*/
    vector1r mass_bound_log_;

    /*!< Type of size redistribution.*/
    string type_;

    /*!< Compute redistribution coefficients..*/
    virtual void compute_coefficient(const real &mass_moving,
                                     const real &mass_moving_log,
                                     const real &ratio_fraction,
                                     int &section_redistribute,
                                     vector1r &coefficient_number,
                                     vector1r &coefficient_mass) const = 0;

  public:

    /*!< Constructors.*/
    ClassRedistributionSizeBase(Ops::Ops &ops, const int &Ng, const string type = "");

    /*!< Destructor.*/
    virtual ~ClassRedistributionSizeBase();

    /*!< Get methods.*/
    void GetDiameterBound(vector<real> &diameter_bound) const;
    int GetNsection() const;
    int GetNbound() const;
    int GetNredistribute(const int &i) const;
    string GetType() const;


    /*!< Redistribute over size sections, does not modify composition.*/
#ifndef SWIG
    void Redistribute(const real &number_total,
                      const real &mass_total,
                      const real &concentration_aer_number,
                      const real &concentration_aer_mass_total,
                      int &Nsection,
                      vector1i &section_index,
                      vector1r &section_number,
                      vector1r &section_mass) const;
#endif

    /*!< Normalize concentration to get number and mass conservation.*/
    virtual void Normalize(const real &number_total_initial,
                           const real &mass_total_initial,
                           real *concentration_aer_number,
                           real *concentration_aer_mass,
                           const int Nspecies = 1) const;

#ifdef AMC_WITH_TEST
    /*!< Test redistribution.*/
    void _Test_(const int &section_min,
                const int &section_max,
                vector<real> &concentration_aer_number,
                vector<real> &concentration_aer_mass_total,
                vector<real> &diameter_mean,
                const bool normalize = true) const;
#endif
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_BASE_HXX
#endif
