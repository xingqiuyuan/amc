// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_CXX

#include "ClassRedistributionComposition.hxx"

namespace AMC
{
  // Constructors.
  template<class R, class D>
  ClassRedistributionComposition<R, D>::ClassRedistributionComposition(ClassRedistributionComposition<R, D>* root,
                                                                       ClassDiscretizationComposition<D>*
                                                                       discretization_composition)
    : redistribution_composition_type(reinterpret_cast<D*>(discretization_composition)),
      discretization_composition_nested_(discretization_composition), Nspecies_total_(0),
      root_(root), Nclass_total_(0), index_(-1), index_internal_mixing_(-1)
  {
    Nspecies_total_ = ClassSpecies::GetNspecies();

    // Increment total number of classes and set index.
    ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    while (redistribution_composition_ptr->root_ != NULL)
      redistribution_composition_ptr = redistribution_composition_ptr->root_;

    index_ = redistribution_composition_ptr->Nclass_total_++;

    index_internal_mixing_ = discretization_composition_nested_->GetIndex();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Debug(2) << Reset() << "name = " << this->name_ << endl;
    *AMCLogger::GetLog() << Bmagenta() << Debug(2) << Reset() << "index = " << index_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "index_internal_mixing = " << index_internal_mixing_ << endl;
#endif


    // If it is an internal mixing. It does not need the root index, except it the top
    // level class is an internal mixing, for which it has to be set to 0.
    if (root_ == NULL)
      this->index_root_ = 0;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bcyan() << Debug(2) << Reset() << "index_root = " << this->index_root_ << endl;
#endif

    // In the same way we set the class index in which to redistribute to itself.
    if (root_ == NULL)
      this->index_class_.assign(Nclass_total_, 0);

    return;
  }


  template<class R, class D>
  ClassRedistributionComposition<R, D>::ClassRedistributionComposition(ClassRedistributionComposition<R, D>* root,
                                                                       ClassDiscretizationComposition<D>*
                                                                       discretization_composition,
                                                                       Ops::Ops &ops)
    : redistribution_composition_type(reinterpret_cast<D*>(discretization_composition), ops),
      discretization_composition_nested_(discretization_composition), Nspecies_total_(0),
      root_(root), Nclass_total_(0), index_(-1), index_internal_mixing_(-1)
  {
    Nspecies_total_ = ClassSpecies::GetNspecies();

    // Increment total number of classes and set index.
    ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    while (redistribution_composition_ptr->root_ != NULL)
      redistribution_composition_ptr = redistribution_composition_ptr->root_;

    index_ = redistribution_composition_ptr->Nclass_total_++;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "name = " << this->name_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "index = " << index_ << endl;
#endif

    // Set nested classes.
    nested_.resize(this->Nclass_);

    for (int i = 0; i < this->Nclass_; i++)
      {
        ClassDiscretizationComposition<D>* discretization_composition_ptr =
          discretization_composition->GetDiscretizationCompositionNested(i);

        if (discretization_composition_ptr->IsInternalMixing())
          nested_[i] = new ClassRedistributionComposition<R, D>(this, discretization_composition_ptr);
        else
          nested_[i] = new ClassRedistributionComposition<R, D>(this, discretization_composition_ptr, ops);
      }

    // Set concentration indexes of parent class : we can only do it there.
    this->index_class_.resize(this->Nclass_);

    for (int i = 0; i < this->Nclass_; i++)
      this->index_class_[i] = nested_[i]->index_;

    this->index_root_ = this->index_class_[0] - 1;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "Nclass_total = " << Nclass_total_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "(" << this->index_root_ << ") <- "
                         << this->name_ << " -> " << this->index_class_ << endl;
#endif

    return;
  }


  // Destructor.
  template<class R, class D>
  ClassRedistributionComposition<R, D>::~ClassRedistributionComposition()
  {
    return;
  }


  // Get method.
  template<class R, class D>
  int ClassRedistributionComposition<R, D>::GetNclassTotal() const
  {
    return Nclass_total_;
  }


  template<class R, class D>
  ClassDiscretizationComposition<D>* ClassRedistributionComposition<R, D>::GetDiscretizationCompositionNestedPtr() const
  {
    return discretization_composition_nested_;
  }


  // Redistribute number/mass over composition classes, does not modify size sections.
  template<class R, class D>
  void ClassRedistributionComposition<R, D>::Redistribute(int &Nclass, vector1i &index_internal_mixing,
                                                          vector1i& index_class, vector2r &concentration) const
  {
    // The top level concentration to redistribute is placed at the beginning of concentration argument vector.

    // Recursively redistribute mass and composition on classes from this one.
    const ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "Nclass_total = " << Nclass_total_ << endl;
#endif

    // If top level class is itself one internal mixing,
    // we do not need to go further, it would segfault anyway.
    if (redistribution_composition_ptr->internal_mixing_)
      {
        index_class[Nclass] = redistribution_composition_ptr->index_;
        index_internal_mixing[Nclass] = redistribution_composition_ptr->index_internal_mixing_;
        Nclass++;
        redistribution_composition_ptr = NULL;
        return;
      }

    vector1i next(Nclass_total_, 0);
    vector1b is_redistributed(Nclass_total_, false);
    vector2b class_work(Nclass_total_);

    // Loop until reached top level.
    while (redistribution_composition_ptr != NULL)
      {
        const int index = redistribution_composition_ptr->index_;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "name = " << redistribution_composition_ptr->name_ << endl;
        CBUG << "index = " << index << endl;
        CBUG << "is_redistributed = " << is_redistributed[index] << endl;
#endif

        // Check if redistribution already done.
        if (! is_redistributed[index])
          {
            const int &index_root = redistribution_composition_ptr->index_root_;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
            CBUG << "index_root = " << index_root << endl;
#endif

            // Pointer to associated discretization composition.
            ClassDiscretizationComposition<D>* discretization_composition_ptr =
              redistribution_composition_ptr->discretization_composition_nested_;

            // Fraction associated with root concentration to be redistributed.
            vector1r fraction;
            discretization_composition_ptr->ComputeFraction(concentration[index_root], fraction);

            // Perform redistribution.
            redistribution_composition_ptr->R::Redistribute(fraction, class_work[index], concentration);

            is_redistributed[index] = true;
          }

        int &next_class = next[index];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "next_class/Nclass = " << next_class << "/" << redistribution_composition_ptr->Nclass_ << endl;
#endif

        // Going to upper level.
        if (next_class == redistribution_composition_ptr->Nclass_)
          redistribution_composition_ptr = redistribution_composition_ptr->root_;
        else
          while (next_class < redistribution_composition_ptr->Nclass_)
            if (class_work[index][next_class])
              {
                redistribution_composition_ptr = redistribution_composition_ptr->nested_[next_class];

                next_class++;

                // If class is an internal mixing, place the redistributed
                // concentration into general vector and come back to upper level.
                if (redistribution_composition_ptr->internal_mixing_)
                  {
#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
                    CBUG << "Redistribute in internal mixing " << redistribution_composition_ptr->name_
                         << " (" << redistribution_composition_ptr->index_ << ")" << endl;
#endif

                    index_class[Nclass] = redistribution_composition_ptr->index_;
                    index_internal_mixing[Nclass] = redistribution_composition_ptr->index_internal_mixing_;
                    Nclass++;
                    redistribution_composition_ptr = redistribution_composition_ptr->root_;
                  }
                else
                  break;
              }
            else
              next_class++;
      }
  }


#ifdef AMC_WITH_TEST
  // Test redistribution.
  template<class R, class D>
  void ClassRedistributionComposition<R, D>::Test(ClassRedistributionComposition<R, D> &redistribution_composition,
                                                  int N, real tol, bool display)
  {
    const ClassDiscretizationComposition<D>* discretization_composition =
      redistribution_composition.discretization_composition_nested_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test each class of "
                         << (R::Type() == AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE ? "tabulated" : "base")
                         << " redistribution composition \"" << discretization_composition->GetName()
                         << "\" (" << discretization_composition->GetNclassTotal()
                         << " of them) " << Fred().Str() << "with N = " << N << Reset().Str()
                         << " and " << Fmagenta().Str() << "tol = " << scientific << tol << Reset().Str() << endl;
#endif

    // Total number of class from discretization.
    int Nclass_total = discretization_composition->GetNclassTotal();

    // Working arrays.
    int Nclass(0);
    vector1i index_class(Nclass_total, 0);
    vector1i index_internal_mixing(Nclass_total, 0);
    vector2r concentration_mass_class(redistribution_composition.GetNclassTotal(),
                                      vector1r(redistribution_composition.Nspecies_total_ + 1, real(0)));

#ifdef AMC_WITH_TIMER
    int time_index_tot = AMCTimer<CPU>::Add("test_redistribution_composition_class_tot_" + discretization_composition->GetName());
#endif

    for (int i = 0; i < Nclass_total; i++)
      {
        string class_name = discretization_composition->GetClassInternalMixing(i)->GetName();

#ifdef AMC_WITH_TIMER
        int time_index = AMCTimer<CPU>::Add("test_redistribution_composition_class_" + class_name);
#endif

        for (int j = 0; j < N; j++)
          {
            vector1r concentration_aer_mass;
            discretization_composition->GenerateRandomComposition(i, concentration_aer_mass, false);

#ifdef AMC_WITH_TIMER
            AMCTimer<CPU>::Start();
#endif

            redistribution_composition._TestSelf_(concentration_aer_mass, Nclass,
                                                  index_internal_mixing,
                                                  index_class, concentration_mass_class);

#ifdef AMC_WITH_TIMER
            AMCTimer<CPU>::Stop(time_index);
            AMCTimer<CPU>::Stop(time_index_tot);
#endif

            // Display before performing checks to get a chance to see something.
            if (display)
              {
                cerr << "Test number " << j << endl
                     << "\tInitial aerosol concentration = " << concentration_aer_mass << endl
                     << "\tInitial class = " << class_name << " (" << i << ")" << endl
                     << "\tRedistributed on " << Nclass << " classes : " << endl;

                for (int k = 0; k < Nclass; k++)
                  {
                    const int &l = index_class[k];
                    const int &m = index_internal_mixing[k];

                    cerr << "\t\t" << k << " class " << discretization_composition->GetClassInternalMixing(m)->GetName()
                         << " (" << l << ")" << endl << "\t\t" << k << " total mass = "
                         << concentration_mass_class[l].back() << endl << "\t\t" << k << " with composition = "
                         << concentration_mass_class[l] << endl;
                  }
              }

            if (Nclass == 0)
              {
                ostringstream sout;
                sout << "Aerosol mass concentration = " << concentration_aer_mass << " could not be redistributed.";
                throw AMC::Error(sout.str());
              }

            // Perform checks.
            real mass_redist(real(0));
            for (int k = 0; k < Nclass; k++)
              {
                const int &l = index_class[k];
                const int &m = index_internal_mixing[k];

                if (abs(compute_vector_sum(concentration_mass_class[l], -1) - real(1)) > tol)
                  {
                    ostringstream sout;
                    sout << "Aerosol composition = " << concentration_mass_class[l]
                         << " in class " << m << " at position " << l << " is not equal to unity.";
                    throw AMC::Error(sout.str());
                  }

                bool on_edge;
                int o = discretization_composition->FindClassCompositionIndex(concentration_mass_class[l], on_edge);

                if (m != o)
                  {
                    ostringstream sout;
                    sout << "Aerosol redistributed mass composition = " << concentration_mass_class[l]
                         << " in class " << m << " is found to be in " << (on_edge ? "edge" : "bulk")
                         << " of class " << o << ".";
                    throw AMC::Error(sout.str());
                  }

                mass_redist += concentration_mass_class[l].back();
              }

            if (abs(mass_redist - real(1)) > tol)
              {
                ostringstream sout;
                sout << "Total redistributed aerosol mass " << mass_redist << " is not equal to initial aerosol mass (1)";
                throw AMC::Error(sout.str());
              }
          }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << setw(40) << left << class_name << " : CPU time = "
                             << AMCTimer<CPU>::GetTimer(time_index) << " seconds (" << scientific
                             << AMCTimer<CPU>::GetTimer(time_index) / real(N > 0 ? N : 1) << " seconds/test)." << endl;
#endif
#endif
      }

    int Ntot = N * Nclass_total;

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(0) << Reset() << "Performed " << Ntot << " redistribution tests for \""
                         << discretization_composition->GetName() << "\" in " << AMCTimer<CPU>::GetTimer(time_index_tot)
                         << " seconds (" << AMCTimer<CPU>::GetTimer(time_index_tot) / real(Ntot > 0 ? Ntot : 1)
                         << " seconds/test)." << endl;
#endif
#endif
  }


  template<class R, class D>
  void ClassRedistributionComposition<R, D>::_TestSelf_(const vector<real> &concentration_aer_mass,
                                                        int &Nclass, vector<int> &index_internal_mixing,
                                                        vector<int> &index_class,
                                                        vector<vector<real> > &concentration_mass_class)
  {
    // Compute total aerosol mass and chemical composition.
    real concentration_aer_mass_total(real(0));

    for (int i = 0; i < Nspecies_total_; i++)
      concentration_aer_mass_total += concentration_aer_mass[i];

    // Put total concentration at end of vector.
    concentration_mass_class[0].back() = real(1);

    // Put composition.
    for (int i = 0; i < Nspecies_total_; i++)
      concentration_mass_class[0][i] = concentration_aer_mass[i] / concentration_aer_mass_total;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "composition = " << concentration_mass_class[0] << endl;
#endif

    // Number of classes over which we will redistribute.
    Nclass = 0;

    // Redistribute over class composition for this size section.
    Redistribute(Nclass, index_internal_mixing, index_class, concentration_mass_class);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "Nclass = " << Nclass << endl;
    CBUG << "index_internal_mixing = " << index_internal_mixing << endl;
    CBUG << "index_class = " << index_class << endl;
#endif

    // Give back to concentration arrays.
    for (int i = 0; i < Nclass; i++)
      {
        const int &j = index_class[i];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "\tj = " << j << endl;
#endif

        concentration_mass_class[j].back() *= concentration_aer_mass_total;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        if (abs(compute_vector_sum(concentration_mass_class[j], -1) - real(1))
            > AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_SUM)
          {
            ostringstream sout;
            sout << "Sum of vector \"concentration_mass_class\" = " << concentration_mass_class[j] << " is not unity.";
            throw AMC::Error(sout.str());
          }
#endif
      }
  }
#endif


  // Manage tables.
  template<class R, class D>
  void ClassRedistributionComposition<R, D>::TableCompute()
  {
    if (R::Type() != AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of redistribution composition is not \"Table\".");

    // Pointer redistribution composition.
    ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    vector<int> next(Nclass_total_, 0);
    vector<bool> is_computed(Nclass_total_, false);

    while (redistribution_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << redistribution_composition_ptr->name_ << "\"." << endl;
#endif

        if (redistribution_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            redistribution_composition_ptr = redistribution_composition_ptr->root_;
            continue;
          }

        int &index = redistribution_composition_ptr->index_;
        if (is_computed[index])
          {
            if (next[index] == redistribution_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "No more nested class, going up." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << redistribution_composition_ptr->name_
                                     << "\" is already computed, go down to nested class number "
                                     << next[index] << "." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->nested_[next[index]++];
              }
          }
        else
          {
            redistribution_composition_ptr->R::Compute();
            is_computed[index] = true;
          }
      }
  }


  template<class R, class D>
  void ClassRedistributionComposition<R, D>::TableClear()
  {
    if (R::Type() != AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of redistribution composition is not \"Table\".");

    // Pointer redistribution composition.
    ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    vector<int> next(Nclass_total_, 0);
    vector<bool> is_cleared(Nclass_total_, false);

    while (redistribution_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << redistribution_composition_ptr->name_ << "\"." << endl;
#endif

        if (redistribution_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            redistribution_composition_ptr = redistribution_composition_ptr->root_;
            continue;
          }

        int &index = redistribution_composition_ptr->index_;

        if (is_cleared[index])
          {
            if (next[index] == redistribution_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "No more nested class, going up." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << redistribution_composition_ptr->name_
                                     << "\" is already cleared, go down to nested class number "
                                     << next[index] << "." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->nested_[next[index]++];
              }
          }
        else
          {
            redistribution_composition_ptr->R::Clear();
            is_cleared[index] = true;
          }
      }
  }


  template<class R, class D>
  void ClassRedistributionComposition<R, D>::TableRead()
  {
    if (R::Type() != AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of redistribution composition is not \"Table\".");

    // Pointer redistribution composition.
    ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    vector<int> next(Nclass_total_, 0);
    vector<bool> is_read(Nclass_total_, false);

    while (redistribution_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << redistribution_composition_ptr->name_ << "\"." << endl;
#endif

        if (redistribution_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            redistribution_composition_ptr = redistribution_composition_ptr->root_;
            continue;
          }

        int &index = redistribution_composition_ptr->index_;

        if (is_read[index])
          {
            if (next[index] == redistribution_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "No more nested class, going up." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << redistribution_composition_ptr->name_
                                     << "\" is already read, go down to nested class number "
                                     << next[index] << "." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->nested_[next[index]++];
              }
          }
        else
          {
            redistribution_composition_ptr->R::Read();
            is_read[index] = true;
          }
      }
  }


  template<class R, class D>
  void ClassRedistributionComposition<R, D>::TableWrite() const
  {
    if (R::Type() != AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of redistribution composition is not \"Table\".");

    // Pointer redistribution composition.
    const ClassRedistributionComposition<R, D>* redistribution_composition_ptr = this;

    vector<int> next(Nclass_total_, 0);
    vector<bool> is_written(Nclass_total_, false);

    while (redistribution_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << redistribution_composition_ptr->name_ << "\"." << endl;
#endif

        if (redistribution_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Debug(3) << "This is an internal mixing, going up." << endl;
#endif
            redistribution_composition_ptr = redistribution_composition_ptr->root_;
            continue;
          }

        const int &index = redistribution_composition_ptr->index_;

        if (is_written[index])
          {
            if (next[index] == redistribution_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Debug(3) << "No more nested class, going up." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << redistribution_composition_ptr->name_
                                     << "\" is already written, go down to nested class number "
                                     << next[index] << "." << endl;
#endif
                redistribution_composition_ptr = redistribution_composition_ptr->nested_[next[index]++];
              }
          }
        else
          {
            redistribution_composition_ptr->R::Write();
            is_written[index] = true;
          }
      }
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_CXX
#endif
