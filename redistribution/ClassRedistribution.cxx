// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_CXX

#include "ClassRedistribution.hxx"

namespace AMC
{
  // Init static data.
  template<class S, class R, class D>
  void ClassRedistribution<S, R, D>::Init(Ops::Ops &ops)
  {
    //
    // Size redistribution.
    //

    ops.SetPrefix(ClassConfiguration::GetPrefix() + ".redistribution.size.");
    redistribution_size_ = new redistribution_size_type(ops, ClassAerosolData::Ng_);

    Nsection_max_ = 0;
    for (int i = 0; i < redistribution_size_->GetNsection(); i++)
      if (Nsection_max_ < redistribution_size_->GetNredistribute(i))
        Nsection_max_ = redistribution_size_->GetNredistribute(i);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Debug(2) << Reset() << "Nsection_max = " << Nsection_max_ << endl;
#endif

    //
    // Composition redistribution.
    //

    ClassRedistributionClass<R, D>::Init(ops);

#ifdef AMC_WITH_LAYER
    //
    // layer redistribution.
    //

    ClassRedistributionLayer::Init();

    // Working array similar to ClassAerosolData::layer_repartition_.
    layer_repartition_work_.resize(ClassAerosolData::NgNspecies_, ClassLayer::Nlayer_max_);
    layer_repartition_work_ = real(0);
#endif

    //
    // Set working arrays.
    //

    concentration_aer_number_work_.assign(ClassAerosolData::Ng_, real(0));
    concentration_aer_mass_work_.assign(ClassAerosolData::NgNspecies_, real(0));
    concentration_aer_mass_total_work_.assign(ClassAerosolData::Ng_, real(0));
  }


  // Clear data.
  template<class S, class R, class D>
  void ClassRedistribution<S, R, D>::Clear()
  {
    delete redistribution_size_;
    Nsection_max_ = 0;

    ClassRedistributionClass<R, D>::Clear();

#ifdef AMC_WITH_LAYER
    ClassRedistributionLayer::Clear();
    layer_repartition_work_.free();
#endif

    concentration_aer_number_work_.clear();
    concentration_aer_mass_work_.clear();
    concentration_aer_mass_total_work_.clear();
  }


  // Redistribute number and mass over size sections and class compositions.
  template<class S, class R, class D>
  void ClassRedistribution<S, R, D>::Redistribute(const int section_min,
                                                  const int section_max,
                                                  real *concentration_aer_number,
                                                  real *concentration_aer_mass)
  {
    // Some local variables.
    vector1i section_index(Nsection_max_, 0);
    vector1r section_number(Nsection_max_, real(0));
    vector1r section_mass(Nsection_max_, real(0));
    vector1i index_class(ClassRedistributionClass<R, D>::Nclass_max_, 0);
    vector1i index_internal_mixing(ClassRedistributionClass<R, D>::Nclass_max_, 0);
    vector2r concentration_mass_class(ClassRedistributionClass<R, D>::Nclass_max_,
                                      vector1r(ClassSpecies::Nspecies_ + 1, real(0)));

    // Total number and mass.
    real number_total(real(0));
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      number_total += concentration_aer_number[i];

    real mass_total(real(0));
    for (int i = 0; i < ClassAerosolData::NgNspecies_; i++)
      mass_total += concentration_aer_mass[i];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION
    CBUG << "number_total = " << number_total << endl;
    CBUG << "mass_total = " << mass_total << endl;
#endif

    // Size and composition redistribution. 
    concentration_aer_number_work_.assign(ClassAerosolData::Ng_, real(0));
    concentration_aer_mass_work_.assign(ClassAerosolData::NgNspecies_, real(0));
    concentration_aer_mass_total_work_.assign(ClassAerosolData::Ng_, real(0));
#ifdef AMC_WITH_LAYER
    layer_repartition_work_ = real(0);
#endif

    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        {
#ifdef AMC_WITH_DEBUG_REDISTRIBUTION
          CBUG << "section = " << i << endl;
#endif

          // Compute composition and total aerosol concentration in general section.
          real concentration_aer_mass_total(real(0));
          int j = i * ClassSpecies::Nspecies_;
          for (int k = 0; k < ClassSpecies::Nspecies_; k++)
            {
              concentration_mass_class[0][k] = concentration_aer_mass[j++];
              concentration_aer_mass_total += concentration_mass_class[0][k];
            }

          if (concentration_aer_mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
            {
              // Put composition and total concentration at beginning of vector.
              // We put 1 instead of total concentration so as to get mass
              // fractions which are applied afterwards.
              for (int k = 0; k < ClassSpecies::Nspecies_; k++)
                concentration_mass_class[0][k] /= concentration_aer_mass_total;
              concentration_mass_class[0].back() = real(1);

              // Number of size sections over which we will redistribute.
              int Nsection(0);

              // Size redistribution scheme.
              redistribution_size_->Redistribute(number_total, mass_total,
                                                 concentration_aer_number[i],
                                                 concentration_aer_mass_total,
                                                 Nsection, section_index,
                                                 section_number, section_mass);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION
              CBUG << "Nsection = " << Nsection << endl;
              CBUG << "section_index = " << section_index << endl;
              CBUG << "section_number = " << section_number << endl;
              CBUG << "section_mass = " << section_mass << endl;
#endif

              // Redistribute on class compositions.
              for (int k = 0; k < Nsection; k++)
                {
                  // Size section index over which particles are redistributed.
                  const int l = section_index[k];

                  // Number of classes over which we will redistribute.
                  int Nclass(0);

                  // Redistribute over class composition for this size section.
#ifdef AMC_WITH_TRACE
                  int trace_index = ClassTrace::general_section_index_reverse_[i];

                  if (trace_index >= 0)
                    trace_index = ClassTrace::size_class_index_[trace_index][l];

                  if (trace_index < 0)
                    ClassRedistributionClass<R, D>::redistribution_composition_[l]
                      ->Redistribute(Nclass, index_internal_mixing,
                                     index_class, concentration_mass_class);
                  else
                    {
                      Nclass = 1;
                      index_class[0] = 0;
                      index_internal_mixing[0] = trace_index;
                      // No need to alter concentration_mass_class as
                      // we make index_class point to first element.
                    }
#else
                  ClassRedistributionClass<R, D>::redistribution_composition_[l]
                    ->Redistribute(Nclass, index_internal_mixing,
                                   index_class, concentration_mass_class);
#endif

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION
                  CBUG << "\tNclass = " << Nclass << endl;
                  CBUG << "\tindex_internal_mixing = " << index_internal_mixing << endl;
                  CBUG << "\tindex_class = " << index_class << endl;
#endif

                  // Give back to concentration arrays.
                  for (int m = 0; m < Nclass; ++m)
                    {
                      const int n = ClassAerosolData::general_section_reverse_[l][index_internal_mixing[m]];

                      const int p = index_class[m];

                      real concentration_mass_total = concentration_mass_class[p].back();
                      const real concentration_number = section_number[k] * concentration_mass_total;
                      concentration_mass_total *= section_mass[k];

                      j = n * ClassSpecies::Nspecies_;
#ifdef AMC_WITH_LAYER
                      // Redistribute layers of general section i into n.
                      ClassRedistributionLayer::Redistribute(i, n,
                                                             i * ClassSpecies::Nspecies_, j,
                                                             concentration_number,
                                                             concentration_aer_number_work_[n],
                                                             concentration_mass_total,
                                                             concentration_aer_mass_total_work_[n],
                                                             concentration_mass_class[p].data(),
                                                             NULL,
                                                             concentration_aer_mass_work_.data() + j,
                                                             ClassAerosolData::layer_repartition_,
                                                             layer_repartition_work_);
#endif

                      concentration_aer_number_work_[n] += concentration_number;
                      concentration_aer_mass_total_work_[n] += concentration_mass_total;

                      for (int q = 0; q < ClassSpecies::Nspecies_; ++q)
                        concentration_aer_mass_work_[j++] += concentration_mass_class[p][q] * concentration_mass_total;
                    }
                }
            }
        }

    // Nullify the sections we have just redistributed.
    // This also clean up concentrations below threshold concentration levels.
    int h = section_min * ClassSpecies::Nspecies_ - 1;
    for (int i = section_min; i < section_max; ++i)
      {
        concentration_aer_number[i] = real(0);
        for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          concentration_aer_mass[++h] = real(0);
      }

#ifdef AMC_WITH_LAYER
    // If there remain something in general section, we have
    // again to merge layers, otherwise it is straightforward.
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        const int j = i * ClassSpecies::Nspecies_;

        if (concentration_aer_number[i] > real(0))
          {
            real concentration_mass_total(real(0));
            for (int k = 0; k < ClassSpecies::Nspecies_; ++k)
              concentration_mass_total += concentration_aer_mass[j + k];

            ClassRedistributionLayer::Redistribute(i, i, j, j,
                                                   concentration_aer_number_work_[i],
                                                   concentration_aer_number[i],
                                                   concentration_aer_mass_total_work_[i],
                                                   concentration_mass_total,
                                                   NULL,
                                                   concentration_aer_mass_work_.data() + j,
                                                   concentration_aer_mass + j,
                                                   ClassAerosolData::layer_repartition_,
                                                   layer_repartition_work_);
          }

        // Finally give back new layer repartitions.
        for (int j = 0; j < ClassLayer::Nspecies_layer_; ++j)
          {
            const int k = j + ClassLayer::species_index_layer_[j];
            for (int l = 0; l < ClassLayer::Nlayer_[i]; ++l)
              ClassAerosolData::layer_repartition_[k][l] = layer_repartition_work_(k, l);
          }
      }
#endif

    // Give back working arrays to concentration arrays. This must be a += operation.
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      concentration_aer_number[i] += concentration_aer_number_work_[i];

    for (int i = 0; i < ClassAerosolData::NgNspecies_; ++i)
      concentration_aer_mass[i] += concentration_aer_mass_work_[i];

    // Normalize concentrations if size redistribution algorithm was not conservative.
    redistribution_size_->Normalize(number_total, mass_total,
                                    concentration_aer_number,
                                    concentration_aer_mass,
                                    ClassSpecies::Nspecies_);
  }


#ifdef AMC_WITH_TEST
  // Test method.
  template<class S, class R, class D>
  void ClassRedistribution<S, R, D>::Test(const ClassModalDistribution<real> &modal_distribution, int N, real tol, bool check)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test whole redistribution with modal distribution \""
                         << modal_distribution.GetName() << "\", with size algorithm \""
                         << redistribution_size_->GetType() << "\" and "
                         << (R::Type() == AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE ? "tabulated" : "base")
                         << " redistribution composition. " << Fred().Str() << "N = " << N << Reset().Str()
                         << ", " << Fmagenta().Str() << "tol = " << scientific << tol << Reset().Str() << endl;

    if (redistribution_size_->GetType() != "MovingDiameter")
      *AMCLogger::GetLog() << Bblue() << Warning() << Reset() << "On rare cases, total number and total mass"
                           << " may not be conserved with this size redistribution algorithm." << endl;
#endif

#ifdef AMC_WITH_TIMER
    int time_index = AMCTimer<CPU>::Add("test_redistribution_" + modal_distribution.GetName());
#endif

    // Record relative difference between redistributed and initial total number and mass.
    real number_total_diff_relative(real(0)), mass_total_diff_relative(real(0));

    for (int i = 0; i < N; i++)
      {
        // Allocate model concentration arrays.
        vector1r concentration_aer_number(ClassAerosolData::Ng_, real(0));
        vector1r concentration_aer_mass(ClassAerosolData::NgNspecies_, real(0));

        // Generate one random aerosol modal distribution.
        vector1r diameter_mean;
        vector2r class_distribution;
        ClassAMC<D>::GenerateModalDistribution(modal_distribution,
                                               concentration_aer_number.data(),
                                               concentration_aer_mass.data(),
                                               diameter_mean,
                                               class_distribution);

        // Redistribute it.
        int section_min(0), section_max(ClassAerosolData::Ng_);
        vector1r concentration_aer_number_redist = concentration_aer_number;
        vector1r concentration_aer_mass_redist = concentration_aer_mass;

#ifdef AMC_WITH_TIMER
        AMCTimer<CPU>::Start();
#endif

        Redistribute(section_min, section_max,
                     concentration_aer_number_redist.data(),
                     concentration_aer_mass_redist.data());

#ifdef AMC_WITH_TIMER
        AMCTimer<CPU>::Stop(time_index);
#endif

        if (! check)
          continue;

        real number_total = compute_vector_sum(concentration_aer_number);
        real mass_total = compute_vector_sum(concentration_aer_mass);

        real number_total_redist = compute_vector_sum(concentration_aer_number_redist);
        real mass_total_redist = compute_vector_sum(concentration_aer_mass_redist);

        // Perform some checks.
        real number_total_diff_rel = abs(number_total_redist - number_total) / number_total;
        real mass_total_diff_rel = abs(mass_total_redist - mass_total) / mass_total;

        if (number_total_diff_rel > tol)
          {
            ostringstream sout;
            sout << "number_total_init = " << number_total << endl
                 << "number_total_redist = " << number_total_redist << endl
                 << "number_total_diff_rel = " << number_total_diff_rel << endl
                 << "Total redistributed number seems not equal to initial number." << endl;
            throw AMC::Error(sout.str());
          }

        if (mass_total_diff_rel > tol)
          {
            ostringstream sout;
            sout << "mass_total_init = " << mass_total << endl
                 << "mass_total_redist = " << mass_total_redist << endl
                 << "mass_total_diff_rel = " << mass_total_diff_rel << endl
                 << "Total redistributed mass seems not equal to initial mass." << endl;
            throw AMC::Error(sout.str());
          }

        number_total_diff_relative += number_total_diff_rel;
        mass_total_diff_relative += mass_total_diff_rel;

        for (int k = 0; k < ClassAerosolData::Ng_; ++k)
          if (concentration_aer_number_redist[k] > AMC_NUMBER_CONCENTRATION_MINIMUM)
            {
              real concentration_aer_mass_redist_tot = real(0);
              vector1r composition(ClassSpecies::Nspecies_, real(0));

              int j = k * ClassSpecies::Nspecies_;
              for (int l = 0; l < ClassSpecies::Nspecies_; l++)
                {
                  composition[l] = concentration_aer_mass_redist[j++];
                  concentration_aer_mass_redist_tot += composition[l];
                }

              if (concentration_aer_mass_redist_tot > AMC_MASS_CONCENTRATION_MINIMUM)
                {
                  for (int l = 0; l < ClassSpecies::Nspecies_; l++)
                    composition[l] /= concentration_aer_mass_redist_tot;

                  // Check section index.
                  real mass_single_log = log(concentration_aer_mass_redist_tot / concentration_aer_number_redist[k]);
                  int m = search_index<real>(ClassDiscretizationSize::mass_bound_log_, mass_single_log);
                  if (m != ClassAerosolData::general_section_[k][0])
                    throw AMC::Error("Wrong size index, expected " +
                                     to_str(ClassAerosolData::general_section_[k][0])
                                     + ", got " + to_str(m) + ".");

                  // Check class index.
                  bool on_edge;
                  int n = ClassDiscretizationClass<D>::discretization_composition_[m]->
                    FindClassCompositionIndex(composition, on_edge);

                  if (! on_edge)
                    if (n != ClassAerosolData::general_section_[k][1])
                      throw AMC::Error("Wrong class size, expected " +
                                       to_str(ClassAerosolData::general_section_[k][1])
                                       + ", got " + to_str(n) + ".");
                }
            }
      }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << modal_distribution.GetName()
                         << " : CPU time = " << AMCTimer<CPU>::GetTimer(time_index) << " seconds (" << scientific
                         << AMCTimer<CPU>::GetTimer(time_index) / real(N > 0 ? N : 1) << " seconds/test)." << endl;
#endif
#endif

    // Display relative difference for total number and mass.
    if (N > 0)
      {
        number_total_diff_relative /= real(N);
        mass_total_diff_relative /= real(N);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Average on " << N << " loops :" << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset()
                             << "\tTotal number relative difference = " << number_total_diff_relative << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset()
                             << "\tTotal mass relative difference = " << mass_total_diff_relative << endl;
#endif
      }
  }
#endif
}

#define AMC_FILE_CLASS_REDISTRIBUTION_CXX
#endif
