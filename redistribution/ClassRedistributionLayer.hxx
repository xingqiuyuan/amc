// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_LAYER_HXX

namespace AMC
{
  /*! 
   * \class ClassRedistributionLayer
   */
  class ClassRedistributionLayer : protected ClassAerosolData
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector3i vector3i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
    typedef typename AMC::vector3r vector3r;

  protected:

    /*!< Layer redistribution coefficients.*/
    static Array<real, 2> layer_redistribution_coefficient_a_, layer_redistribution_coefficient_b_;

    /*!< Mass layer fractions for each general section.*/
    static vector2r layer_mass_fraction_;

    /*!< Compute layer redistribution coefficients.*/
    static void compute_layer_redistribution_coefficient(const int u, const int v, const real &single_mass_u,
                                                         Array<real, 2> &layer_redistribution_coefficient_uv);

  public:

    /*!< Init static data.*/
    static void Init();

    /*!< Clear data.*/
    static void Clear();

#ifndef SWIG
    /*!< Redistribute.*/
    static void Redistribute(const int a, const int b,
                             const int pos_a, const int pos_b,
                             const real &concentration_number_a,
                             const real &concentration_number_b,
                             const real &concentration_mass_total_a,
                             const real &concentration_mass_total_b,
                             const real *composition_mass_a,
                             const real *concentration_mass_a,
                             const real *concentration_mass_b,
                             const vector<vector<real> > &layer_repartition_a,
                             Array<real, 2> &layer_repartition_b);
#endif

#ifdef AMC_WITH_TEST
    /*!< Test method.*/
    static void Test(const int N = 10000);
#endif
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_LAYER_HXX
#endif
