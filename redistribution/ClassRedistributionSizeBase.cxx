// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_BASE_CXX

#include "ClassRedistributionSizeBase.hxx"

namespace AMC
{
  // Constructors.
  ClassRedistributionSizeBase::ClassRedistributionSizeBase(Ops::Ops &ops, const int &Ng, const string type)
    : Ng_(Ng), Nsection_(0), Nbound_(0), type_(type)
  {
    if (ClassDiscretizationSize::Nsection_ == 0)
      throw AMC::Error("Discretization size subsystem seems not initialized, call it first.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Instantiate size redistribution scheme \""
                         << type_ << "\"." << endl;
#endif

    // Default redistribution sections.
    Nsection_ = ClassDiscretizationSize::Nsection_ + 1;
    Nbound_ = Nsection_ + 1;

    diameter_bound_.resize(Nbound_);
    diameter_bound_.front() = ClassDiscretizationSize::diameter_bound_.front();
    diameter_bound_.back() = ClassDiscretizationSize::diameter_bound_.back();

    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      diameter_bound_[i + 1] = ClassDiscretizationSize::diameter_mean_[i];

    ops.Set("diameter_bound", "", diameter_bound_, diameter_bound_);
    Nbound_ = int(diameter_bound_.size());
    Nsection_ = Nbound_ - 1;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Size redistribution diameter bounds = " << diameter_bound_ << endl;
#endif

    mass_bound_.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      mass_bound_[i] = AMC_PI6 * density_fixed_ * diameter_bound_[i] * diameter_bound_[i] * diameter_bound_[i];

    mass_bound_log_.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      mass_bound_log_[i] = log(mass_bound_[i]);

    // In which real size sections redistribute according to the redistribute section.
    Nredistribute_.resize(Nsection_);
    redistribute_index_.resize(Nsection_);

    for (int i = 0; i < Nsection_; i++)
      {
        int j1 = search_index<real>(ClassDiscretizationSize::diameter_bound_, diameter_bound_[i]);
        int j2 = search_index<real>(ClassDiscretizationSize::diameter_bound_, diameter_bound_[i + 1]);

        // For last section, if upper diameter bound equals that of size discretization,
        // j2 may equal or exceed the number of section, in that case perform a manual correction.
        if (i == (Nsection_ - 1))
          if (j2 == ClassDiscretizationSize::Nsection_)
            j2--;

        if (j1 < 0 || j1 >= ClassDiscretizationSize::Nsection_ ||
            j2 < 0 || j2 >= ClassDiscretizationSize::Nsection_)
          throw AMC::Error("j1=" + to_str(j1) + " and/or j2=" + to_str(j2)
                           + " are/is outside of size discretization.");

        redistribute_index_[i] = j1;
        Nredistribute_[i] = j2 - j1 + 1;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "Size section "
                             << i << " ([" << setw(6) << setprecision(4)
                             << diameter_bound_[i] << ", " << setw(6) << setprecision(4)
                             << diameter_bound_[i + 1] << "]) redistributed over sections from "
                             << j1 << " to " << j2 << " (" << Nredistribute_[i] << ")" << endl;
#endif
      }

    return;
  }


  // Destructor.
  ClassRedistributionSizeBase::~ClassRedistributionSizeBase()
  {
    return;
  }


  // Get methods.
  void ClassRedistributionSizeBase::GetDiameterBound(vector<real> &diameter_bound) const
  {
    diameter_bound = diameter_bound_;
  }


  int ClassRedistributionSizeBase::GetNsection() const
  {
    return Nsection_;
  }


  int ClassRedistributionSizeBase::GetNbound() const
  {
    return Nbound_;
  }


  int ClassRedistributionSizeBase::GetNredistribute(const int &i) const
  {
    return Nredistribute_[i];
  }


  string ClassRedistributionSizeBase::GetType() const
  {
    return type_;
  }


  // Redistribute one section over existing size sections, does not modify composition.
  inline void ClassRedistributionSizeBase::Redistribute(const real &number_total,
                                                        const real &mass_total,
                                                        const real &concentration_aer_number,
                                                        const real &concentration_aer_mass_total,
                                                        int &Nsection,
                                                        vector1i &section_index,
                                                        vector1r &section_number,
                                                        vector1r &section_mass) const
  {
    // Number and mass fractions out of total number and mass for this section.
    real number_fraction = concentration_aer_number / number_total;
    real mass_fraction = concentration_aer_mass_total / mass_total;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "number_fraction = " << number_fraction << endl;
    CBUG << "mass_fraction = " << mass_fraction << endl;
#endif

    // Ratio between fractions : the greater means this section
    // contains more mass than number and conversely.
    real ratio_fraction = mass_fraction / (number_fraction + mass_fraction);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "ratio_fraction = " << ratio_fraction << endl;
#endif

    // Compute current moving mass.
    real mass_moving = concentration_aer_mass_total / concentration_aer_number;
    real mass_moving_log = log(mass_moving);

    // For each general section, compute redistribution coefficients,
    // and the size section indexes over which the number and mass
    // are redistributed.
    int section_redistribute;
    this->compute_coefficient(mass_moving, mass_moving_log,
                              ratio_fraction, section_redistribute, 
                              section_number, section_mass);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "section_redistribute = " << section_redistribute << endl;
    CBUG << "coefficient_number = " << section_number << endl;
    CBUG << "coefficient_mass = " << section_mass << endl;
#endif

    // Reset counter.
    Nsection = 0;

    // Now redistribute number and mass.
    const int i = ClassRedistributionSizeBase::redistribute_index_[section_redistribute];
    for (int j = 0; j < ClassRedistributionSizeBase::Nredistribute_[section_redistribute]; ++j)
      if (section_number[j] > real(0))
        {
          // Nsection and j are equal indices only if above condition is satisfied.
          section_index[Nsection] = i + j;
          section_number[Nsection] = section_number[j] * concentration_aer_number;
          section_mass[Nsection] = section_mass[j] * concentration_aer_mass_total;
          Nsection++;
        }
  }


  // Normalize concentration to get number and mass conservation.
  void ClassRedistributionSizeBase::Normalize(const real &number_total_initial,
                                              const real &mass_total_initial,
                                              real *concentration_aer_number,
                                              real *concentration_aer_mass,
                                              const int Nspecies) const
  {
    // Compute new total number and mass.
    real number_total(real(0)), mass_total(real(0));
    vector1r concentration_aer_mass_total(Ng_, real(0));

    int h(0);
    for (int i = 0; i < Ng_; i++)
      {
        number_total += concentration_aer_number[i];

        for (int j = 0; j < Nspecies; j++)
          concentration_aer_mass_total[i] += concentration_aer_mass[h++];

        mass_total += concentration_aer_mass_total[i];
      }

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "number_total = " << number_total << endl;
    CBUG << "mass_total = " << mass_total << endl;
#endif

    // Find a cutoff index.
    const real ratio_initial = mass_total_initial / number_total_initial;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "ratio_initial = " << ratio_initial << endl;
#endif

    real fraction_number0(real(0)), fraction_mass0(real(0));
    real fraction_number1(real(0)), fraction_mass1(real(0));

    int cutoff;
    for (cutoff = 0; cutoff < Ng_; cutoff++)
      {
        fraction_number0 += concentration_aer_number[cutoff];
        fraction_mass0 += concentration_aer_mass_total[cutoff];

        fraction_number1 = number_total - fraction_number0;
        fraction_mass1 = mass_total - fraction_mass0;

        if (fraction_number0 > fraction_number1)
          if (fraction_mass1 > fraction_mass0)
            if (fraction_mass1 > ratio_initial * fraction_number1)
              if (fraction_mass0 < ratio_initial * fraction_number0)
                break;
      }
    cutoff++;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "cutoff = " << cutoff << endl;
#endif

    // Found one cutoff index if loop stoped before last general section.
    if (cutoff < Ng_)
      {
        real det = fraction_number0 * fraction_mass1 - fraction_number1 * fraction_mass0;
        real coefficient0 = (fraction_mass1 * number_total_initial - fraction_number1 * mass_total_initial) / det;
        real coefficient1 = (fraction_number0 * mass_total_initial - fraction_mass0 * number_total_initial) / det;

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
        CBUG << "coefficient0 = " << coefficient0 << endl;
        CBUG << "coefficient1 = " << coefficient1 << endl;
#endif

        for (int i = 0; i < cutoff; i++)
          {
            concentration_aer_number[i] *= coefficient0;

            int j = i * Nspecies;
            for (int k = 0; k < Nspecies; k++)
              concentration_aer_mass[j++] *= coefficient0;
          }

        for (int i = cutoff; i < Ng_; i++)
          {
            concentration_aer_number[i] *= coefficient1;

            int j = i * Nspecies;
            for (int k = 0; k < Nspecies; k++)
              concentration_aer_mass[j++] *= coefficient1;
          }
      }
  }


#ifdef AMC_WITH_TEST
  // Compute redistribution.
  void ClassRedistributionSizeBase::_Test_(const int &section_min,
                                           const int &section_max,
                                           vector<real> &concentration_aer_number,
                                           vector<real> &concentration_aer_mass_total,
                                           vector<real> &diameter_mean,
                                           const bool normalize) const
  {
    // For test purposes, internal mixing is enough.
    if (Ng_ != ClassDiscretizationSize::Nsection_)
      throw AMC::Error("Size redistribution may only be tested in internal mixing.");

    // Total number and mass.
    real number_total(real(0)), mass_total(real(0));
    for (int i = 0; i < Ng_; i++)
      {
        number_total += concentration_aer_number[i];
        mass_total += concentration_aer_mass_total[i];
      }

    // Max number of section in which redistribute.
    int Nsection_max(0);
    for (int i = 0; i < Nsection_; i++)
      if (Nsection_max < Nredistribute_[i])
        Nsection_max = Nredistribute_[i];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "number_total = " << number_total << endl;
    CBUG << "mass_total = " << mass_total << endl;
    CBUG << "Nsection_max = " << Nsection_max << endl;
#endif

    // Local variables.
    vector1i section_index(Nsection_max, 0);
    vector1r section_number(Nsection_max, real(0));
    vector1r section_mass(Nsection_max, real(0));

    int Nredistribute(0);
    vector1i redistribute_index(Nsection_max * Ng_, 0);
    vector1r redistribute_number(Nsection_max * Ng_, real(0));
    vector1r redistribute_mass(Nsection_max * Ng_, real(0));

    // Process each section.
    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        if (concentration_aer_mass_total[i] > AMC_MASS_CONCENTRATION_MINIMUM)
          {
            // Number of size sections over which we will redistribute.
            int Nsection(0);

            // Size redistribution scheme.
            Redistribute(number_total, mass_total,
                         concentration_aer_number[i],
                         concentration_aer_mass_total[i],
                         Nsection, section_index,
                         section_number, section_mass);

            for (int j = 0; j < Nsection; j++)
              {
                redistribute_index[Nredistribute] = section_index[j];
                redistribute_number[Nredistribute] = section_number[j];
                redistribute_mass[Nredistribute] = section_mass[j];
                Nredistribute++;
              }
          }

    // Nullify the concentrations we have just redistributed.
    // This also nullify those sections with too few number or mass in it.
    for (int i = section_min; i < section_max; i++)
      {
        concentration_aer_number[i] = real(0);
        concentration_aer_mass_total[i] = real(0);
      }

    // Give back to concentration array.
    for (int i = 0; i < Nredistribute; i++)
      {
        const int j = redistribute_index[i];

        // Redistribute number.
        concentration_aer_number[j] += redistribute_number[i];

        // Redistribute mass.
        concentration_aer_mass_total[j] += redistribute_mass[i];
      }

    // Normalize concentrations.
    if (normalize)
      this->Normalize(number_total, mass_total,
                      concentration_aer_number.data(),
                      concentration_aer_mass_total.data());

    // Compute the average diameter in each section.
    diameter_mean = ClassDiscretizationSize::diameter_mean_;

    for (int i = 0; i < Ng_; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        if (concentration_aer_mass_total[i] > AMC_MASS_CONCENTRATION_MINIMUM)
          diameter_mean[i] = pow(concentration_aer_mass_total[i] * AMC_INV_PI6
                                 / (concentration_aer_number[i] * ClassDiscretizationSize::density_fixed_), AMC_FRAC3);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_SIZE
    CBUG << "diameter_mean = " << ClassDiscretizationSize::diameter_mean_ << endl;
    CBUG << "diameter_mean_redist = " << diameter_mean << endl;
#endif
  }
#endif
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_BASE_CXX
#endif
