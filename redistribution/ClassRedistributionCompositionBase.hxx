// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_BASE_HXX

#define AMC_REDISTRIBUTION_COMPOSITION_TYPE_BASE  1
#define AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE 2
#define AMC_REDISTRIBUTION_COMPOSITION_FRACTION_NEAR_POINT_THRESHOLD 1.e-6
#define AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_ZERO 1.e-15

namespace AMC
{
  /*! 
   * \class ClassRedistributionCompositionBase
   */
  template<class D>
  class ClassRedistributionCompositionBase
  {
  public:
    typedef AMC::real real;
    typedef float real_polygon;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef D discretization_composition_type;
   
  protected:

    /*!< Redistribution composition unique identifier.*/
    static int id_counter_;

    /*!< Name of redistribution composition, proxy from discretization composition pointer.*/
    string name_;

    /*!< Redistribution composition unique identifier.*/
    int id_;

    /*!< Is it internal mixing, proxy from discretization composition pointer.*/
    bool internal_mixing_;

    /*!< Number of dimensions, proxy from discretization composition pointer.*/
    int Ndim_, Ndim2_;

    /*!< Number of class, proxy from discretization composition pointer.*/
    int Nclass_;

    /*!< Number of points.*/
    int Npoint_;

    /*!< Number of polygons.*/
    int Npolygon_;

    /*!< Size of linear system.*/
    int Nlinear_, Nlinear2_;

    /*!< Root concentration index.*/
    int index_root_;

    /*!< Class redistribution concentration index.*/
    vector1i index_class_;

    /*!< Pointer to the associated composition discretization.*/
    D* discretization_composition_;

    /*!< Is polygon inside one class composition ?.*/
    vector1b polygon_in_class_;

    /*!< Sign of polygon area, true if positive.*/
    vector1b polygon_sign_;

    /*!< Number of species defining each dimensions, proxy from discretization composition pointer.*/
    vector1i Nspecies_;

    /*!< Vector of species index defining each dimensions, proxy from discretization composition pointer.*/
    vector2i species_index_;

    /*!< Polygons over the edge of which we redistribute.*/
    vector2i polygon_;

    /*!< The composition class of the points of each polygon.*/
    vector2i polygon_class_;

    /*!< The polygon coefficients, to determine whether a point is inside.*/
    vector<vector<real_polygon> > polygon_coefficient_;

    /*!< The inverse of polygon determinant.*/
    vector1r polygon_determinant_inv_;

    /*!< The polygon matrix.*/
    vector2r polygon_matrix_;

    /*!< Points over which redistribute.*/
    vector2r point_;

    /*!< Find the polygon in which the fraction is located.*/
    bool in_polygon(const int &i, const vector1r &fraction) const;
    int find_polygon(const vector1r &fraction) const;
    int find_polygon(const vector1i &polygon_index, const vector1r &fraction) const;

    /*!< Apply redistribution coefficients.*/
    void apply_coefficient(const int &Nclass_work,
                           const vector1r &fraction,
                           const vector1i &class_index,
                           const vector2r &class_coefficient,
                           vector1b &class_work, vector2r &concentration) const;

    /*!< Compute redistribution coefficients from given fraction on class compositions.*/
    void compute_coefficient(const int &polygon_index, const vector1r &fraction,
                             int &Nclass_work, vector1i &class_index, vector2r &class_coefficient) const;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
    ClassRedistributionCompositionBase(D* discretization_composition);
    ClassRedistributionCompositionBase(D* discretization_composition, Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassRedistributionCompositionBase();

    /*!< Get methods.*/
    D* GetDiscretizationCompositionPtr() const;
    int GetNpolygon() const;
    void CollectPolygonPointList(const int &i, vector<int> &point_index) const;
    void CollectPointCoordinate(const int &i, vector<real> &coordinate) const;

    /*!< Redistribute number and mass over composition classes, does not modify size sections.*/
    void Redistribute(const vector1r &fraction, vector1b &class_work, vector2r &concentration) const;

    /*!< Fake methods for compatibility.*/
    virtual void Compute();
    virtual void Clear();
    virtual void Read();
    virtual void Write() const;
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_BASE_HXX
#endif
