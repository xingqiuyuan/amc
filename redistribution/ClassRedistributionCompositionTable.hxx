// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_TABLE_HXX

namespace AMC
{
  /*! 
   * \class ClassRedistributionCompositionTable
   */
  template<class D>
  class ClassRedistributionCompositionTable : public ClassRedistributionCompositionBase<D>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef D discretization_composition_type;
   
  private:

    /*!< Is table computed.*/
    bool is_table_computed_;

    /*!< Number of fraction sections.*/
    int Ndisc_;

    /*!< Number of fraction points.*/
    int Npoint_;

    /*!< Number of edges of hypercube.*/
    int Nedge_;

    /*!< Length of table, may be large.*/
    unsigned long Ntable_;

    /*!< Number of hypercube, may be large.*/
    unsigned long Ncube_;

    /*!< Path of table file.*/
    string path_;

    /*!< Increment to circle on all hypercube edges.*/
    vector2i increment_;

    /*!< The classes on which span each polygon, this shadows the same variable in parent class.*/
    vector2i polygon_class_;

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    /*!< Indexes of valid hypercube, those crossing the space composition.*/
    vector<unsigned long> table_cube_index_;
#endif

    /*!< Number of edges for each hypercube.*/
    vector1i table_cube_edge_number_;

    /*!< Table of edges for each hypercube.*/
    vector2i table_cube_edge_index_;

    /*!< Table of points for each hypercube.*/
    vector<vector<unsigned long> > table_cube_point_index_;

    /*!< Table of polygon index on which lie the hypercube.*/
    vector2i table_cube_polygon_index_;

    /*!< Table of class coefficients with which to redistribute for each point of hypercube.*/
    vector3r table_point_class_coefficient_;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
    ClassRedistributionCompositionTable(D* discretization_composition);
    ClassRedistributionCompositionTable(D* discretization_composition, Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassRedistributionCompositionTable();

    /*!< Get methods.*/
    unsigned long GetNpoint() const;
    unsigned long GetNcube() const;
    int GetNdisc() const;
    int GetNedge() const;

    /*!< Set methods.*/
    void SetNdisc(const int &Ndisc);

    /*!< Manage the table.*/
    void Compute();
    void Clear();
    void Read();
    void Write() const;

    /*!< Redistribute number and mass over composition classes, does not modify size sections.*/
    void Redistribute(const vector1r &fraction, vector1b &class_work, vector2r &concentration) const;
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_TABLE_HXX
#endif
