// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_TABLE_CXX

#include "ClassRedistributionCompositionTable.hxx"

namespace AMC
{
  // Type.
  template<class D>
  int ClassRedistributionCompositionTable<D>::Type()
  {
    return AMC_REDISTRIBUTION_COMPOSITION_TYPE_TABLE;
  }


  // Constructors.
  template<class D>
  ClassRedistributionCompositionTable<D>::ClassRedistributionCompositionTable(D* discretization_composition)
    : ClassRedistributionCompositionBase<D>(discretization_composition),
      is_table_computed_(false), Ndisc_(0), Npoint_(0), Ncube_(0), Nedge_(0), path_("")
  {
    return;
  }


  template<class D>
  ClassRedistributionCompositionTable<D>::ClassRedistributionCompositionTable(D* discretization_composition, Ops::Ops &ops)
    : ClassRedistributionCompositionBase<D>(discretization_composition, ops),
      is_table_computed_(false), Ndisc_(0), Npoint_(0), Ncube_(0), Nedge_(0), path_("")
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Instantiate tabulated redistribution composition." << endl;
#endif
#ifdef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << Reset() << "AMC built with fast tabulation indexing flag"
                         << ", this may consume more memory." << endl;
#endif
#endif

    // Set up prefix.
    string prefix_orig = ops.GetPrefix();

    string discretization_name = this->name_;
    string::size_type pos = discretization_name.find_last_of(':');

    if (pos != string::npos)
      discretization_name = discretization_name.substr(pos + 1);

    // Lookup table for redistribution.
    string prefix_redistribution = prefix_orig + discretization_name + ".";
    ops.SetPrefix(prefix_redistribution + "table.");

    Ndisc_ = ops.Get<int>("Ndisc");
    path_ = ops.Get<string>("path", "", path_);

    // If real path, read directly in constructor.
    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Ndisc = " << Ndisc_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "path = " << path_ << ", file_size = " << file_size << endl;
#endif

    if (file_size > 0)
      Read();

    // Revert to original prefix.
    ops.SetPrefix(prefix_orig);

    // Number of edges of each hypercube.
    Nedge_ = 1;
    for (int i = 0; i < this->Ndim_; i++)
      Nedge_ *= 2;

    // Increment for each hypercube edge from the down left corner.
    increment_.assign(Nedge_, vector1i(this->Ndim_));
    vector1i index(this->Ndim_, 0);

    for (int i = 0; i < Nedge_; i++)
      {
        increment_[i] = index;

        index[0]++;
        for (int j = 0; j < this->Ndim_ - 1; j++)
          if (index[j] == 2)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Debug(2) << Reset() << "Number of edges = " << Nedge_ << endl;
    for (int i = 0; i < Nedge_; i++)
      *AMCLogger::GetLog() << Bmagenta() << Debug(3) << Reset() << "Position of edge number "
                           << i << " is " << increment_[i] << endl;
#endif

    // The classes on which span each polygon : taken from parent class.
    polygon_class_ = ClassRedistributionCompositionBase<D>::polygon_class_;
    for (int i = 0; i < this->Npolygon_; i++)
      {
        sort(polygon_class_[i].begin(), polygon_class_[i].end());
        vector1i::iterator it = unique(polygon_class_[i].begin(), polygon_class_[i].end());
        polygon_class_[i].resize(distance(polygon_class_[i].begin(), it));
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "For each polygon, the ordered classes on which it spans :" << endl;
    for (int i = 0; i < this->Npolygon_; i++)
      *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "\tpolygon " << i << " : " << polygon_class_[i] << endl;
#endif

    return;
  }


  // Destructor.
  template<class D>
  ClassRedistributionCompositionTable<D>::~ClassRedistributionCompositionTable()
  {
    return;
  }


  // Get methods.
  template<class D>
  unsigned long ClassRedistributionCompositionTable<D>::GetNcube() const
  {
    return Ncube_;
  }


  template<class D>
  unsigned long ClassRedistributionCompositionTable<D>::GetNpoint() const
  {
    return Npoint_;
  }


  template<class D>
  int ClassRedistributionCompositionTable<D>::GetNdisc() const
  {
    return Ndisc_;
  }


  template<class D>
  int ClassRedistributionCompositionTable<D>::GetNedge() const
  {
    return Nedge_;
  }


  // Set methods.
  template<class D>
  void ClassRedistributionCompositionTable<D>::SetNdisc(const int &Ndisc)
  {
    Ndisc_ = Ndisc;
  }


  // Table.
  template<class D>
  void ClassRedistributionCompositionTable<D>::Compute()
  {
    if (Ndisc_ == 0)
      throw AMC::Error("Null number of discretization, set it first with \"SetNdisc()\" method.");

    if (is_table_computed_)
      throw AMC::Error("Table seems already computed, do you want to recompute it ? use \"Clear()\" method first.");

#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    int time_index = AMCTimer<CPU>::Add("compute_table_redistribution_composition_" + this->name_);
    AMCTimer<CPU>::Start();
#endif

    // Delta of discretization.
    real delta = real(1) / real(Ndisc_);

    // Number of points in discretization.
    int Npoint = Ndisc_ + 1;

    // Max size of table, we will reduce it after computation.
    unsigned long Npoint_max(1);
    for (int i = 0; i < this->Ndim_; i++)
      Npoint_max *= Npoint;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(3) << Reset() << "Npoint_max = " << Npoint_max << endl;
#endif

    vector<unsigned long> table_point_index(Npoint_max, -1);
    vector1i table_point_polygon_index(Npoint_max, -1);
    vector2i table_point_class_index(Npoint_max);
    table_point_class_coefficient_.resize(Npoint_max);

    // Make one first pass to compute class index and coefficients at each point of discretization.
    vector1i index(this->Ndim_, 0);

    Npoint_ = 0;
    for (unsigned long i = 0; i < Npoint_max; i++)
      {
        // Does the point has a physical meaning ?
        int sum(0);
        for (int j = 0; j < this->Ndim_; j++)
          sum += index[j];

        if (sum < Npoint)
          {
            // Store the hypercube index.
            table_point_index[Npoint_] = i;

            // Fraction.
            vector1r fraction(this->Nlinear_, real(1));
            for (int j = 0; j < this->Ndim_; j++)
              {
                fraction[j] = delta * real(index[j]);
                fraction.back() -= fraction[j];
              }

            if (fraction.back() < AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_ZERO)
              fraction.back() = real(0);

            // Search in which polygon the edge point is located.
            table_point_polygon_index[Npoint_] = ClassRedistributionCompositionBase<D>::find_polygon(fraction);

            if (table_point_polygon_index[Npoint_] == this->Npolygon_)
              throw AMC::Error("Point number " + to_str(i) + ", with index = " + to_str(index)
                               + " and fraction " + to_str(fraction) + " is outside of any polygons.");

            int Nclass_work(0);
            int &polygon_index = table_point_polygon_index[Npoint_];
            vector1i table_point_class_index(this->Nclass_, -1);
            vector2r table_point_class_coefficient(this->Nclass_, vector1r(this->Nlinear_ + 1, 0));

            ClassRedistributionCompositionBase<D>::compute_coefficient(polygon_index, fraction,
                                                                       Nclass_work,
                                                                       table_point_class_index,
                                                                       table_point_class_coefficient);

            int Nclass_polygon = int(polygon_class_[polygon_index].size());
            table_point_class_coefficient_[Npoint_].resize(Nclass_polygon);

            for (int j = 0; j < Nclass_polygon; j++)
              {
                int k;
                for (k = 0; k < Nclass_work; k++)
                  if (table_point_class_index[k] == polygon_class_[polygon_index][j])
                    break;

                if (k < Nclass_work)
                  table_point_class_coefficient_[Npoint_][j] = table_point_class_coefficient[k];
                else
                  table_point_class_coefficient_[Npoint_][j] = vector1r(this->Nlinear_ + 1, real(0));
              }

            Npoint_++;
          }

        // Increment index. This circles on all possible points.
        index[0]++;
        for (int j = 0; j < this->Ndim_ - 1; j++)
          if (index[j] == Npoint)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    // Resize table index and shrink to fit as it may be large.
    table_point_index.resize(Npoint_);
    table_point_class_index.resize(Npoint_);
    table_point_polygon_index.resize(Npoint_);
    table_point_class_coefficient_.resize(Npoint_);

#if __cplusplus > 199711L
    table_point_index.shrink_to_fit();
    table_point_class_index.shrink_to_fit();
    table_point_polygon_index.shrink_to_fit();
    table_point_class_coefficient_.shrink_to_fit();
#endif
#endif

    // Max size of hypercube index, we will reduce it after computation.
    unsigned long Ncube_max = 1;
    for (int i = 0; i < this->Ndim_; i++)
      Ncube_max *= Ndisc_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(3) << Reset() << "Ncube_max = " << Ncube_max << endl;
#endif

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    table_cube_index_.resize(Ncube_max);
#endif
    table_cube_point_index_.resize(Ncube_max);
    table_cube_polygon_index_.resize(Ncube_max);
    table_cube_edge_number_.assign(Ncube_max, 0);
    table_cube_edge_index_.resize(Ncube_max);

    // Make one second pass to compute point index fo each hypercube.
    int Ncube_valid(0);
    unsigned long stat_cube_one_polygon(0);
    unsigned long stat_cube_multiple_polygon(0);

    index = vector1i(this->Ndim_, 0);

    Ncube_ = 0;
    for (unsigned long i = 0; i < Ncube_max; i++)
      {
        // Does the hypercube has a physical meaning ?
        // If "left down" corner is strictly inside composition space, retain the hypercube. 
        int sum(0);
        for (int j = 0; j < this->Ndim_; j++)
          sum += index[j];

        if (sum < Ndisc_)
          {
            Ncube_valid++;

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
            // Store the hypercube index.
            table_cube_index_[Ncube_] = i;
#endif
            // Retrieve the point index of hypercube from latest loop.
            int &Nedge = table_cube_edge_number_[Ncube_];
            table_cube_edge_index_[Ncube_].resize(Nedge_);
            table_cube_point_index_[Ncube_].resize(Nedge_);

            for (int j = 0; j < Nedge_; j++)
              {
                int sum2(0);
                vector1i index2(index);
                for (int k = 0; k < this->Ndim_; k++)
                  {
                    index2[k] += increment_[j][k];
                    sum2 += index2[k];
                  }

                // Does point has a physical meaning ?
                if (sum2 < Npoint)
                  {
                    table_cube_edge_index_[Ncube_][Nedge] = j;

                    // Locate point position in table.
                    long unsigned k = index2.back();
                    for (int l = this->Ndim_ - 2; l >= 0; l--)
                      k = index2[l] + k * Npoint;

                    table_cube_point_index_[Ncube_][Nedge] = search_index(table_point_index, k, 0, Npoint_ - 1);
                    Nedge++;
                  }
              }

            // Resize table index and shrink to fit as it may be large.
            table_cube_edge_index_[Ncube_].resize(Nedge);
            table_cube_point_index_[Ncube_].resize(Nedge);

#if __cplusplus > 199711L
            table_cube_edge_index_[Ncube_].shrink_to_fit();
            table_cube_point_index_[Ncube_].shrink_to_fit();
#endif

            // Above which polygon the hypercube lies ?
            vector1i count(this->Npolygon_, 0);

            for (int j = 0; j < Nedge; j++)
              {
                unsigned long &k = table_cube_point_index_[Ncube_][j];
                count[table_point_polygon_index[k]]++;
              }

            // Store the polygon indexes of hypercube.
            int Npolygon(0);
            table_cube_polygon_index_[Ncube_].resize(this->Npolygon_);
            for  (int j = 0; j < this->Npolygon_; j++)
              if (count[j] > 0)
                table_cube_polygon_index_[Ncube_][Npolygon++] = j;

            table_cube_polygon_index_[Ncube_].resize(Npolygon);
#if __cplusplus > 199711L
            table_cube_polygon_index_[Ncube_].shrink_to_fit();
#endif

            // A few statistics.
            if (int(table_cube_polygon_index_[Ncube_].size()) == 1)
              stat_cube_one_polygon++;
            else
              stat_cube_multiple_polygon++;

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
            Ncube_++;
#endif
          }

#ifdef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
          Ncube_++;
#endif

        // Increment index. This circles on all possibles hypercubes.
        index[0]++;
        for (int j = 0; j < this->Ndim_ - 1; j++)
          if (index[j] == Ndisc_)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }


    // Resize table index and shrink to fit as it may be large.
#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    table_cube_index_.resize(Ncube_);
    table_cube_polygon_index_.resize(Ncube_);

#if __cplusplus > 199711L
    table_cube_index_.shrink_to_fit();
    table_cube_polygon_index_.shrink_to_fit();
#endif
#endif

#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Computed redistribution table for composition discretization \""
                         << this->name_ << "\" in " << AMCTimer<CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Npoint = " << Npoint_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Info(3) << Reset() << "Ncube = " << Ncube_ << endl; 
    *AMCLogger::GetLog() << Bgreen() << Info(3) << Reset() << "Ncube_valid = " << Ncube_valid << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Free memory remaining = "
                         << fixed << setprecision(3) << util::get_memory_usage() * 100 << "%" << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Number of hypercube included in one polygon = "
                         << stat_cube_one_polygon << " (" << setprecision(3)
                         << real(stat_cube_one_polygon) / real(Ncube_valid) * real(100) << "%)." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Number of hypercube crossing several polygons = "
                         << stat_cube_multiple_polygon << " (" << setprecision(3)
                         << real(stat_cube_multiple_polygon) / real(Ncube_valid) * real(100) << "%)." << endl;
#endif

    is_table_computed_ = true;
  }


  template<class D>
  void ClassRedistributionCompositionTable<D>::Clear()
  {
    Npoint_ = 0;
    Ncube_ = 0;

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    table_cube_index_.clear();
#endif
    table_cube_point_index_.clear();
    table_cube_polygon_index_.clear();
    table_point_class_coefficient_.clear();
    table_cube_edge_number_.clear();
    table_cube_edge_index_.clear();

    is_table_computed_ = false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info(3) << Reset() << "Cleared redistribution composition table \""
                         << this->name_ << "\"." << endl;
#endif
  }


  template<class D>
  void ClassRedistributionCompositionTable<D>::Read()
  {
    check_file(path_);

    ifstream fin(path_.c_str(), ifstream::binary);
    fin.read(reinterpret_cast<char*>(&Ndisc_), sizeof(int));
    fin.read(reinterpret_cast<char*>(&Ncube_), sizeof(unsigned long));

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    table_cube_index_.resize(Ncube_);
    fin.read(reinterpret_cast<char*>(table_cube_index_.data()), Ncube_ * sizeof(unsigned long));
#endif

    table_cube_edge_number_.resize(Ncube_);
    fin.read(reinterpret_cast<char*>(table_cube_edge_number_.data()), Ncube_ * sizeof(int));

    table_cube_polygon_index_.resize(Ncube_);
    table_cube_edge_index_.resize(Ncube_);
    table_cube_point_index_.resize(Ncube_);

    for (int i = 0; i < Ncube_; i++)
      {
        int Npolygon;
        fin.read(reinterpret_cast<char*>(&Npolygon), sizeof(int));

        table_cube_polygon_index_[i].resize(Npolygon);
        fin.read(reinterpret_cast<char*>(table_cube_polygon_index_[i].data()), Npolygon * sizeof(int));

        int Nedge;
        fin.read(reinterpret_cast<char*>(&Nedge), sizeof(int));

        table_cube_edge_index_[i].resize(Nedge);
        table_cube_point_index_[i].resize(Nedge);
        fin.read(reinterpret_cast<char*>(table_cube_edge_index_[i].data()), Nedge * sizeof(int));
        fin.read(reinterpret_cast<char*>(table_cube_point_index_[i].data()), Nedge * sizeof(unsigned long));
      }

    fin.read(reinterpret_cast<char*>(&Npoint_), sizeof(unsigned long));
    table_point_class_coefficient_.resize(Npoint_);

    for (int i = 0; i < Npoint_; i++)
      {
        int Nclass;
        fin.read(reinterpret_cast<char*>(&Nclass), sizeof(int));

        table_point_class_coefficient_[i].assign(Nclass, vector1r(this->Nlinear_ + 1, 0));
        for (int j = 0; j < Nclass; j++)
          fin.read(reinterpret_cast<char*>(table_point_class_coefficient_[i][j].data()),
                   (this->Nlinear_ + 1) * sizeof(real));
      }

    fin.close();

    is_table_computed_ = true;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Info(3) << "Read redistribution composition table \""
                         << this->name_ << "\" from file \"" << path_ << "\"." << endl;
#endif
  }


  template<class D>
  void ClassRedistributionCompositionTable<D>::Write() const
  {
    if (! is_table_computed_)
      throw AMC::Error("Table not yet computed.");

    ofstream fout(path_.c_str(), ofstream::binary);
    fout.write(reinterpret_cast<const char*>(&Ndisc_), sizeof(int));
    fout.write(reinterpret_cast<const char*>(&Ncube_), sizeof(unsigned long));
#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    fout.write(reinterpret_cast<const char*>(table_cube_index_.data()), Ncube_ * sizeof(unsigned long));
#endif
    fout.write(reinterpret_cast<const char*>(table_cube_edge_number_.data()), Ncube_ * sizeof(int));

    for (int i = 0; i < Ncube_; i++)
      {
        int Npolygon = int(table_cube_polygon_index_[i].size());
        fout.write(reinterpret_cast<const char*>(&Npolygon), sizeof(int));
        fout.write(reinterpret_cast<const char*>(table_cube_polygon_index_[i].data()), Npolygon * sizeof(int));

        int Nedge = int(table_cube_edge_index_[i].size());
        fout.write(reinterpret_cast<const char*>(&Nedge), sizeof(int));
        fout.write(reinterpret_cast<const char*>(table_cube_edge_index_[i].data()), Nedge * sizeof(int));
        fout.write(reinterpret_cast<const char*>(table_cube_point_index_[i].data()), Nedge * sizeof(unsigned long));
      }

    fout.write(reinterpret_cast<const char*>(&Npoint_), sizeof(unsigned long));
    for (int i = 0; i < Npoint_; i++)
      {
        int Nclass = int(table_point_class_coefficient_[i].size());
        fout.write(reinterpret_cast<const char*>(&Nclass), sizeof(int));

        for (int j = 0; j < Nclass; j++)
          fout.write(reinterpret_cast<const char*>(table_point_class_coefficient_[i][j].data()),
                     (this->Nlinear_ + 1) * sizeof(real));
      }

    fout.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Info(3) << "Written redistribution composition table \""
                         << this->name_ << "\" to file \"" << path_ << "\"." << endl;
#endif
  }


  // Redistribute number and mass over composition classes, does not modify size sections.
  template<class D>
  void ClassRedistributionCompositionTable<D>::Redistribute(const vector1r &fraction,
                                                            vector1b &class_work,
                                                            vector2r &concentration) const
  {
    // If table not computed, falls back to parent method.
    //if (! is_table_computed_)
    //  return this->Redistribute(concentration_root);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "fraction = " << fraction << endl;
#endif

    // Locate hypercube corresponding to given fraction.
    vector1i index(this->Ndim_, 0);
    for (int i = 0; i < this->Ndim_; i++)
      index[i] = int(fraction[i] * real(Ndisc_));

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "index = " << index << endl;
#endif

    // Locate hypercube position in table.
    unsigned long cube_position = index.back();
    for (int i = this->Ndim_ - 2; i >= 0; i--)
      cube_position = index[i] + cube_position * Ndisc_;

#ifndef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
    int cube_index = search_index(table_cube_index_, cube_position, 0,
                                  cube_position < Ncube_ ? cube_position : Ncube_ - 1);

    // Another way to do it, but sensibly slower.
    //int cube_index(0);
    //while (table_cube_index_[cube_index] < cube_position)
    //  cube_index++;
#else
    long unsigned int &cube_index = cube_position;
#endif

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "cube_index = " << cube_index << endl;
#endif

    // If hypercube span on more than one polygon, we need to compute coefficients.
    if (int(table_cube_polygon_index_[cube_index].size()) > 1)
      {
        // Find the good polygon, from those given by table.
        int polygon_index = ClassRedistributionCompositionBase<D>::find_polygon(table_cube_polygon_index_[cube_index], fraction);

        // Count number of class over which we redistribute.
        int Nclass_work(0);

        // Compute coefficients.
        vector1i class_index(this->Nclass_, -1);
        vector2r class_coefficient(this->Nclass_, vector1r(this->Nlinear_ + 1, 0));
        ClassRedistributionCompositionBase<D>::compute_coefficient(polygon_index, fraction, Nclass_work,
                                                                   class_index, class_coefficient);

        // Redistribute concentration on concentration work arrays.
        class_work.assign(this->Nclass_, false);
        ClassRedistributionCompositionBase<D>::apply_coefficient(Nclass_work, fraction, class_index,
                                                                 class_coefficient, class_work, concentration);
      }
    else
      {
        // Polygon index.
        const int &polygon_index = table_cube_polygon_index_[cube_index][0];

        // We do not compute coefficient, hence Nclass_work is not set, so we do it here.
        int Nclass_work = int(polygon_class_[polygon_index].size());

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "Nclass_work = " << Nclass_work << endl;
#endif

        // Compute class coefficients by making linear interpolation between those of hypercube edges.
        vector2r class_coefficient(Nclass_work, vector1r(this->Nlinear_ + 1, 0));

        // Compute fraction position inside hypercube.
        vector1r position(this->Ndim_);
        for (int i = 0; i < this->Ndim_; i++)
          position[i] = fraction[i] * real(Ndisc_) - index[i];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "position = " << position << endl;
#endif

        // Edges coefficients.
        const int &Nedge = table_cube_edge_number_[cube_index];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "Nedge = " << Nedge << endl;
#endif

        vector1r edge_coefficient(Nedge, real(1));

        if (Nedge == Nedge_)
          for (int i = 0; i < Nedge_; i++)
            for (int j = 0; j < this->Ndim_; j++)
              edge_coefficient[i] *= real(increment_[i][j]) * position[j]
                + real(1 - increment_[i][j]) * (real(1) - position[j]);
        else
          for (int i = 1; i < Nedge; i++)
            {
              edge_coefficient[i] = real(0);

              const int &k = table_cube_edge_index_[cube_index][i];
              for (int j = 0; j < this->Ndim_; j++)
                edge_coefficient[i] += real(increment_[k][j]) * position[j];
              edge_coefficient[0] -= edge_coefficient[i];
            }

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "edge_coefficient = " << edge_coefficient << endl;
        CBUG << "table_cube_point_index = " << table_cube_point_index_[cube_index] << endl;
        for (int i = 0; i < Nedge; i++)
          CBUG << "table_point_class_coefficient = "
               << table_point_class_coefficient_[table_cube_point_index_[cube_index][i]] << endl;
#endif

        for (int i = 0; i < Nedge; i++)
          {
            const unsigned long &j = table_cube_point_index_[cube_index][i];

            for (int k = 0; k < Nclass_work; k++)
              for (int l = 0; l <= this->Nlinear_; l++)
                class_coefficient[k][l] += edge_coefficient[i] * table_point_class_coefficient_[j][k][l]; 
          }

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
        CBUG << "class_index = " << polygon_class_[polygon_index] << endl;
        CBUG << "class_coefficient = " << class_coefficient << endl;
#endif

        class_work.assign(this->Nclass_, false);
        ClassRedistributionCompositionBase<D>::apply_coefficient(Nclass_work, fraction, polygon_class_[polygon_index],
                                                                 class_coefficient, class_work, concentration);
      }
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_TABLE_CXX
#endif
