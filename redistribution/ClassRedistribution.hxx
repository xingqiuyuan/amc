// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_HXX

namespace AMC
{
  /*! 
   * \class ClassRedistribution
   */
  template<class S, class R, class D>
  class ClassRedistribution :
    public ClassRedistributionClass<R, D>,
#ifdef AMC_WITH_LAYER
    protected ClassRedistributionLayer
#else
    protected ClassAerosolData
#endif
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector3i vector3i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
    typedef typename AMC::vector3r vector3r;

    typedef S redistribution_size_type;

  private:

    /*!< List of size redistribution schemes.*/
    static redistribution_size_type* redistribution_size_;

    /*!< Maximum number of sections over which redistribute.*/
    static int Nsection_max_;

    /*!< Redistribution working arrays.*/
    static vector1r concentration_aer_number_work_;
    static vector1r concentration_aer_mass_work_;
    static vector1r concentration_aer_mass_total_work_;
#ifdef AMC_WITH_LAYER
    /*!< Working array for layer repartition.*/
    static Array<real, 2> layer_repartition_work_;
#endif

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear data.*/
    static void Clear();

    /*!< Redistribute number and mass over size sections and class compositions.*/
    static void Redistribute(const int section_min,
                             const int section_max,
                             real *concentration_aer_number,
                             real *concentration_aer_mass);

#ifdef AMC_WITH_TEST
    /*!< Test method.*/
    static void Test(const ClassModalDistribution<real> &modal_distribution,
                     int N = 1000, real tol = 1.e-8, bool check = true);
#endif
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_HXX
#endif
