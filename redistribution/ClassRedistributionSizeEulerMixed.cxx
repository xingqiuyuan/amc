// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_MIXED_CXX

#include "ClassRedistributionSizeEulerMixed.hxx"

namespace AMC
{
  // Type.
  int ClassRedistributionSizeEulerMixed::Type()
  {
    return AMC_REDISTRIBUTION_SIZE_TYPE_EULER_MIXED;
  }


  // Compute redistribution coefficients.
  void ClassRedistributionSizeEulerMixed::compute_coefficient(const real &mass_moving,
                                                              const real &mass_moving_log,
                                                              const real &ratio_fraction,
                                                              int &section_redistribute,
                                                              vector1r &coefficient_number,
                                                              vector1r &coefficient_mass) const
  {
    // Size redistribute section in which is located the moving mass.
    section_redistribute = search_index(ClassRedistributionSizeBase::mass_bound_log_, mass_moving_log);

    // Proxy.
    const int n = ClassRedistributionSizeBase::Nredistribute_[section_redistribute];

    // Last element.
    coefficient_number[n - 1] = real(1);

    // The first real section where to redistribute.
    const int i = ClassRedistributionSizeBase::redistribute_index_[section_redistribute];

    if (n > 1)
      {
        vector1r gaussian_cumulative_distribution(n + 1);
        for (int j = 0; j < n + 1; ++j)
          gaussian_cumulative_distribution[j] = erf(ClassDiscretizationSize::mass_bound_log_[i + j] - mass_moving_log);

        real gaussian_cumulative_distribution_width = gaussian_cumulative_distribution.back()
          - gaussian_cumulative_distribution.front();

        for (int j = 0; j < n - 1; ++j)
          {
            coefficient_number[j] = (gaussian_cumulative_distribution[j + 1] - gaussian_cumulative_distribution[j])
              / gaussian_cumulative_distribution_width;
            coefficient_number[n - 1] -= coefficient_number[j];
          }
      }

    for (int j = 0; j < n; ++j)
      {
        real ratio_mass = mass_moving / ClassDiscretizationSize::mass_mean_[i + j];
        real ratio_number = pow(ratio_mass, ratio_fraction);
        ratio_mass = ratio_number / ratio_mass;
        coefficient_mass[j] = coefficient_number[j] * ratio_mass;
        coefficient_number[j] *= ratio_number;
      }
  }
  

  // Constructors.
  ClassRedistributionSizeEulerMixed::ClassRedistributionSizeEulerMixed(Ops::Ops &ops, int Ng)
    : ClassRedistributionSizeBase(ops, Ng, "EulerMixed")
  {
    return;
  }


  // Destructor.
  ClassRedistributionSizeEulerMixed::~ClassRedistributionSizeEulerMixed()
  {
    return;
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_EULER_MIXED_CXX
#endif
