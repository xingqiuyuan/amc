// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_HXX

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
#define AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_SUM 1.e-12
#endif

namespace AMC
{
  /*! 
   * \class ClassRedistributionComposition
   */
  template<class R, class D>
  class ClassRedistributionComposition : public R
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef R redistribution_composition_type;
    typedef D discretization_composition_type;

  private:

    /*!< Total number of species : proxy from ClassSpecies.*/
    int Nspecies_total_;

    /*!< Total number of class compositions, included intermediate ones, plus one for top level.*/
    int Nclass_total_;

    /*!< Class concentration index.*/
    int index_;

    /*!< Internal mixing index, only if internal mixing, proxy from discretization.*/
    int index_internal_mixing_;

    /*!< Root composition redistribution.*/
    ClassRedistributionComposition<R, D>* root_;

    /*!< Nested composition discretization of which depend redistribution.*/
    ClassDiscretizationComposition<D>* discretization_composition_nested_;

    /*!< Vector of composition redistribution.*/
    vector<ClassRedistributionComposition<R, D>* > nested_;

  public:

    /*!< Constructors.*/
    ClassRedistributionComposition(ClassRedistributionComposition<R, D>* root,
                                   ClassDiscretizationComposition<D>* discretization_composition);

    ClassRedistributionComposition(ClassRedistributionComposition<R, D>* root,
                                   ClassDiscretizationComposition<D>* discretization_composition,
                                   Ops::Ops &ops);

    /*!< Get method.*/
    int GetNclassTotal() const;
    ClassDiscretizationComposition<D>* GetDiscretizationCompositionNestedPtr() const;

    /*!< Destructor.*/
    ~ClassRedistributionComposition();

    /*!< Redistribute mass over composition classes, does not modify size sections.*/
    void Redistribute(int &Nclass, vector1i &index_internal_mixing, vector1i& index_class, vector2r &concentration) const;

#ifdef AMC_WITH_TEST
    /*!< Test redistribution.*/
    static void Test(ClassRedistributionComposition<R, D> &redistribution_composition,
                     int N = 0, real tol = 1.e-8, bool display = false);

#ifdef SWIG
    %apply int &OUTPUT {int &Nclass};
#endif

    void _TestSelf_(const vector<real> &concentration_aer_mass,
                    int &Nclass, vector<int> &index_internal_mixing,
                    vector<int> &index_class,
                    vector<vector<real> > &concentration_mass_class);

#ifdef SWIG
    %clear int &Nclass;
#endif
#endif

    /*!< Manage tables.*/
    void TableCompute();
    void TableClear();
    void TableRead();
    void TableWrite() const;
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_HXX
#endif
