// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_CLASS_CXX

#include "ClassRedistributionClass.hxx"

namespace AMC
{
  // Init static data.
  template<class R, class D>
  void ClassRedistributionClass<R, D>::Init(Ops::Ops &ops)
  {
    redistribution_composition_list_.resize(ClassDiscretizationClass<D>::discretization_composition_list_.size());

    ops.SetPrefix(ClassConfiguration::GetPrefix() + ".redistribution.composition.");

    for (int i = 0; i < int(redistribution_composition_list_.size()); i++)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset() << "Instantiate composition redistribution scheme"
                             << " for discretization \""
                             << ClassDiscretizationClass<D>::discretization_composition_list_[i]->GetName()
                             << "\"." << endl;
#endif
        if (ClassDiscretizationClass<D>::discretization_composition_list_[i]->IsInternalMixing())
          redistribution_composition_list_[i] =
            new redistribution_composition_type(NULL, ClassDiscretizationClass<D>::
                                                discretization_composition_list_[i]);
        else
          redistribution_composition_list_[i] =
            new redistribution_composition_type(NULL, ClassDiscretizationClass<D>::
                                                discretization_composition_list_[i], ops);
      }

    // Allocate to each size.
    redistribution_composition_.resize(ClassDiscretizationSize::GetNsection());
    for (int i = 0; i < ClassDiscretizationSize::GetNsection(); i++)
      for (int j = 0; j < int(redistribution_composition_list_.size()); j++)
        if (ClassDiscretizationClass<D>::discretization_composition_[i] ==
            ClassDiscretizationClass<D>::discretization_composition_list_[j])
          {
            redistribution_composition_[i] = redistribution_composition_list_[j];
            break;
          }

    // Set maximum number of redistribution classes.
    Nclass_max_ = 0;
    for (int i = 0; i < ClassDiscretizationSize::GetNsection(); i++)
      if (redistribution_composition_[i]->GetNclassTotal() > Nclass_max_)
        Nclass_max_ = redistribution_composition_[i]->GetNclassTotal();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << Reset() << "Nclass_max = " << Nclass_max_ << endl;
#endif
  }


  // Get methods.
  template<class R, class D>
  ClassRedistributionComposition<R, D>* ClassRedistributionClass<R, D>::
  GetRedistributionCompositionListPtr(const int &i)
  {
    return redistribution_composition_list_[i];
  }


  template<class R, class D>
  ClassRedistributionComposition<R, D>* ClassRedistributionClass<R, D>::
  GetRedistributionCompositionPtr(const int &i)
  {
    return redistribution_composition_[i];
  }


  // Clear data.
  template<class R, class D>
  void ClassRedistributionClass<R, D>::Clear()
  {
    Nclass_max_ = 0;
    redistribution_composition_.clear();
    redistribution_composition_list_.clear();
  }


  // Manage table, if any.
  template<class R, class D>
  void ClassRedistributionClass<R, D>::TableCompute()
  {
    for (int i = 0; i < int(redistribution_composition_list_.size()); i++)
      redistribution_composition_list_[i]->TableCompute();
  }


  template<class R, class D>
  void ClassRedistributionClass<R, D>::TableClear()
  {
    for (int i = 0; i < int(redistribution_composition_list_.size()); i++)
      redistribution_composition_list_[i]->TableClear();
  }


  template<class R, class D>
  void ClassRedistributionClass<R, D>::TableRead()
  {
    for (int i = 0; i < int(redistribution_composition_list_.size()); i++)
      redistribution_composition_list_[i]->TableRead();
  }


  template<class R, class D>
  void ClassRedistributionClass<R, D>::TableWrite()
  {
    for (int i = 0; i < int(redistribution_composition_list_.size()); i++)
      redistribution_composition_list_[i]->TableWrite();
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_CLASS_CXX
#endif
