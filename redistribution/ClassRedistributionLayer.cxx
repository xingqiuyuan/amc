// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_LAYER_CXX

#include "ClassRedistributionLayer.hxx"

namespace AMC
{
  // Compute layer redistribution.
  inline void ClassRedistributionLayer::
  compute_layer_redistribution_coefficient(const int u, const int v, const real &single_mass_u,
                                           Array<real, 2> &layer_redistribution_coefficient_uv)
  {
    vector1r layer_mass_fraction_u = layer_mass_fraction_[u];
    for (int i = 0; i < ClassLayer::Nlayer_[u]; ++i)
      layer_mass_fraction_u[i] *= single_mass_u;

    const vector1r &layer_mass_fraction_v = layer_mass_fraction_[v];

    int i(1), j(1);
    real layer_mass_fraction_min(real(0)), layer_mass_fraction_max(real(0)),
      layer_mass_u(layer_mass_fraction_u[1]);

    layer_redistribution_coefficient_uv = real(0);

    while (i <= ClassLayer::Nlayer_[u] && j <= ClassLayer::Nlayer_[v])
      {
        if (layer_mass_fraction_u[i] < layer_mass_fraction_v[j])
          {
            layer_redistribution_coefficient_uv(i - 1, j - 1) =
              (layer_mass_fraction_u[i] - layer_mass_fraction_min) / layer_mass_u;
            layer_mass_fraction_min = layer_mass_fraction_u[i];

            i++;
            layer_mass_u = layer_mass_fraction_u[i] - layer_mass_u;
          }
        else
          {
            layer_redistribution_coefficient_uv(i - 1, j - 1) =
              (layer_mass_fraction_v[j] - layer_mass_fraction_min) / layer_mass_u;
            layer_mass_fraction_min = layer_mass_fraction_v[j];
            j++;
          }
      }
  }


  // Init static data.
  void ClassRedistributionLayer::Init()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Info(1) << Reset() << "Init layer redistribution." << endl;
#endif

    // Set mass fraction layers.
    layer_mass_fraction_.resize(ClassAerosolData::Ng_);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        layer_mass_fraction_[i] = ClassLayer::layer_radius_fraction_[i];
        // avoid first (=0) and last (=1).
        for (int j = 1; j < ClassLayer::Nlayer_[i]; ++j)
          layer_mass_fraction_[i][j] *= ClassLayer::layer_radius_fraction_[i][j] * ClassLayer::layer_radius_fraction_[i][j];
      }

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "section = " << i << ", Nlayer = "
                           << ClassLayer::Nlayer_[i] << ", layer_mass_fraction = " << layer_mass_fraction_[i] << endl;
#endif

    // Layer redistribution coefficients, oversized.
    layer_redistribution_coefficient_a_.resize(ClassLayer::Nlayer_max_, ClassLayer::Nlayer_max_);
    layer_redistribution_coefficient_b_.resize(ClassLayer::Nlayer_max_, ClassLayer::Nlayer_max_);
  }


  // Clear data.
  void ClassRedistributionLayer::Clear()
  {
    layer_mass_fraction_.clear();
    layer_redistribution_coefficient_a_.free();
    layer_redistribution_coefficient_b_.free();
  }


  // Redistribute layers.
  void ClassRedistributionLayer::Redistribute(const int a, const int b,
                                              const int pos_a, const int pos_b,
                                              const real &concentration_number_a,
                                              const real &concentration_number_b,
                                              const real &concentration_mass_total_a,
                                              const real &concentration_mass_total_b,
                                              const real *composition_mass_a,
                                              const real *concentration_mass_a,
                                              const real *concentration_mass_b,
                                              const vector<vector<real> > &layer_repartition_a,
                                              Array<real, 2> &layer_repartition_b)
  {
    // Ratio of total number and mass concentrations.
    const real concentration_number_c = concentration_number_a + concentration_number_b;
    const real ratio_concentration_number_a = concentration_number_a / concentration_number_c;
    const real ratio_concentration_number_b = concentration_number_b / concentration_number_c;

    const real concentration_mass_total_c = concentration_mass_total_a + concentration_mass_total_b;
    const real ratio_concentration_mass_total_a = concentration_mass_total_a / concentration_mass_total_c;
    const real ratio_concentration_mass_total_b = concentration_mass_total_b / concentration_mass_total_c;

    // Ratio of single mass, there is always something in general section a.
    const real ratio_single_mass_a = ratio_concentration_mass_total_a / ratio_concentration_number_a;

    // Compute layer redistribution coefficient of general section a in c (=b).
    compute_layer_redistribution_coefficient(a, b, ratio_single_mass_a, layer_redistribution_coefficient_a_);

    // General section b may be empty at first pass, in this case we avoid it.
    bool avoid_b(true);

    if (ratio_concentration_number_b > real(0))
      {
        const real ratio_single_mass_b = ratio_concentration_mass_total_b / ratio_concentration_number_b;

        // Compute layer redistribution coefficient of general section b in c (=b).
        compute_layer_redistribution_coefficient(b, b, ratio_single_mass_b, layer_redistribution_coefficient_b_);

        avoid_b = false;
      }

    // Loop on species which are effectively layered.
    for (int h = 0; h < ClassLayer::Nspecies_layer_; ++h)
      {
        const int i = ClassLayer::species_index_layer_[h];
        const int ipos_a(pos_a + i);
        const int ipos_b(pos_b + i);

        vector1r layer_repartition_c(ClassLayer::Nlayer_max_, real(0));

        for (int j = 0; j < ClassLayer::Nlayer_[b]; ++j)
          {
            real sum_a(real(0));
            for (int k = 0; k < ClassLayer::Nlayer_[a]; ++k)
              sum_a += layer_repartition_a[ipos_a][k] * layer_redistribution_coefficient_a_(k, j);

            if (avoid_b)
              layer_repartition_c[j] = sum_a;
            else
              {
                const real concentration_mass_species_a =
                  (composition_mass_a != NULL) ? composition_mass_a[i] * concentration_mass_total_a : concentration_mass_a[i];
                const real &concentration_mass_species_b = concentration_mass_b[i];

                real sum_b(real(0));
                for (int k = 0; k < ClassLayer::Nlayer_[b]; ++k)
                  sum_b += layer_repartition_b(ipos_b, k) * layer_redistribution_coefficient_b_(k, j);

                layer_repartition_c[j] = (concentration_mass_species_a * sum_a +
                                          concentration_mass_species_b * sum_b)
                  / (concentration_mass_species_a + concentration_mass_species_b);
              }
          }

        // Give back layer repartition.
        for (int j = 0; j < ClassLayer::Nlayer_[b]; ++j)
          layer_repartition_b(ipos_b, j) = layer_repartition_c[j];
      }
  }

#ifdef AMC_WITH_TEST
  // Test method.
  void ClassRedistributionLayer::Test(const int N)
  {
#ifdef AMC_WITH_TIMER
    int time_index = AMCTimer<CPU>::Add("test_redistribution_layer");
    AMCTimer<CPU>::Start();
#endif

#ifdef AMC_WITH_TIMER
        AMCTimer<CPU>::Stop(time_index);
#endif
  }
#endif
}

#define AMC_FILE_CLASS_REDISTRIBUTION_LAYER_CXX
#endif
