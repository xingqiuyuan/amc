// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_SIZE_TABLE_HXX

#define AMC_REDISTRIBUTION_SIZE_TABLE_NUMBER_DISCRETIZATION_SIZE_DEFAULT 1000
#define AMC_REDISTRIBUTION_SIZE_TABLE_NUMBER_DISCRETIZATION_FRACTION_DEFAULT 100
#define AMC_REDISTRIBUTION_SIZE_TABLE_ROUNDOFF_EPSILON 1.e-6

namespace AMC
{
  /*! 
   * \class ClassRedistributionSizeTable
   */
  template<class M>
  class ClassRedistributionSizeTable : public M
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef M redistribution_method_type;

  private:

    /*!< Is table computed.*/
    bool is_table_computed_;

    /*!< Number of discretization for size sections and mass fraction.*/
    vector1i Ndisc_;

    /*!< Length of table, may be large.*/
    unsigned long Ntable_;

    /*!< Path of table file.*/
    string path_;

    /*!< Table discretization step.*/
    vector1r delta_;

    /*!< Inverse of table discretization step.*/
    vector1r delta_inv_;

    /*!< Table of section number over which redistribute.*/
    vector1i table_section_number_;

    /*!< Table of redistribute section index.*/
    vector2i table_section_index_;

    /*!< Table of coefficients with which to redistribute number concentration.*/
    vector2r table_coefficient_number_;

    /*!< Table of coefficients with which to redistribute mass concentration.*/
    vector2r table_coefficient_mass_;

    /*!< Set delta and delta_inv.*/
    void set_delta();

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
    ClassRedistributionSizeTable(Ops::Ops &ops, int Ng = ClassDiscretizationSize::GetNsection());

    /*!< Destructor.*/
    ~ClassRedistributionSizeTable();

    /*!< Get methods.*/
    unsigned long GetNtable() const;
    void _GetNdisc_(vector<int> &Ndisc) const;
    void _GetTable_(vector<vector<int> > &table_section_index,
                    vector<vector<real> > &table_coefficient_number,
                    vector<vector<real> > &table_coefficient_mass) const;

    /*!< Set methods.*/
    void _SetNdisc_(const vector<int> &Ndisc);

    /*!< Table management.*/
    void Compute();
    void Clear();
    void Read();
    void Write() const;

    /*!< Redistribute one section over existing size sections, does not modify composition.*/
#ifndef SWIG
    void Redistribute(const real &number_total,
                      const real &mass_total,
                      const real &concentration_aer_number,
                      const real &concentration_aer_mass_total,
                      int &Nsection,
                      vector1i &section_index,
                      vector1r &section_number,
                      vector1r &section_mass) const;
#endif
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_SIZE_TABLE_HXX
#endif
