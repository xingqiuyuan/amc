// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_CLASS_HXX

namespace AMC
{
  /*! 
   * \class ClassRedistributionClass
   */
  template<class R, class D>
  class ClassRedistributionClass : protected ClassDiscretizationClass<D>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector3i vector3i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
    typedef typename AMC::vector3r vector3r;

    typedef ClassRedistributionComposition<R, D> redistribution_composition_type;

  protected:

    /*!< List of composition redistribution schemes for each size section.*/
    static vector<redistribution_composition_type* > redistribution_composition_list_;

    /*!< Vector of pointers of composition redistribution scheme per size section.*/
    static vector<redistribution_composition_type* > redistribution_composition_;

    /*!< Maximum number of redistribution class among size sections.*/
    static int Nclass_max_;

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);


    /*!< Get methods.*/
    static redistribution_composition_type* GetRedistributionCompositionListPtr(const int &i);
    static redistribution_composition_type* GetRedistributionCompositionPtr(const int &i);


    /*!< Clear data.*/
    static void Clear();


    /*!< Manage table, if any.*/
    static void TableCompute();
    static void TableClear();
    static void TableRead();
    static void TableWrite();
  };
}

#define AMC_FILE_CLASS_REDISTRIBUTION_CLASS_HXX
#endif
