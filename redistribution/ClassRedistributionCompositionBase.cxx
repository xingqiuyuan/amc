// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_BASE_CXX

#include "ClassRedistributionCompositionBase.hxx"

namespace AMC
{
  // Find the polygon in which the fraction is located.
  template<class D>
  inline bool ClassRedistributionCompositionBase<D>::in_polygon(const int &i, const vector1r &fraction) const
  {
    // Computation is made in simple precision as only sign matters.
    int j(0);
    for (int k = 0; k < Nlinear_; k++)
      {
        real_polygon sum(real_polygon(0));
        for (int l = 0; l < Ndim_; l++)
          sum += fraction[l] * polygon_coefficient_[i][j++];
        sum += polygon_coefficient_[i][j++];

        if (polygon_sign_[i] && (sum < real_polygon(0)))
          return false;

        if ((! polygon_sign_[i]) && sum > real_polygon(0))
          return false;
      }

    // If all test passed.
    return true;
  }


  template<class D>
  inline int ClassRedistributionCompositionBase<D>::find_polygon(const vector1r &fraction) const
  {
    for (int i = 0; i < Npolygon_; i++)
      if (in_polygon(i, fraction))
        return i;
    return Npolygon_;
  }


  template<class D>
  inline int ClassRedistributionCompositionBase<D>::find_polygon(const vector1i &polygon_index, const vector1r &fraction) const
  {
    for (int i = 0; i < int(polygon_index.size()); i++)
      if (in_polygon(polygon_index[i], fraction))
        return polygon_index[i];

    // If polygon not found in those given in arguments.
    return find_polygon(fraction);
  }


  // Apply redistribution coefficients.
  template<class D>
  void ClassRedistributionCompositionBase<D>::apply_coefficient(const int &Nclass_work,
                                                                const vector1r &fraction,
                                                                const vector1i &class_index,
                                                                const vector2r &class_coefficient,
                                                                vector1b &class_work, vector2r &concentration) const
  {
    vector1r &concentration_root = concentration[index_root_];

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    if (abs(compute_vector_sum(concentration_root, -1) - real(1))
        > AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_SUM)
      {
        ostringstream sout;
        sout << "Composition \"concentration_root\" " << concentration_root << " not equal to unity : "
             << compute_vector_sum(concentration_root, -1);
        throw AMC::Error(sout.str());
      }

    CBUG << "Nclass_work = " << Nclass_work << endl;
#endif

    // If only one class, then the redistribution is somewhat obvious ...
    // In fact, class_coefficient[0] == fraction.
    if (Nclass_work == 1)
      {
        // The class index.
        const int &j = class_index[0];

        // Say that we redistribute in this class.
        class_work[j] = true;

        // Set composition and total mass concentration, at end of vector.
        concentration[index_class_[j]] = concentration_root;
      }
    else
      for (int i = 0; i < Nclass_work; i++)
        {
          // The class index.
          const int &j = class_index[i];

          // Say that we redistribute in this class.
          class_work[j] = true;

          // Reference to concentration over which redistribute.
          vector1r &concentration_work = concentration[index_class_[j]];

          // Initialize work vector.
          concentration_work = concentration_root;

          // Set the total mass concentration, at end of vector.
          concentration_work.back() *= class_coefficient[i].back();

          // Then set composition.
          for (int k = 0; k < Nlinear_; k++)
            if (fraction[k] > real(0))
              {
                // Neither fraction is 0 nor class_coefficient[i].back(),
                // thanks to (b[i] > real(0)) test in compute_coefficient.
                real ratio = class_coefficient[i][k] / (fraction[k] * class_coefficient[i].back());

                for (int l = 0; l < Nspecies_[k]; l++)
                  {
                    int s = species_index_[k][l];
                    concentration_work[s] *= ratio;
                  }
              }
            else
              for (int l = 0; l < Nspecies_[k]; l++)
                {
                  int s = species_index_[k][l];
                  concentration_work[s] = real(0);
                }

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
          if (abs(compute_vector_sum(concentration_work, -1) - real(1))
              > AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_SUM)
            {
              ostringstream sout;
              sout << "Composition \"concentration_work\" " << concentration_work << " not equal to unity : "
                   << compute_vector_sum(concentration_work, -1);
              throw AMC::Error(sout.str());
            }
#endif
        }
  }


  // Compute redistribution coefficients from given fraction on class compositions.
  template<class D>
  void ClassRedistributionCompositionBase<D>::compute_coefficient(const int &polygon_index,
                                                                  const vector1r &fraction,
                                                                  int &Nclass_work,
                                                                  vector1i &class_index,
                                                                  vector2r &class_coefficient) const
  {
    // If no polygon found, redistribute all in the class composition fraction.
    if (polygon_index == Npolygon_)
      {
        // Class composition of polygon point.
        bool on_edge(false);
        int i = discretization_composition_->FindClassCompositionIndexRaw(fraction, on_edge);

        // The redistribution fraction is just that of the particles.
        for (int j = 0; j < Nlinear_; j++)
          class_coefficient[Nclass_work][j] = fraction[j];

        // Everything goes in one class.
        class_coefficient[Nclass_work].back() = real(1);

        // Store the class index.
        class_index[Nclass_work++] = i;
      }
    else
      {
        // Coefficients to compute, second member of linear system.
        vector1r b(Nlinear_, real(0));

        // Test if fraction is very close to one polygon point.
        // In this case, do not solve any system for it might
        // be ill conditioned. Loop on polygon points.
        int i(-1);

        for (int j = 0; j < Nlinear_; j++)
          {
            // Index of polygon point.
            const int k = polygon_[polygon_index][j];

            real sum(0);
            for (int l = 0; l < Ndim_; l++)
              sum += abs(fraction[l] - point_[k][l]);

            if (sum < AMC_REDISTRIBUTION_COMPOSITION_FRACTION_NEAR_POINT_THRESHOLD)
              {
                i = j;
                break;
              }
          }

        // If fraction is not close from any polygon points, solve linear system.
        if (i < 0)
          {
            // Alias to the matrix and determinant of linear system.
            const vector1r &A = polygon_matrix_[polygon_index];
            const real &deter_inv = polygon_determinant_inv_[polygon_index];

            // Reference to last point of polygon considered.
            const int k_last = polygon_[polygon_index][Ndim_];

            for (int l = 0; l < Ndim_; ++l)
              b[l] = fraction[l] - point_[k_last][l];

            // Solves linear system directly.
            // Polygons should always cross composition class.
            // Otherwise solve this linear system is useless.
            if (Ndim_ == 1)
              {
                b[0] *= deter_inv;
                b[1] = real(1) - b[0];
              }
            else if (Ndim_ == 2)
              {
                const real btmp0 = (b[0] * A[3] - b[1] * A[2]) * deter_inv;
                const real btmp1 = (b[1] * A[0] - b[0] * A[1]) * deter_inv;
                b[0] = btmp0;
                b[1] = btmp1;
                b[2] = real(1) - btmp0 - btmp1;
              }
            else if (Ndim_ == 3)
              {
                const real btmp0 = (b[0] * (A[4] * A[8] - A[5] * A[7])
                                    - b[1] * (A[3] * A[8] - A[5] * A[6])
                                    + b[2] * (A[3] * A[7] - A[4] * A[6])) * deter_inv;

                const real btmp1 = (b[0] * (A[1] * A[8] - A[2] * A[7])
                                    - b[1] * (A[0] * A[8] - A[2] * A[6])
                                    + b[2] * (A[0] * A[7] - A[1] * A[6])) * deter_inv;

                const real btmp2 = (b[0] * (A[1] * A[5] - A[2] * A[4])
                                    - b[1] * (A[0] * A[5] - A[2] * A[3])
                                    + b[2] * (A[0] * A[4] - A[1] * A[3])) * deter_inv;

                b[0] = btmp0;
                b[1] = btmp1;
                b[2] = btmp2;
                b[3] = real(1) - btmp0 - btmp1 - btmp2;
              }

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
            if (abs(compute_vector_sum(b) - real(1)) > AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_SUM)
              throw AMC::Error("Sum of vector b = " + to_str(b) + " is not unity : " + to_str(compute_vector_sum(b)));
#endif
          }
        else
          {
            // If fraction very close from one point, attribute all mass to it.
            // Note that this may create slight differences with tabulated redistribution.
            b[i] = real(1);
          }

        // Fill coefficients.
        vector1i class_index_reverse(Nclass_, -1);

        // Sum concentration redistributed on polygon points
        // over the class to which they belong, empty points
        // are ignored.
        for (i = 0; i < Nlinear_; ++i)
          if (b[i] > real(0))
            {
              // Polygon point.
              int j = polygon_[polygon_index][i];

              // Class composition of polygon point.
              int k = polygon_class_[polygon_index][i];

              if (class_index_reverse[k] < 0)
                {
                  class_index[Nclass_work] = k;
                  class_index_reverse[k] = Nclass_work++;
                }

              // Change of class index from true one to reverse.
              k = class_index_reverse[k];

              // Average of fractions between points of same class.
              for (int l = 0; l < Nlinear_; ++l)
                class_coefficient[k][l] += b[i] * point_[j][l];

              // Sum of point contribution to same class.
              class_coefficient[k].back() += b[i];
            }
      }
  }


  // Type.
  template<class D>
  int ClassRedistributionCompositionBase<D>::Type()
  {
    return AMC_REDISTRIBUTION_COMPOSITION_TYPE_BASE;
  }


  // Constructors.
  template<class D>
  ClassRedistributionCompositionBase<D>::ClassRedistributionCompositionBase(D* discretization_composition)
    : discretization_composition_(discretization_composition), Ndim_(0), Ndim2_(0), Nclass_(0),
      Npoint_(0), Npolygon_(0), Nlinear_(0), Nlinear2_(0), internal_mixing_(true),
      id_(id_counter_++), name_(""), index_root_(-1)
  {
    name_ = discretization_composition_->name_;
    Nclass_ = discretization_composition_->Nclass_;

    return;
  }


  template<class D>
  ClassRedistributionCompositionBase<D>::ClassRedistributionCompositionBase(D* discretization_composition, Ops::Ops &ops)
    : discretization_composition_(discretization_composition), Ndim_(0), Ndim2_(0), Nclass_(0),
      Npoint_(0), Npolygon_(0), Nlinear_(0), Nlinear2_(0), internal_mixing_(false),
      id_(id_counter_++), name_(""), index_root_(-1)
  {
    // Proxy of discretization composition members, to speed up computation.
    name_ = discretization_composition_->name_;
    Ndim_ = discretization_composition_->Ndim_;
    Nclass_ = discretization_composition_->Nclass_;
    Nlinear_ = discretization_composition_->Nlinear_;
    Nspecies_ = discretization_composition_->Nspecies_;
    species_index_ = discretization_composition_->species_index_;

    Ndim2_ = Ndim_ * Ndim_;
    Nlinear2_ = Nlinear_ * Nlinear_;

    // Set up prefix.
    string prefix_orig = ops.GetPrefix();

    string discretization_name = name_;
    string::size_type pos = discretization_name.find_last_of(':');

    if (pos != string::npos)
      discretization_name = discretization_name.substr(pos + 1);

    // Read points over which redistribute.
    ops.SetPrefix(prefix_orig + discretization_name + ".point.");

    vector1s point_name = ops.GetEntryList();
    Npoint_ = int(point_name.size());
    point_.resize(Npoint_);

    for (int i = 0; i < Npoint_; i++)
      {
        ops.Set(point_name[i], "", point_[i]);

        // Set last dimension so that sum = 1.
        point_[i].push_back(real(1));
        for (int j = 0; j < Ndim_; j++)
          point_[i].back() -= point_[i][j];

        // Avoid roundoff errors with near zero values.
        if (point_[i].back() < AMC_REDISTRIBUTION_COMPOSITION_FRACTION_THRESHOLD_ZERO)
          point_[i].back() = real(0);
      }

    // In which class composition are points ?
    vector1i point_class(Npoint_, 0);
    for (int i = 0; i < Npoint_; i++)
      {
        bool on_edge;
        point_class[i] = discretization_composition_->FindClassCompositionIndexRaw(point_[i], on_edge);

        // If point is on edge, look if it is on inside domain edges, which we do not want
        // because of threshold effects and the fact that points at class composition edges are useless.
        if (on_edge)
          {
            for (int j = 0; j < Nlinear_; j++)
              {
                on_edge = (point_[i][j] == real(1)) || (point_[i][j] == real(0));
                if (on_edge) break;
              }

            // on_edge know means on the inneer edges as
            // we have filtered out the bounding domain edges.
            if (! on_edge)
              {
                ostringstream sout;
                sout << "Point " << point_[i] << " seems to be on one inside domain edge"
                     << " of class composition " << point_class[i] << endl;
                throw AMC::Error(sout.str());
              }
          }

        if (point_class[i] == discretization_composition_->Nclass_)
          throw AMC::Error("Out of range class index (" + to_str(point_class[i]) + ") for point " + to_str(i) + ".");
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Number of redistribution points = " << Npoint_ << endl;
    for (int i = 0; i < Npoint_; i++)
      *AMCLogger::GetLog() << Debug(3) << "\tPoint (" << i << ") " << point_[i]
                           << " is in class composition \""
                           << discretization_composition_->class_name_[point_class[i]] << "\" ("
                           << point_class[i] << ")" << endl;
#endif

    // Read polygons.
    ops.SetPrefix(prefix_orig + discretization_name + ".");
    vector1i polygon;
    ops.Set("polygon", "", polygon);

    if (int(polygon.size()) % Nlinear_ > 0)
      throw AMC::Error("Redistribution polygons of class \"" + discretization_name
                       + "\" have " + to_str(polygon.size()) + " points, which is not divisible by "
                       + to_str(Nlinear_) + " .");

    Npolygon_ = int(polygon.size()) / Nlinear_;

    int i(0);
    for (int j = 0; j < Npolygon_; j++)
      {
        vector1i vtmp(Nlinear_);
        for (int k = 0; k < Nlinear_; k++)
          vtmp[k] = polygon[i++];

        polygon_.push_back(vtmp);
      }

    // Determine sign of each polygon area and its coefficients.
    polygon_sign_.resize(Npolygon_);
    polygon_coefficient_.resize(Npolygon_);

    for (int i = 0; i < Npolygon_; i++)
      {
        vector<real_polygon> mat(Nlinear2_, real_polygon(1));

        int j(0);
        for (int k = 0; k < Nlinear_; k++)
          {
            for (int l = 0; l < Ndim_; l++)
              mat[j++] = real_polygon(point_[polygon_[i][k]][l]);
            j++;
          }

        real_polygon polygon_area = compute_matrix_determinant<real_polygon>(mat, Nlinear_);
        polygon_sign_[i] = polygon_area >= real_polygon(0) ? true : false;
        polygon_coefficient_[i].resize(Nlinear2_);

        int r(0);
        for (int k = 0; k < Nlinear_; k++)
          for (int l = 0; l < Nlinear_; l++)
            {
              vector<real_polygon> mat2(Ndim_ * Ndim_);

              int p(0), q(0);
              for (int m = 0; m < k; m++)
                {
                  for (int n = 0; n < l; n++)
                    mat2[p++] = mat[q++];
                  q++;
                  for (int n = l + 1; n < Nlinear_; n++)
                    mat2[p++] = mat[q++];
                }

              // Jump one row.
              q += Nlinear_;

              for (int m = k + 1; m < Nlinear_; m++)
                {
                  for (int n = 0; n < l; n++)
                    mat2[p++] = mat[q++];
                  q++;
                  for (int n = l + 1; n < Nlinear_; n++)
                    mat2[p++] = mat[q++];
                }

              polygon_coefficient_[i][r++] =
                real_polygon((k + l) % 2 == 0 ? 1 : -1) *
                compute_matrix_determinant<real_polygon>(mat2, Ndim_);
            }
      }

    // Class indexes of polygon points.
    polygon_class_.resize(Npolygon_);
    polygon_in_class_.resize(Npolygon_);

    for (int i = 0; i < Npolygon_; i++)
      {
        polygon_class_[i].resize(Nlinear_);
        for (int j = 0; j < Nlinear_; j++)
          polygon_class_[i][j] = point_class[polygon_[i][j]];
      }

    // Is polygon in one class ?
    polygon_in_class_.resize(Npolygon_);

    for (int i = 0; i < Npolygon_; i++)
      {
        polygon_in_class_[i] = true;
        for (int j = 1; j < Nlinear_; j++)
          if (polygon_class_[i][0] != polygon_class_[i][j])
            {
              polygon_in_class_[i] = false;
              break;
            }
      }

    // Matrix of each polygon.
    polygon_matrix_.resize(Npolygon_);

    for (int h = 0; h < Npolygon_; h++)
      {
        polygon_matrix_[h].resize(Ndim2_);

        const int k_last = polygon_[h][Ndim_];

        int i = -1;
        for (int j = 0; j < Ndim_; ++j)
          {
            const int k = polygon_[h][j];

            for (int l = 0; l < Ndim_; ++l)
              polygon_matrix_[h][++i] = point_[k][l] - point_[k_last][l];
          }
      }

    // Inverse of determinant of each polygon. First let it be the real determinant.
    polygon_determinant_inv_.resize(Npolygon_);
    if (Ndim_ == 1)
      for (int h = 0; h < Npolygon_; h++)
        polygon_determinant_inv_[h] = polygon_matrix_[h][0];
    else if (Ndim_ == 2)
      for (int h = 0; h < Npolygon_; h++)
        polygon_determinant_inv_[h] = polygon_matrix_[h][0] * polygon_matrix_[h][3]
          - polygon_matrix_[h][1] * polygon_matrix_[h][2];
    else if (Ndim_ == 3)
      for (int h = 0; h < Npolygon_; h++)
        polygon_determinant_inv_[h] =
          polygon_matrix_[h][0] * (polygon_matrix_[h][4] * polygon_matrix_[h][8]
                                   - polygon_matrix_[h][6] * polygon_matrix_[h][7])
          - polygon_matrix_[h][3] * (polygon_matrix_[h][1] * polygon_matrix_[h][8]
                                     - polygon_matrix_[h][2] * polygon_matrix_[h][7])
          + polygon_matrix_[h][6] * (polygon_matrix_[h][1] * polygon_matrix_[h][5]
                                     - polygon_matrix_[h][2] * polygon_matrix_[h][4]);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset()
                         << "Number of redistribution polygons = " << Npolygon_ << endl;
    for (int i = 0; i < Npolygon_; i++)
      *AMCLogger::GetLog() << Debug(3) 
                           << "Polygon number " << i << " " << polygon_[i] << endl
                           << "\tclass(es) = " << polygon_class_[i]
                           << " " << polygon_in_class_[i] << endl
                           << "\t sign = " << polygon_sign_[i] << endl
                           << "\t coef = " << polygon_coefficient_[i] << endl
                           << "\t matrix = " << polygon_matrix_[i] << endl
                           << "\t deter = " << polygon_determinant_inv_[i] << endl;
#endif

    // Turn determinant to inverse.
    for (int h = 0; h < Npolygon_; h++)
      if (abs(polygon_determinant_inv_[h]) > real(0))
        polygon_determinant_inv_[h] = real(1) / polygon_determinant_inv_[h];
      else
        throw AMC::Error("Determinant of matrix of polygon number " + to_str(h) + " is null.");

    // Revert to original prefix.
    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  template<class D>
  ClassRedistributionCompositionBase<D>::~ClassRedistributionCompositionBase()
  {
    return;
  }


  /*!< Get methods.*/
  template<class D>
  D* ClassRedistributionCompositionBase<D>::GetDiscretizationCompositionPtr() const
  {
    return discretization_composition_;
  }


  template<class D>
  int ClassRedistributionCompositionBase<D>::GetNpolygon() const
  {
    return Npolygon_;
  }


  template<class D>
  void ClassRedistributionCompositionBase<D>::CollectPolygonPointList(const int &i, vector<int> &point_index) const
  {
    point_index = polygon_[i];
  }


  template<class D>
  void ClassRedistributionCompositionBase<D>::CollectPointCoordinate(const int &i, vector<real> &coordinate) const
  {
    coordinate = point_[i];
  }


  // Redistribute number and mass over composition classes, does not modify size sections.
  template<class D>
  void ClassRedistributionCompositionBase<D>::Redistribute(const vector1r &fraction,
                                                           vector1b &class_work,
                                                           vector2r &concentration) const
  {
#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "fraction = " << fraction << endl;
#endif

    // Polygon index.
    int polygon_index = find_polygon(fraction);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "polygon_index = " << polygon_index << endl;
#endif

    // Count number of class over which we redistribute.
    int Nclass_work(0);

    // Compute coefficients.
    vector1i class_index(Nclass_, -1);
    vector2r class_coefficient(Nclass_, vector1r(Nlinear_ + 1, 0));
    compute_coefficient(polygon_index, fraction, Nclass_work, class_index, class_coefficient);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "class_index = " << class_index << endl;
    CBUG << "class_coefficient = " << class_coefficient << endl;
#endif

    // Redistribute into concentration work array.
    class_work.assign(Nclass_, false);
    apply_coefficient(Nclass_work, fraction, class_index, class_coefficient, class_work, concentration);

#ifdef AMC_WITH_DEBUG_REDISTRIBUTION_COMPOSITION
    CBUG << "class_work = " << class_work << endl;
#endif
  }


  // Fake methods for compatibility.
  template<class D>
  void ClassRedistributionCompositionBase<D>::Compute()
  {
  }


  template<class D>
  void ClassRedistributionCompositionBase<D>::Clear()
  {
  }


  template<class D>
  void ClassRedistributionCompositionBase<D>::Read()
  {
  }


  template<class D>
  void ClassRedistributionCompositionBase<D>::Write() const
  {
  }
}

#define AMC_FILE_CLASS_REDISTRIBUTION_COMPOSITION_BASE_CXX
#endif
