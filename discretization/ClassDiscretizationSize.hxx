// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_SIZE_HXX

namespace AMC
{
  /*! 
   * \class ClassDiscretizationSize
   */
  class ClassDiscretizationSize
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of size sections.*/
    static int Nsection_;

    /*!< Number of size section bounds.*/
    static int Nbound_;

    /*!< Fixed particle density (µg.µm^{-3}).*/
    static real density_fixed_;

    /*!< Section diameter bounds (µm).*/
    static vector1r diameter_bound_;

    /*!< Section mass bounds (µg).*/
    static vector1r mass_bound_;

    /*!< Section mass widths (µg).*/
    static vector1r mass_width_;

    /*!< Section diameter geometric mean (µm).*/
    static vector1r diameter_mean_;

    /*!< Section surface mean (µm2).*/
    static vector1r surface_mean_;

    /*!< Section mass geometric mean (µg).*/
    static vector1r mass_mean_;

    /*!< Section mass bound neperian logarithms (adim).*/
    static vector1r mass_bound_log_;

    /*!< Section mass mean neperian logarithms (adim).*/
    static vector1r mass_mean_log_;

    /*!< Section mass width neperian logarithms (adim).*/
    static vector1r mass_width_log_;

  public:

    /*!< Init data.*/
    static void Init(Ops::Ops &ops);

     /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNsection();
    static int GetNbound();
#ifndef SWIG
    static vector<real> GetDiameterBound();
    static vector<real> GetDiameterMean();
#endif
    static void GetDiameterBound(vector<real> &diameter_bound);
    static void GetDiameterBoundLog(vector<real> &diameter_bound_log);
    static void GetDiameterWidthLog(vector<real> &diameter_width_log);
    static void GetDiameterBoundLog10(vector<real> &diameter_bound_log10);
    static void GetDiameterWidthLog10(vector<real> &diameter_width_log10);
    static void GetDiameterMean(vector<real> &diameter_mean);
    static void GetMassMean(vector<real> &mass_mean);
    static void GetMassBound(vector<real> &mass_bound);
    static real GetDensityFixed();

    /*!< Search section.*/
    static int SearchSectionIndex(const real &diameter);
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_SIZE_HXX
#endif
