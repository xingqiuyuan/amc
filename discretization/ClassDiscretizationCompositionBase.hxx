// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_BASE_HXX

#define AMC_DISCRETIZATION_COMPOSITION_TYPE_BASE  1
#define AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE 2
#define AMC_DISCRETIZATION_COMPOSITION_THRESHOLD_FRACTION_IS_ON_EDGE 1.e-15

namespace AMC
{
  /*!
   * \class ClassDiscretizationCompositionBase
   */
  class ClassDiscretizationCompositionBase
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
    typedef typename AMC::vector3r vector3r;

  protected:

    /*!< The redistribtion class needs several parameters of the discretization.*/
    template<class D> friend class ClassRedistributionCompositionBase;
    template<class D> friend class ClassRedistributionCompositionTable;

    /*!< Discretization composition unique identifier.*/
    static int id_counter_;

    /*!< Discretization composition unique identifier.*/
    int id_;

    /*!< Is this discretization an internal mixing.*/
    bool internal_mixing_;

#ifdef AMC_WITH_TRACE
    /*!< Is this discretization a  trace class.*/
    bool is_trace_;
#endif

    /*!< Number of dimensions.*/
    int Ndim_;

    /*!< Size of linear systems, depending of dimension size.*/
    int Nlinear_;

    /*!< Number of class compositions.*/
    int Nclass_;

    /*!< Number of class trace.*/
    int Ntrace_;

    /*!< Number of points defining class compositions.*/
    int Npoint_;

    /*!< Total number of polygons.*/
    int Npolygon_;

    /*!< Name of discretization.*/
    string name_;

    /*!< Display color, hatch and label.*/
    string color_, hatch_, label_;

    /*!< Vector of class names.*/
    vector<string> class_name_;

    /*!< Number of species defining each dimensions.*/
    vector1i Nspecies_;

    /*!< Vector of species index defining each dimensions.*/
    vector2i species_index_;

    /*!< Coordinates of discretization points.*/
    vector2r point_;

    /*!< Index of points defining each class.*/
    vector2i class_point_index_;

    /*!< Number of points for each class.*/
    vector1i Npoint_class_;

    /*!< Number of surface for each class.*/
    vector1i Nsurface_class_;

    /*!< Area of surfaces for each class for random computation.*/
    vector2r surface_area_random_;

    /*!< Point indexes of surfaces for each class.*/
    vector2i surface_point_index_class_;

    /*!< Polygon point index.*/
    vector2i polygon_;

    /*!< Number of polygons for each class.*/
    vector1i Npolygon_class_;

    /*!< Index of polygons for each class.*/
    vector2i polygon_index_class_;

    /*!< Class index for each polygon.*/
    vector1i polygon_class_index_;

    /*!< Sign of polygon area, true if positive.*/
    vector1b polygon_sign_;

    /*!< Polygon area.*/
    vector<real> polygon_area_;

    /*!< Polygon area for random data.*/
    vector<vector<real> > polygon_area_random_;

    /*!< The polygon coefficients, to determine whether a point is inside.*/
    vector<vector<real> > polygon_coefficient_;

    /*!< Is fraction located in given polygon ? 1 = yes, 0 = on edge, -1 = no.*/
    int in_polygon(const int &i, const vector1r &fraction) const;

    /*!< Find in which composition class is located one particle for tabulation.*/
    void find_class_composition_index_raw_table(const vector1r &fraction, vector1i &class_index, int &Nclass, bool &on_edge) const;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
#ifdef AMC_WITH_TRACE
    ClassDiscretizationCompositionBase(const string &name, const bool is_trace = false);
#else
    ClassDiscretizationCompositionBase(const string &name);
#endif
    ClassDiscretizationCompositionBase(const string &name, Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassDiscretizationCompositionBase();

    /*!< Get methods.*/
    int GetId() const;
    int GetNclass() const;
#ifdef AMC_WITH_TRACE
    int GetNtrace() const;
#endif
    int GetNlinear() const;
    int GetNdim() const;
    int GetNpoint() const;
    int GetNpolygon() const;
    int GetNpolygonClass(const int &i) const;
    int GetNpointClass(const int &i) const;
    string GetName() const;
    string GetClassName(const int &i) const;
    string GetColor() const;
    string GetHatch() const;
    string GetLabel() const;

    void GetDimensionSpeciesIndex(const int &i, vector<int> &species_index) const;
    void GetClassPolygonList(const int &i, vector<int> &polygon_index) const;
    void GetPolygonPointList(const int &i, vector<int> &point_index) const;
    void GetPointCoordinate(const int &i, vector<real> &coordinate) const;
    void GetClassPointList(const int &i, vector<int> &point_index) const;

    /*!< Is it an internal mixing ? */
    bool IsInternalMixing() const;

#ifdef AMC_WITH_TRACE
    /*!< Is it a trace class ? */
    bool IsTrace() const;
#endif

    /*!< Find in which class composition is located one particle.*/
    int FindClassCompositionIndexRaw(const vector1r &fraction, bool &on_edge) const;
    int FindClassCompositionIndexRaw(const vector1i &class_index, const vector1r &fraction, bool &on_edge) const;

    /*!< Generate random fractions in a given composition class.*/
    void GenerateRandomFraction(const int &class_index, vector1r &fraction) const;
    void GenerateEdgeFraction(const int &class_index, vector1r &fraction) const;

    /*!< Generate average fractions in a given composition class.*/
    void GenerateAverageFraction(const int &class_index, vector1r &fraction) const;

    /*!< Fake methods for compatibility.*/
    virtual void Compute();
    virtual void Clear();
    virtual void Read();
    virtual void Write() const;
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_BASE_HXX
#endif
