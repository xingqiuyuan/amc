// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_HXX

#define AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_ZERO 1.e-15
#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
#define AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_SUM 1.e-12
#endif

namespace AMC
{
  /*!
   * \class ClassDiscretizationComposition
   */
  template<class D>
  class ClassDiscretizationComposition : public D
  {
  public:
    typedef D discretization_composition_type;

    typedef AMC::real real;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  private:

    /*!< Total number of species : proxy from ClassSpecies.*/
    int Nspecies_total_;

    /*!< Level discretization.*/
    int level_;

    /*!< Total number of classes, including nested ones.*/
    int Nclass_total_;

    /*!< If internal mixing, index of class composition in concentration vector.*/
    int index_;

    /*!< Root composition discretization.*/
    ClassDiscretizationComposition<D>* root_;

    /*!< Nested composition discretizations.*/
    vector<ClassDiscretizationComposition<D>* > nested_;

    /*!< Pointer to internal mixing classes.*/
    vector<ClassDiscretizationComposition<D>* > internal_mixing_ptr_;

    /*!< Species index or root dimension.*/
    vector1i *species_index_root_;

    /*!< Number of species of root dimension.*/
    int *Nspecies_root_;

    /*!< Dimension index or root dimension.*/
    int dimension_index_root_;

    /*!< Number of random fractions.*/
    int Nfraction_random_;

    /*!< Number of species for each random fractions.*/
    vector1i Nspecies_random_;

    /*!< Class path of internal mixing sections.*/
    vector1i class_index_path_random_;

    /*!< Species index for random computation.*/
    vector2i species_index_random_;

  public:

    /*!< Constructors.*/
#ifdef AMC_WITH_TRACE
    ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root,
                                   const string &name, const bool is_trace = false);
#else
    ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root, const string &name);
#endif
    ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root, const string &name, Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassDiscretizationComposition();

    /*!< Get methods.*/
    int GetNclassTotal() const;
    int GetLevel() const;
    int GetIndex() const;
    int GetNfractionRandom() const;
    
    ClassDiscretizationComposition<D>* GetDiscretizationCompositionNested(const int &i) const;
    ClassDiscretizationComposition<D>* GetClassInternalMixing(const int &i) const;

    /*!< Compute fractions from particle composition.*/
#ifndef SWIG
    void ComputeFraction(const vector1r &composition, vector1r &fraction) const;
#endif
    vector<real> ComputeFraction(const vector<real> &composition) const;

#ifdef SWIG
  %apply bool *OUTPUT {bool &on_edge};
#endif

    /*!< Find in which class composition is located one particle.*/
    int FindClassCompositionIndex(const vector1r &composition, bool &on_edge) const;

#ifdef SWIG
    %clear bool &on_edge;
#endif

    /*!< Generate random particle compositions.*/
    void GenerateRandomComposition(const int &class_index,
                                   vector<real> &composition,
                                   bool on_edge = false) const;

    /*!< Generate average composition.*/
    void GenerateAverageComposition(const int &class_index,
                                    vector<real> &composition) const;

    /*!< Manage tables.*/
    void TableCompute();
    void TableClear();
    void TableRead();
    void TableWrite() const;

#ifdef AMC_WITH_TEST
    /*!< Test function.*/
    static void Test(const ClassDiscretizationComposition<D> &discretization_composition, const int &N);
#endif
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_HXX
#endif
