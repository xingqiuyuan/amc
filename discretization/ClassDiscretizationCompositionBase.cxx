// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COMPOSITION_DISCRETIZATION_BASE_CXX

#include "ClassDiscretizationCompositionBase.hxx"

namespace AMC
{
  // Find the polygon in which the fraction is located.
  inline int ClassDiscretizationCompositionBase::in_polygon(const int &i, const vector1r &fraction) const
  {
#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "i = " << i << endl;
    CBUG << "polygon_sign = " << polygon_sign_[i] << endl;
#endif

    // Computation is made in simple precision as only sign matters.
    bool on_edge(false);
    int j(0);
    for (int k = 0; k < Nlinear_; k++)
      {
        real sum(real(0));
        for (int l = 0; l < Ndim_; l++)
          sum += fraction[l] * polygon_coefficient_[i][j++];
        sum += polygon_coefficient_[i][j++];

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
        CBUG << "\tsum = " << sum << endl;
#endif

        bool on_edge_local = abs(sum) < AMC_DISCRETIZATION_COMPOSITION_THRESHOLD_FRACTION_IS_ON_EDGE;
        on_edge = on_edge || on_edge_local;

        if (! on_edge_local)
          {
            if (polygon_sign_[i] && (sum < real(0)))
              return -1;

            if ((! polygon_sign_[i]) && sum > real(0))
              return -1;
          }
      }

    if (on_edge)
      return 0;

    // If all test passed.
    return 1;
  }


  // Find in which composition class is located one particle for tabulation.
  void ClassDiscretizationCompositionBase::find_class_composition_index_raw_table(const vector1r &fraction,
                                                                                  vector1i &class_index,
                                                                                  int &Nclass, bool &on_edge) const
  {
    on_edge = false;
    Nclass = 0;
    class_index.resize(Npolygon_);

    for (int i = 0; i < Npolygon_; i++)
      {
        int ipos = in_polygon(i, fraction);
        if (ipos >= 0)
          {
            on_edge = on_edge || (ipos == 0);
            class_index[Nclass++] = polygon_class_index_[i];
          }
      }

    if (Nclass == 0)
      class_index[Nclass++] = Nclass_;

    class_index.resize(Nclass);
  }


  // Type.
  int ClassDiscretizationCompositionBase::Type()
  {
    return AMC_DISCRETIZATION_COMPOSITION_TYPE_BASE;
  }


  // Constructors.
#ifdef AMC_WITH_TRACE
  ClassDiscretizationCompositionBase::ClassDiscretizationCompositionBase(const string &name, const bool is_trace)
    : Ndim_(0), Nlinear_(0), Nclass_(1), Npoint_(0), Npolygon_(0), internal_mixing_(true),
      name_(name), color_(""), hatch_(""), label_(""), id_(id_counter_++),
      is_trace_(is_trace), Ntrace_(is_trace ? 1 : 0)
#else
  ClassDiscretizationCompositionBase::ClassDiscretizationCompositionBase(const string &name)
    : Ndim_(0), Nlinear_(0), Nclass_(1), Npoint_(0), Npolygon_(0), internal_mixing_(true),
      name_(name), color_(""), hatch_(""), label_(""), id_(id_counter_++)
#endif
  {
    return;
  }


  ClassDiscretizationCompositionBase::ClassDiscretizationCompositionBase(const string &name, Ops::Ops &ops)
    : Ndim_(0), Nlinear_(0), Nclass_(0), Npoint_(0), Npolygon_(0), internal_mixing_(false),
      name_(name), color_(""), hatch_(""), label_(""), id_(id_counter_++)
  {
#ifdef AMC_WITH_TRACE
    is_trace_ = false;
    Ntrace_ = 0;
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Load base composition discretization \"" << name_ << "\"." << endl;
#endif

    string prefix_orig = ops.GetPrefix();
    string prefix_discretization = prefix_orig + name_ + ".";

    // Read list of index species defining dimensions.
    ops.SetPrefix(prefix_discretization + "dimension.");

    vector1s dimension_name = ops.GetEntryList();
    Ndim_ = int(dimension_name.size());

    if (Ndim_ > 3)
      throw AMC::Error("Ndim > 3 is not yet implemented.");

    Nlinear_ = Ndim_ + 1;
    species_index_.resize(Nlinear_);
    Nspecies_.resize(Nlinear_);

    vector1b species_used(ClassSpecies::GetNspecies(), false);

    for (int i = 0; i < Ndim_; i++)
      {
        ops.Set(dimension_name[i], "", species_index_[i]);
        Nspecies_[i] = int(species_index_[i].size());
        for (int j = 0; j < Nspecies_[i]; j++)
          species_used[species_index_[i][j]] = true;
      }

    // Remaining species, there must be. 
    Nspecies_.back() = 0;
    for (int i = 0; i < ClassSpecies::GetNspecies(); i++)
      if (! species_used[i])
        {
          species_index_.back().push_back(i);
          Nspecies_.back()++;
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Debug(2) << name_ << ":Number of dimensions = " << Ndim_ << endl;
    for (int i = 0; i < Ndim_; i++)
      {
        ostringstream sout;
        sout << name_ << ":For dimension " << i << " : number of species = " << Nspecies_[i] << ",";
        for (int j = 0; j < Nspecies_[i]; j++)
          sout << " " << ClassSpecies::GetName(species_index_[i][j]);
        *AMCLogger::GetLog() << Debug(3) << sout.str() << endl;
      }
#endif

    // Read class composition definitions.
    ops.SetPrefix(prefix_discretization);

    // Read point coordinates.
    vector1s point_name = ops.GetEntryList("point");
    Npoint_ = int(point_name.size());
    point_.resize(Npoint_);

    ops.SetPrefix(prefix_discretization + "point.");
    for (int i = 0; i < Npoint_; i++)
      ops.Set(point_name[i], "", point_[i]);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << name_ << ":Number of points = " << Npoint_ << endl;
    for (int i = 0; i < Npoint_; i++)
      *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << name_ << ":\tPoint number " << i << " : " << point_[i] << endl;
#endif

    // Read class definition.
    ops.SetPrefix(prefix_discretization);
    class_name_ = ops.GetEntryList("class");

    Nclass_ = int(class_name_.size());
    Npolygon_class_.assign(Nclass_, 0);
    polygon_index_class_.resize(Nclass_);
    class_point_index_.resize(Nclass_);
    Npoint_class_.resize(Nclass_);
    Nsurface_class_.resize(Nclass_);
    surface_area_random_.resize(Nclass_);
    surface_point_index_class_.resize(Nclass_);

    // Size of some linear systems.
    int Nlinear2 = Nlinear_ * Nlinear_;

    for (int i = 0; i < Nclass_; i++)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << name_ << ":Load composition class \""
                             << class_name_[i] << "\", number " << i << endl;
#endif
        ops.SetPrefix(prefix_discretization + "class.");

        string prefix_class = prefix_discretization + "class." + class_name_[i] + ".";
        ops.SetPrefix(prefix_class);

        vector1i polygon;
        ops.Set("polygon", "", polygon);

        if (int(polygon.size()) % Nlinear_ > 0)
          throw AMC::Error("Polygons of class \"" + class_name_[i] + "\" have " + to_str(polygon.size())
                           + " points, which is not divisible by " + to_str(Nlinear_) + " .");

        Npolygon_class_[i] = int(polygon.size()) / Nlinear_;

        int j = 0;
        for (int k = 0; k < Npolygon_class_[i]; k++)
          {
            vector1i vtmp(Nlinear_);
            for (int l = 0; l < Nlinear_; l++)
              vtmp[l] = polygon[j++];

            polygon_.push_back(vtmp);
            polygon_class_index_.push_back(i);

            polygon_index_class_[i].push_back(Npolygon_);
            Npolygon_++;
          }

        // Save index of points of this class.
        sort(polygon.begin(), polygon.end());
        vector1i::iterator it = unique(polygon.begin(), polygon.end());
        polygon.resize(distance(polygon.begin(), it));

        class_point_index_[i] = polygon;
        Npoint_class_[i] = int(class_point_index_[i].size());

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << name_ << ":Points for class " << i
                             << " : Npoint_class = " << Npoint_class_[i] << ", point = " << class_point_index_[i] << endl;
#endif

        // Compute surface (convex hull) of each class.
        vector1r point_coordinate(Ndim_ * Npoint_class_[i]);
        j = 0;
        for (int k = 0; k < Npoint_class_[i]; k++)
          for (int l = 0; l < Ndim_; l++)
            point_coordinate[j++] = point_[class_point_index_[i][k]][l];

        // Find convex hull.
        vector1i &surface_point_index_class = surface_point_index_class_[i];
        find_polygon_convex_hull(Ndim_, Npoint_class_[i], point_coordinate, surface_point_index_class);

        // Number of surface elements of the convex hull.
        Nsurface_class_[i] = int(surface_point_index_class.size()) / (Ndim_ > 0 ? Ndim_ : 1);

        // Turn into real point index.
        for (int k = 0; k < int(surface_point_index_class.size()); k++)
          surface_point_index_class[k] = class_point_index_[i][surface_point_index_class[k]];

        // Now compute area of each surface.
        surface_area_random_[i].resize(Nsurface_class_[i]);

        vector1r mat(Nlinear2, real(1));

        for (int k = 0; k < Nsurface_class_[i]; k++)
          {
            j = Nlinear_;
            for (int l = 0; l < Ndim_; l++)
              {
                int m = k * Ndim_;
                for (int n = 0; n < Ndim_; n++)
                  mat[j++] = point_[surface_point_index_class[m + l]][n];
                j++;
              }

            surface_area_random_[i][k] = abs(compute_matrix_determinant<real>(mat, Nlinear_));
          }

        real sum(real(0));
        for (int k = 0; k < Nsurface_class_[i]; k++)
          {
            real tmp = surface_area_random_[i][k];
            surface_area_random_[i][k] = sum;
            sum += tmp;
          }

        for (int k = 0; k < Nsurface_class_[i]; k++)
          surface_area_random_[i][k] /= sum;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << name_ << ": For class " << i
                             << ", Nsurface_class = " << Nsurface_class_[i] << endl;
        *AMCLogger::GetLog() << Bmagenta() << Debug(3) << Reset() << name_ << ": For class " << i
                             << ", surface_point_index_class = " << surface_point_index_class << endl;
        *AMCLogger::GetLog() << Bmagenta() << Debug(3) << Reset() << name_ << ": For class " << i
                             << ", surface_area_random = " << surface_area_random_[i] << endl;
#endif
      }

    // Determine sign of each polygon area and its coefficients.
    polygon_sign_.resize(Npolygon_);
    polygon_area_.assign(Npolygon_, real(0));
    polygon_coefficient_.resize(Npolygon_);

    for (int i = 0; i < Npolygon_; i++)
      {
        vector<real> mat(Nlinear2, real(1));

        int j(0);
        for (int k = 0; k < Nlinear_; k++)
          {
            for (int l = 0; l < Ndim_; l++)
              mat[j++] = real(point_[polygon_[i][k]][l]);
            j++;
          }

        polygon_area_[i] = compute_matrix_determinant<real>(mat, Nlinear_);
        polygon_sign_[i] = polygon_area_[i] >= real(0) ? true : false;
        polygon_area_[i] = abs(polygon_area_[i]);

        polygon_coefficient_[i].resize(Nlinear2);

        int r(0);
        for (int k = 0; k < Nlinear_; k++)
          for (int l = 0; l < Nlinear_; l++)
            {
              vector<real> mat2(Ndim_ * Ndim_);

              int p(0), q(0);
              for (int m = 0; m < k; m++)
                {
                  for (int n = 0; n < l; n++)
                    mat2[p++] = mat[q++];
                  q++;
                  for (int n = l + 1; n < Nlinear_; n++)
                    mat2[p++] = mat[q++];
                }

              // Jump one row.
              q += Nlinear_;

              for (int m = k + 1; m < Nlinear_; m++)
                {
                  for (int n = 0; n < l; n++)
                    mat2[p++] = mat[q++];
                  q++;
                  for (int n = l + 1; n < Nlinear_; n++)
                    mat2[p++] = mat[q++];
                }

              polygon_coefficient_[i][r++] =
                real((k + l) % 2 == 0 ? 1 : -1) *
                compute_matrix_determinant<real>(mat2, Ndim_);
            }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << name_ << ":Number of polygons = " << Npolygon_ << endl;
    for (int i = 0; i < Npolygon_; i++)
      *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << name_
                           << ":Polygon number " << i << " " << polygon_[i] << endl
                           << "\tclass = " << polygon_class_index_[i] << endl
                           << "\t area = " << polygon_area_[i] << endl
                           << "\t sign = " << polygon_sign_[i] << endl
                           << "\t coef = " << polygon_coefficient_[i] << endl;
#endif

    // Set data for random computation.
    polygon_area_random_.resize(Nclass_);

    for (int i = 0; i < Nclass_; i++)
      {
        polygon_area_random_[i].resize(Npolygon_class_[i]);

        real sum(real(0));
        for (int j = 0; j < Npolygon_class_[i]; j++)
          {
            polygon_area_random_[i][j] = sum;
            sum += polygon_area_[polygon_index_class_[i][j]];
          }

        for (int j = 0; j < Npolygon_class_[i]; j++)
          polygon_area_random_[i][j] /= sum;
      }

#ifdef AMC_WITH_TRACE
    // Load trace class, if any.
    ops.SetPrefix(prefix_discretization);
    if (ops.Exists("trace"))
      {
        ops.SetPrefix(prefix_discretization + "trace.");
        vector1s trace_name = ops.GetEntryList();

        Ntrace_ = int(trace_name.size());

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << name_ << ":Load trace class." << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug() << Reset() << name_ << ":Ntrace = " << Ntrace_ << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << name_ << ":trace_name = " << trace_name << endl;
#endif

        for (int i = 0; i < Ntrace_; i++)
          class_name_.push_back(trace_name[i]);
      }
#endif

#ifdef AMC_WITH_TRACE
    if (Nclass_ == 1 && Ntrace_ == 0)
      throw AMC::Error("In composition discretization \"" + this->name_ +
                       "\", only one class composition and no trace class, use internal mixing instead.");
#else
    if (Nclass_ == 1)
      throw AMC::Error("In composition discretization \"" + this->name_ +
                       "\", only one class composition, use internal mixing instead.");
#endif

    // Revert to original prefix.
    ops.SetPrefix(prefix_orig);

    return;
  }


  // Destructor.
  ClassDiscretizationCompositionBase::~ClassDiscretizationCompositionBase()
  {
    return;
  }


  // Get methods.
  int ClassDiscretizationCompositionBase::GetId() const
  {
    return id_;
  }

#ifdef AMC_WITH_TRACE
  int ClassDiscretizationCompositionBase::GetNtrace() const
  {
    return Ntrace_;
  }
#endif

  int ClassDiscretizationCompositionBase::GetNclass() const
  {
    return Nclass_;
  }


  int ClassDiscretizationCompositionBase::GetNlinear() const
  {
    return Nlinear_;
  }


  int ClassDiscretizationCompositionBase::GetNdim() const
  {
    return Ndim_;
  }


  int ClassDiscretizationCompositionBase::GetNpoint() const
  {
    return Npoint_;
  }


  int ClassDiscretizationCompositionBase::GetNpolygon() const
  {
    return Npolygon_;
  }


  int ClassDiscretizationCompositionBase::GetNpolygonClass(const int &i) const
  {
    return Npolygon_class_[i];
  }


  int ClassDiscretizationCompositionBase::GetNpointClass(const int &i) const
  {
    return Npoint_class_[i];
  }


  string ClassDiscretizationCompositionBase::GetName() const
  {
    return name_;
  }


  string ClassDiscretizationCompositionBase::GetClassName(const int &i) const
  {
    return class_name_[i];
  }


  string ClassDiscretizationCompositionBase::GetColor() const
  {
    return color_;
  }


  string ClassDiscretizationCompositionBase::GetHatch() const
  {
    return hatch_;
  }


  string ClassDiscretizationCompositionBase::GetLabel() const
  {
    return label_;
  }


  void ClassDiscretizationCompositionBase::GetDimensionSpeciesIndex(const int &i, vector<int> &species_index) const
  {
    species_index = species_index_[i];
  }


  void ClassDiscretizationCompositionBase::GetClassPolygonList(const int &i, vector<int> &polygon_index) const
  {
    polygon_index = polygon_index_class_[i];
  }


  void ClassDiscretizationCompositionBase::GetPolygonPointList(const int &i, vector<int> &point_index) const
  {
    point_index = polygon_[i];
  }


  void ClassDiscretizationCompositionBase::GetPointCoordinate(const int &i, vector<real> &coordinate) const
  {
    coordinate = point_[i];
  }


  void ClassDiscretizationCompositionBase::GetClassPointList(const int &i, vector<int> &point_index) const
  {
    point_index = class_point_index_[i];
  }


  // Is it an internal mixing ?
  bool ClassDiscretizationCompositionBase::IsInternalMixing() const
  {
    return internal_mixing_;
  }


#ifdef AMC_WITH_TRACE
  // Is it a trace class ?
  bool ClassDiscretizationCompositionBase::IsTrace() const
  {
    return is_trace_;
  }
#endif


  // Find in which composition class is located one particle.
  int ClassDiscretizationCompositionBase::FindClassCompositionIndexRaw(const vector1r &fraction, bool &on_edge) const
  {
    for (int i = 0; i < Npolygon_; i++)
      {
        int ipos = in_polygon(i, fraction);
        if (ipos >= 0)
          {
            on_edge = ipos == 0;
            return polygon_class_index_[i];
          }
      }

    // If no class found.
    return Nclass_ == 1 ? 0 : Nclass_;
  }


  int ClassDiscretizationCompositionBase::FindClassCompositionIndexRaw(const vector1i &class_index, const vector1r &fraction,
                                                                       bool &on_edge) const
  {
    for (int i = 0; i < int(class_index.size()); i++)
      {
        const vector1i &polygon_index_class = polygon_index_class_[class_index[i]];
        for (int j = 0; j < Npolygon_class_[class_index[i]]; j++)
          {
            int ipos = in_polygon(polygon_index_class[j], fraction);
            if (ipos >= 0)
              {
                on_edge = ipos == 0;
                return class_index[i];
              }
          }
      }

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "fraction = " << fraction << " not found in class compositions " << class_index << endl;
#endif

    // If not in class compositions given in argument, search on all possible class compositions.
    return FindClassCompositionIndexRaw(fraction, on_edge);
  }


  // Generate random fractions in a given composition class.
  void ClassDiscretizationCompositionBase::GenerateRandomFraction(const int &class_index, vector1r &fraction) const
  {
    int polygon_index(0);

    if (Npolygon_class_[class_index] > 0)
      {
        real u = real(drand48());
        for (polygon_index = 1; polygon_index < Npolygon_class_[class_index]; ++polygon_index)
          if (u < polygon_area_random_[class_index][polygon_index])
            break;

        polygon_index--;
      }

    // Get polygon index wide.
    polygon_index = polygon_index_class_[class_index][polygon_index];

    // Index of polygon points.
    const vector1i &polygon = polygon_[polygon_index];

    // The number of points in polygons should always be equal to Nlinear_.
    vector1r u;
    generate_random_vector(Nlinear_, u);

    fraction.assign(Nlinear_, real(0));
    for (int i = 0; i < Nlinear_; i++)
      for (int j = 0; j < Ndim_; j++)
        fraction[j] += u[i] * point_[polygon[i]][j];

    fraction.back() = real(1);
    for (int i = 0; i < Ndim_; i++)
      fraction.back() -= fraction[i];
  }


  void ClassDiscretizationCompositionBase::GenerateEdgeFraction(const int &class_index, vector1r &fraction) const
  {
    int surface_index(0);

    // If dimension 1, surfaces are points.
    if (Nsurface_class_[class_index] > 0)
      {
        real u = real(drand48());
        for (surface_index = 1; surface_index < Nsurface_class_[class_index]; surface_index++)
          if (u < surface_area_random_[class_index][surface_index])
            break;

        surface_index--;
      }

    // Index of surface points.
    const vector1i &surface_point_index_class = surface_point_index_class_[class_index];

    // The number of points in surfaces should always be equal to Ndim_.
    fraction.assign(Nlinear_, real(0));

    vector1r u;
    generate_random_vector(Ndim_, u);

    // Turn surface index to pointer at right position in surface_point_index_class vector.
    surface_index *= Ndim_;

    for (int i = 0; i < Ndim_; i++)
      for (int j = 0; j < Ndim_; j++)
        fraction[j] += u[i] * point_[surface_point_index_class[surface_index + i]][j];

    // Ensure unity.
    fraction.back() = real(1);
    for (int i = 0; i < Ndim_; i++)
      fraction.back() -= fraction[i];
  }


  // Generate average fractions in a given composition class.
  void ClassDiscretizationCompositionBase::GenerateAverageFraction(const int &class_index, vector1r &fraction) const
  {
    const vector1i &point_index = class_point_index_[class_index];

    fraction.assign(Nlinear_, real(0));

    // Average between coordinate point of class.
    for (int i = 0; i < Ndim_; i++)
      for (int j = 0; j < Npoint_class_[class_index]; j++)
        fraction[i] += point_[point_index[j]][i];

    real Npoint_class_inv = real(1) / real(Npoint_class_[class_index]);
    for (int i = 0; i < Ndim_; i++)
      fraction[i] *= Npoint_class_inv;

    // Ensure unity.
    fraction.back() = real(1);
    for (int i = 0; i < Ndim_; i++)
      fraction.back() -= fraction[i];
  }


  // Fake methods for compatibility.
  void ClassDiscretizationCompositionBase::Compute()
  {
  }


  void ClassDiscretizationCompositionBase::Clear()
  {
  }


  void ClassDiscretizationCompositionBase::Read()
  {
  }


  void ClassDiscretizationCompositionBase::Write() const
  {
  }
}

#define AMC_FILE_CLASS_COMPOSITION_DISCRETIZATION_BASE_CXX
#endif
