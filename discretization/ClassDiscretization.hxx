// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_HXX


namespace AMC
{
  /*! 
   * \class ClassDiscretization
   */
  template<class D>
  class ClassDiscretization :
    protected ClassDiscretizationSize, protected ClassDiscretizationClass<D>
#ifdef AMC_WITH_TRACE
    , protected ClassTrace
#endif
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;
    typedef typename AMC::vector2s vector2s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Number of generalized sections.*/
    static int Ng_;

    /*!< Number of species : proxy from ClassSpecies.*/
    static int Nspecies_;

    /*!< General section, index of size section,
      index of class composition in discretization.*/
    static vector2i general_section_;

    /*!< General section per size section and class composition.*/
    static vector2i general_section_reverse_;

    /*!< Class composition names per size section, if any.*/
    static vector2s class_composition_name_;

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Get methods.*/
    static int GetNg();
    static void GetGeneralSection(vector<vector<int> > &general_section);
    static void GetGeneralSectionReverse(vector<vector<int> > &general_section_reverse);
    static void GetClassCompositionName(vector<vector<string> > &class_composition_name);
#ifndef SWIG
    static vector<vector<string> >& GetClassCompositionName();
#endif

    /*!< Clear method.*/
    static void Clear();

    /*!< Compute discretization diameter.*/
    static void ComputeDiameter(const real *concentration_aer_num,
                                const real *concentration_aer_mass,
                                vector<int> &section_index,
                                vector<real> &diameter);

    /*!< Compute discretization single mass.*/
    static void ComputeSingleMass(const real *concentration_aer_num,
                                  const real *concentration_aer_mass,
                                  vector<int> &section_index,
                                  vector<real> &single_mass);

    /*!< Redistribute concentrations if not at right place.*/
    static void Redistribute(const int section_min,
                             const int section_max,
                             real *concentration_aer_number,
                             real *concentration_aer_mass);
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_HXX
#endif
