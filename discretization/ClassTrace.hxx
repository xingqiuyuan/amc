// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_TRACE_HXX

namespace AMC
{
  /*! 
   * \class ClassTrace
   */
  class ClassTrace
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of trace types.*/
    static int Ntrace_;

    /*!< General section list index for each trace.*/
    static vector2i general_section_index_;

    /*!< Trace index for each general section, negative if not trace.*/
    static vector1i general_section_index_reverse_;

    /*!< class index per section size for each trace.*/
    static vector2i size_class_index_;

    /*!< Name of each trace type.*/
    static vector1s name_;

  public:

#ifndef SWIG
    /*!< Add.*/
    static void Add(const string &name, const int &i, const int &j, const int &k);

    /*!< Clear.*/
    static void Clear();
#endif

    /*!< Get methods.*/
    static int GetNtrace();
    static string GetName(const int &i);
    static void GetGeneralSectionIndex(const int &i, vector<int> &general_section_index);
    static void GetGeneralSectionIndexReverse(vector<int> &general_section_index_reverse);
    static void GetSizeClassIndex(const int &i, vector<int> &size_class_index);
  };
}

#define AMC_FILE_CLASS_TRACE_HXX
#endif
