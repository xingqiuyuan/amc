// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_CLASS_CXX

#include "ClassDiscretizationClass.hxx"

namespace AMC
{
  // Init.
  template<class D>
  void ClassDiscretizationClass<D>::Init(Ops::Ops &ops)
  {
    const string prefix_orig = ops.GetPrefix();

    //
    // Read composition discretization per size section.
    //

    discretization_composition_.assign(ClassDiscretizationSize::GetNsection(),
                                       reinterpret_cast<ClassDiscretizationComposition<D>* >(NULL));

    ops.SetPrefix(prefix_orig + ".discretization.general_section.");
    vector1s discretization_composition_name = ops.GetEntryList();

    discretization_composition_list_.resize(int(discretization_composition_name.size()));
    for (int i = 0; i < int(discretization_composition_name.size()); i++)
      {
        ops.SetPrefix(prefix_orig + ".discretization.composition.");
        if (discretization_composition_name[i] == "internal_mixing")
          discretization_composition_list_[i] =
            new ClassDiscretizationComposition<D>(NULL, discretization_composition_name[i]);
        else
          discretization_composition_list_[i] =
            new ClassDiscretizationComposition<D>(NULL, discretization_composition_name[i], ops);

        ops.SetPrefix(prefix_orig + ".discretization.general_section.");
        vector1i section_size_index;
        ops.Set(discretization_composition_name[i], "", section_size_index);
        if (section_size_index.empty())
          {
            section_size_index.resize(ClassDiscretizationSize::GetNsection());
            for (int j = 0; j < ClassDiscretizationSize::GetNsection(); j++)
              section_size_index[j] = j;
          }

        for (int j = 0; j < int(section_size_index.size()); j++)
          discretization_composition_[section_size_index[j]] = discretization_composition_list_[i];

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "Associating size sections "
                             << section_size_index << " with composition discretization \""
                             << discretization_composition_name[i] << "\"." << endl;
#endif
      }

    // Check if all size sections have been assigned one composition discretization.
    for (int i = 0; i < ClassDiscretizationSize::GetNsection(); i++)
      if (discretization_composition_[i] == NULL)
        throw AMC::Error("Size section number " + to_str(i) + " did not receive any composition discretization.");

    // Get the number of class composition for each size.
    Nclass_max_ = 0;
    Nclass_.resize(ClassDiscretizationSize::GetNsection());
    for (int i = 0; i < ClassDiscretizationSize::GetNsection(); i++)
      {
        Nclass_[i] = discretization_composition_[i]->GetNclassTotal();

        if (Nclass_[i] > Nclass_max_)
          Nclass_max_ = Nclass_[i];
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "Nclass = " << Nclass_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "Nclass_max = " << Nclass_max_ << endl;
#endif
  }


  template<class D>
  int ClassDiscretizationClass<D>::GetNclass(const int i)
  {
    return Nclass_[i];
  }


  template<class D>
  vector<int> ClassDiscretizationClass<D>::GetNclass()
  {
    return Nclass_;
  }


  template<class D>
  ClassDiscretizationComposition<D>* ClassDiscretizationClass<D>::GetDiscretizationCompositionList(const int i)
  {
    return discretization_composition_list_[i];
  }


  template<class D>
  ClassDiscretizationComposition<D>* ClassDiscretizationClass<D>::GetDiscretizationComposition(const int i)
  {
    return discretization_composition_[i];
  }


  // Find class composition index with its name.
  template<class D>
  int ClassDiscretizationClass<D>::FindClassCompositionIndexByName(const int i, const string name,
                                                                   bool &error_occured)
  {
    int count(0), index(-1);

    for (int j = 0; j < Nclass_[i]; ++j)
      if (discretization_composition_[i]->GetClassInternalMixing(j)->GetName().find(name) != string::npos)
        {
          index = j;
          count++;
        }

    if ((error_occured = ((count != 1) || (index < 0)))) index = -1;

    return index;
  }


  // Manage table, if any.
  template<class D>
  void ClassDiscretizationClass<D>::TableCompute()
  {
    for (int i = 0; i < int(discretization_composition_list_.size()); i++)
      discretization_composition_list_[i]->TableCompute();
  }

  template<class D>
  void ClassDiscretizationClass<D>::TableClear()
  {
    for (int i = 0; i < int(discretization_composition_list_.size()); i++)
      discretization_composition_list_[i]->TableClear();
  }

  template<class D>
  void ClassDiscretizationClass<D>::TableRead()
  {
    for (int i = 0; i < int(discretization_composition_list_.size()); i++)
      discretization_composition_list_[i]->TableRead();
  }

  template<class D>
  void ClassDiscretizationClass<D>::TableWrite()
  {
    for (int i = 0; i < int(discretization_composition_list_.size()); i++)
      discretization_composition_list_[i]->TableWrite();
  }


  // Clear.
  template<class D>
  void ClassDiscretizationClass<D>::Clear()
  {
    Nclass_max_ = 0;
    Nclass_.clear();
    discretization_composition_.clear();
    discretization_composition_list_.clear();
  }
}

#define AMC_FILE_CLASS_DISCRETIZATION_CLASS_CXX
#endif
