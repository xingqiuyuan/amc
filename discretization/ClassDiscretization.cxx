// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_CXX

#include "ClassDiscretization.hxx"

namespace AMC
{
  // Init.
  template<class D>
  void ClassDiscretization<D>::Init(Ops::Ops &ops)
  {
    const string prefix_orig = ops.GetPrefix();

    // Read size discretization.
    ClassDiscretizationSize::Init(ops);

    // Read composition discretization per size section.
    ClassDiscretizationClass<D>::Init(ops);

    ops.SetPrefix(prefix_orig);

    // Proxy for number of species.
    if ((Nspecies_ = ClassSpecies::GetNspecies()) <= 0)
      throw AMC::Error("Species subsystem not yet initialized (Nspecies <= 0).");


    //
    // General sections.
    //

    Ng_ = 0;
    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      Ng_ += ClassDiscretizationClass<D>::Nclass_[i];

    general_section_.resize(Ng_);
    general_section_reverse_.resize(ClassDiscretizationSize::Nsection_);

    int k(0);
    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      {
        general_section_reverse_[i].resize(ClassDiscretizationClass<D>::Nclass_[i]);
        for (int j = 0; j < ClassDiscretizationClass<D>::Nclass_[i]; j++)
          {
            general_section_[k].push_back(i);
            general_section_[k].push_back(j);
            general_section_reverse_[i][j] = k++;
          }
      }

#ifdef AMC_WITH_TRACE
    //
    // Trace class.
    //

    ClassTrace::general_section_index_reverse_.assign(Ng_, -1);
    ClassTrace::Ntrace_ = 0;

    k = 0;
    for (int i = 0; i < ClassDiscretizationSize::Nsection_; i++)
      for (int j = 0; j < ClassDiscretizationClass<D>::Nclass_[i]; j++)
        {
          ClassDiscretizationComposition<D>* internal_mixing_ptr =
            ClassDiscretizationClass<D>::discretization_composition_[i]->GetClassInternalMixing(j);

          // j should be equal to internal_mixing_ptr->GetIndex()
          if (internal_mixing_ptr->IsTrace())
            {
              const string trace_name = internal_mixing_ptr->GetName();
              ClassTrace::Add(trace_name.substr(trace_name.find_last_of(':') + 1), i, j, k);
            }

          k++;
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info(0) << Reset()
                         << "Number of trace sections : " << ClassTrace::Ntrace_ << endl;
    for (int i = 0; i < ClassTrace::Ntrace_; ++i)
      {
        *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset()
                             << "\ttrace section " << i << " : " << ClassTrace::name_[i] << endl;
        *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "\t\tgeneral_section_index = "
                             << ClassTrace::general_section_index_[i] << endl;
        *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "\t\tsize_class_index = "
                             << ClassTrace::size_class_index_[i] << endl;
      }

    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "general_section_index_reverse = "
                         << ClassTrace::general_section_index_reverse_ << endl;
#endif
#endif

    //
    // Class composition names.
    //

    class_composition_name_.assign(ClassDiscretizationSize::Nsection_, vector<string>());

    for (int i = 0; i < ClassDiscretizationSize::Nsection_; ++i)
      {
        class_composition_name_[i].assign(ClassDiscretizationClass<D>::Nclass_[i], "");
        for (int j = 0; j < ClassDiscretizationClass<D>::Nclass_[i]; ++j)
          class_composition_name_[i][j] = ClassDiscretizationClass<D>::
            discretization_composition_[i]->GetClassInternalMixing(j)->GetName();
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug() << Reset() << "class_composition_name = " << class_composition_name_ << endl;
#endif
  }


  // Clear method.
  template<class D>
  void ClassDiscretization<D>::Clear()
  {
#ifdef AMC_WITH_TRACE
    ClassTrace::Clear();
#endif
    ClassDiscretizationClass<D>::Clear();
    ClassDiscretizationSize::Clear();

    class_composition_name_.clear();
  }



  // Get methods.
  template<class D>
  int ClassDiscretization<D>::GetNg()
  {
    return Ng_;
  }


  template<class D>
  void ClassDiscretization<D>::GetGeneralSection(vector<vector<int> > &general_section)
  {
    general_section = general_section_;
  }


  template<class D>
  void ClassDiscretization<D>::GetGeneralSectionReverse(vector<vector<int> > &general_section_reverse)
  {
    general_section_reverse = general_section_reverse_;
  }


  template<class D>
  void ClassDiscretization<D>::GetClassCompositionName(vector<vector<string> > &class_composition_name)
  {
    class_composition_name = class_composition_name_;
  }


#ifndef SWIG
  template<class D>
  vector<vector<string> >& ClassDiscretization<D>::GetClassCompositionName()
  {
    return class_composition_name_;
  }
#endif


  // Compute single diameter.
  template<class D>
  void ClassDiscretization<D>::ComputeDiameter(const real *concentration_aer_num,
                                               const real *concentration_aer_mass,
                                               vector<int> &section_index,
                                               vector<real> &diameter)
  {
    vector<real> single_mass;

    ClassDiscretization<D>::ComputeSingleMass(concentration_aer_num,
                                              concentration_aer_mass,
                                              section_index, single_mass);

    diameter.resize(single_mass.size());

    for (int i = 0; i < int(single_mass.size()); i++)
      diameter[i] = pow(AMC_INV_PI6 * single_mass[i] / ClassDiscretizationSize::density_fixed_, AMC_FRAC3);
  }


  // Compute single mass.
  template<class D>
  void ClassDiscretization<D>::ComputeSingleMass(const real *concentration_aer_num,
                                                 const real *concentration_aer_mass,
                                                 vector<int> &section_index,
                                                 vector<real> &single_mass)
  {
    for (int i = 0; i < Ng_; i++)
      {
        real mass_total(real(0));

        int j = i * Nspecies_;
        for (int k = 0; k < Nspecies_; k++)
          mass_total += concentration_aer_mass[j + k];

        if (concentration_aer_num[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
            {
              section_index.push_back(i);
              single_mass.push_back(mass_total / concentration_aer_num[i]);
            }
      }
  }




  // Redistribute concentrations if not at right place.
  template<class D>
  void ClassDiscretization<D>::Redistribute(const int section_min,
                                            const int section_max,
                                            real *concentration_aer_number,
                                            real *concentration_aer_mass)
  {
    for (int i = section_min; i < section_max; ++i)
      {
        real mass_total(real(0));

        const int j = i * Nspecies_;
        for (int k = 0; k < Nspecies_; ++k)
          mass_total += concentration_aer_mass[j + k];

        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          if (mass_total > AMC_MASS_CONCENTRATION_MINIMUM)
            {
              real single_mass = mass_total / concentration_aer_number[i];
              const int section_index = search_index(ClassDiscretizationSize::mass_bound_, single_mass);

              int class_index(0);

#ifdef AMC_WITH_LOGGER
              if (section_index != general_section_[i][0])
                *AMCLogger::GetLog() << Byellow() << Logger::Error() << Reset() << "General section " << i
                                     << " assumed to be in size section " << general_section_[i][0]
                                     << " actually found in size section " << section_index << endl;
#endif

              if (ClassDiscretizationClass<D>::Nclass_[section_index] > 1)
                {
                  vector1r composition(Nspecies_, real(1) / mass_total);
                  for (int k = 0; k < Nspecies_; k++)
                    composition[k] *= concentration_aer_mass[j + k];
                  bool on_edge(false);
                  class_index = ClassDiscretizationClass<D>::discretization_composition_[section_index]->
                    FindClassCompositionIndex(composition, on_edge);
                }

#ifdef AMC_WITH_LOGGER
              if (class_index != general_section_[i][1])
                *AMCLogger::GetLog() << Bmagenta() << Logger::Error() << Reset() << "General section " << i
                                     << " assumed to be in class composition section "
                                     << general_section_[i][1]
                                     << " actually found in class composition " << class_index << endl;
#endif

              // Now raw redistribution.
              if (section_index != general_section_[i][0] ||
                  class_index != general_section_[i][1])
                {
                  const int l = general_section_reverse_[section_index][class_index];
                  concentration_aer_number[l] += concentration_aer_number[i];
                  concentration_aer_number[i] = real(0);

                  const int m = l * Nspecies_;
                  for (int k = 0; k < Nspecies_; k++)
                    {
                      concentration_aer_mass[m + k] += concentration_aer_mass[j + k];
                      concentration_aer_mass[j + k] = real(0);
                    }
                }
            }
      }
  }
}

#define AMC_FILE_CLASS_DISCRETIZATION_CXX
#endif
