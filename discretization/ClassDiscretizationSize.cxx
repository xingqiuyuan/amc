// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_SIZE_CXX

#include "ClassDiscretizationSize.hxx"

namespace AMC
{
  // Init method.
  void ClassDiscretizationSize::Init(Ops::Ops &ops)
  {
    // Fixed particle density in µg.µm^{-3}.
    density_fixed_ = ops.Get<real>("discretization.size.density_fixed_g_cm3", "", AMC_PARTICLE_DENSITY_DEFAULT) * 1.e-6;

    ops.Set("discretization.size.diameter_bound_um", "", diameter_bound_);
    Nbound_ = int(diameter_bound_.size());
    Nsection_ = Nbound_ - 1;

    diameter_mean_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      diameter_mean_[i] = sqrt(diameter_bound_[i] * diameter_bound_[i + 1]);

    // If mean diameters are provided, read them, otherwise stick with just computed ones.
    ops.Set("discretization.size.diameter_mean_um", "", diameter_mean_, diameter_mean_);

    // Compute mass related discretization from read diameters and fixed aerosol density.
    mass_bound_.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      mass_bound_[i] = AMC_PI6 * density_fixed_ * diameter_bound_[i] * diameter_bound_[i] * diameter_bound_[i];

    mass_width_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      mass_width_[i] = mass_bound_[i + 1] - mass_bound_[i];

    mass_mean_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      mass_mean_[i] = AMC_PI6 * density_fixed_ * diameter_mean_[i] * diameter_mean_[i] * diameter_mean_[i];

    mass_bound_log_.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      mass_bound_log_[i] = log(mass_bound_[i]);

    mass_width_log_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      mass_width_log_[i] = mass_bound_log_[i + 1] - mass_bound_log_[i];

    mass_mean_log_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      mass_mean_log_[i] = log(mass_mean_[i]);

    surface_mean_.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      surface_mean_[i] = AMC_PI * diameter_mean_[i] * diameter_mean_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Number of size  sections = " << Nsection_ << endl;
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Number of bound sections = " << Nbound_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Diameter bound sections (µm) = " << diameter_bound_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Fixed density = " << density_fixed_ << " µg.µm^{-3}" << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info() << Reset() << "Mass bound sections (µg) = " << mass_bound_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "Diameter mean sections (µm) = " << diameter_mean_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Surface mean sections (µm^2) = " << surface_mean_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Mass mean sections (µg) = " << mass_mean_ << endl;
#endif
  }


  // Clear method.
  void ClassDiscretizationSize::Clear()
  {
    density_fixed_ = AMC_PARTICLE_DENSITY_DEFAULT;
    Nsection_ = 0;
    Nbound_ = 0;
    diameter_bound_.clear();
    mass_bound_.clear();
    mass_width_.clear();
    diameter_mean_.clear();
    surface_mean_.clear();
    mass_mean_.clear();
    mass_bound_log_.clear();
    mass_mean_log_.clear();
  }


  // Get methods.
  int ClassDiscretizationSize::GetNsection()
  {
    return Nsection_;
  }


  int ClassDiscretizationSize::GetNbound()
  {
    return Nbound_;
  }


  vector<real> ClassDiscretizationSize::GetDiameterBound()
  {
    return diameter_bound_;
  }


  vector<real> ClassDiscretizationSize::GetDiameterMean()
  {
    return diameter_mean_;
  }


  void ClassDiscretizationSize::GetDiameterBound(vector<real> &diameter_bound)
  {
    diameter_bound = diameter_bound_;
  }


  void ClassDiscretizationSize::GetDiameterBoundLog(vector<real> &diameter_bound_log)
  {
    diameter_bound_log.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      diameter_bound_log[i] = log(diameter_bound_[i]);
  }


  void ClassDiscretizationSize::GetDiameterWidthLog(vector<real> &diameter_width_log)
  {
    diameter_width_log.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      diameter_width_log[i] = log(diameter_bound_[i + 1] / diameter_bound_[i]);
  }


  void ClassDiscretizationSize::GetDiameterBoundLog10(vector<real> &diameter_bound_log10)
  {
    diameter_bound_log10.resize(Nbound_);
    for (int i = 0; i < Nbound_; i++)
      diameter_bound_log10[i] = log10(diameter_bound_[i]);
  }


  void ClassDiscretizationSize::GetDiameterWidthLog10(vector<real> &diameter_width_log10)
  {
    diameter_width_log10.resize(Nsection_);
    for (int i = 0; i < Nsection_; i++)
      diameter_width_log10[i] = log10(diameter_bound_[i + 1] / diameter_bound_[i]);
  }


  void ClassDiscretizationSize::GetDiameterMean(vector<real> &diameter_mean)
  {
    diameter_mean = diameter_mean_;
  }


  void ClassDiscretizationSize::GetMassMean(vector<real> &mass_mean)
  {
    mass_mean = mass_mean_;
  }


  void ClassDiscretizationSize::GetMassBound(vector<real> &mass_bound)
  {
    mass_bound = mass_bound_;
  }


  real ClassDiscretizationSize::GetDensityFixed()
  {
    return density_fixed_;
  }


  // Search section.
  int ClassDiscretizationSize::SearchSectionIndex(const real &diameter)
  {
    return search_index(diameter_bound_, diameter, 0, Nsection_);
  }
}

#define AMC_FILE_CLASS_DISCRETIZATION_SIZE_CXX
#endif
