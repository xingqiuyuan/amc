// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_TRACE_CXX

#include "ClassTrace.hxx"

namespace AMC
{
  // Clear method.
  void ClassTrace::Clear()
  {
    Ntrace_ = 0;
    name_.clear();
    general_section_index_reverse_.clear();
    general_section_index_.clear();
    size_class_index_.clear();
  }


  // Add.
  void ClassTrace::Add(const string &name, const int &i, const int &j, const int &k)
  {
    int l(0);
    for (l = 0; l < Ntrace_; l++)
      if (name_[l] == name)
        break;

    // Whether trace class already exists or not.
    if (l == Ntrace_)
      {
        name_.push_back(name);
        general_section_index_.push_back(vector1i());
        size_class_index_.push_back(vector1i(ClassDiscretizationSize::GetNsection(), -1));
        Ntrace_++;
      }

    general_section_index_reverse_[k] = l;
    general_section_index_[l].push_back(k);
    size_class_index_[l][i] = j;
  }


  // Get methods.
  int ClassTrace::GetNtrace()
  {
    return Ntrace_;
  }


  string ClassTrace::GetName(const int &i)
  {
    return name_[i];
  }


  void ClassTrace::GetGeneralSectionIndex(const int &i, vector<int> &general_section_index)
  {
    general_section_index = general_section_index_[i];
  }


  void ClassTrace::GetGeneralSectionIndexReverse(vector<int> &general_section_index_reverse)
  {
    general_section_index_reverse = general_section_index_reverse_;
  }


  void ClassTrace::GetSizeClassIndex(const int &i, vector<int> &size_class_index)
  {
    size_class_index = size_class_index_[i];
  }
}

#define AMC_FILE_CLASS_TRACE_CXX
#endif
