// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_TABLE_CXX

#include "ClassDiscretizationCompositionTable.hxx"

namespace AMC
{
  // Type.
  int ClassDiscretizationCompositionTable::Type()
  {
    return AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE;
  }


  // Constructors.
#ifdef AMC_WITH_TRACE
  ClassDiscretizationCompositionTable::ClassDiscretizationCompositionTable(const string &name, const bool trace)
    : ClassDiscretizationCompositionBase(name, trace), is_table_computed_(false), Ndisc_(0), Ntable_(0), path_("")
#else
  ClassDiscretizationCompositionTable::ClassDiscretizationCompositionTable(const string &name)
    : ClassDiscretizationCompositionBase(name), is_table_computed_(false), Ndisc_(0), Ntable_(0), path_("")
#endif
  {
    return;
  }


  ClassDiscretizationCompositionTable::ClassDiscretizationCompositionTable(const string &name, Ops::Ops &ops)
    : ClassDiscretizationCompositionBase(name, ops), is_table_computed_(false), Ndisc_(0), Ntable_(0), path_("")
  {
    // Lookup table discretization.
    Ndisc_ = ops.Get<int>(this->name_ + ".table.Ndisc");
    path_ = ops.Get<string>(this->name_ + ".table.path", "", path_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Instantiate tabulated discretization composition." << endl;
#endif
#ifdef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << Reset() << "AMC built with fast tabulation indexing flag"
                         << ", this may consume more memory." << endl;
#endif
#endif

    // If real path, read directly in constructor.
    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Ndisc = " << Ndisc_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "path = " << path_ << ", file_size = " << file_size << endl;
#endif

    if (file_size > 0)
      Read();

    return;
  }


  // Destructor.
  ClassDiscretizationCompositionTable::~ClassDiscretizationCompositionTable()
  {
    return;
  }


  // Get methods.
  unsigned long ClassDiscretizationCompositionTable::GetNtable() const
  {
    return Ntable_;
  }


  int ClassDiscretizationCompositionTable::GetNdisc() const
  {
    return Ndisc_;
  }


  void ClassDiscretizationCompositionTable::GetTable(vector<vector<int> > &table) const
  {
    table.resize(Ntable_);
    for (int i = 0; i < Ntable_; i++)
      {
        table[i].resize(int(table_class_[i].size()) + 1);

        int j(0);
#ifdef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
        table[i][j++] = i;
#else
        table[i][j++] = int(table_index_[i]);
#endif
        for (int k = 0; k < int(table_class_[i].size()); k++)
          table[i][j++] = table_class_[i][k];
      }
  }


  // Set methods.
  void ClassDiscretizationCompositionTable::SetNdisc(const int &Ndisc)
  {
    Ndisc_ = Ndisc;
  }


  // Manage the table lookup.
  void ClassDiscretizationCompositionTable::Compute()
  {
    if (Ndisc_ == 0)
      throw AMC::Error("Null number of discretization, set it first with \"SetNdisc()\" method.");

    if (is_table_computed_)
      throw AMC::Error("Table seems already computed, do you want to recompute it ? use \"Clear()\" method first.");

#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    int time_index = AMCTimer<CPU>::Add("compute_table_discretization_" + this->name_);
    AMCTimer<CPU>::Start();
#endif

    // Number of edges of each hypercube.
    int Nvertice(1);
    for (int i = 0; i < this->Ndim_; i++)
      Nvertice *= 2;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Number of discretization = " << Ndisc_ << endl;
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Number of vertices of hypercube = " << Nvertice << endl;
#endif

    // Increment for each hypercube edge from the down left corner.
    vector2i increment(Nvertice, vector1i(this->Ndim_));
    vector1i index(this->Ndim_, 0);

    for (int i = 0; i < Nvertice; i++)
      {
        increment[i] = index;

        index[0]++;
        for (int j = 0; j < this->Ndim_ - 1; j++)
          if (index[j] == 2)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

    // Max size of table, we will reduce it after computation.
    unsigned long Ntable_max = 1;
    for (int i = 0; i < this->Ndim_; i++)
      Ntable_max *= Ndisc_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Max table size = " << Ntable_max << endl;
#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    *AMCLogger::GetLog() << Bred() << Info(1) << Reset() << "Note : the table size will be reduced after computation." << endl;
#endif
#endif

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    table_index_.resize(Ntable_max);
#endif
    table_edge_.assign(Ntable_max, false);
    table_class_.resize(Ntable_max);

    // Compute the class composition index for each valid hypercube.
    real delta = real(1) / real(Ndisc_);
    index = vector1i(this->Ndim_, 0);

    int Ntable_valid(0);
    unsigned long stat_hypercube_one_class(0);
    unsigned long stat_hypercube_multiple_class(0);

    Ntable_ = 0;
    for (unsigned long i = 0; i < Ntable_max; i++)
      {
        // Does the hypercube has a physical meaning ?
        // If "left down" corner is inside composition space, retain the hypercube. 
        int sum(0);
        for (int j = 0; j < this->Ndim_; j++)
          sum += index[j];

        if (sum < Ndisc_)
          {
            Ntable_valid++;

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
            // Store the hypercube index.
            table_index_[Ntable_] = i;
#endif

            // Above which class compositions the hypercube lies ?
            vector1i count(this->Nclass_ + 1, 0);

            for (int j = 0; j < Nvertice; j++)
              {
                vector1r fraction(this->Ndim_);
                for (int k = 0; k < this->Ndim_; k++)
                  fraction[k] = delta * real(index[k] + increment[j][k]);

                // Search in which class the edge point is located and increment the class counter.
                int Nclass;
                bool on_edge;
                vector1i class_index;
                ClassDiscretizationCompositionBase::
                  find_class_composition_index_raw_table(fraction, class_index, Nclass, on_edge);

                for (int k = 0; k < Nclass; k++)
                  count[class_index[k]]++;

                table_edge_[Ntable_] = table_edge_[Ntable_] || on_edge;
              }

            // Normally at least one edge should be in one class composition.
            if (Nvertice == count.back())
              throw AMC::Error("Hypercube number " + to_str(i) + " is outside of any class composition.");

            // Store the class composition indexes of hypercube, from most probable to least one.
            int Nclass(0);
            table_class_[Ntable_].resize(this->Nclass_);
            for  (int j = 0; j < this->Nclass_; j++)
              {
                int max_index(0);
                int max_value(-1);

                for (int k = 0; k < this->Nclass_; k++)
                  if (max_value < count[k])
                    {
                      max_value = count[k];
                      max_index = k;
                    }

                if (count[max_index] > 0)
                  {
                    table_class_[Ntable_][Nclass++] = max_index;
                    count[max_index] = -1;
                  }
              }

            table_class_[Ntable_].resize(Nclass);

            // If hypercube lies one more than one class, make as if it was on one class edge.
            if (Nclass > 1)
              table_edge_[Ntable_] = true;

            // Perform a few statistics to know about table quality.
            if (Nclass > 1)
              stat_hypercube_multiple_class++;
            else
              stat_hypercube_one_class++;
              
#if __cplusplus > 199711L
            table_class_[Ntable_].shrink_to_fit();
#endif
#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
            Ntable_++;
#endif
          }

#ifdef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
          Ntable_++;
#endif

        // Increment index. This circles on all possibles hypercubes.
        index[0]++;
        for (int j = 0; j < this->Ndim_ - 1; j++)
          if (index[j] == Ndisc_)
            {
              index[j] = 0;
              index[j + 1]++;
            }
      }

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    // Resize table index and shrink to fit a it may be large.
    table_index_.resize(Ntable_);
    table_edge_.resize(Ntable_);
    table_class_.resize(Ntable_);

#if __cplusplus > 199711L
    table_index_.shrink_to_fit();
    table_edge_.shrink_to_fit();
    table_class_.shrink_to_fit();
#endif
#endif

#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Computed table for composition discretization \""
                         << this->name_ << "\" in " << AMCTimer<CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Current table size = " << Ntable_<< endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Number of valid hypercubes = " << Ntable_valid << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Number of hypercube included in one class = "
                         << stat_hypercube_one_class << " (" << setprecision(3)
                         << stat_hypercube_one_class / real(Ntable_valid) * real(100)
                         << "%)." << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Number of hypercube crossing several class = "
                         << stat_hypercube_multiple_class << " (" << setprecision(3)
                         << stat_hypercube_multiple_class / real(Ntable_valid) * real(100)
                         << "%)." << endl;
#endif

    is_table_computed_ = true;
  }


  void ClassDiscretizationCompositionTable::Clear()
  {
    Ntable_ = 0;
#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    table_index_.clear();
#endif
    table_class_.clear();
    table_edge_.clear();
    is_table_computed_ = false;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Info(3) << "Cleared discretization composition table \"" << this->name_ << "\"." << endl;
#endif
  }


  void ClassDiscretizationCompositionTable::Read()
  {
    check_file(path_);

    ifstream fin(path_.c_str(), ifstream::binary);
    fin.read(reinterpret_cast<char*>(&Ndisc_), sizeof(int));
    fin.read(reinterpret_cast<char*>(&Ntable_), sizeof(unsigned long));

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    table_index_.resize(Ntable_);
    fin.read(reinterpret_cast<char*>(table_index_.data()), Ntable_ * sizeof(unsigned long));
#endif

    vector1i table_edge_int(Ntable_);
    fin.read(reinterpret_cast<char*>(table_edge_int.data()), Ntable_ * sizeof(int));

    table_edge_.resize(Ntable_);
    for (int i = 0; i < Ntable_; i++)
      table_edge_[i] = table_edge_int[i] == 1;

    table_class_.resize(Ntable_);
    for (int i = 0; i < Ntable_; i++)
      {
        int Nclass(0);
        fin.read(reinterpret_cast<char*>(&Nclass), sizeof(int));
        table_class_[i].resize(Nclass);
        fin.read(reinterpret_cast<char*>(table_class_[i].data()), Nclass * sizeof(int));
      }

    fin.close();

    is_table_computed_ = true;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Read discretization composition table \""
                         << this->name_ << "\" from file \"" << path_ << "\"." << endl;
#endif
  }


  void ClassDiscretizationCompositionTable::Write() const
  {
    if (! is_table_computed_)
      throw AMC::Error("Table not yet computed.");

    ofstream fout(path_.c_str(), ofstream::binary);
    fout.write(reinterpret_cast<const char*>(&Ndisc_), sizeof(int));
    fout.write(reinterpret_cast<const char*>(&Ntable_), sizeof(unsigned long));

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    fout.write(reinterpret_cast<const char*>(table_index_.data()), Ntable_ * sizeof(unsigned long));
#endif

    vector1i table_edge_int(Ntable_);
    for (int i = 0; i < Ntable_; i++)
      table_edge_int[i] = table_edge_[i] ? 1 : 0;

    fout.write(reinterpret_cast<const char*>(table_edge_int.data()), Ntable_ * sizeof(int));

    for (int i = 0; i < Ntable_; i++)
      {
        int Nclass = table_class_[i].size();
        fout.write(reinterpret_cast<char*>(&Nclass), sizeof(int));
        fout.write(reinterpret_cast<const char*>(table_class_[i].data()), Nclass * sizeof(int));
      }
    fout.close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Written discretization composition table \""
                         << this->name_ << "\" to file \"" << path_ << "\"." << endl;
#endif
  }


  // Find in which composition class is located one particle.
  int ClassDiscretizationCompositionTable::FindClassCompositionIndexRaw(const vector<real> &fraction, bool &on_edge) const
  {
    // Locate fraction.
    vector1i index(this->Ndim_, 0);
    for (int i = 0; i < this->Ndim_; i++)
      index[i] = int(fraction[i] * real(Ndisc_));

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "index = " << index << endl;
#endif

    // Locate hypercube position in table.
    long unsigned int i = index.back();
    for (int j = this->Ndim_ - 2; j >= 0; j--)
      i = index[j] + i * Ndisc_;

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "i = " << i << endl;
#endif

    // If hypercube is all contained in one class return it,
    // otherwise search between possible hypercube class.
#ifdef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    long unsigned int &j = i;
#else
    // Return the class composition to which the hypercube belongs.
    int j = search_index(table_index_, i, 0, i < Ntable_ ? i : Ntable_ - 1);
#endif

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "j = " << j << endl;
    CBUG << "table_class = " << table_class_[j] << endl;
#endif

    // Finally return whether we are on one edge or no.
    if (table_edge_[j])
      return ClassDiscretizationCompositionBase::FindClassCompositionIndexRaw(table_class_[j], fraction, on_edge);
    else
      return table_class_[j][0];
  }
}

#define AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_TABLE_CXX
#endif
