// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_TABLE_HXX


namespace AMC
{
  /*!
   * \class ClassDiscretizationCompositionTable
   */
  class ClassDiscretizationCompositionTable : public ClassDiscretizationCompositionBase
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Is table computed.*/
    bool is_table_computed_;

    /*!< Number of fraction sections.*/
    int Ndisc_;

    /*!< Length of table, may be large.*/
    unsigned long Ntable_;

    /*!< Path of table file.*/
    string path_;

#ifndef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
    /*!< Indexes of valid hypercube, those crossing the space composition.*/
    vector<unsigned long> table_index_;
#endif

    /*!< Whether the hypercube is on one class edge.*/
    vector1b table_edge_;

    /*!< Table of class compositions for each valid hypercube.*/
    vector2i table_class_;

  public:

    /*!< Type.*/
    static int Type();

    /*!< Constructors.*/
#ifdef AMC_WITH_TRACE
    ClassDiscretizationCompositionTable(const string &name, const bool is_trace = false);
#else
    ClassDiscretizationCompositionTable(const string &name);
#endif
    ClassDiscretizationCompositionTable(const string &name, Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassDiscretizationCompositionTable();

    /*!< Get methods.*/
    unsigned long GetNtable() const;
    int GetNdisc() const;
    void GetTable(vector<vector<int> > &table) const;

    /*!< Set methods.*/
    void SetNdisc(const int &Ndisc);

    /*!< Manage the table.*/
    void Compute();
    void Clear();
    void Read();
    void Write() const;

    /*!< Find in which composition class is located one particle.*/
    int FindClassCompositionIndexRaw(const vector<real> &fraction, bool &on_edge) const;
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_COMPOSITION_TABLE_HXX
#endif
