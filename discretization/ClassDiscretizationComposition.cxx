// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COMPOSITION_DISCRETIZATION_CXX

#include "ClassDiscretizationComposition.hxx"

namespace AMC
{
  // Constructors.
  template<class D>
#ifdef AMC_WITH_TRACE
  ClassDiscretizationComposition<D>::ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root,
                                                                    const string &name, const bool is_trace)
    : discretization_composition_type(name, is_trace), root_(root), level_(0), index_(0), Nclass_total_(1),
      Nfraction_random_(1), species_index_root_(NULL), Nspecies_root_(NULL), dimension_index_root_(-1),
      Nspecies_total_(0)
#else
  ClassDiscretizationComposition<D>::ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root, const string &name)
    : discretization_composition_type(name), root_(root), level_(0), index_(0), Nclass_total_(1),
      Nfraction_random_(1), species_index_root_(NULL), Nspecies_root_(NULL), dimension_index_root_(-1),
      Nspecies_total_(0)
#endif
  {
    if (ClassSpecies::GetNspecies() == 0)
      throw AMC::Error("No species in model, maybe you forgot to initialize the Species class.");

    Nspecies_total_ = ClassSpecies::GetNspecies();

    // Set concentration index, only for not nested class compositions.
    if (root_ != NULL)
      {
        index_ = root_->index_++;
        root_->internal_mixing_ptr_.push_back(this);
        this->name_ = root_->name_ + ":" + this->name_;

        // Number of level when internal mixing.
        level_ = root_->level_ + 1;
      }
    else
      internal_mixing_ptr_.push_back(this);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << this->name_ << ":level = " << level_ << endl;
#endif

    // Set data necessary to generate random compositions.
    ClassDiscretizationComposition<D>* discretization_composition_ptr_old = this;
    ClassDiscretizationComposition<D>* discretization_composition_ptr = root_;

    class_index_path_random_.resize(level_);

    while (discretization_composition_ptr != NULL)
      {
        for (int i = 0; i < discretization_composition_ptr->Nclass_; i++)
          if (discretization_composition_ptr->nested_[i] == NULL)
            {
              class_index_path_random_[discretization_composition_ptr->level_] = i;
              break;
            }

        Nfraction_random_ += discretization_composition_ptr->Ndim_;

        for (int i = 0; i < discretization_composition_ptr->Nlinear_; i++)
          if (i != discretization_composition_ptr_old->dimension_index_root_)
            {
              Nspecies_random_.push_back(discretization_composition_ptr->Nspecies_[i]);
              species_index_random_.push_back(discretization_composition_ptr->species_index_[i]);
            }

        discretization_composition_ptr_old = discretization_composition_ptr_old->root_;
        discretization_composition_ptr = discretization_composition_ptr->root_;
      }

    if (Nfraction_random_ == 1)
      {
        Nspecies_random_.push_back(Nspecies_total_);
        species_index_random_.push_back(vector1i(Nspecies_total_));
        for (int i = 0; i < Nspecies_total_; i++)
          species_index_random_[0][i] = i;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << this->name_ << ":Data for random composition generation for class \"" << this->name_ << "\"" << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << this->name_ << ":\tClass index path = " << class_index_path_random_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << this->name_ << ":\tNumber of fractions = " << Nfraction_random_ << endl;
    for (int i = 0; i < Nfraction_random_; i++)
      {
        ostringstream sout;
        sout << "\tFor fraction number " << i << ", number of species = " << Nspecies_random_[i] << ",";
        for (int j = 0; j < Nspecies_random_[i]; j++)
          sout << " " << ClassSpecies::GetName(species_index_random_[i][j]);
        *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << this->name_ << ":" << sout.str() << endl;
      }
#endif

    return;
  }


  template<class D>
  ClassDiscretizationComposition<D>::ClassDiscretizationComposition(ClassDiscretizationComposition<D>* root,
                                                                    const string &name, Ops::Ops &ops)
    : discretization_composition_type(name, ops), root_(root), level_(0), index_(0), Nclass_total_(0),
      Nfraction_random_(0), species_index_root_(NULL), Nspecies_root_(NULL), dimension_index_root_(-1),
      Nspecies_total_(0)
  {
    if (ClassSpecies::GetNspecies() == 0)
      throw AMC::Error("No species in model, maybe you forgot to initialize the Species class.");

#ifdef AMC_WITH_TRACE
    if (this->Ntrace_ > 0 && root_ != NULL)
      throw AMC::Error("Trace classes may only appear in top level discretization.");
#endif

    Nspecies_total_ = ClassSpecies::GetNspecies();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << this->name_ << ":Load composition discretization \"" << this->name_ << "\"" << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << this->name_ << ":Composition discretization type is \"" << D::Type() << "\"" << endl;
#endif

    if (root_ != NULL)
      {
        this->name_ = root_->name_ + ":" + this->name_;
        index_ = root_->index_;

        // Increase level when not internal mixing.
        level_ = root_->level_ + 1;

        // Find root species index.
        for (int i = 0; i < root_->Nlinear_; i++)
          {
            int count(0);
            for (int j = 0; j < this->Ndim_; j++)
              if (is_included(this->species_index_[j], root_->species_index_[i]))
                count++;

            if (count == this->Ndim_)
              {
                species_index_root_ = &root_->species_index_[i];
                Nspecies_root_ = &root_->Nspecies_[i];
                dimension_index_root_ = i;
                break;
              }
          }

        if (species_index_root_ == NULL)
          throw AMC::Error("Unable to find root species index for dicretization \"" + this->name_ + "\".");

        // We need then to properly set the complementary of species index.
        this->Nspecies_.back() = 0;
        this->species_index_.back().clear();

        for (int i = 0; i < *Nspecies_root_; i++)
          {
            bool is_included(false);

            for (int j = 0; j < this->Ndim_; j++)
              for (int k = 0; k < this->Nspecies_[j]; k++)
                if ((*species_index_root_)[i] == this->species_index_[j][k])
                  is_included = true;

            if (! is_included)
              {
                this->species_index_.back().push_back((*species_index_root_)[i]);
                this->Nspecies_.back()++;
              }
          }
      }

#ifdef AMC_WITH_LOGGER
    {
      ostringstream sout;
      sout << "For last dimension : number of species = " << this->Nspecies_.back() << ",";
      for (int i = 0; i < this->Nspecies_.back(); i++)
        sout << " " << ClassSpecies::GetName(this->species_index_.back()[i]);
      *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << this->name_ << ":" << sout.str() << endl;
    }
#endif

    int index_old = index_;

    // Eventually read nested discretizations.
#ifdef AMC_WITH_TRACE
    nested_.assign(this->Nclass_ + this->Ntrace_, reinterpret_cast<ClassDiscretizationComposition<D>* >(NULL));
#else
    nested_.assign(this->Nclass_, reinterpret_cast<ClassDiscretizationComposition<D>* >(NULL));
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << this->name_ << ":Number of classes = " << this->Nclass_ << " :" << endl;
#endif

    for (int i = 0; i < this->Nclass_; i++)
      {
        string::size_type pos = this->name_.find_last_of(':');

        string prefix_class = this->name_;
        if (pos != string::npos)
          prefix_class = this->name_.substr(pos + 1);

        prefix_class += ".class." + this->class_name_[i] + ".";

        bool is_nested = ops.Exists(prefix_class + "discretization");
        string discretization_nested = ops.Get<string>(prefix_class + "discretization", "", "");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << this->name_ << ":"
                             << " Class \"" << this->class_name_[i] << "\" is "
                             << (is_nested ? "nested with discretization \"" + discretization_nested + "\"" :
                                 "internally mixed") << endl;
#endif

        if (is_nested)
          nested_[i] = new ClassDiscretizationComposition<D>(this, discretization_nested, ops);
        else
            nested_[i] = new ClassDiscretizationComposition<D>(this, this->class_name_[i]);

        // This is only useful for display.
        const string prefix_display = prefix_class + "display";

        if (ops.Exists(prefix_display))
          {
            nested_[i]->color_ = ops.Get<string>(prefix_display + ".color", "", "");
            nested_[i]->hatch_ = ops.Get<string>(prefix_display + ".hatch", "", "");
            nested_[i]->label_ = ops.Get<string>(prefix_display + ".label", "", nested_[i]->name_);
          }
      }

#ifdef AMC_WITH_TRACE
    for (int h = 0; h < this->Ntrace_; h++)
      {
        const int i = h + this->Nclass_;

        string::size_type pos = this->name_.find_last_of(':');

        string prefix_class = this->name_;
        if (pos != string::npos)
          prefix_class = this->name_.substr(pos + 1);

        prefix_class += ".trace." + this->class_name_[i] + ".";

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << this->name_ << ":"
                             << " Class \"" << this->class_name_[i] << "\" is a trace with prefix = \""
                             << prefix_class << "\"" << endl;
#endif

        nested_[i] = new ClassDiscretizationComposition<D>(this, this->class_name_[i], true);

        // This is only useful for display.
        const string prefix_display = prefix_class + "display";

        if (ops.Exists(prefix_display))
          {
            nested_[i]->color_ = ops.Get<string>(prefix_display + ".color", "", "");
            nested_[i]->hatch_ = ops.Get<string>(prefix_display + ".hatch", "", "");
            nested_[i]->label_ = ops.Get<string>(prefix_display + ".label", "", nested_[i]->name_);
          }
      }
#endif

    // The total number of class from this level is equal to the index variation.
    Nclass_total_ = index_ - index_old;

    // Give back index to upper level.
    if (root_ != NULL)
      {
        root_->index_ += Nclass_total_;
        for (int i = 0; i < int(internal_mixing_ptr_.size()); i++)
          root_->internal_mixing_ptr_.push_back(internal_mixing_ptr_[i]);
      }

    return;
  }


  // Destructor.
  template<class D>
  ClassDiscretizationComposition<D>::~ClassDiscretizationComposition()
  {
    return;
  }


  // Get methods.
  template<class D>
  int ClassDiscretizationComposition<D>::GetNclassTotal() const
  {
    return Nclass_total_;
  }


  template<class D>
  int ClassDiscretizationComposition<D>::GetLevel() const
  {
    return level_;
  }


  template<class D>
  int ClassDiscretizationComposition<D>::GetIndex() const
  {
    return index_;
  }


  template<class D>
  int ClassDiscretizationComposition<D>::GetNfractionRandom() const
  {
    return Nfraction_random_;
  }


  template<class D>
  ClassDiscretizationComposition<D>* ClassDiscretizationComposition<D>::GetDiscretizationCompositionNested(const int &i) const
  {
    return nested_[i];
  }


  template<class D>
  ClassDiscretizationComposition<D>* ClassDiscretizationComposition<D>::GetClassInternalMixing(const int &i) const
  {
    return internal_mixing_ptr_[i];
  }


  // Compute fractions from particle composition.
  template<class D>
  inline void ClassDiscretizationComposition<D>::ComputeFraction(const vector1r &composition, vector1r &fraction) const
  {

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    if (abs(compute_vector_sum(composition, Nspecies_total_) - real(1))
        > AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_SUM)
      {
        CBUG << "sum of composition = " << composition << " is not unity.";
        throw AMC::Error("See message above.");
      }
#endif

    fraction.assign(this->Nlinear_, real(0));

    for (int i = 0; i < this->Nlinear_; i++)
      for (int j = 0; j < this->Nspecies_[i]; j++)
        fraction[i] += composition[this->species_index_[i][j]];

    if (species_index_root_ != NULL)
      {
        real sum(real(0));
        for (int i = 0; i < *Nspecies_root_; i++)
          sum += composition[(*species_index_root_)[i]];

        if (sum > real(0))
          for (int i = 0; i < this->Nlinear_; i++)
            fraction[i] /= sum;
      }

    // Avoid near zero value which would cause some algorithm to fail.
    for (int i = 0; i < this->Nlinear_; i++)
      if (fraction[i] < AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_ZERO)
        fraction[i] = real(0);

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    if (abs(compute_vector_sum(fraction, this->Nlinear_) - real(1))
        > AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_SUM)
      {
        CBUG << "sum of fraction = " << fraction << " is not unity.";
        throw AMC::Error("See message above.");
      }
#endif
  }


  template<class D>
  vector<real> ClassDiscretizationComposition<D>::ComputeFraction(const vector<real> &composition) const
  {
    vector<real> fraction;
    ComputeFraction(composition, fraction);
    return fraction;
  }


  // Find in which composition class is located one particle.
  template<class D>
  int ClassDiscretizationComposition<D>::FindClassCompositionIndex(const vector1r &composition, bool &on_edge) const
  {
#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    CBUG << "composition = " << composition << endl;
#endif

    // Is composition on a class edge ? Should almost never happen.
    on_edge = false;

    const ClassDiscretizationComposition<D>* discretization_composition_ptr = this;
    while (! discretization_composition_ptr->internal_mixing_)
      {
        vector1r fraction;
        discretization_composition_ptr->ComputeFraction(composition, fraction);

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
        CBUG << "fraction = " << fraction << endl;
#endif

        bool on_edge_raw(false);
        int class_index = discretization_composition_ptr->D::FindClassCompositionIndexRaw(fraction, on_edge_raw);

        // Once it is on edge, keep it on edge : depends on how nested classes are.
        on_edge = on_edge || on_edge_raw;
            
#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
        CBUG << "class_index = " << class_index << endl;
        if (class_index >= discretization_composition_ptr->Nclass_)
          throw AMC::Error("Out of range class index.");
#endif

        // Avoid segmentation fault at this step and always return something.
        if (class_index < discretization_composition_ptr->Nclass_)
          discretization_composition_ptr = discretization_composition_ptr->nested_[class_index];
        else
          return Nclass_total_;
      }

    return discretization_composition_ptr->index_;
  }


  // Generate random particle compositions.
  template<class D>
  void ClassDiscretizationComposition<D>::GenerateRandomComposition(const int &class_index,
                                                                    vector<real> &composition,
                                                                    bool on_edge) const
  {
    // This method should be called only from top level discretization.
    if (root_ != NULL)
      throw AMC::Error("This method can only be called from top level discretization.");

    // Pointer to internal mixing class and its root class.
    const ClassDiscretizationComposition<D>* internal_mixing_class_ptr = internal_mixing_ptr_[class_index];
    const ClassDiscretizationComposition<D>* discretization_composition_ptr = internal_mixing_class_ptr->root_;
    const ClassDiscretizationComposition<D>* discretization_composition_ptr_old = internal_mixing_class_ptr;

    // Random fraction vector.
    vector1r fraction_random(internal_mixing_class_ptr->Nfraction_random_, real(1));

    // Loop until reached top level.
    int pos(0);

    while (discretization_composition_ptr != NULL)
      {
        int class_index_loc = internal_mixing_class_ptr->class_index_path_random_[discretization_composition_ptr->level_];

        vector1r fraction_random_loc;

        if (discretization_composition_ptr->Ndim_ > 0)
          {
            if (on_edge)
              discretization_composition_ptr->D::GenerateEdgeFraction(class_index_loc, fraction_random_loc);
            else
              discretization_composition_ptr->D::GenerateRandomFraction(class_index_loc, fraction_random_loc);

            const int dimension_index = discretization_composition_ptr_old->dimension_index_root_;

            if (dimension_index >= 0)
              for (int i = 0; i < pos; i++)
                fraction_random[i] *= fraction_random_loc[dimension_index];

            for (int i = 0; i < discretization_composition_ptr->Nlinear_; i++)
              if (i != dimension_index)
                fraction_random[pos++] = fraction_random_loc[i];
          }

        // Going up.
        discretization_composition_ptr = discretization_composition_ptr->root_;
        discretization_composition_ptr_old = discretization_composition_ptr_old->root_;
      }

    // Finally, set composition.
    composition.assign(Nspecies_total_, real(0));

    for (int i = 0; i < internal_mixing_class_ptr->Nfraction_random_; i++)
      {
        vector1r composition_random;
        generate_random_vector(internal_mixing_class_ptr->Nspecies_random_[i],
                               composition_random, fraction_random[i]);

        for (int j = 0; j < internal_mixing_class_ptr->Nspecies_random_[i]; j++)
          composition[internal_mixing_class_ptr->species_index_random_[i][j]] = composition_random[j];
      }

#ifdef AMC_WITH_DEBUG_DISCRETIZATION_COMPOSITION
    if (abs(compute_vector_sum(composition, Nspecies_total_) - real(1))
        > AMC_DISCRETIZATION_COMPOSITION_FRACTION_THRESHOLD_SUM)
      {
        CBUG << "sum of composition = " << composition << " is not unity.";
        throw AMC::Error("See message above.");
      }
#endif
  }


  // Generate average composition.
  template<class D>
  void ClassDiscretizationComposition<D>::GenerateAverageComposition(const int &class_index,
                                                                     vector<real> &composition) const
  {
    // This method should be called only from top level discretization.
    if (root_ != NULL)
      throw AMC::Error("This method can only be called from top level discretization.");

    // Pointer to internal mixing class and its root class.
    const ClassDiscretizationComposition<D>* internal_mixing_class_ptr = internal_mixing_ptr_[class_index];
    const ClassDiscretizationComposition<D>* discretization_composition_ptr = internal_mixing_class_ptr->root_;
    const ClassDiscretizationComposition<D>* discretization_composition_ptr_old = internal_mixing_class_ptr;

    // Average fraction vector.
    vector1r fraction_average(internal_mixing_class_ptr->Nfraction_random_, real(1));

    // Loop until reached top level.
    int pos(0);

    while (discretization_composition_ptr != NULL)
      {
        int class_index_loc = internal_mixing_class_ptr->class_index_path_random_[discretization_composition_ptr->level_];

        vector1r fraction_average_loc;

        discretization_composition_ptr->D::GenerateAverageFraction(class_index_loc, fraction_average_loc);

        const int &dimension_index = discretization_composition_ptr_old->dimension_index_root_;

        if (dimension_index >= 0)
          for (int i = 0; i < pos; i++)
            fraction_average[i] *= fraction_average_loc[dimension_index];

        for (int i = 0; i < discretization_composition_ptr->Nlinear_; i++)
          if (i != dimension_index)
            fraction_average[pos++] = fraction_average_loc[i];

        // Going up.
        discretization_composition_ptr = discretization_composition_ptr->root_;
        discretization_composition_ptr_old = discretization_composition_ptr_old->root_;
      }

    // Finally, set composition.
    composition.assign(Nspecies_total_, real(0));

    for (int i = 0; i < internal_mixing_class_ptr->Nfraction_random_; i++)
      {
        real composition_average = real(1) / real(internal_mixing_class_ptr->Nspecies_random_[i]);

        for (int j = 0; j < internal_mixing_class_ptr->Nspecies_random_[i]; j++)
          composition[internal_mixing_class_ptr->species_index_random_[i][j]] = composition_average * fraction_average[i];
      }
  }


  // Manage tables.
  template<class D>
  void ClassDiscretizationComposition<D>::TableCompute()
  {
    if (D::Type() != AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of discretization composition is not \"Table\".");

    // Pointer discretization composition.
    ClassDiscretizationComposition<D>* discretization_composition_ptr = this;

    vector<int> next(D::id_counter_, 0);
    vector<bool> is_computed(D::id_counter_, false);

    while (discretization_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << discretization_composition_ptr->name_ << "\"." << endl;
#endif

        if (discretization_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            discretization_composition_ptr = discretization_composition_ptr->root_;
            continue;
          }

        int &id = discretization_composition_ptr->id_;
        if (is_computed[id])
          {
            if (next[id] == discretization_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "No more nested class, going up." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << discretization_composition_ptr->name_
                                     << "\" is already computed, go down to nested class number "
                                     << next[id] << "." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->nested_[next[id]++];
              }
          }
        else
          {
            discretization_composition_ptr->D::Compute();
            is_computed[id] = true;
          }
      }
  }


  template<class D>
  void ClassDiscretizationComposition<D>::TableClear()
  {
    if (D::Type() != AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of discretization composition is not \"Table\".");

    // Pointer discretization composition.
    ClassDiscretizationComposition<D>* discretization_composition_ptr = this;

    vector<int> next(D::id_counter_, 0);
    vector<bool> is_cleared(D::id_counter_, false);

    while (discretization_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << discretization_composition_ptr->name_ << "\"." << endl;
#endif

        if (discretization_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            discretization_composition_ptr = discretization_composition_ptr->root_;
            continue;
          }

        int &id = discretization_composition_ptr->id_;

        if (is_cleared[id])
          {
            if (next[id] == discretization_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "No more nested class, going up." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << discretization_composition_ptr->name_
                                     << "\" is already cleared, go down to nested class number "
                                     << next[id] << "." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->nested_[next[id]++];
              }
          }
        else
          {
            discretization_composition_ptr->D::Clear();
            is_cleared[id] = true;
          }
      }
  }


  template<class D>
  void ClassDiscretizationComposition<D>::TableRead()
  {
    if (D::Type() != AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of discretization composition is not \"Table\".");

    // Pointer discretization composition.
    ClassDiscretizationComposition<D>* discretization_composition_ptr = this;

    vector<int> next(D::id_counter_, 0);
    vector<bool> is_read(D::id_counter_, false);

    while (discretization_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << discretization_composition_ptr->name_ << "\"." << endl;
#endif

        if (discretization_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            discretization_composition_ptr = discretization_composition_ptr->root_;
            continue;
          }

        int &id = discretization_composition_ptr->id_;

        if (is_read[id])
          {
            if (next[id] == discretization_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "No more nested class, going up." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << discretization_composition_ptr->name_
                                     << "\" is already read, go down to nested class number "
                                     << next[id] << "." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->nested_[next[id]++];
              }
          }
        else
          {
            discretization_composition_ptr->D::Read();
            is_read[id] = true;
          }
      }
  }


  template<class D>
  void ClassDiscretizationComposition<D>::TableWrite() const
  {
    if (D::Type() != AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE)
      throw AMC::Error("Template class of discretization composition is not \"Table\".");

    // Pointer discretization composition.
    const ClassDiscretizationComposition<D>* discretization_composition_ptr = this;

    vector<int> next(D::id_counter_, 0);
    vector<bool> is_written(D::id_counter_, false);

    while (discretization_composition_ptr != NULL)
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Entering in class \""
                             << discretization_composition_ptr->name_ << "\"." << endl;
#endif

        if (discretization_composition_ptr->internal_mixing_)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "This is an internal mixing, going up." << endl;
#endif
            discretization_composition_ptr = discretization_composition_ptr->root_;
            continue;
          }

        const int &id = discretization_composition_ptr->id_;

        if (is_written[id])
          {
            if (next[id] == discretization_composition_ptr->Nclass_)
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "No more nested class, going up." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->root_;
                continue;
              }
            else
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Table for \"" << discretization_composition_ptr->name_
                                     << "\" is already written, go down to nested class number "
                                     << next[id] << "." << endl;
#endif
                discretization_composition_ptr = discretization_composition_ptr->nested_[next[id]++];
              }
          }
        else
          {
            discretization_composition_ptr->D::Write();
            is_written[id] = true;
          }
      }
  }


#ifdef AMC_WITH_TEST
  // Test function.
  template<class D>
  void ClassDiscretizationComposition<D>::Test(const ClassDiscretizationComposition<D> &discretization_composition, const int &N)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test each class of "
                         << (D::Type() == AMC_DISCRETIZATION_COMPOSITION_TYPE_TABLE ? "tabulated" : "base")
                         << " discretization composition \"" << discretization_composition.GetName()
                         << "\" (" << discretization_composition.GetNclassTotal()
                         << " of them) " << Fred().Str() << "with N = " << N << Reset().Str() << "." << endl;
#endif

    // At first test edges.
    for (int i = 0; i < discretization_composition.GetNclassTotal(); i++)
      {
        string class_name = discretization_composition.GetClassInternalMixing(i)->GetName();

        int wrong_class(0);
        for (int j = 0; j < N; j++)
          {
            vector1r composition;
            discretization_composition.GenerateRandomComposition(i, composition, true);

            bool on_edge;
            int k = discretization_composition.FindClassCompositionIndex(composition, on_edge);

            if (! on_edge)
              {
                ostringstream sout;
                sout << "Edge composition " << composition << " generated in class " << i << " is not found on edge.";
                throw AMC::Error(sout.str());
              }

            if (k != i)
              wrong_class++;
          }

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug() << Reset() << setw(40) << left << class_name << " : wrong_class = "
                                 << wrong_class * 100 / real(N) << "%" << endl;
#endif
      }

  // Then, test bulk classes.
#ifdef AMC_WITH_TIMER
    int time_index_tot = AMCTimer<CPU>::Add("test_discretization_composition_tot_" + discretization_composition.GetName());
#endif

    for (int i = 0; i < discretization_composition.GetNclassTotal(); i++)
      {
        string class_name = discretization_composition.GetClassInternalMixing(i)->GetName();

#ifdef AMC_WITH_TIMER
        int time_index = AMCTimer<CPU>::Add("test_discretization_composition_class_" + class_name);
#endif

        for (int j = 0; j < N; j++)
          {
            vector1r composition;
            discretization_composition.GenerateRandomComposition(i, composition, false);

#ifdef AMC_WITH_TIMER
            AMCTimer<CPU>::Start();
#endif

            bool on_edge;
            int k = discretization_composition.FindClassCompositionIndex(composition, on_edge);

#ifdef AMC_WITH_TIMER
            AMCTimer<CPU>::Stop(time_index);
            AMCTimer<CPU>::Stop(time_index_tot);
#endif

            if (on_edge)
              {
                ostringstream sout;
                sout << "Bulk composition " << composition << " generated in class " << i << " is on edge.";
                throw AMC::Error(sout.str());
              }

            if (k != i)
              {
                ostringstream sout;
                sout << "Bulk composition " << composition << " generated in class " << i << " is found in class " << k << ".";
                throw AMC::Error(sout.str());
              }
          }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << setw(40) << left << class_name << " : CPU time = "
                             << AMCTimer<CPU>::GetTimer(time_index) << " seconds (" << setprecision(12)
                             << AMCTimer<CPU>::GetTimer(time_index) / real(N) << " seconds/test)." << endl;
#endif
#endif
      }

    int Ntot = N * discretization_composition.GetNclassTotal();

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(0) << Reset() << "Performed " << Ntot << " tests for \""
                         << discretization_composition.GetName()
                         << "\" in " << AMCTimer<CPU>::GetTimer(time_index_tot) << " seconds"
                         << " (" << setprecision(12) << AMCTimer<CPU>::GetTimer(time_index_tot) / real(Ntot)
                         << " seconds/test)." << endl;
#endif
#endif
  }
#endif
}

#define AMC_FILE_CLASS_COMPOSITION_DISCRETIZATION_CXX
#endif
