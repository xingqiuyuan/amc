// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DISCRETIZATION_CLASS_HXX

namespace AMC
{
  /*! 
   * \class ClassDiscretizationClass
   * \brief Class gathering class discretization, if any.
   */
  template<class D>
  class ClassDiscretizationClass
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

    typedef D discretization_composition_type;
   
  protected:

    /*!< The maximum of class compositions number per size.*/
    static int Nclass_max_;

    /*!< List of composition discretizations.*/
    static vector<ClassDiscretizationComposition<D>* > discretization_composition_list_;

    /*!< Vector of pointers of class composition discretizations per size section.*/
    static vector<ClassDiscretizationComposition<D>* > discretization_composition_;

    /*!< Number of class compositions per size.*/
    static vector1i Nclass_;

  public:

    /*!< Constructor.*/
    static void Init(Ops::Ops &ops);

    /*!< Get methods.*/
    static int GetNclass(const int i);
#ifndef SWIG
    static vector<int> GetNclass();
#endif
    static ClassDiscretizationComposition<D>* GetDiscretizationCompositionList(const int i);
    static ClassDiscretizationComposition<D>* GetDiscretizationComposition(const int i);

    /*!< Find class composition index with its name.*/
    static int FindClassCompositionIndexByName(const int i, const string name, bool &error_occured);

    /*!< Manage table, if any.*/
    static void TableCompute();
    static void TableClear();
    static void TableRead();
    static void TableWrite();

    /*!< Clear.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_DISCRETIZATION_CLASS_HXX
#endif
