#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


from amc import core
from amc.nucleation import *
from amc.species import Species
from ops import Ops
import pylab, numpy
from matplotlib.colors import LogNorm
from numpy import linalg

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)


#
# Power law.
#


font_size = 12
width = 10

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.9
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

param_list = ParameterizationNucleationPowerLaw.GetAvailableParameter()

fig = pylab.figure()
ax = fig.gca()

name = param_list.keys()
P = numpy.array([v["P"] for v in param_list.itervalues()], dtype = numpy.float)
logK = numpy.array([v["logK"] for v in param_list.itervalues()], dtype = numpy.float)

A = numpy.array([P, numpy.ones(len(P))])
w = linalg.lstsq(A.T, logK)[0]

publi = []
for p in name:
    s = p.split(".")[0]
    if not s in publi:
        publi.append(s)

for pub, c in zip(publi, ["b", "g", "m"]):
    P2 = []
    logK2 = []

    for s, p, logk in zip(name, P, logK):
        if pub in s:
            P2.append(p)
            logK2.append(logk)

    ax.scatter(P2, logK2, s = 40, c = c, label = pub.capitalize().replace("_", " (") + ")")

ax.plot(P, w[0] * P + w[1], "r-", lw = 2, label = ur"$log_{10}(K) =%2.2f - %2.2f\times P$" % (w[1], -w[0]))

ax.set_xlabel("P")
ax.set_ylabel(ur"$log_{10}(K)$")
ax.set_xlim(xmin = 0, xmax = P.max() * 1.01)
ax.set_title(ur"(P, K) parameters as $log_{10}(K) = f(P)$ of power law nucleation rate ($J = K\times[H_2SO_4]^P$).")
ax.legend(loc = 1)
pylab.savefig("test_nucleation_power_law_parameter.png")


nucleation_rate = range(len(name))
for i,p in enumerate(name):
    ops.DoString("pow0 = {type = \"power_law:%s\"}" % p)
    ops.SetPrefix("pow0.")
    param_power_law = ParameterizationNucleationPowerLaw(ops)
    concentration_sulfate, nucleation_rate[i] = param_power_law.Test(N = 10)
nucleation_rate = numpy.array(nucleation_rate)

font_size = 17
width = 20

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.07
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.65
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

fig = pylab.figure()
ax = fig.gca()

for i in range(len(name)):
    print name[i]

    if "erupe" in name[i]:
        marker = "s"
    elif "kuang" in name[i]:
        marker = "o"
    elif "sihto" in name[i]:
        marker = "^"

    ax.loglog(concentration_sulfate, nucleation_rate[i], marker = marker, ms = 10, lw = 4, label = name[i])

ax.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax.set_ylabel(ur"Nucleation rate $[\#.cm^{-3}.s^{-1}]$")
ax.set_xlim(xmin = concentration_sulfate.min(), xmax = concentration_sulfate.max())
ax.set_ylim(ymin = nucleation_rate.min(), ymax = nucleation_rate.max())
ax.set_title(ur"Nucleation rate with power law functions ($J = K\times[H_2SO_4]^P$).")
ax.axhline(1, lw = 4, ls = "--", color = "k")
ax.tick_params(labelright = True)
ax.legend(bbox_to_anchor = [1.57, 0.95])

pylab.savefig("test_nucleation_power_law_rate.png")


#
# Vehkamaki.
#


param_vehkamaki = ParameterizationNucleationVehkamaki()


temperature = 236.                    
relative_humidity = 0.55

sulfate, nucleation_rate, critical_cluster_radius, \
    number_molecule, sulfuric_acid_mole_fraction = param_vehkamaki.Test(temperature = temperature,
                                                                        relative_humidity = relative_humidity)

font_size = 12
width = 10

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.9
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

fig = pylab.figure()
ax1 = fig.gca()

ax1.loglog(sulfate, nucleation_rate, "r-^", lw = 2)
ax1.set_ylabel(ur"Nucleation rate $[\#.cm^{-3}.s^{-1}]$", color = "r")
for t1 in ax1.get_yticklabels():
    t1.set_color("r")

ax1.set_ylim(ymin = 1.e-3, ymax = 1.e10)
ax1.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax1.set_xlim(xmin = sulfate.min(), xmax = sulfate.max())

ax1.set_title(ur"Vehkamaki (2002) nucleation parameterization, RH = %3.2f, T = %3.1f K." % (relative_humidity, temperature))
ax1.axhline(1, lw = 4, ls = "--", color = "k")

ax2 = ax1.twinx()
ax2.semilogx(sulfate, number_molecule, "b-o", lw = 2)
ax2.set_ylabel(ur"Number of molecules", color = "b")
for t2 in ax2.get_yticklabels():
    t2.set_color("b")

ax2.set_ylim(ymin = 0, ymax = 25)

pylab.savefig("test_nucleation_vehkamaki_rate.png")

#
pylab.clf()

fig = pylab.figure()
ax1 = fig.gca()

ax1.semilogx(sulfate, sulfuric_acid_mole_fraction, "r-^", lw = 2)
ax1.set_ylabel(ur"Sulfuric acid mole fraction", color = "r")
for t1 in ax1.get_yticklabels():
    t1.set_color("r")

ax1.set_ylim(ymin = sulfuric_acid_mole_fraction.min(), ymax = sulfuric_acid_mole_fraction.max())
ax1.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax1.set_xlim(xmin = sulfate.min(), xmax = sulfate.max())

ax1.set_title(ur"Vehkamaki (2002) nucleation parameterization, RH = %3.2f, T = %3.1f K." % (relative_humidity, temperature))

ax2 = ax1.twinx()
ax2.semilogx(sulfate, critical_cluster_radius, "b-o", lw = 2)
ax2.set_ylabel(ur"Critital cluster radius [nm]", color = "b")
for t2 in ax2.get_yticklabels():
    t2.set_color("b")

ax2.set_ylim(ymin = 0., ymax = 1.)

pylab.savefig("test_nucleation_vehkamaki_radius_mole_fraction.png")


#
sulfate, temperature, relative_humidity, \
    sulfuric_acid_mole_fraction = param_vehkamaki.TestSulfuricAcidMoleFraction()

font_size = 12
width = 10

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.9
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

level = 30
cs = ax.contourf(temperature, relative_humidity * 100, sulfuric_acid_mole_fraction[9], level)
ax.set_xlabel(ur"temperature [K]")
ax.set_ylabel(ur"relative humidity [%]")
cbar = pylab.colorbar(cs)
cbar.ax.set_ylabel(ur"sulfuric acide mole fraction $[0, 1]$")

ax.set_title(ur"Vehkamaki (2002) Sulfuric acid mole fraction, H2SO4 = %g $\#.cm^{-3}$." % sulfate[9])

pylab.savefig("test_nucleation_vehkamaki_sulfuric_acid_mole_fraction.png")


#
# Merikanto.
#


param_merikanto = ParameterizationNucleationMerikanto()

relative_humidity = 0.5

font_size = 12
width = 10

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.9
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

pylab.clf()
fig1 = pylab.figure()
fig2 = pylab.figure()
fig3 = pylab.figure()
fig4 = pylab.figure()
ax1 = fig1.gca()
ax2 = fig2.gca()
ax3 = fig3.gca()
ax4 = fig4.gca()

temperature = 235.15
ammonium = 10.

sulfate, ammonium, nucleation_rate, critical_cluster_radius, \
    mole_fraction, mass_fraction = param_merikanto.Test(temperature = temperature,
                                                        relative_humidity = relative_humidity,
                                                        ammonium = ammonium)

ax1.loglog(sulfate, nucleation_rate[:, 0], "r-", lw = 2, label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax2.semilogx(sulfate, critical_cluster_radius[:, 0], "r-", lw = 2,
             label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 0], "r-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 1], "r-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 0], "r-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 1], "r-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))

ammonium = 0.1

sulfate, ammonium, nucleation_rate, critical_cluster_radius, \
    mole_fraction, mass_fraction = param_merikanto.Test(temperature = temperature,
                                                        relative_humidity = relative_humidity,
                                                        ammonium = ammonium)

ax1.loglog(sulfate, nucleation_rate[:, 0], "b-", lw = 2, label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax2.semilogx(sulfate, critical_cluster_radius[:, 0], "b-", lw = 2,
             label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 0], "b-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 1], "b-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 0], "b-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 1], "b-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))

temperature = 273.15
ammonium = 1000.

sulfate, ammonium, nucleation_rate, critical_cluster_radius, \
    mole_fraction, mass_fraction = param_merikanto.Test(temperature = temperature,
                                                        relative_humidity = relative_humidity,
                                                        ammonium = ammonium)

ax1.loglog(sulfate, nucleation_rate[:, 0], "m-", lw = 2, label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax2.semilogx(sulfate, critical_cluster_radius[:, 0], "m-", lw = 2,
             label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 0], "m-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 1], "m-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 0], "m-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 1], "m-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))

ammonium = 10.

sulfate, ammonium, nucleation_rate, critical_cluster_radius, \
    mole_fraction, mass_fraction = param_merikanto.Test(temperature = temperature,
                                                        relative_humidity = relative_humidity,
                                                        ammonium = ammonium)

ax1.loglog(sulfate, nucleation_rate[:, 0], "c-", lw = 2, label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax2.semilogx(sulfate, critical_cluster_radius[:, 0], "c-", lw = 2,
             label = "T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 0], "c-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax3.semilogx(sulfate, mole_fraction[:, 0, 1], "c-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 0], "c-^", lw = 2,
             label = "sulfate, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))
ax4.semilogx(sulfate, mass_fraction[:, 0, 1], "c-o", lw = 2,
             label = "ammonium, T = %3.2f K, NH3 = %2.1f ppt" % (temperature, ammonium[0]))

ax1.set_ylabel(ur"Nucleation rate $[\#.cm^{-3}.s^{-1}]$")
ax2.set_ylabel(ur"Critical cluster radius [nm]")
ax3.set_ylabel(ur"Mole fraction [0, 1]")
ax4.set_ylabel(ur"Mass fraction [0, 1]")

ax1.set_ylim(ymin = sulfate.min(), ymax = sulfate.max())
ax1.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax2.set_ylim(ymin = sulfate.min(), ymax = sulfate.max())
ax2.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax3.set_ylim(ymin = sulfate.min(), ymax = sulfate.max())
ax3.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax4.set_ylim(ymin = sulfate.min(), ymax = sulfate.max())
ax4.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")


ax1.set_title(ur"Merikanto (2007, 2009b) nucleation parameterization, RH = %3.2f." % relative_humidity)
ax2.set_title(ur"Merikanto (2007, 2009b) critical cluster radius, RH = %3.2f." % relative_humidity)
ax3.set_title(ur"Merikanto (2007, 2009b) mole fraction, RH = %3.2f." % relative_humidity)
ax4.set_title(ur"Merikanto (2007, 2009b) mass fraction, RH = %3.2f." % relative_humidity)
ax1.axhline(1, lw = 4, ls = "--", color = "k")

ax1.set_ylim(ymin = 1.e-5, ymax = 1.e8)
ax2.set_ylim(ymin = 0., ymax = 1.)
ax3.set_ylim(ymin = 0., ymax = 1.)
ax4.set_ylim(ymin = 0., ymax = 1.)

ax1.legend(loc = 2)
ax2.legend(loc = 2)
ax3.legend(bbox_to_anchor = [1.006, 1.01])
ax4.legend(bbox_to_anchor = [1, 0.7])
fig1.savefig("test_nucleation_merikanto_rate.png")
fig2.savefig("test_nucleation_merikanto_radius.png")
fig3.savefig("test_nucleation_merikanto_mole_fraction.png")
fig4.savefig("test_nucleation_merikanto_mass_fraction.png")


#
temperature = 235.15
sulfate, ammonium, nucleation_rate, critical_cluster_radius, \
    mole_fraction, mass_fraction = param_merikanto.Test(temperature = temperature,
                                                        relative_humidity = relative_humidity)

font_size = 12
width = 10

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.9
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

level = 30
cs = ax.contourf(sulfate, ammonium, nucleation_rate, level, norm = LogNorm())
ax.set_xlabel(ur"Sulfate concentration $[\#.cm^{-3}]$")
ax.set_ylabel(ur"Ammonium concentration [ppt]")
ax.set_xscale("log")
ax.set_yscale("log")
cbar = pylab.colorbar(cs)
cbar.ax.set_ylabel(ur"Nucleation rate $[\#.cm^{-3}.s^{-1}]$")

ax.set_title(ur"Merikanto (2007, 2009b) nucleation parameterization, RH = %3.2f, T = %3.2f K." % (relative_humidity, temperature))

pylab.savefig("test_nucleation_merikanto_rate_2d.png")
