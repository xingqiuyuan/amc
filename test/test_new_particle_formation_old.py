#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc.base import MeteorologicalData, Configuration
from amc.species import Species
from amc.discretization import DiscretizationSize
from amc.new_particle_formation import NucleiData, NewParticleFormation
from amc.nucleation import *
from amc import core
from amc.util import ArrayReal

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU, TimerWall


# Configuration.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")

# Species.
Species.Init(ops)

# Meteo
MeteorologicalData.Init()

# Update.
MeteorologicalData.UpdateMeteo()
MeteorologicalData.UpdateNucleation()

data_meteo = MeteorologicalData.GetMeteo()
data_nucl = MeteorologicalData.GetNucleation()

#
# Power law.
#

ops.DoString("pow0 = {type = \"power_law:kuang_2008.atlanta\"}")
ops.SetPrefix("pow0.")
param = ParameterizationNucleationPowerLaw(ops)

ops.SetPrefix("new_part_form.")
NewParticleFormation.Init(ops)

param.SetIndexNPF()
NewParticleFormation.InitStep(param)

# Initial gas concentration and rate.
concentration_gas = ArrayReal(Species.GetNgas())
concentration_gas_test = numpy.zeros(Species.GetNgas(), dtype = numpy.float)
rate_gas_test = numpy.zeros([Species.GetNgas(), 2], dtype = numpy.float)

# Sulfate.
for i in range(Species.GetNgas()):
    concentration_gas[i] = 0.
concentration_gas[0] = 0.01

concentration_gas_test[0] = 0.0
rate_gas_test[0, 0] = 0.05
rate_gas_test[0, 1] = 0.001

# Run.
time_step = 10800.

event_formation, number, mass = NewParticleFormation.ComputeFormation(concentration_gas, time_step)

NewParticleFormation.Test(param,
                          concentration_gas_test,
                          rate_gas_test,
                          time_step,
                          output_file = "test_new_particle_formation_power_law.png")


#
# Vehkamaki.
#

MeteorologicalData.UpdateMeteo(236., core.AMC_PRESSURE_REFERENCE, 0.55)
MeteorologicalData.UpdateNucleation()

param = ParameterizationNucleationVehkamaki()

ops.SetPrefix("new_part_form.")
NewParticleFormation.Init(ops)

param.SetIndexNPF()
NewParticleFormation.InitStep(param)

# Initial gas concentration and rate.
concentration_gas_test = numpy.zeros(Species.GetNgas(), dtype = numpy.float)
rate_gas_test = numpy.zeros([Species.GetNgas(), 2], dtype = numpy.float)

# Sulfate.
for i in range(Species.GetNgas()):
    concentration_gas[i] = 0.
concentration_gas[0] = 0.01

concentration_gas_test[0] = 0.0
rate_gas_test[0, 0] = 0.05
rate_gas_test[0, 1] = 0.001

# Run.
time_step = 10800.

#event_formation, number, mass = NewParticleFormation.ComputeFormation(concentration_gas, time_step)

NewParticleFormation.Test(param,
                          concentration_gas_test,
                          rate_gas_test,
                          time_step,
                          output_file = "test_new_particle_formation_vehkamaki.png")


#
# Merikanto.
#

MeteorologicalData.UpdateMeteo(235.15, core.AMC_PRESSURE_REFERENCE, 0.7)
MeteorologicalData.UpdateNucleation()

param = ParameterizationNucleationMerikanto()

ops.SetPrefix("new_part_form.")
NewParticleFormation.Init(ops)

param.SetIndexNPF()
NewParticleFormation.InitStep(param)

# Initial gas concentration and rate.
concentration_gas_test = numpy.zeros(Species.GetNgas(), dtype = numpy.float)
rate_gas_test = numpy.zeros([Species.GetNgas(), 2], dtype = numpy.float)

# Sulfate and ammonia.
for i in range(Species.GetNgas()):
    concentration_gas[i] = 0.
concentration_gas[0] = 0.01

concentration_gas_test[0] = 0.0
rate_gas_test[0, 0] = 0.05
rate_gas_test[0, 1] = 0.001

concentration_gas_test[1] = 0.1
rate_gas_test[1, 0] = 0.0
rate_gas_test[1, 1] = 0.001

# Run.
time_step = 21600.

NewParticleFormation.SetCondensationTolerance(1.e-4)
NucleiData.ToggleThermodynamics()

#event_formation, number, mass = NewParticleFormation.ComputeFormation(concentration_gas, time_step)

gas, num, mass = NewParticleFormation.Test(param,
                                           concentration_gas_test,
                                           rate_gas_test,
                                           time_step,
                                           output_file = "test_new_particle_formation_merikanto.png",
                                           return_conc = ["gas", "num", "mass"])
