#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core, modal
from amc.base import AMC, MeteorologicalData, AerosolData
from amc.species import Species
from amc.condensation import CondensationFluxCorrectionDryInorganicSalt
from amc.util import set_random_generator, ArrayReal

from ops import Ops

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

# AMC.
AMC.Init("./test_amc.lua", "default")

ops = Ops("./test_amc.lua")
CondensationFluxCorrectionDryInorganicSalt.Init(ops)

# Meteo.
MeteorologicalData.Init()
MeteorologicalData.UpdateMeteo()
MeteorologicalData.UpdateCondensation()

print MeteorologicalData.GetMeteo()

dist = modal.ModalDistribution("urban_eurotrack")

set_random_generator(0)
concentration_aer_num, concentration_aer_mass, diameter_mean, class_distribution = AMC.GenerateModalDistributionRandom(dist)
print class_distribution

#AMC.ComputeThermodynamics(concentration_aer_num, concentration_aer_mass)
AMC.TestThermodynamics(concentration_aer_num, concentration_aer_mass, 1000)


