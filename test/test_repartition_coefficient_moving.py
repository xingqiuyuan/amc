#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import os, sys, numpy, pylab

from amc.base import AMC, Configuration
from amc.util import set_random_generator
from amc.redistribution import RedistributionClassBase
from amc.repartition_coefficient import CoefficientRepartitionCoagulationMovingBase
from amc import core
from ops import Ops

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU

# Random generator.
set_random_generator(0)

#
# AMC.
#

AMC.Init("./test_amc.lua", "default")

#
# Class redistribution.
#

ops = Ops("./test_amc.lua")
RedistributionClassBase.Init(ops)


#
# Moving
#

coef_file = "./coef_repart_moving.bin"
if os.path.isfile(coef_file):
    os.remove(coef_file)

ops.SetPrefix(Configuration.GetPrefix() + ".dynamic.coagulation.repartition_coefficient.")
coef = CoefficientRepartitionCoagulationMovingBase(ops)
coef.SetPath(coef_file)
