#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab, time

from amc.base import AMC, MeteorologicalData, AerosolData
from amc.discretization import DiscretizationSize
from amc.dynamics import DynamicsCoagulationMovingCoefficientBase
from amc.repartition_coefficient import CoefficientRepartitionCoagulationMovingBase
from amc.util import set_random_generator
from amc.modal import ModalDistribution
from amc.redistribution import RedistributionClassBase
from amc.numerics import NumericalSolverCoagulationMovingBase
from amc import core

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU

# Random generator.
set_random_generator(0)

#
# AMC.
#

AMC.Init("./test_amc.lua", "default")

#
# Redistribution.
#

from ops import Ops
ops = Ops("./test_amc.lua")
RedistributionClassBase.Init(ops)

#
# Moving
#

DynamicsCoagulationMovingCoefficientBase.Init(ops)
coef = DynamicsCoagulationMovingCoefficientBase.GetRepartitionCoefficientPtr()

#
# Meteo
#

MeteorologicalData.Init()
MeteorologicalData.UpdateMeteo(temperature = 300., pressure = core.AMC_PRESSURE_ATM)
MeteorologicalData.UpdateCoagulation()

print MeteorologicalData.GetVariable0d()
print MeteorologicalData.GetVariable0dUnitList()

#
# Initial distribution.
#

ModalDistribution.Init("test_amc.lua")
dist = ModalDistribution("seigneur_1986_urban")

class_dist = [[] for i in range(DiscretizationSize.GetNsection())]
class_dist[0] = [0.1, 0., 0.9]
class_dist[1] = [0.2, 0., 0.8]
class_dist[2] = [0.3, 0., 0.7]
class_dist[3] = [0., 0., 0.2, 0.2, 0.2, 0., 0.2, 0.2, 0.]
class_dist[4] = [0., 0., 0.2, 0.2, 0.2, 0., 0.2, 0.2, 0.]
class_dist[5] = [0., 0., 0.2, 0.2, 0.2, 0., 0.2, 0.2, 0.]
class_dist[6] = [0., 0., 0.2, 0.2, 0.2, 0., 0.2, 0.2, 0.]
class_dist[7] = [0.1, 0., 0.9]
class_dist[8] = [0.2, 0., 0.8]
class_dist[9] = [1.]

concentration_aer_num, concentration_aer_mass, diameter_mean = AMC.GenerateModalDistribution(dist, class_dist)

time_step = 100.
time_out = 3600. * 12

output_file = "./test_coagulation_dynamic_moving.bin"
if os.path.isfile(output_file):
    os.remove(output_file)

if not os.path.isdir("./fig"):
    os.makedirs("./fig")

# Solve coagulation over [0, time_out] with time_step, save result in "ouput_file".
NumericalSolverCoagulationMovingBase.Test(time_step, time_out, concentration_aer_num, concentration_aer_mass, output_file)

#
# Display results.
#

AMC.ReadConcentration(output_file,
                      concentration_aer_num, concentration_aer_mass, None, 0)

AMC.PlotConcentration(concentration_aer_num, concentration_aer_mass,
                      margins = {"left" : 0.05, "right" : 0.2, "bottom" : 0.1, "top" : 0.1},
                      threshold_number = 10., threshold_mass = 1.e-15,
                      legend_number = [1.5, 1.1], legend_mass = [1.5, 1.1],
                      font_size = 14, width = 48,
                      text = [0.5, 0.95, "t = 0 s", 18],
                      output_file = "./fig/test_coagulation_dynamic_moving_initial.png")

concentration_aer_num_im_beg, concentration_aer_mass_im_beg = \
    AMC.ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass)

AMC.ReadConcentration(output_file,
                      concentration_aer_num, concentration_aer_mass, None, -1)

AMC.PlotConcentration(concentration_aer_num, concentration_aer_mass,
                      margins = {"left" : 0.05, "right" : 0.2, "bottom" : 0.1, "top" : 0.1},
                      threshold_number = 10., threshold_mass = 1.e-15,
                      legend_number = [1.5, 1.1], legend_mass = [1.5, 1.1],
                      font_size = 14, width = 48,
                      text = [0.5, 0.95, "t = %d s" % time_out, 18],
                      output_file = "./fig/test_coagulation_dynamic_moving_final.png")

concentration_aer_num_im_end, concentration_aer_mass_im_end = \
    AMC.ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass)


fig = DiscretizationSize.PlotInternalMixingNumberConcentration(concentration_aer_num_im_end * 1.e-6,
                                                               fmt = "r-o", label = "final",
                                                               lw = 2, dNdlogDp = True)

DiscretizationSize.PlotInternalMixingNumberConcentration(concentration_aer_num_im_beg * 1.e-6, fig,
                                                         fmt = "b-s", label = "initial",
                                                         lw = 2, dNdlogDp = True, threshold = 1.e-2,
                                                         ylabel = ur"number concentration  $[\#.cm^{-3}]$",
                                                         title = "Number distribution", legend = 1,
                                                         output_file = "./fig/test_coagulation_dynamic_moving_number.png")


fig = DiscretizationSize.PlotInternalMixingMassConcentration(concentration_aer_mass_im_beg.sum(1),
                                                             fmt = "b-s", label = "initial",
                                                             lw = 2, dQdlogDp = True)

DiscretizationSize.PlotInternalMixingMassConcentration(concentration_aer_mass_im_end.sum(1), fig,
                                                       fmt = "r-o", label = "final",
                                                       lw = 2, dQdlogDp = True, threshold = 50.,
                                                       ylabel = ur"mass concentration  $[\mu m^{3}.cm^{-3}]$",
                                                       title = "Mass distribution", legend = 1,
                                                       output_file = "./fig/test_coagulation_dynamic_moving_mass.png")

sys.exit(0)

i = 0
while AMC.ReadConcentration(output_file,
                            concentration_aer_num, concentration_aer_mass, None, i):
    t = time_step * i
    print t

    AMC.PlotConcentration(concentration_aer_num, concentration_aer_mass,
                          margins = {"left" : 0.05, "right" : 0.2, "bottom" : 0.1, "top" : 0.1},
                          threshold_number = 10., threshold_mass = 1.e-15,
                          legend_number = [1.5, 1.1], legend_mass = [1.5, 1.1],
                          font_size = 14, width = 48,
                          text = [0.5, 0.95, "t = %05d s" % int(t), 18],
                          output_file = "./fig/test_coagulation_dynamic_moving_%05d.png" % int(t))

    i += 10
