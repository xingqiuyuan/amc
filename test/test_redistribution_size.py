#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc.discretization import DiscretizationSize
from amc.redistribution import \
    RedistributionSizeEulerHybrid, RedistributionSizeEulerMixed, \
    RedistributionSizeTableEulerHybrid, RedistributionSizeTableEulerMixed
from amc import core, modal

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerAMC

from ops import Ops
import time, numpy
import os, sys, copy
import pylab

if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

if os.path.isfile("redist_size.bin"):
    os.remove("redist_size.bin")

ops = Ops("./test_amc.lua")

ops.SetPrefix("default2.discretization.size.")
DiscretizationSize.Init(ops)

ops.SetPrefix("default2.redistribution.size.")
redist1 = RedistributionSizeEulerHybrid(ops)
redist2 = RedistributionSizeEulerMixed(ops)
redist3 = RedistributionSizeTableEulerHybrid(ops)
redist4 = RedistributionSizeTableEulerMixed(ops)

ops.Close()

#
#
#

diameter_bound = DiscretizationSize.GetDiameterBoundList()

width = 26 / 2.54
margins = [0.1, 0.9, 0.1, 0.9]
pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

font_size = 14
pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

dist_name_list = ['diesel_kittelson',
                  'land_eurotrack',
                  'marine_eurotrack',
                  'seigneur_1986_clear',
                  'seigneur_1986_hazy',
                  'seigneur_1986_urban',
                  'urban_eurotrack']

normalize = True

result = {}
for dist_name in dist_name_list:
    result[dist_name] = {"orig" : {}, redist1.GetType() : {}, redist2.GetType() : {}}

    dist = modal.ModalDistribution(dist_name)

    diameter_mean, dist_num, dist_vol = dist.ComputeSectionalDistribution(diameter_bound)

    dist_num *= 1.e6
    dist_mass = dist_vol * DiscretizationSize.GetDensityFixed() * 1.e6

    redist1_num, redist1_mass, redist1_diameter = redist1.Test(dist_num, dist_mass, normalize = normalize)
    redist2_num, redist2_mass, redist2_diameter = redist2.Test(dist_num, dist_mass, normalize = normalize)

    result[dist_name]["orig"]["num"] = dist_num.sum()
    result[dist_name]["orig"]["mass"] = dist_mass.sum()
    result[dist_name][redist1.GetType()]["num"] = [redist1_num.sum(), abs(dist_num - redist1_num).sum() / dist_num.sum()]
    result[dist_name][redist1.GetType()]["mass"] = [redist1_mass.sum(), abs(dist_mass - redist1_mass).sum() / dist_mass.sum()]
    result[dist_name][redist2.GetType()]["num"] = [redist2_num.sum(), abs(dist_num - redist2_num).sum() / dist_num.sum()]
    result[dist_name][redist2.GetType()]["mass"] = [redist2_mass.sum(), abs(dist_mass - redist2_mass).sum() / dist_mass.sum()]

    fig = pylab.figure()
    ax1 = fig.add_subplot(121)

    ax1.loglog(diameter_mean, dist_num, "ks-", label = "init", lw = 1)
    ax1.loglog(redist1_diameter, redist1_num, "mo-", label = "hybrid", lw = 1)
    ax1.loglog(redist2_diameter, redist2_num, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax1.axvline(diam, ls = "-.", color = "k")
    ax1.legend(loc = 1)
    ax1.set_xlabel(ur"diameter $d_p$")
    ax1.set_ylabel(ur"$\#.m^{-3}$")
    ax1.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax1.set_title("Number distribution.")
    #pylab.figtext(0.08, 0.94, "init=" + "%1.3g" % dist_num.sum() + ", redist=" + "%1.3g" % redist_num.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_num - redist_num).sum() / dist_num.sum()))

    ax2 = fig.add_subplot(122)

    ax2.semilogx(diameter_mean, dist_mass, "ks-", label = "init", lw = 1)
    ax2.semilogx(redist1_diameter, redist1_mass, "mo-", label = "hybrid", lw = 1)
    ax2.semilogx(redist2_diameter, redist2_mass, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax2.axvline(diam, ls = "-.", color = "k")
    ax2.legend(loc = 2)
    ax2.set_xlabel(ur"diameter $d_p$")
    ax2.set_ylabel(ur"$\mu g.m^{-3}$")
    ax2.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax2.set_title("Mass distribution.")
    #pylab.figtext(0.52, 0.94, "init=" + "%1.3g" % dist_mass.sum() + ", redist=" + "%1.3g" % redist_mass.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_mass - redist_mass).sum() / dist_mass.sum()))

    pylab.figtext(0.45, 0.97, dist_name, size = 18)

    fig.savefig("./test_redist_" + dist_name + ".png")

#
# Tables.
#

redist3.Compute()
redist4.Compute()

result_table = {}
for dist_name in dist_name_list:
    result_table[dist_name] = {"orig" : {}, redist3.GetType() : {}, redist4.GetType() : {}}

    dist = modal.ModalDistribution(dist_name)

    diameter_mean, dist_num, dist_vol = dist.ComputeSectionalDistribution(diameter_bound)

    dist_num *= 1.e6
    dist_mass = dist_vol * DiscretizationSize.GetDensityFixed() * 1.e6

    number_total = dist_num.sum()
    mass_total = dist_mass.sum()

    redist3_num, redist3_mass, redist3_diameter = redist3.Test(dist_num, dist_mass, normalize = normalize)
    redist4_num, redist4_mass, redist4_diameter = redist4.Test(dist_num, dist_mass, normalize = normalize)

    result_table[dist_name]["orig"]["num"] = dist_num.sum()
    result_table[dist_name]["orig"]["mass"] = dist_mass.sum()
    result_table[dist_name][redist3.GetType()]["num"] = [redist3_num.sum(), abs(dist_num - redist3_num).sum() / dist_num.sum()]
    result_table[dist_name][redist3.GetType()]["mass"] = [redist3_mass.sum(), abs(dist_mass - redist3_mass).sum() / dist_mass.sum()]
    result_table[dist_name][redist4.GetType()]["num"] = [redist4_num.sum(), abs(dist_num - redist4_num).sum() / dist_num.sum()]
    result_table[dist_name][redist4.GetType()]["mass"] = [redist4_mass.sum(), abs(dist_mass - redist4_mass).sum() / dist_mass.sum()]

    fig = pylab.figure()
    ax1 = fig.add_subplot(121)

    ax1.loglog(diameter_mean, dist_num, "ks-", label = "init", lw = 1)
    ax1.loglog(redist3_diameter, redist3_num, "mo-", label = "hybrid", lw = 1)
    ax1.loglog(redist4_diameter, redist4_num, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax1.axvline(diam, ls = "-.", color = "k")
    ax1.legend(loc = 1)
    ax1.set_xlabel(ur"diameter $d_p$")
    ax1.set_ylabel(ur"$\#.m^{-3}$")
    ax1.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax1.set_title("Number distribution.")
    #pylab.figtext(0.08, 0.94, "init=" + "%1.3g" % dist_num.sum() + ", redist=" + "%1.3g" % redist_num.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_num - redist_num).sum() / dist_num.sum()))

    ax2 = fig.add_subplot(122)

    ax2.semilogx(diameter_mean, dist_mass, "ks-", label = "init", lw = 1)
    ax2.semilogx(redist3_diameter, redist3_mass, "mo-", label = "hybrid", lw = 1)
    ax2.semilogx(redist4_diameter, redist4_mass, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax2.axvline(diam, ls = "-.", color = "k")
    ax2.legend(loc = 2)
    ax2.set_xlabel(ur"diameter $d_p$")
    ax2.set_ylabel(ur"$\mu g.m^{-3}$")
    ax2.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax2.set_title("Mass distribution.")
    #pylab.figtext(0.52, 0.94, "init=" + "%1.3g" % dist_mass.sum() + ", redist=" + "%1.3g" % redist_mass.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_mass - redist_mass).sum() / dist_mass.sum()))

    pylab.figtext(0.45, 0.97, dist_name, size = 14)

    fig.savefig("./test_redist_table_" + dist_name + ".png")


redist3.Write()
redist3.Clear()
redist3.Read()

redist4.Write()
redist4.Clear()
redist4.Read()

result_table_rw = {}
for dist_name in dist_name_list:
    result_table_rw[dist_name] = {"orig" : {}, redist3.GetType() : {}, redist4.GetType() : {}}

    dist = modal.ModalDistribution(dist_name)

    diameter_mean, dist_num, dist_vol = dist.ComputeSectionalDistribution(diameter_bound)

    dist_num *= 1.e6
    dist_mass = dist_vol * DiscretizationSize.GetDensityFixed() * 1.e6

    number_total = dist_num.sum()
    mass_total = dist_mass.sum()

    redist3_num, redist3_mass, redist3_diameter = redist3.Test(dist_num, dist_mass, normalize = normalize)
    redist4_num, redist4_mass, redist4_diameter = redist4.Test(dist_num, dist_mass, normalize = normalize)

    result_table_rw[dist_name]["orig"]["num"] = dist_num.sum()
    result_table_rw[dist_name]["orig"]["mass"] = dist_mass.sum()
    result_table_rw[dist_name][redist3.GetType()]["num"] = [redist3_num.sum(), abs(dist_num - redist3_num).sum() / dist_num.sum()]
    result_table_rw[dist_name][redist3.GetType()]["mass"] = [redist3_mass.sum(), abs(dist_mass - redist3_mass).sum() / dist_mass.sum()]
    result_table_rw[dist_name][redist4.GetType()]["num"] = [redist4_num.sum(), abs(dist_num - redist4_num).sum() / dist_num.sum()]
    result_table_rw[dist_name][redist4.GetType()]["mass"] = [redist4_mass.sum(), abs(dist_mass - redist4_mass).sum() / dist_mass.sum()]

    fig = pylab.figure()
    ax1 = fig.add_subplot(121)

    ax1.loglog(diameter_mean, dist_num, "ks-", label = "init", lw = 1)
    ax1.loglog(redist3_diameter, redist3_num, "mo-", label = "hybrid", lw = 1)
    ax1.loglog(redist4_diameter, redist4_num, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax1.axvline(diam, ls = "-.", color = "k")
    ax1.legend(loc = 1)
    ax1.set_xlabel(ur"diameter $d_p$")
    ax1.set_ylabel(ur"$\#.m^{-3}$")
    ax1.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax1.set_title("Number distribution.")
    #pylab.figtext(0.08, 0.94, "init=" + "%1.3g" % dist_num.sum() + ", redist=" + "%1.3g" % redist_num.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_num - redist_num).sum() / dist_num.sum()))

    ax2 = fig.add_subplot(122)

    ax2.semilogx(diameter_mean, dist_mass, "ks-", label = "init", lw = 1)
    ax2.semilogx(redist3_diameter, redist3_mass, "mo-", label = "hybrid", lw = 1)
    ax2.semilogx(redist4_diameter, redist4_mass, "cD-", label = "mixed", lw = 1)
    for diam in diameter_bound:
        ax2.axvline(diam, ls = "-.", color = "k")
    ax2.legend(loc = 2)
    ax2.set_xlabel(ur"diameter $d_p$")
    ax2.set_ylabel(ur"$\mu g.m^{-3}$")
    ax2.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
    ax2.set_title("Mass distribution.")
    #pylab.figtext(0.52, 0.94, "init=" + "%1.3g" % dist_mass.sum() + ", redist=" + "%1.3g" % redist_mass.sum() +
    #              ", rdiff=" + "%1.3g" % (abs(dist_mass - redist_mass).sum() / dist_mass.sum()))

    pylab.figtext(0.45, 0.97, dist_name, size = 14)

    fig.savefig("./test_redist_table_rw_" + dist_name + ".png")
