#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc.base import MeteorologicalData, Configuration
from amc.species import Species
import amc.new_particle_formation as npf
from amc.nucleation import *
from amc.util import ArrayReal
from amc import core

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU, TimerWall


# Configuration.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")

# Species.
Species.Init(ops)

# Meteo
MeteorologicalData.Init()

# Update.
MeteorologicalData.UpdateMeteo()
MeteorologicalData.UpdateCondensation()

data_meteo = MeteorologicalData.GetMeteo()

#
# Power law.
#

ops.DoString("pow0 = {type = \"power_law:kuang_2008.atlanta\"}")
ops.SetPrefix("pow0.")
param = ParameterizationNucleationPowerLaw(ops)

ops.SetPrefix("new_part_form.")
npf.NewParticleFormation.Init(ops)

param.SetIndexNPF()
npf.NewParticleFormation.InitStep(param)

# Gas.
concentration_gas = ArrayReal(Species.GetNgas())
for i in range(Species.GetNgas()):
    concentration_gas[i] = 0.

initial_gas =  numpy.zeros(Species.GetNgas(), dtype = numpy.float)
production_gas = numpy.zeros([Species.GetNgas(), 2], dtype = numpy.float)

# Sulfate.
concentration_gas[0] = 0.2 # µg.m^{-3}
#initial_gas[0] = 0.01
production_gas[0, 0] = 0.05
production_gas[0, 1] = 0.001

timeout = 10800.
time_output_frequency = 10.

#npf.NewParticleFormation.ToggleDebugPause()
formation_event, number, mass = npf.NewParticleFormation.Run(concentration_gas, timeout)

# N sections from 1 to 10 nm
N = 11
diameter_bound = numpy.logspace(0, 1, N)

#npf.NewParticleFormation.Test(param, timeout, initial_gas, production_gas,
#                              diameter_bound, time_output_frequency)
