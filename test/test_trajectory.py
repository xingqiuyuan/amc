#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
import driver
from driver import *
from random import uniform, seed

if core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

if core.DRIVER_HAS_TIMER > 0:
    from driver.util import TimerCPU, TimerWall

# Init AMC before ... or trouble ahead.
amc.AMX_EulerMixedBaseMoving.Initiate("./test_amc.lua", "default")

# Init trajectory model.
trajectory.TrajectoryChimere.Init("./test_driver.lua", "trajectory.chimere")

N = 1

traj = [trajectory.TrajectoryChimere() for i in range(N)]

lat_min = 36.
lat_max = 55.
lon_min = -14.
lon_max = -1.

seed(0)

tmax = numpy.zeros(N)
for i in range(N):
    tmax[i] = traj[i].Load(uniform(lat_min, lat_max), uniform(lon_min, lon_max), 0., 600.)

if N > 1:
    fig = display.PlotTrajectory(traj[0], offset = 1.7, margins = {"left" : 0.})
    for i in range(1, N - 1):
        display.PlotTrajectory(traj[i], fig = fig)
    display.PlotTrajectory(traj[-1], fig = fig, legend = [1.4, 1.], output_file = "./test_trajectory.png")
elif N == 1:
    display.PlotTrajectory(traj[-1], offset = 1.7, margins = {"left" : 0.}, fmt = "k-",
                           legend = [1.4, 1.], output_file = "./test_trajectory.png")


# Initial concentrations from CHIMERE output file.
base.InitialConcentration.Init()

cinit = base.InitialConcentration("chimere_sa")

Nz = traj[0].GetNz()

concentration_aer_num = [[] for i in range(Nz)]
concentration_aer_mass = [[] for i in range(Nz)]
concentration_gas = [[] for i in range(Nz)]

for i in range(Nz):
    cinit.Read("test_trajectory_ic_%0d.bin" % i)
    concentration_aer_num[i], concentration_aer_mass[i], concentration_gas[i] = cinit.Redistribute(size = "EulerMixed")

# Run.
if os.path.isfile(traj[0].GetOutputFile()):
    os.remove(traj[0].GetOutputFile())

traj[0].Run(concentration_aer_num, concentration_aer_mass, concentration_gas)
