#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab

from amc import AMC, AMCtable, \
    DiscretizationSize, \
    DynamicsCoagulationStaticCoefficientBase, \
    DynamicsCoagulationStaticCoefficientTable, \
    CoefficientRepartitionCoagulationStaticBase, \
    CoefficientRepartitionCoagulationStaticTable, \
    MeteorologicalData, modal, set_random_generator, core

if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC


# AMC.
AMC.Init("./test_amc.lua", "default4")

# Random generator.
set_random_generator(0)

#
# Static
#

# Redistribution.
from ops import Ops
ops = Ops("./test_amc.lua")
DynamicsCoagulationStaticCoefficientBase.Init(ops)

coef = DynamicsCoagulationStaticCoefficientBase.GetRepartitionCoefficientPtr()

if len(sys.argv[1:]) == 0 and os.path.isfile(coef.GetPath()):
    coef2 = CoefficientRepartitionCoagulationStaticBase(ops)
else:
    coef.ClearCoefficient()
    precision = coef.ComputeCoefficient(float(sys.argv[1]))

    coef_file = coef.GetPath()
    if len(sys.argv[1:]) > 1:
        coef_file = sys.argv[2]

    if os.path.isfile(coef_file):
        os.remove(coef_file)

    coef.Write()


# Meteo
MeteorologicalData.Init()
MeteorologicalData.UpdateCoagulation()

print MeteorologicalData.GetMeteo()
print MeteorologicalData.GetCoagulation()

# Initial distribution.
dist = modal.ModalDistribution("seigneur_1986_urban")

class_dist = numpy.zeros(AMC.GetNclass(0), dtype = numpy.float)
class_dist[0] = 0.5
class_dist[-1] = 0.5

concentration_aer_num, concentration_aer_mass, diameter_mean = AMC.GenerateModalDistribution(dist, class_dist)

concentration_aer_num_beg_pretty, concentration_aer_mass_beg_pretty = \
    AMC.GetConcentrationPretty(concentration_aer_num, concentration_aer_mass)

AMC.PlotConcentration(concentration_aer_num_beg_pretty,
                      kind = "number", legend = 1,
                      output_file = "./dist_number_init_coag.png")

degree_num, degree_mass = AMC.ComputeExternalMixingDegree(concentration_aer_num, concentration_aer_mass)
print degree_num
print degree_mass

concentration_aer_num_im_beg, concentration_aer_mass_im_beg = \
    AMC.ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass)


# Time loop.
deltat = 10.
tend = 3600. * 12
tfig = tend
deltatfig = 100.
t = 0

if not os.path.isdir("./fig/coag_static"):
    os.makedirs("./fig/coag_static")

print t, concentration_aer_num_im_beg.sum(), concentration_aer_mass_im_beg.sum(0)
AMC.Plot2d(concentration_aer_num_beg_pretty, output_file = "./fig/coag_static/test_dist2d_num_%05d.png" % int(t))

while deltat > 0.:
    # Compute internal diameter and masses.
    AMC.ComputeParticleDiameterMassFixedDensity(concentration_aer_num, concentration_aer_mass)

    rate_aer_num, rate_aer_mass = \
        DynamicsCoagulationStaticCoefficientBase.Rate(concentration_aer_num, concentration_aer_mass)

    deltat2 = deltat
    for i in range(AMC.GetNg()):
        if concentration_aer_num[i] + deltat2 * rate_aer_num[i] < 0.:
            deltat2 = min(deltat2, numpy.floor(- 0.99 * concentration_aer_num[i] / rate_aer_num[i]))

    for i in range(AMC.GetNgNspecies()):
        if concentration_aer_mass[i] + deltat2 * rate_aer_mass[i] < 0.:
            deltat2 = min(deltat2, numpy.floor(- 0.99 * concentration_aer_mass[i] / rate_aer_mass[i]))

    for i in range(AMC.GetNg()):
        concentration_aer_num[i] += deltat2 * rate_aer_num[i]

    for i in range(AMC.GetNgNspecies()):
        concentration_aer_mass[i] += deltat2 * rate_aer_mass[i]

    #AMC.Redistribute(concentration_aer_num, concentration_aer_mass)

    concentration_aer_num_im, concentration_aer_mass_im = \
        AMC.ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass)

    t += deltat
    deltat = min(deltat, tend - t)

    print t, concentration_aer_num_im.sum(), concentration_aer_mass_im.sum(0)

    if t >= tfig:
        concentration_aer_num_pretty, concentration_aer_mass_pretty = \
            AMC.GetConcentrationPretty(concentration_aer_num, concentration_aer_mass)

        AMC.Plot2d(concentration_aer_num_pretty, output_file = "./fig/coag_static/test_dist2d_num_%05d.png" % int(t))

        tfig += deltatfig

# End.

concentration_aer_num_im_end, concentration_aer_mass_im_end = \
    AMC.ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass)

pylab.close('all')
fig = pylab.figure()

ax = fig.gca()

diameter_bound = DiscretizationSize.GetDiameterBoundList()
diameter_width_log = [numpy.log(diameter_bound[i + 1] / diameter_bound[i]) for i in range(DiscretizationSize.GetNsection())]

concentration_aer_num_im_beg = numpy.array([x / y for x, y in zip(concentration_aer_num_im_beg, diameter_width_log)]) * 1.e-6
concentration_aer_num_im_end = numpy.array([x / y for x, y in zip(concentration_aer_num_im_end, diameter_width_log)]) * 1.e-6

ax.loglog(diameter_mean, concentration_aer_num_im_end, "r-o", label = "final")
ax.loglog(diameter_mean, concentration_aer_num_im_beg, "b-s", label = "init")

ax.set_xlim(0.001, 10.)
ax.set_ylim(1.e-2, concentration_aer_num_im_beg.max() * 2)
pylab.show()
