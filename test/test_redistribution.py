#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core, modal
from amc.base import AMC
from amc.discretization import DiscretizationSize, \
    DiscretizationCompositionBase, DiscretizationCompositionTable
from amc.util import set_random_generator, generate_random_vector
from amc.redistribution import \
    RedistributionClassBase, RedistributionClassTable, \
    RedistributionClassBaseTable, RedistributionClassTableBase, \
    RedistributionSizeMovingCompositionBase, \
    RedistributionSizeMovingCompositionBaseTable, \
    RedistributionSizeMovingCompositionTable, \
    RedistributionSizeHybridCompositionBase, \
    RedistributionSizeHybridCompositionBaseTable, \
    RedistributionSizeHybridCompositionTable, \
    RedistributionSizeHybridTableCompositionBase, \
    RedistributionSizeHybridTableCompositionBaseTable, \
    RedistributionSizeHybridTableCompositionTable, \
    RedistributionSizeMixedCompositionBase, \
    RedistributionSizeMixedCompositionBaseTable, \
    RedistributionSizeMixedCompositionTable, \
    RedistributionSizeMixedTableCompositionBase, \
    RedistributionSizeMixedTableCompositionBaseTable, \
    RedistributionSizeMixedTableCompositionTable

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerAMC

from ops import Ops
import time, numpy
import random
import os, sys
import pylab

if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

AMC.Init("./test_amc.lua", "default")

ops = Ops("./test_amc.lua")
RedistributionSizeMovingCompositionBase.Init(ops)
RedistributionSizeMixedCompositionBase.Init(ops)
RedistributionSizeHybridCompositionBase.Init(ops)


# Test.
N = 100
set_random_generator(0)

dist_name_list = modal.GetAvailableDistribution()
print dist_name_list

for dist_name in dist_name_list:
    dist = modal.ModalDistribution(dist_name)
    RedistributionSizeMixedCompositionBase.Test(dist, N)
    RedistributionSizeHybridCompositionBase.Test(dist, N)
