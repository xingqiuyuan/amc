#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab, copy
from ops import Ops
from amc import Species, SpeciesEquilibrium, \
    ParameterizationCondensationDiffusionLimitedDahneke, \
    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1, \
    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt, \
    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2, \
    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt, \
    ParameterizationCondensationDiffusionLimitedFuchsSutugin, \
    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1, \
    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt, \
    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2, \
    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt, \
    core, set_random_generator, ParameterizationKelvinEffect, \
    ModelThermodynamicIsoropia, ModelThermodynamicAEC, ModelThermodynamicPankow


if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC


# Config.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)
SpeciesEquilibrium.Init(ops)

# Thermodynamic models.
ops.SetPrefix("default.thermodynamic.Aisrpia.")
isrpia = ModelThermodynamicIsoropia(ops)

ops.SetPrefix("default.thermodynamic.Bpank.")
pank = ModelThermodynamicPankow(ops)

ops.SetPrefix("default.thermodynamic.Caec.")
aec = ModelThermodynamicAEC(ops)

thermo = [isrpia, aec, pank]

# Condensation parameterizations.
ops.SetPrefix("default.dynamic.condensation.")
cond1 = ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1(ops)
cond2 = ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2(ops)
ke = ParameterizationKelvinEffect(ops)

set_random_generator(0)
total_mass = 10.

concentration_aer_mass = Species.GenerateRandomComposition() * total_mass
idx = Species.GetIndex("MGLY")
concentration_aer_mass[idx] = 0.0

concentration_gas = core.vector1r(Species.GetNgas(), 1.)

equilibrium_aer_internal = core.vector1r(SpeciesEquilibrium.GetNspecies() + 1, 0.)
equilibrium_gas_surface1 = core.vector1r(Species.GetNgas())
equilibrium_gas_surface2 = core.vector1r(Species.GetNgas())

diameter = 0.05
pressure = 1.013e5
relative_humidity = 0.7
temperature = 300.
density = 1.2

for i in range(3):
    thermo[i].Test2(temperature,
                    relative_humidity,
                    concentration_aer_mass,
                    equilibrium_aer_internal,
                    equilibrium_gas_surface1)

for i in range(Species.GetNgas()):
    equilibrium_gas_surface2[i] = equilibrium_gas_surface1[i]

print list(equilibrium_aer_internal)
print list(equilibrium_gas_surface1)

lwc = equilibrium_aer_internal[-1]
print lwc

kelvin_effect = ke.Test(temperature, diameter, lwc, concentration_aer_mass)
print list(kelvin_effect)

rate_aer_mass1 = cond1.Test(diameter, density,
                            kelvin_effect,
                            concentration_gas,
                            concentration_aer_mass,
                            equilibrium_aer_internal,
                            equilibrium_gas_surface1,
                            temperature = temperature,
                            pressure = pressure)

rate_aer_mass2 = cond2.Test(diameter, density,
                            kelvin_effect,
                            concentration_gas,
                            concentration_aer_mass,
                            equilibrium_aer_internal,
                            equilibrium_gas_surface2,
                            temperature = temperature,
                            pressure = pressure)

print "Rate"
print rate_aer_mass1
print rate_aer_mass2
print "Gas surface"
print list(equilibrium_gas_surface1)
print list(equilibrium_gas_surface2)
