#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc.base import MeteorologicalData, Configuration
from amc.species import Species
from amc.discretization import DiscretizationSize
import amc.new_particle_formation as npf
from amc.nucleation import *
from amc import core
from amc.util import set_random_generator

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU, TimerWall


# Configuration.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")

# Species.
Species.Init(ops)

# Meteo
MeteorologicalData.Init()

# Update.
MeteorologicalData.UpdateMeteo()
MeteorologicalData.UpdateCondensation()

data_meteo = MeteorologicalData.GetMeteo()

#
# Init.
#

ops.SetPrefix("new_part_form.")
npf.ParticleData.Init(ops)

#
# Test.
#

set_random_generator(0)

npf.ParticleData.Test(10000, False)
