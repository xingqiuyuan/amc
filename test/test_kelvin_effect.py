#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc import AMC, ParameterizationKelvinEffect, Species, \
    core, set_random_generator

font_size = 12
width = 10
margins = [0.1, 0.84, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1


if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC

# Config.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)

# Kevin effect.
ops.SetPrefix("default.dynamic.condensation.")
param = ParameterizationKelvinEffect(ops)

dmin = 0.001
dmax = 10.0
q = dmax / dmin
n = 1000
diameter = [dmin * numpy.power(q, float(i) / float(n - 1)) for i in range(n)]
temperature = 300.
total_mass = 10.
lwc = total_mass * 0.1

set_random_generator(0)
concentration_aer_mass = Species.GenerateRandomComposition() * total_mass

ke = []
for i,d in enumerate(diameter):
    ke.append(param.Test(temperature, d, lwc, concentration_aer_mass))

ke = numpy.array(ke, dtype = numpy.float)


pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

semivol = Species.GetSemivolatileList()

for i in range(Species.GetNgas()):
    ax.loglog(diameter, ke[:,i], lw = 2, label = Species.GetName(semivol[i]))

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("Kelvin effect [adim]")
ax.set_title("Kelvin effect as a function of diameter fo various species.")
ax.legend(bbox_to_anchor = [1.21, 1])
pylab.savefig("test_kelvin_effect.png")
