#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc.species import Species
from amc.discretization import DiscretizationCompositionBase, \
    DiscretizationCompositionTable
from amc import core
from amc.util import set_random_generator

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerAMC

from ops import Ops
import time, numpy
import os, sys
import pylab, glob

if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)

ops.SetPrefix("default.discretization.composition.")
disc = DiscretizationCompositionBase(None, "disc0", ops)

for i in range(disc.GetNclass()):
    print disc.GetClassPointCoordinateList(i)

# Set random generator for reproducibility.
set_random_generator(0)

N = 100000


#
# Base class look up.
#


DiscretizationCompositionBase.Test(disc, N)
if core.AMC_HAS_TIMER > 0:
    time = TimerAMC.Display()
    TimerAMC.Clear();


#
# Table class look up.
#


# Reset and compute table.
for fic in glob.glob("./disc*.bin"):
    os.remove(fic)

ops.SetPrefix("default.discretization.composition.")
disc_table = DiscretizationCompositionTable(None, "disc0", ops)

disc_table.TableCompute()

DiscretizationCompositionTable.Test(disc_table, N)
if core.AMC_HAS_TIMER > 0:
    time_table = TimerAMC.Display()
    TimerAMC.Clear();


# Write and read.
disc_table.TableWrite()
disc_table.TableClear()
disc_table.TableRead()

DiscretizationCompositionTable.Test(disc_table, N)


#
# Display.
#

N = 200

width = 20
font_size = 15
class_color = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00",
              "#00FFFF", "#FF00FF", "#808080", "#A52A2A",
              "#FF7F50", "#FFA500", "#FFC0CB", "#000000"]

fig = disc.Plot2d(class_color = class_color, width = width, font_size = font_size)
ax = fig.gca()

for i in range(disc.GetNclassTotal()):
    c = disc.GenerateAverageComposition(i)
    f = disc.ComputeFraction(c)
    ax.scatter(f[0], f[1], 30, marker = "s", color = class_color[i], zorder = 10)

    for j in range(N):
        c = disc.GenerateRandomComposition(i)
        f = disc.ComputeFraction(c)
        ax.scatter(f[0], f[1], 10, marker = "o", color = class_color[i], zorder = 10)

#pylab.show()
pylab.savefig("test_disc0_composition.png")


# At edge.
fig = disc.Plot2d(class_color = class_color, width = width, font_size = font_size)
ax = fig.gca()

for i in range(disc.GetNclassTotal()):
    c = disc.GenerateAverageComposition(i)
    f = disc.ComputeFraction(c)
    ax.scatter(f[0], f[1], 30, marker = "s", color = class_color[i], zorder = 10)

    for j in range(N):
        c = disc.GenerateRandomCompositionOnEdge(i)
        f = disc.ComputeFraction(c)
        ax.scatter(f[0], f[1], 10, marker = "o", color = class_color[i], zorder = 10)

#pylab.show()
pylab.savefig("test_disc0_composition_edge.png")


# Nested.
ops.SetPrefix("default.discretization.composition.")
disc1 = DiscretizationCompositionBase(None, "disc1", ops)

pylab.clf()
fig = disc1.Plot2d(class_color = class_color, width = width, font_size = font_size)
ax = fig.gca()

for i in range(disc1.GetNclassTotal()):
    c = disc1.GenerateAverageComposition(i)
    f = disc1.ComputeFraction(c)
    ax.scatter(f[0], f[1], 30, marker = "s", color = class_color[i], zorder = 10)

    for j in range(N):
        c = disc1.GenerateRandomComposition(i)
        f = disc1.ComputeFraction(c)
        ax.scatter(f[0], f[1], 10, marker = "o", color = class_color[i], zorder = 10)

pylab.savefig("test_disc1_composition.png")


ops.SetPrefix("default.discretization.composition.")
disc2 = DiscretizationCompositionBase(None, "disc2", ops)

pylab.clf()
fig = disc2.Plot1d(class_color = class_color, width = width, font_size = font_size)
ax = fig.gca()

for i in range(disc2.GetNclassTotal()):
    c = disc2.GenerateAverageComposition(i)
    f = disc2.ComputeFraction(c)
    ax.scatter(0.3, f[0], 30, marker = "s", color = class_color[i], zorder = 10)

    for j in range(N):
        c = disc2.GenerateRandomComposition(i)
        f = disc2.ComputeFraction(c)
        ax.scatter(0.2, f[0], 10, marker = "o", color = class_color[i], zorder = 10)

pylab.savefig("test_disc2_composition.png")

ops.Close()

#
#
#
for i in range(disc.GetNclassTotal()):
    im = disc.GetClassInternalMixing(i)
    print i, im.GetName() #, im.GetColor(), im.GetLabel(), im.GetHatch() 


ops.Close()
