#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc_mpi import AMC, AMCtable, \
    CoefficientRepartitionCoagulationStaticBase, \
    CoefficientRepartitionCoagulationStaticTable, \
    set_random_generator, core

from mpi4py import MPI

if core.AMC_HAS_LOGGER > 0:
    from amc_mpi import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

import os, sys

precision = 0.0001
if len(sys.argv[1:]) > 0:
    precision = float(sys.argv[1])

coefficient_file = "./coagulation_coefficient_mpi.nc"
if len(sys.argv[2:]) > 0:
    coefficient_file = sys.argv[2]

# Init MPI.
AMC.MPI_Init()

rank = AMC.GetRank()
Nrank = AMC.GetNrank()

comm = MPI.COMM_WORLD
print comm.Get_rank(), comm.Get_size()


# AMC.
AMC.Init("./test_amc.lua", "default4")

# Config.
from ops import Ops
ops = Ops("./test_amc.lua")
ops.SetPrefix("default4.dynamic.coagulation.coefficient.")

# Random generator.
set_random_generator(0)

#
# Static
#

coef = CoefficientRepartitionCoagulationStaticBase(ops)

precision = coef.ComputeCoefficient(precision)

comm.Barrier()

print rank, precision

# Gather all coefficients in rank number 0.
coef.MPI_GatherCoefficient(0)

if rank == 0:
    coef.Write(coefficient_file)

comm.Barrier()
