#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc.discretization import DiscretizationSize
from amc import core
from ops import Ops
import time, numpy
from pylab import *

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default1.discretization.size.")
DiscretizationSize.Init(ops)
ops.Close()

print DiscretizationSize.GetNbound(), DiscretizationSize.GetDiameterBound()
print DiscretizationSize.GetNsection(), DiscretizationSize.GetDiameterMean()

ops = Ops("./test_amc.lua")
ops.SetPrefix("default2.discretization.size.")
DiscretizationSize.Init(ops)
ops.Close()

print DiscretizationSize.GetNbound(), DiscretizationSize.GetDiameterBound()
print DiscretizationSize.GetNsection(), DiscretizationSize.GetDiameterMean()

DiscretizationSize.Clear()

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.discretization.size.")
DiscretizationSize.Init(ops)
ops.Close()

print DiscretizationSize.GetNbound(), DiscretizationSize.GetDiameterBound()
print DiscretizationSize.GetNsection(), DiscretizationSize.GetDiameterMean()

N = 100000
diam = DiscretizationSize.GetDiameterBound()
diam_log = numpy.log(diam)
diam_rand = numpy.exp([numpy.random.uniform(diam_log[0], diam_log[-1]) for i in range(N)])

section_index = []
start = time.clock()

for d in diam_rand:
    section_index.append(DiscretizationSize.SearchSectionIndex(d))

end = time.clock()
section_index = numpy.array(section_index, dtype = numpy.int)

cpu_time = (end - start) / float(N)

fig = figure(figsize = (14, 8))
semilogx(diam_rand, section_index, "kx")
xlabel("diameters")
ylabel("sections")
xlim(xmin = diam.min(), xmax = diam.max())
ylim(ymin = 0, ymax = DiscretizationSize.GetNsection() - 1)
xticks(diam, ["%1.2f" % x for x in diam])
for x in diam:
    axvline(x, c = "k", ls = "--")

title("Random diameters and their associated sections, CPU time = %2.6g, N = %d" % (cpu_time, N))
savefig("./test_diameter_random.png")
