#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core
from amc.species import Species
from amc.util import set_random_generator
from amc.redistribution import \
    RedistributionCompositionNestedBase, \
    RedistributionCompositionNestedBaseTable, \
    RedistributionCompositionNestedTableBase, \
    RedistributionCompositionNestedTable
from amc.discretization import DiscretizationCompositionBase, \
    DiscretizationCompositionTable

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU

from ops import Ops
import time, numpy
import random, glob
import os, sys
import pylab

if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)


#
# Init.
#


set_random_generator(0)
ops.SetPrefix("default.discretization.composition.")
disc = DiscretizationCompositionBase(None, "disc0", ops)

ops.SetPrefix("default.redistribution.composition.")
redist = RedistributionCompositionNestedBase(None, disc, ops)

#
# Test base redistribution.
#


tol = 1.e-8
N = 100000
display = False

RedistributionCompositionNestedBase.Test(redist, N, tol, display)

if core.AMC_HAS_TIMER > 0:
    time = TimerCPU.Display()
    TimerCPU.Clear();


#
# Test table redistribution.
#


for f in glob.glob("./redist_comp?.bin"):
    os.remove(f)

redist_table = RedistributionCompositionNestedBaseTable(None, disc, ops)
redist_table.TableCompute()
redist_table.TableWrite()

tol = 1.e-8
N = 100000
display = False

RedistributionCompositionNestedBaseTable.Test(redist_table, N, tol, display)

if core.AMC_HAS_TIMER > 0:
    time_table = TimerCPU.Display()
    TimerCPU.Clear();    


redist_table.Clear()
redist_table.TableRead()

RedistributionCompositionNestedBaseTable.Test(redist_table, N, tol, display)

if core.AMC_HAS_TIMER > 0:
    time_table_rw = TimerCPU.Display()
    TimerCPU.Clear();    


#
# Test table against base.
#

tol = 1.e-8
N = 10000
for i in range(disc.GetNclassTotal()):
    print disc.GetClassInternalMixing(i).GetName()

    for j in range(N):
        conc = disc.GenerateRandomComposition(i)
        tab = redist.TestSelf(conc)
        tab_redist = redist_table.TestSelf(conc)

        if len(tab) != len(tab_redist):
            raise ValueError("Base and tabulated redistribution have not the same number of classes.")

        for k, t, tr in zip(range(len(tab)), tab, tab_redist):
            if t[0] != tr[0]:
                raise ValueError("At index " + str(k) + " class index is " + str(t[0]) +
                                 " for base and " + str(tr[0]) + " for table.")

            if abs(t[2] - tr[2]) / t[2] > tol:
                raise ValueError("At index " + str(k) + " mass in class index " + str(t[0]) +
                                 " is " + str(t[2]) + " for base and " + str(tr[2]) + " for table.")

            td = abs(t[3] - tr[3]).sum() / t[3].sum()
            if td > tol:
                raise ValueError("At index " + str(k) + " composition in class index " + str(t[0]) +
                                 " is " + str(t[3]) + " for base and " + str(tr[3]) + " for table.")


#
# Display.
#

width = 20
font_size = 15
class_color = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00",
              "#00FFFF", "#FF00FF", "#808080", "#A52A2A",
              "#FF7F50", "#FFA500", "#FFC0CB", "#000000"] 

fig = disc.Plot2d(width = 20, class_color = class_color, line_width = 5)
fig = redist.Plot2d(fig = fig, line_width = 2)

ax = fig.gca()

N = 10
for i in range(disc.GetNclassTotal()):
    for j in range(N):
        conc = disc.GenerateRandomComposition(i)
        frac = disc.ComputeFraction(conc)

        tab = redist.TestSelf(conc)

        frac_redist = [ disc.ComputeFraction(tab[k][3]) for k in range(len(tab)) ]
        ratio_redist = [ tab[k][2] for k in range(len(tab)) ]

        ax.scatter(frac[0], frac[1], 100, color = "m", marker = "s", zorder = 10)
        for f, r in zip(frac_redist, ratio_redist):
            ax.scatter(f[0], f[1], 100 * r, color = "m", marker = "^", zorder = 10)
            x = [frac[0], f[0]]
            y = [frac[1], f[1]]
            ax.plot(x, y, color = "m", linestyle = "-", linewidth = 1, zorder = 10)

pylab.savefig("test_redist_composition.png")
