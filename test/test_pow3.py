#!/usr/bin/env python

import os, sys
import subprocess
import numpy

if os.path.isfile("test_pow3"):
    os.remove("test_pow3")

if len(sys.argv[1:]) > 0:
    compilation="g++ -o test_pow3 -%s test_pow3.cpp" % sys.argv[1]
else:
    compilation="g++ -o test_pow3 test_pow3.cpp"

print compilation
exit_status = os.system(compilation)
if exit_status > 0:
    sys.exit(exit_status)


N = 30

standard_cpu_time_single = numpy.zeros(N, dtype = numpy.float)
optimize_cpu_time_single = numpy.zeros(N, dtype = numpy.float)
standard_cpu_time_all = numpy.zeros(N, dtype = numpy.float)
optimize_cpu_time_all = numpy.zeros(N, dtype = numpy.float)

for i in range(N):
    print "pass %02i" % (i + 1)
    p = subprocess.Popen("./test_pow3", shell = False, stdout = subprocess.PIPE)

    output = p.communicate()[0].split("\n")

    standard_cpu_time_single[i] = float(output[1].split("=")[1])
    optimize_cpu_time_single[i] = float(output[6].split("=")[1])
    standard_cpu_time_all[i] = float(output[2].split("=")[1])
    optimize_cpu_time_all[i] = float(output[7].split("=")[1])

print "Standard power function :"
print "\tCPU time (single) = %g (%g)" % (standard_cpu_time_single.mean(),
                                         standard_cpu_time_single.std() / standard_cpu_time_single.mean())
print "\tCPU time (all)    = %f (%f)" % (standard_cpu_time_all.mean(),
                                         standard_cpu_time_all.std() / standard_cpu_time_all.mean())
print "Optimized power function :"
print "\tCPU time (single) = %g (%g)" % (optimize_cpu_time_single.mean(),
                                         optimize_cpu_time_single.std() / optimize_cpu_time_single.mean())
print "\tCPU time (all)    = %f (%f)" % (optimize_cpu_time_all.mean(),
                                         optimize_cpu_time_all.std() / optimize_cpu_time_all.mean())


