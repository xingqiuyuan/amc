#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc
from amc.base import AMC
from amc import core as amc_core

import driver
from driver import base
from driver import core as driver_core
from driver import amc as driver_amc
from random import uniform, seed

if driver_core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

if amc_core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

# Init AMC before ... or trouble ahead.
driver_amc.AMX_EulerMixedBaseMoving.Initiate("./test_amc.lua", "default")

# Init initial concentration class.
base.InitialConcentration.Init()

print base.InitialConcentration.GetAvailableTypeList()

cinit = base.InitialConcentration("chimere_sa")

cinit.LoadChimere("./data/out.20081201_20090131_01.nc")

concentration_aer_num, concentration_aer_mass, concentration_gas = cinit.Redistribute(size = "EulerMixed")

# Init standalone amc for display.
AMC.Init("./test_amc.lua", "default")

concentration_aer_num_pretty, concentration_aer_mass_pretty = \
    AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                            legend = [1.37, 1.], margins = {"right" : 0.25},
                            output_file = "./test_driver_ic_number.png")

AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                            percentage = True, legend = [1.37, 1.], margins = {"right" : 0.25},
                            output_file = "./test_driver_ic_number_percentage.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          legend = [1.37, 1.02], margins = {"right" : 0.25},
                          output_file = "./test_driver_ic_mass.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          percentage = True, legend = [1.37, 1.02], margins = {"right" : 0.25},
                          output_file = "./test_driver_ic_mass_percentage.png")

# Test read/write integrity.
cinit.Write("test_ic.bin")
cinit.Read("test_ic.bin")

concentration_aer_num2, concentration_aer_mass2, concentration_gas2 = cinit.Redistribute(size = "EulerMixed")

print abs(concentration_aer_num - concentration_aer_num2).sum()
print abs(concentration_aer_mass - concentration_aer_mass2).sum()
print abs(concentration_gas - concentration_gas2).sum()

# For trajectory test.
Nz = 8
for z in range(Nz):
    cinit.LoadChimere("./data/out.20081201_20090131_01.nc", z)
    cinit.Write("test_trajectory_ic_%0d.bin" % z)
