#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


from amc.species import Species, SpeciesEquilibrium, Phase, SpeciesTracer
from amc import core
from ops import Ops

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)

ops.SetPrefix("default.")
SpeciesEquilibrium.Init(ops)

ops.SetPrefix("default.")
Phase.Init(ops)

print Species.GetSpeciesList()
print SpeciesTracer.GetTracerList()
print SpeciesEquilibrium.GetSpeciesList()

print "Nspecies =", Species.GetNspecies()
print "Ngas =", Species.GetNgas()
print "Ntracer =", SpeciesTracer.GetNtracer()
print "Not semivolatile =", Species.GetNotSemivolatile()
print "NspeciesEquilibrium =", SpeciesEquilibrium.GetNspecies()
print "Nphase =", Phase.GetNphase()

for i in range(Phase.GetNphase()):
    print "Species list for phase \"%s\" :" % Phase.GetName(i), Phase.GetSpecies(i)

