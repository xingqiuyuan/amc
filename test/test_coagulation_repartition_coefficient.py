#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import AMC, AMCtable, \
    CoefficientRepartitionCoagulationMovingBase, \
    CoefficientRepartitionCoagulationMovingTable, \
    CoefficientRepartitionCoagulationStaticBase, \
    CoefficientRepartitionCoagulationStaticTable, \
    set_random_generator, core

if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

import os

if os.path.isfile("./coagulation_coefficient.nc"):
    os.remove("./coagulation_coefficient.nc")


# AMC.
AMC.Init("./test_amc.lua", "default4")

# Config.
from ops import Ops
ops = Ops("./test_amc.lua")
ops.SetPrefix("default4.dynamic.coagulation.coefficient.")

# Random generator.
set_random_generator(0)

#
# Static
#

coef_static = CoefficientRepartitionCoagulationStaticBase(ops)

precision = coef_static.ComputeCoefficient(0.001)

print precision

a = coef_static.GetCoefficient()

coef_static.Write()

coef_static_file = CoefficientRepartitionCoagulationStaticBase(ops)

b = coef_static_file.GetCoefficient()

if len(a) != len(b):
    raise ValueError("Written and read coefficients have not the same length.")

for i in range(len(a)):
    for j in range(3):
        if a[i][j] != b[i][j]:
            print a[i][j], b[i][j]
            raise ValueError("For couple " + str(i) + ", written and read coefficients seems not equal.")

#
# Moving.
#

if os.path.isfile("./coagulation_coefficient.nc"):
    os.remove("./coagulation_coefficient.nc")

coef_moving = CoefficientRepartitionCoagulationMovingBase(ops)

precision = coef_moving.ComputeCoefficient(0.001)

print precision

a = coef_moving.GetCoefficient()

coef_moving.Write()

coef_moving_file = CoefficientRepartitionCoagulationMovingBase(ops)

b = coef_moving_file.GetCoefficient()

if len(a) != len(b):
    raise ValueError("Written and read coefficients have not the same length.")

for i in range(len(a)):
    for j in range(3):
        if a[i][j] != b[i][j]:
            print a[i][j], b[i][j]
            raise ValueError("For couple " + str(i) + ", written and read coefficients seems not equal.")

print "Test OK."
