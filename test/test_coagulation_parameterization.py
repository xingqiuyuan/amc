#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc.coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander, \
    ParameterizationCoagulationTurbulent, ParameterizationCoagulationBrownian
from amc import core

if core.AMC_HAS_WAALS_VISCOUS == 1:
    from amc.coagulation import ParameterizationCoagulationBrownianWaalsViscous

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

from ops import Ops
import time, numpy
import os, sys
import pylab
from matplotlib import ticker, cm
from matplotlib.colors import LogNorm

font_size = 12
width = 10
margins = [0.1, 0.9, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 4
pylab.matplotlib.rcParams["xtick.minor.size"] = 2
pylab.matplotlib.rcParams["ytick.major.size"] = 4
pylab.matplotlib.rcParams["ytick.minor.size"] = 2


# AMC.
if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

temperature = 300.
pressure = 1.01325e5
rh = 0.7

#
# Brownian parameterization.
#


param = ParameterizationCoagulationBrownian()
param.SetKnudsenMin(0)
param.SetKnudsenMax(10000)
param.Plot2d(temperature = temperature,
             pressure = pressure,
             title = "Brownian kernel for two particles, T=%3.0fK" % temperature + ", P=%1.3gPa" % pressure,
             output_file = "coag_param_brownian.png")

#
diam1, diam2, kernel_transition = param.TestKernel(temperature = temperature, pressure = pressure)

param.SetKnudsenMin(10000)
diam1, diam2, kernel_continuous = param.TestKernel(temperature = temperature, pressure = pressure)

param.SetKnudsenMin(0)
param.SetKnudsenMax(0)
diam1, diam2, kernel_free_molecular = param.TestKernel(temperature = temperature, pressure = pressure, airfmp = 0)

airfmp = core.ParameterizationPhysics.ComputeAirFreeMeanPath(temperature, pressure)
knudsen = 2. * airfmp / diam1

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)
ax.loglog(knudsen, kernel_transition.diagonal(), "c-", label = "transition", lw = 5)
ax.loglog(knudsen, kernel_continuous.diagonal(), "r-", label = "continuous", lw = 2)
ax.loglog(knudsen, kernel_free_molecular.diagonal(), "g-", label = "free molecular", lw = 2)
ax.set_xlim(knudsen.min(), knudsen.max())
ax.set_xlabel(ur"Knudsen number")
ax.set_ylabel(ur"Brownian kernel [$m^3.s^{-1}$]")
ax.set_title("Brownian kernel for two particles with equal diameters.")
ax.legend(loc = 2)
pylab.savefig("coag_param_brownian2.png")

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)
ax.loglog(diam1, kernel_transition.diagonal() * 1.e6, "c-", label = "transition", lw = 5)
ax.loglog(diam1, kernel_continuous.diagonal() * 1.e6, "r-", label = "continuous", lw = 2)
ax.loglog(diam1, kernel_free_molecular.diagonal() * 1.e6, "g-", label = "free molecular", lw = 2)
ax.set_xlim(diam1.min(), diam1.max())
ax.set_xlabel(ur"Particle diameter [$\mu m$]")
ax.set_ylabel(ur"Brownian kernel [$cm^3.s^{-1}$]")
ax.set_title("Brownian kernel for two particles with equal diameters.")
ax.legend(loc = 2)
pylab.savefig("coag_param_brownian3.png")


#
# Brownian Waals viscous parameterization.
#


param = ParameterizationCoagulationBrownianWaalsViscous()
param.SetHamaker(core.AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * 200.)
#param.ToggleViscousEffect()

param.Plot2d(temperature = temperature,
             pressure = pressure,
             title = "Waals/Viscous Brownian kernel for two particles, T=%3.1fK" % temperature + \
             ", P=%1.3gPa" % pressure + ", A/kT=%1.2g" % param.GetHamakerRelative(temperature),
             output_file = "coag_param_waals_viscous_brownian.png")

#

diameter, correction_factor_viscous = param.TestCorrectionFactor(temperature = temperature, pressure = pressure)
param.ToggleViscousEffect()
diameter, correction_factor = param.TestCorrectionFactor(temperature = temperature, pressure = pressure)

param.SetHamaker(core.AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * 20.)

param.ToggleViscousEffect()
diameter, correction_factor2_viscous = param.TestCorrectionFactor(temperature = temperature, pressure = pressure)
param.ToggleViscousEffect()
diameter, correction_factor2 = param.TestCorrectionFactor(temperature = temperature, pressure = pressure)

knudsen = numpy.zeros(len(diameter), dtype = numpy.float)
for i in range(len(diameter)):
    knudsen[i] = core.ParameterizationPhysics.ComputeParticleKnudsenNumberAllen(diameter[i], diameter[i], temperature, pressure)

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

ax.semilogx(knudsen, correction_factor_viscous, "r-", label = "A/kT=200, WITH viscous effect", lw = 2)
ax.semilogx(knudsen, correction_factor, "b-", label = "A/kT=200, withOUT viscous effect", lw = 2)
ax.semilogx(knudsen, correction_factor2_viscous, "r-.", label = "A/kT=20, WITH viscous effect", lw = 2)
ax.semilogx(knudsen, correction_factor2, "b-.", label = "A/kT=20, withOUT viscous effect", lw = 2)
ax.set_xlabel("Particle Knudsen number")
ax.set_ylabel("Correction factor")
ax.set_xlim(knudsen.min(), knudsen.max())
ax.set_ylim(0, numpy.ceil(correction_factor.max()))
ax.set_title(ur"Correction factor for min and max  Hamaker value, T = 300K, P = 1atm, $\rho_p = 1 g.cm^{-3}$")
ax.legend(loc = 2)
pylab.savefig("coag_param_waals_viscous_brownian_correction_factor.png")


#
# Gravitational settling parameterization.
#


param = ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander()

param.Plot2d(temperature = temperature,
             pressure = pressure,
             rh = rh,
             dmin1 = 0.1,
             dmin2 = 0.1,
             title = "Gravitational kernel for two particles, T=%3.1fK" % temperature \
             + ", P=%1.3gPa" % pressure + ", RH=%1.1f" % rh,
             output_file = "coag_param_gravitational.png")


#
# Turbulent diffusion parameterization.
#

eddy_dissipation_rate = 5.e-4
param = ParameterizationCoagulationTurbulent()

param.Plot2d(temperature = temperature,
             pressure = pressure,
             rh = rh,
             eddy_dissipation_rate = eddy_dissipation_rate,
             title = "Turbulent kernel for two particles, T=%3.1fK" % temperature + ", P=%1.3gPa" % pressure \
             + ", RH=%1.1f" % rh + ", E=%1.1gm2s-3" % eddy_dissipation_rate,
             output_file = "coag_param_turbulent.png")


#
# All kernels.
#

diameter1 = 2.
dmin2 = 2.e-1
dmax2 = 20.

param = ParameterizationCoagulationBrownian()
fig = param.Plot1d(diameter1 = diameter1, dmin2 = dmin2, dmax2 = dmax2, fmt = "b-")

param = ParameterizationCoagulationBrownianWaalsViscous()
param.SetHamaker(core.AMC_COAGULATION_BROWNIAN_BOLTZMANN_CONSTANT * temperature * 200.)
fig = param.Plot1d(fig = fig, diameter1 = diameter1, dmin2 = dmin2, dmax2 = dmax2, fmt = "r-",
                   label = "Brownian Waals/Viscous, A/kT=%1.2g" % param.GetHamakerRelative(temperature))

param = ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander()

fig = param.Plot1d(fig = fig, diameter1 = diameter1, dmin2 = dmin2, dmax2 = dmax2, fmt = "g-",
                   label = "Gravitational with Friedlander collision efficiency")

param = ParameterizationCoagulationTurbulent()
fig = param.Plot1d(fig = fig, diameter1 = diameter1, dmin2 = dmin2, dmax2 = dmax2, fmt = "m-", legend = 2)

ax = fig.gca()
ax.set_ylim(1.e-18, 1.e-13)
fig.savefig("coag_param_comparison.png")

