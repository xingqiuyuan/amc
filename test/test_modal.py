#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import modal
import pylab, numpy

pylab.clf()

dist_name_list = modal.GetAvailableDistribution()
fmt_list = ["k-", "g-", "r-", "c-", "m-", "y-"]

#
#
#

fig = None

i = 1
for dist_name, fmt in zip(dist_name_list, fmt_list):
    print i, dist_name
    dist = modal.ModalDistribution(dist_name)
    fig = dist.Plot(fig, fmt = fmt, legend = 1, lw = 2)
    i += 1

ax = fig.gca()
ax.set_title("Available modal distributions. Number density.")
fig.savefig("test_modal_number.png")

#
#
#

fig = None

i = 1
for dist_name, fmt in zip(dist_name_list, fmt_list):
    print i, dist_name
    dist = modal.ModalDistribution(dist_name)
    fig = dist.Plot(fig, legend = 2, volume = True, fmt = fmt, lw = 2)
    i += 1

ax = fig.gca()
ax.set_title("Available modal distributions. Volume density.")
fig.savefig("test_modal_volume.png")

#
#
#

fig = None

i = 1
for dist_name, fmt in zip(dist_name_list, fmt_list):
    print i, dist_name
    dist = modal.ModalDistribution(dist_name)
    fig = dist.Plot(fig, legend = 2, volume = True, log = True, fmt = fmt, lw = 2)
    i += 1

ax = fig.gca()
ax.set_title("Available modal distributions. Logarithm volume density.")
fig.savefig("test_modal_volume_log.png")

#
#
#

fig = None

i = 1
for dist_name, fmt in zip(dist_name_list, fmt_list):
    print i, dist_name
    dist = modal.ModalDistribution(dist_name)
    fig = dist.Plot(fig, fmt = fmt, lw = 2, legend = [1.3, 1], log = True)
    i += 1

ax = fig.gca()
ax.set_title("Available modal distributions. Logarithm number density.")
ax.set_aspect(0.3, "box")
fig.savefig("test_modal_number_log.png")

#
#
#

dmin = 0.001
dmax = 10.
Nb = 20
h = numpy.log(dmax / dmin) / float(Nb)

for dist_name in dist_name_list:
    print dist_name

    dist = modal.ModalDistribution(dist_name)
    diam_mean, dist_num, dist_vol, diam_bound = dist.ComputeSectionalDistributionRegular(dmin, dmax, Nb)

    fig = None
    fig = dist.Plot(fig, fmt = "k-", lw = 2, log = True)

    ax = fig.gca()
    ax.loglog(diam_mean, dist_num / h, "bo")
    for diam in diam_bound:
        ax.axvline(diam, ls = "-.", color = "k")

    ax.set_title("Number density. " + dist.GetName())
    fig.savefig("test_modal_" + dist.GetName() + "_number_log.png")

    fig = None
    fig = dist.Plot(fig, volume = True, fmt = "k-", lw = 2)

    ax = fig.gca()
    ax.semilogx(diam_mean, dist_vol / h, "b-.o")
    for diam in diam_bound:
        ax.axvline(diam, ls = "-.", color = "k")

    ax.set_title("Volume density. " + dist.GetName())
    fig.savefig("test_modal_" + dist.GetName() + "_volume.png")
