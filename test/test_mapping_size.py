#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from driver import amc, core
from driver.mapping import MappingSize, MappingSizeTest
from driver.util import TimerCPU
from ops import Ops
from matplotlib.backends.backend_pdf import PdfPages
import pylab, numpy, datetime

if core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

#
# AMC.
#

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
amc.DiscretizationSize.Init(ops)
ops.Close()


#
# Mapping.
#

MappingSize.Init()

ops = Ops("./test_driver.lua")
ops.SetPrefix("mapping.")
mapsize = MappingSizeTest(ops, "test_size", True)
ops.Close()

diameter_bound_amc = amc.DiscretizationSize.GetDiameterBound()
Nsection_amc = amc.DiscretizationSize.GetNsection()
Nbound_amc = Nsection_amc + 1

print diameter_bound_amc, Nsection_amc

seed = 0
numpy.random.seed(seed)

N = 100
with PdfPages('test_mapping_size.pdf') as pdf:
    for h in range(N):
        i = numpy.random.randint(0, Nsection_amc - 1)
        j = numpy.random.randint(i + 1, Nsection_amc)

        dmin = numpy.random.uniform(diameter_bound_amc[i], diameter_bound_amc[i + 1])
        dmax = numpy.random.uniform(diameter_bound_amc[j], diameter_bound_amc[j + 1])
        dmean = numpy.exp(numpy.random.uniform(numpy.log(dmin), numpy.log(dmax)))

        print "%d : %d %d : [%1.3g, %1.3g] (%1.3g)" % (h, i, j, dmin, dmax, dmean)
        fig = mapsize.Plot(dmin, dmax, dmean)
        pdf.savefig(fig)
        pylab.close(fig)

    d = pdf.infodict()
    d['Title'] = 'Illustration of mapping size test.'
    d['Author'] = u'Edouard Debry'
    d['Subject'] = 'Random sections mapped on AMC size discretization (one per page).'
    d['Keywords'] = 'Mapping size section CHIMERE'
    d['CreationDate'] = datetime.datetime.today()
    d['ModDate'] = datetime.datetime.today()


#MappingSizeTest.Run()

