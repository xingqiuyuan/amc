#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import os, sys, numpy, pylab

from amc.repartition_coefficient import Ring
from amc import core

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU

#
# Test ring cross volume computation.
#

b = Ring(4, 7)

Ring.Test(b)

#
# Case probability.
#

case_probability = Ring.GetCaseCounterTest() / float(Ring.GetCaseCounterTest().sum()) * 100.

pylab.clf()

pylab.matplotlib.rcParams["figure.figsize"] = (16, 8)
pylab.matplotlib.rcParams["figure.subplot.left"] = 0.05
pylab.matplotlib.rcParams["figure.subplot.right"] = 0.99
pylab.matplotlib.rcParams["figure.subplot.bottom"] = 0.1
pylab.matplotlib.rcParams["figure.subplot.top"] = 0.9

fig = pylab.figure()
ax = fig.gca()

x = range(len(case_probability))
ax.bar(x, case_probability)

ax.set_xlim(0, len(x))

xloc = range(0, len(x), 2)
xlabel = ["%d" % y for y in xloc]
ax.set_xticks(xloc)
ax.set_xticklabels(xlabel)

ax.set_ylim(0., case_probability.max() * 1.1)
ax.set_xlabel("Case index")
ax.set_ylabel("Probability in %")
ax.set_title("Ring case occurence probability, refer to \"ClassRing::compute_cross_case(...)\" for an explanation of all cases.")
fig.savefig("ring_case_probability.png")

os.system("display ./ring_case_probability.png")
