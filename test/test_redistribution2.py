#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core, modal
from amc.base import AMC
from amc.discretization import DiscretizationSize, \
    DiscretizationCompositionBase, DiscretizationCompositionTable
from amc.util import set_random_generator, generate_random_vector
from amc.redistribution import \
    RedistributionClassBase, RedistributionClassTable, \
    RedistributionClassBaseTable, RedistributionClassTableBase, \
    RedistributionSizeMovingCompositionBase, \
    RedistributionSizeMovingCompositionBaseTable, \
    RedistributionSizeMovingCompositionTable, \
    RedistributionSizeHybridCompositionBase, \
    RedistributionSizeHybridCompositionBaseTable, \
    RedistributionSizeHybridCompositionTable, \
    RedistributionSizeHybridTableCompositionBase, \
    RedistributionSizeHybridTableCompositionBaseTable, \
    RedistributionSizeHybridTableCompositionTable, \
    RedistributionSizeMixedCompositionBase, \
    RedistributionSizeMixedCompositionBaseTable, \
    RedistributionSizeMixedCompositionTable, \
    RedistributionSizeMixedTableCompositionBase, \
    RedistributionSizeMixedTableCompositionBaseTable, \
    RedistributionSizeMixedTableCompositionTable

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerAMC

from ops import Ops
import time, numpy
import random
import os, sys
import pylab

if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

AMC.Init("./test_amc.lua", "default")

ops = Ops("./test_amc.lua")
RedistributionSizeMovingCompositionBase.Init(ops)
RedistributionSizeMixedCompositionBase.Init(ops)


# Distribution.
dist = modal.ModalDistribution("urban_eurotrack")

set_random_generator(0)
concentration_aer_num, concentration_aer_mass, diameter_mean = AMC.GenerateModalDistributionRandom(dist)

concentration_aer_num_beg, concentration_aer_mass_beg = \
    AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

concentration_aer_number_im_beg, \
    concentration_aer_mass_im_beg = AMC.ComputeConcentrationIM(concentration_aer_num,
                                                               concentration_aer_mass)
concentration_aer_mass_im_total_beg = concentration_aer_mass_im_beg.sum(1)

diameter_beg = [numpy.power(m * core.AMC_INV_PI6 / (n * DiscretizationSize.GetDensityFixed()), core.AMC_FRAC3)
                for m, n in zip(concentration_aer_mass_im_total_beg, concentration_aer_number_im_beg)]


# Redistribute.
section_min = 0
section_max = AMC.GetNg()
#RedistributionSizeMovingCompositionBase.Redistribute(section_min, section_max,
#                                                     concentration_aer_num,
#                                                     concentration_aer_mass)

RedistributionSizeMixedCompositionBase.Redistribute(section_min, section_max,
                                                     concentration_aer_num,
                                                     concentration_aer_mass)

concentration_aer_num_end, concentration_aer_mass_end = \
    AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

concentration_aer_number_im_end, \
    concentration_aer_mass_im_end = AMC.ComputeConcentrationIM(concentration_aer_num,
                                                               concentration_aer_mass)
concentration_aer_mass_im_total_end = concentration_aer_mass_im_end.sum(1)

diameter_end = [numpy.power(m * core.AMC_INV_PI6 / (n * DiscretizationSize.GetDensityFixed()), core.AMC_FRAC3)
                for m, n in zip(concentration_aer_mass_im_total_end, concentration_aer_number_im_end)]

# Display.
width = 20
font_size = 15
class_color = ["#FF0000", "#00FF00", "#0000FF", "#FFFF00",
              "#00FFFF", "#FF00FF", "#808080", "#A52A2A",
              "#FF7F50", "#FFA500", "#FFC0CB", "#000000"]
point_size = 40

# Composition.
ops.SetPrefix("default.discretization.composition.");
for i in range(DiscretizationSize.GetNsection()):
    disc_name = AMC.GetDiscretizationComposition(i).GetName()
    print i, disc_name

    if disc_name != "internal_mixing":
        disc = DiscretizationCompositionBase(None, disc_name, ops)

        title = "Size section %d, discretization \"%s\"." % (i, disc_name)
        pylab.clf()
        if disc.GetNdim() == 1:
            fig = disc.Plot1d(class_color = class_color, width = width, font_size = font_size, title = title)
            ax = fig.gca()
            for j in range(disc.GetNclassTotal()):
                f = disc.ComputeFraction(concentration_aer_mass_beg[i][j] / concentration_aer_mass_beg[i][j].sum())
                ax.scatter(0.2, f[0], point_size, marker = "s", color = "k", zorder = 10)
                f = disc.ComputeFraction(concentration_aer_mass_end[i][j] / concentration_aer_mass_end[i][j].sum())
                point_size_redist = point_size * concentration_aer_mass_end[i][j].sum() / concentration_aer_mass_beg[i][j].sum()
                ax.scatter(0.3, f[0], point_size_redist, marker = "o", color = "k", zorder = 10)

        else:
            fig = disc.Plot2d(class_color = class_color, width = width, font_size = font_size,
                              title = title, point_size = 0)
            ax = fig.gca()
            for j in range(disc.GetNclassTotal()):
                f = disc.ComputeFraction(concentration_aer_mass_beg[i][j] / concentration_aer_mass_beg[i][j].sum())
                ax.scatter(f[0], f[1], point_size, marker = "s", color = "k", zorder = 10)
                f = disc.ComputeFraction(concentration_aer_mass_end[i][j] / concentration_aer_mass_end[i][j].sum())
                point_size_redist = point_size * concentration_aer_mass_end[i][j].sum() / concentration_aer_mass_beg[i][j].sum()
                ax.scatter(f[0], f[1], point_size_redist, marker = "o", color = "k", zorder = 10)

        pylab.savefig("test_redist_%d_%s.png" % (i, disc_name))

ops.Close()

# Size.
diameter_bound = DiscretizationSize.GetDiameterBoundList()

width = 26 / 2.54
margins = [0.1, 0.9, 0.1, 0.9]
pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

font_size = 14
pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

pylab.clf()
fig = pylab.figure()
ax1 = fig.add_subplot(121)

ax1.loglog(diameter_beg, concentration_aer_number_im_beg, "ks-", label = "beg", lw = 1)
ax1.loglog(diameter_end, concentration_aer_number_im_end, "mo-", label = "end", lw = 1)
for diam in diameter_bound:
    ax1.axvline(diam, ls = "-.", color = "k")
ax1.legend(loc = 1)
ax1.set_xlabel(ur"diameter $d_p$")
ax1.set_ylabel(ur"$\#.m^{-3}$")
ax1.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
ax1.set_title("Number distribution.")

ax2 = fig.add_subplot(122)

ax2.semilogx(diameter_beg, concentration_aer_mass_im_total_beg, "ks-", label = "beg", lw = 1)
ax2.semilogx(diameter_end, concentration_aer_mass_im_total_end, "mo-", label = "end", lw = 1)
for diam in diameter_bound:
    ax2.axvline(diam, ls = "-.", color = "k")
ax2.legend(loc = 2)
ax2.set_xlabel(ur"diameter $d_p$")
ax2.set_ylabel(ur"$\mu g.m^{-3}$")
ax2.set_xlim(diameter_bound[0] * 0.999, diameter_bound[-1] * 1.001)
ax2.set_title("Mass distribution.")

pylab.figtext(0.45, 0.97, dist.GetName(), size = 18)

fig.savefig("./test_redist_size.png")


# Check concentrations.
for i in range(DiscretizationSize.GetNsection()):
    disc_ptr = AMC.GetDiscretizationComposition(i)
    print i, disc_ptr.GetName(), disc_ptr.GetNclassTotal()

    for j in range(disc_ptr.GetNclassTotal()):
        k, on_edge = disc_ptr.FindClassCompositionIndex(concentration_aer_mass_end[i][j] / concentration_aer_mass_end[i][j].sum())


print concentration_aer_mass_im_total_beg.sum(), concentration_aer_mass_im_total_end.sum()
