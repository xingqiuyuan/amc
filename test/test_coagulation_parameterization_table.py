#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core
from amc.coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander, \
    ParameterizationCoagulationTurbulent, ParameterizationCoagulationBrownian, \
    ParameterizationCoagulationTable

if core.AMC_HAS_WAALS_VISCOUS == 1:
    from amc.coagulation import ParameterizationCoagulationBrownianWaalsViscous

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

from ops import Ops
import time, numpy
import os, sys
import pylab

# AMC.
if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

temperature = 300.
pressure = 1.01325e5
rh = 0.7


#
# Brownian.
#


ops = Ops("./test_amc.lua")
ops.SetPrefix("default3.table_brownian.")

param = ParameterizationCoagulationBrownian()

# Do not read table from path as we want to compute it.
param_table = ParameterizationCoagulationTable(ops, False)

param_table.Compute()
param_table.Write()

param_table2 = ParameterizationCoagulationTable(ops)

start = time.clock()
d1, d2, kernel = param.TestKernel(temperature = temperature,
                                  pressure = pressure)                          
end = time.clock()
cpu_time_param = (end - start)

start = time.clock()
d1, d2, kernel_table = param_table.TestKernel(temperature = temperature,
                                              pressure = pressure)                          
end = time.clock()
cpu_time_param_table = (end - start)

start = time.clock()
d1, d2, kernel_table2 = param_table2.TestKernel(temperature = temperature,
                                                pressure = pressure)                          
end = time.clock()
cpu_time_param_table2 = (end - start)

print cpu_time_param, cpu_time_param_table, cpu_time_param_table2


#
# Brownian Waals Viscous.
#


ops.SetPrefix("default3.table_brownian_waals_viscous.")

param = ParameterizationCoagulationBrownianWaalsViscous()

# Do not read table from path as we want to compute it.
param_table = ParameterizationCoagulationTable(ops, False)

param_table.Compute()
param_table.Write()

param_table2 = ParameterizationCoagulationTable(ops)

start = time.clock()
d1, d2, kernel = param.TestKernel(temperature = temperature,
                                  pressure = pressure)                          
end = time.clock()
cpu_time_param = (end - start)

start = time.clock()
d1, d2, kernel_table = param_table.TestKernel(temperature = temperature,
                                              pressure = pressure)                          
end = time.clock()
cpu_time_param_table = (end - start)

start = time.clock()
d1, d2, kernel_table2 = param_table2.TestKernel(temperature = temperature,
                                                pressure = pressure)                          
end = time.clock()
cpu_time_param_table2 = (end - start)

print cpu_time_param, cpu_time_param_table, cpu_time_param_table2
