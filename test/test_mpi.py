#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc_mpi.base import AMC
from amc_mpi import core

# Init MPI on python side, not doing this may cause the error :
# python: symbol lookup error: /usr/lib/openmpi/lib/openmpi/mca_paffinity_linux.so:
# undefined symbol: mca_base_param_reg_int
#
from mpi4py import MPI

# Init logger, which also use MPI, so it must be called after MPI initialization.
if core.AMC_HAS_LOGGER > 0:
    from amc_mpi.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

# Init MPI on C++ side : this is not really MPI initialization, it just set the rank_ and Nrank_ value.
# But it does MPI initialization if not done before (in full C++ programs).
AMC.MPI_Init()

rank = AMC.GetRank()
Nrank = AMC.GetNrank()
print rank, Nrank

comm = MPI.COMM_WORLD
print comm.Get_rank(), comm.Get_size()
