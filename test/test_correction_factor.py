#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops

from amc import CondensationCorrectionFactorDahneke, \
    CondensationCorrectionFactorFuchsSutugin, Species, core, \
    ParameterizationPhysics

font_size = 12
width = 10
margins = [0.1, 0.9, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1


if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)

fuchs = CondensationCorrectionFactorFuchsSutugin()

dahn = CondensationCorrectionFactorDahneke()

dmin = 0.001
dmax = 10.0
q = dmax / dmin
n = 1000
diameter = [dmin * numpy.power(q, float(i) / float(n - 1)) for i in range(n)]
temperature = 300.
pressure = 1.013e5
accomodation = 1.0
species = ["H2SO4", "NH3", "BiA2D"]
color = ["r", "g", "b"]


pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

for spec, c in zip(species, color):
    idx = Species.GetIndex(spec)

    collision_factor = Species.GetProperty(idx, "collision_factor")
    molecular_diameter = Species.GetProperty(idx, "molecular_diameter")
    # From µg.mol-1 to kg.mol-1
    molar_mass = Species.GetProperty(idx, "molar_mass") * 1.e-9

    gasdiff = ParameterizationPhysics.ComputeGasDiffusivity(temperature,
                                                            pressure,
                                                            collision_factor,
                                                            molecular_diameter,
                                                            molar_mass)

    gasvqmg = ParameterizationPhysics.ComputeGasQuadraticMeanVelocity(temperature, molar_mass)

    # From meter to micrometer.
    fuchs_fmp = fuchs.FreeMeanPath(gasdiff, gasvqmg) * 1.e6
    dahn_fmp = dahn.FreeMeanPath(gasdiff, gasvqmg) * 1.e6

    fuchs_corr = [fuchs.CorrectionFactor(2. * fuchs_fmp / d, accomodation) for d in diameter]
    dahn_corr = [fuchs.CorrectionFactor(2. * dahn_fmp / d, accomodation) for d in diameter]

    ax.semilogx(diameter, fuchs_corr, c + "-", lw = 2, label = spec + " " + fuchs.GetName())
    ax.semilogx(diameter, dahn_corr, c + "-.", lw = 2, label = spec + " " + dahn.GetName())

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("correction factor [adim]")
ax.set_title("Transition regime correction factor for various species, accomodation = 1, temperature = 300 K.")
ax.legend(loc = 2)
pylab.savefig("test_correction_factor.png")
