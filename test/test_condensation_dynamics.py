#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc import AMC, AMCtable, \
    DiscretizationSize, Species, \
    DynamicsCondensationBase, AerosolData, \
    MeteorologicalData, modal, core, ArrayReal, \
    set_random_generator, \
    NumericalSolverCondensationBase

if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC

set_random_generator(0)

# AMC.
AMC.Init("./test_amc.lua", "default")

# Meteo
MeteorologicalData.Init()

# Condensation.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default")
DynamicsCondensationBase.Init(ops)

# Update.
MeteorologicalData.UpdateMeteo()
MeteorologicalData.UpdateCondensation()

data_meteo = MeteorologicalData.GetMeteo()
data_cond = MeteorologicalData.GetCondensation()

# Initial distribution.
#dist = modal.ModalDistribution("urban_eurotrack")
dist = modal.ModalDistribution("seigneur_1986_hazy")

concentration_aer_num, concentration_aer_mass, diameter_mean = AMC.GenerateModalDistributionRandom(dist)

# Remove MGLY concentration because too much volatile : give it to glyoxal.
idx_mgly = Species.GetIndex("MGLY")
idx_gly = Species.GetIndex("GLY")

for i in range(AMC.GetNg()):
    j = i * Species.GetNspecies()
    concentration_aer_mass[j + idx_gly] += concentration_aer_mass[j + idx_mgly]
    concentration_aer_mass[j + idx_mgly] = float(0)


concentration_gas = ArrayReal(Species.GetNgas())

concentration_gas[Species.GetIndexGas("H2SO4")]  = 1.
concentration_gas[Species.GetIndexGas("NH3")]    = 1.
concentration_gas[Species.GetIndexGas("HNO3")]   = 0.5
concentration_gas[Species.GetIndexGas("HCl")]    = 0.5
concentration_gas[Species.GetIndexGas("BiA2D")]  = 0.2
concentration_gas[Species.GetIndexGas("BiA1D")]  = 0.2
concentration_gas[Species.GetIndexGas("BiA0D")]  = 0.2
concentration_gas[Species.GetIndexGas("GLY")]    = 0.2
concentration_gas[Species.GetIndexGas("MGLY")]   = 0.2
concentration_gas[Species.GetIndexGas("AnBlP")]  = 0.2
concentration_gas[Species.GetIndexGas("AnBmP")]  = 0.2
concentration_gas[Species.GetIndexGas("BiBlP")]  = 0.2
concentration_gas[Species.GetIndexGas("BiBmP")]  = 0.2
concentration_gas[Species.GetIndexGas("BiISO1")] = 0.2
concentration_gas[Species.GetIndexGas("BiISO2")] = 0.2
concentration_gas[Species.GetIndexGas("C16")]    = 0.2
concentration_gas[Species.GetIndexGas("C19")]    = 0.2
concentration_gas[Species.GetIndexGas("C25")]    = 0.2


# Time loop.
deltat = 0.001
deltatshow = 1.
deltatfig = 1.
t = 0.
tshow = 0.
tfig = 0.
tend = 120.

concentration_gas_evolution = []

mass_convervation_beg = AMC.ComputeMassConservation(concentration_gas, concentration_aer_mass)

ifig = 0
while t < tend:

    # Aerosol thermodynamics.
    AMC.ComputeThermodynamics(concentration_gas, concentration_aer_num, concentration_aer_mass)

    #print AerosolData.GetEquilibriumGasSurface()
    #print AerosolData.GetEquilibriumAerosolInternal()
    #print AerosolData.GetLiquidWaterContent()

    # Compute particle diameter.
    AMC.ComputeParticleDiameter(concentration_aer_num, concentration_aer_mass)

    #print AerosolData.GetDiameter()

    if t >= tshow:
        print t
        concentration_gas_evolution.append(AMC.GetConcentrationGasPretty(concentration_gas))
        tshow += deltatshow

    if t >= tfig:
        concentration_aer_num_pretty, concentration_aer_mass_pretty = \
            AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

        composition = AMC.ComputeParticleComposition(concentration_aer_num, concentration_aer_mass)

        fig = AMC.PlotParticle(concentration_aer_num, concentration_aer_mass)

        fig.text(0.45, 0.93, "t = %04d s" % int(t), fontsize = 14)
        fig.savefig("test_cond_particle_%04d.png" % ifig)

        fig = AMC.PlotComposition(composition, autopct = '', labels = None, legend_ncol = 2,
                                  legend_bbox_to_anchor = [3.5, 0.9], text_size = 10)


        fig.text(0.45, 0.98, "t = %04d s" % int(t), fontsize = 14)
        fig.savefig("test_cond_comp_%04d.png" % ifig)

        tfig += deltatfig
        ifig += 1

    #rate_gas = core.vector1r(Species.GetNgas(), 0.)
    #rate_aer_mass = core.vector1r(AMC.GetNgNspecies(), 0.)

    # Lagrangian rate.
    #DynamicsCondensationBase.RateLagrangian(concentration_aer_num, concentration_aer_mass,
    #                                        concentration_gas, rate_aer_mass, rate_gas)

    # Time scales.
    #time_scale_aer = DynamicsCondensationBase.ComputeTimeScaleAerosolMass(concentration_aer_mass, rate_aer_mass)
    #time_scale_gas = DynamicsCondensationBase.ComputeTimeScaleGas(concentration_gas, rate_gas)

    deltat_effective = NumericalSolverCondensationBase.LagrangianExplicit(deltat, concentration_aer_num,
                                                                          concentration_aer_mass, concentration_gas)

    t += deltat_effective

# End.


# Mass conservation.
mass_convervation_end = AMC.ComputeMassConservation(concentration_gas, concentration_aer_mass)

print "beg", mass_convervation_beg
print "end", mass_convervation_end

# Gas time evolution.

width = 24
width /= 2.54

margins = [0.1, 0.75, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

font_size = 12

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1

fig = pylab.figure()
ax = fig.add_subplot(111)

concentration_gas_evolution = numpy.array(concentration_gas_evolution, dtype = numpy.float)

semivolatile = Species.GetSemivolatile()

for i in range(Species.GetNgas()):
    ax.plot(concentration_gas_evolution[:,i], "-s", label = Species.GetName(semivolatile[i]))

ax.set_xlabel(ur"time [s]")
ax.set_ylabel(ur"gas concentration  $[\mu g.m^{-3}]$")
ax.set_title("Gas concentration evolution.")
ax.legend(bbox_to_anchor = [1.3, 1])

fig.savefig("test_cond_gas_evolution.png")
