#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import os, sys, numpy, pylab

from amc.discretization import DiscretizationSize
from amc.repartition import PDF
from amc import core

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc.util import TimerCPU


#
# AMC.
#

from ops import Ops
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
DiscretizationSize.Init(ops)

#
# PDF.
#

pdf = PDF()

# Static data.
print "Npdf =", PDF.GetNpdf()
print "Ncase =", PDF.GetNcase()
print "Nstep =", PDF.GetNstep()
print "pdf =", PDF.GetPdfName()
print "case =", PDF.GetCaseName()
print "step =", PDF.GetStepName()

for i in range(PDF.GetNpdf()):
    print PDF.GetCasePdfIndex(i)

for i in range(PDF.GetNcase()):
    print PDF.GetCaseStepIndex(i)


pdf.Plot(i = 5, j = 3, fi = 0.5, fj = 0.5, legend = 2, output_file = "test_pdf_01.png")
pdf.Plot(i = 5, j = 3, fi = 0.3, fj = 0.7, legend = 2, output_file = "test_pdf_02.png")
pdf.Plot(i = 5, j = 3, fi = 0.7, fj = 0.3, legend = 2, output_file = "test_pdf_03.png")
pdf.Plot(i = 5, j = 3, fi = 0.2, fj = 0.8, legend = 2, output_file = "test_pdf_04.png")
pdf.Plot(i = 5, j = 3, fi = 0.8, fj = 0.2, legend = 2, output_file = "test_pdf_05.png")
pdf.Plot(i = 5, j = 3, fi = 0.1, fj = 0.9, legend = 2, output_file = "test_pdf_06.png")
pdf.Plot(i = 5, j = 3, fi = 0.9, fj = 0.1, legend = 2, output_file = "test_pdf_07.png")

pdf.Plot(i = 5, j = 4, fi = 0.5, fj = 0.5, legend = 2, output_file = "test_pdf_08.png")
pdf.Plot(i = 5, j = 4, fi = 0.3, fj = 0.7, legend = 2, output_file = "test_pdf_09.png")
pdf.Plot(i = 5, j = 4, fi = 0.7, fj = 0.3, legend = 2, output_file = "test_pdf_10.png")
pdf.Plot(i = 5, j = 4, fi = 0.2, fj = 0.8, legend = 2, output_file = "test_pdf_11.png")
pdf.Plot(i = 5, j = 4, fi = 0.8, fj = 0.2, legend = 2, output_file = "test_pdf_12.png")
pdf.Plot(i = 5, j = 4, fi = 0.1, fj = 0.9, legend = 2, output_file = "test_pdf_13.png")
pdf.Plot(i = 5, j = 4, fi = 0.9, fj = 0.1, legend = 2, output_file = "test_pdf_14.png")

pdf.Plot(i = 5, fi = 0.5, legend = 2, output_file = "test_pdf_15.png")
pdf.Plot(i = 5, fi = 0.4, legend = 2, output_file = "test_pdf_16.png")
pdf.Plot(i = 5, fi = 0.6, legend = 2, output_file = "test_pdf_17.png")
pdf.Plot(i = 5, fi = 0.2, legend = 2, output_file = "test_pdf_18.png")
pdf.Plot(i = 5, fi = 0.8, legend = 2, output_file = "test_pdf_19.png")
pdf.Plot(i = 5, fi = 0.1, legend = 2, output_file = "test_pdf_20.png")
pdf.Plot(i = 5, fi = 0.9, legend = 2, output_file = "test_pdf_21.png")

pdf.Plot(i = 5, fi = 0.4, fj = 0.6, legend = 2, output_file = "test_pdf_22.png")
pdf.Plot(i = 5, fi = 0.6, fj = 0.4, legend = 2, output_file = "test_pdf_23.png")
pdf.Plot(i = 5, fi = 0.2, fj = 0.8, legend = 2, output_file = "test_pdf_24.png")
pdf.Plot(i = 5, fi = 0.8, fj = 0.2, legend = 2, output_file = "test_pdf_25.png")
pdf.Plot(i = 5, fi = 0.1, fj = 0.9, legend = 2, output_file = "test_pdf_26.png")
pdf.Plot(i = 5, fi = 0.9, fj = 0.1, legend = 2, output_file = "test_pdf_27.png")


pdf.Test()
