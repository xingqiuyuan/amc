#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core, modal
from amc.base import AMC, AMCtable
from amc.species import Species
from amc.util import set_random_generator
import pylab

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

AMC.Init("./test_amc.lua", "default")

modal.Init()
dist = modal.ModalDistribution("urban_eurotrack")

set_random_generator(0)
concentration_aer_num, concentration_aer_mass, diameter_mean, class_distribution = AMC.GenerateModalDistributionRandom(dist)
print class_distribution

concentration_aer_num_pretty, concentration_aer_mass_pretty = \
    AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                            legend = [1.37, 1.], margins = {"right" : 0.25},
                            output_file = "./test_dist_amc_number.png")

AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                            percentage = True, legend = [1.37, 1.], margins = {"right" : 0.25},
                            output_file = "./test_dist_amc_number_percentage.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          legend = [1.37, 1.02], margins = {"right" : 0.25},
                          output_file = "./test_dist_amc_mass.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          percentage = True, legend = [1.37, 1.02], margins = {"right" : 0.25},
                          output_file = "./test_dist_amc_mass_percentage.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          species = 5, legend = [1.37, 1.], margins = {"right" : 0.25},
                          output_file = "./test_dist_amc_mass_dust.png")

AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                          species = 6, legend = [1.37, 1.], margins = {"right" : 0.25},
                          output_file = "./test_dist_amc_mass_bc.png")


degree_num, degree_mass = AMC.ComputeExternalMixingDegree(concentration_aer_num, concentration_aer_mass)
