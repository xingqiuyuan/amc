#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
import driver
from driver.geo_data import *
from driver import core

if core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

if core.DRIVER_HAS_TIMER > 0:
    from driver.util import TimerCPU, TimerWall


# Init trajectory model.
GeoDataBase.Init("./test_driver.lua")

GeoDataBase.GetNameList()

elev = GeoDataElevation()
