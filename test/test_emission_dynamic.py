#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc
from amc import base as amc_base
from amc import core as amc_core
from amc.emission import DynamicsEmissionBase, Source, Tracer

import driver
from driver import base as driver_base
from driver import core as driver_core
from driver import amc as driver_amc
from driver import trajectory
from random import uniform, seed

from ops import Ops

if driver_core.DRIVER_HAS_LOGGER > 0:
    from driver.util import LoggerDriver
    LoggerDriver.Init("./test_driver.lua")

if amc_core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

# Init AMC before ... or trouble ahead.
driver_amc.AMX_EulerMixedBaseMoving.Initiate("./test_amc.lua", "default")

# Init trajectory model.
trajectory.TrajectoryChimere.Init("./test_driver.lua", "default")

traj = trajectory.TrajectoryChimere()

lat_min = 36.
lat_max = 55.
lon_min = -14.
lon_max = -1.

seed(0)

tmax = traj.Load(uniform(lat_min, lat_max), uniform(lon_min, lon_max), 0., 600.)

# Init standalone amc for display.
amc_base.AMC.Init("./test_amc.lua", "default")

# Emission.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default")
DynamicsEmissionBase.Init(ops)

tracer_name = [Tracer.GetName(i) for i in range(Tracer.GetNtracer())]
source_name = [Source.GetName(i) for i in range(Source.GetNsource())]

emission_rate_mass = []
for name in tracer_name:
    emis = traj.GetVariable(name)
    print name, emis.min(), emis.mean(), emis.max()
    emission_rate_mass.append(emis.mean())

emission_rate_number = []
for name in source_name:
    emis = traj.GetVariable("number_" + name)
    print name, emis.min(), emis.mean(), emis.max()
    emission_rate_number.append(emis.mean())

# Map emissions on AMC size and species.
rate_aer_number, rate_aer_mass, \
    emission_rate_number = DynamicsEmissionBase.Rate(emission_rate_mass, emission_rate_number)
