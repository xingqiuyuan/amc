#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core
from amc.species import Species, SpeciesEquilibrium
from amc.base import AMC
from amc.thermodynamics import ModelThermodynamicIsoropiaAEC
from ops import Ops

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)
SpeciesEquilibrium.Init(ops)

ops.SetPrefix("default.thermodynamic.isrpia_aec.")
isrpia_aec = ModelThermodynamicIsoropiaAEC(ops)
ops.Close()
