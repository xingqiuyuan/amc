#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import core, \
    ParameterizationCollisionEfficiencyFriedlander, \
    ParameterizationCollisionEfficiencySeinfeld, \
    ParameterizationCollisionEfficiencyJacobson

if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC

from ops import Ops
import time, numpy
import os, sys
import pylab

# AMC.
if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

temperature = 300.
pressure = 1.01325e5
rh = 0.7

#
# Parameterizations.
#

pylab.close("all")

#
# Friedlander
#

diameter1 = 1.
dmin2 = 0.001
dmax2 = 10.
N2 = 1000

param = ParameterizationCollisionEfficiencyFriedlander()

fig = param.Plot1d(temperature = temperature,
                   pressure = pressure,
                   relative_humidity = rh,
                   diameter1 = diameter1,
                   dmin2 = dmin2, dmax2 = dmax2, N2 = N2,
                   fmt = "k-", lw = 2,
                   title = ur"Friedlander parameterization for collision efficiency, diameter1 = " + str(diameter1) + " $\mu m$",
                   output_file = "collistion_efficiency_friedlander.png")

#
# Jacobson
#

diameter1 = 1.
dmin2 = 0.001
dmax2 = 10.
N2 = 1000

param = ParameterizationCollisionEfficiencyJacobson()

fig = param.Plot1d(temperature = temperature,
                   pressure = pressure,
                   relative_humidity = rh,
                   diameter1 = diameter1,
                   dmin2 = dmin2, dmax2 = dmax2, N2 = N2,
                   fmt = "k-", lw = 2,
                   title = ur"Jacobson parameterization for collision efficiency, diameter1 = " + str(diameter1) + " $\mu m$",
                   output_file = "collistion_efficiency_jacobson.png")

#
# Seinfeld
#

dmin2 = 0.002
dmax2 = 20.
N2 = 1000

param = ParameterizationCollisionEfficiencySeinfeld()

fig = param.Plot1d(temperature = temperature,
                   pressure = pressure,
                   relative_humidity = rh,
                   diameter1 = 100.,
                   label = "$D_p = 0.1$ mm",
                   dmin2 = dmin2, dmax2 = dmax2, N2 = N2,
                   fmt = "k-", lw = 2)

param.Plot1d(fig = fig,
             temperature = temperature,
             pressure = pressure,
             relative_humidity = rh,
             diameter1 = 1000.,
             label = "$D_p = 1$ mm",
             dmin2 = dmin2, dmax2 = dmax2, N2 = N2,
             fmt = "k-.", lw = 2,
             legend = 2,
             title = ur"Seinfeld parameterization for collision efficiency",
             output_file = "collistion_efficiency_seinfeld.png")
