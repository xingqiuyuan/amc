#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc.species import Species
from amc.physics import ParameterizationPhysics
from amc import core

if core.AMC_HAS_LOGGER > 0:
    from amc.util import LoggerAMC

from ops import Ops
import time, numpy
import os, sys
import pylab

font_size = 12
width = 10
margins = [0.1, 0.9, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1


# AMC.
if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")

ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)


dmin = 0.001
dmax = 10.0
N = 1000
q = numpy.power(dmax / dmin, 1. / float(N - 1))
diameter = [dmin * numpy.power(q, i) for i in range(N)]
diameter[0] = dmin
diameter[-1] = dmax


#
# Air free mean path.
#


pressure = [87000., 101325., 108570.]
temperature = [x + 273 for x in range(-50, 51, 5)]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

for pres,fmt in zip(pressure, ["k-^", "m-s", "c-o"]):
    airfmp = [ParameterizationPhysics.ComputeAirFreeMeanPath(temp, pres) for temp in temperature]
    ax.plot(temperature, airfmp, fmt, linewidth = 2, label = str(pres / 1000) + " kPa")

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"air free mean path [$\mu m$]")
ax.set_title("Air free mean path for low/medium/high atmospheric pressure.")
ax.legend(loc = 2)

pylab.savefig("test_air_free_mean_path.png")


#
# Water saturation vapor pressure.
#


temperature = [x + 273 for x in range(-50, 51, 5)]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

waterpsat = numpy.array([ParameterizationPhysics.ComputeWaterSaturationVaporPressurePreining(temp)
                         for temp in temperature], dtype = numpy.float) * 1.e-3

ax.plot(temperature, waterpsat, "k-^", linewidth = 2)
ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel("Water saturation vapor pressure [kPascal]")
ax.set_title("Water saturation vapor pressure as a function of temperature.")
ax.legend(loc = 2)

pylab.savefig("test_water_psat.png")


pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)
ax.semilogy(temperature, waterpsat, "k-^", linewidth = 2)
ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel("Water saturation vapor pressure [kPascal]")
ax.set_title("Water saturation vapor pressure as a function of temperature.")
ax.legend(loc = 2)

pylab.savefig("test_water_psat_log.png")


#
# Air density.
#


relative_humidity = [0.5, 0.7, 0.9]
pressure = [87000., 101325., 108570.]
temperature = [x + 273 for x in range(-50, 51, 5)]
fmt = ["k-^", "k-o", "k-s", "m-^", "m-o", "m-s", "c-^", "c-o", "c-s"]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

i = 0
for rh in relative_humidity:
    for pres in pressure:
        airdens = [ParameterizationPhysics.ComputeAirDensity(temp, pres, rh) for temp in temperature]
        ax.plot(temperature, airdens, fmt[i], linewidth = 2, label = "P=" + str(pres / 1000) + " kPa, RH=" + str(rh))
        i += 1

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"air density [$kg.m^{-3}$]")
ax.set_title("Air density for various atmospheric relative humidity and pressure.")
ax.legend(loc = 1)
pylab.savefig("test_air_density.png")


#
# Air dynamic viscosity.
#


temperature = [x + 273 for x in range(-50, 51, 5)]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

airdynvisc = numpy.array([ParameterizationPhysics.ComputeAirDynamicViscosity(temp)
                          for temp in temperature], dtype = numpy.float)

ax.plot(temperature, airdynvisc, "k-x", linewidth = 2)
ax.axvline(x = 273.15, lw = 2, ls = "-.", color = "b")
ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
locs, labels = pylab.yticks()
pylab.yticks(locs, map(lambda x: "%.2g" % x, locs * 1.e5))
ax.set_ylabel(ur"Air dynamic viscosity [$kg.m^{-1}.s^{-1} \times 10^5$]")
ax.set_title(ur"Air dynamic viscosity ($\times 10^5$) as a function of temperature.")
ax.legend(loc = 2)

pylab.savefig("test_air_dynamic_viscosity.png")


#
# Species gas diffusivity.
#


species = ["H2SO4", "NH3", "HNO3", "HCl", "BiA2D"]
pressure = 101325.
temperature = [x + 273 for x in range(-50, 51, 5)]
fmt = ["k-^", "m-o", "c-s", "r-^", "g-o", "b-s"]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

i = 0
for spec in species:
    species_index = Species.GetIndex(spec)
    collision_factor = Species.GetProperty("collision_factor", species_index)
    molecular_diameter = Species.GetProperty("molecular_diameter", species_index)
    # From µg.mol-1 to kg.mol-1
    molar_mass = Species.GetProperty(species_index, "molar_mass") * 1.e-9

    gasdiff = numpy.array([ParameterizationPhysics.ComputeGasDiffusivity(temp, pressure,
                                                                         collision_factor,
                                                                         molecular_diameter,
                                                                         molar_mass) for temp in temperature])
    # Turn from m2.s-1 to cm2.s-1.
    gasdiff *= 1.e4

    ax.plot(temperature, gasdiff, fmt[i], linewidth = 2, label = spec)
    i += 1

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
locs, labels = pylab.yticks()
pylab.yticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"Diffusivity [$cm^2.s^{-1}$]")
ax.set_title(ur"Gas diffusivity as a function of temperature at P=" + str(pressure / 1000) + " kPa.")
ax.legend(loc = 2)
pylab.savefig("test_gas_diffusivity.png")


#
# Species gas quadratic mean velocity.
#


species = ["H2SO4", "NH3", "HNO3", "HCl", "BiA2D"]
temperature = [x + 273 for x in range(-50, 51, 5)]
fmt = ["k-^", "m-o", "c-s", "r-^", "g-o", "b-s"]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

i = 0
for spec in species:
    species_index = Species.GetIndex(spec)
    # From µg.mol-1 to kg.mol-1
    molar_mass = Species.GetProperty(species_index, "molar_mass") * 1.e-9

    gasvqmg = numpy.array([ParameterizationPhysics.ComputeGasQuadraticMeanVelocity(temp, molar_mass) for temp in temperature])
    ax.plot(temperature, gasvqmg, fmt[i], linewidth = 2, label = spec)
    i += 1

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"Quadratic mean velocity [$m.s^{-1}$]")
ax.set_title(ur"Gas quadratic mean velocity as a function of temperature.")
ax.legend(loc = 2)
pylab.savefig("test_gas_quadratic_mean_velocity.png")


#
# Species saturation vapor pressure.
#


species = ["BiA2D", "BiA1D", "BiA0D", "GLY", "AnBlP", "AnBmP"]
temperature = [x + 273 for x in range(-50, 51, 5)]
fmt = ["k-^", "m-o", "c-s", "r-^", "g-o", "b-s"]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

i = 0
for spec in species:
    species_index = Species.GetIndex(spec)
    vaporization_enthalpy = Species.GetProperty(species_index, "vaporization_enthalpy")
    saturation_vapor_pressure = Species.GetProperty(species_index, "saturation_vapor_pressure")
    gaspsat = numpy.array([ParameterizationPhysics.ComputeSaturationVaporPressure(temp,
                                                                                  saturation_vapor_pressure,
                                                                                  vaporization_enthalpy)
                           for temp in temperature])
    ax.semilogy(temperature, gaspsat, fmt[i], linewidth = 2, label = spec)
    i += 1

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"Saturation vapor pressure [Pascal]")
ax.set_title(ur"Saturation vapor pressure as a function of temperature.")
ax.legend(loc = 2)
pylab.savefig("test_gas_saturation_vapor_pressure.png")


#
# Species saturation vapor concentration.
#


species = ["BiA2D", "BiA1D", "BiA0D", "GLY", "AnBlP", "AnBmP"]
temperature = [x + 273 for x in range(-50, 51, 5)]
fmt = ["k-^", "m-o", "c-s", "r-^", "g-o", "b-s"]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

i = 0
for spec in species:
    species_index = Species.GetIndex(spec)
    vaporization_enthalpy = Species.GetProperty(species_index, "vaporization_enthalpy")
    saturation_vapor_concentration = Species.GetProperty(species_index, "saturation_vapor_concentration")
    gascsat = numpy.array([ParameterizationPhysics.ComputeSaturationVaporPressure(temp,
                                                                                  saturation_vapor_concentration,
                                                                                  vaporization_enthalpy)
                           for temp in temperature])
    ax.semilogy(temperature, gascsat, fmt[i], linewidth = 2, label = spec)
    i += 1

ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"Saturation vapor concentration [$\mu g.m^{-3}$]")
ax.set_title(ur"Saturation vapor concentration as a function of temperature.")
ax.legend(loc = 2)
pylab.savefig("test_gas_saturation_vapor_concentration.png")


#
# Water dynamic viscosity.
#


temperature = [x + 273 for x in range(-50, 51, 5)]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

waterdynvisc = numpy.array([ParameterizationPhysics.ComputeWaterDynamicViscosity(temp)
                            for temp in temperature], dtype = numpy.float)

ax.plot(temperature, waterdynvisc, "k-x", linewidth = 2)
ax.axvline(x = 273.15, lw = 2, ls = "-.", color = "b")
ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"water dynamic viscosity [$kg.m^{-1}.s^{-1}$]")
ax.set_title(ur"Water dynamic viscosity as a function of temperature.")
ax.legend(loc = 2)

pylab.savefig("test_water_dynamic_viscosity.png")


#
# Water surface tension.
#


temperature = [x + 273 for x in range(-50, 51, 5)]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

watersurftens = numpy.array([ParameterizationPhysics.ComputeWaterSurfaceTension(temp)
                             for temp in temperature], dtype = numpy.float)

ax.plot(temperature, watersurftens, "k-x", linewidth = 2)
ax.axvline(x = 273.15, lw = 2, ls = "-.", color = "b")
ax.set_xlim(xmin = temperature[0], xmax = temperature[-1])
ax.set_xlabel("temperature [Kelvin]")
ax.set_ylabel(ur"water surface tension [$N.m^{-1}$]")
ax.set_title(ur"Water surface tension as a function of temperature.")
ax.legend(loc = 2)

pylab.savefig("test_water_surface_tension.png")


#
# Particle slip flow correction.
#


pressure = 101325.
temperature = 298.15
airfmp = ParameterizationPhysics.ComputeAirFreeMeanPath(temperature, pressure)

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

particle_slip_flow_correction_allen = [ParameterizationPhysics.ComputeParticleCunninghamSlipFlowCorrectionAllen(d, airfmp) for d in diameter]
particle_slip_flow_correction_kasten = [ParameterizationPhysics.ComputeParticleCunninghamSlipFlowCorrectionKasten(d, airfmp) for d in diameter]
particle_slip_flow_correction_millikan = [ParameterizationPhysics.ComputeParticleCunninghamSlipFlowCorrectionMillikan(d, airfmp) for d in diameter]
particle_slip_flow_correction_fuchs = [ParameterizationPhysics.ComputeParticleCunninghamSlipFlowCorrectionFuchs(d, airfmp) for d in diameter]

ax.loglog(diameter, particle_slip_flow_correction_allen, "r-", lw = 2, label = "Allen")
ax.loglog(diameter, particle_slip_flow_correction_kasten, "b-.", lw = 2, label = "Kasten")
ax.loglog(diameter, particle_slip_flow_correction_millikan, "m--", lw = 2, label = "Millikan")
ax.loglog(diameter, particle_slip_flow_correction_fuchs, "c:", lw = 2, label = "Fuchs")

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
locs, labels = pylab.yticks()
pylab.yticks(locs, map(lambda x: "%1.0f" % x, locs))
ax.set_ylabel(ur"slip flow correction [adim]")
ax.set_title("Slip flow corrections as a function of diameter for T = %1.2f K and P = %1.0f Pa." % (temperature, pressure))
ax.legend(loc = 1)

pylab.savefig("test_slip_flow_correction.png")


#
# Particle Diffusivity.
#


pressure = 101325.
temperature = 298.15

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

# In cm2/s
particle_diffusivity_allen = [ParameterizationPhysics.ComputeParticleDiffusivityAllen(d, temperature, pressure)
                              * 1.e4 for d in diameter]
particle_diffusivity_kasten = [ParameterizationPhysics.ComputeParticleDiffusivityKasten(d, temperature, pressure)
                              * 1.e4 for d in diameter]
particle_diffusivity_millikan = [ParameterizationPhysics.ComputeParticleDiffusivityMillikan(d, temperature, pressure)
                              * 1.e4 for d in diameter]
particle_diffusivity_fuchs = [ParameterizationPhysics.ComputeParticleDiffusivityFuchs(d, temperature, pressure)
                              * 1.e4 for d in diameter]

ax.loglog(diameter, particle_diffusivity_allen, "r-", lw = 2, label = "Allen")
ax.loglog(diameter, particle_diffusivity_kasten, "b-.", lw = 2, label = "Kasten")
ax.loglog(diameter, particle_diffusivity_millikan, "m--", lw = 2, label = "Millikan")
ax.loglog(diameter, particle_diffusivity_fuchs, "c:", lw = 2, label = "Fuchs")

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"particle diffusivity [$cm^2.s^{-1}$]")
ax.set_title("Particle diffusivities as a function of diameter for T = %1.2f K and P = %1.0f Pa." % (temperature, pressure))
ax.legend(loc = 1)

pylab.savefig("test_particle_diffusivity.png")


#
# Particle free mean path.
#


pressure = 101325.
temperature = 298.15

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

# In µm.
particle_freemeanpath_allen = [ParameterizationPhysics.ComputeParticleFreeMeanPathAllen(d, temperature, pressure)
                               * 1.e6 for d in diameter]
particle_freemeanpath_kasten = [ParameterizationPhysics.ComputeParticleFreeMeanPathKasten(d, temperature, pressure)
                                * 1.e6 for d in diameter]
particle_freemeanpath_millikan = [ParameterizationPhysics.ComputeParticleFreeMeanPathMillikan(d, temperature, pressure)
                                  * 1.e6 for d in diameter]
particle_freemeanpath_fuchs = [ParameterizationPhysics.ComputeParticleFreeMeanPathFuchs(d, temperature, pressure)
                               * 1.e6 for d in diameter]

ax.semilogx(diameter, particle_freemeanpath_allen, "r-", lw = 2, label = "Allen")
ax.semilogx(diameter, particle_freemeanpath_kasten, "b-.", lw = 2, label = "Kasten")
ax.semilogx(diameter, particle_freemeanpath_millikan, "m--", lw = 2, label = "Millikan")
ax.semilogx(diameter, particle_freemeanpath_fuchs, "c:", lw = 2, label = "Fuchs")

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"particle free mean path [$\mu m$]")
ax.set_title("Particle free mean paths as a function of diameter for T = %1.2f K and P = %1.0f Pa." % (temperature, pressure))
ax.legend(loc = 1)

pylab.savefig("test_particle_freemeanpath.png")


#
# Particle quadratic mean velocity.
#


temperature = [250., 298., 350.]

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

# In µm.
particle_quadraticmeanvelocity = [[ParameterizationPhysics.ComputeParticleQuadraticMeanVelocity(d, temp)
                                   for d in diameter] for temp in temperature]

ax.loglog(diameter, particle_quadraticmeanvelocity[0], "r-", lw = 2, label = "T = %1.0f K" % temperature[0])
ax.loglog(diameter, particle_quadraticmeanvelocity[1], "b-.", lw = 2, label = "T = %1.0f K" % temperature[1])
ax.loglog(diameter, particle_quadraticmeanvelocity[2], "g--", lw = 2, label = "T = %1.0f K" % temperature[2])

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"particle quadratic mean velocity [$m.s^{-1}$]")
ax.set_title("Particle quadratic mean velocity as a function of diameter and temperature." % temperature)
ax.legend(loc = 1)

pylab.savefig("test_particle_quadraticmeanvelocity.png")


#
# Particle terminal velocity.
#


pressure = 101325.
temperature = 298.15

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

# In cm.s-1.
particle_terminalvelocity_allen = [ParameterizationPhysics.ComputeParticleTerminalVelocityAllen(d, temperature, pressure)
                                   * 1.e2 for d in diameter]
particle_terminalvelocity_kasten = [ParameterizationPhysics.ComputeParticleTerminalVelocityKasten(d, temperature, pressure)
                                    * 1.e2 for d in diameter]
particle_terminalvelocity_millikan = [ParameterizationPhysics.ComputeParticleTerminalVelocityMillikan(d, temperature, pressure)
                                      * 1.e2 for d in diameter]
particle_terminalvelocity_fuchs = [ParameterizationPhysics.ComputeParticleTerminalVelocityFuchs(d, temperature, pressure)
                                   * 1.e2 for d in diameter]

ax.loglog(diameter, particle_terminalvelocity_allen, "r-", lw = 2, label = "Allen")
ax.loglog(diameter, particle_terminalvelocity_kasten, "b-.", lw = 2, label = "Kasten")
ax.loglog(diameter, particle_terminalvelocity_millikan, "m--", lw = 2, label = "Millikan")
ax.loglog(diameter, particle_terminalvelocity_fuchs, "c:", lw = 2, label = "Fuchs")

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
locs, labels = pylab.yticks()
pylab.yticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"particle terminal velocity [$cm.s^{-1}$]")
ax.set_title("Particle terminal velocity according to diameter for T = %1.2f K and P = %1.0f Pa." % (temperature, pressure))
ax.legend(loc = 2)

pylab.savefig("test_particle_terminalvelocity.png")


#
# Particle Knudsen number, equal diameters.
#


pressure = 101325.
temperature = 298.15

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

particle_knudsennumber_allen = [ParameterizationPhysics.ComputeParticleKnudsenNumberAllen(d, d, temperature, pressure)
                                for d in diameter]
particle_knudsennumber_kasten = [ParameterizationPhysics.ComputeParticleKnudsenNumberKasten(d, d, temperature, pressure)
                                 for d in diameter]
particle_knudsennumber_millikan = [ParameterizationPhysics.ComputeParticleKnudsenNumberMillikan(d, d, temperature, pressure)
                                   for d in diameter]
particle_knudsennumber_fuchs = [ParameterizationPhysics.ComputeParticleKnudsenNumberFuchs(d, d, temperature, pressure)
                                for d in diameter]

ax.loglog(diameter, particle_knudsennumber_allen, "r-", lw = 2, label = "Allen")
ax.loglog(diameter, particle_knudsennumber_kasten, "b-.", lw = 2, label = "Kasten")
ax.loglog(diameter, particle_knudsennumber_millikan, "m--", lw = 2, label = "Millikan")
ax.loglog(diameter, particle_knudsennumber_fuchs, "c:", lw = 2, label = "Fuchs")

ax.set_xlim(xmin = diameter[0], xmax = diameter[-1])
ax.set_xlabel(r"diameter $[\mu m]$")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
locs, labels = pylab.yticks()
pylab.yticks(locs, map(lambda x: "%.2g" % x, locs))
ax.set_ylabel(ur"knudsen number [adim]")
ax.set_title("Knudsen number between equal diameters particles for T = %1.2f K and P = %1.0f Pa." % (temperature, pressure))
ax.legend(loc = 1)

pylab.savefig("test_particle_knudsennumber1.png")



#
# Particle surface tension, different diameters.
#


from matplotlib.colors import LogNorm

pylab.clf()
fig = pylab.figure()
ax = fig.add_subplot(111)

particle_knudsennumber_allen = \
    numpy.array([[ParameterizationPhysics.ComputeParticleKnudsenNumberAllen(d1, d2, temperature, pressure)
                  for d1 in diameter] for d2 in diameter], dtype = numpy.float)
particle_knudsennumber_kasten = \
    numpy.array([[ParameterizationPhysics.ComputeParticleKnudsenNumberKasten(d1, d2, temperature, pressure)
                  for d1 in diameter] for d2 in diameter], dtype = numpy.float)
particle_knudsennumber_fuchs = \
    numpy.array([[ParameterizationPhysics.ComputeParticleKnudsenNumberFuchs(d1, d2, temperature, pressure)
                  for d1 in diameter] for d2 in diameter], dtype = numpy.float)
particle_knudsennumber_millikan = \
    numpy.array([[ParameterizationPhysics.ComputeParticleKnudsenNumberMillikan(d1, d2, temperature, pressure)
                  for d1 in diameter] for d2 in diameter], dtype = numpy.float)

level_min = particle_knudsennumber_allen.min()
level_max = particle_knudsennumber_allen.max()
level_min = numpy.floor(numpy.floor(numpy.log10(level_min)))
level_max = numpy.floor(numpy.ceil(numpy.log10(level_max)))
level = 30
level = numpy.logspace(level_min, level_max, level)

cs = ax.contourf(diameter, diameter, particle_knudsennumber_allen, level, norm = LogNorm())
ax.set_xscale("log")
ax.set_yscale("log")
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel(ur"diameter [$\mu m$]")
locs, labels = pylab.xticks()
pylab.xticks(locs, map(lambda x: "%.2g" % x, locs))
pylab.yticks(locs, map(lambda x: "%.2g" % x, locs))

cbar = pylab.colorbar(cs, ticks = level)
cbar.ax.set_ylabel(" knudsen number [adim]")
ax.set_title("Knudsen number between different diameters (T = %1.2f K, P = %1.0f Pa)." % (temperature, pressure))
pylab.savefig("test_particle_knudsennumber2.png")
