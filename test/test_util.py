#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

from amc import util, core
from amc.util import LoggerAMC
from ops import Ops
import time, numpy
from numpy import linalg
import os, sys
import pylab
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from mpl_toolkits.mplot3d import Axes3D


font_size = 12
width = 10
margins = [0.1, 0.9, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1


# AMC.
if core.AMC_HAS_LOGGER > 0:
    LoggerAMC.Init("./test_amc.lua")


# Set random generator.
util.set_random_generator(0)

#
# Generator random vectors of sum = 1.
#

#N = 10000000
N = 100000
P = 20
tol = 1.e-8

vsum = numpy.zeros(P, dtype = numpy.float)
vmean = numpy.ones(P, dtype = numpy.float) / P

vdiff = numpy.zeros(N, dtype = numpy.float)
for i in range(N):
    v = util.generate_random_vector(P)

    if abs(util.compute_vector_sum(v) - 1.) > tol:
        print v, v.sum()
        raise ValueError("Vector sum seems not equal to unity.")

    vsum += v

    vtmp = vsum / float(i + 1) - vmean
    vdiff[i] = numpy.sqrt((vtmp * vtmp).sum()) 

vsum /= float(N)
print vsum, vsum.sum(), 1. / float(P)

index = numpy.array(range(1, N + 1))
index_log = numpy.log(index)

A = numpy.array([index_log, numpy.ones(N)])
w = linalg.lstsq(A.T, numpy.log(vdiff))[0]

fig = pylab.figure()
ax = fig.add_subplot(111)
ax.loglog(index, vdiff, "k-")
ax.loglog(index, numpy.exp(w[0] * index_log + w[1]), "r-")
ax.set_xlim(xmin = 1, xmax = N + 1)
ax.set_title("Test random vector generator.")
pylab.savefig("./test_random_vector_generator.png")


#
# Matrix determinant.
#


#N = 10000000
N = 10000

P = 3
tol = 1.e-13
vsum = 10.

for i in range(N):
    m = util.generate_random_vector(P * P, vsum)

    det1 = util.compute_matrix_determinant(m, P)

    m.shape = [P, P]
    det2 = linalg.det(m)

    if abs(det1 - det2) > tol:
        print m, det1, det2
        raise ValueError("Wrong value for determinant.")


#
# Find polygon convex hull 2D.
#

N = 100
P = 2
vx = numpy.random.uniform(0., 1., N)
vy = numpy.random.uniform(0., 1., N)
point_coordinate = [[x, y] for x, y in zip(vx, vy)]

# Put points in correct order.
centroid = [vx.mean(), vy.mean()]

# Reorder thanks to polar coordinates.
point_coordinate.sort(key = lambda p: numpy.arctan2(p[1] - centroid[1], p[0] - centroid[0]))

fig = pylab.figure()
ax = fig.add_subplot(111)
ax.scatter(vx, vy, 30, color = "k", marker = "o", zorder = 10)
ax.scatter(centroid[0], centroid[1], 60, color = "b", marker = "s", zorder = 10)
ax.set_xlim(xmin = 0., xmax = 1.)
ax.set_ylim(ymin = 0., ymax = 1.)
pylab.axes().set_aspect(1., "box")
polygon = Polygon(point_coordinate, True, color = "grey", lw = 0, alpha = 0.7)
ax.add_patch(polygon)

point_coordinate_flat = numpy.array(point_coordinate).flatten()

edge = util.find_polygon_convex_hull(P, N, point_coordinate_flat)

for p in edge:
    px = [point_coordinate[i][0] for i in p]
    py = [point_coordinate[i][1] for i in p]
    ax.scatter(px, py, 120, color = "r", marker = "^")
    ax.plot(px, py, "r-", lw = 2)

ax.set_title("Convex hull of a 2D polygon.")
pylab.savefig("./test_polygon_convex_hull_2d.png")


#
# Find polygon convex hull 3D.
#

N = 100
P = 3

vx = numpy.random.uniform(0., 1., N)
vy = numpy.random.uniform(0., 1., N)
vz = numpy.random.uniform(0., 1., N)
point_coordinate = [[x, y, z] for x, y, z in zip(vx, vy, vz)]

# Put points in correct order.
centroid = [vx.mean(), vy.mean(), vz.mean()]

# Reorder thanks to polar coordinates.
#point_coordinate.sort(key = lambda p: numpy.arctan2(p[1] - centroid[1], p[0] - centroid[0]))

fig = pylab.figure()
ax = fig.add_subplot(111, projection = "3d")
ax.scatter(vx, vy, vz, color = "k", marker = "o", zorder = 10)
ax.scatter(centroid[0], centroid[1], centroid[2], color = "b", marker = "s", zorder = 10)
#polygon = Polygon(point_coordinate, True, color = "grey", lw = 0, alpha = 0.7)
#ax.add_patch(polygon)

point_coordinate_flat = numpy.array(point_coordinate).flatten()

edge = util.find_polygon_convex_hull(P, N, point_coordinate_flat)

for p in edge:
    px = [point_coordinate[i][0] for i in p]
    py = [point_coordinate[i][1] for i in p]
    pz = [point_coordinate[i][2] for i in p]
    ax.scatter(px, py, pz, color = "r", marker = "^")
    ax.plot(px, py, pz, "r-", lw = 2)

ax.set_title("Convex hull of a 3D polygon.")
pylab.savefig("./test_polygon_convex_hull_3d.png")


# 2D projections.

# XY.
point_coordinate = [[x, y] for x, y in zip(vx, vy)]

# Put points in correct order.
centroid = [vx.mean(), vy.mean()]

fig = pylab.figure()
ax = fig.add_subplot(111)
ax.scatter(vx, vy, color = "k", marker = "o", zorder = 10)
ax.scatter(centroid[0], centroid[1], color = "b", marker = "s", zorder = 10)
polygon = Polygon(point_coordinate, True, color = "grey", lw = 0, alpha = 0.7)
ax.add_patch(polygon)

point_coordinate_flat = numpy.array(point_coordinate).flatten()

for p in edge:
    px = [point_coordinate[i][0] for i in p]
    py = [point_coordinate[i][1] for i in p]
    ax.scatter(px, py, color = "r", marker = "^")
    ax.plot(px, py, "r-", lw = 2)

ax.set_title("Convex hull of a 3D polygon - XY projection.")
pylab.savefig("./test_polygon_convex_hull_3d_xy.png")

# YZ.
point_coordinate = [[y, z] for y, z in zip(vy, vz)]

# Put points in correct order.
centroid = [vy.mean(), vz.mean()]

fig = pylab.figure()
ax = fig.add_subplot(111)
ax.scatter(vy, vz, color = "k", marker = "o", zorder = 10)
ax.scatter(centroid[0], centroid[1], color = "b", marker = "s", zorder = 10)
polygon = Polygon(point_coordinate, True, color = "grey", lw = 0, alpha = 0.7)
ax.add_patch(polygon)

point_coordinate_flat = numpy.array(point_coordinate).flatten()

for p in edge:
    py = [point_coordinate[i][0] for i in p]
    pz = [point_coordinate[i][1] for i in p]
    ax.scatter(py, pz, color = "r", marker = "^")
    ax.plot(py, pz, "r-", lw = 2)

ax.set_title("Convex hull of a 3D polygon - YZ projection.")
pylab.savefig("./test_polygon_convex_hull_3d_yz.png")


# XZ.
point_coordinate = [[x, z] for x, z in zip(vx, vz)]

# Put points in correct order.
centroid = [vx.mean(), vz.mean()]

fig = pylab.figure()
ax = fig.add_subplot(111)
ax.scatter(vx, vz, color = "k", marker = "o", zorder = 10)
ax.scatter(centroid[0], centroid[1], color = "b", marker = "s", zorder = 10)
polygon = Polygon(point_coordinate, True, color = "grey", lw = 0, alpha = 0.7)
ax.add_patch(polygon)

point_coordinate_flat = numpy.array(point_coordinate).flatten()

for p in edge:
    px = [point_coordinate[i][0] for i in p]
    pz = [point_coordinate[i][1] for i in p]
    ax.scatter(px, pz, color = "r", marker = "^")
    ax.plot(px, pz, "r-", lw = 2)

ax.set_title("Convex hull of a 3D polygon - XZ projection.")
pylab.savefig("./test_polygon_convex_hull_3d_xz.png")
