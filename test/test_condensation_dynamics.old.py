#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab

from amc import AMC, AMCtable, \
    DiscretizationSize, Species, \
    DynamicsCondensationBase, DynamicsCondensationTable, \
    RedistributionSizeEulerHybrid, RedistributionSizeMixedCompositionBase, \
    RedistributionSizeEulerMixed, RedistributionSizeMixedCompositionBaseTable, \
    RedistributionSizeHybridCompositionBase, RedistributionSizeMixedCompositionTable, \
    RedistributionSizeHybridCompositionBaseTable, RedistributionSizeMixedTableCompositionBase, \
    RedistributionSizeHybridCompositionTable, RedistributionSizeMixedTableCompositionBaseTable, \
    RedistributionSizeHybridTableCompositionBase, RedistributionSizeMixedTableCompositionTable, \
    RedistributionSizeHybridTableCompositionBaseTable, RedistributionSizeTableEulerHybrid, \
    RedistributionSizeHybridTableCompositionTable, RedistributionSizeTableEulerMixed, \
    MeteorologicalData, modal, core, ArrayReal

if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC


# AMC.
AMC.Init("./test_amc.lua", "default")

# Redistribution.
from ops import Ops
ops = Ops("./test_amc.lua")
RedistributionSizeHybridCompositionBase.Init(ops)

# Condensation.
DynamicsCondensationBase.Init(ops)

# Meteo
MeteorologicalData.Init()
MeteorologicalData.UpdateCondensation()

print MeteorologicalData.GetMeteo()
print MeteorologicalData.GetCondensation()

# Initial distribution.
dist = modal.ModalDistribution("seigneur_1986_hazy")

class_dist = numpy.zeros(AMC.GetNclass(0), dtype = numpy.float)
class_dist[0] = 1.0

concentration_aer_num, concentration_aer_mass, diameter_mean = AMC.GenerateModalDistribution(dist, class_dist)

concentration_aer_num_beg_pretty, concentration_aer_mass_beg_pretty = \
    AMC.GetConcentrationPretty(concentration_aer_num, concentration_aer_mass)

concentration_gas = ArrayReal(Species.GetNgas())

# 1 µg/m3 fo sulfate
concentration_gas[0] = 1.0

# Time loop.
deltat = 100.
tend = 3600 * 12
tfig = tend
deltatfig = 100.
t = 0

print t, concentration_aer_num_im_beg.sum(), concentration_aer_mass_im_beg.sum(0)

while deltat > 0.:
    # Compute internal diameter and masses.
    AMC.ComputeParticleDiameterMassFixedDensity(concentration_aer_num, concentration_aer_mass)

    # Condensation/evaporation rate.
    rate_aer_mass, rate_gas = \
        DynamicsCondensationBase.RateLagrangian(concentration_aer_num, concentration_aer_mass, concentration_gas)

    deltat2 = deltat
    for i in range(AMC.GetNgNspecies()):
        if concentration_aer_mass[i] + deltat2 * rate_aer_mass[i] < 0.:
            deltat2 = min(deltat2, numpy.floor(- 0.99 * concentration_aer_mass[i] / rate_aer_mass[i]))

    for i in range(AMC.GetNgNspecies()):
        concentration_aer_mass[i] += deltat2 * rate_aer_mass[i]

    for i in range(Species.GetNspecies()):
        concentration_gas[i] += deltat2 * rate_gas[i]
    
    t += deltat2
    deltat = min(deltat, tend - t)

        AMC.Plot2d(concentration_aer_num_pretty, output_file = "./fig/coag_moving/test_dist2d_num_%05d.png" % int(t))

        tfig += deltatfig

# End.

concentration_aer_num_end_pretty, concentration_aer_mass_end_pretty = \
    AMC.GetConcentrationPretty(concentration_aer_num, concentration_aer_mass)

pylab.close('all')

fig = pylab.figure()

ax = fig.gca()

diameter_bound = DiscretizationSize.GetDiameterBoundList()
diameter_width_log = [numpy.log(diameter_bound[i + 1] / diameter_bound[i]) for i in range(DiscretizationSize.GetNsection())]

concentration_aer_num_im_beg = numpy.array([x / y for x, y in zip(concentration_aer_num_im_beg, diameter_width_log)]) * 1.e-6
concentration_aer_num_im_end = numpy.array([x / y for x, y in zip(concentration_aer_num_im_end, diameter_width_log)]) * 1.e-6

ax.loglog(diameter_mean, concentration_aer_num_im_end, "r-o", label = "final")
ax.loglog(diameter_mean, concentration_aer_num_im_beg, "b-s", label = "init")

ax.set_xlim(0.001, 10.)
ax.set_ylim(1.e-2, concentration_aer_num_im_beg.max() * 2)
pylab.show()


pylab.clf()
fig = pylab.figure()

ax = fig.gca()

concentration_aer_mass_tot_beg = numpy.array([x / y for x, y in zip(concentration_aer_mass_im_beg.sum(1), diameter_width_log)])
concentration_aer_mass_tot_end = numpy.array([x / y for x, y in zip(concentration_aer_mass_im_end.sum(1), diameter_width_log)])

diameter_mean = DiscretizationSize.GetDiameterMeanList()
ax.semilogx(diameter_mean, concentration_aer_mass_tot_beg, "b-s", label = "init")
ax.semilogx(diameter_mean, concentration_aer_mass_tot_end, "r-o", label = "final")

ax.set_xlim(0.001, 10.)
ax.set_ylim(0., concentration_aer_mass_tot_end.max() * 1.1)
pylab.show()

