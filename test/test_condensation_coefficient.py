#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import os, sys, numpy, pylab
from ops import Ops
from amc import AMC, Species, SpeciesEquilibrium, core
from amc import ParameterizationCondensationDiffusionLimitedDahneke, \
    ParameterizationCondensationDiffusionLimitedFuchsSutugin

font_size = 12
width = 10
margins = [0.1, 0.84, 0.1, 0.9]

pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)
pylab.matplotlib.rcParams["figure.subplot.left"] = margins[0]
pylab.matplotlib.rcParams["figure.subplot.right"] = margins[1]
pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins[2]
pylab.matplotlib.rcParams["figure.subplot.top"] = margins[3]

pylab.matplotlib.rcParams["font.size"] = font_size
pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
pylab.matplotlib.rcParams["axes.labelsize"] = font_size
pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
pylab.matplotlib.rcParams["xtick.major.size"] = 2
pylab.matplotlib.rcParams["xtick.minor.size"] = 1
pylab.matplotlib.rcParams["ytick.major.size"] = 2
pylab.matplotlib.rcParams["ytick.minor.size"] = 1


if core.AMC_HAS_LOGGER > 0:
    from amc import LoggerAMC
    LoggerAMC.Init("./test_amc.lua")

if core.AMC_HAS_TIMER > 0:
    from amc import TimerAMC

# Config.
ops = Ops("./test_amc.lua")
ops.SetPrefix("default.")
Species.Init(ops)
SpeciesEquilibrium.Init(ops)

# Condensation parameterizations.
ops.SetPrefix("default.dynamic.condensation.")
param1 = ParameterizationCondensationDiffusionLimitedDahneke(ops)
param2 = ParameterizationCondensationDiffusionLimitedFuchsSutugin(ops)

dmin = 0.001
dmax = 10.0
q = dmax / dmin
n = 1000
diameter = [dmin * numpy.power(q, float(i) / float(n - 1)) for i in range(n)]
mass = [core.AMC_PI6 * core.AMC_PARTICLE_DENSITY_DEFAULT *
        core.AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3 *
        d * d * d for d in diameter]
temperature = 300.
pressure = 1.013e5

coef1 = []
coef2 = []
for i,d in enumerate(diameter):
    coef1.append(param1.TestCoefficient(temperature, pressure, d))
    coef2.append(param2.TestCoefficient(temperature, pressure, d))

coef1 = numpy.array(coef1, dtype = numpy.float)
coef2 = numpy.array(coef2, dtype = numpy.float)

tcarac1 = numpy.zeros(coef1.shape, dtype = numpy.float)
tcarac2 = numpy.zeros(coef2.shape, dtype = numpy.float)

for i in range(n):
    for j in range(Species.GetNgas()):
        tcarac1[i, j] = 0.1 * mass[i] / coef1[i, j]
        tcarac2[i, j] = 0.1 * mass[i] / coef2[i, j]

semivol = Species.GetSemivolatileList()

pylab.clf()
fig = pylab.figure()

ax = fig.add_subplot(111)
for i in range(Species.GetNgas()):
    ax.loglog(diameter, coef1[:,i], ls = "-" , lw = 2, label = Species.GetName(semivol[i]))

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("condensation coefficient [$m^3.s^{-1}$]")
ax.set_title("Condensation coefficient for various species with correction factor \""
             + param1.GetCorrectionFactor().GetName() + "\"")

ax.legend(bbox_to_anchor = [1.21, 1])
pylab.savefig("test_condensation_coefficient_" + param1.GetCorrectionFactor().GetName().lower() + ".png")

#
pylab.clf()
fig = pylab.figure()

ax = fig.add_subplot(111)
for i in range(Species.GetNgas()):
    ax.loglog(diameter, tcarac1[:,i], ls = "-" , lw = 2, label = Species.GetName(semivol[i]))

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("condensation time scale [$s$]")
ax.set_title("Condensation time scale for various species with correction factor \""
             + param1.GetCorrectionFactor().GetName() + "\" and $\Delta C = 1\mu g.m^{-3}$")

ax.legend(bbox_to_anchor = [1.21, 1])
pylab.savefig("test_condensation_coefficient_tcarac_" + param1.GetCorrectionFactor().GetName().lower() + ".png")



pylab.clf()
fig = pylab.figure()

ax = fig.add_subplot(111)
for i in range(Species.GetNgas()):
    ax.loglog(diameter, coef2[:,i], ls = "-" , lw = 2, label = Species.GetName(semivol[i]))

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("condensation coefficient [$m^3.s^{-1}$]")
ax.set_title("Condensation coefficient for various species with correction factor \""
             + param2.GetCorrectionFactor().GetName() + "\"")
ax.legend(bbox_to_anchor = [1.21, 1])
pylab.savefig("test_condensation_coefficient_" + param2.GetCorrectionFactor().GetName().lower() + ".png")

#
pylab.clf()
fig = pylab.figure()

ax = fig.add_subplot(111)
for i in range(Species.GetNgas()):
    ax.loglog(diameter, tcarac2[:,i], ls = "-" , lw = 2, label = Species.GetName(semivol[i]))

ax.set_xlim(xmin = dmin, xmax = dmax)
ax.set_xlabel(ur"diameter [$\mu m$]")
ax.set_ylabel("condensation time scale [$s$]")
ax.set_title("Condensation time scale for various species with correction factor \""
             + param2.GetCorrectionFactor().GetName() + "\" and $\Delta C = 1\mu g.m^{-3}$")

ax.legend(bbox_to_anchor = [1.21, 1])
pylab.savefig("test_condensation_coefficient_tcarac_" + param2.GetCorrectionFactor().GetName().lower() + ".png")
