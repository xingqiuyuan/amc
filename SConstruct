#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import os

# Compilers.
cpp_compiler = "g++"
fortran_compiler = "gfortran"
linker = "g++"

# MPI libraries.
mpi_include = ["/usr/include/openmpi"]
mpi_library = ["mpi", "mpi_cxx"]

#
# AMC (standalone).
#

# Path to AMC.
amc_path = os.environ["HOME"] + "/code/amc"

# List of required libraries for AMC. 
library_list = ["rt", "dl", "lua", "lua5.1", "gfortran", "blitz"]

# Include path.
include_path_list = []

#
# Drivers.
#

# Required libraries.
driver_library_list = ["cblas", "ptf77blas", "satlas", "blas", "atlas"]

# NetCDF libraries.
netcdf_library = ["netcdf", "netcdf_c++"]

#
# Chimere.
#

# Path to Chimere.
chimere_path = os.environ["HOME"] + "/code/chimere/ccrt"

# Some user settings for Chimere.
chimere_fortran_compilation_option = ["-cpp", "-Wall", "-pedantic", "-fno-second-underscore", "-mcmodel=medium"]
chimere_fortran_module_path = os.path.join(chimere_path, "src")
chimere_library_list = ["mpi", "mpi_mpifh", "mpi_f77", "mpi_f90", "mpi_cxx", "netcdf", "netcdff", "netcdf_c++"]

# For non standard libraries, set accordingly "chimere_include_path_list" and "chimere_library_path_list".
chimere_include_path_list = []
chimere_library_path_list = []

# Chimere compilation.
fortran_preprocessor = "gcc -x c -P -E -o $TARGET $SOURCES"
chimere_cpp_compiler = "mpic++"
chimere_fortran_compiler = "mpif90"
chimere_linker = "mpic++"

# AMC templates for Chimere. First one matters.
chimere_amc_discretization_composition = ["base", "table"]
chimere_amc_redistribution_composition = ["base", "table"]
chimere_amc_redistribution_size = ["euler_mixed", "euler_hybrid", "moving_diameter", "table_mixed", "table_hybrid"]
chimere_amc_coefficient_repartition_coagulation = ["moving", "static"]

execfile(amc_path + "/share/SConstruct")

