// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
//
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

%module(directors="1") amc_core
%{
#include "AMCHeader.hxx"
#define SWIG_PYTHON_EXTRA_NATIVE_CONTAINERS
  typedef double real;

#include "typedef.hxx"
  %}

%include <typemaps.i>
%include "std_string.i"
%include "std_vector.i"
%include "std_map.i"

namespace std
{
  %template(vector1i)  vector<int>;
  %template(vector2i)  vector<vector<int> >;
  %template(vector1s)  vector<string>;
  %template(vector2s)  vector<vector<string> >;
  %template(vector1r)  vector<double>;
  %template(vector2r)  vector<vector<double> >;
  %template(vector3r)  vector<vector<vector<double> > >;
  %template(map_string_real)  map<string, double>;
  %template(map_string_string)  map<string, string>;

  %extend vector<vector<int> >
  {
    int __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<int>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, int value)
    {
      (*self)[i][j] = value;
    }
  }

  %extend vector<vector<string> >
  {
    string __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<string>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, string value)
    {
      (*self)[i][j] = value;
    }
  }

  %extend vector<vector<double> >
  {
    double __getitem__(int i, int j)
    {
      return (*self)[i][j];
    }

    vector<double>* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, double value)
    {
      (*self)[i][j] = value;
    }
  }

  %extend vector<vector<vector<double> > >
  {
    double __getitem__(int i, int j, int k)
    {
      return (*self)[i][j][k];
    }

    vector<vector<double> >* __getitem__(int i)
    {
      if (i < 0 || i >= self->size())
        throw std::out_of_range("Failed!");
      return &(*self)[i];
    }

    void __setitem__(int i, int j, int k, double value)
    {
      (*self)[i][j][k] = value;
    }
  }
}

using namespace std;

%include "carrays.i"
%array_class(double, ArrayReal);

#ifdef AMC_WITH_LOGGER
#define LOGGER_WITH_OPS
#define LOGGER_WITH_TIME
#define LOGGER_WITH_FORMAT
#ifdef AMC_WITH_MPI
#define LOGGER_WITH_MPI
#endif
%include "logger.i"
using namespace Logger;
#endif

%exception
{
  try
    {
      $action
        }
  catch(AMC::Error& e)
    {
      PyErr_SetString(PyExc_Exception, e.What().c_str());
      return NULL;
    }
  catch(Ops::Error& e)
    {
      PyErr_SetString(PyExc_Exception, e.What().c_str());
      return NULL;
    }
  catch(std::exception& e)
    {
      PyErr_SetString(PyExc_Exception, e.what());
      return NULL;
    }
  catch(string& s)
    {
      PyErr_SetString(PyExc_Exception, s.c_str());
      return NULL;
    }
  catch(const char* s)
    {
      PyErr_SetString(PyExc_Exception, s);
      return NULL;
    }
  catch(...)
    {
      PyErr_SetString(PyExc_Exception, "Unknown exception...");
      return NULL;
    }
}

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  %rename(SpeciesNPF)                 ClassSpecies;
  %rename(DiscretizationSizeNPF)      ClassDiscretizationSize;
  %rename(ParticleNPF)                ClassParticle;
  %rename(ConcentrationNPF)           ClassConcentration;
  %rename(MeteoDataNPF)               ClassMeteoData;
  %rename(ParticleDataNPF)            ClassParticleData;
}
#endif

namespace AMC
{
  %rename(MeteorologicalData)            ClassMeteorologicalData;
  %rename(ParameterizationPhysics)       ClassParameterizationPhysics;

  %ignore ClassParameterizationDiameterBase;
  %rename(ParameterizationDiameterFixed)         ClassParameterizationDiameterFixed;
  %rename(ParameterizationDiameterDensityFixed)  ClassParameterizationDiameterDensityFixed;
  %rename(ParameterizationDiameterDensityMoving) ClassParameterizationDiameterDensityMoving;
#ifdef AMC_WITH_GERBER
  %rename(ParameterizationDiameterGerber)        ClassParameterizationDiameterGerber;
#endif
#ifdef AMC_WITH_FRACTAL_SOOT
  %rename(ParameterizationDiameterFractalSoot)   ClassParameterizationDiameterFractalSoot;
#endif

  %rename(Species)                       ClassSpecies;
  %rename(SpeciesTracer)                 ClassSpeciesTracer;
  %rename(SpeciesEquilibrium)            ClassSpeciesEquilibrium;
  %rename(Phase)                         ClassPhase;

#ifdef AMC_WITH_TRACE
  %rename(Trace)                         ClassTrace;
#endif
#ifdef AMC_WITH_LAYER
  %rename(Layer)                         ClassLayer;
#endif
  %rename(Configuration)                 ClassConfiguration;

#ifdef AMC_WITH_SURFACE_TENSION
  %rename(ParameterizationSurfaceTensionBase)     ClassParameterizationSurfaceTensionBase;
  %rename(ParameterizationSurfaceTensionFixed)    ClassParameterizationSurfaceTensionFixed;
  %rename(ParameterizationSurfaceTensionAverage)  ClassParameterizationSurfaceTensionAverage;
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
  %rename(ParameterizationSurfaceTensionJacobson) ClassParameterizationSurfaceTensionJacobson;
#endif
#endif

  %rename(ParameterizationCollisionEfficiencyBase)         ClassParameterizationCollisionEfficiencyBase;
#ifdef AMC_WITH_COLLISION_EFFICIENCY
  %rename(ParameterizationCollisionEfficiencyFriedlander)  ClassParameterizationCollisionEfficiencyFriedlander;
  %rename(ParameterizationCollisionEfficiencyJacobson)     ClassParameterizationCollisionEfficiencyJacobson;
  %rename(ParameterizationCollisionEfficiencySeinfeld)     ClassParameterizationCollisionEfficiencySeinfeld;
#endif

#ifdef AMC_WITH_COAGULATION
  %rename(ParameterizationCoagulationBase)                 ClassParameterizationCoagulationBase;
  %rename(ParameterizationCoagulationBrownian)             ClassParameterizationCoagulationBrownian;
#ifdef AMC_WITH_WAALS_VISCOUS
  %rename(ParameterizationCoagulationBrownianWaalsViscous) ClassParameterizationCoagulationBrownianWaalsViscous;
#endif
  %rename(ParameterizationCoagulationTurbulent)            ClassParameterizationCoagulationTurbulent;
  %rename(ParameterizationCoagulationUnit)                 ClassParameterizationCoagulationUnit;
  %rename(ParameterizationCoagulationVoid)                 ClassParameterizationCoagulationVoid;
  %rename(ParameterizationCoagulationTable)                ClassParameterizationCoagulationTable;
#endif

#ifdef AMC_WITH_CONDENSATION
  %ignore ClassCondensationFluxCorrectionAqueousBase;
  %ignore ClassCondensationFluxCorrectionAqueousVoid;
  %rename(CondensationCorrectionFactorBase) ClassCondensationCorrectionFactorBase;
  %rename(CondensationCorrectionFactorDahneke) ClassCondensationCorrectionFactorDahneke;
  %rename(CondensationCorrectionFactorFuchsSutugin) ClassCondensationCorrectionFactorFuchsSutugin;
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  %rename(CondensationFluxCorrectionAqueousVersion1) ClassCondensationFluxCorrectionAqueousVersion1;
  %rename(CondensationFluxCorrectionAqueousVersion2) ClassCondensationFluxCorrectionAqueousVersion2;
#endif
  %ignore ClassCondensationFluxCorrectionDryVoid;
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  %rename(CondensationFluxCorrectionDryInorganicSalt) ClassCondensationFluxCorrectionDryInorganicSalt;
#endif
#ifdef AMC_WITH_KELVIN_EFFECT
  %rename(ParameterizationKelvinEffect) ClassParameterizationKelvinEffect;
#endif
  %rename(ParameterizationCondensationBase) ClassParameterizationCondensationBase;
  %rename(ParameterizationCondensationVoid) ClassParameterizationCondensationVoid;
#endif

#ifdef AMC_WITH_NUCLEATION
  %rename(ParameterizationNucleationBase)     ClassParameterizationNucleationBase;
  %rename(ParameterizationNucleationVoid)     ClassParameterizationNucleationVoid;
#ifdef AMC_WITH_POWER_LAW
  %rename(ParameterizationNucleationPowerLaw) ClassParameterizationNucleationPowerLaw;
#endif
#ifdef AMC_WITH_VEHKAMAKI
  %rename (ParameterizationNucleationVehkamaki) ClassParameterizationNucleationVehkamaki;
#endif
#ifdef AMC_WITH_MERIKANTO
  %rename (ParameterizationNucleationMerikanto) ClassParameterizationNucleationMerikanto;
#endif
#endif
  %rename(ModelThermodynamicBase)        ClassModelThermodynamicBase;
  %rename(ModelThermodynamicVoid)        ClassModelThermodynamicVoid;
#ifdef AMC_WITH_ISOROPIA
  %rename(ModelThermodynamicIsoropia)    ClassModelThermodynamicIsoropia;
#endif
#ifdef AMC_WITH_PANKOW
  %rename(ModelThermodynamicPankow)    ClassModelThermodynamicPankow;
#endif
#ifdef AMC_WITH_AEC
  %rename(ModelThermodynamicAEC)    ClassModelThermodynamicAEC;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
  %rename(ModelThermodynamicIsoropiaAEC)    ClassModelThermodynamicIsoropiaAEC;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
  %rename(ModelThermodynamicIsoropiaH2O)    ClassModelThermodynamicIsoropiaH2O;
#endif
#ifdef AMC_WITH_H2O
  %rename(ModelThermodynamicH2O)    ClassModelThermodynamicH2O;
#endif
#ifdef AMC_WITH_ADSORPTION
  %rename(ModelAdsorptionBase)   ClassModelAdsorptionBase;
  %rename(ModelAdsorptionPankow) ClassModelAdsorptionPankow;
#endif
  %rename(DiscretizationSize)            ClassDiscretizationSize;
  %rename(Thermodynamics)                ClassThermodynamics;
  %rename(Parameterization)              ClassParameterization;
  %rename(AerosolData)                   ClassAerosolData;
  %rename(RedistributionSizeBase)        ClassRedistributionSizeBase;
  %rename(RedistributionSizeEulerMixed)  ClassRedistributionSizeEulerMixed;
  %rename(RedistributionSizeEulerHybrid) ClassRedistributionSizeEulerHybrid;
  %rename(RedistributionSizeMovingDiameter) ClassRedistributionSizeMovingDiameter;
#ifdef AMC_WITH_LAYER
  %rename(RedistributionLayer) ClassRedistributionLayer;
#endif
#ifdef AMC_WITH_REPARTITION_COEFFICIENT
  %rename(PDF) ClassPDF;
#ifdef AMC_WITH_TRACE
  %rename(CoefficientRepartitionCoagulationTrace) ClassCoefficientRepartitionCoagulationTrace;
#endif
#ifdef AMC_WITH_LAYER
  %rename(Ring) ClassRing;
  %extend ClassRing
  {
    string __str__()
    {
      return (*self).Str();
    }
  }

  %rename(CoefficientRepartitionCoagulationLayer) ClassCoefficientRepartitionCoagulationLayer;
#endif
  %rename(CoefficientRepartitionCoagulationBase) ClassCoefficientRepartitionCoagulationBase;
  %rename(CoefficientRepartitionCoagulationSize) ClassCoefficientRepartitionCoagulationSize;
#endif

  // Dynamics.
  %rename(DynamicsBase)  ClassDynamicsBase;

#ifdef AMC_WITH_CONDENSATION
  // Condensation.
  %rename(DynamicsCondensation)  ClassDynamicsCondensation;
#endif

#ifdef AMC_WITH_NUCLEATION
  // Nucleation.
  %rename(DynamicsNucleation)  ClassDynamicsNucleation;
#endif

#ifdef AMC_WITH_DIFFUSION
  %rename(DynamicsDiffusion) ClassDynamicsDiffusion;
#endif

#ifdef AMC_WITH_NUMERICS
  %rename(NumericalSolverBase) ClassNumericalSolverBase;
#ifdef AMC_WITH_CONDENSATION
  %rename(NumericalSolverCondensation) ClassNumericalSolverCondensation;
#endif
#ifdef AMC_WITH_NUCLEATION
  %rename(NumericalSolverNucleation) ClassNumericalSolverNucleation;
#endif
#endif
}

%include "AMCHeader.hxx"

%include "share/functions.hxx"
#ifdef AMC_WITH_LOGGER
%include "share/AMCLogger.hxx"
#endif
#ifdef AMC_WITH_TIMER
%include "share/AMCTimer.hxx"
#endif
#ifdef AMC_WITH_STATISTICS
%include "share/AMCStatistics.hxx"
#endif
#ifdef AMC_WITH_MODAL
%include "share/ClassModalDistribution.hxx"
#endif
%include "parameterization/ClassParameterizationPhysics.hxx"
%include "parameterization/ClassParameterizationDiameterBase.hxx"
%include "parameterization/ClassParameterizationDiameterFixed.hxx"
%include "parameterization/ClassParameterizationDiameterDensityFixed.hxx"
%include "parameterization/ClassParameterizationDiameterDensityMoving.hxx"
#ifdef AMC_WITH_GERBER
%include "parameterization/ClassParameterizationDiameterGerber.hxx"
#endif
#ifdef AMC_WITH_FRACTAL_SOOT
%include "parameterization/ClassParameterizationDiameterFractalSoot.hxx"
#endif
%include "species/ClassSpecies.hxx"
%include "species/ClassSpeciesTracer.hxx"
%include "species/ClassSpeciesEquilibrium.hxx"
%include "species/ClassPhase.hxx"
%include "base/ClassConfiguration.hxx"
%include "base/ClassMeteorologicalData.hxx"
#ifdef AMC_WITH_SURFACE_TENSION
%include "parameterization/ClassParameterizationSurfaceTensionBase.hxx"
%include "parameterization/ClassParameterizationSurfaceTensionFixed.hxx"
%include "parameterization/ClassParameterizationSurfaceTensionAverage.hxx"
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
%include "parameterization/ClassParameterizationSurfaceTensionJacobson.hxx"
#endif
#endif
%include "parameterization/ClassParameterizationCollisionEfficiencyBase.hxx"
#ifdef AMC_WITH_COLLISION_EFFICIENCY
%include "parameterization/ClassParameterizationCollisionEfficiencyFriedlander.hxx"
%include "parameterization/ClassParameterizationCollisionEfficiencyJacobson.hxx"
%include "parameterization/ClassParameterizationCollisionEfficiencySeinfeld.hxx"
#endif
#ifdef AMC_WITH_COAGULATION
%include "parameterization/ClassParameterizationCoagulationBase.hxx"
%include "parameterization/ClassParameterizationCoagulationBrownian.hxx"
#ifdef AMC_WITH_WAALS_VISCOUS
%include "parameterization/ClassParameterizationCoagulationBrownianWaalsViscous.hxx"
#endif
%include "parameterization/ClassParameterizationCoagulationGravitational.hxx"
%include "parameterization/ClassParameterizationCoagulationTurbulent.hxx"
%include "parameterization/ClassParameterizationCoagulationUnit.hxx"
%include "parameterization/ClassParameterizationCoagulationVoid.hxx"
%include "parameterization/ClassParameterizationCoagulationTable.hxx"
#endif
#ifdef AMC_WITH_CONDENSATION
%include "parameterization/ClassCondensationCorrectionFactorBase.hxx"
%include "parameterization/ClassCondensationCorrectionFactorDahneke.hxx"
%include "parameterization/ClassCondensationCorrectionFactorFuchsSutugin.hxx"
%include "parameterization/ClassCondensationFluxCorrectionAqueousBase.hxx"
%include "parameterization/ClassCondensationFluxCorrectionAqueousVoid.hxx"
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
%include "parameterization/ClassCondensationFluxCorrectionAqueousVersion1.hxx"
%include "parameterization/ClassCondensationFluxCorrectionAqueousVersion2.hxx"
#endif
%include "parameterization/ClassCondensationFluxCorrectionDryVoid.hxx"
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
%include "parameterization/ClassCondensationFluxCorrectionDryInorganicSalt.hxx"
#endif
#ifdef AMC_WITH_KELVIN_EFFECT
%include "parameterization/ClassParameterizationKelvinEffect.hxx"
#endif
%include "parameterization/ClassParameterizationCondensationBase.hxx"
%include "parameterization/ClassParameterizationCondensationVoid.hxx"
%include "parameterization/ClassParameterizationCondensationDiffusionLimited.hxx"
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
%include "parameterization/ClassParameterizationCondensationDiffusionLimitedSoot.hxx"
#endif
#endif

#ifdef AMC_WITH_NUCLEATION
%include "parameterization/ClassParameterizationNucleationBase.hxx"
%include "parameterization/ClassParameterizationNucleationVoid.hxx"
#ifdef AMC_WITH_POWER_LAW
%include "parameterization/ClassParameterizationNucleationPowerLaw.hxx"
#endif
#ifdef AMC_WITH_VEHKAMAKI
%include "parameterization/ClassParameterizationNucleationVehkamaki.hxx"
#endif
#ifdef AMC_WITH_MERIKANTO
%include "parameterization/ClassParameterizationNucleationMerikanto.hxx"
#endif
#endif

%include "thermodynamics/ClassModelThermodynamicBase.hxx"
%include "thermodynamics/ClassModelThermodynamicVoid.hxx"
#ifdef AMC_WITH_ISOROPIA
%include "thermodynamics/ClassModelThermodynamicIsoropia.hxx"
#endif
#ifdef AMC_WITH_PANKOW
%include "thermodynamics/ClassModelThermodynamicPankow.hxx"
#endif
#ifdef AMC_WITH_AEC
%include "thermodynamics/ClassModelThermodynamicAEC.hxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
%include "thermodynamics/ClassModelThermodynamicIsoropiaAEC.hxx"
#endif
#ifdef AMC_WITH_H2O
%include "thermodynamics/ClassModelThermodynamicH2O.hxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
%include "thermodynamics/ClassModelThermodynamicIsoropiaH2O.hxx"
#endif
#ifdef AMC_WITH_ADSORPTION
%include "thermodynamics/ClassModelAdsorptionBase.hxx"
%include "thermodynamics/ClassModelAdsorptionPankow.hxx"
#endif
%include "discretization/ClassDiscretizationSize.hxx"
%include "discretization/ClassDiscretizationCompositionBase.hxx"
%include "discretization/ClassDiscretizationCompositionTable.hxx"
%include "discretization/ClassDiscretizationComposition.hxx"
%include "discretization/ClassDiscretizationClass.hxx"
#ifdef AMC_WITH_TRACE
%include "discretization/ClassTrace.hxx"
#endif
%include "discretization/ClassDiscretization.hxx"
%include "redistribution/ClassRedistributionSizeBase.hxx"
%include "redistribution/ClassRedistributionSizeMovingDiameter.hxx"
%include "redistribution/ClassRedistributionSizeEulerHybrid.hxx"
%include "redistribution/ClassRedistributionSizeEulerMixed.hxx"
%include "redistribution/ClassRedistributionSizeTable.hxx"
%include "redistribution/ClassRedistributionCompositionBase.hxx"
%include "redistribution/ClassRedistributionCompositionTable.hxx"
%include "redistribution/ClassRedistributionComposition.hxx"
%include "redistribution/ClassRedistributionClass.hxx"
#ifdef AMC_WITH_LAYER
%include "base/ClassLayer.hxx"
#endif
%include "base/ClassAerosolData.hxx"
%include "base/ClassThermodynamics.hxx"
%include "base/ClassParameterization.hxx"
#ifdef AMC_WITH_LAYER
%include "redistribution/ClassRedistributionLayer.hxx"
#endif
%include "redistribution/ClassRedistribution.hxx"

#ifdef AMC_WITH_REPARTITION_COEFFICIENT
%include "repartition_coefficient/ClassPDF.hxx"
#ifdef AMC_WITH_TRACE
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationTrace.hxx"
#endif
#ifdef AMC_WITH_LAYER
%include "repartition_coefficient/ClassRing.hxx"
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationLayer.hxx"
#endif
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationBase.hxx"
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationSize.hxx"
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationStatic.hxx"
%include "repartition_coefficient/ClassCoefficientRepartitionCoagulationMoving.hxx"
#endif

%include "dynamics/ClassDynamicsBase.hxx"
#ifdef AMC_WITH_COAGULATION
%include "dynamics/ClassDynamicsCoagulation.hxx"
#endif

#ifdef AMC_WITH_CONDENSATION
%include "dynamics/ClassDynamicsCondensation.hxx"
#endif

#ifdef AMC_WITH_NUCLEATION
%include "dynamics/ClassDynamicsNucleation.hxx"
#endif

#ifdef AMC_WITH_DIFFUSION
%include "dynamics/ClassDynamicsDiffusion.hxx"
#endif

#ifdef AMC_WITH_NUMERICS
%include "numerics/ClassNumericalSolverBase.hxx"
#ifdef AMC_WITH_COAGULATION
%include "numerics/ClassNumericalSolverCoagulation.hxx"
#endif
#ifdef AMC_WITH_CONDENSATION
%include "numerics/ClassNumericalSolverCondensation.hxx"
#endif
#ifdef AMC_WITH_NUCLEATION
%include "numerics/ClassNumericalSolverNucleation.hxx"
#endif
#endif

%include "base/ClassAMC.hxx"
#ifdef AMC_WITH_RUN
%include "base/ClassAMX.hxx"
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
%include "new_particle_formation/ClassSpecies.hxx"
%include "new_particle_formation/ClassDiscretizationSize.hxx"
%include "new_particle_formation/ClassParticle.hxx"
%include "new_particle_formation/ClassConcentration.hxx"
%include "new_particle_formation/ClassMeteoData.hxx"
%include "new_particle_formation/ClassParticleData.hxx"
%include "new_particle_formation/ClassDynamics.hxx"
%include "new_particle_formation/ClassNumericalSolver.hxx"
%include "new_particle_formation/ClassNewParticleFormation.hxx"
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  %template(DynamicsNPF) ClassDynamics<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
  %template(NumericalSolverNPF) ClassNumericalSolver<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
  %template(NewParticleFormation) ClassNewParticleFormation<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
}
#endif

namespace AMC
{
  %template(compute_vector_sum)             compute_vector_sum<real>;
  %template(compute_matrix_determinant)     compute_matrix_determinant<real>;
  %template(find_polygon_convex_hull)       find_polygon_convex_hull<real>;
  %template(generate_random_vector)         generate_random_vector<real>;

  // Unit conversion.
  %template(convert_ppm_to_molecule_per_cm3) convert_ppm_to_molecule_per_cm3<real>;
  %template(convert_ppt_to_molecule_per_cm3) convert_ppt_to_molecule_per_cm3<real>;
  %template(convert_molecule_per_cm3_to_ppm) convert_molecule_per_cm3_to_ppm<real>;
  %template(convert_molecule_per_cm3_to_ppt) convert_molecule_per_cm3_to_ppt<real>;

  %template(IsNotInitialized) ClassConfiguration::IsInitialized<init_flag::no>;
  %template(IsInitializedAMC) ClassConfiguration::IsInitialized<init_flag::amc>;
  %template(IsInitializedAMX) ClassConfiguration::IsInitialized<init_flag::amx>;

  %template(SetNotInitialized) ClassConfiguration::SetInitialized<init_flag::no>;
  %template(SetInitializedAMC) ClassConfiguration::SetInitialized<init_flag::amc>;
  %template(SetInitializedAMX) ClassConfiguration::SetInitialized<init_flag::amx>;

#ifdef AMC_WITH_TIMER
  %template(TimerCPU)  AMCTimer<CPU>;
  %template(TimerWall) AMCTimer<Wall>;
#endif

#ifdef AMC_WITH_MODAL
  %template(ModalDistribution)              ClassModalDistribution<real>;
#endif

  %template(DiscretizationCompositionBase)  ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;
  %template(DiscretizationCompositionTable) ClassDiscretizationComposition<ClassDiscretizationCompositionTable>;

  %template(DiscretizationClassBase)  ClassDiscretizationClass<ClassDiscretizationCompositionBase>;
  %template(DiscretizationClassTable) ClassDiscretizationClass<ClassDiscretizationCompositionTable>;

  %template(DiscretizationBase)  ClassDiscretization<ClassDiscretizationCompositionBase>;
  %template(DiscretizationTable) ClassDiscretization<ClassDiscretizationCompositionTable>;

  %template(RedistributionSizeTableEulerMixed)  ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>;
  %template(RedistributionSizeTableEulerHybrid) ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>;

  %template(RedistributionCompositionBaseBase)   ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>;
  %template(RedistributionCompositionTableBase)  ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>;
  %template(RedistributionCompositionBaseTable)  ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>;
  %template(RedistributionCompositionTableTable) ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>;

  %template(RedistributionCompositionNestedBase)   ClassRedistributionComposition<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %template(RedistributionCompositionNestedTableBase)  ClassRedistributionComposition<ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %template(RedistributionCompositionNestedBaseTable)  ClassRedistributionComposition<ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %template(RedistributionCompositionNestedTable) ClassRedistributionComposition<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %template(RedistributionClassBase) ClassRedistributionClass<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %template(RedistributionClassBaseTable) ClassRedistributionClass<ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %template(RedistributionClassTableBase) ClassRedistributionClass<ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %template(RedistributionClassTable) ClassRedistributionClass<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  // Redistribution.
  %template(RedistributionSizeMixedCompositionBase) ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
%template(RedistributionSizeHybridCompositionBase) ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMixedTableCompositionBase) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
%template(RedistributionSizeHybridTableCompositionBase) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMovingCompositionBase) ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMixedCompositionBaseTable) ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
%template(RedistributionSizeHybridCompositionBaseTable) ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMixedTableCompositionBaseTable) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
%template(RedistributionSizeHybridTableCompositionBaseTable) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMovingCompositionBaseTable) ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %template(RedistributionSizeMixedCompositionTable) ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
%template(RedistributionSizeHybridCompositionTable) ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

%template(RedistributionSizeMovingCompositionTable) ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %template(RedistributionSizeMixedTableCompositionTable) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
%template(RedistributionSizeHybridTableCompositionTable) ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  // Repartition coefficient.
#ifdef AMC_WITH_REPARTITION_COEFFICIENT
  %template(CoefficientRepartitionCoagulationStaticBase) ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase>;
  %template(CoefficientRepartitionCoagulationStaticTable) ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable>;
  %template(CoefficientRepartitionCoagulationMovingBase) ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %template(CoefficientRepartitionCoagulationMovingTable) ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
#endif

  // Coagulation.
#ifdef AMC_WITH_COAGULATION
  %template(ParameterizationCoagulationGravitationalCollisionEfficiencyVoid) ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyBase>;

#ifdef AMC_WITH_COLLISION_EFFICIENCY
  %template(ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander) ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyFriedlander>;
  %template(ParameterizationCoagulationGravitationalCollisionEfficiencyJacobson) ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyJacobson>;
  %template(ParameterizationCoagulationGravitationalCollisionEfficiencySeinfeld) ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencySeinfeld>;
#endif

  %template(DynamicsCoagulationStaticCoefficientBase) ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase> >;
  %template(DynamicsCoagulationStaticCoefficientTable) ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable> >;
  %template(DynamicsCoagulationMovingCoefficientBase) ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase> >;
  %template(DynamicsCoagulationMovingCoefficientTable) ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable> >;
#endif

  // Condensation.
#ifdef AMC_WITH_CONDENSATION
  %template(ParameterizationCondensationDiffusionLimitedDahneke) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedFuchsSutugin) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  %template(ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;

  %template(ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  %template(ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %template(ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;

  %template(ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %template(ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt) ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
  %template(ParameterizationCondensationDiffusionLimitedSootDahneke) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedSootFuchsSutugin) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  %template(ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;

  %template(ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;
  %template(ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  %template(ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1DryInorganicSalt) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %template(ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;

  %template(ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2DryInorganicSalt) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %template(ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt) ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#endif
#endif

#ifdef AMC_WITH_NUMERICS
#ifdef AMC_WITH_COAGULATION
  %template(NumericalSolverCoagulationStaticBase) ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase> >;
  %template(NumericalSolverCoagulationStaticTable) ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable> >;
  %template(NumericalSolverCoagulationMovingBase) ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase> >;
  %template(NumericalSolverCoagulationMovingTable) ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable> >;
#endif
#endif

  // AMC.
  %template(AMC_Base)  ClassAMC<ClassDiscretizationCompositionBase>;
  %template(AMC_Table) ClassAMC<ClassDiscretizationCompositionTable>;

#ifdef AMC_WITH_RUN
  // AMX.
#ifdef AMC_WITH_COAGULATION
  %template(AMX_EulerMixedBaseCoefMoving) ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %template(AMX_EulerMixedTableCoefMoving) ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %template(AMX_EulerMixedBaseCoefStatic) ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %template(AMX_EulerMixedTableCoefStatic) ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;

  %template(AMX_EulerHybridBaseCoefMoving) ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %template(AMX_EulerHybridTableCoefMoving) ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %template(AMX_EulerHybridBaseCoefStatic) ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %template(AMX_EulerHybridTableCoefStatic) ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;

  %template(AMX_EulerMovingBaseCoefMoving) ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %template(AMX_EulerMovingTableCoefMoving) ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %template(AMX_EulerMovingBaseCoefStatic) ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %template(AMX_EulerMovingTableCoefStatic) ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;
#endif

  %template(AMX_EulerMixedBase) ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base>;
  %template(AMX_EulerMixedTable) ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table>;

  %template(AMX_EulerHybridBase) ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base>;
  %template(AMX_EulerHybridTable) ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table>;

  %template(AMX_EulerMovingBase) ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base>;
  %template(AMX_EulerMovingTable) ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table>;
#endif

#ifdef AMC_WITH_LOGGER
  %feature("director") AMCLogger;
#endif

#ifdef AMC_WITH_TIMER
  %feature("director") AMCTimer<CPU>;
  %feature("director") AMCTimer<Wall>;
#endif

#ifdef AMC_WITH_STATISTICS
  %feature("director") AMCStatistics;
#endif

  %feature("director") ClassParameterizationPhysics;
  %feature("director") ClassParameterizationDiameterBase;
  %feature("director") ClassParameterizationDiameterFixed;
  %feature("director") ClassParameterizationDiameterDensityFixed;
  %feature("director") ClassParameterizationDiameterDensityMoving;
#ifdef AMC_WITH_GERBER
  %feature("director") ClassParameterizationDiameterGerber;
#endif
#ifdef AMC_WITH_FRACTAL_SOOT
  %feature("director") ClassParameterizationDiameterFractalSoot;
#endif

#ifdef AMC_WITH_MODAL
  %feature("director") ClassModalDistribution<real>;
#endif
  %feature("director") ClassModelThermodynamicBase;
  %feature("director") ClassModelThermodynamicVoid;
#ifdef AMC_WITH_ISOROPIA
  %feature("director") ClassModelThermodynamicIsorropia;
#endif
#ifdef AMC_WITH_PANKOW
  %feature("director") ClassModelThermodynamicPankow;
#endif
#ifdef AMC_WITH_AEC
  %feature("director") ClassModelThermodynamicAEC;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
  %feature("director") ClassModelThermodynamicIsoropiaAEC;
#endif
#ifdef AMC_WITH_H2O
  %feature("director") ClassModelThermodynamicH2O;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
  %feature("director") ClassModelThermodynamicIsoropiaH2O;
#endif
  %feature("director") ClassDiscretizationSize;
  %feature("director") ClassSpecies;
  %feature("director") ClassSpeciesTracer;
  %feature("director") ClassSpeciesEquilibrium;
  %feature("director") ClassConfiguration;
  %feature("director") init_flag;
  %feature("director") ClassThermodynamics;
  %feature("director") ClassParameterization;
  %feature("director") ClassAerosolData;
  %feature("director") ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;
  %feature("director") ClassDiscretizationComposition<ClassDiscretizationCompositionTable>;
  %feature("director") ClassDiscretizationClass<ClassDiscretizationCompositionBase>;
  %feature("director") ClassDiscretizationClass<ClassDiscretizationCompositionTable>;
  %feature("director") ClassDiscretization<ClassDiscretizationCompositionBase>;
  %feature("director") ClassDiscretization<ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>;
  %feature("director") ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>;
  %feature("director") ClassRedistributionSizeEulerMixed;
  %feature("director") ClassRedistributionSizeEulerHybrid;
  %feature("director") ClassRedistributionSizeMovingDiameter;
  %feature("director") ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>;
#ifdef AMC_WITH_LAYER
  %feature("director") ClassRedistributionLayer;
#endif

  %feature("director") ClassRedistributionClass<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistributionClass<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistributionClass<ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistributionClass<ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  // Redistribution.
  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerMixed, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeEulerHybrid, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %feature("director") ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase>;
  %feature("director") ClassRedistribution<ClassRedistributionSizeMovingDiameter, ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable>;

  %feature("director") ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyBase>;

  %feature("director") ClassParameterizationCollisionEfficiencyBase;
#ifdef AMC_WITH_COLLISION_EFFICIENCY
  %feature("director") ClassParameterizationCollisionEfficiencyFriedlander;
  %feature("director") ClassParameterizationCollisionEfficiencyJacobson;
  %feature("director") ClassParameterizationCollisionEfficiencySeinfeld;
#endif

  %feature("director") ClassDynamicsBase;

#ifdef AMC_WITH_COAGULATION
  %feature("director") ClassParameterizationCoagulationBase;
  %feature("director") ClassParameterizationCoagulationBrownian;
  %feature("director") ClassParameterizationCoagulationTable<ClassParameterizationCoagulationBrownian>;
#ifdef AMC_WITH_WAALS_VISCOUS
  %feature("director") ClassParameterizationCoagulationBrownianWaalsViscous;
  %feature("director") ClassParameterizationCoagulationTable<ClassParameterizationCoagulationBrownianWaalsViscous>;
#endif
  %feature("director") ClassParameterizationCoagulationUnit;
  %feature("director") ClassParameterizationCoagulationTurbulent;
  %feature("director") ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyBase>;
#ifdef AMC_WITH_COLLISION_EFFICIENCY
  %feature("director") ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyFriedlander>;
  %feature("director") ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyJacobson>;
  %feature("director") ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencySeinfeld>;
#endif
  %feature("director") ClassParameterizationCoagulationVoid;

#ifdef AMC_WITH_REPARTITION_COEFFICIENT
  %feature("director") ClassPDF;
#ifdef AMC_WITH_TRACE
  %feature("director") ClassCoefficientRepartitionCoagulationTrace;
#endif
#ifdef AMC_WITH_LAYER
  %feature("director") ClassRing;
  %feature("director") ClassCoefficientRepartitionCoagulationLayer;
#endif
  %feature("director") ClassCoefficientRepartitionCoagulationBase;
  %feature("director") ClassCoefficientRepartitionCoagulationSize;
  %feature("director") ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase>;
  %feature("director") ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable>;
  %feature("director") ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase> >;
  %feature("director") ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable> >;
#endif

  %feature("director") ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase> >;
  %feature("director") ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable> >;
  %feature("director") ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase> >;
  %feature("director") ClassDynamicsCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable> >;
#endif

#ifdef AMC_WITH_CONDENSATION
  %feature("director") ClassCondensationCorrectionFactorBase;
  %feature("director") ClassCondensationCorrectionFactorDahneke;
  %feature("director") ClassCondensationCorrectionFactorFuchsSutugin;
  %feature("director") ClassParameterizationCondensationBase;
  %feature("director") ClassParameterizationCondensationVoid;

  %feature("director") ClassParameterizationCondensationDiffusionLimitedDahneke<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;

  %feature("director") ClassDynamicsCondensation;

#ifdef AMC_WITH_KELVIN_EFFECT
  %feature("director") ClassParameterizationKelvinEffect;
#endif

#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  %feature("director") ClassCondensationFluxCorrectionAqueousVersion1;
  %feature("director") ClassCondensationFluxCorrectionAqueousVersion2;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  %feature("director") ClassCondensationFluxCorrectionDryInorganicSalt;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
  %feature("director") ClassParameterizationCondensationDiffusionLimitedDahneke<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVoid, ClassCondensationFluxCorrectionDryVoid>;
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryVoid>;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryVoid>;

#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion1, ClassCondensationFluxCorrectionDryInorganicSalt>;

  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
  %feature("director") ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin, ClassCondensationFluxCorrectionAqueousVersion2, ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#endif
#endif

#ifdef AMC_WITH_NUCLEATION
  %feature("director") ClassParameterizationNucleationBase;
  %feature("director") ClassParameterizationNucleationVoid;
#ifdef AMC_WITH_POWER_LAW
  %feature("director") ClassParameterizationNucleationPowerLaw;
#endif
#ifdef AMC_WITH_VEHKAMAKI
  %feature("director") ClassParameterizationNucleationVehkamaki;
#endif
#ifdef AMC_WITH_MERIKANTO
  %feature("director") ClassParameterizationNucleationMerikanto;
#endif
  %feature("director") ClassDynamicsNucleation;
#endif

#ifdef AMC_WITH_DIFFUSION
  %feature("director") ClassDynamicsDiffusion;
#endif

#ifdef AMC_WITH_NUMERICS
  %feature("director") ClassNumericalSolverBase;
#ifdef AMC_WITH_COAGULATION
  %feature("director") ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionBase> >;
  %feature("director") ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationStatic<ClassDiscretizationCompositionTable> >;
  %feature("director") ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>, ClassDiscretizationCompositionBase> >;
  %feature("director") ClassNumericalSolverCoagulation<ClassCoefficientRepartitionCoagulationMoving<ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>, ClassDiscretizationCompositionTable> >;
#endif
#ifdef AMC_WITH_CONDENSATION
  %feature("director") ClassNumericalSolverCondensation;
#endif
#ifdef AMC_WITH_NUCLEATION
  %feature("director") ClassNumericalSolverNucleation;
#endif
#endif

  // AMC.
  %feature("director") ClassAMC<ClassDiscretizationCompositionBase>;
  %feature("director") ClassAMC<ClassDiscretizationCompositionTable>;

#ifdef AMC_WITH_RUN
  //AMX.
#ifdef AMC_WITH_COAGULATION
  %feature("director") ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %feature("director") ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %feature("director") ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %feature("director") ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;

  %feature("director") ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %feature("director") ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %feature("director") ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %feature("director") ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;

  %feature("director") ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base, coag_coef_moving_base>;
  %feature("director") ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table, coag_coef_moving_table>;
  %feature("director") ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base, coag_coef_static_base>;
  %feature("director") ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table, coag_coef_static_table>;
#endif

  %feature("director") ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base>;
  %feature("director") ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table>;

  %feature("director") ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base>;
  %feature("director") ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table>;

  %feature("director") ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base>;
  %feature("director") ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table>;
#endif
}

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  %feature("director") ClassSpecies;
  %feature("director") ClassDiscretizationSize;
  %feature("director") ClassParticle;
  %feature("director") ClassConcentration;
  %feature("director") ClassMeteoData;
  %feature("director") ClassParticleData;
  %feature("director") ClassDynamics;
  %feature("director") ClassNumericalSolver;
  %feature("director") ClassNewParticleFormation;
}
#endif
