// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_NUCLEATION_CXX

#include "ClassDynamicsNucleation.hxx"

namespace AMC
{
  // Init static data.
  void ClassDynamicsNucleation::Init(Ops::Ops &ops)
  {
    // Clear everything.
    ClassDynamicsNucleation::Clear();

    string prefix_nucleation = ClassConfiguration::GetPrefix() + ".";
    if (prefix_nucleation == ".")
      prefix_nucleation = ops.GetPrefix();

    prefix_nucleation += "dynamic.nucleation.";
    ops.SetPrefix(prefix_nucleation);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << "Instantiate nucleation dynamics for AMC"
                         << " with configuration section \"" << prefix_nucleation << "\"." << endl;
#endif

    //
    // Load parameterization list.
    //

    ops.SetPrefix(prefix_nucleation + "list.");

    vector1s param_list = ops.GetEntryList();
    Nparam_ = int(param_list.size());

    parameterization_list_.resize(Nparam_);
    parameterization_section_.resize(Nparam_);
    parameterization_default_index_ = -1;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info() << Reset() << "Number of nucleation parameterization = " << Nparam_ << endl;
#endif

    for (int i = 0; i < Nparam_; i++)
      {
        ops.SetPrefix(prefix_nucleation + "list." + param_list[i] + ".");
        const string type = ops.Get<string>("type");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset()
                             << "Nucleation parameterization number " << i << " is of type \"" << type << "\"." << endl; 
#endif

        // Is this parameterization the default ? Yes if it is the sole one.
        const bool parameterization_is_default = ops.Get<bool>("default", "", Nparam_ == 1);
        if (parameterization_is_default)
          {
            parameterization_default_index_ = i;

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fblue() << Info(2) << Reset() << "\tthis parameterization is the default." << endl;
#endif
          }

        if (type == "void")
          {
            ClassParameterizationNucleationVoid param;
            parameterization_list_[i] = param.clone();
          }
#ifdef AMC_WITH_POWER_LAW
        else if (type.find("power_law") != string::npos)
          {
            ClassParameterizationNucleationPowerLaw param(ops);
            parameterization_list_[i] = param.clone();
          }
#endif
#ifdef AMC_WITH_VEHKAMAKI
        else if (type == "vehkamaki")
          {
            ClassParameterizationNucleationVehkamaki param;
            parameterization_list_[i] = param.clone();
          }
#endif
#ifdef AMC_WITH_MERIKANTO
        else if (type == "merikanto")
          {
            ClassParameterizationNucleationMerikanto param;
            parameterization_list_[i] = param.clone();
          }
#endif
        else
          throw AMC::Error("Parameterization \"" + type + "\" not implemented," +
                           " valid types are \"power_law\", \"vehkamaki\", \"merikanto\" or \"void\".");

        parameterization_section_[i] = ops.Get<int>("section");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(2) << Reset()
                             << "\tassociated with general sections " << parameterization_section_[i] << endl;
#endif

        // The location to which the formula apply, default to whole domain.
        if (ops.Exists("location") && ! parameterization_is_default)
          {
            vector1i location_index;

            const string location_file = ops.Get<string>("location");
            ifstream fin(location_file.c_str());
            int index;
            while (fin >> index)
              location_index.push_back(index);
            fin.close();

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fyellow() << Info(2) << Reset()
                                 << "\tset location from file \"" << location_file << "\"." << endl;
#endif

            int location_index_max(0);
            for (int j = 0; j < int(location_index.size()); ++j)
              if (location_index[j] > location_index_max)
                location_index_max = location_index[j];

            const int location_index_max_old = int(parameterization_index_.size());
            if (location_index_max_old < location_index_max)
              parameterization_index_.resize(location_index_max);

            for (int j = location_index_max_old; j < location_index_max; ++j)
              parameterization_index_[j] = -1;

            for (int j = 0; j < int(location_index.size()); ++j)
              parameterization_index_[location_index[j]] = i;
          }
      }

    // Set default parameterization.
    if (parameterization_default_index_ >= 0)
      for (int i = 0; i < int(parameterization_index_.size()); ++i)
        if (parameterization_index_[i] < 0)
          parameterization_index_[i] = parameterization_default_index_;

    // New particle formation.
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    ops.SetPrefix(prefix_nucleation);

    if (ops.Exists("new_particle_formation"))
      {
        string prefix_npf = prefix_nucleation + "new_particle_formation.";
        if (ops.Is<string>("new_particle_formation"))
          prefix_npf = ops.Get<string>("new_particle_formation") + ".";

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bgreen() << Info() << "Instantiate new particle formation model"
                             << " from configuration section \"" << prefix_npf << "\"." << Reset().Str() << endl;
        *AMCLogger::GetLog() << Bmagenta() << Info() << "Note : This model is intended to handle particles"
                             << " which have not yet grown up to the lowest AMC diameter." << Reset().Str() << endl;
        *AMCLogger::GetLog() << Bmagenta() << Info() << "Note : This is generally the case for newly nucleated"
                             << " particles which have one diameter around 1 nm, whereas most aerosol models"
                             << " begin at 10, 40 nm." << Reset().Str() << endl;
#endif
        ops.SetPrefix(prefix_npf);

        NPF::ClassNewParticleFormation<NPF::npf_parameterization_coagulation_type,
                                       NPF::npf_parameterization_condensation_type>::Init(ops);

        // In return, set for each parameterization where
        // to find its species and gas concentrations.
        for (int i = 0; i < Nparam_; ++i)
          parameterization_list_[i]->SetIndexNPF();
      }
#endif
  }


  // Clear static data.
  void ClassDynamicsNucleation::Clear()
  {
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    NPF::ClassNewParticleFormation<NPF::npf_parameterization_coagulation_type,
                                   NPF::npf_parameterization_condensation_type>::Clear();
#endif

    Nparam_ = 0;
    parameterization_default_index_ = -1;
    parameterization_list_.clear();
    parameterization_index_.clear();
  }


  // Get methods.
  int ClassDynamicsNucleation::GetNparam()
  {
    return Nparam_;
  }


  int ClassDynamicsNucleation::GetParameterizationDefaultIndex()
  {
    return parameterization_default_index_;
  }


  void ClassDynamicsNucleation::GetParameterizationIndex(vector<int> &parameterization_index)
  {
    parameterization_index = parameterization_index_;
  }


  ClassParameterizationNucleationBase* ClassDynamicsNucleation::GetParameterizationPtr(const int &i)
  {
    return parameterization_list_[i];
  }


  // Compute rate for given general sections.
  void ClassDynamicsNucleation::Rate(const int section_min,
                                     const int section_max,
                                     const real *concentration_gas,
                                     vector<real> &rate_aer_number,
                                     vector<real> &rate_aer_mass,
                                     const int location_index)
  {
    const int param_index = (parameterization_index_.size() > location_index)
      ? parameterization_index_[location_index] : parameterization_default_index_;

    // If negative, no parameterization for that location, or not at all (Nparam_ == 0).
    if (param_index < 0)
      return;

    // If not space dependent, default to first parameterization.
    const int section_index = parameterization_section_[param_index];
    if (section_index >= section_min)
      if (section_index < section_max)
        parameterization_list_[param_index]->ComputeKernel(section_index,
                                                           concentration_gas,
                                                           rate_aer_number,
                                                           rate_aer_mass);
  }
}

#define AMC_FILE_CLASS_DYNAMICS_NUCLEATION_CXX
#endif
