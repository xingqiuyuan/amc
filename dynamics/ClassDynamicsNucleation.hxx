// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_NUCLEATION_HXX

namespace AMC
{
  /*! 
   * \class ClassDynamicsNucleation
   */
  class ClassDynamicsNucleation : protected ClassDynamicsBase
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Number of parameterizations.*/
    static int Nparam_;

    /*!< Index of default parameterization.*/
    static int parameterization_default_index_;

    /*!< List of nucleation parameterizations.*/
    static vector<ClassParameterizationNucleationBase* > parameterization_list_;

    /*!< Vector of indices of general section managed by one parameterization.*/
    static vector1i parameterization_section_;

    /*!< The parameterization to use depending on location and general section.*/
    static vector1i parameterization_index_; 

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNparam();
    static int GetParameterizationDefaultIndex();
    static void GetParameterizationIndex(vector<int> &parameterization_index);
    static ClassParameterizationNucleationBase* GetParameterizationPtr(const int &i);

    /*!< Compute rate for given general sections.*/
    static void Rate(const int section_min,
                     const int section_max,
                     const real *concentration_gas,
                     vector<real> &rate_aer_number,
                     vector<real> &rate_aer_mass,
                     const int location_index = -1);
  };
}

#define AMC_FILE_CLASS_DYNAMICS_NUCLEATION_HXX
#endif
