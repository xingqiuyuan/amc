// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_BASE_CXX

#include "ClassDynamicsBase.hxx"

namespace AMC
{
  // Compute time scales.
  void ClassDynamicsBase::ComputeTimeScale(const real *concentration,
                                              const vector<real> &rate,
                                              vector<int> &time_scale_index,
                                              vector<real> &time_scale_value)
  {
    int n = int(rate.size());
    time_scale_index.resize(n);
    time_scale_value.assign(n, numeric_limits<real>::max());

    for (int i = 0; i < n; i++)
      {
        time_scale_index[i] = i;
        if (abs(rate[i]) > real(0))
          time_scale_value[i] = concentration[i] / abs(rate[i]);
      }

    sort(time_scale_index.begin(), time_scale_index.end(), _compare_(time_scale_value));
  }


  map<string, real> ClassDynamicsBase::ComputeTimeScaleAerosolNumber(const real *concentration,
                                                                     const vector<real> &rate)
  {
    vector<int> time_scale_index;
    vector<real> time_scale_value;

    ClassDynamicsBase::ComputeTimeScale(concentration, rate,
                                        time_scale_index,
                                        time_scale_value);

    int h = get_power_out_of(ClassAerosolData::Ng_) + 1;

    map<string, real> time_scale;
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      {
        const int &j = time_scale_index[i];
        ostringstream name;
        name << setw(h) << setfill('0') << i
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[j][0]
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[j][1];

        if (time_scale_value[j] < numeric_limits<real>::max())
          time_scale[name.str()] = time_scale_value[j];
      }

    return time_scale;
  }


  map<string, real> ClassDynamicsBase::ComputeTimeScaleAerosolMass(const real *concentration,
                                                                   const vector<real> &rate)
  {
    vector<int> time_scale_index;
    vector<real> time_scale_value;

    ClassDynamicsBase::ComputeTimeScale(concentration, rate,
                                        time_scale_index,
                                        time_scale_value);

    int h = get_power_out_of(ClassAerosolData::NgNspecies_) + 1;

    map<string, real> time_scale;
    for (int i = 0; i < ClassAerosolData::NgNspecies_; i++)
      {
        const int j = time_scale_index[i];

        // General section index.
        int k = j / ClassSpecies::Nspecies_;

        // Species index.
        int l = j % ClassSpecies::Nspecies_;

        ostringstream name;
        name << setw(h) << setfill('0') << i
             << "_" << ClassSpecies::name_[l]
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[k][0]
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[k][1];

        if (time_scale_value[j] < numeric_limits<real>::max())
          time_scale[name.str()] = time_scale_value[j];
      }

    return time_scale;
  }


  map<string, real> ClassDynamicsBase::ComputeTimeScaleGas(const real *concentration,
                                                           const vector<real> &rate)
  {
    vector<int> time_scale_index;
    vector<real> time_scale_value;

    ClassDynamicsBase::ComputeTimeScale(concentration, rate,
                                        time_scale_index,
                                        time_scale_value);

    int h = get_power_out_of(ClassSpecies::Ngas_) + 1;

    map<string, real> time_scale;
    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      {
        const int j = time_scale_index[i];

        // Species index.
        const int k = ClassSpecies::semivolatile_[j];

        ostringstream name;
        name << setw(h) << setfill('0') << i
             << "_" << ClassSpecies::name_[k];

        if (time_scale_value[j] < numeric_limits<real>::max())
          time_scale[name.str()] = time_scale_value[j];
      }

    return time_scale;
  }


  // Remove unstable particles from concentration vectors.
  void ClassDynamicsBase::RemoveUnstableParticle(const int section_min,
                                                 const int section_max,
                                                 real *concentration_aer_number,
                                                 real *concentration_aer_mass,
                                                 real *concentration_gas)
  {
    // Check if some particles have completely vanished.
    const real &mass_lowest = ClassDiscretizationSize::mass_bound_.front();

    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        {
          real mass_single(real(0));

          const int j = i * ClassSpecies::Nspecies_;
          for (int k = 0; k < ClassSpecies::Nspecies_; ++k)
            mass_single += concentration_aer_mass[j + k];

          mass_single /= concentration_aer_number[i];

          // If particle diameter shrinked below the lowest model admissible diameter,
          // assumes particle no more stable and let it completely evaporate.
          if (mass_single < mass_lowest)
            {
#ifdef AMC_WITH_DEBUG_DYNAMIC_BASE
              CBUG << "Unstable particle at index = " << i
                   << ", mass_single = " << mass_single
                   << ", mass_lowest = " << mass_lowest << endl;
#endif
              concentration_aer_number[i] = real(0);
              for (int k = 0; k < ClassSpecies::Ngas_; k++)
                {
                  real &conc = concentration_aer_mass[j + ClassSpecies::semivolatile_[k]];
                  concentration_gas[k] += conc; 
                  conc = real(0);
                }
            }
        }
  }
}

#define AMC_FILE_CLASS_DYNAMICS_BASE_CXX
#endif
