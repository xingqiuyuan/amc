// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_CONDENSATION_HXX

#define AMC_CONDENSATION_QSSA_THRESHOLD_MIN_DEFAULT  0.9
#define AMC_CONDENSATION_QSSA_THRESHOLD_MAX_DEFAULT -0.9
#define AMC_CONDENSATION_NITER_MAX_DEFAULT 10
#define AMC_CONDENSATION_ACCURACY_DEFAULT 1.e7
#define AMC_CONDENSATION_PGTOL_DEFAULT  1.e-4
#define AMC_CONDENSATION_EPSILON_DEFAULT 0.001


namespace AMC
{
  /*! 
   * \class ClassDynamicsCondensation
   */
  class ClassDynamicsCondensation : protected ClassDynamicsBase
  {
  public:
    typedef AMC::real real;
    typedef typename AMC::vector1s vector1s;
    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;

  private:

    /*!< Maximum number of iteration for BFGS.*/
    static int Niter_max_;

    /*!< Required accuracy for BFGS.*/
    static double accuracy_;

    /*!< Required pgtol for BFGS.*/
    static double pgtol_;

    /*!< Finite difference.*/
    static real epsilon_;

    /*!< Number of parameterizations.*/
    static int Nparam_;

    /*!< Number of equilibrium models.*/
    static int Nmodel_equilibrium_;

#ifdef AMC_WITH_KELVIN_EFFECT
    /*!< Pointer to Kelvin effect parameterization.*/
    static ClassParameterizationKelvinEffect* kelvin_effect_;
#endif

    /*!< QSSA threshold above which (in absolute value) the QSSA
      for one species and one section indicates non equilibirum.*/
    static real qssa_threshold_min_, qssa_threshold_max_;

    /*!< Vector of indices of general sections managed by one parameterization.*/
    static vector2i parameterization_section_;

    /*!< List of condensation equilibrium thermodynamic models.*/
    static vector<ClassModelThermodynamicBase* > equilibrium_model_list_;

    /*!< List of condensation parameterizations.*/
    static vector<ClassParameterizationCondensationBase* > parameterization_list_;

    /*!< Makes species far from equilibrium condensate or evaporate.*/
    static void out_of_equilibrium_species_mass_transfer_(const int &section_min,
                                                          const int &section_max,
                                                          const real *concentration_aer_number,
                                                          real *concentration_aer_mass,
                                                          real *concentration_gas,
                                                          vector1i &species_at_equilibrium,
                                                          vector1r &rate_aer_mass);

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Init physical data necessary for condensation parameterizations.*/
    static void InitStep();

    /*!< Get methods.*/
    static int GetNparam();
    static ClassParameterizationCondensationBase* GetParameterizationPtr(const int &i);
#ifdef AMC_WITH_KELVIN_EFFECT
    static ClassParameterizationKelvinEffect* GetKelvinEffectPtr();
#endif

    /*!< Solves global equilibrium between several sections.*/
    static void GlobalEquilibriumBulk(const int section_min,
                                      const int section_max,
                                      const real *concentration_aer_number,
                                      real *concentration_aer_mass,
                                      real *concentration_gas);

    /*!< Solves global equilibrium between several sections with a size resolved method.*/
    static void GlobalEquilibriumSectionResolved(const int section_min,
                                                 const int section_max,
                                                 const real *concentration_aer_number,
                                                 real *concentration_aer_mass,
                                                 real *concentration_gas);

    /*!< Correct equilibrium gas surface with Kelvin effect.*/
    static void CorrectEquilibriumGasSurfaceWithKelvinEffect(const int section_min,
                                                             const int section_max,
                                                             const real *concentration_aer_number,
                                                             const vector1r &kelvin_effect);

    /*!< Compute lagrangian rate for given general sections.*/
    static void RateLagrangian(const int section_min,
                               const int section_max,
                               const real *concentration_aer_number,
                               const real *concentration_aer_mass,
                               const real *concentration_gas,
                               vector1r &rate_aer_mass,
                               vector1r &rate_gas);

    /*!< Growth time scale for condensation.*/
    static void ComputeTimeScaleGrowth(const real *concentration,
                                       const vector<real> &rate,
                                       vector<int> &time_scale_index,
                                       vector<real> &time_scale_value);

    static map<string, real> ComputeTimeScaleGrowth(const real *concentration,
                                                    const vector<real> &rate);
  };
}

#define AMC_FILE_CLASS_DYNAMICS_CONDENSATION_HXX
#endif
