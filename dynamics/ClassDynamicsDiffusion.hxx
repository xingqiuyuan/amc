// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_DIFFUSION_HXX

namespace AMC
{
  /*! 
   * \class ClassDynamicsDiffusion
   */
  class ClassDynamicsDiffusion
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector2b vector2b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector3i vector3i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Number of parameterizations.*/
    static int Nparam_;

    /*!< List of diffusion parameterizations.*/
    //static vector<ClassParameterizationDiffusionBase* > parameterization_list_;

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Get methods.*/
    static int GetNparam();
    //static ClassParameterizationDiffusionBase* GetParameterizationPtr(const int &i);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Compute rate for given general sections.*/
    static void Rate(const int section_min,
                     const int section_max,
                     const real *concentration_aer_number,
                     const real *concentration_aer_mass);
  };
}

#define AMC_FILE_CLASS_DYNAMICS_DIFFUSION_HXX
#endif
