// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_DIFFUSION_CXX

#include "ClassDynamicsDiffusion.hxx"

namespace AMC
{
  // Init static data.
  void ClassDynamicsDiffusion::Init(Ops::Ops &ops)
  {
    // Clear everything.
    ClassDynamicsDiffusion::Clear();
  }


  // Get methods.
  int ClassDynamicsDiffusion::GetNparam()
  {
    return Nparam_;
  }

  //template<class D>
  //ClassParameterizationDiffusionBase* ClassDynamicsDiffusion<D>::GetParameterizationPtr(const int &i)
  //{
  //  return parameterization_list_[i];
  //}


  // Clear static data.
  void ClassDynamicsDiffusion::Clear()
  {
    Nparam_ = 0;
    //parameterization_list_.clear();
  }


  // Diffusion concentration rates.
  void ClassDynamicsDiffusion::Rate(const int section_min,
                                    const int section_max,
                                    const real *concentration_aer_number,
                                    const real *concentration_aer_mass)
  {

  }
}

#define AMC_FILE_CLASS_DYNAMICS_DIFFUSION_CXX
#endif
