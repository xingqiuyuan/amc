// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_COAGULATION_CXX

#include "ClassDynamicsCoagulation.hxx"

namespace AMC
{
  // Init static data.
  template<class C>
  void ClassDynamicsCoagulation<C>::Init(Ops::Ops &ops)
  {
    // Clear everything.
    ClassDynamicsCoagulation<C>::Clear();

    string prefix_coagulation = ClassConfiguration::GetPrefix() + ".dynamic.coagulation.";
    ops.SetPrefix(prefix_coagulation);

    // Load parameterization list.
    vector1s param_list = ops.GetEntryList("list");
    Nparam_ = int(param_list.size());

    parameterization_list_.resize(Nparam_);
    parameterization_couple_.resize(Nparam_);

    vector2b couple_assigned(ClassAerosolData::Ng_, vector1b(ClassAerosolData::Ng_, false));

    for (int i = 0; i < Nparam_; i++)
      {
        ops.SetPrefix(prefix_coagulation + "list." + param_list[i] + ".");
        string type = ops.Get<string>("type");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug() << Reset()
                             << "Instantiate coagulation parameterization number " << i
                             << " of type \"" << type << "\"." << endl;
#endif

        if (type == "brownian")
          {
            ClassParameterizationCoagulationBrownian param(ops);
            parameterization_list_[i] = param.clone();
          }
#ifdef AMC_WITH_WAALS_VISCOUS
        else if (type == "brownian_waals_viscous")
          {
            ClassParameterizationCoagulationBrownianWaalsViscous param(ops);
            parameterization_list_[i] = param.clone();
          }
#endif
        else if (type == "turbulent")
          {
            ClassParameterizationCoagulationTurbulent param(ops);
            parameterization_list_[i] = param.clone();
          }
#ifdef AMC_WITH_COLLISION_EFFICIENCY
        else if (type == "gravitational_friedlander")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyFriedlander> param(ops);
            parameterization_list_[i] = param.clone();
          }
        else if (type == "gravitational_seinfeld")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencySeinfeld> param(ops);
            parameterization_list_[i] = param.clone();
          }
        else if (type == "gravitational_jacobson")
          {
            ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyJacobson> param(ops);
            parameterization_list_[i] = param.clone();
          }
#endif
        else if (type == "void")
          {
            ClassParameterizationCoagulationVoid param;
            parameterization_list_[i] = param.clone();
          }
        else if (type == "table")
          {
            ClassParameterizationCoagulationTable param(ops);
            parameterization_list_[i] = param.clone();
          }
        else if (type == "unit")
          {
            ClassParameterizationCoagulationUnit param(ops);
            parameterization_list_[i] = param.clone();
          }
        else
          throw AMC::Error("Parameterization \"" + type + "\" not implemented, valid types are " +
                           "\"void\", \"unit\", \"brownian\", \"brownian_waals_viscous\", \"turbulent\", " +
                           "\"gravitational_friedlander\", \"gravitational_seinfeld\", \"gravitational_jacobson\"" +
                           " and \"table\".");

        if (ops.Exists("size"))
          {
            int imin = ops.Get<int>("size.min");
            int imax = ops.Get<int>("size.max_excluded");
            if (imax < 0)
              imax += ClassDiscretizationSize::Nsection_;

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset()
                                 << "Assign size sections from " << imin << " to " << imax
                                 << " (excluded) to parameterization number " << i << endl;
#endif

            int jmin = ClassAerosolData::general_section_reverse_[imin].front();
            int jmax = ClassAerosolData::general_section_reverse_[imax].back();

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset()
                                 << "Assign general sections from " << jmin << " to " << jmax
                                 << " (excluded) to parameterization number " << i << endl;
#endif

            vector1i couple(2);
            for (int j1 = jmin; j1 < jmax; j1++)
              for (int j2 = jmin; j2 <= j1; j2++)
                {
                  couple[0] = j1;
                  couple[1] = j2;
                  parameterization_couple_[i].push_back(couple);
                  couple_assigned[j1][j2] = true;
                }
          }
      }

    ops.SetPrefix(prefix_coagulation);

    // Not assigned couples set to default, which is the first one.
    vector1i couple(2);
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      for (int j = 0; j <= i; j++)
        if (! couple_assigned[i][j])
          {
            couple[0] = i;
            couple[1] = j;
            parameterization_couple_[0].push_back(couple);
          }

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < Nparam_; i++)
      {
        ostringstream sout;
        for (int j = 0; j < int(parameterization_couple_[i].size()); j++)
          sout << "  " << parameterization_couple_[i][j] << ",";

        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset()
                             << "List of couples assigned to parameterization " << i << " :" << sout.str() << endl;
      }
#endif

    // Repartition coefficients.
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset()
                         << "Instantiate repartition coefficients of type \""
                         << (C::Type() == AMC_COEFFICIENT_REPARTITION_TYPE_STATIC ? "static" : "moving")
                         << "\"." << endl;
#endif

    ops.SetPrefix(prefix_coagulation + "repartition_coefficient.");
    repartition_coefficient_ = new C(ops);

    // Symetric coagulation kernel.
    kernel_.resize(ClassAerosolData::Ng_, ClassAerosolData::Ng_);
    kernel_ = real(0);

#ifdef AMC_WITH_LAYER
    layer_repartition_rate_.resize(ClassAerosolData::NgNspecies_, ClassLayer::Nlayer_max_);
    layer_repartition_rate_ = real(0);
#endif
  }


  // Init at beginning of time step physical properties which depends of meteorological parameters.
    template<class C>
  void ClassDynamicsCoagulation<C>::InitStep()
  {
    ClassMeteorologicalData::UpdateCoagulation();
  }


  // Get methods.
  template<class C>
  int ClassDynamicsCoagulation<C>::GetNparam()
  {
    return Nparam_;
  }

  template<class C>
  ClassParameterizationCoagulationBase* ClassDynamicsCoagulation<C>::GetParameterizationPtr(const int &i)
  {
    return parameterization_list_[i];
  }

  template<class C>
  void ClassDynamicsCoagulation<C>::GetParameterizationCouple(const int &i, vector<vector<int> > &couple)
  {
    couple = parameterization_couple_[i];
  }

  template<class C>
  C* ClassDynamicsCoagulation<C>::GetRepartitionCoefficientPtr()
  {
    return repartition_coefficient_;
  }

  // Clear static data.
  template<class C>
  void ClassDynamicsCoagulation<C>::Clear()
  {
    Nparam_ = 0;
    kernel_.free();
    parameterization_list_.clear();
    parameterization_couple_.clear();
    delete repartition_coefficient_;
    repartition_coefficient_ = NULL;
#ifdef AMC_WITH_LAYER
    layer_repartition_rate_.free();
#endif
  }


  // Compute Kernel.
  template<class C>
  void ClassDynamicsCoagulation<C>::ComputeKernel()
  {
    // Init coagulation kernel.
    kernel_ = real(0);

    // Coagulation kernel computation.
    for (int i = 0; i < Nparam_; i++)
      parameterization_list_[i]->ComputeKernel(parameterization_couple_[i], kernel_);

    // Coaulation kernel is symmetric.
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      for (int j = 0; j < i; ++j)
        kernel_(j, i) = kernel_(i, j);

    // Bug fix.
    kernel_ *= real(0.25);
  }


  // Coagulation concentration rates.
  template<class C>
  void ClassDynamicsCoagulation<C>::Rate(const int section_min,
                                         const int section_max,
                                         const real *concentration_aer_number,
                                         const real *concentration_aer_mass,
                                         vector<real> &rate_aer_number,
                                         vector<real> &rate_aer_mass)
  {
    // Compute coagulation gain thanks to repartition coefficients.
#ifdef AMC_WITH_LAYER
    layer_repartition_rate_ = real(0);
    repartition_coefficient_->ComputeGain(section_min, section_max,
                                          concentration_aer_number,
                                          concentration_aer_mass,
                                          kernel_,
                                          rate_aer_number,
                                          rate_aer_mass,
                                          layer_repartition_rate_);
#else
    repartition_coefficient_->ComputeGain(section_min, section_max,
                                          concentration_aer_number,
                                          concentration_aer_mass,
                                          kernel_,
                                          rate_aer_number,
                                          rate_aer_mass);
#endif

    // Compute loss for number and species mass of each general section.
    for (int i = section_min; i < section_max; ++i)
      {
        real loss(real(0));
        for (int j = 0; j < ClassAerosolData::Ng_; j++)
          loss += kernel_(i, j) * concentration_aer_number[j];

        rate_aer_number[i] -= concentration_aer_number[i] * loss;

#ifdef AMC_WITH_LAYER
        for (int j = 0; j < ClassLayer::Nspecies_layer_; ++j)
          {
            const int k = i * ClassSpecies::Nspecies_ + ClassLayer::species_index_layer_[j];
            if (concentration_aer_mass[k] > real(0))
              {
                const real ratio(rate_aer_mass[k] / concentration_aer_mass[k]);
                for (int l = 0; l < ClassLayer::Nlayer_[i]; ++l)
                  layer_repartition_rate_(k, l) =
                    ratio * (layer_repartition_rate_(k, l) - ClassAerosolData::layer_repartition_[k][l]);
              }
          }
#endif

        int j = ClassSpecies::Nspecies_ * i - 1;
        for (int k = 0; k < ClassSpecies::Nspecies_; ++k)
          rate_aer_mass[++j] -= concentration_aer_mass[j] * loss;
      }
  }
}

#define AMC_FILE_CLASS_DYNAMICS_COAGULATION_CXX
#endif
