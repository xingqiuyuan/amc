// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_COAGULATION_HXX

namespace AMC
{
  /*! 
   * \class ClassDynamicsCoagulation
   */
  template<class C>
  class ClassDynamicsCoagulation : protected ClassDynamicsBase
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector2b vector2b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector3i vector3i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Number of parameterizations.*/
    static int Nparam_;

    /*!< List of coagulation parameterizations.*/
    static vector<ClassParameterizationCoagulationBase* > parameterization_list_;

    /*!< Vector of indices of general section couples managed by one parameterization.*/
    static vector3i parameterization_couple_;

    /*!< Coagulation kernel.*/
    static Array<real, 2> kernel_;

    /*!< Repartition coefficients.*/
    static C* repartition_coefficient_;

#ifdef AMC_WITH_LAYER
    /*!< Layer repartition coagulation rate.*/
    static Array<real, 2> layer_repartition_rate_;
#endif

  public:

    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);

    /*!< Init physical data necessary for coagulation parameterizations.*/
    static void InitStep();

    /*!< Get methods.*/
    static int GetNparam();
    static ClassParameterizationCoagulationBase* GetParameterizationPtr(const int &i);
    static void GetParameterizationCouple(const int &i, vector<vector<int> > &couple);
    static C* GetRepartitionCoefficientPtr();

    /*!< Clear static data.*/
    static void Clear();

    /*!< Compute Kernel.*/
    static void ComputeKernel();

    /*!< Compute rate for given general sections.*/
    static void Rate(const int section_min,
                     const int section_max,
                     const real *concentration_aer_number,
                     const real *concentration_aer_mass,
                     vector<real> &rate_aer_number,
                     vector<real> &rate_aer_mass);
  };
}

#define AMC_FILE_CLASS_DYNAMICS_COAGULATION_HXX
#endif
