// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_BASE_HXX

namespace AMC
{
#ifndef SWIG
  struct _compare_
  {
    const vector1r &value_vector_;

    _compare_(const vector1r &value_vector) : value_vector_(value_vector) { return; }

    bool operator() (int i, int j) { return value_vector_[i] < value_vector_[j]; }
  };
#endif

  /*! 
   * \class ClassDynamicsBase
   */
  class ClassDynamicsBase : protected ClassAerosolData
  {
  public:

    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  public:

    /*!< Compute time scales.*/
    static void ComputeTimeScale(const real *concentration,
                                 const vector<real> &rate,
                                 vector<int> &time_scale_index,
                                 vector<real> &time_scale_value);

    static map<string, real> ComputeTimeScaleAerosolNumber(const real *concentration,
                                                           const vector<real> &rate);

    static map<string, real> ComputeTimeScaleAerosolMass(const real *concentration,
                                                         const vector<real> &rate);

    static map<string, real> ComputeTimeScaleGas(const real *concentration,
                                                 const vector<real> &rate);


    /*!< Remove unstable particles from concentration vectors.*/
    static void RemoveUnstableParticle(const int section_min,
                                       const int section_max,
                                       real *concentration_aer_number,
                                       real *concentration_aer_mass,
                                       real *concentration_gas);
  };
}

#define AMC_FILE_CLASS_DYNAMICS_BASE_HXX
#endif
