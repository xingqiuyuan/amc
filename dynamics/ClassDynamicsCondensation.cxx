// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_DYNAMICS_CONDENSATION_CXX

#include "ClassDynamicsCondensation.hxx"

namespace AMC
{
  // Makes species far from equilibrium condensate or evaporate.
  void ClassDynamicsCondensation::out_of_equilibrium_species_mass_transfer_(const int &section_min,
                                                                            const int &section_max,
                                                                            const real *concentration_aer_number,
                                                                            real *concentration_aer_mass,
                                                                            real *concentration_gas,
                                                                            vector1i &species_at_equilibrium,
                                                                            vector1r &rate_aer_mass)
  {
    // We first compute c/e coefficient and rates,
    // assuming local thermodynamics has been updated.
    vector1r condensation_coefficient(ClassAerosolData::NgNgas_);

#ifdef AMC_WITH_KELVIN_EFFECT
    vector1r kelvin_effect(ClassAerosolData::NgNphase_, real(1));

    if (kelvin_effect_ != NULL)
      kelvin_effect_->ComputeKelvinEffect(0, ClassAerosolData::Ng_,
                                          concentration_aer_number,
                                          concentration_aer_mass,
                                          kelvin_effect);
#endif

    for (int i = 0; i < Nparam_; i++)
      parameterization_list_[i]->ComputeKernel(0, ClassAerosolData::Ng_,
                                               parameterization_section_[i],
                                               concentration_aer_number,
                                               concentration_aer_mass,
                                               concentration_gas,
                                               condensation_coefficient,
                                               rate_aer_mass);

    // Some gas species may be far from equilibrium, not driven by it all
    // because its saturation vapor pressure is very low, bound to condensate,
    // or because of a high Kelvin effect and a low bulk gas concentration,
    // bound to evaporate.

    // It is not a good idea to hand over these species to equilibrium models
    // as they will certainly make them condensate or evaporate completeley
    // at the expense of sections we are not putting at equilibrium.

    // So this first part of routine guess which species are far from equilibrium
    // with QSSA ratio and make them condensate or evaporate, letting enough gas for
    // concentration for further c/e computation.
    for (int h = 0; h < ClassSpecies::Ngas_; h++)
      {
        bool bound_to_condensate(true);
        bool bound_to_evaporate(true);

        for (int i = section_min; i < section_max; i++)
          {
            int j = i * ClassSpecies::Ngas_ + h;
            real qssa_num = concentration_gas[h] - ClassAerosolData::equilibrium_gas_surface_[j];
            real qssa_denum = concentration_gas[h] + ClassAerosolData::equilibrium_gas_surface_[j];
            real qssa = (qssa_denum > real(0)) ? qssa_num / qssa_denum : real(0);

            bound_to_condensate = bound_to_condensate && (qssa > qssa_threshold_max_);
            bound_to_evaporate = bound_to_evaporate && (qssa < qssa_threshold_min_);
          }

        if (! (bound_to_condensate || bound_to_evaporate))
          continue;

        // General species index in AMC.
        int i = ClassSpecies::semivolatile_[h];

        // Sum of condensation coefficient above all general sections.
        real condensation_coefficient_total(real(0));
        for (int j = 0; j < ClassAerosolData::Ng_; j++)
          condensation_coefficient_total += condensation_coefficient[j * ClassSpecies::Ngas_ + h];

        // The total mass transfer towards aerosols,
        // to be substracted to gas phase in the end.
        real mass_transfer_total(real(0));

        // Sulfate likely and some organics with very low vapor pressure.
        if (bound_to_condensate)
          {
            species_at_equilibrium[h] = 1;

            // The total mass rate cannot be zero in this context.
            real tmp = concentration_gas[h] / condensation_coefficient_total;

            for (int j = section_min; j < section_max; j++)
              {
                real mass_transfer = condensation_coefficient[j * ClassSpecies::Ngas_ + h] * tmp;
                concentration_aer_mass[j * ClassSpecies::Nspecies_ + i] += mass_transfer;
                mass_transfer_total += mass_transfer;
              }
          }

        // Mostly, species with high molar mass, hence high
        // kelvin effect, and a low bulk gas concentration.
        if (bound_to_evaporate)
          {
            species_at_equilibrium[h] = -1;

            for (int j = section_min; j < section_max; j++)
              {
                int k = j * ClassSpecies::Ngas_ + h;

                // The mass transfer from gas to particle phase, negative value.
                real mass_transfer = - condensation_coefficient[k]
                  * ClassAerosolData::equilibrium_gas_surface_[k]
                  / condensation_coefficient_total;

                // This time the transfer mass may be positive or negative,
                // mainly because of Kelvin effect, small size sections
                // may evaporate, and greater sections may condense what
                // previous sections lost.
                int l = j * ClassSpecies::Nspecies_ + i;
                real concentration_aer_new = concentration_aer_mass[l] + mass_transfer;

                // If new concentration is negative due to excess evaporation.
                // Let concentration be zero and limit transfer.
                if (concentration_aer_new < real(0))
                  {
                    mass_transfer = - concentration_aer_mass[l];
                    concentration_aer_mass[l] = real(0);
                  }
                else
                  concentration_aer_mass[l] += concentration_aer_new;

                mass_transfer_total += mass_transfer;
              }
          }

        // Finally, substract to bulk gas conc,
        // mass_transfer_total is > 0 in case of condensation, < 0 in case of evaporation.
        concentration_gas[h] += mass_transfer_total;

        // Avoid clipping.
        concentration_gas[h] = concentration_gas[h] > real(0) ? concentration_gas[h] : real(0);
      }
  }


  // Init static data.
  void ClassDynamicsCondensation::Init(Ops::Ops &ops)
  {
    // Clear everything.
    ClassDynamicsCondensation::Clear();

    // Dry flux correction needs special initialization.
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
    ClassCondensationFluxCorrectionDryInorganicSalt::Init(ops);
#endif

    string prefix_condensation = ClassConfiguration::GetPrefix() + ".dynamic.condensation.";
    ops.SetPrefix(prefix_condensation);

    //
    // Load parameterization list.
    //

    vector1s param_list = ops.GetEntryList("list");
    Nparam_ = int(param_list.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset()
                         << "For condensation, Nparam = " << Nparam_ << endl;
#endif

    parameterization_section_.resize(Nparam_);
    parameterization_list_.resize(Nparam_);

    vector1b section_assigned(ClassAerosolData::Ng_, false);

    for (int i = 0; i < Nparam_; i++)
      {
        ops.SetPrefix(prefix_condensation + "list." + param_list[i] + ".");

        string type = ops.Get<string>("type");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug() << Reset()
                             << "Instantiate condensation parameterization number " << i
                             << " of type \"" << type << "\"." << endl;
#endif

        string correction_factor_type = ops.Get<string>("correction_factor", "", "dahneke");
        string flux_correction_aqueous_type = ops.Get<string>("flux_correction.aqueous.type", "", "void");
        bool flux_correction_dry_inorganic_salt = ops.Exists("flux_correction.dry");

        if (type == "void")
          {
            ClassParameterizationCondensationVoid param;
            parameterization_list_[i] = param.clone();
          }
        else if (type == "diffusion_limited")
          {
            if (correction_factor_type == "dahneke")
              {
                if (flux_correction_aqueous_type == "void")
                  parameterization_list_[i] =
                    new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                          ClassCondensationFluxCorrectionAqueousVoid,
                                                                          ClassCondensationFluxCorrectionDryVoid>(ops);
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
                else if (flux_correction_aqueous_type == "version1")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                              ClassCondensationFluxCorrectionAqueousVersion1,
                                                                              ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                              ClassCondensationFluxCorrectionAqueousVersion1,
                                                                              ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
                else if (flux_correction_aqueous_type == "version2")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                              ClassCondensationFluxCorrectionAqueousVersion2,
                                                                              ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                              ClassCondensationFluxCorrectionAqueousVersion2,
                                                                              ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
#endif
                else
                  throw AMC::Error("Aqueous flux correction type \"" + flux_correction_aqueous_type + "\" not implemented.");
              }
            else if (correction_factor_type == "fuchs-sutugin")
              {
                if (flux_correction_aqueous_type == "void")
                  parameterization_list_[i] =
                    new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                          ClassCondensationFluxCorrectionAqueousVoid,
                                                                          ClassCondensationFluxCorrectionDryVoid>(ops);
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
                else if (flux_correction_aqueous_type == "version1")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                              ClassCondensationFluxCorrectionAqueousVersion1,
                                                                              ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                              ClassCondensationFluxCorrectionAqueousVersion1,
                                                                              ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
                else if (flux_correction_aqueous_type == "version2")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                              ClassCondensationFluxCorrectionAqueousVersion2,
                                                                              ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                              ClassCondensationFluxCorrectionAqueousVersion2,
                                                                              ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
#endif
                else
                  throw AMC::Error("Aqueous flux correction type \"" + flux_correction_aqueous_type + "\" not implemented.");
              }
            else
              throw AMC::Error("Correction factor formula \"" + correction_factor_type + "\" not implemented.");
          }
#if AMC_WITH_DIFFUSION_LIMITED_SOOT
        else if (type == "diffusion_limited_soot") // Diffusion limited parameterization for soot like particles.
          {
            if (correction_factor_type == "dahneke")
              {
                if (flux_correction_aqueous_type == "void")
                  parameterization_list_[i] =
                    new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                              ClassCondensationFluxCorrectionAqueousVoid,
                                                                              ClassCondensationFluxCorrectionDryVoid>(ops);
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
                else if (flux_correction_aqueous_type == "version1")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion1,
                                                                                  ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                                    ClassCondensationFluxCorrectionAqueousVersion1,
                                                                                    ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
                else if (flux_correction_aqueous_type == "version2")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion2,
                                                                                  ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion2,
                                                                                  ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
#endif
                else
                  throw AMC::Error("Aqueous flux correction type \"" + flux_correction_aqueous_type + "\" not implemented.");
              }
            else if (correction_factor_type == "fuchs-sutugin")
              {
                if (flux_correction_aqueous_type == "void")
                  parameterization_list_[i] =
                    new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                                ClassCondensationFluxCorrectionAqueousVoid,
                                                                                ClassCondensationFluxCorrectionDryVoid>(ops);
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
                else if (flux_correction_aqueous_type == "version1")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion1,
                                                                                  ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion1,
                                                                                  ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
                else if (flux_correction_aqueous_type == "version2")
                  {
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
                    if (flux_correction_dry_inorganic_salt)
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion2,
                                                                                  ClassCondensationFluxCorrectionDryInorganicSalt>(ops);
                    else
#endif
                      parameterization_list_[i] =
                        new ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                                  ClassCondensationFluxCorrectionAqueousVersion2,
                                                                                  ClassCondensationFluxCorrectionDryVoid>(ops);
                  }
#endif
                else
                  throw AMC::Error("Aqueous flux correction type \"" + flux_correction_aqueous_type + "\" not implemented.");
              }
            else
              throw AMC::Error("Correction factor formula \"" + correction_factor_type + "\" not implemented.");
          }
#endif
        else
          throw AMC::Error("Condensation/evaporation parameterization \"" + type + "\" not implemented.");

        // Which section to assign to this parameterization.
        if (ops.Exists("section"))
          {
            ops.Set("section", "", parameterization_section_[i]);
            for (int j = 0; j < int(parameterization_section_[i].size()); j++)
              section_assigned[parameterization_section_[i][j]] = true;
          }
      }

    // Assign not specified sections to default, which is the first one.
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      if (! section_assigned[i])
        parameterization_section_[0].push_back(i);

#ifdef AMC_WITH_LOGGER
    for (int i = 0; i < Nparam_; i++)
      {
        *AMCLogger::GetLog() << Byellow() << Debug(2) << Reset() << "Parameterization " << i
                             << " : " << parameterization_list_[i]->GetName() << endl;
        *AMCLogger::GetLog() << Fyellow() << Debug(3) << Reset()
                             << "List of couples assigned to parameterization " << i
                             << " : " <<  parameterization_section_[i] << endl;
      }
#endif

    //
    // Load kelvin effect.
    //

#ifdef AMC_WITH_KELVIN_EFFECT
    ops.SetPrefix(prefix_condensation);
    if (ops.Exists("kelvin_effect"))
      {
        ops.SetPrefix(prefix_condensation + "kelvin_effect.");
        kelvin_effect_ = new ClassParameterizationKelvinEffect(ops);
      }
#endif

    //
    // Load equilibrium model list and some parameters.
    //

    ops.SetPrefix(prefix_condensation + "equilibrium.");

    qssa_threshold_min_ = ops.Get<real>("qssa_threshold.min", "", qssa_threshold_min_);
    qssa_threshold_max_ = ops.Get<real>("qssa_threshold.max", "", qssa_threshold_max_);

    // BFGS data.
    Niter_max_ = ops.Get<int>("bfgs.Niter_max", "", Niter_max_);
    accuracy_ = ops.Get<double>("bfgs.accuracy", "", accuracy_);
    pgtol_ = ops.Get<double>("bfgs.pgtol", "", pgtol_);
    epsilon_ = ops.Get<real>("bfgs.epsilon", "", epsilon_);

    vector1s model_list = ops.GetEntryList("model");
    Nmodel_equilibrium_ = int(model_list.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Nmodel_equilibrium = " << Nmodel_equilibrium_ << endl;
#endif
    
    equilibrium_model_list_.resize(Nmodel_equilibrium_);

    for (int i = 0; i < Nmodel_equilibrium_; i++)
      {
        ops.SetPrefix(prefix_condensation + "equilibrium.model." + model_list[i] + ".");

        string type = ops.Get<string>("type");

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug() << Reset()
                             << "Instantiate condensation equlibrium model of type \"" << type << "\"." << endl;
#endif

        if (type == "void")
          {
            ClassModelThermodynamicVoid param;
            equilibrium_model_list_[i] = param.clone();
          }
#ifdef AMC_WITH_ISOROPIA
        else if (type == "isoropia")
          {
            ClassModelThermodynamicIsoropia param(ops);
            equilibrium_model_list_[i] = param.clone();
          }
#endif
#ifdef AMC_WITH_PANKOW
        else if (type == "pankow")
          {
            ClassModelThermodynamicPankow param(ops);
            equilibrium_model_list_[i] = param.clone();
          }
#endif
#ifdef AMC_WITH_AEC
        else if (type == "aec")
          {
            ClassModelThermodynamicAEC param(ops);
            equilibrium_model_list_[i] = param.clone();
          }
#endif
#ifdef AMC_WITH_H2O
        else if (type == "h2o")
          {
            ClassModelThermodynamicH2O param(ops);
            equilibrium_model_list_[i] = param.clone();
          }
#endif
        else
          throw AMC::Error("Model \"" + type + "\" not implemented, " +
                           "valid types are \"void\", \"isoropia\", \"pankow\", \"aec\" or \"h2o\".");
      }
  }


  // Clear static data.
  void ClassDynamicsCondensation::Clear()
  {
#ifdef AMC_WITH_KELVIN_EFFECT
    if (kelvin_effect_ != NULL)
      delete kelvin_effect_;
    kelvin_effect_ = NULL;
#endif

    Niter_max_ = AMC_CONDENSATION_NITER_MAX_DEFAULT;
    accuracy_ = AMC_CONDENSATION_ACCURACY_DEFAULT;
    pgtol_ = AMC_CONDENSATION_PGTOL_DEFAULT;
    epsilon_ = AMC_CONDENSATION_EPSILON_DEFAULT;
    qssa_threshold_min_ = AMC_CONDENSATION_QSSA_THRESHOLD_MIN_DEFAULT;
    qssa_threshold_max_ = AMC_CONDENSATION_QSSA_THRESHOLD_MAX_DEFAULT;
    Nparam_ = 0;
    Nmodel_equilibrium_ = 0;
    equilibrium_model_list_.clear();
    parameterization_list_.clear();
    parameterization_section_.clear();
  }


  // Set physical properties needed by parameterizations.
  void ClassDynamicsCondensation::InitStep()
  {
    ClassMeteorologicalData::UpdateCondensation();
  }


  // Get methods.
  int ClassDynamicsCondensation::GetNparam()
  {
    return Nparam_;
  }


  ClassParameterizationCondensationBase* ClassDynamicsCondensation::GetParameterizationPtr(const int &i)
  {
    return parameterization_list_[i];
  }


#ifdef AMC_WITH_KELVIN_EFFECT
  ClassParameterizationKelvinEffect* ClassDynamicsCondensation::GetKelvinEffectPtr()
  {
    return kelvin_effect_;
  }
#endif


  // Solves global equilibrium between several sections.
  void ClassDynamicsCondensation::GlobalEquilibriumBulk(const int section_min,
                                                        const int section_max,
                                                        const real *concentration_aer_number,
                                                        real *concentration_aer_mass,
                                                        real *concentration_gas)
  {
    // We first compute c/e coefficient and rates,
    // assuming local thermodynamics has been updated.
    vector1r rate_aer_mass(ClassAerosolData::NgNgas_, real(0));
    vector1i species_at_equilibrium(ClassSpecies::Ngas_, 0);

    out_of_equilibrium_species_mass_transfer_(section_min,
                                              section_max,
                                              concentration_aer_number,
                                              concentration_aer_mass,
                                              concentration_gas,
                                              species_at_equilibrium,
                                              rate_aer_mass);

    // Then call the global thermodynamic models.
    // It is implicitely assumed that all sections
    // have the same configuration : one section may call
    // several models which manages independent set of species,
    // but all models have to be called for all sections at equilibrium.
    real liquid_water_content(real(0));
    vector1r concentration_aer_total(ClassSpecies::Nspecies_, real(0)),
      concentration_aer_equilibrium(ClassSpecies::Nspecies_, real(0)),
      equilibrium_aer_internal(ClassSpeciesEquilibrium::Nspecies_, real(0)),
      concentration_gas_equilibrium(ClassSpecies::Ngas_, real(0)),
      concentration_gas_apparent(ClassSpecies::Ngas_, real(0));

    // If species severely condensed or evaporated from previous routine,
    // we avoid to hand it over to global equilibrium method.
    // Nevertheless we let the bulk gas concentration in case of evaporation.
    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      if (species_at_equilibrium[i] <= 0)
        concentration_gas_apparent[i] = concentration_gas[i];

    for (int i = 0; i < Nmodel_equilibrium_; i++)
      equilibrium_model_list_[i]->ComputeGlobalEquilibrium(section_min,
                                                           section_max,
                                                           concentration_gas_apparent.data(),
                                                           concentration_aer_mass,
                                                           concentration_aer_total,
                                                           equilibrium_aer_internal,
                                                           liquid_water_content,
                                                           concentration_aer_equilibrium,
                                                           concentration_gas_equilibrium);

    // Now, we need to redistribute the equilibrium on general sections.
    // Note that this has nothing to do with the size and
    // composition redistribution which has to take place after.

    // Perform the redistribution for gas species.
    // They are redistributed according to the previous rates.
    // If the overall variation is positive (condensation),
    // we redistribute only on sections for which rate was indeed positive,
    // and vice versa for evaporation.

    // This is here our sole chance to get a somewhat kind of external
    // mixing redistribution (if you work in external mixing) as rates
    // for one species and one size section may be different according
    // to the class composition and its particular composition which in
    // turn affect the equilibrium gas surface concentration.
    for (int h = 0; h < ClassSpecies::Ngas_; h++)
      {
        // If species was already put at equilibrium before, avoid redistribution.
        if (species_at_equilibrium[h] != 0)
          continue;

        // General species index in AMC.
        int i = ClassSpecies::semivolatile_[h];

        // Aerosol mass variation after and before equilibrium. Positive or negative.
        real variation_concentration_aer = concentration_aer_equilibrium[i] - concentration_aer_total[i];

        // Overall condensation or evaporation ?
        // Guess over which redistribute the overall variation.
        real rate_aer_mass_cond(real(0)), rate_aer_mass_evap(real(0));
        int Nsection_condensation(0), Nsection_evaporation(0);
        vector1i section_to_condensate(ClassAerosolData::Ng_, 0),
          section_to_evaporate(ClassAerosolData::Ng_, 0);

        for (int j = section_min; j < section_max; j++)
          {
            int k = j * ClassSpecies::Ngas_ + h;

            if (rate_aer_mass[k] >= real(0))
              {
                rate_aer_mass_cond += rate_aer_mass[k];
                section_to_condensate[Nsection_condensation++] = j;
              }
            else
              {
                // Need positive rate.
                rate_aer_mass_evap -= rate_aer_mass[k];
                section_to_evaporate[Nsection_evaporation++] = j;
              }
          }

        // Try to guess net condensation and evaporation rate.
        real ratio, variation_concentration_aer_cond,
          variation_concentration_aer_evap;

        if (variation_concentration_aer > real(0) &&
            rate_aer_mass_cond > real(0))
          {
            ratio = (rate_aer_mass_cond + rate_aer_mass_evap) / rate_aer_mass_cond;
            // Variations are in absolute value.
            variation_concentration_aer_cond = ratio * variation_concentration_aer;
            variation_concentration_aer_evap = variation_concentration_aer_cond
              - variation_concentration_aer;
          }
        else if (variation_concentration_aer < real(0) &&
                 rate_aer_mass_evap < real(0))
          {
            // Need positive ratio.
            ratio = (rate_aer_mass_evap + rate_aer_mass_cond) / rate_aer_mass_evap;
            // Variations are in absolute value.
            variation_concentration_aer_evap = - ratio * variation_concentration_aer;
            variation_concentration_aer_cond = variation_concentration_aer_evap
              - variation_concentration_aer;
          }
        else
          {
            // Overall condensation but no positive flux !
            // or overall evaporation but no negative flux !
            // This should not happen and we would throw an exception here.
            // But to prevent from terminating 3D applications, we do nothing,
            // hoping the problem will be solved on next iteration.
            Nsection_condensation = 0;
            Nsection_evaporation = 0;
            variation_concentration_aer = real(0);
          }

        // Firs redistribute evaporation.
        for (int j = 0; j < Nsection_evaporation; j++)
          {
            int k = section_to_evaporate[j];

            // This mass is negative because of rate_aer_mass.
            real mass_redistributed = variation_concentration_aer_evap
              * rate_aer_mass[k * ClassSpecies::Ngas_ + h] / rate_aer_mass_evap;

            // The equilibrium concentration for this section.
            int l = k * ClassSpecies::Nspecies_ + i;

            real concentration_aer_mass_section_equilibrium = 
              concentration_aer_mass[l] + mass_redistributed;

            // If negative due to evaporation, we set the new concentration to zero,
            // and transfer the lack of evaporation for this section to the gas phase.
            // Thus mass conservation is ensured.
            if (concentration_aer_mass_section_equilibrium < real(0))
              {
                // Variation between current mass and redistributed one is negative.
                // This diminuishes the overall variation at the expense of gas phase.
                variation_concentration_aer += (concentration_aer_mass[l] + mass_redistributed);
                concentration_aer_mass_section_equilibrium = real(0);
              }

            concentration_aer_mass[l] = concentration_aer_mass_section_equilibrium;
          }

        // Now redistribute condensation.
        for (int j = 0; j < Nsection_condensation; j++)
          {
            int k = section_to_condensate[j];

            // Positive mass.
            real mass_redistributed = variation_concentration_aer_cond
              * rate_aer_mass[k * ClassSpecies::Ngas_ + h] / rate_aer_mass_cond;

            // The equilibrium concentration for this section. No risk of clipping.
            concentration_aer_mass[k * ClassSpecies::Nspecies_ + i] += mass_redistributed;
          }

        // In the end, update bulk gas concentration.
        // The gas phase gains (loses) what the aerosol phase loses (gains).
        concentration_gas[h] -= variation_concentration_aer;

        // Avoid clipping, but by construction this should not happen.
        concentration_gas[h] = concentration_gas[h] > real(0) ? concentration_gas[h] : real(0);
      }
  }


  // Solves global equilibrium between several sections with a size resolved method.
  void ClassDynamicsCondensation::GlobalEquilibriumSectionResolved(const int section_min,
                                                                   const int section_max,
                                                                   const real *concentration_aer_number,
                                                                   real *concentration_aer_mass,
                                                                   real *concentration_gas)
  {
    // We first compute c/e coefficient and rates,
    // assuming local thermodynamics has been updated.
    vector1r rate_aer_mass(ClassAerosolData::NgNgas_, real(0));
    vector1i species_at_equilibrium(ClassSpecies::Ngas_, 0);

    out_of_equilibrium_species_mass_transfer_(section_min,
                                              section_max,
                                              concentration_aer_number,
                                              concentration_aer_mass,
                                              concentration_gas,
                                              species_at_equilibrium,
                                              rate_aer_mass);

    // If species severely condensed or evaporated from previous routine,
    // we avoid to hand it over to global equilibrium method.
    vector1r concentration_gas_apparent(ClassSpecies::Ngas_, real(0));

    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      if (species_at_equilibrium[i] <= 0)
        concentration_gas_apparent[i] = concentration_gas[i];

    //
    // Compute size resolved equilibrium for remaining species with a minimization method.
    //

    // Guess which gas species to minimize.
    int Ngas(0);
    vector1i species_index(ClassSpecies::Ngas_, 0),
      species_index_gas(ClassSpecies::Ngas_, 0);
    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      if (species_at_equilibrium[i] != 0)
        {
          species_index_gas[Ngas] = i;
          species_index[Ngas++] = ClassSpecies::semivolatile_[i];
        }

    // Guess which sections to minimize, avoid empty ones.
    int Nsection(0);
    vector1i section_index(ClassDiscretizationSize::Nsection_);
    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        section_index[Nsection++] = i;

    // Finite difference.
    real epsilon = real(1) + epsilon_;

    // Setulb parameters.
    int Nmetric = 5;
    int Nparam = Nsection * Ngas;
    vector<double> lbound(Nparam, double(0));
    vector<double> ubound(Nparam, double(0));
    vector<int> bound_type(Nparam, 2);
    string task(' ', 61);
    int iprint = -1;
    int fint = 1;
    int work_arr_length = (2 * Nmetric + 4) * Nparam
      + (11 * Nmetric + 8) * Nmetric;
    vector<double> work_arr(work_arr_length);
    vector<int> work_iarr(3 * Nparam);
    vector<char> work_carr(60);
    vector<int> work_larr(4), work_iarr0(44);
    vector<double> work_arr0(29);

    task.substr(0, 5) = "START";

    // Mass conservation.
    vector1r mass_conservation(Ngas, real(0));
    for (int i = 0; i < Ngas; i++)
      {
        mass_conservation[i] = concentration_gas_apparent[species_index_gas[i]];
        int j = species_index[i];
        for (int k = 0; k < Nsection; k++)
          mass_conservation[i] += concentration_aer_mass[section_index[k] * ClassSpecies::Nspecies_ + j];
      }

    // Unkown vector.
    vector<double> x(Nparam);
    int h(0);
    for (int i = 0; i < Nsection; i++)
      for (int j = 0; j < Ngas; j++)
        x[h++] = concentration_aer_mass[section_index[i] * ClassSpecies::Nspecies_ + species_index[j]];

    int Niter(0);
    while (task.substr(0, 4) != "STOP" && Niter < Niter_max_)
      {
        // Compute thermodynamics.
        ClassThermodynamics::Compute(section_min,
                                     section_max,
                                     concentration_aer_number,
                                     concentration_aer_mass);

        // Compute diameters. This uses lwc computed by thermodynamics.
        ClassParameterization::ComputeParticleDiameter(section_min, section_max,
                                                       concentration_aer_number,
                                                       concentration_aer_mass);

#ifdef AMC_WITH_KELVIN_EFFECT
        // Compute Kelvin effect.
        vector1r kelvin_effect(ClassAerosolData::NgNphase_, real(1));

        if (kelvin_effect_ != NULL)
          kelvin_effect_->ComputeKelvinEffect(section_min, section_max,
                                              concentration_aer_number,
                                              concentration_aer_mass,
                                              kelvin_effect);
#endif

        // Compute rates.
        rate_aer_mass.assign(ClassAerosolData::NgNgas_, real(0));
        vector1r condensation_coefficient(ClassAerosolData::NgNgas_);

        for (int i = 0; i < Nparam_; i++)
          parameterization_list_[i]->ComputeKernel(section_min, section_max,
                                                   parameterization_section_[i],
                                                   concentration_aer_number,
                                                   concentration_aer_mass,
                                                   concentration_gas_apparent.data(),
                                                   condensation_coefficient,
                                                   rate_aer_mass);

        // Unkown vector upper bound.
        // It may not exceed the maximum of bulk gas condensation.
        for (int i = 0; i < Ngas; i++)
          {
            int j = species_index_gas[i];

            real condensation_coefficient_total(real(0));

            for (int k = 0; k < Nsection; k++)
              condensation_coefficient_total +=
                condensation_coefficient[section_index[k] * ClassSpecies::Ngas_ + j];

            real tmp = concentration_gas_apparent[j] / condensation_coefficient_total;
            for (int k = 0; k < Nsection; k++)
              ubound[k * Ngas + i] = tmp * condensation_coefficient[section_index[k] * ClassSpecies::Ngas_ + j];
          }

        // Compute function.
        double function(double(0));
        for (int i = 0; i < Nsection; i++)
          for (int j = 0; j < Ngas; j++)
            {
              int k = section_index[i] * ClassSpecies::Ngas_ + species_index_gas[j];
              function += rate_aer_mass[k] * rate_aer_mass[k];
            }
        function *= double(0.5);

        // Compute gradient.
        vector<double> gradient(Nparam, double(0));

        // First part of gradient is analytic, because of mass conservation.
        for (int j1 = 0; j1 < Ngas; j1++)
          for (int i1 = 0; i1 < Nsection; i1++)
            {
              int k1 = i1 * Ngas + j1;
              for (int i2 = 0; i2 < Nsection; i2++)
                {
                  int k2 = section_index[i2] * ClassSpecies::Ngas_ + species_index_gas[j1];
                  gradient[k1] -= rate_aer_mass[k2] * condensation_coefficient[k2];
                }
            }

        // Second part concerns thermodynamic and must be done numerically, section by section.
        for (int j1 = 0; j1 < Ngas; j1++)
          {
            vector1r concentration_aer_mass_perturbed(concentration_aer_mass,
                                                      concentration_aer_mass + ClassAerosolData::NgNspecies_);

            for (int i1 = 0; i1 < Nsection; i1++)
              {
                int k1 = section_index[i1];
                int l1 = k1 * ClassSpecies::Nspecies_ + species_index[j1];

                // If species not present, we assume perturbation
                // to be epsilon_ percent or total particle mass.
                if (concentration_aer_mass_perturbed[l1] > real(0))
                  concentration_aer_mass_perturbed[l1] *= epsilon;
                else
                  concentration_aer_mass_perturbed[l1] =
                    concentration_aer_number[k1] * ClassAerosolData::mass_[k1] * epsilon_;
              }

            // Compute thermodynamics.
            ClassThermodynamics::Compute(section_min, section_max,
                                         concentration_aer_number,
                                         concentration_aer_mass_perturbed.data());
#ifdef AMC_WITH_KELVIN_EFFECT
            // Correct with already compute Kelvin effect.
            CorrectEquilibriumGasSurfaceWithKelvinEffect(section_min, section_max,
                                                         concentration_aer_number,
                                                         kelvin_effect);
#endif

            // Compute perturbed rates.
            vector1r rate_aer_mass_perturbed(ClassAerosolData::NgNgas_, real(0));

            // Diameters and Kelvin effect are not recomputed.
            // Assume main difference comes from equilibrium_gas_surface.
            for (int i = 0; i < Nparam_; i++)
              parameterization_list_[i]->ComputeKernel(section_min, section_max,
                                                       parameterization_section_[i],
                                                       concentration_aer_number,
                                                       concentration_aer_mass_perturbed.data(),
                                                       concentration_gas_apparent.data(),
                                                       condensation_coefficient,
                                                       rate_aer_mass_perturbed);

            for (int i1 = 0; i1 < Nsection; i1++)
              {
                int k1 = i1 * Ngas + j1;
                int l1 = section_index[i1];

                for (int j2 = 0; j2 < Ngas; j2++)
                  {
                    int k2 = l1 * ClassSpecies::Ngas_ + species_index_gas[j2];
                    gradient[k1] += (rate_aer_mass[k2] - rate_aer_mass_perturbed[k2]) / epsilon_;
                  }
              }
          }

        setulb_(&Nparam, &Nmetric, x.data(), lbound.data(),
                ubound.data(), bound_type.data(), &function,
                gradient.data(), &accuracy_, &pgtol_,
                work_arr.data(), work_iarr.data(), (char*)task.c_str(), &iprint,
                work_carr.data(), work_larr.data(), work_iarr0.data(),
                work_arr0.data(), &fint, &fint);

        if (task.substr(0, 5) == "NEW_X")
          setulb_(&Nparam, &Nmetric, x.data(), lbound.data(),
                  ubound.data(), bound_type.data(), &function,
                  gradient.data(), &accuracy_, &pgtol_,
                  work_arr.data(), work_iarr.data(), (char*)task.c_str(), &iprint,
                  work_carr.data(), work_larr.data(), work_iarr0.data(),
                  work_arr0.data(), &fint, &fint);

        // Give back x to concentration vector.
        h = 0;
        for (int i = 0; i < Nsection; i++)
          for (int j = 0; j < Ngas; j++)
            concentration_aer_mass[section_index[i] * ClassSpecies::Nspecies_ + species_index[j]] = x[h++];

        // Update gas concentration thanks to mass conservation.
        for (int i = 0; i < Ngas; i++)
          {
            int j = species_index_gas[i];
            int k = species_index[i];

            concentration_gas_apparent[j] = mass_conservation[i];
            for (int l = 0; l < Nsection; l++)
              concentration_gas_apparent[j] -=
                concentration_aer_mass[section_index[l] * ClassSpecies::Nspecies_ + k];

            // Prevent clipping.
            concentration_gas_apparent[j] =
              concentration_gas_apparent[j] > real(0) ? concentration_gas_apparent[j] : real(0);
          }

        Niter++;
      }

    // At last update gas concentration with mass conservation.
    for (int i = 0; i < Ngas; i++)
      {
        int j = species_index_gas[i];
        int k = species_index[i];

        concentration_gas[j] = mass_conservation[i];
        for (int l = 0; l < Nsection; l++)
          concentration_gas[j] -= concentration_aer_mass[section_index[l] * ClassSpecies::Nspecies_ + k];

        // Prevent clipping.
        concentration_gas[j] = concentration_gas[j] > real(0) ? concentration_gas[j] : real(0);
      }
  }


  // Correct equilibrium gas surface with Kelvin effect.
  void ClassDynamicsCondensation::CorrectEquilibriumGasSurfaceWithKelvinEffect(const int section_min,
                                                                               const int section_max,
                                                                               const real *concentration_aer_number,
                                                                               const vector1r &kelvin_effect)
  {
    // Loop on sections, if not too few particles.
    for (int i = section_min; i < section_max; i++)
      if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
        {
          // Start index in concentration mass.
          int j = i * ClassSpecies::Nspecies_;

          // Start index in kelvin effect and tension surface.
          int k = i * ClassPhase::Nphase_;

          // Gas concentration index.
          int l = i * ClassSpecies::Ngas_;

          // Correct the equilibrium gas surface concentration, but only if it does not come from aDsorption.
          for (int m = 0; m < ClassSpecies::Ngas_; m++)
            {
              // Position in gas concentration vector.
              int n = l + m;

              // Correct only if it does not come from aDsorption.
              if (ClassAerosolData::equilibrium_gas_surface_[n] >= real(0))
                if (ClassAerosolData::adsorption_coefficient_[n] < real(0))
                  {
                    // The phase index where lies the semivolatile species.
                    const int &phase_index = ClassAerosolData::phase_[j + ClassSpecies::semivolatile_[m]];

                    // Account for Kelvin effect, eventually.
                    ClassAerosolData::equilibrium_gas_surface_[n] *= kelvin_effect[k + phase_index];
                  }
            }
        }
  }


  // Compute lagrangian rate for given general sections. To get eulerian ones, you need to redistribute it.
  void ClassDynamicsCondensation::RateLagrangian(const int section_min,
                                                 const int section_max,
                                                 const real *concentration_aer_number,
                                                 const real *concentration_aer_mass,
                                                 const real *concentration_gas,
                                                 vector1r &rate_aer_mass,
                                                 vector1r &rate_gas)
  {
    vector1r condensation_coefficient(ClassAerosolData::NgNgas_),
      rate_aer_mass_local(ClassAerosolData::NgNspecies_, real(0));

#ifdef AMC_WITH_KELVIN_EFFECT
    vector1r kelvin_effect(ClassAerosolData::NgNphase_, real(1));

    if (kelvin_effect_ != NULL)
      kelvin_effect_->ComputeKelvinEffect(section_min, section_max,
                                          concentration_aer_number,
                                          concentration_aer_mass,
                                          kelvin_effect);

#ifdef AMC_WITH_DEBUG_CONDENSATION
    cout << "kelvin_effect = " << kelvin_effect << endl;
#endif

#endif

    // All the work is sent to the parameterizations as it involves species dependent computation.
    for (int i = 0; i < Nparam_; i++)
      parameterization_list_[i]->ComputeKernel(section_min, section_max,
                                               parameterization_section_[i],
                                               concentration_aer_number,
                                               concentration_aer_mass,
                                               concentration_gas,
                                               condensation_coefficient,
                                               rate_aer_mass_local);

#ifdef AMC_WITH_DEBUG_CONDENSATION
    cout << "condensation_coefficient = " << condensation_coefficient << endl;
    cout << "rate_aer_mass = " << rate_aer_mass_local << endl;
#endif

    // Do not forget we possibly add the condensation rate to other things (coagulation, ...).
    for (int i = section_min; i < section_max; i++)
      {
        const int j = i * ClassSpecies::Nspecies_;

        for (int k = 0; k < ClassSpecies::Ngas_; k++)
          {
            const int l = ClassSpecies::semivolatile_[k];

            const int m = j + l;
            rate_aer_mass[m] += rate_aer_mass_local[m];

            // Only substract the condensation part of rate.
            rate_gas[k] -= rate_aer_mass_local[m];
          }
      }
  }


  // Growth time scale for condensation.
  void ClassDynamicsCondensation::ComputeTimeScaleGrowth(const real *concentration,
                                                         const vector<real> &rate,
                                                         vector<int> &time_scale_index,
                                                         vector<real> &time_scale_value)
  {
    // Total mass concentration and rate.
    vector1r concentration_total(ClassAerosolData::Ng_, real(0)),
      rate_total(ClassAerosolData::Ng_, real(0));

    int h(0);
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      for (int j = 0; j < ClassSpecies::Nspecies_; j++)
      {
        concentration_total[i] += concentration[h];
        rate_total[i] += rate[h];
        h++;
      }

    ClassDynamicsBase::ComputeTimeScale(concentration_total.data(),
                                        rate_total,
                                        time_scale_index,
                                        time_scale_value);
  }


  map<string, real> ClassDynamicsCondensation::ComputeTimeScaleGrowth(const real *concentration,
                                                                      const vector<real> &rate)
  {
    vector<int> time_scale_index;
    vector<real> time_scale_value;

    ComputeTimeScaleGrowth(concentration,
                           rate,
                           time_scale_index,
                           time_scale_value);

    int h = get_power_out_of(ClassAerosolData::Ng_) + 1;

    map<string, real> time_scale;
    for (int i = 0; i < ClassAerosolData::Ng_; i++)
      {
        const int &j = time_scale_index[i];
        ostringstream name;
        name << setw(h) << setfill('0') << i
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[j][0]
             << "_" << setw(2) << setfill('0') << ClassAerosolData::general_section_[j][1];

        if (time_scale_value[j] < numeric_limits<real>::max())
          time_scale[name.str()] = time_scale_value[j];
      }

    return time_scale;
  }
}

#define AMC_FILE_CLASS_DYNAMICS_CONDENSATION_CXX
#endif
