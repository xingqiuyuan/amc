%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\restylefloat{table}
\usepackage[lined, boxed, commentsnumbered]{algorithm2e}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}

\title{Henry constant}

%\date{}
\author{Edouard Debry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Definition}

The Henry constant is the equilibrium constant of the change state reaction of component $X_i$ from the gas phase to the aqueous
phase :
\begin{equation}\label{eq:Hi}
  (X_i)_g \rightleftharpoons (X_i)_{aq}~,~~H_i = \frac{[X_i]}{p_i}
\end{equation}
where
\begin{itemize}
\item[$\circ$] $[X_i]$ is the mole concentration of $X_i$ in the aqueous phase per water volume, in $\text{mol}.\text{L}^{-1}$,
\item[$\circ$] $p_i$ the partial pressure of gaseous $X_i$, in $\text{atm}$ ${\scriptstyle (1~\text{atm}~=~1.01325\times
    10^5~Pa )}$,
\end{itemize}
The Henry constant in Eq. \ref{eq:Hi} is then expressed in $\text{mol}.\text{L}^{-1}.\text{atm}^{-1}$. The higher it is, the
more the component $X_i$ absorbs in the aqueous phase.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Infinite dilution}

When the component $X_i$ is infinitely diluted, the Henry constant may be derived from the Raoul law :
\begin{equation}\label{eq:raoult}
  p_i = p_i^* \gamma_i^\infty x_i
\end{equation}
where 
\begin{itemize}
\item[$\circ$] $x_i$ is the molar fraction if $X_i$ in aqueous phase :
  \begin{equation}\label{eq:xi2}
    x_i = \frac{n_i}{n_i + n_w}
  \end{equation}
  in which the mole number of $X_i$ is linked to its aqueous concentration :
  \begin{equation}\label{eq:ni}
    [X_i] = \frac{n_i \rho_w}{n_w M_w} 
  \end{equation}
  with $\rho_w$ the water density, in $g.\text{L}^{-1}$, and $M_w$ its molar mass, in $g.\text{mol}^{-1}$.
\item[$\circ$] $\gamma_i^{\infty}$ is the activity coefficient of component $X_i$ when infinitely diluted, that is to say when $X_i$
  is only surrounded by water molecules, it then stands for the affinity of component $X_i$ with them, it is less than unity if $X_i$
  has some affinities, greater otherwise.
\end{itemize}

Reverting Eq. \ref{eq:xi2} and using Eq. \ref{eq:ni} leads to :
\begin{equation}\label{eq:ni2}
  [X_i] = \frac{x_i}{1 - x_i}\frac{\rho_w}{M_w}\sim x_i\frac{\rho_w}{M_w}~\text{when}~x_i\rightarrow 0
\end{equation}
Substituting $x_i$ in Eq. \ref{eq:raoult} by its latest expression (Eq. \ref{eq:ni2}) finally gives :
\begin{equation}\label{eq:raoult2}
  p_i = p_i^* \gamma_i^\infty [X_i] \frac{M_w}{\rho_w}
\end{equation}
from which we derive the Henry constant in infinite dilution conditions :
\begin{equation}\label{eq:Hip}
  H_i^\infty = \frac{\rho_w}{M_w\gamma_i^\infty p_i^*}
\end{equation}

Thus, the Henry constant for one pure component in the case of infinite dilution is all the greater as its saturation vapor
pressure is low or as it has more affinity with water molecules.

In other words, one semivolatile species with a low saturation vapor pressure but no affinity with water, i.e. insoluble, may have
a low Henry constant because of an activity coefficient much greater than unity.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Non diluted conditions}

The Henry's constant in infinite dilution conditions from Eq. \ref{eq:Hip} is usually the only available value, either computed or
measured, so that far from dilute conditions the Henry constant in Eq. \ref{eq:Hi} is computed from the infinite dilution value by
correcting the activity coefficient :
\begin{equation}\label{eq:Hi3}
  H_i = \frac{\gamma_i^\infty}{\gamma_i} H_i^\infty = \frac{H_i^\infty}{\zeta_i}~,~~\zeta_i = \frac{\gamma_i}{\gamma_i^\infty}
\end{equation}
where $\gamma_i$ is the activity coefficient of component $X_i$ in the complex aqueous mixture. The activity coefficient ratio
$\zeta_i$ can be viewed as the distance of the complex mixture from infinite dilution.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Partition coefficient}

The gas/particle partitioning is usually expressed with one partitioning coefficient $K_{aq}$ defined as follows :
\begin{equation}\label{eq:Kaq}
  K_{aq} = \frac{A_i}{G_i \text{LWC}}
\end{equation}
where
\begin{itemize}
\item[$\circ$] $G_i$ is the mass concentration of $X_i$ in gas phase per air volume,
\item[$\circ$] $A_i$ is the mass concentration of $X_i$ in particles per air volume,
\item[$\circ$] $\text{LWC}$ is the liquid water content of particles per air volume.
\end{itemize}
All concentrations are expressed in $\mu g.m^{-3}$ so that the unit of $K_{aq}$ is $m^{3}.\mu g^{-1}$

The gas concentration $G_i$ is related to the partial pressure $p_i$ by the perfect gas law :
\begin{equation}\label{eq:pi}
  p_i \times 1.01325 \times 10^5 = \frac{G_i}{M_i}R_g T
\end{equation}
where 
\begin{itemize}
\item[$\circ$] $M_i$ is the molar mass of component $X_i$ in $\mu g.\text{mol}^{-1}$,
\item[$\circ$] $R_g$ is the perfect gas constant ${\scriptstyle (= 8.314~J.\text{mol}^{-1}.K^{-1})}$,
\item[$\circ$] $T$ is the temperature in Kelvin.
\end{itemize}
The factor $1.01325 \times 10^5$ accounts for the pressure conversion from atm to Pascal.

The particle mass concentration $A_i$ can be related to the aqueous concentration $[X_i]$ from Eq. \ref{eq:ni} given that
\begin{equation}\label{eq:alwc}
  A_i = n_i M_i~\text{and}~\text{LWC} = n_w M_w \times 10^6
\end{equation}
The $10^6$ factor accounts for mass conversion from gram to microgram. Substituting $n_i$ and $n_w$ with Eqs. \ref{eq:alwc} into
Eq. \ref{eq:ni} gives :
\begin{equation}\label{eq:ni3}
  [X_i] = \frac{A_i \rho_w}{M_i \text{LWC}}10^6
\end{equation}
The partition coefficient from Eq. \ref{eq:Kaq} may then be derived from the Henry's constant as follows :
\begin{equation}\label{eq:Kaq1}
  \begin{split}
    K_{aq} = &\frac{1}{G_i}\frac{A_i}{\text{LWC}} = \frac{R_g T}{p_i M_i \times 1.01325 \times 10^5} \frac{[X_i] M_i}{\rho_w 10^6}\\
    = & \frac{[X_i]}{p_i} \frac{R_g T}{\rho_w \times 1.01325 \times 10^{11}} = \frac{H_i^\infty}{\zeta_i}\frac{R_g T}{\rho_w
      \times 1.01325 \times 10^{11}}\\
    = & \frac{H_i^\infty R_g T}{\rho_w \zeta_i\times 1.01325 \times 10^{11}}
  \end{split}
\end{equation}

The Henry constant is sometimes also given in
\begin{equation}
  \biggl[\frac{\text{(aq.)}~\mu g .m^{-3}}{\text{(water)}~\mu g.m^{-3}}\biggr]\bigg/\text{(gas)}~\mu g.m^{-3}
\end{equation}
so that it has in fact the same units as the partition coefficient. The Henry constant value with such units, $(H_i^\infty)'$,
is then simply the partition coefficient divided by the ratio of activity coefficients :
\begin{equation}\label{eq:Kaq2}
  (H_i^\infty)'  = \frac{H_i^\infty R_g T}{\rho_w \times 1.01325 \times 10^{11}} = \frac{H_i^\infty R_g T}{1.01325 \times 10^{14}}
\end{equation}
considering the water density constant and equal to $10^3 g.\text{L}^{-1}$

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Change with temperature}

The saturation vapor pressure for pure component ($p_i^*$) evolves with temperature thanks to the Clausius-Clapeyron relation :
\begin{equation}\label{eq:dhvap}
  p_i^*(T) = p_i^o \exp\biggl[-\frac{\Delta H_{vap}}{R_g}\biggl(\frac{1}{T} - \frac{1}{T_0}\biggr)\biggr] = C(T) \times p_i^o
\end{equation}
where $\Delta H_{vap}$ is the vaporization enthalpy in $J.\text{mol}^{-1}$, considered constant along the temperature variation.

As the Henry constant for diluted systems (Eq. \ref{eq:Hip}) is conversely proportional to the saturation vapor pressure, it also
evolves with temperature, but conversely :
\begin{equation}\label{eq:dhvap1}
  H_i^\infty(T) = \frac{(H_i^\infty)^o}{C(T)}
\end{equation}
This is also true for $(H_i^\infty)'$ except that, as its expression (Eq. \ref{eq:Kaq2}) directly depends on temperature, it needs
further corrections :
\begin{equation}\label{eq:dhvap2}
  (H_i^\infty)'(T) = \frac{T}{T_0}\frac{(H_i^\infty)^{'o}}{C(T)}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

