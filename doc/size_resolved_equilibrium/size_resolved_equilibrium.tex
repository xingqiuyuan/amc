%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\restylefloat{table}
\usepackage[lined, boxed, commentsnumbered]{algorithm2e}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}

\title{Size resolved equilibrium}

%\date{}
\author{Edouard Debry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

\cleardoublepage


\tableofcontents

\cleardoublepage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

\begin{itemize}
\item[$\square$] molar mass of species $X_i~,~~i=1,\cdots,c$ :
  \begin{equation}\label{eq:mi}
    M_i~~[\mu g .mol^{-1}]
  \end{equation}
\item[$\square$] specific density of species $X_i$ :
  \begin{equation}\label{eq:mi}
    \rho_i~~[\mu g .\mu m^{-3}]
  \end{equation}
\item[$\square$] number concentration in particle size $k=1,\cdots, s$ :
  \begin{equation}\label{eq:n}
    N^k~~[\#.m^{-3}]
  \end{equation}
  The set of number concentrations $(N^k)$ constitutes the particle size distribution.
\item[$\square$] mass concentration in particle size $k$ and species $X_i$ :
  \begin{equation}\label{eq:qi}
    Q_i^k~~[\mu g.m^{-3}]
  \end{equation}
\item[$\square$] molar concentration in particle size $k$ and species $X_i$ :
  \begin{equation}\label{eq:mi}
    M_i^k = \frac{Q_i^k}{M_i}~~[mol.m^{-3}]
  \end{equation}
\item[$\square$] total molar concentration in particle size $k$ :
  \begin{equation}\label{eq:mi}
    M^k = \sum_{i=1}^c M_i^k~~[mol.m^{-3}]
  \end{equation}
\item[$\square$] mass and molar fraction of species $X_i$ in particle size $k$ :
  \begin{equation}\label{eq:qi}
    x_i^k = \frac{M_i^k}{M^k}~,~~y_i^k = \frac{Q_i^k}{Q^k}
  \end{equation}
\item[$\square$] liquid water content of particle size $k$ :
  \begin{equation}\label{eq:qw}
    Q_w^k({\scriptstyle RH, T, Q_1^k,\dots,Q_c^k})~~[\mu g.m^{-3}]
  \end{equation}
  it is usually a complex function of relative humidity ($RH$), temperature ($T$) and particle chemical composition.
\item[$\square$] aqueous concentration of species $X_i$ in particle size $k$, if species is hydrophilic and an aqueous phase exists :
  \begin{equation}\label{eq:[i]}
    [X_i]^k = 10^{15} \frac{M_i^k}{Q_w^k}~~[mol.L^{-1}]
  \end{equation}
  where the factor $10^{15}$ accounts for the conversion from $\mu m^3$ to $L$ (liter).
\item[$\square$] volume concentration in particle size $k$ :
  \begin{equation}\label{eq:vk}
    V^k = \sum_{i=1}^c\frac{Q_i^k}{\rho_i} + \frac{Q_w^k}{\rho_w}~~[\mu m^3 .m^{-3}]
  \end{equation}
\item[$\square$] volumic diameter of particle size $k$ :
  \begin{equation}\label{eq:dpk}
    d_p^k = \biggl(\frac{6V^k}{\pi N^k}\biggr)^{\frac{1}{3}}~~[\mu m]
  \end{equation}
\item[$\square$] average density in particle size $k$ :
  \begin{equation}\label{eq:rhopk}
    \rho_p^k = \frac{Q^k}{V^k}~~[\mu g .\mu m^{-3}]
  \end{equation}
\item[$\square$] average molar mass in particle size $k$ :
  \begin{equation}\label{eq:rhopk}
    \overline{M}_k = \frac{Q^k}{M^k}~~[\mu g .mol^{-1}]
  \end{equation}
\item[$\square$] Kelvin effect for particle size $k$ :
  \begin{equation}\label{eq:etak}
    \eta(d_p^k) = \exp\biggl(\frac{4\sigma_p^k \overline{M}_k}{R_g T\rho_p^k d_p^k}\biggr)
  \end{equation}
  where $\sigma_p^k$ is the particle surface tension and $R_g = 8.314~J.mol^{-1}.K^{-1}$ the ideal gas constant.
\item[$\square$] mass concentration of species $X_i$ in the gas phase :
  \begin{equation}\label{eq:cig}
    c_i^g~~[\mu g.m^{-3}]
  \end{equation}
also known as the bulk gas concentration.
\item[$\square$] mass transfer rate between particle $k$ and gas phase for species $X_i$ :
  \begin{equation}\label{eq:dqi}
    \frac{\,d Q_i^k}{\,dt} = N^k \alpha_i(d_p^k) \biggl[c_i^g - \eta(d_p^k) c_i^{eq}{\scriptstyle (RH, T,
      Q_1^k,\dots,Q_c^k)}\biggr]
  \end{equation}
  where $\alpha_i$ is the diffusion limited condensation coefficient ($m^3.s^{-1}$) and $c_i^{eq}$ the gas concentration ($\mu
  g.m^{-3}$) of species $X_i$ over a flat surface with which the particle is in thermodynamic equilibrium. The product between
  this latest vapor concentration and the Kelvin effect is the surface vapor concentration above particles of size $k$.
\item[$\square$] equilibrium gas concentration :
  \begin{itemize}
  \item[$\circ$] Raoult's law, when all components are present in similar proportions :
    \begin{equation}\label{eq:raoult}
      (c_i^{eq})_k = c_i^* \gamma_i^k x_i^k
    \end{equation}
    where $c_i^*$ is the saturation concentration ($\mu g.m^{-3}$) of pure species $X_i$ at given temperature and $\gamma_i^k$ the
    activity coefficient of species $X_i$ in mixture of particle $k$, equal to unity in case of ideal mixture.
  \item[$\circ$] Henry's law, when all species but one may be considered as infinitely diluted solutes into the excluded one, the solvent,
    usually water :
    \begin{equation}\label{eq:henry}
      (p_i^{eq})_k = \frac{1}{H_i} \frac{\gamma_i^k}{\gamma_i^\infty} [X_i]^k~,~~(p_i^{eq})_k = \frac{(c_i^{eq})_k}{M_i}R_g T
    \end{equation}
  \end{itemize}
\item[$\square$] mass conservation :
  \begin{equation}\label{eq:ms}
    \sum_{k=1}^s Q_i^k + c_i^g = K_i
  \end{equation}
\end{itemize}

The thermodynamic equilibrium between several particles and the gas phase is achieved when the mass transfer rate
(Eq. \ref{eq:dqi}) is made void, that is to say the gas phase concentration equals the surface vapor concentration for all
particles and all species :
\begin{equation}\label{eq:equi}
 i=1,\cdots,c~,~~c_i^g = \eta(d_p^k) (c_i^{eq})_k~,~~k=1,\cdots,s
\end{equation}
That is to note the thermodynamic equilibrium, written like this, does not depend on the particle size distribution.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{One particle and one component}

When there is only one component ($X_1$), the equilibrium gas concentration ($c_i^{eq}$) becomes equal to the saturation vapor
concentration of pure component ($c_i^*$) and Eq. \ref{eq:equi} simplifies to :
\begin{equation}\label{eq:equi0}
c_1^g = \eta(d_p^1) c_1^*
\end{equation}
Helping us with mass conservation (Eq. \ref{eq:ms}) and the Kelvin effect expression (Eq. \ref{eq:etak}), Eq. \ref{eq:equi0}
can be developed into
\begin{equation}\label{eq:equi0}
K_1 - N^1 \rho_1 \frac{\pi}{6}(d_p^1)^3 = \exp\biggl(\frac{4\sigma_1 M_1}{R_g T\rho_1 d_p^1}\biggr) c_1^*
\end{equation}
which an equation with the particle diameter ($d_p^1$) as unknown.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Two particles and one component}

The case of two particles, or even more, with one single component remains similar. Eqs. \ref{eq:equi} simplifies to :
\begin{equation}\label{eq:equi1}
c_1^g = \eta(d_p^k) c_1^*~,~~k=1,2
\end{equation}
The left hand term does not depend of the particle whereas the right hand term does, so at most one particle size may subsist at
thermodynamic equilibrium :
\begin{itemize}
\item[$\square$] if $\forall k\in\{1,2\}~,~c_i^g < \eta(d_p^k) c_i^*~$, then both particles shrink until complete vanishement as
  the previous alone particle.
\item[$\square$] if $\forall k\in\{1,\cdots, s\}~,~c_i^g > \eta(d_p^k) c_i^*~$, then both particles grow endlessly. Nevertheless,
  if they end up with a different diameter, their surface 

 condense, reducing their Kelvin effect and
  enhancing condensation as long as the bulk gas concentration remains much greater than $c_i^*$. In this case, all particles may
  grow up to the particle size for which Kelvin effect becomes negligible. On the contrary, if the bulk gas concentration meets the
  surface vapor concentrations of some particles, beginning by the lowest ones, these one may evaporate endlessly as their surface
  vapor concentration will begin to grow and the bulk gas concentration may still diminuish by condensing on larger particles,
  even if balanced by evaporation of smaller particles.
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Several particles and two components}

\begin{equation}\label{eq:equi1}
c_1^g = \eta(d_p^k) c_1^* x_1^k~,~~c_2^g = \eta(d_p^k) c_2^* x_2^k
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Two particles and two components}



\begin{equation}\label{eq:cieq}
(c_i^{eq})^k = c_i^o\gamma_i^k \frac{{\displaystyle \frac{Q_i^k}{M_i}}}{{\displaystyle \frac{Q_1^k}{M_1} + \dots + \frac{Q_c^k}{M_c}}}
\end{equation}

We assume $\gamma_i^k = 1$, then :

\begin{equation}\label{eq:equi2}
  \biggl(K_i - \sum_{j=1}^s Q_i^j\biggr)\biggl(\sum_{j=1}^c \frac{Q_j^k}{M_j}\biggr) = \eta^k c_i^o \frac{Q_i^k}{M_i}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

