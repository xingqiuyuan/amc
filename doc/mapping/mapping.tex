%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\usepackage{xfrac}
\usepackage[lined, boxed, ruled, commentsnumbered]{algorithm2e}

\title{Mapping CHIMERE particle size and species concentrations to AMC.}

%\date{}
\author{Edouard Debry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

\section{The problem}

In the CHIMERE model, several data, among which initial and boundary concentrations, are precomputed with one
particle size discretization and one speciation which may not correspond to the AMC ones specified in its
configuration file.

Facing this, the user would have to recompute the data if he wants to keep with its AMC settings or modify
them in order to be compatible with the aforemensioned data, which is not always possible, at least concerning
speciation.

Then, we propose in the sequel one interface to perform at run time the necessary conversions from CHIMERE to
AMC particle settings, that is to say to map particle concentrations from CHIMERE data to the AMC size
discretization and speciation.

This mapping operation does not need to be reversible, but requires to be fast and conservative.

\section{Size mapping}


Let $(d^j)_{j=1}^{n_s+ 1}$ and $(d_p^j)_{j=1}^{n_s}$ be respectively the bound and (fixed) mean diameters of
the $n_s$ AMC size sections. The size mapping problem consists in splitting the number ($N$) and mass ($Q$)
concentrations of one given CHIMERE section, delimited by diameters $d_{\min}$ and $d_{\max}$, between the AMC
size sections which cross the CHIMERE one, as plotted on figure \ref{fig:mapsection} :

\FloatBarrier
\begin{figure}[htb]
  \begin{center}
    \input{map_section.pdf_t}
  \end{center}
  \caption{Mapping one CHIMERE size section over AMC ones.}\label{fig:mapsection}
\end{figure}
\FloatBarrier

The mapping has to conserve the CHIMERE section number and mass concentrations :
\begin{equation}\label{eq:map}
\left\{
\begin{split}
Q &= Q_1 + Q_2 + Q_3\\
N &= N_1 + N_2 + N_3
\end{split}\right.
\end{equation}
Those of AMC size sections have to satisfy $Q_i = m_i N_i$ where $m_i$ is the single of mass of particles
within $i^{th}$ size section. Thus previous equation can be rewritten as follows :
\begin{equation}\label{eq:map2}
\left\{
\begin{split}
u_1 f_1 + u_2 f_2 + u_3 f_3 &= 1\\
f_1 + f_2 + f_3 &= 1
\end{split}\right.
\end{equation}
where $f_i$ and $u_i$ are respectively the ratio between AMC and CHIMERE number concentrations and single
masses for each AMC size sections :
\begin{equation}\label{eq:ratio}
f_i \triangleq \frac{N_i}{N}~,~~u_i \triangleq \frac{m_i}{m}
\end{equation}

Eqs. \ref{eq:map2} form an underdetermined system with $(f_1, f_2, f_3)$ as unknowns. In order to overcome
this undetermination, we gather the two lowest AMC sections, these whose single mass are inferior to the
CHIMERE section single mass, into one section, labeled $12$, whose single mass is defined as the geometric
mean of both AMC sections :
\begin{equation}\label{eq:geosinglemass}
m_{12} = \sqrt{m_1 m_2}
\end{equation}

Thus, the CHIMERE section is first mapped on AMC sections $12$ and $3$, which comes to the system :
\begin{equation}\label{eq:map12}
\left\{
\begin{split}
u_{12} f_{12} + u_3 f_3 &= 1\\
f_{12} + f_3 &= 1
\end{split}\right.
\end{equation}
where $f_{12}$ and $u_{12}$ are defined similarly to Eqs. \ref{eq:ratio} :
\begin{equation}\label{eq:ratio12}
f_{12} \triangleq \frac{N_{12}}{N}~,~~u_{12} \triangleq \frac{m_{12}}{m} = \sqrt{u_1 u_2}
\end{equation}
This latest system accepts one unique solution :
\begin{equation}\label{eq:map12sol}
f_{12} = \frac{u_3 - 1}{u_3 - u_{12}}~,~~f_3 = \frac{1 - u_{12}}{u_3 - u_{12}}
\end{equation}
Nevertheless, the number and mass concentrations of size section $12$ still have to be mapped on AMC size
sections $1$ and $2$, which is done by simply repeating the previous mapping :
\begin{equation}\label{eq:map12p}
\left\{
\begin{split}
u'_1 f'_1 + u'_2 f'_2 &= 1\\
f'_1 + f'_2 &= 1
\end{split}\right.
\end{equation}
with the difference that fraction $f'_i$ and single mass $u'_i$ now refer to the combined section $12$ :
\begin{equation}\label{eq:ratio12p}
i=1,2~,~~f'_i \triangleq \frac{N_i}{N_{12}}~,~~u'_1 \triangleq \frac{m_1}{m_{12}} = \frac{u_1}{u_{12}}
~,~u'_2 \triangleq \frac{m_2}{m_{12}} = \frac{u_2}{u_{12}}
\end{equation}
Again, this system admits one unique solution :
\begin{equation}\label{eq:map12psol}
f'_1 = \frac{u'_2 - 1}{u'_2 - u'_1}~,~~f'_2 = \frac{1 - u'_1}{u'_2 - u'_1}
\end{equation}
from which we derive fractions $f_1$ et $f_2$ with respect to the CHIMERE section :
\begin{equation}\label{eq:map12sol2}
\begin{split}
f_1 &= f_{12} f'_1 = \biggl(\frac{u_3 - 1}{u_3 - u_{12}}\biggr)\biggl(\frac{u_2 - u_{12}}{u_2 - u_1}\biggr)\\
f_2 &= f_{12} f'_2 = \biggl(\frac{u_3 - 1}{u_3 - u_{12}}\biggr)\biggl(\frac{u_{12} - u_1}{u_2 - u_1}\biggr)
\end{split}
\end{equation}

\paragraph*{Some remarks}
\begin{itemize}
\item[$\blacksquare$] Let us point out that, although previous systems have one unique solution, the mapping
in this case is not unique because it depends on which AMC sections are primarily combined and on the combined
section single mass definition. Indeed, associating sections $2$ and $3$ would also lead to one unique solution,
but not equal to the previous one (Eqs. \ref{eq:map12sol2} and \ref{eq:map12sol}).

      Nevertheless, as both single masses of AMC sections $1$ and $2$ are on the same side of the CHIMERE
      single mass, it appears more logical to associate sections $1$ and $2$ rather than $2$ and $3$.
\item[$\blacksquare$] This algorithm can be generalized to an arbitrary number of AMC size sections, depending
on how many of them the CHIMERE section spans. In the sequel, we develop such an algorithm. It is intended to
be used only with a few number of AMC size sections, five/six at most, and a limited CHIMERE section size
range.

      Dealing with a greater number of sections or with a wider size range should be performed with an
      appropriate distribution taken from a multimodal representation. As an example, this method is not
      fitted to distribute \ce{PM_{2.5}} or \ce{PM10} over the AMC size discretization.
\end{itemize}

 \FloatBarrier
\begin{figure}[htb]
  \begin{center}
    \input{map_algo.pdf_t}
  \end{center}
  \caption{Mapping algorithm of one CHIMERE section over AMC ones.}\label{fig:mapalgo}
\end{figure}
\FloatBarrier


Figure \ref{fig:mapalgo} depicts the subsequent steps in order to map one CHIMERE section over several size
sections of the AMC particle size discretization.

The prerequisite is to find the AMC size sections over which the CHIMERE section lies : we decide that one AMC
size section is included in the mapping if and only if its mean single size ($m_i$) is within the CHIMERE
section boundaries ($m_{lo}$ and $m_{hi}$). This is more restrictive than comparing boundaries between
themselves but makes more sense as the algorithm is based only on the particle single masses. Furthermore,
this prevents from redistributing on AMC size sections which hardly overlap the CHIMERE one.

Then, the algorithm consists in gathering AMC sections into two groups depending on which side their single
mass is from the CHIMERE single mass ($m$), then redistributing the CHIMERE section number and mass
concentration between these two groups. This process is repeated within the two groups until concentrations
are redistributed to all AMC true size sections.

\begin{algorithm}[H]
  \SetAlgoLined
  \DontPrintSemicolon
  \KwData{CHIMERE section boundaries $\mathbf{m_{lo}}$, $\mathbf{m_{hi}}$, and
    AMC size sections $\mathbf{m_i}~,~~i=1,\dots, n_s$}
  \KwResult{Number $\mathbf{N}$ and list $\mathbf{\mathcal{S}}$ of AMC sections included in mapping.}
  \Begin{
      $\mathbf{N}\longleftarrow 0$\;
      $\mathbf{\mathcal{S}}\longleftarrow \emptyset$\;
      \For{ $i\leftarrow 0$ \KwTo $n_s$}{
        \If{$\mathbf{m_{lo}}\le \mathbf{m_i} < \mathbf{m_{hi}}$}{
          $\mathbf{\mathcal{S}}\longleftarrow i$\;
          $\mathbf{N}\longleftarrow \mathbf{N} + 1$\;
        }
      }
    }
\caption{Mapping algorithm : find AMC sections.}\label{alg:mapalgo1}
\end{algorithm}


\begin{algorithm}[H]
  \SetAlgoLined \DontPrintSemicolon \KwData{Number $\mathbf{N}$ and list $\mathbf{\mathcal{S}}$ of AMC
  sections included in mapping, CHIMERE section single mass $\mathbf{m}$.}
  \KwResult{Fractions $\mathbf{f_i}~,i=1,\dots,
  \mathbf{N}$ for number concentrations redistributed.}
  \Begin{
      $\mathbf{f_i}\longleftarrow 1$\;
      $\mathbf{\mathcal{M}}\longleftarrow \mathbf{m}$\;
      $n\longleftarrow 0$\;
      $n\longleftarrow 1$\;
      \While{$n > 0$}
            {
            }
    }
\caption{Mapping algorithm : redistribute over AMC sections.}\label{alg:mapalgo2}
\end{algorithm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

