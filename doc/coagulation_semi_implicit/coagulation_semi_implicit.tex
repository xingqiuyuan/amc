%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\restylefloat{table}
\usepackage[lined, boxed, commentsnumbered]{algorithm2e}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}

\title{Semi implicit algorithm for coagulation}

%\date{}
\author{Edouard Debry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle

The aerosol population is characterized by several concentrations :
\begin{itemize}
\item[$\square$] number concentration in particle size $k=1,\cdots, s$ :
  \begin{equation}\label{eq:n}
    N^k~~[\#.m^{-3}]
  \end{equation}
  The set of number concentrations $(N^k)$ constitutes the particle size distribution.
\item[$\square$] mass concentration in particle size $k$ and species $X_i$ :
  \begin{equation}\label{eq:qi}
    Q_i^k~~[\mu g.m^{-3}]
  \end{equation}
\end{itemize}

These concentrations undergo the coagulation process :
\begin{itemize}
\item[$\circ$] number equation :
\begin{equation}\label{eq:coagnum}
  \frac{\,d N^k}{\,dt} = \sum_{(j\ge l) \in S^k} b_{jl}f_{jl}^k K_{jl} N^j N^l - N^k \sum_{j=1}^s K_{jk} N^j
\end{equation}
where $f_{jl}^k$ is the number repartition coefficient for coagulation between sections $j$ and $l$ into section $k$.
\item[$\circ$] mass equation for species $X_i$ :
\begin{equation}\label{eq:coagmass}
  \frac{\,d Q_i^k}{\,dt} = \sum_{(j\ge l) \in S^k} b_{jl} (g_{jl}^k)_i K_{jl} [Q_i^j N^l + N^j Q_i^l ] - Q_i^k \sum_{j=1}^s K_{jk} N^j
\end{equation}
where $(g_{jl}^{k})_i$ is the mass repartition coefficient for species $X_i$ and coagulation between sections $j$ and $l$ into
section $k$. The set $S^k$ gather all couples of sections $(j\ge l)$ whose coagulation may fall (partly) in section $k$.
\end{itemize}
The coefficient $K_{jl}$ is the coagulation kernel ($[m^3.s^{-1}]$) and $b_{jl} = 1 - \frac{1}{2}\delta_{(j=l)}$ avoids double
counting.


Introducing the semi-implicit method for both equations (\ref{eq:coagnum},\ref{eq:coagmass}) leads to :
\begin{itemize}
\item[$\circ$] number equation :
\begin{equation}\label{eq:coagnum2}
  (N^k)^{t+1} = (N^k)^t + \Delta t \biggl(\sum_{(j\ge l) \in S^k} b_{jl}f_{jl}^k K_{jl} (N^j)^{t+1} (N^l)^t - (N^k)^{t+1} \sum_{j=1}^s K_{jk} (N^j)^t\biggr)
\end{equation}
\item[$\circ$] mass equation for species $X_i$ :
\begin{equation}\label{eq:coagmass2}
\begin{split}
  (Q_i^k)^{t+1} = (Q_i^k)^t + \Delta t \biggl(&\sum_{(j\ge l) \in S^k} b_{jl} (g_{jl}^k)_i K_{jl} [(Q_i^j)^{t+1} (N^l)^t + (N^j)^t (Q_i^l)^{t+1} ]\\ &- (Q_i^k)^{t+1} \sum_{j=1}^s K_{jk} (N^j)^t\biggr)
\end{split}
\end{equation}
\end{itemize}
with $\Delta t$ the time step in seconds.

Eqs. (\ref{eq:coagnum2},\ref{eq:coagmass2}) may be rewritten as follows :
\begin{itemize}
\item[$\circ$] number equation :
\begin{equation}\label{eq:coagnum3}
  (N^k)^{t+1}\biggl[1 +\Delta t \sum_{j=1}^s K_{jk} (N^j)^t\biggr] - \Delta t \sum_{(j\ge l) \in S^k} b_{jl}f_{jl}^k K_{jl} (N^j)^{t+1} (N^l)^t = (N^k)^t 
\end{equation}
\item[$\circ$] mass equation for species $X_i$ :
\begin{equation}\label{eq:coagmass3}
\begin{split}
  (Q_i^k)^{t+1}\biggl[1 +\Delta t \sum_{j=1}^s K_{jk} (N^j)^t\biggr] - \Delta t \sum_{(j\ge l) \in S^k} b_{jl} (g_{jl}^k)_i K_{jl} [(Q_i^j)^{t+1} (N^l)^t + (N^j)^t (Q_i^l)^{t+1} ] = (Q_i^k)^t 
\end{split}
\end{equation}
\end{itemize}

and finally

\begin{itemize}
\item[$\circ$] number equation :
\begin{equation}\label{eq:coagnum3}
  (N^k)^{t+1}\biggl[1 +\Delta t \sum_{j=1}^s K_{jk} (N^j)^t\biggr] - \Delta t \sum_{j=1}^k \delta_{(j \in
    S^k)}(N^j)^{t+1}\biggl(\sum_{l / (j\ge l) \in S^k} b_{jl}f_{jl}^k K_{jl} (N^l)^t\biggr) = (N^k)^t 
\end{equation}
\item[$\circ$] mass equation for species $X_i$ :
\begin{equation}\label{eq:coagmass3}
\begin{split}
&  (Q_i^k)^{t+1}\biggl[1 +\Delta t \sum_{j=1}^s K_{jk} (N^j)^t\biggr]\\
&- \Delta t \sum_{j=1}^k \delta_{(j \in S^k)}(Q_i^j)^{t+1}\biggl(
\sum_{l / (j\ge l) \in S^k} b_{jl}(g_{jl}^k)_i K_{jl} (N^l)^t + \sum_{l / (l\ge j) \in S^k} b_{lj}(g_{lj}^k)_i K_{lj} (N^l)^t\biggr) = (Q_i^k)^t 
\end{split}
\end{equation}
\end{itemize}

Eqs. (\ref{eq:coagnum3},\ref{eq:coagmass3}) can be put in matrix form :
\begin{equation}\label{eq:coagmat}
  A{\bf N^{t+1}} = {\bf N^t}~,~~B_i {\bf Q_i^{t+1}} = {\bf Q_i^t}
\end{equation}
where $A$ and $B_i$ are $s\times s$ low triangular matrices by block, hence inversible at low cost.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

