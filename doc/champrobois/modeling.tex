%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\usepackage{xfrac}

\title{Champrobois}

%\date{}
\author{Edouard Debry}

\newcommand{\half}{\sfrac{1}{2}}
\newcommand{\km}{{\scriptscriptstyle k - \sfrac{1}{2}}}
\newcommand{\kp}{{\scriptscriptstyle k + \sfrac{1}{2}}}
\newcommand{\jm}{{\scriptscriptstyle j - \sfrac{1}{2}}}
\newcommand{\jp}{{\scriptscriptstyle j + \sfrac{1}{2}}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical method for advection-diffusion-loss equation}
\label{num}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introduction}
A given pollutant $X$ is emitted at one extremity of a cylinder of length $H$ and radius $R$. It undergoes advection along the
cylinder z-axis, radial diffusion as well as deposition loss on the cylinder's wall. Let be $(r, z)\mapsto c(r, z, t)$ the
pollutant concentration at the cylinder position $(r,z)$, expressed in $\mu g.m^{-3}$, such that $c(r, z, t)\,dr\,dz$ is the
pollutant mass, expressed in $\mu g$, inside the position range $[r, r + \,dr]$ and $[z, z + \,dz]$ and at time
$t$.

The pollutant concentration is ruled by an advection-diffusion equation :
\begin{equation}\label{eq:adl}
\frac{\partial c}{\partial t} = - \frac{\partial (Vc)}{\partial z} +
\frac{\partial}{\partial z}\biggl(D\frac{\partial c}{\partial z} \biggr) +
\frac{\partial}{\partial r}\biggl(rD\frac{\partial (c / r)}{\partial r} \biggr)
\end{equation}
where
\begin{itemize}
\item[$\square$] $V(z, t)~[m.s^{-1}]$ is the air flow speed at position $z$ and time $t$.
\item[$\square$] $D(r, z)~[m^2.s^{-1}]$ is the diffusion coefficient of pollutant $X$ in air at position $(r,z)$.
\end{itemize}
Eq. \ref{eq:adl} can be rearranged by introducing diffusion flux :
\begin{equation}\label{eq:adlf}
\frac{\partial c}{\partial t} = - \frac{\partial F^z}{\partial z} - \frac{\partial (F^r)}{\partial r}~,~~F^z \triangleq Vc -
D\frac{\partial c}{\partial z}~,~~F^r \triangleq -Dr\frac{\partial (c/r)}{\partial r}
\end{equation}
The center of cylinder's left boundary coincides with the axis origin, where pollutant $X$ is emitted at a rate $V_sc_s$, $c_s$
being the instant pollutant concentration at time $t$ coming out from the source with an instant speed $V_s$.  The pollutant $X$
may also deposit on the cylinder's wall, with a rate $L~[s^{-1}]$. Then, the diffusion fluxes at the axis origin and on
cylinder's wall respectively match the emission and deposition rates :
\begin{equation}
F^z(0, 0, t) = V_s(t)c_s(t)~,~~F^r(R, z, t) = (2\pi R)L c
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Space and time discretization}

The cylinder is discretized along its axis into $n_z$ regular sections of width $\Delta z$ from $0$ to $n_z - 1$. The center of
$k^{th}$ section is denoted $z_k$, its left and right borders are respectively labeled $z_{\km}$ and $z_{\kp}$, related to
previous centers by :
\begin{equation}
k\ge 1~,~~z_{\kp}\triangleq \frac{z_{k} + z_{k+1}}{2}
\end{equation}
Thus, $z_{-\half}~{\scriptstyle (=0)}$ and $z_{n_z-\half} {\scriptstyle (=H)}$ coincides with the axis origin and the cylinder length.

In the same way, the cylinder is discretized into $n_r$ regular sections from $0$ to $n_r - 1$ along its radius, such that
$r_{-\half}~{\scriptstyle (=0)}$ and $r_{n_r-\half} {\scriptstyle (=R)}$ coincides with the cylinder's axis origin and the
cylinder's wall.
 
On each section, we define the total pollutant mass and its average concentration :
\begin{equation}
C_{jk} \triangleq \int\limits_{r_{\jm}}^{r_{\jp}}\int\limits_{z_{\km}}^{z_{\kp}}c(r,z,t)\,dr\,dz~,~~c_{jk} \triangleq \frac{C_{jk}}{\Delta r\Delta z}
\end{equation}
Integrating Eq. \ref{eq:adlf} on $(j,k)^{th}$ section yields to :
\begin{equation}\label{eq:adlfs}
 \frac{\,dc_{jk}}{\,dt} =\frac{1}{\Delta z} \biggl[F_{j,\km}^z(t) - F_{j,\kp}^z(t)\biggr]
+ \frac{1}{\Delta r}\biggl[F_{\jm,k}^r(t) - F_{\jp,k}^r(t)\biggr]
\end{equation}
The total concentration balance in $(j,k)^{th}$ section results from the variation between entering flux and outgoing
fluxes. These ones are defined by :
\begin{align}\label{eq:flux}
k\ge 1~,~~&F_{j,\kp}^z(t) = Vc_{j,\kp}- D_{(j,\kp)}\frac{\partial c}{\partial z}\biggr|_{j,z_\kp}\\
&F_{0,-\half}^z(t) = V_sc_s(t)~,~~j > 0~,~F_{j, -\half}^z = 0\\
j \ge 1~,~~&F_{\jp,k}^r(t) = -r_\jp D_{(\jp,k)} \frac{\partial (c/ r)}{\partial r}\biggr|_{\jp,k}\\
           &F_{0, k}^r(t) = 0~,~~F_{n_r - \half,k}^r(t) = (2\pi R) L c_{n_r - \half,k}
\end{align}
note that the fluxes are indeed positive if their direction coincides
with that of the axis {\scriptsize ($r$ or $z$ increasing)} and negative otherwise.

Eq. \ref{eq:adlfs} is then integrated in time over one time step $\Delta t~ {\scriptstyle (\triangleq t_{h+1} - t_h)}$ :
\begin{equation}
c_{jk}^{h+1} - c_{jk}^h = \frac{1}{\Delta z}\biggl[\int\limits_{t_h}^{t_{h+1}} F_{j,\km}^z\,dt -
\int\limits_{t_h}^{t_{h+1}}F_{j,\kp}^z\,dt\biggr] +
\frac{1}{\Delta r}\biggl[\int\limits_{t_h}^{t_{h+1}} F_{\jm,k}^r\,dt -
\int\limits_{t_h}^{t_{h+1}}F_{\jp,k}^r\,dt\biggr]
\end{equation}
The time integrals are approximated by using the flux and concentration value at time $t_h$, thus introducing an explicit
scheme :
\begin{equation}\label{eq:adflsq}
c_{jk}^{h+1} - c_{jk}^h = s_z\biggl[(F_{j,\km}^z)^h - (F_{j,\kp}^z)^h\biggr] + s_r\biggl[(F_{\jm,k}^r)^h - (F_{\jp,k}^r)^h \biggr]~,~~
s_z = \frac{\Delta t}{\Delta z}~,~s_r = \frac{\Delta t}{\Delta r}
\end{equation}
For the section ${\scriptstyle (0,0)}$, the left hand side flux is forced by the source term : 
\begin{equation}\label{eq:adflsq0}
c_{00}^{h+1} - c_{00}^h = s_z\biggl[\underbrace{\frac{V_s}{\Delta t}\int\limits_{t_h}^{t_{h+1}}c_s(t)\,dt}_{F_s^h} -
F_{0,\half}^h\biggr] + s_r \biggl[\dots\biggr]
\end{equation}
We still need to link the flux expression \ref{eq:flux} to known concentrations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Advection flux}

The advection part of the flux $F^z$ is approximated according to the Lax-Wendroff scheme with Roe's Superbee
flux limiter :
\begin{equation}\label{eq:fluxa}
F^a(z_{\kp}) = \underbrace{F_1^a(z_{\kp})}_{\text{$1^{st}$ order term}} +
\underbrace{F_2^a(z_{\kp})\phi_{\kp}}_{\text{$2^{nd}$ order term}}
\end{equation}
where $\phi_{\kp}$ represents the flux limiter. In the sequel, we note $f=Vc$.
First and second order terms are respectively defined by
\begin{equation}\label{eq:fluxal}
F_1^a(z_{\kp}) =
\begin{cases}
f_k~&,~~\nu_{\kp} \ge 0\\
f_{k+1}~&,~~\nu_{\kp} < 0
\end{cases}
\end{equation}
and
\begin{equation}\label{eq:fluxah}
F_2^a(z_{\kp})\phi_{\kp} = \frac{1}{2}[f_{k+1} - f_k]
\begin{cases}
(1 - \nu_{\kp})~&,~~\nu_{\kp} \ge 0\\
-(1 + \nu_{\kp})~&,~~\nu_{\kp} < 0
\end{cases}
\end{equation}
where
\begin{equation}\label{eq:nu}
\nu_{\kp} = s_z
\begin{cases}
\frac{f_{k+1} - f_k}{c_{k+1} - c_k}~&,~~c_{k+1} \ne c_k\\
V_k~&,~~\text{otherwise}
\end{cases}
\end{equation}
The flux limiter is a defined by :
\begin{equation}\label{eq:phi}
\begin{split}
&\phi_{\kp} = \max(0, \min(2\theta_{\kp}, 1), \min(\theta_{\kp}, 2))\\
&\theta_{\kp} = \frac{c_{j+1} - c_{j}}{c_{k+1} - c_{k}}~,~~j = k - \frac{\nu_{\kp}}{|\nu_{\kp}|}
\end{split}
\end{equation}
Eqs. \ref{eq:fluxa}, \ref{eq:fluxal}, \ref{eq:fluxah}, \ref{eq:nu} and \ref{eq:phi} constitutes the Lax-Wendroff scheme with Roe's
Superbee flux limiter, which is of second order in time and space, ensures mass conservation and statisfies to the
TVD\footnote{Total Variation Diminuishing}.

The flux variation becomes :
\begin{equation}\label{eq:fluxdiff}
\begin{split}
&(F^a)(z_{\km}) - (F^a)(z_{\kp}) = F_1^a(z_{\km}) - F_1^a(z_{\kp})
+ F_2^a(z_{\km})\phi_{\km} - F_2^a(z_{\kp})\phi_{\kp}\\
&=
\begin{cases}
(f_{k-1} - f_k) - \frac{1 -\nu_{\kp}}{2}(f_{k+1} - f_k)\phi_{\kp} +\frac{1 -\nu_{\km}}{2}(f_k - f_{k-1})\phi_{\km}~,&~~\nu_{\kp}
\ge 0\\
(f_k - f_{k+1}) + \frac{1 +\nu_{\kp}}{2}(f_{k+1} - f_k)\phi_{\kp} -\frac{1 +\nu_{\km}}{2}(f_k - f_{k-1})\phi_{\km}~,&~~\nu_{\kp}
<0
\end{cases}
\end{split}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Diffusion flux}

The diffusion part of the fluxes $F^z$ and $F^r$ (Eqs. \ref{eq:flux}) are approximated with a second order method.

%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Z axis}

\begin{equation}\label{eq:fluxdz}
F_{j,\kp}^z(t) = - D_{(j,\kp)}\frac{\partial c}{\partial z}\biggr|_{j,z_\kp} 
= D_{(j,\kp)}\frac{c_{jk} - c_{j,k+1}}{\Delta z} + O({\scriptstyle \Delta z^2})
\end{equation}
so that the flux variation becomes :
\begin{equation}\label{eq1:fluxdz}
F_{j,\km}^z - F_{j,\kp}^z = \frac{1}{\Delta z}\biggl(D_{j,\km}c_{j,k-1} - (D_{j,\km} + D_{j,\kp}) c_{jk}+ D_{j,\kp}c_{j,k+1}
\biggr)
\end{equation}
The diffusion coefficient at cross section is approximated by the average between both adjacent sections :
\begin{equation}\label{eq:diffz}
D_{j,\kp} \triangleq \frac{D_{j,k} + D_{j,k+1}}{2}
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Radial}

\begin{equation}\label{eq:fluxdr}
F_{\jp,k}^r(t) = -r_\jp D_{(\jp,k)} \frac{\partial (c/ r)}{\partial r}\biggr|_{\jp,k}
= r_\jp D_{(\jp,k)} \frac{c_{jk} / r_j - c_{j + 1,k} / r_{j + 1}}{\Delta r} + O({\scriptstyle \Delta r^2})
\end{equation}
so that the flux variation becomes
\begin{equation}\label{eq1:fluxdr}
  F_{\jm,k}^r - F_{\jp,k}^r =\frac{1}{\Delta r}\biggl[ \frac{r_{\jm}}{r_{j-1}}D_{(\jm,k)}c_{j-1,k} 
- \biggl(\frac{r_{\jm}}{r_j}D_{(\jm,k)} + \frac{r_{\jp}}{r_j}D_{(\jp,k)} \biggr) c_{jk} +\frac{r_{\jp}}{r_{j+1}}D_{(\jp,k)}c_{j+1,k}\biggr]
\end{equation}
The diffusion coefficient at cross section is approximated by the average between both adjacent sections :
\begin{equation}\label{eq:diffr}
D_{\jp,k} \triangleq \frac{D_{j,k} + D_{j + 1,k}}{2}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Enthalpy}

\begin{equation}\label{eq0:hi}
h_i = c_p^i T_i
\end{equation}

\begin{equation}\label{eq:hi}
\frac{\partial (\rho_i h_i)}{\partial t} + \frac{\partial (V\rho_i h_i)}{\partial z} =
\frac{\partial }{\partial z}\biggl(h_i D_i \frac{\partial \rho_i}{\partial z}\biggr)
+ \frac{\partial }{\partial r}\biggl(h_i D_i r\frac{\partial (\rho_i / r)}{\partial r}\biggr) + k_i (T_\infty - T_i)
\end{equation}

\begin{equation}\label{Ti}
\forall i~,~~T_i \simeq T
\end{equation}

\begin{equation}\label{Ti}
\rho h = \sum_i \rho_ih_i = T \sum_i \rho_i c_p^i = \rho c_p T~,~~ k = \sum_i k_i
\end{equation}

\begin{equation}\label{eq:ei}
\frac{\partial (\rho c_p T)}{\partial t} + \frac{\partial (V\rho c_p T)}{\partial z} =
\frac{\partial }{\partial z}\biggl(T \sum_i c_p^i D_i \frac{\partial \rho_i}{\partial z}\biggr)
+ \frac{\partial }{\partial r}\biggl(T\sum_i c_p^i D_i r\frac{\partial (\rho_i / r)}{\partial r}\biggr)
+ k (T_\infty - T)
\end{equation}

\begin{equation}\label{cpi}
\forall i~,~~c_p^i \simeq c_p
\end{equation}

\begin{equation}\label{eq:ei2}
\frac{\partial (\rho T)}{\partial t} + \frac{\partial (V\rho T)}{\partial z} =
\frac{\partial }{\partial z}\biggl(T\sum_i D_i \frac{\partial \rho_i}{\partial z}\biggr)
+ \frac{\partial }{\partial r}\biggl(T\sum_i D_i r\frac{\partial (\rho_i / r)}{\partial r}\biggr)
+ \frac{k}{c_p} (T_\infty - T)
\end{equation}

\begin{equation}\label{eq0:hi}
\begin{split}
\rho\biggl[\frac{\partial T}{\partial t} + \frac{\partial (V T)}{\partial z}\biggr]
+ T \biggl[\frac{\partial \rho}{\partial t} + \frac{\partial (V\rho)}{\partial z}\biggr] =&
\frac{\partial }{\partial z}\biggl(T\sum_i D_i \frac{\partial \rho_i}{\partial z}\biggr)
+ \frac{\partial }{\partial r}\biggl(T\sum_i D_i r\frac{\partial (\rho_i / r)}{\partial r}\biggr)\\
&+ \frac{k}{c_p} (T_\infty - T)
\end{split}
\end{equation}

\begin{equation}\label{eq0:hi3}
\frac{\partial T}{\partial t} + \frac{\partial (V T)}{\partial z}
= \frac{1}{\rho}\biggl(\sum_i D_i \frac{\partial \rho_i}{\partial z}\biggr)\frac{\partial T}{\partial z}
+ \frac{1}{\rho}\biggl(\sum_i D_i r\frac{\partial (\rho_i / r)}{\partial r}\biggr)\frac{\partial T}{\partial r}+ \frac{k}{\rho c_p} (T_\infty - T)
\end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

