%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,french,twoside,12pt,titlepage]{article}

\usepackage{epsfig}
\usepackage{placeins}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{lscape}
\usepackage[usenames, dvipsnames]{color}
\usepackage{float}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[version=3]{mhchem}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{rotating}
\usepackage{longtable}
\usepackage{url}
\usepackage{fancyhdr}
\usepackage{pgfplots}
\usepackage{fancybox}
\usepackage{chngpage}
\usepackage{anyfontsize}
\usepackage{capt-of}
\restylefloat{table}
\usepackage[lined, boxed, commentsnumbered]{algorithm2e}

\title{Multiple layers}

%\date{}
\author{Edouard Debry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Layers definition}
\label{layers}
Each general section $A$ may have several layers, let be $L_a$ the number of them.

The particle number concentration in this general section is $N_a$ and the particle mass concentration of each species $X_i$ is
$Q_a^i$. These mass concentrations stand for all layers, their repartition among layers is characterized by the set of fractions
$(f_{a,1}^i, \cdots, f_{a, L_a}^i)$ which obviously verify :
\begin{equation}\label{rel:f}
\forall X_i~,~~\sum_{j=1}^{L_a} f_{a,j}^i = 1
\end{equation}
so that the mass concentration of species $X_i$ in the $j^{th}$ layer of particle $A$ is defined by :
\begin{equation}
Q_{a,j}^i \triangleq f_{a,j}^i Q_a^i
\end{equation}
The total mass concentration $Q_a$ of particle $A$ is the sum of previous species mass concentrations :
\begin{equation}
Q_a = \sum_i Q_a^i
\end{equation}
whose repartition between layers is defined by a set of fractions $(f_{a,1}, \cdots, f_{a, L_a})$ also checking Eq.
\ref{rel:f}. One may show that the total mass fractions are linked to the species ones by writing in two different ways the total
mass concentration for $j^{th}$ layer :
\begin{equation}
Q_{a,j} = \sum_i Q_{a,j}^i = \sum_i f_{a,j}^i Q_a^i~,~~ Q_{a,j} = f_{a,j}Q_a
\end{equation}
from which we obtain
\begin{equation}
f_{a,j} = \sum_i f_{a,j}^i f_a^i~,~~f_a^i = \frac{Q_a^i}{Q_a}
\end{equation}
where $f_a^i$ is the mass fraction of species $X_i$ in particle A all layers included.

Layers are believed to have concentric shapes (cf. figure \ref{fig:layer}), together with the assumed particle spherical
shape. The first layer is the inner most one, also known as the particle core, the last layer is the outer most one, that is to
say this is the sole layer interacting with the gas phase.

\FloatBarrier
\begin{figure}[htb]
  \begin{center}
    \input{particle_layer.pdf_t}
  \end{center}
  \caption{Particle A with four layers ($L_a = 4$).}\label{fig:layer}
\end{figure}
\FloatBarrier

More precisely, the layer discretization is defined by a set of fixed radius $(r_a^0, r_a^1, \cdots, r_a^{L_a})$ so that the layer
labeled by $j$ is delimited by $[r_a^j, r_a^{j+1}[$. By convention, $r_a^0 = 0$. The highest radius $r_a^{L_a}$ should always
match the current particle radius $\frac{d_p^a}{2}$. Unfortunately, this latter is bound to increase or decrease, within the size
section limits, according to particle growth or shrink, whereas the former remains fixed.

Consequently, the width of the outest layer is chosen so it always include the particle diameter, that is to say :
\begin{equation}\label{eq:lim}
r_a^{L_a - 1} \le \frac{d_p^a}{2} < r_a^{L_a}
\end{equation}
As the particle diameter lies between the section bounds, relationship \ref{eq:lim} is ensured by chosing $r_a^{L_a - 1} =
\frac{d_a^-}{2}$ and $r_a^{L_a} = \frac{d_a^+}{2}$. This also ensures that the outest layer will always exist.

This may introduce some errors at some point, but enables to precompute fraction variables implied by the coagulation and
condensation/evaporation processes. We choose here to favour computational efficiency. This can still be reconsidered if one
cannot afford subsequent errors.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Coagulation}

The number and mass coagulation flux for species $X_i$ between two distinct particles $A$ and $B$ are given by :
\begin{equation}\label{eq:flux}
K_{a,b} N_a N_b~,~~K_{a,b} (Q_a^i N_b + N_a Q_b^i)
\end{equation}
where $K_{a,b}$ is the coagulation kernel between both particles, which will be omited in the sequel. The total mass
flux is given by :
\begin{equation}\label{eq:flux2}
\sum_i K_{a,b}(Q_a^i N_b + N_a Q_b^i) = K_{a,b}(Q_a N_b + N_a Q_b)
\end{equation}
When two particles coagulate, whether their respective layers will merge or not depend on their sizes. As an example, if particle
$A$ is much smaller than $B$ one, all layers of particle $A$ are likely to stand in the outer most layer of particle $B$. On the
contrary, if $A$ and $B$ particles have similar sizes, corresponding layers between $A$ and $B$ may merge themselves, provided $A$
and $B$ have the same layer discretization.

In the sequel, we derive the mass flux for each layer from Eqs. \ref{eq:flux} and \ref{eq:flux2}. We first define the single
masses of particles $A$ and $B$ :
\begin{equation}\label{eq:single}
  m_a \triangleq \frac{Q_a}{N_a}~,~~m_b \triangleq \frac{Q_b}{N_b}
\end{equation}
which are partitioned over all species :
\begin{equation}\label{eq:single2}
  m_a = \sum_i m_a^i~,~m_a^i = \frac{Q_a^i}{N_a}~,~~m_b = \sum_i m_b^i~,~m_b^i = \frac{Q_b^i}{N_b}
\end{equation}
Assuming particle $A$ is smaller than particle $B$, the mass of species $X_i$ in the $l^{th}$ layer of particle $AB$, denoted
$m_{ab,l}^i$, resulting from coagulation between $A$ and $B$, is the sum of the $X_i$ mass already present in particle $B$ in this
layer, $m_{b,l}^i$, plus the sum of $X_i$ mass in particle $A$ over all its layers, each one multiplied by the fraction of layer
$j$ of particle $A$ which merges into layer $l$ of particle $B$, denoted $F_{ab}^{jl}$ :
\begin{equation}\label{eq:flux3}
  m_{ab,l}^i = \sum_{j=1}^{L_a} F_{ab}^{jl}m_{a,j}^i + m_{b,l}^i
\end{equation}
Coefficients $F_{ab}^{jl}$ depends only on the layer discretization of $A$ and $B$, ther mathematical definition and computation
is presented in appendix \ref{a1}.

Thanks to previous definitions, one may develop Eq. \ref{eq:flux3} :
\begin{equation}\label{eq:flux4}
  \begin{split}
  m_{ab,l}^i  &= \sum_j F_{ab}^{jl}f_{a,j}^im_a^i + f_{b,l}^im_b^i\\
&= \sum_j F_{ab}^{jl}f_{a,j}^i\frac{Q_a^i}{N_a} + f_{b,l}^i\frac{Q_b^i}{N_b}\\
&=\sum_j \frac{F_{ab}^{jl}f_{a,j}^iQ_a^i N_b + f_{b,l}^iQ_b^i N_a}{N_aN_b}\\
&=\frac{\sum_j F_{ab}^{jl}f_{a,j}^iQ_a^i N_b + f_{b,l}^iQ_b^i N_a}{N_aN_b}
  \end{split}
\end{equation}
In the last expression, we recognize the flux number at the denominator so that the mass flux of species $X_i$ in $l^{th}$ layer
resulting from coagulation between particle $A$ and $B$ is :
\begin{equation}\label{eq:flux5}
Q_a^iN_b\sum_j F_{ab}^{jl}f_{a,j}^i + f_{b,l}^iQ_b^i N_a
\end{equation}
The corresponding fraction of species $X_i$ in $l^{th}$ layer is defined by :
\begin{equation}\label{eq:frac2}
  f_{ab,l}^i \triangleq \frac{m_{ab,l}^i}{m_{ab}^i}
\end{equation}
which comes to the ratio of mass fluxes :
\begin{equation}\label{eq:frac3}
  \begin{split}
  f_{ab,l}^i &= \frac{Q_a^iN_b\sum_j F_{ab}^{jl}f_{a,j}^i + f_{b,l}^iQ_b^i N_a}{Q_a^iN_b + Q_b^iN_a}\\
  &= \frac{m_a^i\sum_j F_{ab}^{jl}f_{a,j}^i + f_{b,l}^im_b^i}{m_a^i + m_b^i}\\
  &= \frac{f_a^i m_a\sum_j F_{ab}^{jl}f_{a,j}^i + f_{b,l}^i f_b^i m_b}{f_a^i m_a + f_b^i m_b}\\
  &= \frac{f_a^i g\sum_j F_{ab}^{jl}f_{a,j}^i + f_{b,l}^i f_b^i}{f_a^i g + f_b^i}~,~~g \triangleq \frac{m_a}{m_b}\le 1
  \end{split}
\end{equation}
where $g$ is the ratio of single masses.

One may check that the sum of fractions $f_{ab,l}^i$ over all $B$ layers sum to unity :
\begin{equation}\label{eq:frac4}
  \begin{split}
    \sum_{l=1}^{L_b} f_{ab,l}^i &= \frac{f_a^i g\sum_l\sum_j F_{ab}^{jl}f_{a,j}^i + \sum_l f_{b,l}^i f_b^i}{f_a^i g + f_b^i}\\
    &= \frac{f_a^i g\sum_j(\sum_l F_{ab}^{jl})f_{a,j}^i + (\sum_l f_{b,l}^i) f_b^i}{f_a^i g + f_b^i}\\
    &= \frac{f_a^i g\sum_jf_{a,j}^i + f_b^i}{f_a^i g + f_b^i}\\
    &= \frac{f_a^i g + f_b^i}{f_a^i g + f_b^i} = 1
  \end{split}
\end{equation}
The layer fractions from coagulation are thus known prior to numerical integration.


The coagulation numerical integration usually requires coagulation fluxes to be redistributed on existing general sections.

Let be $C$ one of the general sections over which coagulation between $A$ and $B$ is mapped. If $C$ is distinct from $B$, its
layer discretization may be different, making necessary to project $B$ layers on $C$ ones. This step is common to the
condensation/evaporation redistribution process when treated with a lagrangian approach.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Condensation/evaporation}

The condensation/evaporation process is a mass transfer between gas and particle phases. Hence, it only involves the outer most
layer of particle, more precisely the outer most not empty one, which may reduce to the core one. We denote this layer $l_b$
($0\le l_b < L_b$) for particle in general section $B$. Then, only this layer will grow or shrink by condensation/evaporation.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Lagrangian growth}
Following one lagrangian approach, the mass transfer rate for species $X_i$ in particle $B$ is given by :
\begin{equation}\label{eq:dqi}
  \frac{\,d Q_b^i}{\,dt} = N_b \alpha_i(d_p^b) \biggl[c_i^g - \eta(d_p^b) c_i^{eq}{\scriptstyle (RH, T,
    f_{b,l_b}^1 Q_b^1,\dots,f_{b,l_b}^j Q_b^j,\dots)}\biggr]
\end{equation}
At first sight, condensation/evaporation is treated as unaware of multiple layers, except that the equilibrium gas concentration
($c_i^{eq}$), which is usually a complex function of particle composition, is now restricted to the particle composition of the
layer ($l_b$) in touch with the gas phase :
\begin{equation}\label{eq:comp}
  \forall X_i~,~~Q_{b,l_b}^i = f_{b,l_b}^i Q_b^i
\end{equation}

Nevertheless, after numerical integration of Eqs. \ref{eq:dqi}, only the mass species concentration in $l_b$ layer should be
affected by the species mass transfer. Let $Q_b^i$ be the mass concentration of species $X_i$ at time $t$ and let us note
$(Q_b^i)'$ the one at time $t+1$ resulting from numerical integration of \ref{eq:dqi} over one time step $\Delta t$. In the same
manner, we note $f_{b,l}^i$ and $(f_{b,l}^i)'$ the layer fraction of species $X_i$ in particle $B$.

The fact that mass concentrations of layers strictly below $l_b$ should not be affected means :
\begin{equation}\label{eq:comp}
  \forall l < l_b~,~\forall X_i~,~~(Q_{b,l}^i)' = Q_{b,l}^i \Leftrightarrow (f_{b,l}^i)' = f_{b,l}^i \frac{Q_b^i}{(Q_b^i)'}
  = \frac{f_{b,l}^i}{1 + G_b^i}~,~~G_b^i \triangleq \frac{\Delta Q_b^i}{Q_b^i}
\end{equation}
where $G_b^i$ is the growth fraction of species $X_i$ in particle $B$. If this one is positive (condensation), fractions of inner
layers tend to diminuish, if this one is negative (evaporation) they tend to increase, but the numerical integration should always
ensure $G_b^i > -1$.

Fractions in layer $l_b$ are deduced from previous ones :
\begin{equation}\label{eq:com2}
  \begin{split}
  \forall X_i~,~~(f_{b,l_b}^i)' &= 1 - \sum_{l< l_b}(f_{b,l}^i)' = 1 - \frac{1}{1 + G_b^i} \sum_{l< l_b}(f_{b,l}^i)\\
  &=  1 - \frac{1}{1 + G_b^i} \biggl(1 - f_{b,l_b}^i\biggr) = \frac{G_b^i + f_{b,l_b}^i}{1 + G_b^i}
  \end{split}
\end{equation}
The fractions of the outest layer tend to increase with condensation but is bounded by unity, they decrease with evaporation up to
0 if $G_b^i = - f_{b,l_b}^i$ that is to say if all the mass of species $X_i$ in layer $l_b$ evaporates. The numerical integration
should also ensure $G_b^i > - f_{b,l_b}^i$, which guarantee $G_b^i > -1$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Redistribution step}

Particles from general section $B$ which have been let grow or shrink to their exact sizes have then to be redistributed on
existing general sections. Let be $C$ one of the general section, distinct from $B$, over which we have to redistribute previously
grown or shrinked particles of $B$ (as in coagulation).

The redistribution process is first performed on mass concentrations all layers included, as if unaware
of the layer discretization. Nevertheless, both general sections $B$ and $C$ may not share the same layer discretization, making
necessary to project $B$ layers on $C$ ones. This will only affect the fraction layers, the following
algorithm \ref{eq:algoredist} is an example of how to perform this projection.  \FloatBarrier
\begin{algorithm}[H]
  \SetKwInOut{Input}{input}
  \SetKwInOut{Output}{output}

  \Input{Layer fractions ($f_{b,l}^i$) and ($f_{c,l}^i$) of both general sections}
  \Output{Layer fractions ($f_{c,l}^i$) of general sections $C$}
  \BlankLine
  {\bf 1) Project mass concentrations of $B$ layers on $C$ layers.}
  $j = 0~,~l = 0~,~d = 0~,~v_1 = 0~,~v_2 = 0$\;
  $\forall k,i~,~~Q_k^i = f_{c,k}^i Q_c^i$\;
  \While{$j < L_b$}{
    $d = \min(r_b^{j+1}, r_c^{l+1})$\;
    $v_2\leftarrow d^3$\;
    $f\leftarrow (v_2 - v_1) / v_b^j~,~~v_j^b = (r_b^{j+1})^3 - (r_b^j)^3$\; 
    $v_1\leftarrow v_2$\;
    $\forall X_i~,~~Q_l^i\leftarrow Q_l^i + f\times f_{b,j}^i Q_b^i$\;
    \eIf{$r_b^j < r_c^l$}{
      $j\leftarrow j + 1$\;
    }{
      $l\leftarrow l + 1$\;
    }
  }
  \BlankLine
  {\bf 2) Compute $C$ layer fractions.}
  $\forall i~,~~Q^i\leftarrow \sum_k Q_k^i$\;
  \For{$0\le l < L_c$}{
    \For{$i$}{
      $f_{c,l}^i\leftarrow Q_l^i / Q^i$\;      
    }
  }
\caption{Layer redistributions.}\label{eq:algoredist}
\end{algorithm}
This algorithm may be improved by assuming one particular shape for concentrations inside each layer.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\cleardoublepage
\appendix

\section{Layer fractions}
\label{a1}

Figure \ref{fig:cross} shows what could be the physical appearance of coagulation between particles $A$ and $B$. Before diffusing
itself into particle $B$, particle $A$ may exist as a whole so that its inner layers will not necessarily merge with $B$ inner
layers.

\FloatBarrier
\begin{figure}[htb]
  \begin{center}
    \input{layer_fraction.pdf_t}
  \end{center}
  \caption{Layer crossing between particle A and B after coagulation.}\label{fig:cross}
\end{figure}
\FloatBarrier

Indeed, the mid layer of particle A, labeled by $j$ and colored in \textcolor{cyan}{cyan}, may rather merge with one of the outer
$B$ layer, labeled by $l$ and colored in \textcolor{magenta}{magenta}. The volume of layer $j$ which crosses layer $l$, denoted
$V_{ab}^{jl}$ will merge into this latter layer.

Assuming $V_a^j$ to be the total volume of layer $j$, the fraction of layer $j$
of particle $A$ which merges into layer $l$ of particle $B$ is defined by :
\begin{equation}\label{eq:defF}
  F_{ab}^{jl} \triangleq \frac{V_{ab}^{jl}}{V_a^j}
\end{equation}

The purpose of this appendix is to propose one computation of these coefficients, which only depend of layer discretization.
In the sequel, we implicitely assume that the current particle radius match the outest radius layer, there lies the error which
was mentioned in section \ref{layers}.

After coagulation, the radius of particle B becomes $r_{ab} = [(r_a)^3 + (r_b)^3]^{\frac{1}{3}}$. The distance between centers of
both particles ($\mathbf{OO'}$) is equal to $d = r_{ab} - r_a$. Each point {\bf M} in the volume produced by the crossing between
$j$ and $l$ layers may be defined in spherial coordinates either from particle A, $\overrightarrow{OM} = r \vec{u}_r$, or from
particle B, $\overrightarrow{O'M} = r' \vec{u'}_r$. Both coordinate systems are related by :
\begin{equation}
\begin{split}
  \overrightarrow{O'M} &= \overrightarrow{O'O} + \overrightarrow{OM} = [d + r\cos(\theta)]\vec{z} +
  r\sin(\theta)\cos(\varphi)\vec{x} + r\sin(\theta)\sin(\varphi)\vec{y}\\
  \overrightarrow{O'M} &= r'\cos(\theta')\vec{z} + r'\sin(\theta')\cos(\varphi')\vec{x} + r'\sin(\theta')\sin(\varphi')\vec{y}
\end{split}
\end{equation}
so that $(r', \theta', \varphi')$ may be expressed as a function of $(r, \theta, \varphi)$ :
\begin{equation}
\begin{split}
r' &= \sqrt{r^2 + d^2 + 2rd\cos(\theta)}\\
\tan(\theta') &= \frac{r\sin(\theta)}{d + r\cos(\theta)}\\
\varphi' &= \varphi
\end{split}
\end{equation}
The volume delimited by the crossing of A and B layers may then receive its mathematical definition :
\begin{equation}\label{int:0}
  V_{ab}^{jl} = 2\pi\int_{r = r_a^j}^{r_a^{j+1}}\int_{\theta = 0}^{\pi}E(r_b^l \le r'< r_b^{l+1})r^2
  \sin(\theta)\,dr\,d\theta
\end{equation}
where we have already integrated with respect to $\varphi$ and $E$ is the following function :
\begin{equation}
E = 
\begin{cases}
  & 1~,~~\text{if}~~r_b^l \le r'< r_b^{l+1}\\
  & 0~,~~\text{otherwise}
\end{cases}
\end{equation}

It can be seen from figure \ref{fig:cross} that this latest condition implies one minimal and maximal $\theta$ value for one given
radius $r$, when $r'$ meets its bounds :
\begin{equation}\label{eq:f1}
\cos(\theta_{\min}^r) = 
\begin{cases}
  & \frac{(r_b^{l+1})^2 - r^2 - d^2}{2rd}\\
  & 1~~(\theta_{\min}^r = 0)
\end{cases}
~,~~\cos(\theta_{\max}^r) = 
\begin{cases}
&\frac{(r_b^l)^2 - r^2 - d^2}{2rd}\\
&-1~~(\theta_{\max}^r = \pi)
\end{cases}
\end{equation}
Then, for a given radius value $r$, the condition $E$ in integral \ref{int:0} disappears as it is always true within
$[\theta_{\min}^r, \theta_{\max}^r]$ :
\begin{equation}\label{int:00}
  V_{ab}^{jl} =  2\pi\int_{r = r_a^j}^{r_a^{j+1}}r^2\biggl[\int_{\theta = \theta_{\min}^r}^{\theta_{\max}^r}\sin(\theta)\,d\theta\biggr]\,dr
\end{equation}
which allows its analytical computation :
\begin{equation}\label{int:01}
  V_{ab}^{jl} = 2\pi\int_{r = r_a^j}^{r_a^{j+1}}r^2\biggl[-\cos(\theta)\biggr]_{\theta_{\min}^r}^{\theta_{\max}^r}\,dr
  = 2\pi\int_{r = r_a^j}^{r_a^{j+1}}r^2 [\cos(\theta_{\min}^r) - \cos(\theta_{\max}^r)]\,dr
\end{equation}

Then, according to Eqs. \ref{eq:f1} , four different cases arise :
\begin{itemize}
\item[$\square$] $\mathbf{r > d - r_b^l, r_b^{l+1} - d}$
  \begin{equation}\label{intI1}
    \begin{split}
      \mathbf{I_1}(u, v) &= 2\pi\int_{u}^{v}r^2 [\cos(\theta_{\min}^r) - \cos(\theta_{\max}^r)]\,dr\\
      &=2\pi\int_{u}^{v}r^2 \frac{(r_b^{l+1})^2 - (r_b^l)^2}{2rd}\,dr = \pi \frac{[(r_b^{l+1})^2 - (r_b^l)^2](v^2 - u^2)}{2d}
    \end{split}
  \end{equation}
\item[$\square$] $\mathbf{r_b^{l+1} - d < r < d - r_b^l}$
  \begin{equation}\label{intI2}
    \begin{split}
      \mathbf{I_2}(u, v) & = 2\pi\int_{u}^{v}r^2 \biggl[1 + \frac{(r_b^{l+1})^2 - r^2 - d^2}{2rd}\biggr]\,dr\\
      & = \frac{\pi}{d}\int_{u}^{v}(- r^3 + 2dr^2 + [(r_b^{l+1})^2 - d^2]r)\,dr\\
      & = \frac{\pi}{d}\biggl[r^2\biggl(-\frac{1}{4}r^2 + \frac{2}{3}dr + \frac{1}{2}[(r_b^{l+1})^2 - d^2]\biggr)\biggr]_u^v
    \end{split}
  \end{equation}
\item[$\square$] $\mathbf{d - r_b^l < r < r_b^{l+1} - d}$
  \begin{equation}\label{intI3}
    \begin{split}
      \mathbf{I_3}(u, v) &= 2\pi\int_{u}^{v}r^2 \biggl[1 + \frac{r^2 + d^2 - (r_b^l)^2}{2rd}\biggr]\,dr\\
      & = \frac{\pi}{d}\int_{u}^{v}(r^3 + 2dr^2 + [d^2  - (r_b^l)^2]r)\,dr\\
      & = \frac{\pi}{d}\biggl[r^2\biggl(\frac{1}{4}r^2 + \frac{2}{3}dr + \frac{1}{2}[d^2  - (r_b^l)^2]\biggr) \biggr]_u^v
    \end{split}
  \end{equation}
\item[$\square$] $\mathbf{r < d - r_b^l, r_b^{l+1} - d}$
  \begin{equation}\label{intI4}
    \begin{split}
      \mathbf{I_4}(u, v) &= 2\pi\int_{u}^{v}r^2 [\cos(\theta_{\min}^r) - \cos(\theta_{\max}^r)]\,dr
      = 4\pi\int_{u}^{v}r^2\,dr = \frac{4}{3}\pi [v^3 - u^3]
    \end{split}
  \end{equation}
\end{itemize}

The integral \ref{int:01} finally comes to a sum of one or more of integrals \ref{intI1}, \ref{intI2}, \ref{intI3} and \ref{intI4},
depending of the order of $r_a^j$, $r_a^{j+1}$, $d - r_b^l$ and $r_b^{l+1} -d$, ten cases arise :
\begin{enumerate}
\item $\mathbf{r_b^{l+1} -d, d - r_b^l < r_a^j < r_a^{j+1}}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2.5, 1) node {I$_1$};
      \draw (axis cs:0, -1) node {$d - r_b^l$};
      \draw (axis cs:0,  1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c1}
    V_{ab}^{jl} = I_1(r_a^j, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_b^{l+1} -d < r_a^j< d - r_b^l < r_a^{j+1}}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (2, 0)
        (2, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:1.5, 1) node {I$_2$};
      \draw (axis cs:3, 1) node {I$_1$};
      \draw (axis cs:2, -1) node {$d - r_b^l$};
      \draw (axis cs:0, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c2}
    V_{ab}^{jl} = I_2(r_a^j, d - r_b^l) + I_1(d - r_b^l, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_b^{l+1} -d < r_a^j < r_a^{j+1} < d - r_b^l}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2.5, 1) node {I$_2$};
      \draw (axis cs:5, -1) node {$d - r_b^l$};
      \draw (axis cs:0, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c3}
    V_{ab}^{jl} = I_2(r_a^j, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_a^j<  r_b^{l+1} -d < r_a^{j+1} < d - r_b^l}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (3, 0)
        (3, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2, 1) node {I$_4$};
      \draw (axis cs:3.5, 1) node {I$_2$};
      \draw (axis cs:5, -1) node {$d - r_b^l$};
      \draw (axis cs:3, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c4}
    V_{ab}^{jl} = I_4(r_a^j, r_b^{l+1} -d) + I_2(r_b^{l+1} -d, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_a^j < r_a^{j+1} < d - r_b^l, r_b^{l+1} -d}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2.5, 1) node {I$_4$};
      \draw (axis cs:5, -1) node {$d - r_b^l$};
      \draw (axis cs:5,  1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c5}
    V_{ab}^{jl} = I_4(r_a^j, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_a^j< d - r_b^l < r_a^{j+1} < r_b^{l+1} -d}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (3, 0)
        (3, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2, 1) node {I$_4$};
      \draw (axis cs:3.5, 1) node {I$_3$};
      \draw (axis cs:3, -1) node {$d - r_b^l$};
      \draw (axis cs:5, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c6}
    V_{ab}^{jl} = I_4(r_a^j, d - r_b^l) + I_1(d - r_b^l, r_a^{j+1})
  \end{equation}
\item $\mathbf{d - r_b^l < r_a^j < r_a^{j+1} < r_b^{l+1} -d}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:2.5, 1) node {I$_3$};
      \draw (axis cs:0, -1) node {$d - r_b^l$};
      \draw (axis cs:5,  1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c7}
    V_{ab}^{jl} = I_3(r_a^j, r_a^{j+1})
  \end{equation}
\item $\mathbf{d - r_b^l < r_a^j< r_b^{l+1} -d < r_a^{j+1}}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (2, 0)
        (2, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:1.5, 1) node {I$_3$};
      \draw (axis cs:3, 1) node {I$_1$};
      \draw (axis cs:0, -1) node {$d - r_b^l$};
      \draw (axis cs:2, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c8}
    V_{ab}^{jl} = I_3(r_a^j, r_b^{l+1} - d) + I_1(r_b^{l+1} - d, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_a^j< d - r_b^l < r_b^{l+1} -d < r_a^{j+1}}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (2, 0)
        (2, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (3, 0)
        (3, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:1.5, 1) node {I$_4$};
      \draw (axis cs:2.5, 1) node {I$_3$};
      \draw (axis cs:3.5, 1) node {I$_1$};
      \draw (axis cs:2, -1) node {$d - r_b^l$};
      \draw (axis cs:3, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c9}
    V_{ab}^{jl} = I_4(r_a^j, d - r_b^l) + I_3(d - r_b^l, r_b^{l+1} - d) + I_1(r_b^{l+1} - d, r_a^{j+1})
  \end{equation}
\item $\mathbf{r_a^j< r_b^{l+1} -d < d - r_b^l < r_a^{j+1}}$
  \FloatBarrier
  \begin{tikzpicture}
    \begin{axis}[hide axis, height=4cm, width=\textwidth, ymin=-3, ymax=3, xmin=-0.5]
      \addplot coordinates {
        (0, 0)
        (1, 0)
        (2, 0)
        (3, 0)
        (4, 0)
        (5, 0)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (1, 0)
        (1, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (2, 0)
        (2, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (3, 0)
        (3, 1)
      };
      \addplot [color=black, line width=1.5pt] coordinates {
        (4, 0)
        (4, 1)
      };
      \addplot [color=black, only marks, mark=square*] coordinates {
        (1, 0)
        (4, 0)
      };
      \draw (axis cs:1.5, 1) node {I$_4$};
      \draw (axis cs:2.5, 1) node {I$_2$};
      \draw (axis cs:3.5, 1) node {I$_1$};
      \draw (axis cs:3, -1) node {$d - r_b^l$};
      \draw (axis cs:2, -1) node {$r_b^{l+1} - d$};
      \draw (axis cs:1, -1) node {$r_a^j$};
      \draw (axis cs:4, -1) node {$r_a^{j+1}$};
    \end{axis}
  \end{tikzpicture}
  \begin{equation}\label{int:c10}
    V_{ab}^{jl} = I_4(r_a^j, r_b^{l+1} - d) + I_2(r_b^{l+1} - d, d - r_b^l) + I_1(d - r_b^l, r_a^{j+1})
  \end{equation}
\end{enumerate}

The following table summarizes the different cases with the number or integrals, the integral number list and the integral
boundaries, for each of them. Integral boundaries are given as the the radius index list with respect to vector $\{r_a^j,
r_a^{j+1}, d - r_b^l, r_b^{l+1} - d\}$.

\FloatBarrier
\begin{table}[h]
  \begin{center}
    \begin{tabular}{c|c|c|c}
      case & number & integrals & radius index \\
      \hline
      1 & 1 &    $1$       &   $0, 1$       \\
      2 & 2 &    $2, 1$    &   $0, 2, 1$    \\
      3 & 1 &    $2$       &   $0, 1$       \\
      4 & 2 &    $4, 2$    &   $0, 3, 1$    \\
      5 & 1 &    $4$       &   $0, 1$       \\
      6 & 2 &    $4, 3$    &   $0, 2, 1$    \\
      7 & 1 &    $3$       &   $0, 1$       \\
      8 & 2 &    $3, 1$    &   $0, 3, 1$    \\
      9 & 3 &    $4, 3, 1$ &   $0, 2, 3, 1$ \\
      10 & 3 &    $4, 2, 1$ &   $0, 3, 2, 1$ \\
    \end{tabular}
    \caption{Integrals to compute for each case.}\label{tab:1}
  \end{center}
\end{table}
\FloatBarrier


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

