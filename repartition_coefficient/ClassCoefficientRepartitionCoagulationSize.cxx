// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_SIZE_CXX

#include "ClassCoefficientRepartitionCoagulationSize.hxx"

namespace AMC
{
  // Constructors.
  ClassCoefficientRepartitionCoagulationSize::ClassCoefficientRepartitionCoagulationSize()
    : ClassPDF(), is_computed_(false),
      Ncouple_((ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Instantiate size repartition coefficients." << endl;
#endif

    // Size section index of each general section.
    general_section_size_.resize(ClassAerosolData::Ng_);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      general_section_size_[i] = ClassAerosolData::general_section_[i][0];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Info(2) << Reset() << "Number of general section = " << ClassAerosolData::Ng_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(2) << Reset() << "Associated size sections = "
                         << general_section_size_ << endl;
#endif

    // Compute coefficient indexes : this does not change.
    Ncoefficient_.assign(Ncouple_, 0);
    repartition_coefficient_index_.resize(Ncouple_);
    repartition_coefficient_number_.resize(Ncouple_);
    repartition_coefficient_mass_.resize(Ncouple_);

    int h(0);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      for (int j = 0; j <= i; ++j)
        {
          const int ii = general_section_size_[i];
          const int jj = general_section_size_[j];
          const int hh = ii * (ii + 1) / 2 + jj;

          Ncoefficient_[h] = ClassPDF::n_[hh];
          repartition_coefficient_index_[h] = ClassPDF::i_[hh];

          if (Ncoefficient_[h] > 1)
            {
              repartition_coefficient_number_[h].assign(Ncoefficient_[h], real(0));
              repartition_coefficient_mass_[h].assign(Ncoefficient_[h], real(0));
            }
          else
            {
              repartition_coefficient_number_[h].assign(Ncoefficient_[h], real(1));
              repartition_coefficient_mass_[h].assign(Ncoefficient_[h], real(1));
            }

          h++;
        }

    return;
  }


  // Destructor.
  
  ClassCoefficientRepartitionCoagulationSize::~ClassCoefficientRepartitionCoagulationSize()
  {
    return;
  }


  // Clear.
  
  void ClassCoefficientRepartitionCoagulationSize::Clear()
  {
    is_computed_ = false;
    Ncouple_ = 0;
    Ncoefficient_.clear();
    general_section_size_.clear();
    repartition_coefficient_index_.clear();
    repartition_coefficient_number_.clear();
    repartition_coefficient_mass_.clear();
  }


  // Get methods.
  
  int ClassCoefficientRepartitionCoagulationSize::GetNcouple() const
  {
    return Ncouple_;
  }

  
  void ClassCoefficientRepartitionCoagulationSize::GetCoefficientNumber(vector<vector<real> > &coefficient) const
  {
    coefficient = repartition_coefficient_number_;
  }

  
  void ClassCoefficientRepartitionCoagulationSize::GetCoefficientMass(vector<vector<real> > &coefficient) const
  {
    coefficient = repartition_coefficient_mass_;
  }


  // Compute repartition coefficients.
  
  void ClassCoefficientRepartitionCoagulationSize::ComputeCoefficient(const vector<real> &x)
  {
    // Data for PDF.
    vector1r alpha(ClassPDF::Npdf_, real(0)), beta(ClassPDF::Npdf_, real(0)),
      gamma(ClassPDF::Npdf_, real(0)), step(ClassPDF::Nstep_, real(0));

    int h(0);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        const int ii = general_section_size_[i];

        for (int j = 0; j <= i; ++j)
          {
#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
            CBUG << "i = " << i << endl;
            CBUG << "j = " << j << endl;
            CBUG << "h = " << h << endl;
#endif

            const int jj = general_section_size_[j];
            const int hh = ii * (ii + 1) / 2 + jj;

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
            CBUG << "ii = " << ii << endl;
            CBUG << "jj = " << jj << endl;
            CBUG << "hh = " << hh << endl;
#endif

            if (ClassPDF::n_[hh] > 1)
              {
                int &Ncoefficient = Ncoefficient_[h];
                vector1i &repartition_coefficient_index = repartition_coefficient_index_[h];
                vector1r &repartition_coefficient_number = repartition_coefficient_number_[h];
                vector1r &repartition_coefficient_mass = repartition_coefficient_mass_[h];

                const real &xi = x[i];
                const real &xj = x[j];

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
                CBUG << "xi = " << xi << endl;
                CBUG << "xj = " << xj << endl;
#endif
                int icase(-1);
                if (ii != jj || xi != xj)
                  icase = ClassPDF::compute_pdf_(ii, jj, xi, xj, step, alpha, beta, gamma);
                else
                  icase = ClassPDF::compute_pdf_(ii, xi, step, alpha, beta, gamma);

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
                CBUG << "icase = " << icase << endl;
#endif

                if (icase < 0)
                  {
                    // If an error occured in the PDF computation (should never happen),
                    // put everything in the size section where lies the average
                    // coagulation size.

                    // Size section.
                    const real xij = xi + xj;
                    const int k = AMC::search_index(x_, xij);

                    repartition_coefficient_index[0] = k;
                    repartition_coefficient_number[0] = real(1);
                    repartition_coefficient_mass[0] = real(1);
                    Ncoefficient = 1;
                  }
                else
                  {
                    repartition_coefficient_index = ClassPDF::i_[hh];
                    repartition_coefficient_number.assign(ClassPDF::n_[hh], real(0));
                    repartition_coefficient_mass.assign(ClassPDF::n_[hh], real(0));

                    ClassPDF::integrate_pdf_(hh, icase, step, alpha, beta, gamma,
                                             repartition_coefficient_number,
                                             repartition_coefficient_mass);

                    // Scale and normalize mass coefficient.
                    const real xij_inv = real(1) / (xi + xj);

                    for (int k = 0; k < ClassPDF::n_[hh]; ++k)
                      repartition_coefficient_mass[k] = (ClassPDF::y_[hh][0] * repartition_coefficient_number[k]
                                                         + ClassPDF::dy_[hh] * repartition_coefficient_mass[k]) * xij_inv;
                  }

                // Too small coefficients may arise from this algorithm depending on average section size.
                // We prefer to filter them, not because of robustness, but because it would waste
                // CPU time on very small quantities afterwards, which we abominate above all.
                Ncoefficient = 0;
                real repartition_coefficient_number_remain(real(0)), repartition_coefficient_mass_remain(real(0));

                for (int k = 0; k < ClassPDF::n_[hh]; ++k)
                  if (repartition_coefficient_number[k] > AMC_COEFFICIENT_REPARTITION_SIZE_THRESHOLD)
                    {
                      if (k > Ncoefficient)
                        {
                          repartition_coefficient_index[Ncoefficient] = repartition_coefficient_index[k];
                          repartition_coefficient_number[Ncoefficient] = repartition_coefficient_number[k];
                          repartition_coefficient_mass[Ncoefficient] = repartition_coefficient_mass[k];
                        }
                      Ncoefficient++;
                    }
                  else
                    {
                      repartition_coefficient_number_remain += repartition_coefficient_number[k];
                      repartition_coefficient_mass_remain += repartition_coefficient_mass[k];
                    }

                // Put too small coefficients in first one.
                if (Ncoefficient < ClassPDF::n_[hh])
                  {
                    repartition_coefficient_number[0] += repartition_coefficient_number_remain;
                    repartition_coefficient_mass[0] += repartition_coefficient_mass_remain;
                  }

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
                CBUG << "Ncoefficient = " << Ncoefficient << endl;
                CBUG << "repartition_coefficient_index = " << repartition_coefficient_index << endl;
                CBUG << "repartition_coefficient_number = " << repartition_coefficient_number << endl;
                CBUG << "repartition_coefficient_mass = " << repartition_coefficient_mass << endl;

                if (abs(compute_vector_sum(repartition_coefficient_number, Ncoefficient) - real(1)) >
                    AMC_COEFFICIENT_REPARTITION_SIZE_THRESHOLD_SUM)
                  throw AMC::Error("Number coefficient sum is not unity.");

                if (abs(compute_vector_sum(repartition_coefficient_mass, Ncoefficient) - real(1)) >
                    AMC_COEFFICIENT_REPARTITION_SIZE_THRESHOLD_SUM)
                  throw AMC::Error("Mass coefficient sum is not unity.");

                const real xij = xi + xj;

                for (int k = 0; k < Ncoefficient; ++k)
                  {
                    const int l = repartition_coefficient_index_[h][k];
                    const real xm = xij * repartition_coefficient_mass[k] / repartition_coefficient_number[k];

                    if (xm < ClassPDF::x_[l] || xm > ClassPDF::x_[l + 1])
                      throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                       ") (h = " + to_str(h) + "), average equivalent volume (xm) " +
                                       "for coefficient " + to_str(k) + " in section " + to_str(l) +
                                       " is not within this section. xm = " + to_str(xm) + ", x[l] = " +
                                       to_str(ClassPDF::x_[l]) + ", x[l + 1] = " + to_str(ClassPDF::x_[l + 1]) +
                                       ", repartition_coefficient_number = " + to_str(repartition_coefficient_number) +
                                       ", repartition_coefficient_mass = " + to_str(repartition_coefficient_mass));
                  }
#endif
              }

            // Increment couple.
            ++h;
          }
      }

    // Say we have computed.
    this->is_computed_ = true;
  }


  
  void ClassCoefficientRepartitionCoagulationSize::ComputeCoefficientCenter()
  {
    vector1r x(ClassAerosolData::Ng_);

    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      x[i] = ClassPDF::cx_[general_section_size_[i]];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Compute size repartition"
                         << " coefficients with centered equivalent volume." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "x = " << x << endl;
#endif

    ComputeCoefficient(x);
  }

#ifdef AMC_WITH_TEST
  // Test function.*/
  void ClassCoefficientRepartitionCoagulationSize::Test(const int N, const real tol, const real rmin, const real rmax)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test PDF size repartition coefficient with "
                         << Fred().Str() << "N = " << N << Reset().Str() << " and " << Fred().Str()
                         << " tol = " << tol << Reset().Str() << endl;
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Ncouple = " << Ncouple_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "rmin = " << rmin << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(3) << Reset() << "rmax = " << rmax << endl;
#endif

#ifdef AMC_WITH_TIMER
    int time_index = AMCTimer<CPU>::Add("test_repartition_coefficient_size");
#endif

    vector1r x(ClassAerosolData::Ng_);

    // Perform test.
    for (int u = 0; u < N; ++u)
      {
        // Randomly choose the average single mass as repartition coefficients depend on it.
        for (int i = 0; i < ClassAerosolData::Ng_; ++i)
          {
            real r = real(drand48());
            r = r < rmin ? rmin : r;
            r = r > rmax ? rmax : r;
            const int ii = general_section_size_[i];
            x[i] = ClassPDF::x_[ii] + r * ClassPDF::dx_[ii];
          }

#ifdef AMC_WITH_TIMER
        AMCTimer<CPU>::Start();
#endif

        // Compute Coefficients.
        ComputeCoefficient(x);

#ifdef AMC_WITH_TIMER
        AMCTimer<CPU>::Stop(time_index);
#endif

        // Check number and mass coefficients.
        int h(-1);
        for (int i = 0; i < ClassAerosolData::Ng_; ++i)
          for (int j = 0; j <= i; ++j)
            {
              const real xij = x[i] + x[j];
              const int Ncoefficient = this->Ncoefficient_[++h];

              if (Ncoefficient > 1)
                {
                  const vector1i &repartition_coefficient_index = repartition_coefficient_index_[h];
                  const vector1r &repartition_coefficient_number = repartition_coefficient_number_[h];
                  const vector1r &repartition_coefficient_mass = repartition_coefficient_mass_[h];

                  if (abs(compute_vector_sum(repartition_coefficient_number) - real(1)) > tol)
                    throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                     ") (h = " + to_str(h) + "), sum of number repartition coefficients is not unity." +
                                     " repartition_coefficient_number = " + to_str(repartition_coefficient_number));

                  if (abs(compute_vector_sum(repartition_coefficient_mass) - real(1)) > tol)
                    throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                     ") (h = " + to_str(h) + "), sum of mass repartition coefficients is not unity." +
                                     " repartition_coefficient_mass = " + to_str(repartition_coefficient_mass));

                  for (int k = 0; k < Ncoefficient; ++k)
                    {
                      const int l = repartition_coefficient_index[k];
                      const real xm = xij * repartition_coefficient_mass[k] / repartition_coefficient_number[k];

                      if (xm < ClassPDF::x_[l] || xm > ClassPDF::x_[l + 1])
                        throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                         ") (h = " + to_str(h) + "), average equivalent volume (xm) " +
                                         "for coefficient " + to_str(k) + " in section " + to_str(l) +
                                         " is not within this section. xm = " + to_str(xm) + ", x[l] = " +
                                         to_str(ClassPDF::x_[l]) + ", x[l + 1] = " + to_str(ClassPDF::x_[l + 1]) +
                                         ", repartition_coefficient_number = " + to_str(repartition_coefficient_number) +
                                         ", repartition_coefficient_mass = " + to_str(repartition_coefficient_mass));
                    }
                }
            }
      }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset()
                         << "Performed " << N << " tests in " << AMCTimer<CPU>::GetTimer(time_index)
                         << " seconds (" << scientific << AMCTimer<CPU>::GetTimer(time_index) / real(N > 0 ? N : 1)
                         << " seconds/test)." << endl;
#endif
#endif
  }
#endif
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_SIZE_CXX
#endif
