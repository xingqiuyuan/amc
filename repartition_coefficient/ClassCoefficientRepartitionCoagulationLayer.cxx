// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_LAYER_CXX

#include "ClassCoefficientRepartitionCoagulationLayer.hxx"

namespace AMC
{
  // Compute layer repartition of coagulation couple.
  inline void ClassCoefficientRepartitionCoagulationLayer::
  compute_layer_repartition_couple(const int a, const int b, const int c,
                                   const real *concentration_number,
                                   const real *concentration_mass)
  {
    for (int h = 0; h < ClassLayer::Nspecies_layer_; ++h)
      {
        const int i = ClassLayer::species_index_layer_[h];

        const int ai = a * ClassSpecies::Nspecies_ + i;
        const int bi = b * ClassSpecies::Nspecies_ + i;

        const real factor_a(concentration_mass[ai] * concentration_number[b]),
          factor_b(concentration_mass[bi] * concentration_number[a]);

        vector1r &layer_repartition_couple_work = layer_repartition_couple_work_[i];

        real numerator(real(0));
        for (int l = 0; l < ClassLayer::Nlayer_[b]; ++l)
          {
            for (int j = 0; j < ClassLayer::Nlayer_[a]; ++j)
              numerator += repartition_coefficient_[c](j, l) * ClassAerosolData::layer_repartition_[ai][j];
            numerator = numerator * factor_a + ClassAerosolData::layer_repartition_[bi][l] * factor_b;

            layer_repartition_couple_work[l] = numerator / (factor_a + factor_b);
          }
      }
  }


  // Constructors.
  ClassCoefficientRepartitionCoagulationLayer::ClassCoefficientRepartitionCoagulationLayer()
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info() << Reset() << "Init layer coagulation repartition coefficient." << endl;
#endif

    // Resize coefficients.
    const int Ncouple = (ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2;
    compute_coefficient_.assign(Ncouple, true);
    repartition_coefficient_.resize(Ncouple);

    int h(-1);
    for (int b = 0; b < ClassAerosolData::Ng_; ++b)
      for (int a = 0; a <= b; ++a)
        {
          repartition_coefficient_[++h].resize(ClassLayer::Nlayer_[a], ClassLayer::Nlayer_[b]);
          compute_coefficient_[h] = ClassLayer::Nlayer_[b] > 1;
          
          if (! compute_coefficient_[h])
            repartition_coefficient_[h] = real(1);
        }

#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    const int time_index = AMCTimer<CPU>::Add("compute_coagulation_repartition_coefficient_layer");
    AMCTimer<CPU>::Start();
#endif

    // Compute coefficients.
    ComputeCoefficientLayer();

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
    *AMCLogger::GetLog() << Byellow() << Info(2) << Reset() << "Computed layer repartition"
                         << " coefficients in " << AMCTimer<CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif

    // Layer repartition for each couple coagulation.
    layer_repartition_couple_work_.assign(ClassSpecies::Nspecies_, vector1r(ClassLayer::Nlayer_max_, real(0)));

    return;
  }


  // Destructor.
  ClassCoefficientRepartitionCoagulationLayer::~ClassCoefficientRepartitionCoagulationLayer()
  {
    return;
  }


  // Compute methods.
  void ClassCoefficientRepartitionCoagulationLayer::ComputeCoefficientLayer()
  {
    int h(-1);
    for (int b = 0; b < ClassAerosolData::Ng_; ++b)
      {
        // Layer radius for b. We cannot make a proxy as coagulation makes the layer increase.
        vector1r layer_radius_b(ClassLayer::Nlayer_[b] + 1, real(0));

        for (int a = 0; a <= b; ++a)
          {
#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
            CBUG << "Computing repartition coefficients" << " of layers of a = "
                 << a << " into layers of b = " << b << " :" << endl;
#endif

            // Radius of coagulated particles.
            const real rab = pow(ClassAerosolData::layer_radius_[b].back() * ClassAerosolData::layer_radius_[b].back() *
                                 ClassAerosolData::layer_radius_[b].back() + ClassAerosolData::layer_radius_[a].back() *
                                 ClassAerosolData::layer_radius_[a].back() * ClassAerosolData::layer_radius_[a].back(),
                                 AMC_FRAC3);

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
            CBUG << "rab = " << rab << endl;
#endif

            // Compute layer radius for b due to coagulation (volume addition).
            for (int i = 1; i <= ClassLayer::Nlayer_[b]; ++i)
              layer_radius_b[i] = rab * ClassLayer::layer_radius_fraction_[b][i];

            // Proxy to layer radius for a.
            const vector1r &layer_radius_a = ClassAerosolData::layer_radius_[a];

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
            CBUG << "layer_radius_a = " << layer_radius_a << endl;
            CBUG << "layer_radius_b = " << layer_radius_b << endl;
#endif

            const real distance = rab - layer_radius_a.back();

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
            CBUG << "distance = " << distance << endl;
#endif

            if (compute_coefficient_[++h])
              for (int j = 0; j < ClassLayer::Nlayer_[a]; ++j)
                {
                  ClassRing ring_a(layer_radius_a[j], layer_radius_a[j + 1]);

                  // Layer volume.
                  const real layer_volume_a_inv = real(1) / ring_a.ComputeSelfVolume();

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
                  CBUG << "layer_volume_a = " << layer_volume_a << endl;
#endif

                  for (int l = 0; l < ClassLayer::Nlayer_[b]; ++l)
                    {
#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
                      CBUG << "\tfor layer j = " << j << " of a into layer l = " << l << " of b :" << endl;
#endif
                      ClassRing ring_b(layer_radius_b[l], layer_radius_b[l + 1]);

                      // Scale by volume of j^th layer of a.
                      repartition_coefficient_[h](j, l) = ring_b.ComputeCrossVolume(distance, ring_a) * layer_volume_a_inv;

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
                      CBUG << "\t\trepartition_coefficient = " << repartition_coefficient_[h](j, l) << endl;
#endif
                    }

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
                  real sum(real(0));
                  for (int l = 0; l < ClassLayer::Nlayer_[b]; ++l)
                    sum += repartition_coefficient_[h](j, l);

                  if (abs(sum - real(1)) > AMC_COEFFICIENT_REPARTITION_COAGULATION_LAYER_EPSILON)
                    throw AMC::Error("Sum of repartition coefficients for layers of general section " +
                                     to_str(a) + " into layers of general section " + to_str(b) +
                                     " is not equal to unity (" + to_str(sum) + ").");
#endif
                }
          }
      }
  }



  void ClassCoefficientRepartitionCoagulationLayer::
  ComputeLayerRepartitionCouple(const real *concentration_number,
                                const real *concentration_mass,
                                vector<vector<real> > &layer_repartition_couple)
  {
    // Layer coefficients should be updated prior to calling this function,
    // fortunately in most cases, layer repartition coefficients are computed once for all.

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Warning()
                         << "Layer coefficients should be updated prior to calling this function, fortunately, "
                         << "layer coefficients are computed once for all in most cases." << Reset().Str() << endl;
#endif

    // Layer repartition of species in coagulation couples.
    const int Ncouple = (ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2;
    layer_repartition_couple.resize(Ncouple * ClassSpecies::Nspecies_);

    int c(0);
    for (int b = 0; b < ClassAerosolData::Ng_; ++b)
      for (int a = 0; a <= b; ++a)
        {
          for (int i = 0; i < ClassSpecies::Nspecies_; ++i)
            if (ClassLayer::is_species_layered_[i])
              layer_repartition_couple[c + i].assign(ClassLayer::Nlayer_[b], real(1) / real(ClassLayer::Nlayer_[b]));
            else
              layer_repartition_couple[c + i].assign(1, real(1));

          if (concentration_number[a] * concentration_number[b] > real(0))
            {
              compute_layer_repartition_couple(a, b, c,
                                               concentration_number,
                                               concentration_mass);

              for (int h = 0; h < ClassLayer::Nspecies_layer_; ++h)
                {
                  const int i = ClassLayer::species_index_layer_[h];
                  const int ci = c + i;
                  for (int l = 0; l < ClassLayer::Nlayer_[b]; ++l)
                    layer_repartition_couple[ci][l] = layer_repartition_couple_work_[i][l];
                }
            }

          ++c;
        }
  }


  // Get methods.
  void ClassCoefficientRepartitionCoagulationLayer::GetCoefficientLayer(vector<vector<real> > &coefficient) const
  {
    // Resize coefficients.
    coefficient.resize((ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2);

    int h(-1);
    for (int b = 0; b < ClassAerosolData::Ng_; ++b)
      for (int a = 0; a <= b; ++a)
        {
          coefficient[++h].assign(ClassLayer::Nlayer_[a] * ClassLayer::Nlayer_[b], real(0));

          int k(-1);
          for (int j = 0; j < ClassLayer::Nlayer_[a]; ++j)
            for (int l = 0; l < ClassLayer::Nlayer_[b]; ++l)
              coefficient[h][++k] = repartition_coefficient_[h](j, l);
        }
  }
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_LAYER_CXX
#endif
