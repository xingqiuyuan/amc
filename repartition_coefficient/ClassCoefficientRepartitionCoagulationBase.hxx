// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_BASE_HXX

#define AMC_COEFFICIENT_REPARTITION_TYPE_STATIC  1
#define AMC_COEFFICIENT_REPARTITION_TYPE_MOVING  2
#define AMC_COEFFICIENT_REPARTITION_FRACTION_THRESHOLD_SUM 1.e-9

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationBase
   */
#if !defined(AMC_WITH_TRACE) && !defined(AMC_WITH_LAYER)
  class ClassCoefficientRepartitionCoagulationBase :
    protected ClassAerosolData
#else
  class ClassCoefficientRepartitionCoagulationBase :
#endif
#ifdef AMC_WITH_TRACE
    public ClassCoefficientRepartitionCoagulationTrace
#endif
#ifdef AMC_WITH_LAYER
    , public ClassCoefficientRepartitionCoagulationLayer, protected ClassRedistributionLayer
#endif
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Are repartition coefficients computed.*/
    bool is_computed_;

    /*!< Number of coagulation couples.*/
    int Ncouple_;

    /*!< Path to repartition coefficient file.*/
    string path_;

    /*!< Number of coefficients per coagulation couples.*/
    vector1i Ncoefficient_;

    /*!< General section indexes of repartition coefficients.*/
    vector2i repartition_coefficient_index_;

    /*!< Repartition coefficients for number and mass concentrations.*/
    vector2r repartition_coefficient_number_;
    vector3r repartition_coefficient_mass_;

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationBase(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationBase();

    /*!< Are repartition coefficients computed ?.*/
    bool IsComputed() const;

    /*!< Get methods.*/
    int GetNcouple() const;
    string GetPath() const;
    void GetNcoefficient(vector<int> &Ncoefficient) const;
    void GetCoefficientIndex(vector<vector<int> > &index) const;
    void GetCoefficientIndexReverse(vector<vector<int> > &index1, vector<vector<int> > &index2) const;
    void GetCoefficientNumber(vector<vector<real> > &coefficient) const;
    void GetCoefficientMass(const int i, vector<vector<real> > &coefficient) const;

    /*!< Set methods.*/
    void SetPath(const string path);

    /*!< Read repartition coefficients.*/
    void Read(string path = "");

    /*!< Write repartition coefficients.*/
    void Write() const;
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_BASE_HXX
#endif
