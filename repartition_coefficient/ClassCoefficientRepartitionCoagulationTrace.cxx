// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_TRACE_CXX

#include "ClassCoefficientRepartitionCoagulationTrace.hxx"

namespace AMC
{
  // Constructors.
  ClassCoefficientRepartitionCoagulationTrace::ClassCoefficientRepartitionCoagulationTrace(Ops::Ops &ops)
  {
    repartition_coefficient_index_.resize((ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Info() << Reset() << "Init trace coagulation"
                         << " repartition coefficient with default scheme." << endl;
#endif

    // Default scheme for trace repartition coefficients.
    int h(0);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      for (int j = 0; j <= i; ++j)
        {
          // If section i is trace and j is not, coagulation goes into this trace class;
          // if sections i and j are traces, coagulation goes into internal/external mixing,
          // for we do not know a priori the result fo coagulation between two trace classes,
          // but this may be overwritten by configuration.
          // If section is not trace, coagulation always goes into internal/external mixing,
          // whatever j is, unless overwritten by configuration.
          if (ClassTrace::general_section_index_reverse_[i] < 0)
            repartition_coefficient_index_[h] = -1;
          else
            if (ClassTrace::general_section_index_reverse_[j] < 0)
              repartition_coefficient_index_[h] = ClassTrace::general_section_index_reverse_[i];
            else
              repartition_coefficient_index_[h] = -1;
          h++;
        }

    // Eventually overwrites default scheme with configuration.
    const string prefix_orig = ops.GetPrefix();

    if (ops.Exists("trace"))
      {
#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset() << "Overwrite trace repartition coefficient default scheme"
                             << " with configuration section \"" << prefix_orig << "trace.\"" << endl;
#endif

        ops.SetPrefix(prefix_orig + "trace.");
        vector1s scheme_list = ops.GetEntryList(); 

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "scheme_list = " << scheme_list << endl;
#endif

        for (int i = 0; i < int(scheme_list.size()); i++)
          {
            vector1s name_list;
            ops.Set(scheme_list[i], "", name_list);

            if (int(name_list.size()) != 3)
              throw AMC::Error("For trace scheme \"" + scheme_list[i] + "\", need 3 class names.");

            vector1i index_list(3, -1);
            for (int j = 0; j < 3; j++)
              if (name_list[j].find("internal_mixing") == string::npos &&
                  name_list[j].find("external_mixing") == string::npos)
                {
                  for (int k = 0; k < ClassTrace::Ntrace_; k++)
                    if (name_list[j].find(ClassTrace::name_[k]) != string::npos)
                      {
                        index_list[j] = k;
                        break;
                      }

                  if (index_list[j] < 0)
                    throw AMC::Error("Did not find \"" + name_list[j] + "\" among trace classes.");
                }

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "Perform scheme \"" << scheme_list[i] << "\" :" << endl;
            for (int j = 0; j < 3; j++)
              *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "\t\""
                                   << name_list[j] << "\" with index = " << index_list[j] << endl;
#endif
            int h(0);
            for (int j = 0; j < ClassAerosolData::Ng_; ++j)
              for (int k = 0; k <= j; ++k)
                {
                  if ((ClassTrace::general_section_index_reverse_[j] == index_list[0] &&
                       ClassTrace::general_section_index_reverse_[k] == index_list[1]) ||
                      (ClassTrace::general_section_index_reverse_[j] == index_list[1] &&
                       ClassTrace::general_section_index_reverse_[k] == index_list[0]))
                    repartition_coefficient_index_[h] = index_list[2];
                  h++;
                }
          }

        ops.SetPrefix(prefix_orig);
      }

    return;
  }


  // Destructor.
  ClassCoefficientRepartitionCoagulationTrace::~ClassCoefficientRepartitionCoagulationTrace()
  {
    return;
  }

  // Get methods.
  void ClassCoefficientRepartitionCoagulationTrace::GetCoefficientIndexTrace(vector<int> &index) const
  {
    index = repartition_coefficient_index_;
  }
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_TRACE_CXX
#endif
