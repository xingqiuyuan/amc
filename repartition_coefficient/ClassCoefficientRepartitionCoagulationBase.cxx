// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_BASE_CXX

#include "ClassCoefficientRepartitionCoagulationBase.hxx"

namespace AMC
{
  // Constructors.
  ClassCoefficientRepartitionCoagulationBase::ClassCoefficientRepartitionCoagulationBase(Ops::Ops &ops)
    : Ncouple_((ClassAerosolData::Ng_ + 1) * ClassAerosolData::Ng_ / 2), is_computed_(false), path_("")
#ifdef AMC_WITH_TRACE
    , ClassCoefficientRepartitionCoagulationTrace(ops)
#endif
#ifdef AMC_WITH_LAYER
    , ClassCoefficientRepartitionCoagulationLayer()
#endif
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug() << Reset() << "Ncouple = " << Ncouple_ << endl;
#endif

    // Path to coefficient file, if any.
    path_ = ops.Get<string>("path", "", "");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug() << Reset() << "path = \"" << path_ << "\"." << endl;
#endif

    // If real path, read directly in constructor.
    ifstream fin(path_.c_str());

    int file_size(0);
    if (fin.good())
      {
        fin.seekg(0, ios_base::end);
        file_size = fin.tellg();
      }

    fin.close();

    if (file_size > 0)
      Read();

    return;
  }


  // Destructor.
  ClassCoefficientRepartitionCoagulationBase::~ClassCoefficientRepartitionCoagulationBase()
  {
    return;
  }


  // Are repartition coefficients computed ?.
  bool ClassCoefficientRepartitionCoagulationBase::IsComputed() const
  {
    return is_computed_;
  }


  // Get methods.
  int ClassCoefficientRepartitionCoagulationBase::GetNcouple() const
  {
    return Ncouple_;
  }


  string ClassCoefficientRepartitionCoagulationBase::GetPath() const
  {
    return path_;
  }


  void ClassCoefficientRepartitionCoagulationBase::GetNcoefficient(vector<int> &Ncoefficient) const
  {
    Ncoefficient = Ncoefficient_;
  }


  void ClassCoefficientRepartitionCoagulationBase::GetCoefficientIndex(vector<vector<int> > &index) const
  {
    index = repartition_coefficient_index_;
  }


  void ClassCoefficientRepartitionCoagulationBase::GetCoefficientIndexReverse(vector<vector<int> > &index1,
                                                                              vector<vector<int> > &index2) const
  {
    index1.resize(ClassAerosolData::Ng_);
    index2.resize(ClassAerosolData::Ng_);

    for (int i1 = 0; i1 < ClassAerosolData::Ng_; ++i1)
      for (int i2 = 0; i2 <= i1; ++i2)
        {
          const int i = (i1 + 1) * i1 / 2 + i2;
          for (int j = 0; j < Ncoefficient_[i]; ++j)
            {
              const int k = repartition_coefficient_index_[i][j];
              index1[k].push_back(i1);
              index2[k].push_back(i2);
            }
        }
  }


  void ClassCoefficientRepartitionCoagulationBase::GetCoefficientNumber(vector<vector<real> > &coefficient) const
  {
    if (! is_computed_)
      throw AMC::Error("Repartition coefficients not computed yet.");

    coefficient = repartition_coefficient_number_;
  }


  void ClassCoefficientRepartitionCoagulationBase::GetCoefficientMass(const int i, vector<vector<real> > &coefficient) const
  {
    if (! is_computed_)
      throw AMC::Error("Repartition coefficients not computed yet.");

    coefficient.resize(Ncouple_);

    for (int j = 0; j < Ncouple_; ++j)
      {
        coefficient[j].assign(Ncoefficient_[j], real(0));

        for (int k = 0; k < Ncoefficient_[j]; ++k)
          coefficient[j][k] = repartition_coefficient_mass_[j][k][i];
      }
  }


  // Set methods.
  void ClassCoefficientRepartitionCoagulationBase::SetPath(const string path)
  {
    path_ = path;
  }


  // Read repartition coefficients.
  void ClassCoefficientRepartitionCoagulationBase::Read(string path)
  {
    if (path.empty())
      path = path_;

    check_file(path);

    ifstream fin(path.c_str(), ifstream::binary);

    fin.read(reinterpret_cast<char*>(&Ncouple_), sizeof(int));

    int Nspecies(0);
    fin.read(reinterpret_cast<char*>(&Nspecies), sizeof(int));

    Ncoefficient_.resize(Ncouple_);
    fin.read(reinterpret_cast<char*>(Ncoefficient_.data()), Ncouple_ * sizeof(int));

    repartition_coefficient_index_.resize(Ncouple_);
    repartition_coefficient_number_.resize(Ncouple_);
    repartition_coefficient_mass_.resize(Ncouple_);

    for (int i = 0; i < Ncouple_; ++i)
      {
        repartition_coefficient_index_[i].resize(Ncoefficient_[i]);
        repartition_coefficient_number_[i].resize(Ncoefficient_[i]);
        repartition_coefficient_mass_[i].assign(Ncoefficient_[i], vector1r(Nspecies));

        fin.read(reinterpret_cast<char*>(repartition_coefficient_index_[i].data()), Ncoefficient_[i] * sizeof(int));
        fin.read(reinterpret_cast<char*>(repartition_coefficient_number_[i].data()), Ncoefficient_[i] * sizeof(real));

        for (int j = 0; j < Ncoefficient_[i]; ++j)
          fin.read(reinterpret_cast<char*>(repartition_coefficient_mass_[i][j].data()), Nspecies * sizeof(real));
      }

    fin.close();

    // Coefficients are computed.
    is_computed_ = true;
  }


  // Write repartition coefficients.
  void ClassCoefficientRepartitionCoagulationBase::Write() const
  {
    if (! is_computed_)
      throw AMC::Error("Repartition coefficients not computed yet.");

    ofstream fout(path_.c_str(), ofstream::binary);
    if (! fout.good())
      throw AMC::Error("Unable to open repartition coefficient file \"" + path_ + "\" for writing.");

    fout.write(reinterpret_cast<const char*>(&Ncouple_), sizeof(int));

    const int Nspecies = int(repartition_coefficient_mass_[0][0].size());
    fout.write(reinterpret_cast<const char*>(&Nspecies), sizeof(int));

    fout.write(reinterpret_cast<const char*>(Ncoefficient_.data()), Ncouple_ * sizeof(int));

    for (int i = 0; i < Ncouple_; ++i)
      {
        fout.write(reinterpret_cast<const char*>(repartition_coefficient_index_[i].data()), Ncoefficient_[i] * sizeof(int));
        fout.write(reinterpret_cast<const char*>(repartition_coefficient_number_[i].data()), Ncoefficient_[i] * sizeof(real));
        for (int j = 0; j < Ncoefficient_[i]; ++j)
          fout.write(reinterpret_cast<const char*>(repartition_coefficient_mass_[i][j].data()), Nspecies * sizeof(real));
      }

    fout.close();
  }
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_BASE_CXX
#endif
