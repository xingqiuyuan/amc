// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_SIZE_HXX

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT_SIZE
#define AMC_COEFFICIENT_REPARTITION_SIZE_THRESHOLD_SUM 1.e-12
#endif
#define AMC_COEFFICIENT_REPARTITION_SIZE_THRESHOLD 1.e-8

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationSize
   */
  class ClassCoefficientRepartitionCoagulationSize : protected ClassPDF, protected ClassAerosolData
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector2b vector2b;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Are repartition coefficients computed.*/
    bool is_computed_;

    /*!< Number of coagulation couples.*/
    int Ncouple_;

    /*!< General section size index, proxy from ClassAMC.*/
    vector1i general_section_size_;

    /*!< Number of coefficients per coagulation couples.*/
    vector1i Ncoefficient_;

    /*!< General section indexes of repartition coefficients.*/
    vector2i repartition_coefficient_index_;

    /*!< Repartition coefficients for number and mass concentrations.*/
    vector2r repartition_coefficient_number_;
    vector2r repartition_coefficient_mass_;

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationSize();

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationSize();

    /*!< Clear.*/
    void Clear();

    /*!< Get methods.*/
    int GetNcouple() const;
    void GetCoefficientNumber(vector<vector<real> > &coefficient) const;
    void GetCoefficientMass(vector<vector<real> > &coefficient) const;

    /*!< Compute repartition coefficients.*/
    void ComputeCoefficient(const vector<real> &x);
    void ComputeCoefficientCenter();

#ifdef AMC_WITH_TEST
    /*!< Test method.*/
    void Test(const int N = 1e5, const real tol = real(1.e-12),
              const real rmin = 0.001, const real rmax = 0.999);
#endif
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_SIZE_HXX
#endif
