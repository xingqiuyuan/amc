// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PDF_CXX

#include "ClassPDF.hxx"

namespace AMC
{
  // Compute case.
  inline int ClassPDF::compute_pdf_(const int i, const real &xi, vector1r &step,
                                    vector1r &alpha, vector1r &beta, vector1r &gamma) const
  {
    real &yimim = step[0];
    real &yiim  = step[2];
    real &yimip = step[3];
    real &yii   = step[4];
    real &yiip  = step[6];
    real &yipip = step[8];

    yimim = real(0);
    yipip = real(1);
    yimip = real(0.5);
    yii = (xi - x_[i]) * dx_inv_[i];
    yiim = real(0.5) * yii;
    yiip = yimip + yiim;

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "yimim = " << yimim << endl;
    CBUG << "yiim = " << yiim << endl;
    CBUG << "yimip = " << yimip << endl;
    CBUG << "yii = " << yii << endl;
    CBUG << "yiip = " << yiip << endl;
    CBUG << "yipip = " << yipip << endl;
#endif

    const real ui = (real(1) - yiip) / yiim;
    const real vi = real(1) / ui;

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "ui = " << ui << endl;
    CBUG << "vi = " << vi << endl;
#endif

    // No hope to get something good if
    // one of these are strictly negative.
    if (ui < real(0) || vi < real(0))
      return -1;

    // PDF values.
    const real t0 = real(2) * ui;
    const real t1 = real(2) * vi;

    beta[0] = t0 * t0;
    beta[9] = - beta[0];

    beta[4] = t1 * t1;
    beta[13] = - beta[4];

    beta[3] = real(8) - beta[0];
    beta[10] = beta[4] - real(8);

    alpha[0] = real(0);
    alpha[4] = real(2) * t1;
    alpha[9] = real(2) * t0;
    alpha[3] = real(4) * yii;
    alpha[10] = alpha[3] * ui;
    alpha[13] = real(0);

    gamma[0] = real(0);
    gamma[9] = real(0);

    gamma[3] = yii;
    gamma[10] = yii;

    gamma[4] = real(1);
    gamma[13] = real(1);

    if (yii > real(0.5))
      {
        // Case 13.
        return 19;
      }
    else
      {
        // Case 11.
        return 18;
      }

    // If not returned yet.
    return -1;
  }


  inline int ClassPDF::compute_pdf_(const int i, const int j, const real &xi, const real &xj,
                                    vector1r &step, vector1r &alpha, vector1r &beta, vector1r &gamma) const
  {
    // Index of couple.
    const int k = i * (i + 1) / 2 + j;

    real &yimjm = step[0];
    real &yimj  = step[1];
    real &yijm  = step[2];
    real &yimjp = step[3];
    real &yij   = step[4];
    real &yipjm = step[5];
    real &yijp  = step[6];
    real &yipj  = step[7];
    real &yipjp = step[8];

    yimjm = real(0);
    yipjp = real(1);
    yimj = (xj - x_[j]) * dy_inv_[k];
    yijm = (xi - x_[i]) * dy_inv_[k];
    yimjp = y_[k][1];
    yipjm = y_[k][2];
    yipj = yimj + yipjm;
    yijp = yijm + yimjp;
    yij = yimj + yijm;
    
#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "yimjm = " << yimjm << endl;
    CBUG << "yijm = " << yijm << endl;
    CBUG << "yimj = " << yimj << endl;
    CBUG << "yimjp = " << yimjp << endl;
    CBUG << "yij = " << yij << endl;
    CBUG << "yipjm = " << yipjm << endl;
    CBUG << "yijp = " << yijp << endl;
    CBUG << "yipj = " << yipj << endl;
    CBUG << "yipjp = " << yipjp << endl;
#endif

    // Compute PDF for given case.
    const real ui = (real(1) - yijp) / yijm;
    const real uj = (real(1) - yipj) / yimj;
    const real vi = real(1) / ui;
    const real vj = real(1) / uj;

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "ui = " << ui << endl;
    CBUG << "vi = " << vi << endl;
    CBUG << "uj = " << uj << endl;
    CBUG << "vj = " << vj << endl;
#endif

    // No hope to get something good if
    // one of these are strictly negative.
    if (ui < real(0) || vi < real(0) ||
        uj < real(0) || vj < real(0))
      return -1;

    // PDF values.
    alpha[1] = ui / yipjm;
    alpha[5] = alpha[1]; 
    alpha[6] = alpha[1]; 

    alpha[12] = vi / yipjm;
    alpha[7] = alpha[12]; 
    alpha[8] = alpha[12]; 

    alpha[2] = uj / yimjp;
    alpha[11] = vj / yimjp;

    alpha[4] = alpha[11] + alpha[12];
    alpha[9] = alpha[1] + alpha[2];

    beta[0] = alpha[1] * alpha[2];
    beta[1] = alpha[1] * alpha[11];

    beta[2] = alpha[2] * alpha[12];
    beta[4] = alpha[11] * alpha[12];

    beta[9] = - beta[0];
    beta[11] = - beta[1];

    beta[12] = - beta[2];
    beta[13] = - beta[4];

    beta[3] = beta[1] + beta[2] - beta[0];
    beta[10] = beta[4] - beta[1] - beta[2];

    beta[6] = beta[2] - beta[0];
    beta[7] = beta[4] - beta[1];

    alpha[3] = beta[1] * yijm  + beta[2] * yimj;
    alpha[10] = beta[2] * (real(1) - yijp)  + beta[1] * (real(1) - yipj);

    gamma[0] = real(0);
    gamma[1] = yimjp;
    gamma[2] = yipjm;
    gamma[3] = yij;
    gamma[4] = real(1);
    gamma[5] = real(0);
    gamma[6] = yijm;
    gamma[7] = yijp;
    gamma[8] = real(0);
    gamma[9] = real(0);
    gamma[10] = yij;
    gamma[11] = yimjp;
    gamma[12] = yipjm;
    gamma[13] = real(1);

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "alpha = " << alpha << endl;
    CBUG << "beta = " << beta << endl;
    CBUG << "gamma = " << gamma << endl;
#endif

    // Determine in which case we are.
    if (yijm < yimj)
      {
        // Case 2.
        if (yipjm < yijp)
          {
            // Case 21.
            if (yij < yimjp)
              {
                // Case 211.
                return 8;
              }
            else if (yipjm < yij)
              {
                // Case 213.
                return 10;
              }
            else
              {
                // Case 212.
                return 9;
              }
          }
        else
          {
            // Case 22.
            if (yij < yimjp)
              {
                // Case 221.
                return 11;
              }
            else
              {
                // Case 222.
                return 12;
              }
          }
      }
    else if (yipj < yijp)
      {
        // Case 3.
        if (yijm < yimjp)
          {
            // Case 31.
            if (yij < yimjp)
              {
                // Case 311.
                return 13;
              }
            else if (yipjm < yij)
              {
                // Case 313.
                return 15;
              }
            else
              {
                // Case 312.
                return 14;
              }
          }
        else
          {
            // Case 32.
            if (yij < yipjm)
              {
                // Case 321.
                return 16;
              }
            else
              {
                // Case 322.
                return 17;
              }
          }
      }
    else
      {
        // Case 1.
        if (yijm < yimjp)
          {
            if (yijp < yipjm)
              {
                // Case 13.
                if (yij < yimjp)
                  {
                    // Case 131.
                    return 5;
                  }
                else
                  {
                    // Case 132.
                    return 6;
                  }
              }
            else
              {
                // Case 11.
                if (yij < yimjp)
                  {
                    // Case 111.
                    return 0;
                  }
                else if (yipjm < yij)
                  {
                    // Case 113.
                    return 2;
                  }
                else
                  {
                    // Case 112.
                    return 1;
                  }
              }
          }
        else
          {
            if (yijp < yipjm)
              {
                // Case 14.
                return 7;
              }
            else
              {
                // Case 12.
                if (yij < yipjm)
                  {
                    // Case 121.
                    return 3;
                  }
                else
                  {
                    // Case 122.
                    return 4;
                  }
              }
          }
      }

    // If not returned yet.
    return -1;
  }


  // Integrate pdf.
  inline void ClassPDF::integrate_pdf_(const int h, const int icase, const vector1r &step, const vector1r &alpha,
                                       const vector1r &beta, const vector1r &gamma, vector1r &f, vector1r &g) const
  {
    // Bounds of integrals.
    const vector1r &z = z_[h];

    // Alias for given case.
    const int Nstep_case = Nstep_case_[icase];
    const vector1i &case_pdf = case_pdf_[icase];
    const vector1i &case_step = case_step_[icase];

    // Loop on PDF segments.
    int l0(0), l1(1), m0(0), m1(0);
    real x0(real(0)), x1(step[case_step[l0]]);
    real dx, xm, xa, xb, fa, fb;

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "z = " << z << endl;
    CBUG << "Nstep_case = " << Nstep_case << endl;
#endif

    while (l1 < Nstep_case)
      {
#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "l1 = " << l1 << endl;
#endif

        // Determine segment.
        x0 = x1;
        x1 = step[case_step[l1]];

        m1 = m0;

        // Index of PDF shape.
        const int o = case_pdf[l0];

#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "x1 = " << x1 << endl;
        CBUG << "m0 = " << m0 << endl;
        CBUG << "z[m0] = " << z[m0] << endl;
#endif

        if (x1 > z[m0])
          x1 = z[m0++];
        else
          l0 = l1++;

#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "x0 = " << x0 << ", x1 = " << x1 << endl;
#endif

        // Compute coefficents by integrating PDF over each segment.
        dx = (x1 - x0) * real(0.5);
        xm = (x1 + x0) * real(0.5);
        xa = xm - dx * real(AMC_PDF_GAUSS_LEGENDRE_ROOT);
        xb = xm + dx * real(AMC_PDF_GAUSS_LEGENDRE_ROOT);

        fa = alpha[o] + beta[o] * (xa - gamma[o]);
        fb = alpha[o] + beta[o] * (xb - gamma[o]);

#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "m1 = " << m1 << endl;
#endif

        f[m1] += (fa + fb) * dx;
        g[m1] += (xa * fa + xb * fb) * dx;
      }
  }


  // Constructor.
  ClassPDF::ClassPDF()
    : s_(ClassDiscretizationSize::Nsection_), nmax_(0),
      factor_(AMC_PI6 * ClassDiscretizationSize::density_fixed_),
      x_(ClassDiscretizationSize::diameter_bound_)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Instantiate ClassPDF, used to compute"
                         << " the Probability Density Function of coagulation couples." << endl;
#endif

    // Volume equivalent.
    for (int i = 0; i <= s_; ++i)
      x_[i] = x_[i] * x_[i] * x_[i];

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "s = " << s_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "x = " << x_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "factor = " << factor_ << endl;
#endif

    cx_.resize(s_);
    dx_.resize(s_);
    dx_inv_.resize(s_);

    for (int i = 0; i < s_; ++i)
      {
        cx_[i] = (x_[i + 1] + x_[i]) * real(0.5);
        dx_[i] = x_[i + 1] - x_[i];
        dx_inv_[i] = real(1) / dx_[i];
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "dx = " << dx_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(3) << Reset() << "dx_inv = " << dx_inv_ << endl;
#endif

    // For each couple.
    y_.resize(s_ * (s_ + 1) / 2);
    dy_.resize(s_ * (s_ + 1) / 2);
    dy_inv_.resize(s_ * (s_ + 1) / 2);
    n_.resize(s_ * (s_ + 1) / 2);
    i_.resize(s_ * (s_ + 1) / 2);
    z_.resize(s_ * (s_ + 1) / 2);

    int h(0);
    for (int i = 0; i < s_; ++i)
      for (int j = 0; j <= i; ++j)
        {
          y_[h].resize(4);
          y_[h][0] = x_[i] + x_[j];
          y_[h][1] = x_[i] + x_[j + 1];
          y_[h][2] = x_[i + 1] + x_[j];
          y_[h][3] = x_[i + 1] + x_[j + 1];

          dy_[h] = dx_[i] + dx_[j];
          dy_inv_[h] = real(1) / dy_[h];

          const real ximjm = y_[h][0];
          const real xipjp = y_[h][3];

          for (int k = 3; k > 0; --k)
            y_[h][k] = (y_[h][k] - y_[h][0]) * dy_inv_[h];

          int imjm = search_index(x_, ximjm);
          int ipjp = search_index(x_, xipjp);

          // Keep coagulation out of discretization into last section.
          ipjp = ipjp < s_ ? ipjp : s_ - 1;

#ifdef AMC_WITH_DEBUG_PDF
          CBUG << "ximjm = " << ximjm << endl;
          CBUG << "xipjp = " << xipjp << endl;
          CBUG << "imjm = " << imjm << endl;
          CBUG << "ipjp = " << ipjp << endl;
#endif

          // Number of coefficients.
          n_[h] = ipjp - imjm + 1;

#ifdef AMC_WITH_DEBUG_PDF
          CBUG << "n = " << n_[h] << endl;
#endif

          if (nmax_ < n_[h])
            nmax_ = n_[h];

          // Indexes of sections.
          i_[h].resize(n_[h]);
          for (int k = 0; k < n_[h]; ++k)
            i_[h][k] = imjm + k;

          // Bounds of repartition coefficients.
          z_[h].resize(n_[h]);
          for (int k = 1; k < n_[h]; ++k)
            z_[h][k - 1] = (x_[i_[h][k]] - ximjm) / dy_[h];
          z_[h].back() = real(1);

          for (int k = 0; k < n_[h] - 1; ++k)
            if (z_[h][k] <= real(0) || z_[h][k] >= real(1))
              throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                               "), z[k] is not within ]0, 1[  (k = " + to_str(k) +
                               " z[k] = " + to_str(z_[h][k]) + ").");

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset()
                               << "For couple (i, j) = (" << i << ", " << j
                               << ") (h = " << h << ") : " << endl 
                               << "\ty = " << y_[h] << endl
                               << "\tdy = " << dy_[h] << endl
                               << "\tn = " << n_[h] << endl
                               << "\ti = " << i_[h] << endl
                               << "\tz = " << z_[h] << endl;
#endif
          h++;
        }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "nmax = " << nmax_ << endl;
#endif

    return;
  }


  // Destructor.
  ClassPDF::~ClassPDF()
  {
    return;
  }


  // Get methods.
  vector<real> ClassPDF::GetX() const
  {
    return x_;
  }

  vector<real> ClassPDF::GetCX() const
  {
    return cx_;
  }

  int ClassPDF::GetNpdf()
  {
    return Npdf_;
  }

  int ClassPDF::GetNcase()
  {
    return Ncase_;
  }

  int ClassPDF::GetNstep()
  {
    return Nstep_;
  }

  vector<string> ClassPDF::GetPdfName()
  {
    return pdf_;
  }

  vector<int> ClassPDF::GetCaseName()
  {
    return case_;
  }

  vector<string> ClassPDF::GetStepName()
  {
    return step_;
  }

  int ClassPDF::GetNpdfCase(const int i)
  {
    return Npdf_case_[i];
  }

  int ClassPDF::GetNstepCase(const int i)
  {
    return Nstep_case_[i];
  }

  vector<int> ClassPDF::GetCasePdfIndex(const int i)
  {
    return case_pdf_[i];
  }

  vector<int> ClassPDF::GetCaseStepIndex(const int i)
  {
    return case_step_[i];
  }


  // Compute the PDF.
  int ClassPDF::Compute(const int i, const int j, const real &fi, const real &fj,
                        vector<real> &y, vector<real> &p, const bool test) const
  {
    // Algorithm only valid for i >= j.
    if (i < j) throw AMC::Error("First index must be >= to second one.");

    // Algorithm only valid if fi and fj within ]0, 1[.
    if (fi <= 0 || fi >= 1) throw AMC::Error("First fraction must be within ]0, 1[.");
    if (fj <= 0 || fj >= 1) throw AMC::Error("Second fraction must be within ]0, 1[.");

    // Compute case.
    vector1r alpha(Npdf_, real(0)), beta(Npdf_, real(0)),
      gamma(Npdf_, real(0)), step(Nstep_, real(0));

    int icase(-1);

    if (i == j && fi == fj && (! test))
      {
        const real xi = x_[i] + fi * dx_[i];

#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "xi = " << xi << endl;
#endif

        icase = compute_pdf_(i, xi, step, alpha, beta, gamma);
      }
    else
      {
        const real xi = x_[i] + fi * dx_[i];
        const real xj = x_[j] + fj * dx_[j];

#ifdef AMC_WITH_DEBUG_PDF
        CBUG << "xi = " << xi << endl;
        CBUG << "xj = " << xj << endl;
#endif

        icase = compute_pdf_(i, j, xi, xj, step, alpha, beta, gamma);
      }

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "icase = " << icase << endl;
#endif

    if (icase < 0)
      throw AMC::Error("Unable to find the correct case.");

    // Compute PDF.
    const int Npdf_case = Npdf_case_[icase];
    const int Nstep_case = Nstep_case_[icase];
    const vector1i &case_pdf = case_pdf_[icase];
    const vector1i &case_step = case_step_[icase];

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "Npdf_case = " << Npdf_case << endl;
    CBUG << "Nstep_case = " << Nstep_case << endl;
    CBUG << "case_pdf = " << case_pdf << endl;
    CBUG << "case_step = " << case_step << endl;
    CBUG << "alpha = " << alpha << endl;
    CBUG << "beta = " << beta << endl;
    CBUG << "gamma = " << gamma << endl;
#endif

    y.assign(Nstep_case, 0);
    for (int i = 0; i < Nstep_case; ++i)
      y[i] = step[case_step[i]];

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "y = " << y << endl;
#endif

    p.assign(2 * Npdf_case, 0);

    int h(-1);
    for (int i = 0; i < Npdf_case; ++i)
      {
        const int j = case_pdf[i];

        p[++h] = alpha[j] + beta[j] * (y[i] - gamma[j]);
        p[++h] = alpha[j] + beta[j] * (y[i + 1] - gamma[j]);
      }

    // At last, return the case with which we worked.
    return icase;
  }


  // Compute the PDF.
  void ClassPDF::Integrate(const int i, const int j, const real &fi, const real &fj,
                           vector<real> &f, vector<real> &g) const
  {
    // Algorithm only valid for i >= j.
    if (i < j) throw AMC::Error("First index must be >= to second one.");

    // Algorithm only valid if fi and fj within ]0, 1[.
    if (fi <= 0 || fi >= 1) throw AMC::Error("First fraction must be within ]0, 1[.");
    if (fj <= 0 || fj >= 1) throw AMC::Error("Second fraction must be within ]0, 1[.");

    // Compute case.
    vector1r alpha(Npdf_, real(0)), beta(Npdf_, real(0)),
      gamma(Npdf_, real(0)), step(Nstep_, real(0));

    const real xi = x_[i] + fi * dx_[i];
    const real xj = x_[j] + fj * dx_[j];

#ifdef AMC_WITH_DEBUG_PDF
    CBUG << "xi = " << xi << endl;
    CBUG << "xj = " << xj << endl;
#endif

    // Index of couple.
    const int h = i * (i + 1) / 2 + j;

    // Number of integrals.
    const int n = n_[h];

    if (n > 1)
      {
        f.assign(n, real(0));
        g.assign(n, real(0));

        // PDF case.
        int icase(-1);

        if (i != j || xi != xj)
          icase = compute_pdf_(i, j, xi, xj, step, alpha, beta, gamma);
        else
          icase = compute_pdf_(i, xi, step, alpha, beta, gamma);

        integrate_pdf_(h, icase, step, alpha, beta, gamma, f, g);

        // Normalize g.
        const real yij_inv = real(1) / step[4];
        for (int k = 0; k < n; ++k)
          g[k] *= yij_inv;
      }
  }


#ifdef AMC_WITH_TEST
  // Test PDF class.
  void ClassPDF::Test(const int N, const real tol) const
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test PDF class with "
                         << Fred().Str() << "N = " << N << Reset().Str() << " and " << Fred().Str()
                         << " tol = " << tol << Reset().Str() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "s = " << s_ << endl;
#endif

    const real delta = real(1) / real(N);

    // Local arrays.
    vector1r alpha(Npdf_, real(0)), beta(Npdf_, real(0)),
      gamma(Npdf_, real(0)), step(Nstep_, real(0)),
      y(Nstep_, real(0)), p(Nstep_, real(0)),
      f(nmax_, real(0)), g(nmax_, real(0));

#ifdef AMC_WITH_TIMER
    int time_index_compute = AMCTimer<CPU>::Add("test_pdf_compute");
    int time_index_integrate = AMCTimer<CPU>::Add("test_pdf_integrate");
#endif

    // Loop on size sections and their centers.
    int count_compute(0), count_integrate(0);

    for (int i = 0; i < s_; ++i)
      for (int j = 0; j <= i; ++j)
        {
          // Index of couple.
          const int h = i * (i + 1) / 2 + j;

          // Number of integrals.
          const int n = n_[h];

          // Alias to couple size section bound.
          const vector1r &z = z_[h];

          for (int k = 1; k < N; ++k)
            for (int l = 1; l < N; ++l)
              {
                const real fk = real(k) * delta;
                const real fl = real(l) * delta;

                const real xi = x_[i] + fk * dx_[i];
                const real xj = x_[j] + fl * dx_[j];

                // PDF case.
                int icase(-1);

#ifdef AMC_WITH_TIMER
                AMCTimer<CPU>::Start();
#endif

                if (i != j || k != l)
                  icase = compute_pdf_(i, j, xi, xj, step, alpha, beta, gamma);
                else
                  icase = compute_pdf_(i, xi, step, alpha, beta, gamma);

#ifdef AMC_WITH_TIMER
                AMCTimer<CPU>::Stop(time_index_compute);
#endif

                if (icase < 0)
                  throw AMC::Error("Unable to find the correct case (icase < 0) with (i, j) = (" + to_str(i) +
                                   ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                   ") : icase = " + to_str(icase) + ", step = " + to_str(step));

                const int Npdf_case = Npdf_case_[icase];
                const int Nstep_case = Nstep_case_[icase];
                const vector1i &case_pdf = case_pdf_[icase];
                const vector1i &case_step = case_step_[icase];

                y.assign(Nstep_case, 0);
                for (int m = 0; m < Nstep_case; ++m)
                  y[m] = step[case_step[m]];

                for (int m = 1; m < Npdf_case; ++m)
                  {
                    const int o = case_pdf[m];
                    p[m] = alpha[o] + beta[o] * (y[m] - gamma[o]);
                  }

                // Performs self tests.
                real dx(real(0)), xm(real(0)), xa(real(0)), xb(real(0));
                real fa(real(0)), fb(real(0)), psum(real(0)), pxsum(real(0));

                real x0(real(0)), x1(step[case_step[0]]);

                for (int m = 0; m < Npdf_case; ++m)
                  {
                    // Determine segment.
                    x0 = x1;
                    x1 = step[case_step[m + 1]];

                    const int o = case_pdf[m];

                    dx = (x1 - x0) * real(0.5);
                    xm = (x1 + x0) * real(0.5);
                    xa = xm - dx * real(AMC_PDF_GAUSS_LEGENDRE_ROOT);
                    xb = xm + dx * real(AMC_PDF_GAUSS_LEGENDRE_ROOT);

                    fa = alpha[o] + beta[o] * (xa - gamma[o]);
                    fb = alpha[o] + beta[o] * (xb - gamma[o]);

                    psum += (fa + fb) * dx;
                    pxsum += (xa * fa + xb * fb) * dx;
                  }

                if (abs(psum - real(1)) > tol)
                  throw AMC::Error("PDF sum is not unity with (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                   ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) + ") : icase = " +
                                   to_str(icase) + ", step = " + to_str(step));

                const real &yij = step[4];

                if (abs(pxsum / yij - real(1)) > tol)
                  throw AMC::Error("PDF esperancy is not equal to xij (= step[4]) with (i, j) = (" + to_str(i) +
                                   ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                   ") : icase = " + to_str(icase) + ", step = " + to_str(step));

                // Increment counter.
                count_compute++;

                // Test integration.
                if (n > 1)
                  {
                    f.assign(nmax_, real(0));
                    g.assign(nmax_, real(0));

#ifdef AMC_WITH_TIMER
                    AMCTimer<CPU>::Start();
#endif

                    integrate_pdf_(h, icase, step, alpha, beta, gamma, f, g);

                    // Normalize g.
                    const real yij_inv = real(1) / yij;
                    for (int m = 0; m < n; ++m)
                      g[m] *= yij_inv;

#ifdef AMC_WITH_TIMER
                    AMCTimer<CPU>::Stop(time_index_integrate);
#endif

                    for (int m = 0; m < n; ++m)
                      if (f[m] < real(0))
                        throw AMC::Error("Number (f) coefficient " + to_str(m) + " is < 0 with (i, j) = (" + to_str(i) +
                                         ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                         ") : icase = " + to_str(icase) + ", step = " + to_str(step) +
                                         ", f = " + to_str(f) + ", g = " + to_str(g) + ".");

                    for (int m = 0; m < n; ++m)
                      if (g[m] < real(0))
                        throw AMC::Error("Mass (g) coefficient " + to_str(m) + " is < 0 with (i, j) = (" + to_str(i) +
                                         ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                         ") : icase = " + to_str(icase) + ", step = " + to_str(step) +
                                         ", f = " + to_str(f) + ", g = " + to_str(g) + ".");

                    if (abs(compute_vector_sum(f, n) - real(1)) > tol)
                      throw AMC::Error("Number (f) coefficient sum is not unity with (i, j) = (" + to_str(i) +
                                       ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                       ") : icase = " + to_str(icase) + ", step = " + to_str(step) +
                                       ", f = " + to_str(f) + ", g = " + to_str(g) + ".");

                    if (abs(compute_vector_sum(g, n) - real(1)) > tol)
                      throw AMC::Error("Mass (g) coefficient sum is equal to unity with (i, j) = (" + to_str(i) +
                                       ", " + to_str(j) + ") and (fi, fj) = (" + to_str(fk) + ", " + to_str(fl) +
                                       ") : icase = " + to_str(icase) + ", step = " + to_str(step) +
                                       ", f = " + to_str(f) + ", g = " + to_str(g) + ".");

                    {
                      const real ym = yij * g[0] / f[0];

                      if (ym > z[0])
                        throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                         ") (h = " + to_str(h) + "), average equivalent volume (ym) " +
                                         "for coefficient " + to_str(0) + " is not within section." +
                                         " ym = " + to_str(ym) + ", z = " + to_str(z) +
                                         ", f = " + to_str(f) + ", g = " + to_str(g));
                    }

                    for (int m = 1; m < n; ++m)
                      {
                        const real ym = yij * g[m] / f[m];

                        if (ym < z[m - 1] || ym > z[m])
                          throw AMC::Error("For couple (i, j) = (" + to_str(i) + ", " + to_str(j) +
                                           ") (h = " + to_str(h) + "), average equivalent volume (ym) " +
                                           "for coefficient " + to_str(m) + " is not within section." +
                                           " ym = " + to_str(ym) + ", z = " + to_str(z) +
                                           ", f = " + to_str(f) + ", g = " + to_str(g));
                      }

                    // Increment counter.
                    count_integrate++;
                  }
              }
        }

#ifdef AMC_WITH_TIMER
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << "Performed " << count_compute << " computations in "
                         << AMCTimer<CPU>::GetTimer(time_index_compute) << " seconds (" << scientific
                         << AMCTimer<CPU>::GetTimer(time_index_compute) / real(count_compute > 0 ? count_compute : 1)
                         << " seconds/test)." << endl;
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << "Performed " << count_integrate << " integrations in "
                         << AMCTimer<CPU>::GetTimer(time_index_integrate) << " seconds (" << scientific
                         << AMCTimer<CPU>::GetTimer(time_index_integrate) / real(count_integrate > 0 ? count_integrate : 1)
                         << " seconds/test)." << endl;
#endif
#endif
  }
#endif
}

#define AMC_FILE_CLASS_PDF_CXX
#endif
