// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_MOVING_HXX

#define AMC_COEFFICIENT_REPARTITION_NUMBER_CONCENTRATION_MINIMUM_SQUARE 1.e-12

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationMoving
   */
  template<class R, class D>
  class ClassCoefficientRepartitionCoagulationMoving
    : public ClassCoefficientRepartitionCoagulationBase,
      protected ClassCoefficientRepartitionCoagulationSize,
      protected ClassRedistributionClass<R, D>
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  private:

    /*!< Maximum number of coefficients per couple.*/
    int Ncoefficient_max_;

    /*!< Maximum number of discretization class compositions (not trace ones).*/
    int Nclass_max_discretization_;

    /*!< Maximum number of redistribution class compositions (not trace ones).*/
    int Nclass_max_redistribution_;

    /*!< Working arrays.*/
    int Nclass_work_;
    vector1i index_class_work_;
    vector1i index_internal_mixing_work_;
    vector2r flux_mass_class_work_;
    vector1r x_work_;
    vector1r rate_aer_mass_total_work_;

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
    void check_mean_mass_section(const int k, const real &flux_number_section, const real &flux_mass_section) const;
#endif

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationMoving(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationMoving();

    /*!< Type.*/
    static int Type();

    /*!< Compute coefficients.*/
    void ComputeCoefficient(const real* concentration_number,
                            const real* concentration_mass);

#ifndef SWIG
    /*!< Compute coagulation gain.*/
#ifdef AMC_WITH_LAYER
    void ComputeGain(const int section_min,
                     const int section_max,
                     const real* concentration_number,
                     const real* concentration_mass,
                     const Array<real, 2> &kernel,
                     vector1r &rate_aer_number,
                     vector1r &rate_aer_mass,
                     Array<real, 2> &layer_repartition_rate);
#else
    void ComputeGain(const int section_min,
                     const int section_max,
                     const real* concentration_number,
                     const real* concentration_mass,
                     const Array<real, 2> &kernel,
                     vector1r &rate_aer_number,
                     vector1r &rate_aer_mass);
#endif
#endif
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_MOVING_HXX
#endif
