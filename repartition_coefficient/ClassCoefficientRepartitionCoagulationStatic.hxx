// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_STATIC_HXX

#define AMC_COEFFICIENT_REPARTITION_PRECISION_EPSILON 1.e-6
#define AMC_COEFFICIENT_REPARTITION_PRECISION_FREQUENCY 100
#define AMC_COEFFICIENT_REPARTITION_MONTE_CARLO_NUMBER_MIN 1e4
#define AMC_COEFFICIENT_REPARTITION_MONTE_CARLO_NUMBER_MAX 1e8
#define AMC_COEFFICIENT_REPARTITION_EDGE_PROBABILITY 0.1
#define AMC_COEFFICIENT_REPARTITION_EDGE_RATE 1000

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationStatic
   */
  template<class D>
  class ClassCoefficientRepartitionCoagulationStatic
    : public ClassCoefficientRepartitionCoagulationBase,
      protected ClassDiscretizationClass<D>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  private:

    /*!< MPI variables.*/
    int rank_, Nrank_;

    /*!< Edge probability and rate.*/
    real edge_probability_, edge_rate_;

    /*!< Number of coagulation couples per rank.*/
    vector1i Ncouple_local_;

    /*!< Offset of coagulation couples per rank.*/
    vector1i Ncouple_offset_;

    /*!< Set couples per rank.*/
    void set_couple_per_rank();

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationStatic(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationStatic();

    /*!< Type.*/
    static int Type();

    /*!< Compute repartition coefficients.*/
    real ComputeCoefficient(const real &precision,
                            int monte_carlo_number_min = AMC_COEFFICIENT_REPARTITION_MONTE_CARLO_NUMBER_MIN,
                            int monte_carlo_number_max = AMC_COEFFICIENT_REPARTITION_MONTE_CARLO_NUMBER_MAX,
                            int precision_frequency = AMC_COEFFICIENT_REPARTITION_PRECISION_FREQUENCY);

#ifdef AMC_WITH_MPI
    // Gather coefficients.
    void MPI_GatherCoefficient(int recv_rank = 0);
#endif

#ifndef SWIG 
    /*!< Compute coagulation gain.*/
#ifdef AMC_WITH_LAYER
    void ComputeGain(const int section_min,
                     const int section_max,
                     const real* concentration_number,
                     const real* concentration_mass,
                     const Array<real, 2> &kernel,
                     vector1r &rate_aer_number,
                     vector1r &rate_aer_mass,
                     Array<real, 2> &layer_repartition_rate) const;
#else
    void ComputeGain(const int section_min,
                     const int section_max,
                     const real* concentration_number,
                     const real* concentration_mass,
                     const Array<real, 2> &kernel,
                     vector1r &rate_aer_number,
                     vector1r &rate_aer_mass) const;
#endif
#endif
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_STATIC_HXX
#endif
