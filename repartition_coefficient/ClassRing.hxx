// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_RING_HXX

#define RING_INDEX_MIN_MIN 0
#define RING_INDEX_MIN_MAX 1
#define RING_INDEX_MAX_MIN 2
#define RING_INDEX_MAX_MAX 3

#define RING_CASE_0_PI    0
#define RING_CASE_0_MAX   1
#define RING_CASE_MIN_PI  2
#define RING_CASE_MIN_MAX 3

#define RING_PI 3.1415926535897932

#define RING_NUMBER_CASE 43

namespace AMC
{
  /*! 
   * \class ClassRing
   */
  class ClassRing
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;

  protected:

    /*!< Minimal and maximal radius.*/
    real radius_min_, radius_max_;

#ifdef AMC_WITH_TEST
    /*!< Counter of case occurence.*/
    static vector<int> case_counter_test_;
#endif

    /*!< Check radius.*/
    void check_radius() const;

    /*!< Compute integral in various cases.*/
    real compute_integral_0_pi(const real &distance, const real &radius0, const real &radius1) const;
    real compute_integral_0_max(const real &distance, const real &radius0, const real &radius1) const;
    real compute_integral_min_pi(const real &distance, const real &radius0, const real &radius1) const;
    real compute_integral_min_max(const real &distance, const real &radius0, const real &radius1) const;

    /*!< Compute cross angle.*/
    real compute_cross_angle(const real &distance, const real &radius_prime, const real &radius) const;

    /*!< Compute distance from angle.*/
    void compute_cross_distance(const real &distance, const ClassRing &ring,
                                const vector1r &cross_angle, vector1r &cross_distance) const;

    /*!< Compute crossing case depending on ring positions.*/
    int compute_cross_case(const real &distance, const ClassRing &ring,
                           const vector1r &cross_distance, const vector1r &cross_angle,
                           vector1i &integral_case, vector1r &integral_radius) const;

  public:

    /*!< Constructors.*/
    ClassRing();
    ClassRing(const real &radius_min, const real &radius_max);

    /*!< Get methods.*/
    real GetRadiusMin() const;
    real GetRadiusMax() const;

    /*!< Set methods.*/
    void SetRadiusMin(const real &radius_min);
    void SetRadiusMax(const real &radius_max);

    /*!< Destructor.*/
    ~ClassRing();

    /*!< Compute ring volume.*/
    real ComputeSelfVolume() const;

    /*!< Compute cross volume angles with another ring.*/
    void ComputeCrossAngle(const real &distance, const ClassRing &ring,
                           vector<real> &cross_angle, const bool in_degree = false) const;

    /*!< Compute cross volume distances with another ring.*/
    void ComputeCrossDistance(const real &distance, const ClassRing &ring, vector<real> &cross_distance) const;

    /*!< Compute cross case.*/
    int ComputeCrossCase(const real &distance, const ClassRing &ring,
                         vector<int> &integral_case, vector<real> &integral_radius) const;

    /*!< Compute cross volume with another ring.*/
    real ComputeCrossVolume(const real &distance, const ClassRing &ring) const;

    /*!< Ring string.*/
    string Str() const;

#ifdef AMC_WITH_TEST
    /*!< Test.*/
    static void Test(const ClassRing &b, const int N = 500, const real epsilon = real(1.e-10));

    /*!< Get case counter test.*/
    static void GetCaseCounterTest(vector<int> &case_counter_test);

    /*!< Reset case counter test.*/
    static void ResetCaseCounterTest();
#endif
  };
}

#define AMC_FILE_CLASS_RING_HXX
#endif
