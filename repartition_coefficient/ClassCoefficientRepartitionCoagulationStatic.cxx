// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_STATIC_CXX

#include "ClassCoefficientRepartitionCoagulationStatic.hxx"

namespace AMC
{
  // Set couples per rank.
  template<class D>
  void ClassCoefficientRepartitionCoagulationStatic<D>::set_couple_per_rank()
  {
    int Ncouple_local = this->Ncouple_ / Nrank_;
    int Ncouple_remaining = this->Ncouple_ % Nrank_;

    Ncouple_local_.assign(Nrank_, Ncouple_local);
    for (int irank = 0; irank < Nrank_; irank++)
      if (Ncouple_remaining > 0)
        if (irank < Ncouple_remaining)
          Ncouple_local_[irank]++;

    Ncouple_offset_.resize(Nrank_);
    for (int irank = 0; irank < Nrank_; irank++)
      {
        Ncouple_offset_[irank] = Ncouple_local * irank;

        for (int i = 0; i < irank; i++)
          if (i < Ncouple_remaining)
            Ncouple_offset_[irank]++;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset()
                         << "Rank " << rank_ << "/" << Nrank_
                         << " computes " << Ncouple_local_[rank_] << " couples from "
                         << Ncouple_offset_[rank_] << " to "
                         << Ncouple_local_[rank_] + Ncouple_offset_[rank_] << "." << endl;
#endif
  }


  // Constructors.
  template<class D>
  ClassCoefficientRepartitionCoagulationStatic<D>::ClassCoefficientRepartitionCoagulationStatic(Ops::Ops &ops)
    : ClassCoefficientRepartitionCoagulationBase(ops), rank_(0), Nrank_(1),
      edge_probability_(AMC_COEFFICIENT_REPARTITION_EDGE_PROBABILITY),
      edge_rate_(AMC_COEFFICIENT_REPARTITION_EDGE_RATE)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset()
                         << "Instantiate static repartition coefficients." << endl;
#endif

    // Probability that composition is on edge.
    edge_probability_ = ops.Get<real>("edge.probability", "", edge_probability_);
    edge_rate_ = ops.Get<real>("edge.rate", "", edge_rate_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(2) << Reset() << "Compositions will be generated randomly on edges"
                         << " with a probability from 1 to " << edge_probability_ << "." << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Edge probability decreasing rate = " << edge_rate_ << endl;
#endif

    // Number of couples to compute depending on parallel configuration.
#ifdef AMC_WITH_MPI
    if (! MPI::Is_initialized())
      throw AMC::Error("MPI subsystem not yet initialized.");

    rank_ = MPI::COMM_WORLD.Get_rank();
    Nrank_ = MPI::COMM_WORLD.Get_size();
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug() << Reset() << "rank = " << rank_ << "/" << Nrank_ << endl;
#endif
    set_couple_per_rank();

    return;
  }


  // Destructor.
  template<class D>
  ClassCoefficientRepartitionCoagulationStatic<D>::~ClassCoefficientRepartitionCoagulationStatic()
  {
    return;
  }


  // Type.
  template<class D>
  int ClassCoefficientRepartitionCoagulationStatic<D>::Type()
  {
    return AMC_COEFFICIENT_REPARTITION_TYPE_STATIC;
  }


  // Compute repartition coefficients.
  template<class D>
  real ClassCoefficientRepartitionCoagulationStatic<D>::ComputeCoefficient(const real &precision,
                                                                           int monte_carlo_number_min,
                                                                           int monte_carlo_number_max,
                                                                           int precision_frequency)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Compute static coagulation "
                         << "repartition coefficients with Monte Carlo number min/max = "
                         << monte_carlo_number_min << "/" << monte_carlo_number_max
                         << " and required precision = " << precision << " (0 means not used)." << endl;
#endif

    // Number of couples to compute depending on parallel configuration.
    const int Ncouple_local = Ncouple_local_[rank_];
    const int Ncouple_offset = Ncouple_offset_[rank_];

#ifdef AMC_WITH_MPI
    MPI::COMM_WORLD.Barrier();
    usleep(100);
#endif

#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    int time_index = AMCTimer<CPU>::Add("compute_coagulation_repartition_coefficient_static_" + to_str(rank_));
    AMCTimer<CPU>::Start();
#endif

    // Resize repartition coefficients with default size.
    this->repartition_coefficient_index_ =
      vector2i(Ncouple_local, vector1i(ClassAerosolData::Ng_, 0));
    this->repartition_coefficient_number_ =
      vector2r(Ncouple_local, vector1r(ClassAerosolData::Ng_, 0));
    this->Ncoefficient_ = vector1i(Ncouple_local, 0);
    int Ncoefficient_total(0);

    vector2i bound_size_index(4, vector1i(2));
    for (int i = 0; i < 2; i++)
      for (int j = 0; j < 2; j++)
        {
          int k = 2 * i + j;
          bound_size_index[k][0] = i;
          bound_size_index[k][1] = j;
        }

    //
    // Begin Monte Carlo loop.
    //

    // Global minimal precision.
    real precision_min_global(real(0));

    int section_size_index_last = ClassDiscretizationSize::Nsection_ - 1;
    bool first_time(true);

    for (int i = 0; i < Ncouple_local; i++)
      {
        // General section couple indexes.
        const int g1 = int((-1 + sqrt(1 + 8 * (Ncouple_offset + i))) / 2);
        const int g2 = Ncouple_offset + i - g1 * (g1 + 1) / 2;

        // Size section and composition class indexes corresponding to the general section.
        const int b1 = ClassAerosolData::general_section_[g1][0];
        const int b2 = ClassAerosolData::general_section_[g2][0];

        const int c1 = ClassAerosolData::general_section_[g1][1];
        const int c2 = ClassAerosolData::general_section_[g2][1];

        // Reference to repartition coefficients.
        vector1i &repartition_coefficient_index = this->repartition_coefficient_index_[i];
        vector1r &repartition_coefficient_number = this->repartition_coefficient_number_[i];

        // Store data for precision computation.
        real precision_min_local(real(0));
        vector1r repartition_coefficient_number_old(ClassAerosolData::Ng_, real(0));

        const ClassDiscretizationComposition<D> *discretization_composition1 =
          ClassDiscretizationClass<D>::discretization_composition_[b1];
        const ClassDiscretizationComposition<D> *discretization_composition2 =
          ClassDiscretizationClass<D>::discretization_composition_[b2];

        // Actually, the Monte Carlo loop.
        bool end_bound_loop(false);
        int bound_index1(0), bound_index2(0);
        int u;

        for (u = 0; u < monte_carlo_number_max; u++)
          {
            real mass1, mass2;

            // Avoid to get only one coefficient because random algorithm
            // does not go to the extreme, this does not hold for last size section.
            // This is somewhat a workaround with respect to a more thorough
            // random sampling implementation (e.g. latin hypercube).
            if (end_bound_loop)
              {
                mass1 = ClassDiscretizationSize::mass_bound_[b1] + drand48() * ClassDiscretizationSize::mass_width_[b1];
                mass2 = ClassDiscretizationSize::mass_bound_[b2] + drand48() * ClassDiscretizationSize::mass_width_[b2];
              }
            else
              {
                mass1 = ClassDiscretizationSize::mass_bound_[b1 + bound_index1];
                mass2 = ClassDiscretizationSize::mass_bound_[b2 + bound_index2];

                bound_index2++;
                if (bound_index2 == 2)
                  {
                    bound_index1++;
                    bound_index2 = 0;
                  }

                if (bound_index1 == 2)
                  end_bound_loop = true;
              }

            // Total mass of coagulated particle.
            real mass3 = mass1 + mass2;

            // Search size section index, it is at least greater
            // or equal than the maximum between b1 and b2.
            int b3 = search_index(ClassDiscretizationSize::mass_bound_, mass3, b1);

            // If outside of size discretization, keep it inside.
            b3 = (b3 >= ClassDiscretizationSize::Nsection_) ? section_size_index_last : b3;

            // Class composition of particle generated by coagulation.
            int c3(0);

#ifdef AMC_WITH_TRACE
            c3 = ClassCoefficientRepartitionCoagulationTrace::repartition_coefficient_index_[i];

            if (c3 >= 0)
              c3 = ClassTrace::size_class_index_[c3][b3];

            if (c3 < 0)
              {
#endif
                // If External mixing, get the class composition.
                if (! ClassDiscretizationClass<D>::discretization_composition_[b3]->IsInternalMixing())
                  {
                    // Generate random compositions for each class compositions.
                    vector1r composition1(ClassSpecies::Nspecies_, real(0)),
                      composition2(ClassSpecies::Nspecies_, real(0));

                    const real edge_probability = edge_probability_
                      + (real(1) - edge_probability_) * exp(- u / edge_rate_);
                    bool on_edge = drand48() < edge_probability_;

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
                    CBUG << "edge_probability = " << edge_probability << endl;
                    CBUG << "on_edge = " << (on_edge ? "yes" : "no") << endl;
#endif

                    discretization_composition1->GenerateRandomComposition(c1, composition1, on_edge);
                    discretization_composition2->GenerateRandomComposition(c2, composition2, on_edge);

                    // Mass fraction from coagulationg particles in new one.
                    const real f1 = mass1 / mass3;
                    const real f2 = mass2 / mass3;

                    // Species mass fractions.
                    vector1r composition3(ClassSpecies::Nspecies_);
                    for (int j = 0; j < ClassSpecies::Nspecies_; j++)
                      composition3[j] = composition1[j] * f1 + composition2[j] * f2;

                    on_edge = false;
                    c3 = ClassDiscretizationClass<D>::discretization_composition_[b3]->
                      FindClassCompositionIndex(composition3, on_edge);

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
                    cerr << "u: " << u << endl
                         << "b1: " << b1 << ", b2: " << b2 << endl 
                         << "f1: " << f1 << ", f2: " << f2 << endl
                         << "c1: " << c1 << ", c2: " << c2 << endl 
                         << "composition1: " << composition1 << endl 
                         << "composition2: " << composition2 << endl 
                         << "b3: " << b3 << endl 
                         << "c3: " << c3 << endl 
                         << "composition3: " << composition3 << endl;
                    getchar();
#endif
                  }
                else // If internal mixing.
                  c3 = 0;
#ifdef AMC_WITH_TRACE
              }
#endif

            // General section where coagulated particles fall.
            const int g3 = ClassAerosolData::general_section_reverse_[b3][c3];

            // Get general sections index.
            repartition_coefficient_index[g3]++;

            // Evaluate relative error.
            if (u > monte_carlo_number_min)
              if (u % precision_frequency == 0)
                {
                  real u_inv = real(1) / real(u + 1);
                  for (int j = 0; j < ClassAerosolData::Ng_; j++)
                    repartition_coefficient_number[j] = real(repartition_coefficient_index[j]) * u_inv;

                  if (! first_time)
                    {
                      precision_min_local = real(0);

                      int count(0);
                      for (int j = 0; j < ClassAerosolData::Ng_; j++)
                        if (repartition_coefficient_number_old[j] > real(0))
                          {
                            real precision_local = abs(repartition_coefficient_number[j] - repartition_coefficient_number_old[j])
                              / (repartition_coefficient_number_old[j] + AMC_COEFFICIENT_REPARTITION_PRECISION_EPSILON);
                            if (precision_min_local < precision_local)
                              precision_min_local = precision_local;
                            count++;
                          }

                      if (count > 1 || b3 == section_size_index_last)
                        if (precision_min_local < precision)
                          break;
                    }

                  for (int j = 0; j < ClassAerosolData::Ng_; j++)
                    repartition_coefficient_number_old[j] = repartition_coefficient_number[j];

                  first_time = false;
                }
          }

        // If Monte Carlo loop was broken before end, increment counter
        // as we broke before it to be updated.
        if (u < monte_carlo_number_max)
          u++;

        // Save minimal precision across couples.
        if (precision_min_global < precision_min_local)
          precision_min_global = precision_min_local;

#ifdef AMC_WITH_LOGGER
        if (precision_min_local < precision)
          *AMCLogger::GetLog() << Fgreen() << Info() << Reset()
                               << "For couple [" << setw(3) << right << g1 << "," << setw(3) << right << g2
                               << "] (i = " << left << setw(6) << i << ") reached minimal precision "
                               << setw(6) << setprecision(6) << scientific << precision_min_local
                               << " at step number " << setw(8) << right << u << endl;
        else
          *AMCLogger::GetLog() << Fred() << Info() << Reset()
                               << "For couple [" << setw(3) << right << g1 << "," << setw(3) << right << g2
                               << "] (i = " << left << setw(6) << i << ") could not reach required precision"
                               << ", current one is " << setw(6) << setprecision(6)
                               << scientific << precision_min_local << endl;
#endif
        // Scale and reorder repartition coefficients.
        real u_inv = real(1) / real(u);
        for (int j = 0; j < ClassAerosolData::Ng_; j++)
          if (repartition_coefficient_index[j] > 0)
            {
              repartition_coefficient_number[this->Ncoefficient_[i]] =
                real(repartition_coefficient_index[j]) * u_inv;
              repartition_coefficient_index[this->Ncoefficient_[i]++] = j;
            }

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "Ncoefficient = " << this->Ncoefficient_[i] << endl;
        if (this->Ncoefficient_[i] == 1)
          *AMCLogger::GetLog() << Fred() << Warning() << Reset()
                               << "Only one coefficient for couple [" << setw(3) << right << g1 << "," << setw(3)
                               << right << g2 << "] (i = " << left << setw(6) << i << ")" << endl;   
#endif
        repartition_coefficient_index.resize(this->Ncoefficient_[i]);
        repartition_coefficient_number.resize(this->Ncoefficient_[i]);

        Ncoefficient_total += this->Ncoefficient_[i];

#if __cplusplus > 199711L
        repartition_coefficient_index.shrink_to_fit();
        repartition_coefficient_number.shrink_to_fit();
#endif

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(3) << Reset() << "coefficients = " << repartition_coefficient_number << endl;
#endif

        // Check if sum property is satisfied.
        if (abs(compute_vector_sum(repartition_coefficient_number) - real(1)) > AMC_COEFFICIENT_REPARTITION_FRACTION_THRESHOLD_SUM)
          throw AMC::Error("Coefficients sum is not equal to unity for couple [" + to_str(g1) + "  " + to_str(g2) + "].");
      }

#ifdef AMC_WITH_MPI
    MPI::COMM_WORLD.Barrier();
    usleep(100);
#endif

    // Mass static repartition coefficient are not used, hence set to zero.
    // In case of parallel computation, this is only useful for master.
    this->repartition_coefficient_mass_.resize(this->Ncouple_);
    for (int i = 0; i < this->Ncouple_; ++i)
      this->repartition_coefficient_mass_[i].assign(this->Ncoefficient_[i], vector1r(ClassSpecies::Nspecies_, real(0)));


    //
    // End Monte Carlo loop.
    //


#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    AMCTimer<CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(3) << Reset() << "Computed " << Ncoefficient_total
                         << " coagulation coefficients in " << AMCTimer<CPU>::GetTimer(time_index) << " seconds"
                         << " with minimal precision " << setw(6) << setprecision(6)
                         << scientific << precision_min_global << " ." << endl;
#endif
#endif

    // Say that coefficients are computed.
    this->is_computed_ = true;

    // In the end, return the minimal precision reached and let user retry if not satisfied.
    return precision_min_global;
  }


#ifdef AMC_WITH_MPI
  // Gather coefficients.
  template<class D>
  void ClassCoefficientRepartitionCoagulationStatic<D>::MPI_GatherCoefficient(int recv_rank)
  {
    if (! this->is_computed_)
      throw AMC::Error("Repartition coefficients not computed yet.");

    // If only one rank, just return.
    if (Nrank_ == 1)
      return;

    const int Ncouple_local = Ncouple_local_[rank_];
    const int Ncouple_offset = Ncouple_offset_[rank_];

    if (rank_ == recv_rank)
      {
        // Resize coefficients arrays.
        this->repartition_coefficient_index_.resize(this->Ncouple_);
        this->repartition_coefficient_number_.resize(this->Ncouple_);
        this->Ncoefficient_.resize(this->Ncouple_);

        // Put coefficient of receive rank at the right place.
        for (int i = 0; i < Ncouple_local; i++)
          {
            int j = Ncouple_offset + i;
            this->Ncoefficient_[j] = this->Ncoefficient_[i];
            this->repartition_coefficient_index_[j] = this->repartition_coefficient_index_[i];
            this->repartition_coefficient_number_[j] = this->repartition_coefficient_number_[i];
          }

        // Receive and place coefficients of other ranks.
        for (int irank = 0; irank < Nrank_; irank++)
          if (irank != recv_rank)
            {
              int itag(0);
              MPI::COMM_WORLD.Recv(this->Ncoefficient_.data() + Ncouple_offset_[irank],
                                   Ncouple_local_[irank], MPI::INT, irank, itag++);
#ifdef AMC_WITH_LOGGER
              *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "Receive " << Ncouple_local_[irank]
                                   << " couples from worker " << irank << " ." << endl;
#endif
              for (int i = 0; i < Ncouple_local_[irank]; i++)
                {
                  int j = Ncouple_offset_[irank] + i;
                  this->repartition_coefficient_index_[j].resize(this->Ncoefficient_[j]);
                  repartition_coefficient_number_[j].resize(this->Ncoefficient_[j]);
                  MPI::COMM_WORLD.Recv(this->repartition_coefficient_index_[j].data(),
                                       this->Ncoefficient_[j], MPI::INT, irank, itag++);
                  MPI::COMM_WORLD.Recv(this->repartition_coefficient_number_[j].data(),
                                       this->Ncoefficient_[j], AMC_MPI_REAL, irank, itag++);
                }
            }
      }
    else
      {
        int itag(0);
        MPI::COMM_WORLD.Send(this->Ncoefficient_.data(), Ncouple_local, MPI::INT, 0, itag++);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Send " << Ncouple_local
                             << " couples to worker " << recv_rank << " ." << endl;
#endif

        for (int i = 0; i < Ncouple_local; i++)
          {
            MPI::COMM_WORLD.Send(this->repartition_coefficient_index_[i].data(),
                                 this->Ncoefficient_[i], MPI::INT, recv_rank, itag++);
            MPI::COMM_WORLD.Send(this->repartition_coefficient_number_[i].data(),
                                 this->Ncoefficient_[i], AMC_MPI_REAL, recv_rank, itag++);
          }
      }
  }
#endif




  // Compute coagulation gain.
  template<class D>
#ifdef AMC_WITH_LAYER
    void ClassCoefficientRepartitionCoagulationStatic<D>::ComputeGain(const int section_min,
                                                                      const int section_max,
                                                                      const real* concentration_number,
                                                                      const real* concentration_mass,
                                                                      const Array<real, 2> &kernel,
                                                                      vector1r &rate_aer_number,
                                                                      vector1r &rate_aer_mass,
                                                                      Array<real, 2> &layer_repartition_rate) const
#else
    void ClassCoefficientRepartitionCoagulationStatic<D>::ComputeGain(const int section_min,
                                                                      const int section_max,
                                                                      const real* concentration_number,
                                                                      const real* concentration_mass,
                                                                      const Array<real, 2> &kernel,
                                                                      vector1r &rate_aer_number,
                                                                      vector1r &rate_aer_mass) const
#endif
  {
    // Declare once for all.
    vector1r flux_mass(ClassSpecies::Nspecies_, real(0));

    for (int g1 = 0; g1 < ClassAerosolData::Ng_; ++g1)
      for (int g2 = 0; g2 <= g1; ++g2)
        {
          const int i = g1 * (g1 + 1) / 2 + g2;

          // Reference to repartition coefficients.

          // Number flux.
          real flux_number = kernel(g1, g2) * concentration_number[g1] * concentration_number[g2];

          // Mass flux.
          int k1 = g1 * ClassSpecies::Nspecies_ - 1;
          int k2 = g2 * ClassSpecies::Nspecies_ - 1;
          for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
            flux_mass[j] = kernel(g1, g2) * (concentration_number[g2] * concentration_mass[++k1] +
                                             concentration_number[g1] * concentration_mass[++k2]);

          if (g1 == g2)
            {
              flux_number *= real(0.5);
              for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
                flux_mass[j] *= real(0.5);
            }

          for (int j = 0; j < this->Ncoefficient_[i]; ++j)
            {
              const int g3 = this->repartition_coefficient_index_[i][j];

              // Avoid sections we do not want to compute.
              if (g3 < section_min || g3 >= section_max)
                continue;

              rate_aer_number[g3] += this->repartition_coefficient_number_[i][j] * flux_number;

              int k = g3 * ClassSpecies::Nspecies_ - 1;
              for (int l = 0; l < ClassSpecies::Nspecies_; ++l)
                rate_aer_mass[++k] += repartition_coefficient_number_[i][j] * flux_mass[l];
            }
        }
  }
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_STATIC_CXX
#endif
