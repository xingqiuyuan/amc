// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_MOVING_CXX

#include "ClassCoefficientRepartitionCoagulationMoving.hxx"

namespace AMC
{
#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
  template<class R, class D>
  void ClassCoefficientRepartitionCoagulationMoving<R, D>::
  check_mean_mass_section(const int k, const real &flux_number_section, const real &flux_mass_section) const
  {
    const real mass_mean_section = flux_mass_section / flux_number_section;
    if (k < ClassDiscretizationSize::Nsection_ - 1)
      if (mass_mean_section < ClassDiscretizationSize::mass_bound_[k] ||
          mass_mean_section > ClassDiscretizationSize::mass_bound_[k + 1])
        throw AMC::Error("Average mass (" + to_str(mass_mean_section) +
                         ") for coagulation flux in section " + to_str(k) + " is outside of section bounds : " +
                         to_str(mass_mean_section / ClassDiscretizationSize::mass_bound_[k]) + ", " +
                         to_str(mass_mean_section / ClassDiscretizationSize::mass_bound_[k + 1]));
  }
#endif


  // Constructors.
  template<class R, class D>
  ClassCoefficientRepartitionCoagulationMoving<R, D>::ClassCoefficientRepartitionCoagulationMoving(Ops::Ops &ops)
    : ClassCoefficientRepartitionCoagulationBase(ops), ClassCoefficientRepartitionCoagulationSize(),
      Ncoefficient_max_(0), Nclass_max_discretization_(0), Nclass_max_redistribution_(0)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Instantiate moving repartition coefficients." << endl;
#endif

    for (int h = 0; h < ClassCoefficientRepartitionCoagulationBase::Ncouple_; ++h)
      {
        int Ncoefficient(0);
        for (int i = 0; i < ClassCoefficientRepartitionCoagulationSize::Ncoefficient_[h]; ++i)
          Ncoefficient += ClassDiscretizationClass<D>::Nclass_[ClassCoefficientRepartitionCoagulationSize::
                                               repartition_coefficient_index_[h][i]];

        if (Ncoefficient > Ncoefficient_max_)
          Ncoefficient_max_ = Ncoefficient;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Ncoefficient_max = " << Ncoefficient_max_ << endl;
#endif

    // Maximum number of real classes, proxy from parent classes.
    Nclass_max_discretization_ = ClassDiscretizationClass<D>::Nclass_max_;
    Nclass_max_redistribution_ = ClassRedistributionClass<R, D>::Nclass_max_;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Nclass_max_discretization = "
                         << Nclass_max_discretization_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Nclass_max_redistribution = "
                         << Nclass_max_redistribution_ << endl;
#endif

    // Resize coefficients.
    ClassCoefficientRepartitionCoagulationBase::Ncoefficient_.
      assign(ClassCoefficientRepartitionCoagulationBase::Ncouple_, real(0));
    ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_index_.
      assign(ClassCoefficientRepartitionCoagulationBase::Ncouple_, vector1i(Ncoefficient_max_, 0));
    ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_number_.
      assign(ClassCoefficientRepartitionCoagulationBase::Ncouple_, vector1r(Ncoefficient_max_, real(0)));
    ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_mass_.
      assign(ClassCoefficientRepartitionCoagulationBase::Ncouple_,
             vector2r(Ncoefficient_max_, vector1r(ClassSpecies::Nspecies_, real(0))));

    // Working arrays.
    Nclass_work_ = 0;
    index_class_work_.assign(Nclass_max_discretization_, 0);
    index_internal_mixing_work_.assign(Nclass_max_discretization_, 0);
    flux_mass_class_work_.assign(Nclass_max_redistribution_, vector1r(ClassSpecies::Nspecies_ + 1, real(0)));
    x_work_.assign(ClassAerosolData::Ng_, real(0));
    rate_aer_mass_total_work_.assign(ClassAerosolData::Ng_, real(0));

    return;
  }


  // Destructor.
  template<class R, class D>
  ClassCoefficientRepartitionCoagulationMoving<R, D>::~ClassCoefficientRepartitionCoagulationMoving()
  {
    return;
  }


  // Type.
  template<class R, class D>
  int ClassCoefficientRepartitionCoagulationMoving<R, D>::Type()
  {
    return AMC_COEFFICIENT_REPARTITION_TYPE_MOVING;
  }


  // Compute coefficients.
  template<class R, class D>
  void ClassCoefficientRepartitionCoagulationMoving<R, D>::ComputeCoefficient(const real* concentration_number,
                                                                              const real* concentration_mass)
  {
    x_work_.assign(ClassAerosolData::Ng_, real(0));

    // Compute average equivalent volume for each general sections.
    int h(-1);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          x_work_[i] += concentration_mass[++h];

        const int j = ClassCoefficientRepartitionCoagulationSize::general_section_size_[i];

        if (x_work_[i] > real(0) && concentration_number[i] > real(0))
          {
            x_work_[i] /= (concentration_number[i] * ClassPDF::factor_);
            if (x_work_[i] < ClassPDF::x_[j] || x_work_[i] >= ClassPDF::x_[j + 1])
              x_work_[i] = ClassPDF::cx_[j];
          }
        else
          x_work_[i] = ClassPDF::cx_[j];
      }

    // Compute moving size repartition coefficients.
    ClassCoefficientRepartitionCoagulationSize::ComputeCoefficient(x_work_);

    // Loop on general sections.
    int i(0);

    for (int g1 = 0; g1 < ClassAerosolData::Ng_; ++g1)
      for (int g2 = 0; g2 <= g1; ++g2)
        {
#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
          CBUG << "[g1, g2] = [" << g1 << ", " << g2 << "]" << endl;
          CBUG << "i = " << i << endl;
#endif

          // Coagulation number flux between the two general sections.
          const real total_number = concentration_number[g1] * concentration_number[g2];

          // Composition of coagulated particles.
          vector1r composition3(ClassSpecies::Nspecies_, real(0));
          real total_mass(real(0));

          int k1 = g1 * ClassSpecies::Nspecies_ - 1;
          int k2 = g2 * ClassSpecies::Nspecies_ - 1;
          for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
            {
              composition3[j] = concentration_number[g2] * concentration_mass[++k1]
                + concentration_number[g1] * concentration_mass[++k2];
              total_mass += composition3[j];
            }

          // Reference to repartition coefficients.
          int &Ncoefficient = ClassCoefficientRepartitionCoagulationBase::Ncoefficient_[i];
          vector1i &repartition_coefficient_index =
            ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_index_[i];
          vector1r &repartition_coefficient_number =
            ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_number_[i];
          vector2r &repartition_coefficient_mass =
            ClassCoefficientRepartitionCoagulationBase::repartition_coefficient_mass_[i];

          Ncoefficient = 0;
          repartition_coefficient_index.assign(Ncoefficient_max_, 0);
          repartition_coefficient_number.assign(Ncoefficient_max_, real(0));
          repartition_coefficient_mass.assign(Ncoefficient_max_, vector1r(ClassSpecies::Nspecies_, real(0)));

          if (total_number > AMC_COEFFICIENT_REPARTITION_NUMBER_CONCENTRATION_MINIMUM_SQUARE)
            if (total_mass > real(0))
              {
                // Compute current coagulated particle composition.
                for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
                  composition3[j] /= total_mass;

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
                if (abs(compute_vector_sum(composition3) - real(1)) >
                    AMC_COEFFICIENT_REPARTITION_FRACTION_THRESHOLD_SUM)
                  throw AMC::Error("Sum of fraction composition is not unity : " + to_str(composition3));
#endif

#ifdef AMC_WITH_LAYER
                ClassCoefficientRepartitionCoagulationLayer::
                  compute_layer_repartition_couple(g1, g2, i,
                                                   concentration_number,
                                                   concentration_mass);
#endif

                // For each size section over which the coagulated particles fall,
                // redistribute number and mass on corresponding class compositions.
                for (int j = 0; j < ClassCoefficientRepartitionCoagulationSize::Ncoefficient_[i]; ++j)
                  {
                    // Index of size section.
                    const int k = ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_index_[i][j];

                    // Put composition.
                    for (int l = 0; l < ClassSpecies::Nspecies_; ++l)
                      flux_mass_class_work_[0][l] = composition3[l];

                    // Put total concentration at end of vector.
                    flux_mass_class_work_[0].back() = real(1);

                    // Number of classes over which we will redistribute.
                    Nclass_work_ = 0;

                    // Redistribute over class composition for this size section.
#ifdef AMC_WITH_TRACE
                    int trace_index = ClassCoefficientRepartitionCoagulationTrace::repartition_coefficient_index_[i];

                    if (trace_index >= 0)
                      trace_index = ClassTrace::size_class_index_[trace_index][k];

                    if (trace_index < 0)
                      ClassRedistributionClass<R, D>::redistribution_composition_[k]
                        ->Redistribute(Nclass_work_, index_internal_mixing_work_, index_class_work_, flux_mass_class_work_);
                    else
                      {
                        Nclass_work_ = 1;
                        index_class_work_[0] = 0;
                        index_internal_mixing_work_[0] = trace_index;
                      }
#else
                    ClassRedistributionClass<R, D>::redistribution_composition_[k]
                      ->Redistribute(Nclass_work_, index_internal_mixing_work_, index_class_work_, flux_mass_class_work_);
#endif

                    // Give back to flux arrays.
                    for (int l = 0; l < Nclass_work_; ++l)
                      {
                        const int m = index_class_work_[l];

                        // General section index.
                        const int n = ClassAerosolData::general_section_reverse_[k][index_internal_mixing_work_[l]];

                        // Repartition coefficient index.
                        repartition_coefficient_index[Ncoefficient] = n;

                        // Number repartition coefficients.
                        repartition_coefficient_number[Ncoefficient] = flux_mass_class_work_[m].back()
                          * ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_number_[i][j];

                        // Total mass for repartition coefficients.
                        const real repartition_coefficient_mass_total = flux_mass_class_work_[m].back()
                          * ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_mass_[i][j];

                        for (int s = 0; s < ClassSpecies::Nspecies_; ++s)
                          if (composition3[s] > real(0))
                            repartition_coefficient_mass[Ncoefficient][s] =
                              repartition_coefficient_mass_total * flux_mass_class_work_[m][s] / composition3[s];

                        // Increment number of repartition coefficients for given couple.
                        Ncoefficient++;
                      }
                  }
              }

          // Increment index of couple.
          ++i;
        }
  }


  // Compute coagulation gain.
  template<class R, class D>
#ifdef AMC_WITH_LAYER
  void ClassCoefficientRepartitionCoagulationMoving<R, D>::ComputeGain(const int section_min,
                                                                       const int section_max,
                                                                       const real* concentration_number,
                                                                       const real* concentration_mass,
                                                                       const Array<real, 2> &kernel,
                                                                       vector1r &rate_aer_number,
                                                                       vector1r &rate_aer_mass,
                                                                       Array<real, 2> &layer_repartition_rate)
#else
  void ClassCoefficientRepartitionCoagulationMoving<R, D>::ComputeGain(const int section_min,
                                                                       const int section_max,
                                                                       const real* concentration_number,
                                                                       const real* concentration_mass,
                                                                       const Array<real, 2> &kernel,
                                                                       vector1r &rate_aer_number,
                                                                       vector1r &rate_aer_mass)
#endif
  {
    rate_aer_mass_total_work_.assign(ClassAerosolData::Ng_, real(0));
    x_work_.assign(ClassAerosolData::Ng_, real(0));

    // Compute average equivalent volume for each general sections.
    int h(-1);
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          x_work_[i] += concentration_mass[++h];

        const int j = ClassCoefficientRepartitionCoagulationSize::general_section_size_[i];

        if (x_work_[i] > real(0) && concentration_number[i] > real(0))
          {
            x_work_[i] /= (concentration_number[i] * ClassPDF::factor_);
            if (x_work_[i] < ClassPDF::x_[j] || x_work_[i] >= ClassPDF::x_[j + 1])
              x_work_[i] = ClassPDF::cx_[j];
          }
        else
          x_work_[i] = ClassPDF::cx_[j];
      }

    // Compute moving size repartition coefficients.
    ClassCoefficientRepartitionCoagulationSize::ComputeCoefficient(x_work_);

    // Loop on general sections.
    int i(0);

    for (int g1 = 0; g1 < ClassAerosolData::Ng_; ++g1)
      for (int g2 = 0; g2 <= g1; ++g2)
        {
#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
          CBUG << "[g1, g2] = [" << g1 << ", " << g2 << "]" << endl;
          CBUG << "i = " << i << endl;
#endif

          // Coagulation number flux between the two general sections.
          const real total_number = concentration_number[g1] * concentration_number[g2];
          real flux_number = total_number * kernel(g1, g2);

          // Composition of coagulated particles.
          vector1r composition3(ClassSpecies::Nspecies_, real(0));
          real flux_mass(real(0)), total_mass(real(0));

          int k1 = g1 * ClassSpecies::Nspecies_ - 1;
          int k2 = g2 * ClassSpecies::Nspecies_ - 1;
          for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
            {
              composition3[j] = concentration_number[g2] * concentration_mass[++k1]
                + concentration_number[g1] * concentration_mass[++k2];
              total_mass += composition3[j];
            }

          flux_mass = total_mass * kernel(g1, g2);

          if (total_number > AMC_COEFFICIENT_REPARTITION_NUMBER_CONCENTRATION_MINIMUM_SQUARE)
            if (flux_mass > real(0))
              {
                // Compute current coagulated particle composition.
                for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
                  composition3[j] /= total_mass;

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
                if (abs(compute_vector_sum(composition3) - real(1)) >
                    AMC_COEFFICIENT_REPARTITION_FRACTION_THRESHOLD_SUM)
                  throw AMC::Error("Sum of fraction composition is not unity : " + to_str(composition3));
#endif

#ifdef AMC_WITH_LAYER
                ClassCoefficientRepartitionCoagulationLayer::
                  compute_layer_repartition_couple(g1, g2, i,
                                                   concentration_number,
                                                   concentration_mass);
#endif

                // When same general section, need to half fluxes,
                // otherwise you do not get mass conservation.
                if (g1 == g2)
                  {
                    flux_number *= real(0.5);
                    flux_mass *= real(0.5);
                  }

                // For each size section over which the coagulated particles fall,
                // redistribute number and mass on corresponding class compositions.
                for (int j = 0; j < ClassCoefficientRepartitionCoagulationSize::Ncoefficient_[i]; ++j)
                  {
                    // Index of size section.
                    const int k = ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_index_[i][j];

                    // Number and mass flux in this section.
                    const real flux_number_section = flux_number *
                      ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_number_[i][j];
                    const real flux_mass_section = flux_mass *
                      ClassCoefficientRepartitionCoagulationSize::repartition_coefficient_mass_[i][j];

#ifdef AMC_WITH_DEBUG_REPARTITION_COEFFICIENT
                    check_mean_mass_section(k, flux_number_section, flux_mass_section);
#endif

                    // Put composition.
                    for (int l = 0; l < ClassSpecies::Nspecies_; ++l)
                      flux_mass_class_work_[0][l] = composition3[l];

                    // Put total concentration at end of vector.
                    flux_mass_class_work_[0].back() = real(1);

                    // Number of classes over which we will redistribute.
                    Nclass_work_ = 0;

                    // Redistribute over class composition for this size section.
#ifdef AMC_WITH_TRACE
                    int trace_index = ClassCoefficientRepartitionCoagulationTrace::repartition_coefficient_index_[i];

                    if (trace_index >= 0)
                      trace_index = ClassTrace::size_class_index_[trace_index][k];

                    if (trace_index < 0)
                      ClassRedistributionClass<R, D>::redistribution_composition_[k]
                        ->Redistribute(Nclass_work_, index_internal_mixing_work_, index_class_work_, flux_mass_class_work_);
                    else
                      {
                        Nclass_work_ = 1;
                        index_class_work_[0] = 0;
                        index_internal_mixing_work_[0] = trace_index;
                      }
#else
                    ClassRedistributionClass<R, D>::redistribution_composition_[k]
                      ->Redistribute(Nclass_work_, index_internal_mixing_work_, index_class_work_, flux_mass_class_work_);
#endif

                    // Give back to flux arrays.
                    for (int l = 0; l < Nclass_work_; ++l)
                      {
                        const int m = index_class_work_[l];

                        // General section index.
                        const int n = ClassAerosolData::general_section_reverse_[k][index_internal_mixing_work_[l]];

                        // Avoid sections we do not want to compute.
                        if (n < section_min || n >= section_max)
                          continue;

                        real flux_mass_total_section_class = flux_mass_class_work_[m].back();
                        const real flux_number_section_class = flux_number_section * flux_mass_total_section_class;
                        flux_mass_total_section_class *= flux_mass_section;

                        int r = n * ClassSpecies::Nspecies_;
#ifdef AMC_WITH_LAYER
                        ClassRedistributionLayer::Redistribute(g2, n, 0, r,
                                                               flux_number_section_class,
                                                               rate_aer_number[n],
                                                               flux_mass_total_section_class,
                                                               rate_aer_mass_total_work_[n],
                                                               flux_mass_class_work_[m].data(),
                                                               NULL, rate_aer_mass.data() + r,
                                                               layer_repartition_couple_work_,
                                                               layer_repartition_rate);
#endif
                        rate_aer_number[n] += flux_number_section_class;
                        rate_aer_mass_total_work_[n] += flux_mass_total_section_class;

                        --r;
                        for (int s = 0; s < ClassSpecies::Nspecies_; ++s)
                          rate_aer_mass[++r] += flux_mass_total_section_class * flux_mass_class_work_[m][s];
                      }
                  }
              }

          // Increment index of couple.
          ++i;
        }
  }
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_MOVING_CXX
#endif
