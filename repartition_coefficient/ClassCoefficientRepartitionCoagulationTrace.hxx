// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_TRACE_HXX

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationTrace
   */
  class ClassCoefficientRepartitionCoagulationTrace : protected ClassAerosolData
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Trace repartition coefficient index.*/
    vector1i repartition_coefficient_index_;

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationTrace(Ops::Ops &ops);

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationTrace();

    /*!< Get methods.*/
    void GetCoefficientIndexTrace(vector<int> &index) const;
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_TRACE_HXX
#endif
