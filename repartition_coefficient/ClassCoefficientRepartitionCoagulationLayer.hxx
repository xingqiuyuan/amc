// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_LAYER_HXX

#ifdef AMC_WITH_DEBUG_COEFFICIENT_REPARTITION_COAGULATION_LAYER
#define AMC_COEFFICIENT_REPARTITION_COAGULATION_LAYER_EPSILON 1.e-11
#endif

namespace AMC
{
  /*! 
   * \class ClassCoefficientRepartitionCoagulationLayer
   */
  class ClassCoefficientRepartitionCoagulationLayer : protected ClassAerosolData
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Whether to compute coefficients or not.*/
    vector<bool> compute_coefficient_;

    /*!< Layer repartition coefficients.*/
    vector<Array<real, 2> > repartition_coefficient_;

    /*!< Working array storing layer repartition for coagulation couple.*/
    vector<vector<real> > layer_repartition_couple_work_;

    /*!< Compute layer repartition of coagulation couple.*/
    void compute_layer_repartition_couple(const int a, const int b, const int c,
                                          const real *concentration_number,
                                          const real *concentration_mass);

  public:

    /*!< Constructors.*/
    ClassCoefficientRepartitionCoagulationLayer();

    /*!< Destructor.*/
    ~ClassCoefficientRepartitionCoagulationLayer();

    /*!< Compute methods.*/
    void ComputeCoefficientLayer();
    void ComputeLayerRepartitionCouple(const real *concentration_number,
                                       const real *concentration_mass,
                                       vector<vector<real> > &layer_repartition_couple);

    /*!< Get methods.*/
    void GetCoefficientLayer(vector<vector<real> > &coefficient) const;
  };
}

#define AMC_FILE_CLASS_COEFFICIENT_REPARTITION_COAGULATION_LAYER_HXX
#endif
