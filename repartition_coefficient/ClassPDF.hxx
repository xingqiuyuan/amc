// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PDF_HXX

#define AMC_PDF_GAUSS_LEGENDRE_ROOT 0.57735026918962584

namespace AMC
{
  /*! 
   * \class ClassPDF
   */
  class ClassPDF : protected ClassDiscretizationSize
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
    typedef typename AMC::vector1s vector1s;

  protected:

    /*!< Number of sections, proxy from ClassAMC.*/
    int s_;

    /*!< Maximum number of index of size sections.*/
    int nmax_;

    /*!< Number of index of size sections.*/
    vector1i n_;

    /*!< Factor to convert single particle mass into volume equivalent.*/
    real factor_;

    /*!< Volume equivalent discretization bounds.*/
    vector1r x_;

    /*!< Volume equivalent center of each section.*/
    vector1r cx_;

    /*!< Width of volume equivalent sections.*/
    vector1r dx_, dx_inv_;

    /*!< Normalized volume equivalent for each couple.*/
    vector2r y_;

    /*!< Width of volume equivalent for each couple.*/
    vector1r dy_, dy_inv_;

    /*!< Index of size sections.*/
    vector2i i_;

    /*!< Bounds of size sections.*/
    vector2r z_;

    /*!< Number of PDF shapes.*/
    static int Npdf_;

    /*!< Number of cases.*/
    static int Ncase_;

    /*!< Nuber of step points.*/
    static int Nstep_;

    /*!< Number of pdf for each case.*/
    static vector1i Npdf_case_;

    /*!< Number of steps for each case.*/
    static vector1i Nstep_case_;

    /*!< Name of PDF shapes.*/
    static vector1s pdf_;

    /*!< Name of PDF points.*/
    static vector1s step_;

    /*!< Name of each case.*/
    static vector1i case_;

    /*!< Vector of indexes of PDF shapes for each case.*/
    static vector2i case_pdf_;

    /*!< Vector of indexes of step points for each case.*/
    static vector2i case_step_;

    /*!< Compute pdf.*/
    int compute_pdf_(const int i, const real &xi, vector1r &step, vector1r &alpha, vector1r &beta, vector1r &gamma) const;

    int compute_pdf_(const int i, const int j, const real &xi, const real &xj,
                     vector1r &step, vector1r &alpha, vector1r &beta, vector1r &gamma) const;

    /*!< Integrate pdf.*/
    void integrate_pdf_(const int h, const int icase, const vector1r &step, const vector1r &alpha,
                        const vector1r &beta, const vector1r &gamma, vector1r &f, vector1r &g) const;

  public:

    /*!< Constructor.*/
    ClassPDF();

    /*!< Destructor.*/
    ~ClassPDF();

    /*!< Get methods.*/
    vector<real> GetX() const;
    vector<real> GetCX() const;
    static int GetNpdf();
    static int GetNcase();
    static int GetNstep();
    static vector<string> GetPdfName();
    static vector<int> GetCaseName();
    static vector<string> GetStepName();
    static int GetNpdfCase(const int i);
    static int GetNstepCase(const int i);
    static vector<int> GetCasePdfIndex(const int i);
    static vector<int> GetCaseStepIndex(const int i);

    /*!< Compute the PDF.*/
    int Compute(const int i, const int j, const real &fi, const real &fj, vector<real> &y, vector<real> &p,
                const bool test = false) const;

    /*!< Compute the PDF.*/
    void Integrate(const int i, const int j, const real &fi, const real &fj, vector<real> &f, vector<real> &g) const;

#ifdef AMC_WITH_TEST
    /*!< Test PDF class.*/
    void Test(const int N = 1000, const real tol = real(1.e-12)) const;
#endif
  };
}

#define AMC_FILE_CLASS_PDF_HXX
#endif
