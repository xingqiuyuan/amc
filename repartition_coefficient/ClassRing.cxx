// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_RING_CXX

#include "ClassRing.hxx"

#ifndef SWIG
#define COMPUTE_INTEGRAL(a, b, c) D * (-b * P + S * (c - real(2) * a * P + S * (b + a * S)))
#endif

namespace AMC
{
  // Check radius.
  inline void ClassRing::check_radius() const
  {
    if (radius_min_ > radius_max_)
      throw AMC::Error("Min radius of ring (" + to_str(radius_min_) +
                       ") is greater than its max radius (" + to_str(radius_max_) + ").");
  }


  // Compute integral in various cases.
  inline real ClassRing::compute_integral_0_pi(const real &distance, const real &radius0, const real &radius1) const
  {
    return real(1.333333333333333333) * (radius1 * radius1 * radius1 - radius0 * radius0 * radius0);
  }

  inline real ClassRing::compute_integral_0_max(const real &distance, const real &radius0, const real &radius1) const
  {
    const real S(radius0 + radius1), P(radius0 * radius1), D(radius1 - radius0),
      rd(distance * distance - radius_min_ * radius_min_);
    return COMPUTE_INTEGRAL(real(0.25), real(0.666666666666666666) * distance, real(0.5) * rd) / distance;
  }

  inline real ClassRing::compute_integral_min_pi(const real &distance, const real &radius0, const real &radius1) const
  {
    const real S(radius0 + radius1), P(radius0 * radius1), D(radius1 - radius0),
      rd(radius_max_ * radius_max_ - distance * distance);
    return COMPUTE_INTEGRAL(real(-0.25), real(0.666666666666666666) * distance, real(0.5) * rd) / distance;
  }

  inline real ClassRing::compute_integral_min_max(const real &distance, const real &radius0, const real &radius1) const
  {
    return (radius_max_ * radius_max_ - radius_min_ * radius_min_)
      * (radius1 * radius1 - radius0 * radius0) / (real(2) * distance);
  }


  // Compute cross angle.
  inline real ClassRing::compute_cross_angle(const real &distance, const real &radius, const real &radius_prime) const
  {
    if (radius > real(0))
      {
        const real cosinus = (radius_prime * radius_prime - radius * radius - distance * distance)
          / (real(2) * radius * distance);

        if (cosinus < real(-1))
          return real(RING_PI);
        else if (cosinus > real(1))
          return real(0);
        else
          return acos(cosinus);
      }
    else
      {
        if (distance > radius_prime)
          return real(RING_PI);
        else
          return real(0);
      }
  }


  // Compute distance from angle.
  inline void ClassRing::compute_cross_distance(const real &distance, const ClassRing &ring,
                                                const vector1r &cross_angle, vector1r &cross_distance) const
  {
    cross_distance.assign(4, real(0));

    // Distance from min bound argument ring to min bound current ring.
    if (cross_angle[RING_INDEX_MIN_MIN] == real(0))
      cross_distance[RING_INDEX_MIN_MIN] = radius_min_ - ring.radius_min_ - distance;
    else if (cross_angle[RING_INDEX_MIN_MIN] == real(RING_PI))
      {
        cross_distance[RING_INDEX_MIN_MIN] = distance - radius_min_ - ring.radius_min_;
        if (cross_distance[RING_INDEX_MIN_MIN] < real(0))
          cross_distance[RING_INDEX_MIN_MIN] = ring.radius_min_ - distance - radius_min_;
      }

    // Distance from min bound argument ring to max bound current ring.
    if (cross_angle[RING_INDEX_MIN_MAX] == real(0))
      cross_distance[RING_INDEX_MIN_MAX] = radius_max_ - ring.radius_min_ - distance;
    else if (cross_angle[RING_INDEX_MIN_MAX] == real(RING_PI))
      {
        cross_distance[RING_INDEX_MIN_MAX] = distance - radius_max_ - ring.radius_min_;      
        if (cross_distance[RING_INDEX_MIN_MAX] < real(0))
          cross_distance[RING_INDEX_MIN_MAX] = ring.radius_min_ - distance - radius_max_;
      }

    // Distance from max bound argument ring to min bound current ring.
    if (cross_angle[RING_INDEX_MAX_MIN] == real(0))
      cross_distance[RING_INDEX_MAX_MIN] = radius_min_ - ring.radius_max_ - distance;
    else if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI))
      {
        cross_distance[RING_INDEX_MAX_MIN] = distance - radius_min_ - ring.radius_max_;
        if (cross_distance[RING_INDEX_MAX_MIN] < real(0))
          cross_distance[RING_INDEX_MAX_MIN] = ring.radius_max_ - radius_min_ - distance;
      }

    // Distance from max bound argument ring to max bound current ring.
    if (cross_angle[RING_INDEX_MAX_MAX] == real(0))
      cross_distance[RING_INDEX_MAX_MAX] = radius_max_ - ring.radius_max_ - distance;
    else if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI))
      {
        cross_distance[RING_INDEX_MAX_MAX] = distance - radius_max_ - ring.radius_max_;      
        if (cross_distance[RING_INDEX_MAX_MAX] < real(0))
          cross_distance[RING_INDEX_MAX_MAX] = ring.radius_max_ - distance - radius_max_;
      }

    // Ensure positivity whenever roundoff errors occurs.
    for (int i = 0; i < 4; ++i)
      if (cross_distance[i] < real(0))
        cross_distance[i] = real(0);

#ifdef AMC_WITH_DEBUG_RING
    CBUG << "cross_distance = " << cross_distance << endl;
#endif
  }


  // Compute crossing case depending on ring positions.
  inline int ClassRing::compute_cross_case(const real &distance, const ClassRing &ring,
                                           const vector1r &cross_distance, const vector1r &cross_angle,
                                           vector1i &integral_case, vector1r &integral_radius) const
  {
    if (cross_angle[RING_INDEX_MIN_MIN] == real(0)) // MIN_MIN = 0; MIN_MAX = 0;
      {
        if (cross_angle[RING_INDEX_MAX_MIN] == real(0)) // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN = 0; MAX_MAX = 0;
          {
            integral_case = {};
            integral_radius = {};
            return 0;
          }
        else if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI)) // MIN_MIN = 0; MIN_MAX = 0;
          {
            if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN = PI;
              {
                integral_case = {RING_CASE_0_MAX, RING_CASE_0_PI};
                integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                   ring.radius_max_};
                return 1;
              }
            else if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI)) // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN = PI;
              {
                integral_case = {RING_CASE_0_PI};
                integral_radius = {radius_min_, radius_max_};
                return 2;
              }
            else // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI};
              {
                if (ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX] <
                    ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN])
                  {
                    integral_case = {RING_CASE_0_MAX, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                       ring.radius_max_};
                    return 3;
                  }
                else
                  {
                    integral_case = {RING_CASE_0_MAX, RING_CASE_0_PI, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_max_};
                    return 4;
                  }
              }
          }
        else // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN != {0, PI};
          {
            if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN != {0, PI};
              {
                integral_case = {RING_CASE_0_MAX};
                integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                   ring.radius_max_};
                return 5;
              }
            else // MIN_MIN = 0; MIN_MAX = 0; MAX_MIN != {0, PI}; MAX_MAX != {0, PI};
              {
                integral_case = {RING_CASE_0_MAX, RING_CASE_MIN_MAX};
                integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                   ring.radius_max_};
                return 6;
              }
          }
      }
    else if (cross_angle[RING_INDEX_MIN_MIN] == real(RING_PI))
      {
        if (ring.radius_min_ > distance) // MIN_MIN = PI;
          {
            if (cross_angle[RING_INDEX_MIN_MAX] == real(0)) // MIN_MIN = PI; MAX_MIN = PI;
              {
                if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI;
                  {
                    integral_case = {RING_CASE_0_PI};
                    integral_radius = {ring.radius_min_, ring.radius_max_};
                    return 7;
                  }
                else if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI))
                  { // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX = PI;
                    integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                    return 8;
                  }
                else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI};
                  {
                    integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_max_};
                    return 9;
                  }
              }
            else if (cross_angle[RING_INDEX_MIN_MAX] == real(RING_PI)) // MIN_MIN = PI; ring.radius_min_ > distance;
              {
                integral_case = {};
                integral_radius = {};
                return 10;
              }
            else // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI; ring.radius_min_ > distance;
              {
                if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI)) // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI;
                  {
                    integral_case = {RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                    return 11;
                  }
                else // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI; MAX_MAX != {0, PI};
                  {
                    integral_case = {RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_, ring.radius_max_};
                    return 12;
                  }
              }
          }
        else // MIN_MIN = PI; ring.radius_min <= distance;
          {
            if (cross_angle[RING_INDEX_MIN_MAX] == real(0)) // MIN_MIN = PI; ring.radius_min <= distance;
              {
                if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI)) // MIN_MIN = PI; MIN_MAX = 0;
                  {
                    if (ring.radius_max_ > distance) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI;
                      {
                        if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI;
                          {
                            integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX, RING_CASE_0_PI};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                               ring.radius_max_};
                            return 13;                       
                          }
                        else if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI)) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI;
                          {
                            if (cross_distance[RING_INDEX_MIN_MIN] > cross_distance[RING_INDEX_MIN_MAX])
                              {
                                integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                                integral_radius = {ring.radius_min_,
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                                return 14;
                              }
                            else
                              {
                                integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                                integral_radius = {ring.radius_min_,
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                                return 15;
                              }
                          }
                        else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI}; ring.radius_max > distance;
                          {
                            if (cross_distance[RING_INDEX_MIN_MIN] > cross_distance[RING_INDEX_MIN_MAX])
                              {
                                integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                                integral_radius = {ring.radius_min_,
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                                   ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                                   ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                                   ring.radius_max_};
                                return 16;
                              }
                            else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI}; ring.radius_max > distance;
                              {
                                if (ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX] <
                                    ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN])
                                  {
                                    integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                                    integral_radius = {ring.radius_min_,
                                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                                       ring.radius_max_};
                                    return 17;
                                  }
                                else
                                  {
                                    integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX, RING_CASE_0_PI, RING_CASE_MIN_PI};
                                    integral_radius = {ring.radius_min_,
                                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                                       ring.radius_max_};
                                    return 18;
                                  }
                              }
                          }
                      }
                    else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; ring.radius_max <= distance;
                      {
                        if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI;
                          {
                            integral_case = {RING_CASE_0_PI};
                            integral_radius = {ring.radius_min_, ring.radius_max_};
                            return 19;
                          }
                        else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI};
                          {
                            integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                               ring.radius_max_};
                            return 20;
                          }
                      }
                  }
                else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN != {0, PI}; ring.radius_min <= distance;
                  {
                    if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN != {0, PI};
                      {
                        integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX};
                        integral_radius = {ring.radius_min_,
                                           ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                           ring.radius_max_};
                        return 21;
                      }
                    else // MIN_MIN = PI; MIN_MAX = 0; MAX_MIN != {0, PI}; MAX_MAX != {0, PI};
                      {
                        if (cross_distance[RING_INDEX_MIN_MIN] > cross_distance[RING_INDEX_MIN_MAX])
                          {
                            integral_case = {RING_CASE_0_PI, RING_CASE_MIN_PI, RING_CASE_MIN_MAX};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_max_};
                            return 22;
                          }
                        else
                          {
                            integral_case = {RING_CASE_0_PI, RING_CASE_0_MAX, RING_CASE_MIN_MAX};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                               ring.radius_max_};
                            return 23;
                          }
                      }
                  }
              }
            else if (cross_angle[RING_INDEX_MIN_MAX] == real(RING_PI))
              { // MIN_MIN = PI; ring.radius_min <= distance;
                if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI))
                  { // MIN_MIN = PI; MIN_MAX = PI; ring.radius_min <= distance;
                    if (ring.radius_max_ > distance)
                      { // MIN_MIN = PI; MIN_MAX = PI; MAX_MIN = PI; ring.radius_min <= distance;
                        if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI))
                          {
                            integral_case = {RING_CASE_0_PI};
                            integral_radius = {radius_min_, radius_max_};
                            return 24;
                          }
                        else // MIN_MIN = PI; MIN_MAX = PI; MAX_MIN = PI; MAX_MAX != {0, PI};
                          { // ring.radius_min <= distance; ring.radius_max_ > distance;
                            integral_case = {RING_CASE_MIN_PI, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                            integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                               ring.radius_max_};
                            return 25;
                          }
                      }
                    else // MIN_MIN = PI; MIN_MAX = PI; MAX_MIN = PI;
                      { // ring.radius_max_ <= distance; ring.radius_min <= distance;
                        if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI))
                          {
                            integral_case = {};
                            integral_radius = {};
                            return 26;
                          }
                        else // MIN_MIN = PI; MIN_MAX = PI; MAX_MIN = PI; MAX_MAX != {0, PI};
                          { // ring.radius_min <= distance; ring.radius_max_ <= distance;
                            integral_case = {RING_CASE_MIN_PI};
                            integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                               ring.radius_max_};
                            return 27;
                          }
                      }
                  }
                else // MIN_MIN = PI; MIN_MAX = PI; MAX_MIN != {0, PI}; ring.radius_min <= distance;
                  {
                    integral_case = {RING_CASE_MIN_PI, RING_CASE_MIN_MAX};
                    integral_radius = {ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                       ring.radius_max_};
                    return 28;
                  }
              }
            else // MIN_MIN = PI; ring.radius_min <= distance; MIN_MAX != {0, PI};
              {
                if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI))
                  {
                    if (ring.radius_max_ > distance) // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI;
                      {
                        if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI))
                          { // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI; MAX_MAX = PI;
                            integral_case = {RING_CASE_MIN_PI, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                               ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                            return 29;
                          }
                        else // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN = PI; MAX_MAX = PI;
                          {
                            integral_case = {RING_CASE_MIN_PI, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                            integral_radius = {ring.radius_min_,
                                               ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                               ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                               ring.radius_max_};
                            return 30;
                          }
                      }
                    else // MIN_MIN = PI; MIN_MAX != {0, PI}; ring.radius_max <= distance; MAX_MIN = PI; MAX_MAX != {0, PI};
                      {
                        integral_case = {RING_CASE_MIN_PI};
                        integral_radius = {ring.radius_min_, ring.radius_max_};
                        return 31;
                      }
                  }
                else // MIN_MIN = PI; MIN_MAX != {0, PI}; MAX_MIN != {0, PI}; MAX_MAX != {0, PI}
                  {
                    integral_case = {RING_CASE_MIN_PI, RING_CASE_MIN_MAX};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MIN],
                                       ring.radius_max_};
                    return 32;
                  }
              }
          }
      }
    else // MIN_MIN != {0, PI}
      {
        if (cross_angle[RING_INDEX_MIN_MAX] == real(0)) // MIN_MIN != {0, PI}; MIN_MAX = 0;
          {
            if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI))
              {
                if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN = PI;
                  {
                    integral_case = {RING_CASE_0_MAX, RING_CASE_0_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                       ring.radius_max_};
                    return 33;
                  }
                else if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI)) // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN = PI;
                  {
                    if (cross_distance[RING_INDEX_MIN_MAX] + ring.radius_min_ <
                        ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN])
                      {
                        integral_case = {RING_CASE_0_MAX, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                        integral_radius = {ring.radius_min_,
                                           ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                           ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                           ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                        return 34;
                      }
                    else
                      {
                        integral_case = {RING_CASE_0_MAX, RING_CASE_0_PI, RING_CASE_MIN_PI};
                        integral_radius = {ring.radius_min_,
                                           ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                           ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                           ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                        return 35;
                      }
                  }
                else // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN = PI; MAX_MAX != {0, PI};
                  {
                    if (cross_distance[RING_INDEX_MIN_MAX] + ring.radius_min_ < distance + radius_min_)
                      {
                        integral_case = {RING_CASE_0_MAX, RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                        integral_radius = {ring.radius_min_,
                                           ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                           distance + radius_min_,
                                           ring.radius_max_};
                        return 36;
                      }
                    else
                      {
                        integral_case = {RING_CASE_0_MAX, RING_CASE_0_PI, RING_CASE_MIN_PI};
                        integral_radius = {ring.radius_min_,
                                           distance + radius_min_,
                                           ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                           ring.radius_max_};
                        return 37;
                      }
                  }
              }
            else // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN != {0, PI};
              {
                if (cross_angle[RING_INDEX_MAX_MAX] == real(0)) // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN != {0, PI};
                  {
                    integral_case = {RING_CASE_0_MAX};
                    integral_radius = {ring.radius_min_, ring.radius_max_};
                    return 38;
                  }
                else // MIN_MIN != {0, PI}; MIN_MAX = 0; MAX_MIN != {0, PI}; MAX_MAX != {0, PI};
                  {
                    integral_case = {RING_CASE_0_MAX, RING_CASE_MIN_MAX};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_min_ + cross_distance[RING_INDEX_MIN_MAX],
                                       ring.radius_max_};
                    return 39;
                  }
              }
          }
        else // MIN_MIN != {0, PI}; MIN_MAX != {0, PI};
          {
            if (cross_angle[RING_INDEX_MAX_MIN] == real(RING_PI)) // MIN_MIN != {0, PI}; MIN_MAX != {0, PI};
              {
                if (cross_angle[RING_INDEX_MAX_MAX] == real(RING_PI)) // MIN_MIN != {0, PI}; MIN_MAX != {0, PI}; MAX_MIN = PI;
                  {
                    integral_case = {RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MAX]};
                    return 40;
                  }
                else // MIN_MIN != {0, PI}; MIN_MAX != {0, PI}; MAX_MIN = PI; MAX_MAX != {0, PI};
                  {
                    integral_case = {RING_CASE_MIN_MAX, RING_CASE_MIN_PI};
                    integral_radius = {ring.radius_min_,
                                       ring.radius_max_ - cross_distance[RING_INDEX_MAX_MIN],
                                       ring.radius_max_};
                    return 41;
                  }
              }
            else // MIN_MIN != {0, PI}; MIN_MAX != {0, PI}; MAX_MIN != {0, PI};
              {
                integral_case = {RING_CASE_MIN_MAX};
                integral_radius = {ring.radius_min_, ring.radius_max_};
                return 42;
              }
          }
      }

    // In case not returned yet.
    return -1;
  }


  // Constructors.
  ClassRing::ClassRing() : radius_min_(real(0)), radius_max_(std::numeric_limits<real>::max())
  {
    return;
  }

  ClassRing::ClassRing(const real &radius_min, const real &radius_max)
    : radius_min_(radius_min), radius_max_(radius_max)
  {
    check_radius();
    return;
  }


  // Destructor.
  ClassRing::~ClassRing()
  {
    return;
  }


  // Get methods.
  real ClassRing::GetRadiusMin() const
  {
    return radius_min_;
  }

  real ClassRing::GetRadiusMax() const
  {
    return radius_max_;
  }


  // Set methods.
  void ClassRing::SetRadiusMin(const real &radius_min)
  {
    radius_min_ = radius_min;
    check_radius();
  }

  void ClassRing::SetRadiusMax(const real &radius_max)
  {
    radius_max_ = radius_max;
    check_radius();
  }


  // Compute ring volume.
  real ClassRing::ComputeSelfVolume() const
  {
    return real(1.333333333333333333) * real(RING_PI)
      * (radius_max_ * radius_max_ * radius_max_
         - radius_min_ * radius_min_ * radius_min_);
  }


  // Compute cross volume angles with another ring.
  void ClassRing::ComputeCrossAngle(const real &distance, const ClassRing &ring,
                                    vector<real> &cross_angle, const bool in_degree) const
  {
    if (distance < real(0))
      throw AMC::Error("Distance between ring centers must be >= 0.");

    cross_angle = {compute_cross_angle(distance, ring.radius_min_, radius_min_),
                   compute_cross_angle(distance, ring.radius_min_, radius_max_),
                   compute_cross_angle(distance, ring.radius_max_, radius_min_),
                   compute_cross_angle(distance, ring.radius_max_, radius_max_)};

    if (in_degree)
      for (int i = 0; i < 4; i++)
        cross_angle[i] *= real(180) / real(RING_PI);

#ifdef AMC_WITH_DEBUG_RING
    CBUG << "cross_angle = " << cross_angle << endl;
#endif
  }


  // Compute cross volume distances with another ring.
  void ClassRing::ComputeCrossDistance(const real &distance, const ClassRing &ring, vector<real> &cross_distance) const
  {
    vector1r cross_angle;
    ComputeCrossAngle(distance, ring, cross_angle);

    compute_cross_distance(distance, ring, cross_angle, cross_distance);

#ifdef AMC_WITH_DEBUG_RING
    for (int i = 0; i < 4; i++)
      if (cross_distance[i] < real(0))
        throw AMC::Error("Negative cross distance at index " + to_str(i) + ".");
#endif
  }


  // Compute cross case.
  int ClassRing::ComputeCrossCase(const real &distance, const ClassRing &ring,
                                  vector<int> &integral_case, vector<real> &integral_radius) const
  {
    vector1r cross_angle;
    ComputeCrossAngle(distance, ring, cross_angle);

    vector1r cross_distance;
    compute_cross_distance(distance, ring, cross_angle, cross_distance);

    return compute_cross_case(distance, ring, cross_distance, cross_angle, integral_case, integral_radius);    
  }


  // Compute cross volume with another ring.
  real ClassRing::ComputeCrossVolume(const real &distance, const ClassRing &ring) const
  {
    vector1i integral_case;
    vector1r integral_radius;
    const int case_integral = ComputeCrossCase(distance, ring, integral_case, integral_radius);

#ifdef AMC_WITH_DEBUG_RING
    CBUG << "case_integral = " << case_integral << endl;
    CBUG << "integral_case = " << integral_case << endl;
    CBUG << "integral_radius = " << integral_radius << endl;

    if (case_integral < 0)
      throw AMC::Error("Negative case index, did not found any.");

    for (int i = 0; i < int(integral_case.size()); ++i)
      if (integral_radius[i] > integral_radius[i + 1])
        throw AMC::Error("Integral radius are not in ascending order.");
#endif

#ifdef AMC_WITH_TEST
    case_counter_test_[case_integral]++;
#endif

    real cross_volume(real(0));
    for (int i = 0; i < int(integral_case.size()); ++i)
      if (integral_radius[i + 1] > integral_radius[i])
        {
          if (integral_case[i] == RING_CASE_0_PI)
            cross_volume += compute_integral_0_pi(distance, integral_radius[i], integral_radius[i + 1]);
          else if (integral_case[i] == RING_CASE_0_MAX)
            cross_volume += compute_integral_0_max(distance, integral_radius[i], integral_radius[i + 1]);
          else if (integral_case[i] == RING_CASE_MIN_PI)
            cross_volume += compute_integral_min_pi(distance, integral_radius[i], integral_radius[i + 1]);
          else if (integral_case[i] == RING_CASE_MIN_MAX)
            cross_volume += compute_integral_min_max(distance, integral_radius[i], integral_radius[i + 1]);
          else
            throw AMC::Error("Unknown case (" + to_str(integral_case[i]) + ") for cross volume computation.");
        }

    return cross_volume * RING_PI;
  }


  // Ring string.
  string ClassRing::Str() const
  {
    ostringstream s;
    s << "(" << radius_min_ << ", " << radius_max_ << ")";
    return s.str();
  }


#ifdef AMC_WITH_TEST
  // Test.
  void ClassRing::Test(const ClassRing &b, const int N, const real epsilon)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Test ring class with ring b = " << b.Str() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "N = " << N << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "epsilon = " << epsilon << endl;
#endif

    // Reset counter.
    ResetCaseCounterTest();

    // Volume of ring b.
    const real ring_volume_b = b.ComputeSelfVolume();

#ifdef AMC_WITH_TIMER
    // Record the beginning time.
    const int time_index = AMCTimer<CPU>::Add("ring_test");
#endif

    // Distance max between ring centers.
    const real distance_max = b.radius_max_ * real(1.5);

    // Delta distance between ring centers.
    const real distance_delta = distance_max / real(N - 1);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Info(2) << Reset() << "distance_max = " << distance_max << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(2) << Reset() << "distance_delta = " << distance_delta << endl;
#endif

    int count(0);
    for (int h = 0; h < N; ++h)
      {
        // Distance between centers.
        const real distance = h * distance_delta;

        // Max radius of ring a.
        const real radius_max_a = (distance + b.radius_max_) * real(1.5);

        // Delta radius for a ring.
        const real radius_delta_a = radius_max_a / real(N - 1);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Info(3) << Reset() << "radius_max_a = " << radius_max_a << endl;
        *AMCLogger::GetLog() << Fred() << Info(3) << Reset() << "radius_delta_a = " << radius_delta_a << endl;
#endif

        for (int i = 1; i < N; ++i)
          for (int j = 0; j < i; ++j)
            {
              // The rings we use for test.
              ClassRing a(radius_delta_a * real(j), radius_delta_a * real(i));

              // Compute self volumes.
              const real ring_volume_a = a.ComputeSelfVolume();

#ifdef AMC_WITH_DEBUG_RING
              CBUG << std::string('=', 64) << endl;
              CBUG << "distance = " << distance << endl;
              CBUG << "a = " << a.Str() << ", ring_volume_a = " << ring_volume_a << endl;
              CBUG << "b = " << b.Str() << ", ring_volume_b = " << ring_volume_b << endl;
#endif

#ifdef AMC_WITH_TIMER
              AMCTimer<CPU>::Start();
#endif

              // Compute cross volume.
              const real cross_volume_ba = b.ComputeCrossVolume(distance, a);
              const real cross_volume_ab = a.ComputeCrossVolume(distance, b);

#ifdef AMC_WITH_TIMER
              AMCTimer<CPU>::Stop(time_index);
#endif

#ifdef AMC_WITH_DEBUG_RING
              CBUG << "cross_volume_ba = " << cross_volume_ba << ", cross_volume_ab = " << cross_volume_ab << endl;
#endif

              if (cross_volume_ba < - real(epsilon) || cross_volume_ab < - real(epsilon))
                throw AMC::Error("Negative cross volumes.");

              if (abs(cross_volume_ba - cross_volume_ab) > epsilon)
                throw AMC::Error("Different cross volumes.");

              if ((cross_volume_ba - ring_volume_a) > epsilon ||
                  (cross_volume_ab - ring_volume_b) > epsilon)
                throw AMC::Error("Cross volumes greater then self volumes.");

              count += 2;
            }
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Test ring class with ring b = " << b.Str() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "N = " << N << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "epsilon = " << epsilon << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(2) << Reset() << "distance_max = " << distance_max << endl;
    *AMCLogger::GetLog() << Fcyan() << Info(2) << Reset() << "distance_delta = " << distance_delta << endl;
#ifdef AMC_WITH_TIMER
    // How much CPU time did we use ?
    *AMCLogger::GetLog() << Fyellow() << Info(2) << Reset() << "Completed " << count
                         << " ring cross volume tests in " << AMCTimer<CPU>::GetTimer(time_index)
                         << " seconds (" << AMCTimer<CPU>::GetTimer(time_index) / (count > 0 ? count : 1)
                         << " seconds per test)." << endl;
#endif
#endif
  }


  // Get case counter test.
  void ClassRing::GetCaseCounterTest(vector<int> &case_counter_test)
  {
    case_counter_test = case_counter_test_;
  }


  // Reset case counter test.
  void ClassRing::ResetCaseCounterTest()
  {
    case_counter_test_.assign(RING_NUMBER_CASE, 0);
  }
#endif
}

#define AMC_FILE_CLASS_RING_CXX
#endif
