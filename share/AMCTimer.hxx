// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_TIMER_HXX

namespace AMC
{
  /*!
   * \class CPU
   */
  class CPU
  {
  public:
    typedef clock_t point_type_;
    typedef clock_t duration_type_;

  public:

    /*!< Zero duration time for CPU.*/
    static duration_type_ Zero();

    /*!< Point time for CPU.*/
    static point_type_ Point();

    /*!< Convert the CPU duration time into floating seconds.*/
    static double Second(const duration_type_ &duration);
  };


  /*!
   * \class Wall
   */
  class Wall
  {
  public:

#if __cplusplus > 199711L
    typedef std::chrono::steady_clock clock_;
#else
    typedef std::chrono::monotonic_clock clock_;
#endif

    typedef std::chrono::time_point<clock_> point_type_;
    typedef std::chrono::duration<double> duration_type_;

  public:

    /*!< Zero duration time for wall elapsed time.*/
    static duration_type_ Zero();

    /*!< Point time for wall elapsed time.*/
    static point_type_ Point();

    /*!< Convert the wall elapsed duration time into floating seconds.*/
    static double Second(const duration_type_ &duration);
  };


  /*!
   * \class AMCTimer
   */
  template<class C>
  class AMCTimer
  {
  private:

    /*!< Number of time records.*/
    static int Ntimer_;

    /*!< Vector of time records.*/
    static vector<typename C::duration_type_> timer_;

    /*!< Vector of string to which cpu time corresponds.*/
    static vector<string> name_;

    /*!< Start of timer.*/
    static typename C::point_type_ start_;

  public:

    /*!< Add one record.*/
    static int Add(const string name);

    /*!< Remove one or all records.*/
    static void Clear(int i = -1);

    /*!< Reset timers.*/
    static void Reset(int i = -1);

    /*!< Start end methods.*/
    static void Start();
    static void Stop(const int &i);

    /*!< Get methods.*/
    static int GetNtimer();
    static double GetTimer(const int &i);
    static string GetName(const int &i);

    /*!< Display all timers.*/
    static map<string, double> Display(int i = -1);
  };
}

#define AMC_FILE_AMC_TIMER_HXX
#endif
