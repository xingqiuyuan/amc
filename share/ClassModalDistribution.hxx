// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODAL_DISTRIBUTION_HXX

#define MODAL_DISTRIBUTION_CONFIGURATION_FILE_DEFAULT  "modal.lua"
#define MODAL_DISTRIBUTION_PI6         0.5235987755982988
#define MODAL_DISTRIBUTION_INV_PI6     1.909859317102744
#define MODAL_DISTRIBUTION_SQRT2PI     2.5066282746310002
#define MODAL_DISTRIBUTION_INV_SQRT2PI 0.3989422804014327
#define MODAL_DISTRIBUTION_FRAC3       0.3333333333333333
#define MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT 10000

namespace AMC
{
  /*! 
   * \class ClassModalDistribution
   * \brief Computes sectional particle aerosol distributions from modal parameters.
   */
  template<class T>
  class ClassModalDistribution
  {
  private:

    /*!< Configuration file.*/
    static string configuration_file_;

    /*!< Is class initialized.*/
    static bool is_initialized_;

    /*!< Name of distribution.*/
    string name_;

    /*!< Number of modes.*/
    int Nmode_;

    /*!< Number of quadrature points.*/
    int Nquadrature_;

    /*!< Are mode concentrations given as volume or number.*/
    bool is_volume_;

    /*!< Mode number or volume concentration.*/
    vector<T> concentration_;

    /*!< Mode geometric mean diameter in µm.*/
    vector<T> geometric_mean_diameter_;

    /*!< Logarithm of mode geometric mean diameter.*/
    vector<T> geometric_mean_diameter_log_;

    /*!< Mode geometric standard deviation.*/
    vector<T> geometric_standard_deviation_;

    /*!< Inverse of logarithm of mode geometric standard deviation.*/
    vector<T> geometric_standard_deviation_log_inv_;

    /*!< Get number density.*/
    T get_number_density(const T &diameter) const;

    /*!< Get volume density.*/
    T get_volume_density(const T &diameter) const;

    /*!< Read ops configuration.*/
    void read(Ops::Ops &ops);

  public:

    /*!< Init.*/
    static void Init(const string configuration_file = "modal.lua");

    /*!< Clear.*/
    static void Clear();

    /*!< Get available distributions from config files.*/
    static vector<string> GetAvailableDistribution();

    /*!< Get configuration file.*/
    static string GetConfigurationFile();

    /*!< Constructors.*/
    ClassModalDistribution(Ops::Ops &ops);
    ClassModalDistribution(bool is_volume = false);
    ClassModalDistribution(const string &name);

    /*!< Destructor.*/
    ~ClassModalDistribution();

    /*!< Add one mode.*/
    void AddMode(const T &geometric_mean_diameter,
                 const T &geometric_standard_deviation,
                 const T concentration = 1);

    /*!< Get methods.*/
    string GetName() const;
    bool IsVolume() const;
    int GetNmode() const;
    int GetNquadrature() const;
    T GetConcentration(const int &i) const;
    T GetDiameter(const int &i) const;
    T GetStd(const int &i) const;
    T GetNumberDensity(const T &diameter) const;
    T GetVolumeDensity(const T &diameter) const;

    /*!< Set methods.*/
    void SetNquadrature(const int Nquadrature = MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT);
    void SetName(const string &name);

    /*!< Compute sectional distribution from modes.*/
    void ComputeSectionalDistribution(const vector<T> &diameter,
                                      vector<T> &diameter_mean,
                                      vector<T> &distribution_number,
                                      vector<T> &distribution_volume,
                                      int Nquadrature = 0) const;
  };
}

#define AMC_FILE_CLASS_MODAL_DISTRIBUTION_HXX
#endif
