// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_STATISTICS_CXX

#include "AMCStatistics.hxx"


namespace AMC
{
  // Initialize statistics.
  inline void AMCStatistics::reset_statistics(const int &i)
  {
    value_min_[i].assign(n_[i], numeric_limits<real>::max());
    value_max_[i].assign(n_[i], numeric_limits<real>::min());
    value_mean_[i].assign(n_[i], real(0));
    value_std_[i].assign(n_[i], real(0));
    count_[i] = 0;
  }


  // Resize statistics.
  inline void AMCStatistics::resize_statistics(const int &i, const int n)
  {
    if (n_[i] != n)
      {
#ifdef AMC_WITH_DEBUG_STATISTICS
        CBUG << "i = " << i << ", n = " << n << endl;
#endif
        value_min_[i].resize(n);
        value_max_[i].resize(n);
        value_mean_[i].resize(n);
        value_std_[i].resize(n);
        n_[i] = n;
      }

    reset_statistics(i);
  }


  // Write statistics to file.
  inline void AMCStatistics::statistics_to_file(const int &i)
  {
    // Check if file path exists, create it if not.
    char *file_path_c = new char [file_path_[i].length() + 1];
    strcpy(file_path_c, file_path_[i].c_str());
    char *base_dir_c = dirname(file_path_c);

    char *cwd = get_current_dir_name();

    if (chdir(base_dir_c) != 0)
      {
        const string base_dir(base_dir_c);
        util::make_directory(base_dir);
#ifdef AMC_WITH_DEBUG_STATISTICS
        CBUG << "make directory \"" << base_dir << endl;
#endif
      }

    int ignore_value = chdir(cwd);

    delete file_path_c;
    free(cwd);

    // Current time in human readable format.
    time_t now_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

#ifdef AMC_WITH_DEBUG_STATISTICS
    CBUG << "now_t = " << ctime(&now_t) << endl;
#endif

    ofstream fout(file_path_[i].c_str());

    fout << "Time : " << ctime(&now_t) << endl;
    fout << AMCStatistics::Display(i);

    fout.close();
  }


  // Init.
  void AMCStatistics::Init(const string &config_file)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Init Statistics subsystem"
                         << " from configuration file \"" << config_file << "\"." << endl;
#endif

    Ops::Ops ops(config_file);

    if (ops.Exists("statistics"))
      {
        ops.SetPrefix("statistics.");

        const bool active_default = ops.Get<bool>("active", "", AMC_STATISTICS_ACTIVE);

        string write_directory = ops.Get<string>("write.directory", "", "");
        if (! write_directory.empty())
          if (write_directory.back() != '/')
            write_directory += "/";

        const int write_frequency_default = ops.Get<int>("write.frequency", "", AMC_STATISTICS_WRITE_FREQUENCY);
        const int compute_frequency_default = ops.Get<int>("compute.frequency", "", AMC_STATISTICS_COMPUTE_FREQUENCY);
        if (compute_frequency_default <= 0)
          throw AMC::Error("Null or negative default compute frequency for statistics (got "
                           + to_str(compute_frequency_default) + ").");

        const bool compute_rolling_default = ops.Get<bool>("compute.rolling", "", AMC_STATISTICS_COMPUTE_ROLLING);

        const vector1s name = ops.GetEntryList("list");

        for (int i = 0; i < int(name.size()); ++i)
          {
            ops.SetPrefix("statistics.list." + name[i] + ".");

            if (ops.Get<bool>("active", "", active_default))
              {
#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Adding statistics \"" << name[i] << "\"." << endl;
#endif
                index_[name[i]] = Nstatistics_;
                compute_rolling_.push_back(ops.Get<bool>("compute.rolling", "", compute_rolling_default));
                compute_frequency_.push_back(ops.Get<int>("compute.frequency", "", compute_frequency_default));
                if (compute_frequency_.back() <= 0)
                  throw AMC::Error("Null or negative compute frequency for statistics (got "
                                   + to_str(compute_frequency_) + ").");

                write_frequency_.push_back(ops.Get<int>("write.frequency", "", write_frequency_default));

                name_.push_back(name[i]);
                file_path_.push_back(write_directory + ops.Get<string>("file_path", "", ""));

#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "\tcompute_rolling ?"
                                     << (compute_rolling_.back() ? "yes" : "no") << endl;
                *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "\tcompute_frequency = "
                                     << compute_frequency_.back() << endl;
                *AMCLogger::GetLog() << Fcyan() << Debug() << Reset() << "\twrite_frequency = "
                                     << write_frequency_.back() << endl;
                *AMCLogger::GetLog() << Fcyan() << Debug() << Reset() << "\tfile_path = "
                                     << file_path_.back() << endl;
#endif

                Nstatistics_++;
              }

            value_min_.resize(Nstatistics_);
            value_max_.resize(Nstatistics_);
            value_mean_.resize(Nstatistics_);
            value_std_.resize(Nstatistics_);

            n_.assign(Nstatistics_, 0);
            count_.assign(Nstatistics_, 0);
            time_last_.assign(Nstatistics_, clock_::now());
          }
      }

    ops.Close();

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << Reset() << "Registered " << Nstatistics_ << " statistics." << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug() << Reset() << "Statistics name = " << name_ << endl;
#endif
  }


  // Add one statistics.
  void AMCStatistics::Add(const string &name, const vector<real> &value)
  {
    // If nothing registered, just return.
    if (Nstatistics_ == 0)
      return;

    // Safely return in case wrong name : avoid simulation crashes.
    map<string, int>::iterator it = index_.find(name);
    if (it == index_.end())
      return;

    const int i(it->second);

#ifdef AMC_WITH_DEBUG_STATISTICS
    CBUG << "name = " << name_[i] << ", index = " << i << endl;
#endif

    resize_statistics(i, int(value.size()));

    const int n = n_[i];
    vector1r &value_min = value_min_[i];
    vector1r &value_max = value_max_[i];
    vector1r &value_mean = value_mean_[i];
    vector1r &value_std = value_std_[i];

#ifdef AMC_WITH_DEBUG_STATISTICS
    CBUG << "compute_frequency = " << compute_frequency_[i] << endl;
    CBUG << "count = " << count_[i] << endl;
#endif

    if (count_[i] % (unsigned long)(compute_frequency_[i]) > 0)
      {
        for (int j = 0; j < n; ++j)
          if (value[j] > real(0))
            {
              if (value[j] < value_min[j]) value_min[j] = value[j];
              if (value[j] > value_max[j]) value_max[j] = value[j];
              value_mean[j] += value[j];
              value_std[j] += value[j] * value[j];
            }

        count_[i]++;
      }
    else
      {
#ifdef AMC_WITH_DEBUG_STATISTICS
        CBUG << "compute_rolling = " << (compute_rolling_[i] ? "yes" : "no") << endl;
#endif

        // If you do not roll, we need to keep track of old mean and std.
        vector1r save_mean;
        vector1r save_std;

        if (! compute_rolling_[i])
          {
            save_mean = value_mean;
            save_std = value_std;
          }

        // Compute current mean and std.
        const real count_inv = real(1) / real(count_[i]);

        for (int j = 0; j < n; ++j)
          value_mean[j] *= count_inv;

        for (int j = 0; j < n; ++j)
          value_std[j] = sqrt(value_std[j] * count_inv - value_mean[j]);

        // If file name exists,
        if (! file_path_[i].empty())
          {
            point_type_ time_now = clock_::now();
            duration_type_ duration = time_now - time_last_[i];

#ifdef AMC_WITH_DEBUG_STATISTICS
            CBUG << "duration = " << duration.count() << endl;
            CBUG << "write_frequency = " << write_frequency_[i] << endl;
#endif

            // and if duration is above write frequency (secs),
            if (duration.count() > double(write_frequency_[i]))
              {
                // then write stats in file.
                statistics_to_file(i);
                time_last_[i] = time_now;
              }
          }

        // If on a rolling period, reset statistics,
        // otherwise give back saved mean and std.
        if (compute_rolling_[i])
          reset_statistics(i); // This also reset count_[i] to 0.
        else
          {
            value_mean = save_mean;
            value_std = save_std;
          }
      }
  }


  // Clear statistics.
  void AMCStatistics::Clear()
  {
    Nstatistics_ = 0;
    compute_rolling_.clear();
    compute_frequency_.clear();
    write_frequency_.clear();
    index_.clear();
    n_.clear();
    name_.clear();
    value_min_.clear();
    value_max_.clear();
    value_mean_.clear();
    value_std_.clear();
    count_.clear();
    file_path_.clear();
    time_last_.clear();
  }


  // Get methods.
  int AMCStatistics::GetNstatistics()
  {
    return Nstatistics_;
  }


  int AMCStatistics::GetComputeFrequency(const int &i)
  {
    return compute_frequency_[i];
  }


  bool AMCStatistics::IsComputeRolling(const int &i)
  {
    return compute_rolling_[i];
  }


  int AMCStatistics::GetWriteFrequency(const int &i)
  {
    return write_frequency_[i];
  }


  int AMCStatistics::GetIndex(const string &name)
  {
    return index_[name];
  }


  string AMCStatistics::GetName(const int &i)
  {
    return name_[i];
  }


  string AMCStatistics::GetFilePath(const int &i)
  {
    return file_path_[i];
  }


  int AMCStatistics::GetCount(const int &i)
  {
    return int(count_[i]);
  }


  vector<string> AMCStatistics::GetNameList()
  {
    return name_;
  }


  // Display statistics.
  string AMCStatistics::Display(int i, const int precision)
  {
    ostringstream sout;

    if (i < 0)
      for (int i = 0; i < Nstatistics_; ++i)
        sout << AMCStatistics::Display(i);
    else
      sout << "name : " << name_[i] << endl
           << "min : " << setprecision(precision) << value_min_[i] << endl
           << "max : " << setprecision(precision) << value_max_[i] << endl
           << "mean : " << setprecision(precision) << value_mean_[i] << endl
           << "std : " << setprecision(precision) << value_std_[i] << endl;

    // Return everything as a string.
    return sout.str();
  }
}

#define AMC_FILE_AMC_STATISTICS_CXX
#endif
