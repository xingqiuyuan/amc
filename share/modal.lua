
modal_distribution = {
   general = {
      Nquadrature = 10000,
   },

   -- Seigneur et al. 1986
   seigneur_1986_clear = {
      diameter = {0.03, 0.2, 6.0},
      std = {1.8, 1.6, 2.2},
      volume = {0.03, 1.0, 5.0}
   },

   seigneur_1986_hazy = {
      diameter = {0.044, 0.24, 6.0},
      std = {1.2, 1.8, 2.2},
      volume = {0.09, 5.8, 25.9}
   },

   seigneur_1986_urban = {
      diameter = {0.038, 0.32, 5.7},
      std = {1.8, 2.16, 2.21},
      volume = {0.63, 38.4, 30.8}
   },

   -- Kittelson et al. 2006
   diesel_kittelson = {
      diameter = {0.011, 0.17},
      std = {1.27, 1.9},
      volume = {26.4, 4878.}
   },

   -- Eurotrack
   urban_eurotrack = {
      diameter = {0.014, 0.057, 0.86},
      std = {1.8, 2.16, 2.21},
      number = {111572., 31269., 2.33}
   },

   land_eurotrack = {
      diameter = {0.015, 0.076, 1.02},
      std = {1.7, 2.0, 2.16},
      number = {13641., 5312., 1.07}
   },

   marine_eurotrack = {
      diameter = {0.0616, 0.24, 0.968},
      std = {1.47, 1.4, 1.87},
      number = {347., 215., 4.09}
   }
}