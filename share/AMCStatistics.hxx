// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_STATISTICS_HXX

#define AMC_STATISTICS_ACTIVE             false  // By default, statistics are not active.
#define AMC_STATISTICS_COMPUTE_ROLLING    true   // By default, statistics are compute on a rolling period. 
#define AMC_STATISTICS_COMPUTE_FREQUENCY  60     // By default, statistics are compute every XX loops.
#define AMC_STATISTICS_WRITE_FREQUENCY    60     // By default, statistics are written to files every XX seconds.

namespace AMC
{
  /*!
   * \class AMCStatistics
   */
  class AMCStatistics
  {
  public:
#if __cplusplus > 199711L
    typedef std::chrono::steady_clock clock_;
#else
    typedef std::chrono::monotonic_clock clock_;
#endif

    typedef std::chrono::time_point<clock_> point_type_;
    typedef std::chrono::duration<double> duration_type_;

  private:

    /*!< Number of statistics.*/
    static int Nstatistics_;

    /*!< Whether to compute stats on a rolling period.*/
    static vector1b compute_rolling_;

    /*!< Frequency with which compute statistics.*/
    static vector1i compute_frequency_;

    /*!< Frequency with which write in files.*/
    static vector1i write_frequency_;

    /*!< Last time.*/
    static vector<point_type_> time_last_;

    /*!< Index of statistics.*/
    static map<string, int> index_;

    /*!< Size of statistics vector.*/
    static vector1i n_;

    /*!< Name of statistics.*/
    static vector1s name_;

    /*!< Min, max and mean value.*/
    static vector2r value_min_;
    static vector2r value_max_;
    static vector2r value_mean_;
    static vector2r value_std_;

    /*!< Count for each statistics.*/
    static vector<unsigned long> count_;

    /*!< File name for each statistics.*/
    static vector1s file_path_;

    /*!< Reset statistics.*/
    static void reset_statistics(const int &i);

    /*!< Resize statistics.*/
    static void resize_statistics(const int &i, const int size);

    /*!< Write statistics to file.*/
    static void statistics_to_file(const int &i);

  public:

    /*!< Init.*/
    static void Init(const string &config_file);

    /*!< Add one statistics.*/
    static void Add(const string &name, const vector<real> &value);

    /*!< Clear statistics.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNstatistics();
    static int GetComputeFrequency(const int &i);
    static bool IsComputeRolling(const int &i);
    static int GetWriteFrequency(const int &i);
    static int GetIndex(const string &name);
    static string GetName(const int &i);
    static string GetFilePath(const int &i);
    static int GetCount(const int &i);
    static vector<string> GetNameList();

    /*!< Display statistics.*/
    static string Display(int i = -1, const int precision = 6);
  };
}

#define AMC_FILE_AMC_STATISTICS_HXX
#endif
