// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_FUNCTIONS_HXX

namespace AMC
{
#ifndef SWIG
  /*!< Compute cubic root.*/
  float pow3(float x);
  double pow3(double x);

  /*!< Get the number of power of p out of n.*/
  int get_power_out_of(const int &n, const int p = 10);

  /*!< Check index.*/
  void check_index(const int &index_min, const int &index_max, const int &index);
#endif

  /*!< Get combinations of p values within n.*/
  bool get_combination(const int &p, const int &n, vector<int> &index_reference, vector<int> &index);

  /*!< Find convex hull of a polygon.*/
  template<typename T>
  void find_polygon_convex_hull(const int &p, const int &n, const vector<T> &point, vector<int> &);

#ifndef SWIG
  /*!< Search index in a vector.*/
  template<typename T>
  int search_index(const vector<T> &v, const T &x, int imin = 0, int imax = -1);

  template<typename T>
  int search_index_ascending(const vector<T> &v, const T x, int imin = 0);

  template<typename T>
  int search_index_descending(const vector<T> &v, const T x, int imax = -1);

  /*!< Test is one vector (v) is included into another one (w).*/
  template<typename T>
  bool is_included(const vector<T> &v, const vector<T> &w);
#endif

  /*!< Compute sum of vector or part of vector.*/
  template<typename T>
  T compute_vector_sum(const vector<T> &v, int n = 0);

  /*!< Compute the determinant of a matrix, stored in row major (C-style).*/
  template<typename T>
  T compute_matrix_determinant(const vector<T> &matrix, int n);

#ifndef SWIG
  /*!< Generate pseudo-random vector with law uniform.*/
  template<typename T>
  void generate_random_vector(const int &n, vector<T> &random, const T sum = T(1));
#endif

  /*!< Set the random generator seed in C++.*/
  void set_random_generator(const int &seed);

  /*!< Generate one random vector.*/
  template<typename T>
  vector<T> generate_random_vector(const int &n, const T sum = T(1));

  /*!< Split one 2D domain into most equally possible subdomains.*/
  void split_domain(const int n, int &p, int &q);

  /*!< Compute the Gauss-Legendre quadrature zeros and weights, from Numerical Recipes in C.*/
  template<typename T>
  void compute_gauss_legendre_zero_weight(const int &n, T *x, T *w,
                                          const T x1 = T(-1), const T x2 = T(1),
                                          const T eps = T(3.e-11));

  /*!< Cubic splines.*/
  template<typename T>
  void compute_cubic_spline(const int n, const T *x, const T *y, T *y2, const T *yp1 = NULL, const T *ypn = NULL);

  template<typename T>
  T apply_cubic_spline(const int n, const T *xa, const T *ya, const T *y2a, const T &x);

  /*!< Convert ppm to molecule per cm3.*/
  template<typename T>
  T convert_ppm_to_molecule_per_cm3(const T &pressure,
                                    const T &temperature,
                                    const T concentration = T(1));

  /*!< Convert ppt to molecule per cm3.*/
  template<typename T>
  T convert_ppt_to_molecule_per_cm3(const T &pressure,
                                    const T &temperature,
                                    const T concentration = T(1));

  /*!< Convert molecule per cm3 to ppm.*/
  template<typename T>
  T convert_molecule_per_cm3_to_ppm(const T &pressure,
                                    const T &temperature,
                                    const T concentration = T(1));

  /*!< Convert molecule per cm3 to ppt.*/
  template<typename T>
  T convert_molecule_per_cm3_to_ppt(const T &pressure,
                                    const T &temperature,
                                    const T concentration = T(1));
}

#define AMC_FILE_FUNCTIONS_HXX
#endif
