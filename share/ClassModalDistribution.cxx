// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_MODAL_DISTRIBUTION_CXX

#include "ClassModalDistribution.hxx"

namespace AMC
{
  // Get number density.
  template<class T>
  inline T ClassModalDistribution<T>::get_number_density(const T &diameter) const
  {
    if (is_volume_)
      return MODAL_DISTRIBUTION_INV_PI6 / (diameter * diameter * diameter) * get_volume_density(diameter);
    else
      {
        T diameter_log = log10(diameter);
        T number(T(0));
        for (int i = 0; i < Nmode_; i++)
          {
            T tmp = (diameter_log - geometric_mean_diameter_log_[i]) * geometric_standard_deviation_log_inv_[i];
            number += concentration_[i] * exp(-T(0.5) * tmp * tmp) * geometric_standard_deviation_log_inv_[i];
          }
        return number * MODAL_DISTRIBUTION_INV_SQRT2PI;
      }
  }


  // Get volume density.
  template<class T>
  inline T ClassModalDistribution<T>::get_volume_density(const T &diameter) const
  {
    if (! is_volume_)
      return MODAL_DISTRIBUTION_PI6 * diameter * diameter * diameter * get_number_density(diameter);
    else
      {
        T diameter_log = log10(diameter);
        T volume(T(0));
        for (int i = 0; i < Nmode_; i++)
          {
            T tmp = (diameter_log - geometric_mean_diameter_log_[i]) * geometric_standard_deviation_log_inv_[i];
            volume += concentration_[i] * exp(-T(0.5) * tmp * tmp) * geometric_standard_deviation_log_inv_[i];
          }
        return volume * MODAL_DISTRIBUTION_INV_SQRT2PI;
      }
  }


  // Read ops configuration.
  template<class T>
  void ClassModalDistribution<T>::read(Ops::Ops &ops)
  {
    Nquadrature_ = ops.Get<int>("Nquadrature", "", Nquadrature_);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "Nquadrature = " << Nquadrature_ << endl;
#endif

    if (name_.empty())
      {
        name_ = ops.GetPrefix();
        name_.resize(name_.size() - 1);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "name = " << name_ << endl;
#endif

    ops.Set("diameter", "v >= 0.", geometric_mean_diameter_);
    Nmode_ = int(geometric_mean_diameter_.size());

    ops.Set("std", "v >= 0.", geometric_standard_deviation_);
    if (Nmode_ != int(geometric_standard_deviation_.size()))
      throw AMC::Error("Number of standard deviation modes differs from number of geometric diameters.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "geometric_mean_diameter = " << geometric_mean_diameter_ << endl;
    *AMCLogger::GetLog() << Fred() << Debug(2) << Reset() << "geometric_standard_deviation = "
                         << geometric_standard_deviation_ << endl;
    *AMCLogger::GetLog() << Fyellow() << Debug(2) << Reset() << "Nmode = " << Nmode_ << endl;
#endif

    // Compute logarithm of mode parameters and convert factor from number to volume.
    geometric_mean_diameter_log_.resize(Nmode_);
    geometric_standard_deviation_log_inv_.resize(Nmode_);

    for (int i = 0; i < Nmode_; i++)
      {
        geometric_mean_diameter_log_[i] = log10(geometric_mean_diameter_[i]);
        geometric_standard_deviation_log_inv_[i] = T(1) / log10(geometric_standard_deviation_[i]);
      }

    // Read mode parameters.
    if (ops.Exists("number"))
      {
        is_volume_ = false;
        ops.Set("number", "v >= 0.", vector<T>(Nmode_, T(1)), concentration_);
      }
    else if (ops.Exists("volume"))
      {
        is_volume_ = true;
        ops.Set("volume", "v >= 0.", vector<T>(Nmode_, T(1)), concentration_);
      }
    else
      throw AMC::Error("No concentrations given, either number (#/m3) or volume (µm^3/m3).");

#ifdef AMC_WITH_LOGGER
  *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "is_volume = " << (is_volume_ ? "yes" : "no") << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "concentration = " << concentration_ << endl;
#endif

    if (int(concentration_.size()) != Nmode_)
      throw AMC::Error("Number of mode concentrations differs from number of geometric diameters.");
  }


  // Init.
  template<class T>
  void ClassModalDistribution<T>::Init(const string configuration_file)
  {
    check_file(configuration_file);
    configuration_file_ = configuration_file;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Global instantiation of modal distribution"
                         << " class with configuration file \"" << configuration_file_ << "\"." << endl;
#endif

    is_initialized_ = true;
  }


  // Clear.
  template<class T>
  void ClassModalDistribution<T>::Clear()
  {
    configuration_file_ = MODAL_DISTRIBUTION_CONFIGURATION_FILE_DEFAULT;
    is_initialized_ = false;
  }


  // Get available distributions from config files.
  template<class T>
  vector<string> ClassModalDistribution<T>::GetAvailableDistribution()
  {
    if (! is_initialized_)
      throw AMC::Error("Modal distribution class is not yet initialized, call Init() first.");

    Ops::Ops ops(configuration_file_);
    vector<string > distribution_name_list = ops.GetEntryList("modal_distribution");
    ops.Close();

    // Remove general section.
    for (vector<string>::iterator it = distribution_name_list.begin(); it != distribution_name_list.end(); it++)
      if (it->find("general") != string::npos)
        distribution_name_list.erase(it);

    return distribution_name_list;
  }


  // Get configuration file.
  template<class T>
  string ClassModalDistribution<T>::GetConfigurationFile()
  {
    return configuration_file_;
  }


  // Constructors.
  template<class T>
  ClassModalDistribution<T>::ClassModalDistribution(Ops::Ops &ops)
    : Nmode_(0), is_volume_(false), Nquadrature_(MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Instantiate modal distribution"
                         << " from ops prefix \"" << ops.GetPrefix() << "\"." << endl;
#endif

    read(ops);

    return;
  }


  template<class T>
  ClassModalDistribution<T>::ClassModalDistribution(bool is_volume)
    : Nmode_(0), is_volume_(is_volume), Nquadrature_(MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Instantiate empty modal distribution with "
                         << (is_volume_ ? "volume" : "number") << " concentrations." << endl;
#endif

    return;
  }


  template<class T>
  ClassModalDistribution<T>::ClassModalDistribution(const string &name)
    : Nmode_(0), name_(name), Nquadrature_(MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT)
  {
    if (! is_initialized_)
      throw AMC::Error("Modal distribution class is not yet initialized, call Init() first.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Instantiate modal"
                         << " distribution of type \"" << name_ << "\"." << endl;
#endif

    Ops::Ops ops(configuration_file_);

    ops.SetPrefix("modal_distribution.general.");
    Nquadrature_ = ops.Get<int>("Nquadrature", "", Nquadrature_);

    ops.SetPrefix("modal_distribution." + name_ + ".");

    read(ops);

    return;
  }


  // Destructor.
  template<class T>
  ClassModalDistribution<T>::~ClassModalDistribution()
  {
    return;
  }


  // Add one mode.
  template<class T>
  void ClassModalDistribution<T>::AddMode(const T &geometric_mean_diameter,
                                          const T &geometric_standard_deviation,
                                          const T concentration)
  {
    geometric_mean_diameter_.push_back(geometric_mean_diameter);
    geometric_standard_deviation_.push_back(geometric_standard_deviation);

    geometric_mean_diameter_log_.push_back(log10(geometric_mean_diameter));
    geometric_standard_deviation_log_inv_.push_back(T(1) / log10(geometric_standard_deviation));

    concentration_.push_back(concentration);

    Nmode_++;
  }


  // Get members.
  template<class T>
  string ClassModalDistribution<T>::GetName() const
  {
    return name_;
  }

  template<class T>
  bool ClassModalDistribution<T>::IsVolume() const
  {
    return is_volume_;
  }

  template<class T>
  int ClassModalDistribution<T>::GetNmode() const
  {
    return Nmode_;
  }

  template<class T>
  int ClassModalDistribution<T>::GetNquadrature() const
  {
    return Nquadrature_;
  }

  template<class T>
  T ClassModalDistribution<T>::GetConcentration(const int &i) const
  {
    return concentration_[i];
  }

  template<class T>
  T ClassModalDistribution<T>::GetDiameter(const int &i) const
  {
    return geometric_mean_diameter_[i];
  }

  template<class T>
  T ClassModalDistribution<T>::GetStd(const int &i) const
  {
    return geometric_standard_deviation_[i];
  }

  template<class T>
  T ClassModalDistribution<T>::GetNumberDensity(const T &diameter) const
  {
    return get_number_density(diameter);
  }

  template<class T>
  T ClassModalDistribution<T>::GetVolumeDensity(const T &diameter) const
  {
    return get_volume_density(diameter);
  }

  // Set methods.
  template<class T>
  void ClassModalDistribution<T>::SetNquadrature(const int Nquadrature)
  {
    Nquadrature_ = Nquadrature;
  }

  template<class T>
  void ClassModalDistribution<T>::SetName(const string &name)
  {
    name_ = name;
  }


  // Get sectional distribution from modes.
  template<class T>
  void ClassModalDistribution<T>::ComputeSectionalDistribution(const vector<T> &diameter,
                                                               vector<T> &diameter_mean,
                                                               vector<T> &distribution_number,
                                                               vector<T> &distribution_volume,
                                                               int Nquadrature) const
  {
    int Nb = int(diameter.size()) - 1;

    if (Nb <= 0)
      throw AMC::Error("Need at least one size section (two diameters).");

    if (Nquadrature == 0)
      Nquadrature = Nquadrature_;

    if (Nquadrature <= 0)
      throw AMC::Error("Negative or null number of quadrature points.");

    for (int i = 0; i < Nb; i++)
      {
        const T &diameter1 = diameter[i];
        const T &diameter2 = diameter[i + 1];
        T log_diameter1 = log(diameter[i]);
        T log_diameter2 = log(diameter[i + 1]);
        T log_difference = log_diameter2 - log_diameter1;
        T log_h = log_difference / T(Nquadrature);

        T num(T(0)), vol(T(0));
        num = (get_number_density(diameter1) + get_number_density(diameter2)) * T(0.5);
        vol = (get_volume_density(diameter1) + get_volume_density(diameter2)) * T(0.5);

        for (int j = 1; j < Nquadrature; j++)
          {
            T log_diameter = log_diameter1 + log_h * T(j);
            T diameter = exp(log_diameter);
            num += get_number_density(diameter);
            vol += get_volume_density(diameter);
          }

        distribution_number.push_back(num * log_h);
        distribution_volume.push_back(vol * log_h);
      }

    diameter_mean.resize(Nb);
    for (int i = 0; i < Nb; i++)
      {
        diameter_mean[i] = pow(distribution_volume[i] / distribution_number[i]
                               * MODAL_DISTRIBUTION_INV_PI6, MODAL_DISTRIBUTION_FRAC3);

        if (diameter_mean[i] < diameter[i] || diameter_mean[i] >= diameter[i + 1])
          throw AMC::Error("Mean diameter of size section " + to_str(i) + " (" + to_str(diameter_mean[i])
                           + ") is outside of section bound diameters [" + to_str(diameter[i])
                           + ", " + to_str(diameter[i + 1]) + "[");
      }
  }
}

#define AMC_FILE_CLASS_MODAL_DISTRIBUTION_CXX
#endif
