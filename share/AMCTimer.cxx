// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_TIMER_CXX

#include "AMCTimer.hxx"

namespace AMC
{
  // Zero duration time for CPU.
  CPU::duration_type_ CPU::Zero()
  {
    return CPU::duration_type_(0);
  }

  // Point time for CPU.
  CPU::point_type_ CPU::Point()
  {
    return clock();
  }

  // Convert the CPU duration time into floating seconds.
  double CPU::Second(const duration_type_ &duration)
  {
    return double(duration) / double(CLOCKS_PER_SEC);    
  }


  // Zero duration time for wall elapsed time.
  Wall::duration_type_ Wall::Zero()
  {
    return clock_::duration::zero();
  }

  // Point time for wall elapsed time.
  Wall::point_type_ Wall::Point()
  {
    return clock_::now();
  }

  // Convert the wall elapsed duration time into floating seconds.
  double Wall::Second(const duration_type_ &duration)
  {
    return duration.count();
  }


  // Add one record.
  template<class C>
  int AMCTimer<C>::Add(const string name)
  {
    name_.push_back(name);
    timer_.push_back(C::Zero());
    return Ntimer_++;
  }


  // Remove one or all records.
  template<class C>
  void AMCTimer<C>::Clear(int i)
  {
    if (i < 0)
      {
        name_.clear();
        timer_.clear();
        Ntimer_ = 0;
      }
    else
      {
        name_.erase(name_.begin() + i);
        timer_.erase(timer_.begin() + i);
        Ntimer_--;
      }
  }


  // Reset one or all timers.
  template<class C>
  void AMCTimer<C>::Reset(int i)
  {
    if (i < 0)
      for (i = 0; i < Ntimer_; i++)
        timer_[i] = C::Zero();
    else
      timer_[i] = C::Zero();
  }


  // Start end methods.
  template<class C>
  void AMCTimer<C>::Start()
  {
    start_= C::Point();
  }


  template<class C>
  void AMCTimer<C>::Stop(const int &i)
  {
    timer_[i] += C::Point() - start_;
  }


  // Get methods.
  template<class C>
  int AMCTimer<C>::GetNtimer()
  {
    return Ntimer_;
  }


  template<class C>
  double AMCTimer<C>::GetTimer(const int &i)
  {
    return C::Second(timer_[i]);
  }


  template<class C>
  string AMCTimer<C>::GetName(const int &i)
  {
    return name_[i];
  }


  // Display all timers.
  template<class C>
  map<string, double> AMCTimer<C>::Display(int i)
  {
    map<string, double> timer;

    if (i < 0)
      for (i = 0; i < Ntimer_; i++)
        timer[name_[i]] = AMCTimer<C>::GetTimer(i);
    else
        timer[name_[i]] = AMCTimer<C>::GetTimer(i);

    return timer;
  }
}

#define AMC_FILE_AMC_TIMER_CXX
#endif
