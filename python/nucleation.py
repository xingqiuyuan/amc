#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import numpy as _numpy
import pylab as _pylab
import os as _os
from util import to_array as _to_array


class ParameterizationNucleationBase(_core.ParameterizationNucleationBase):
    def GetSpeciesIndex(self):
        species_index = _core.vector1i()
        _core.ParameterizationNucleationBase.GetSpeciesIndex(self, species_index)
        return _numpy.array(species_index, dtype = _numpy.int)

    def GetGasIndex(self):
        gas_index = _core.vector1i()
        _core.ParameterizationNucleationBase.GetGasIndex(self, gas_index)
        return _numpy.array(gas_index, dtype = _numpy.int)


class ParameterizationNucleationVoid(_core.ParameterizationNucleationVoid, ParameterizationNucleationBase):
    pass


if _core.AMC_HAS_POWER_LAW == 1:
    class ParameterizationNucleationPowerLaw(_core.ParameterizationNucleationPowerLaw, ParameterizationNucleationBase):
        # Configuration file.
        config_file = _os.path.join(_os.path.dirname(_os.path.realpath(__file__)), "../parameterization/"
                                    + _core.ParameterizationNucleationPowerLaw.GetConfigurationFile())

        def GetSpeciesFraction(self):
            species_fraction = _core.vector1r()
            _core.ParameterizationNucleationPowerLaw.GetSpeciesFraction(self, species_fraction)
            return _numpy.array(species_fraction, dtype = _numpy.float)

        if _core.AMC_HAS_TEST == 1:
            def Test(self, **kwargs):
                N = 10
                if kwargs.has_key("N"):
                    N = kwargs["N"]

                sulfate = _numpy.logspace(_numpy.log10(_core.AMC_NUCLEATION_POWER_LAW_CONCENTRATION_SULFATE_MIN_MOLEC_CM3),
                                          _numpy.log10(_core.AMC_NUCLEATION_POWER_LAW_CONCENTRATION_SULFATE_MAX_MOLEC_CM3), N)

                nucleation_rate = _numpy.zeros(len(sulfate), dtype = _numpy.float)
                for i in range(len(sulfate)):
                    nucleation_rate[i] = self.ComputeKernel(sulfate[i])

                return sulfate, nucleation_rate


        @staticmethod
        def GetAvailableParameter(configuration_file = ""):
            name = _core.vector1s()
            K = _core.vector1r()
            logK = _core.vector1r()
            P = _core.vector1r()

            Nparam = _core.ParameterizationNucleationPowerLaw.GetAvailableParameter(name, P, K, logK, configuration_file)

            param_list = {}
            for i in range(Nparam):
                param_list[name[i]] = {}
                param_list[name[i]]["P"] = P[i]
                param_list[name[i]]["K"] = K[i]
                param_list[name[i]]["logK"] = logK[i]

            return param_list


if _core.AMC_HAS_VEHKAMAKI == 1:
    class ParameterizationNucleationVehkamaki(_core.ParameterizationNucleationVehkamaki, ParameterizationNucleationBase):
        if _core.AMC_HAS_TEST == 1:
            def Test(self, **kwargs):
                N = 100
                if kwargs.has_key("N"):
                    N = kwargs["N"]

                sulfate = _numpy.logspace(_numpy.log10(1.e6), _numpy.log10(1.e9), N)

                temperature = 236.
                if kwargs.has_key("temperature"):
                    temperature = kwargs["temperature"]
                    
                relative_humidity = 0.55
                if kwargs.has_key("relative_humidity"):
                    relative_humidity = kwargs["relative_humidity"]

                nucleation_rate = _numpy.zeros(len(sulfate), dtype = _numpy.float)
                critical_cluster_radius = _numpy.zeros(len(sulfate), dtype = _numpy.float)
                number_molecule = _numpy.zeros(len(sulfate), dtype = _numpy.float)
                sulfuric_acid_mole_fraction = _numpy.zeros(len(sulfate), dtype = _numpy.float)

                for i in range(len(sulfate)):
                    nucleation_rate[i] = self.ComputeKernel(temperature, relative_humidity, sulfate[i])
                    critical_cluster_radius[i] = self.ComputeCriticalClusterRadius(temperature, relative_humidity, sulfate[i])
                    number_molecule[i] = self.ComputeNumberMoleculeTotal(temperature, relative_humidity, sulfate[i])
                    sulfuric_acid_mole_fraction[i] = self.ComputeSulfuricAcidMoleFraction(temperature,
                                                                                          relative_humidity,
                                                                                          sulfate[i])

                return sulfate, nucleation_rate, critical_cluster_radius, number_molecule, sulfuric_acid_mole_fraction


            def TestSulfuricAcidMoleFraction(self, **kwargs):
                N = 10
                if kwargs.has_key("N"):
                    N = kwargs["N"]

                sulfate = _numpy.logspace(_numpy.log10(1.e6), _numpy.log10(1.e9), N)

                temperature = _numpy.linspace(_core.AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MIN,
                                              _core.AMC_NUCLEATION_VEHKAMAKI_TEMPERATURE_MAX, N)

                relative_humidity = _numpy.linspace(_core.AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MIN,
                                                    _core.AMC_NUCLEATION_VEHKAMAKI_RELATIVE_HUMIDITY_MAX, N)

                sulfuric_acid_mole_fraction = _numpy.zeros([N, N, N], dtype = _numpy.float)

                for i in range(N):
                    for j in range(N):
                        for k in range(N):
                            sulfuric_acid_mole_fraction[i, j, k] = self.ComputeSulfuricAcidMoleFraction(temperature[j],
                                                                                                        relative_humidity[k],
                                                                                                        sulfate[i])

                return sulfate, temperature, relative_humidity, sulfuric_acid_mole_fraction

        else:
            pass


if _core.AMC_HAS_MERIKANTO == 1:
    class ParameterizationNucleationMerikanto(_core.ParameterizationNucleationMerikanto, ParameterizationNucleationBase):
        def ComputeNumberMolecule(self,
                                  temperature,
                                  relative_humidity,
                                  concentration_sulfate_molec_cm3,
                                  concentration_ammonium_ppt):
            number_molecule = _core.vector1r()
            _core.ParameterizationNucleationMerikanto.ComputeNumberMolecule(self,
                                                                            temperature,
                                                                            relative_humidity,
                                                                            concentration_sulfate_molec_cm3,
                                                                            concentration_ammonium_ppt,
                                                                            number_molecule)
            return _numpy.array(number_molecule, dtype =_numpy.float)


        def ComputeMoleFraction(self,
                                temperature,
                                relative_humidity,
                                concentration_sulfate_molec_cm3,
                                concentration_ammonium_ppt):
            mole_fraction = _core.vector1r()
            _core.ParameterizationNucleationMerikanto.ComputeMoleFraction(self,
                                                                          temperature,
                                                                          relative_humidity,
                                                                          concentration_sulfate_molec_cm3,
                                                                          concentration_ammonium_ppt,
                                                                          mole_fraction)
            return _numpy.array(mole_fraction, dtype =_numpy.float)


        def ComputeMassFraction(self,
                                temperature,
                                relative_humidity,
                                concentration_sulfate_molec_cm3,
                                concentration_ammonium_ppt):
            mass_fraction = _core.vector1r()
            _core.ParameterizationNucleationMerikanto.ComputeMassFraction(self,
                                                                          temperature,
                                                                          relative_humidity,
                                                                          concentration_sulfate_molec_cm3,
                                                                          concentration_ammonium_ppt,
                                                                          mass_fraction)
            return _numpy.array(mass_fraction, dtype =_numpy.float)


        if _core.AMC_HAS_TEST == 1:
            def Test(self, **kwargs):
                N = 100
                if kwargs.has_key("N"):
                    N = kwargs["N"]

                temperature = (_core.AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MIN +
                               _core.AMC_NUCLEATION_MERIKANTO_TEMPERATURE_MAX) * 0.5
                if kwargs.has_key("temperature"):
                    temperature = kwargs["temperature"]

                relative_humidity = (_core.AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MIN +
                                     _core.AMC_NUCLEATION_MERIKANTO_RELATIVE_HUMIDITY_MAX) * 0.5
                if kwargs.has_key("relative_humidity"):
                    relative_humidity = kwargs["relative_humidity"]

                ammonium = _numpy.logspace(_numpy.log10(_core.AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MIN),
                                           _numpy.log10(_core.AMC_NUCLEATION_MERIKANTO_CONCENTRATION_AMMONIUM_PPT_MAX), N)
                if kwargs.has_key("ammonium"):
                    ammonium = _numpy.array([kwargs["ammonium"]], dtype = _numpy.float)

                #sulfate = _numpy.logspace(_numpy.log10(_core.AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MIN),
                #                      _numpy.log10(_core.AMC_NUCLEATION_MERIKANTO_CONCENTRATION_SULFATE_MOLEC_CM3_MAX), N)
                sulfate = _numpy.logspace(_numpy.log10(1.e5), _numpy.log10(1.e9), N)

                nucleation_rate = _numpy.zeros([len(sulfate), len(ammonium)], dtype = _numpy.float)
                critical_cluster_radius = _numpy.zeros([len(sulfate), len(ammonium)], dtype = _numpy.float)
                mole_fraction = _numpy.zeros([len(sulfate), len(ammonium), 3], dtype = _numpy.float)
                mass_fraction = _numpy.zeros([len(sulfate), len(ammonium), 3], dtype = _numpy.float)

                for i in range(len(sulfate)):
                    for j in range(len(ammonium)):
                        nucleation_rate[i, j] = self.ComputeKernel(temperature,
                                                                   relative_humidity,
                                                                   sulfate[i], ammonium[j])
                        critical_cluster_radius[i, j] = self.ComputeCriticalClusterRadius(temperature,
                                                                                          relative_humidity,
                                                                                          sulfate[i], ammonium[j])
                        mole_fraction[i, j] = self.ComputeMoleFraction(temperature,
                                                                       relative_humidity,
                                                                       sulfate[i], ammonium[j])
                        mass_fraction[i, j] = self.ComputeMassFraction(temperature,
                                                                       relative_humidity,
                                                                       sulfate[i], ammonium[j])

                return sulfate, ammonium, nucleation_rate, critical_cluster_radius, mole_fraction, mass_fraction
        

