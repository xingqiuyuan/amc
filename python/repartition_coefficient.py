#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as _core

import sys as _sys
import numpy as _numpy
import pylab as _pylab
from util import to_array as _to_array

if _core.AMC_HAS_LAYER > 0:
    from amc.base import Layer as _Layer


class PDF(_core.PDF):

    def GetX(self):
        return _numpy.array(_core.PDF.GetX(self), dtype = _numpy.float)

    def GetCX(self):
        return _numpy.array(_core.PDF.GetCX(self), dtype = _numpy.float)

    @staticmethod
    def GetPdfName():
        return list(_core.PDF.GetPdfName())

    @staticmethod
    def GetCaseName():
        return _numpy.array(_core.PDF.GetCaseName(), dtype = _numpy.int)

    @staticmethod
    def GetStepName():
        return list(_core.PDF.GetStepName())

    @staticmethod
    def GetCasePdfIndex(i):
        return _numpy.array(_core.PDF.GetCasePdfIndex(i), dtype = _numpy.int)

    @staticmethod
    def GetCaseStepIndex(i):
        return _numpy.array(_core.PDF.GetCaseStepIndex(i), dtype = _numpy.int)


    # Compute the PDF.
    def Compute(self, **kwargs):
        x = _core.vector1r()
        p = _core.vector1r()

        i = kwargs["i"]

        if kwargs.has_key("fi"):
            fi = kwargs["fi"]
        else:
            fi = 0.5

        if kwargs.has_key("j"):
            j = kwargs["j"]
        else:
            j = i

        if kwargs.has_key("fj"):
            fj = kwargs["fj"]
        else:
            fj = fi

        if kwargs.has_key("test"):
            test = kwargs["test"]
        else:
            test = False

        icase = _core.PDF.Compute(self, i, j, fi, fj, x, p, test)
        return icase, _numpy.array(x, dtype = _numpy.float), _numpy.array(p, dtype = _numpy.float)


    # Integrate the PDF.
    def Integrate(self, **kwargs):
        f = _core.vector1r()
        g = _core.vector1r()

        i = kwargs["i"]

        if kwargs.has_key("fi"):
            fi = kwargs["fi"]
        else:
            fi = 0.5

        if kwargs.has_key("j"):
            j = kwargs["j"]
        else:
            j = i

        if kwargs.has_key("fj"):
            fj = kwargs["fj"]
        else:
            fj = fi

        _core.PDF.Integrate(self, i, j, fi, fj, f, g)
        return _numpy.array(f, dtype = _numpy.float), _numpy.array(g, dtype = _numpy.float)


    # Plot the PDF.
    def Plot(self, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        if kwargs.has_key("fmt"):
            fmt = kwargs["fmt"]
        else:
            fmt = "k-o"

        if kwargs.has_key("lw"):
            lw = kwargs["lw"]
        else:
            lw = 2

        i = kwargs["i"]

        if kwargs.has_key("fi"):
            fi = kwargs["fi"]
        else:
            fi = 0.5

        if kwargs.has_key("j"):
            j = kwargs["j"]
        else:
            j = i

        if kwargs.has_key("fj"):
            fj = kwargs["fj"]
        else:
            fj = fi

        if kwargs.has_key("test"):
            test = kwargs["test"]
        else:
            test = False

        if kwargs.has_key("label"):
            label = kwargs["label"]
        else:
            if i == j and fi == fj and not test:
                label = "i = %d, fi = %2.1f" % (i, fi)
            else:
                label = "(i, j) = (%d, %d), (fi, fj) = (%2.1f, %2.1f)" % (i, j, fi, fj)

        icase, x, p = self.Compute(**kwargs)

        step_name = PDF.GetStepName()
        slabel = [step_name[k] for k in PDF.GetCaseStepIndex(icase)]
        if i == j and fi == fj and not test:
            slabel = [s.replace("j", "i") for s in slabel]

        x2 = _numpy.array([[x[i], x[i]] for i in range(len(x))]).flatten()[1:-1]
        ax.plot(x2, p, fmt, lw = lw, label = label)

        for i in range(len(x)):
            ax.axvline(x[i], lw = 1, ls = "--", color = "k")

        ax.set_xticks(x)
        ax.set_xticklabels(['%1.2g\n$%s$' % (y, s) for y, s in zip(x, slabel)])

        if kwargs.has_key("legend"):
            if isinstance(kwargs["legend"], int):
                ax.legend(loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


if _core.AMC_HAS_TRACE > 0:
    class CoefficientRepartitionCoagulationTrace(_core.CoefficientRepartitionCoagulationTrace):
        pass


if _core.AMC_HAS_LAYER > 0:
    class CoefficientRepartitionCoagulationLayer(_core.CoefficientRepartitionCoagulationLayer):
        @staticmethod
        def ComputeLayerRepartitionCouple(concentration_number, concentration_mass):
            layer_repartition_couple = _core.vector2r()
            _core.CoefficientRepartitionCoagulationLayer.ComputeLayerRepartitionCouple(concentration_number,
                                                                                       concentration_mass,
                                                                                       layer_repartition_couple)
            return _to_array(layer_repartition_couple)


    class Ring(_core.Ring):

        def ComputeCrossAngle(self, distance, ring, in_degree = False):
            angle = _core.vector1r()
            _core.Ring.ComputeCrossAngle(self, distance, ring, angle, in_degree)
            return {"min_min" : angle[_core.RING_INDEX_MIN_MIN], "min_max" : angle[_core.RING_INDEX_MIN_MAX], 
                    "max_min" : angle[_core.RING_INDEX_MAX_MIN], "max_max" : angle[_core.RING_INDEX_MAX_MAX]}

        def ComputeCrossDistance(self, distance, ring):
            cross_distance = _core.vector1r()
            _core.Ring.ComputeCrossDistance(self, distance, ring, cross_distance)
            return {"min_min" : cross_distance[_core.RING_INDEX_MIN_MIN], "min_max" : cross_distance[_core.RING_INDEX_MIN_MAX], 
                    "max_min" : cross_distance[_core.RING_INDEX_MAX_MIN], "max_max" : cross_distance[_core.RING_INDEX_MAX_MAX]}

        def ComputeCrossCase(self, distance, ring):
            integral_case = _core.vector1i()
            integral_radius = _core.vector1r()
            case_integral = _core.Ring.ComputeCrossCase(self, distance, ring, integral_case, integral_radius)

            str_case = ["0_pi", "0_max", "min_pi", "min_max"]

            return case_integral, [str_case[i] for i in _numpy.array(integral_case, dtype = _numpy.int)], \
                _numpy.array(integral_radius, dtype = _numpy.float)

        def PlotCrossVolume(self, distance, ring, **kwargs):
            if kwargs.has_key("width"):
                width = kwargs["width"]
            else:
                width = 30

            _pylab.matplotlib.rcParams["figure.figsize"] = (width, width)

            margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
            if kwargs.has_key("margins"):
                for k,v in kwargs["margins"].iteritems():
                    margins[k] = v

            _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
            _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
            _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
            _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

            if kwargs.has_key("font_size"):
                font_size = kwargs["font_size"]
            else:
                font_size = 12

            _pylab.matplotlib.rcParams["font.size"] = font_size
            _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
            _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
            _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
            _pylab.matplotlib.rcParams["xtick.major.size"] = 2
            _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
            _pylab.matplotlib.rcParams["ytick.major.size"] = 2
            _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)

            _pylab.axes().set_aspect("equal")

            bhi = _pylab.Circle((0., 0.), self.GetRadiusMax(), fill = False,
                                lw = 2, color = 'r', clip_on = True, zorder = 9)
            blo = _pylab.Circle((0., 0.), self.GetRadiusMin(), fill = False,
                                lw = 2, color = 'r', clip_on = True, zorder = 10)
            ahi = _pylab.Circle((distance, 0.), ring.GetRadiusMax(), fill = False,
                                lw = 2, color = 'b', clip_on = True, zorder = 11)
            alo = _pylab.Circle((distance, 0.), ring.GetRadiusMin(), fill = False,
                                lw = 2, color = 'b', clip_on = True, zorder = 12)

            ax.add_artist(blo)
            ax.add_artist(bhi)
            ax.add_artist(alo)
            ax.add_artist(ahi)

            ax.axhline(linewidth = 1.1, color = "k", zorder = 13)
            ax.axvline(linewidth = 1.1, color = "k", zorder = 13)
            ax.axvline(x = distance, linewidth = 1.1, color = "k", zorder = 13)

            ax.axvline(x = - self.GetRadiusMin(), linewidth = 1.1, linestyle = "--", color = "k", zorder = 13)
            ax.axvline(x = - self.GetRadiusMax(), linewidth = 1.1, linestyle = "--", color = "k", zorder = 13)
            ax.axvline(x =   self.GetRadiusMin(), linewidth = 1.1, linestyle = "--", color = "k", zorder = 13)
            ax.axvline(x =   self.GetRadiusMax(), linewidth = 1.1, linestyle = "--", color = "k", zorder = 13)
            ax.axvline(x = distance - ring.GetRadiusMin(), linewidth = 1.1, linestyle = "-.", color = "k", zorder = 13)
            ax.axvline(x = distance - ring.GetRadiusMax(), linewidth = 1.1, linestyle = "-.", color = "k", zorder = 13)
            ax.axvline(x = distance + ring.GetRadiusMin(), linewidth = 1.1, linestyle = "-.", color = "k", zorder = 13)
            ax.axvline(x = distance + ring.GetRadiusMax(), linewidth = 1.1, linestyle = "-.", color = "k", zorder = 13)

            cross_angle = _core.vector1r()
            _core.Ring.ComputeCrossAngle(self, distance, ring, cross_angle)

            if cross_angle[0] > 0. and cross_angle[0] < _core.RING_PI:
                    ax.plot([distance, distance + _numpy.cos(cross_angle[0]) * ring.GetRadiusMin() * 1.5],
                            [0, _numpy.sin(cross_angle[0]) * ring.GetRadiusMin() * 1.5], "k-", lw = 1.2)
            if cross_angle[1] > 0. and cross_angle[1] < _core.RING_PI:
                    ax.plot([distance, distance + _numpy.cos(cross_angle[1]) * ring.GetRadiusMin() * 1.5],
                            [0, _numpy.sin(cross_angle[1]) * ring.GetRadiusMin() * 1.5], "k-", lw = 1.2)
            if cross_angle[2] > 0. and cross_angle[2] < _core.RING_PI:
                ax.plot([distance, distance + _numpy.cos(cross_angle[2]) * ring.GetRadiusMax() * 1.5],
                        [0, _numpy.sin(cross_angle[2]) * ring.GetRadiusMax() * 1.5], "k-", lw = 1.2)
            if cross_angle[3] > 0. and cross_angle[3] < _core.RING_PI:
                ax.plot([distance, distance + _numpy.cos(cross_angle[3]) * ring.GetRadiusMax() * 1.5],
                        [0, _numpy.sin(cross_angle[3]) * ring.GetRadiusMax() * 1.5], "k-", lw = 1.2)

            width = max(self.GetRadiusMax(), distance + ring.GetRadiusMax()) * 1.1
            ax.set_xlim(-width, width)
            ax.set_ylim(-width, width)

            if kwargs.has_key("output_file"):
                _pylab.savefig(kwargs["output_file"])
                _pylab.clf()
            else:
                _pylab.show()


        if _core.AMC_HAS_TEST > 0:
            @staticmethod
            def GetCaseCounterTest():
                case_counter_test = _core.vector1i()
                _core.Ring.GetCaseCounterTest(case_counter_test)
                return _numpy.array(case_counter_test, dtype = _numpy.int)

            def ComputeCrossVolumeTest(self, d, a):
                cross_angle = self.ComputeCrossAngle(d, a, True)
                cross_distance = self.ComputeCrossDistance(d, a)
                case_integral, integral_case, integral_radius = self.ComputeCrossCase(d, a)
                cross_volume = self.ComputeCrossVolume(d, a)
                _sys.stdout.write(("      self ring = (%f, %f)\n" % (self.GetRadiusMin(), self.GetRadiusMax())) + 
                                  ("       distance = %f\n" % d) +
                                  ("  argument ring = (%f, %f)\n" % (a.GetRadiusMin(), a.GetRadiusMax())) +
                                  ("    cross_angle = %s\n" % str(cross_angle)) +
                                  (" cross_distance = %s\n" % str(cross_distance)) +
                                  ("  case_integral = %d\n" % case_integral) +
                                  ("  integral_case = %s\n" % str(integral_case)) +
                                  ("integral_radius = %s\n" % str(integral_radius)) +
                                  ("   cross_volume = %s\n" % str(cross_volume)))


class CoefficientRepartitionCoagulationBase(_core.CoefficientRepartitionCoagulationBase):

    if _core.AMC_HAS_LAYER > 0:
        def GetCoefficientLayer(self):
            coefficient_vect = _core.vector2r()
            _core.CoefficientRepartitionCoagulationLayer.GetCoefficientLayer(self, coefficient_vect)
            coefficient_vect = _to_array(coefficient_vect)
            h = 0
            coefficient = []
            for b in range(_core.AerosolData.GetNg()):
                coefficient.append([])
                for a in range(b + 1):
                    coefficient_vect[h].shape = (_Layer.GetNlayer()[a], _Layer.GetNlayer()[b])
                    coefficient[b].append(coefficient_vect[h])
                    h += 1
            return coefficient

    if _core.AMC_HAS_TRACE > 0:
        def GetCoefficientIndexTrace(self):
            index = _core.vector1i()
            _core.CoefficientRepartitionCoagulationBase.GetCoefficientIndexTrace(self, index)
            return _numpy.array(index, dtype = _numpy.int)

    def GetNcoefficient(self):
        Ncoefficient = _core.vector1i()
        _core.CoefficientRepartitionCoagulationBase.GetNcoefficient(self, Ncoefficient)
        return _numpy.array(Ncoefficient, dtype = _numpy.int)


    def GetCoefficientIndex(self):
        index = _core.vector2i()
        _core.CoefficientRepartitionCoagulationBase.GetCoefficientIndex(self, index)
        return [_numpy.array(index[i], dtype = _numpy.int) for i in range(self.GetNcouple())]


    def GetCoefficientIndexReverse(self):
        index1 = _core.vector2i()
        index2 = _core.vector2i()
        N = _core.CoefficientRepartitionCoagulationBase.GetCoefficientIndexReverse(self, index1, index2)

        index = [[] for i in range(N)]
        for i in range(N):
            for j in range(len(index1[i])):
                index[i].append([index1[i][j], index2[i][j]])
        return index


    def GetCoefficientNumber(self):
        coefficient = _core.vector2r()
        _core.CoefficientRepartitionCoagulationBase.GetCoefficientNumber(self, coefficient)
        return [_numpy.array(coefficient[i], dtype = _numpy.float) for i in range(self.GetNcouple())]


    def GetCoefficientMass(self, i):
        coefficient = _core.vector2r()
        _core.CoefficientRepartitionCoagulationBase.GetCoefficientMass(self, i, coefficient)
        return [_numpy.array(coefficient[i], dtype = _numpy.float) for i in range(self.GetNcouple())]


class CoefficientRepartitionCoagulationSize(_core.CoefficientRepartitionCoagulationSize):

    def GetCoefficientNumber(self):
        coefficient = _core.vector2r()
        _core.CoefficientRepartitionCoagulationSize.GetCoefficientNumber(self, coefficient)
        return [_numpy.array(coefficient[i], dtype = _numpy.float) for i in range(self.GetNcouple())]


    def GetCoefficientMass(self):
        coefficient = _core.vector2r()
        _core.CoefficientRepartitionCoagulationSize.GetCoefficientMass(self, coefficient)
        return [_numpy.array(coefficient[i], dtype = _numpy.float) for i in range(self.GetNcouple())]


    def ComputeCoefficient(self, x):
        x_vect = _core.vector1r(x)
        _core.CoefficientRepartitionCoagulationSize.ComputeCoefficient(self, x_vect)


class CoefficientRepartitionCoagulationStaticBase(CoefficientRepartitionCoagulationBase,
                                                  _core.CoefficientRepartitionCoagulationStaticBase):
    pass


class CoefficientRepartitionCoagulationStaticTable(CoefficientRepartitionCoagulationBase,
                                                   _core.CoefficientRepartitionCoagulationStaticTable):
    pass


class CoefficientRepartitionCoagulationMovingBase(CoefficientRepartitionCoagulationBase,
                                                  _core.CoefficientRepartitionCoagulationMovingBase):
    pass


class CoefficientRepartitionCoagulationMovingTable(CoefficientRepartitionCoagulationBase,
                                                   _core.CoefficientRepartitionCoagulationMovingTable):
    pass
