#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as _core

from discretization import DiscretizationSize as _DiscretizationSize
from species import Species as _Species
from species import SpeciesEquilibrium as _SpeciesEquilibrium
from util import to_array as _to_array
import numpy as _numpy
import pylab as _pylab
import copy as _copy
import random as _random
from matplotlib.colors import LogNorm as _LogNorm
from matplotlib.colors import ListedColormap as _ListedColormap
import os as _os
import sys as _sys
import re as _re
from inspect import currentframe as _cf



class Configuration(_core.Configuration):
    pass

    class init_flag(_core.init_flag):
        pass


class AerosolData(_core.AerosolData):

    @staticmethod
    def GetGeneralSection():
        general_section = _core.vector2i()
        _core.AerosolData.GetGeneralSection(general_section)
        return _to_array(general_section)

    @staticmethod
    def GetGeneralSectionReverse():
        general_section_reverse = _core.vector2i()
        _core.AerosolData.GetGeneralSectionReverse(general_section_reverse)
        return _to_array(general_section_reverse)

    @staticmethod
    def GetDiameter():
        diameter = _core.vector1r()
        _core.AerosolData.GetDiameter(diameter)
        return _numpy.array(diameter, dtype = _numpy.float)

    @staticmethod
    def GetMass():
        mass = _core.vector1r()
        _core.AerosolData.GetMass(mass)
        return _numpy.array(mass, dtype = _numpy.float)

    @staticmethod
    def GetDensity():
        density = _core.vector1r()
        _core.AerosolData.GetDensity(density)
        return _numpy.array(density, dtype = _numpy.float)

    @staticmethod
    def GetTemperature():
        temperature = _core.vector1r()
        _core.AerosolData.GetTemperature(temperature)
        return _numpy.array(temperature, dtype = _numpy.float)

    @staticmethod
    def GetEquilibriumGasSurface():
        equilibrium_gas_surface = _core.vector1r()
        _core.AerosolData.GetEquilibriumGasSurface(equilibrium_gas_surface)
        equilibrium_gas_surface = _numpy.array(equilibrium_gas_surface, dtype = _numpy.float)
        equilibrium_gas_surface.shape = [AerosolData.GetNg(), _Species.GetNgas()]
        return equilibrium_gas_surface

    @staticmethod
    def GetEquilibriumAerosolInternal():
        equilibrium_aer_internal = _core.vector1r()
        _core.AerosolData.GetEquilibriumAerosolInternal(equilibrium_aer_internal)
        equilibrium_aer_internal = _numpy.array(equilibrium_aer_internal, dtype = _numpy.float)
        equilibrium_aer_internal.shape = [AerosolData.GetNg(), _SpeciesEquilibrium.GetNspecies()]
        return equilibrium_aer_internal

    @staticmethod
    def GetLiquidWaterContent():
        liquid_water_content = _core.vector1r()
        _core.AerosolData.GetLiquidWaterContent(liquid_water_content)
        return _numpy.array(liquid_water_content, dtype = _numpy.float)

    @staticmethod
    def GetSurfaceTension():
        surface_tension = _core.vector1r()
        _core.AerosolData.GetSurfaceTension(surface_tension)
        surface_tension = _numpy.array(surface_tension, dtype = _numpy.float)
        surface_tension.shape = (AerosolData.GetNg(), _core.Phase.GetNphase())
        return surface_tension

    if _core.AMC_HAS_LAYER > 0:
        @staticmethod
        def GetLayerRepartition():
            layer_repartition = _core.vector2r()
            _core.AerosolData.GetLayerRepartition(layer_repartition)
            return _to_array(layer_repartition)
        
        @staticmethod
        def GetLayerRadius():
            layer_radius = _core.vector2r()
            _core.AerosolData.GetLayerRadius(layer_radius)
            return _to_array(layer_radius)

        @staticmethod
        def ComputeConcentrationLayer(concentration_aer_mass):
            concentration_aer_mass_layer = _core.vector2r()
            _core.AerosolData.ComputeConcentrationLayer(concentration_aer_mass,
                                                        concentration_aer_mass_layer)
            return _to_array(concentration_aer_mass_layer)

    @staticmethod
    def ComputeMassConservation(concentration_gas, concentration_aer_mass):
        mass_conservation = _core.vector1r()
        _core.AerosolData.ComputeMassConservation(concentration_gas, concentration_aer_mass, mass_conservation)
        return _numpy.array(mass_conservation, dtype = _numpy.float)

    @staticmethod
    def ComputeConcentrationAerosolMassTotal(concentration_aer_mass):
        concentration_aer_mass_total = _core.vector1r()
        _core.AMC_Base.ComputeConcentrationAerosolMassTotal(concentration_aer_mass,
                                                            concentration_aer_mass_total)
        return _numpy.array(concentration_aer_mass_total, dtype = _numpy.float)

    @staticmethod
    def ComputeParticleComposition(concentration_aer_number, concentration_aer_mass,
                                   section_min = 0, section_max = -1):
        if section_max < 0:
            section_max = AerosolData.GetNg()

        composition = _core.vector2r()
        _core.AerosolData.ComputeParticleComposition(section_min, section_max,
                                                     concentration_aer_number,
                                                     concentration_aer_mass,
                                                     composition)

        return [_numpy.array(composition[i], dtype = _numpy.float) for i in range(AerosolData.GetNg())]


class MeteorologicalData(_core.MeteorologicalData):

    @staticmethod
    def GetVariable0d(name = None):
        if name == None:
            return dict(_core.MeteorologicalData.GetVariable0d())
        else:
            return _core.MeteorologicalData.GetVariable0d(name)

    @staticmethod
    def GetVariable1d(name = None, i = None):
        if name == None:
            variable = {}
            for name in GetVariable1dList():
                variable[name] = GetVariable1d(name)
            return variable
        else:
            if i == None:
                return _numpy.array(_core.MeteorologicalData.GetVariable1d(name))
            else:
                return _core.MeteorologicalData.GetVariable1d(name, i)

    @staticmethod
    def GetVariable0dUnitList():
        return dict(_core.MeteorologicalData.GetVariable0dUnitList())

    @staticmethod
    def GetVariable1dUnitList():
        return dict(_core.MeteorologicalData.GetVariable1dUnitList())

    @staticmethod
    def GetVariable0dList():
        return list(_core.MeteorologicalData.GetVariable0dList())

    @staticmethod
    def GetVariable1dList():
        return list(_core.MeteorologicalData.GetVariable1dList())

    @staticmethod
    def UpdateMeteo(**kwargs):
        _core.MeteorologicalData.UpdateMeteo(_core.map_string_real(kwargs))

    @staticmethod
    def GetSortedSaturationVaporConcentration():
        index_ascending = _core.vector1i()
        saturation_vapor_concentration = _core.vector1r()
        _core.MeteorologicalData_GetSortedSaturationVaporConcentration(index_ascending, saturation_vapor_concentration)
        return _numpy.array(index_ascending, dtype = _numpy.int), \
            _numpy.array(saturation_vapor_concentration, dtype = _numpy.float)


if _core.AMC_HAS_LAYER > 0:
    class Layer(_core.Layer):

        @staticmethod
        def GetNlayer():
            Nlayer = _core.vector1i()
            _core.Layer.GetNlayer(Nlayer)
            return _numpy.array(Nlayer, dtype = _numpy.int)

        @staticmethod
        def GetSpeciesIndexLayer():
            species_index_layer = _core.vector1i()
            _core.Layer.GetSpeciesIndexLayer(species_index_layer)
            return _numpy.array(species_index_layer, dtype = _numpy.int)

        @staticmethod
        def IsSpeciesLayered(raw = False):
            is_species_layered = _core.vector1b()
            _core.Layer.IsSpeciesLayered(is_species_layered)
            if raw:
                return _numpy.array(is_species_layered, dtype = _numpy.bool)
            else:
                return {k : v for k, v in zip([_core.Species.GetName(i) for i in range(_core.Species.GetNspecies())],
                                              ["yes" if is_species_layered[i] else "no" for i in
                                               range(_core.Species.GetNspecies())])}

        @staticmethod
        def GetLayerRadiusFraction():
            layer_radius_fraction = _core.vector2r()
            _core.Layer.GetLayerRadiusFraction(layer_radius_fraction)
            return _to_array(layer_radius_fraction)


class Thermodynamics(_core.Thermodynamics):

    @staticmethod
    def Compute(concentration_aer_number,
                concentration_aer_mass,
                section_min = 0, section_max = 0):
        if section_max <= 0:
            section_max += AerosolData.GetNg()
        _core.Thermodynamics.Compute(section_min, section_max,
                                     concentration_aer_number,
                                     concentration_aer_mass)


class Parameterization(_core.Parameterization):

    @staticmethod
    def ComputeParticleDiameter(concentration_aer_number, concentration_aer_mass,
                                section_min = 0, section_max = -1):
        if section_max < 0:
            section_max = AerosolData.GetNg()
        _core.Parameterization.ComputeParticleDiameter(section_min, section_max,
                                                       concentration_aer_number,
                                                       concentration_aer_mass)


class AMC(_core.AMC_Base):

    @staticmethod
    def ComputeConcentrationIM(concentration_aer_num, concentration_aer_mass):
        Nsection = _DiscretizationSize.GetNsection()
        Nspecies = _Species.GetNspecies()
        concentration_aer_num_im = _numpy.zeros(Nsection, dtype = _numpy.float)
        concentration_aer_mass_im = _numpy.zeros([Nsection, Nspecies], dtype = _numpy.float)

        i = 0
        for j in range(Nsection):
            for k in range(AMC.GetNclass(j)):
                concentration_aer_num_im[j] += concentration_aer_num[i]
                i += 1

        i = 0
        for j in range(Nsection):
            for k in range(AMC.GetNclass(j)):
                for l in range(Nspecies):
                    concentration_aer_mass_im[j, l] += concentration_aer_mass[i]
                    i += 1

        return concentration_aer_num_im, concentration_aer_mass_im


    @staticmethod
    def ComputeExternalMixingDegree(concentration_aer_number, concentration_aer_mass):
        degree_num = _core.vector1r()
        degree_mass = _core.vector2r()
        _core.AMC_Base.ComputeExternalMixingDegree(concentration_aer_number,
                                                   concentration_aer_mass,
                                                   degree_num, degree_mass)
        return _numpy.array(degree_num, dtype = _numpy.float), \
            _numpy.array([_numpy.array(degree_mass[i], dtype = _numpy.float)
                          for i in range(_DiscretizationSize.GetNsection())])


    @staticmethod
    def GetConcentrationAerPretty(concentration_aer_number, concentration_aer_mass):

        concentration_aer_num_pretty = []
        concentration_aer_mass_pretty = []

        i = 0
        j = 0
        for k in range(_DiscretizationSize.GetNsection()):
            conc_num = _numpy.zeros(_core.DiscretizationClassBase.GetNclass(k), dtype = _numpy.float)
            conc_mass = _numpy.zeros((_core.DiscretizationClassBase.GetNclass(k),
                                      _Species.GetNspecies()), dtype = _numpy.float)

            for l in range(_core.DiscretizationClassBase.GetNclass(k)):
                conc_num[l] = concentration_aer_number[i]
                i += 1

                for h in range(_Species.GetNspecies()):
                    conc_mass[l, h] = concentration_aer_mass[j]
                    j += 1

            concentration_aer_num_pretty.append(conc_num)
            concentration_aer_mass_pretty.append(conc_mass)

        same_class_number = True
        Nclass0 = _core.DiscretizationClassBase.GetNclass(0)
        for k in range(1, _DiscretizationSize.GetNsection()):
            if Nclass0 != _core.DiscretizationClassBase.GetNclass(k):
                same_class_number = False
                break

        if same_class_number:
            concentration_aer_num_pretty = _numpy.array(concentration_aer_num_pretty)
            concentration_aer_mass_pretty = _numpy.array(concentration_aer_mass_pretty)

        return concentration_aer_num_pretty, concentration_aer_mass_pretty


    @staticmethod
    def GetConcentrationGasPretty(concentration_gas):
        return [concentration_gas[i] for i in range(_Species.GetNgas())]


    if _core.AMC_HAS_MODAL > 0:
        @staticmethod
        def GenerateModalDistribution(modal_distribution, class_distribution):

            concentration_aer_number = _core.ArrayReal(AerosolData.GetNg())
            concentration_aer_mass = _core.ArrayReal(AerosolData.GetNgNspecies())
            diameter_mean = _core.vector1r()

            if class_distribution == None:
                class_distribution_vect = _core.vector2r()
            else:
                class_distribution_vect = _core.vector2r(class_distribution)

            _core.AMC_Base.GenerateModalDistribution(modal_distribution,
                                                     concentration_aer_number,
                                                     concentration_aer_mass,
                                                     diameter_mean,
                                                     class_distribution_vect)

            if class_distribution == None:
                return concentration_aer_number, concentration_aer_mass, diameter_mean, \
                    _to_array(class_distribution_vect)
            else:
                return concentration_aer_number, concentration_aer_mass, diameter_mean


        @staticmethod
        def GenerateModalDistributionRandom(modal_distribution):
            return AMC.GenerateModalDistribution(modal_distribution, None)


    @staticmethod
    def PlotNumberConcentration(concentration_aer, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        if kwargs.has_key("line_width"):
            line_width = kwargs["line_width"]
        else:
            line_width = 1

        # The width of the bars
        if kwargs.has_key("bar_width"):
            bar_width = kwargs["bar_width"]
        else:
            bar_width = 0.8

        if kwargs.has_key("bar_offset"):
            bar_offset = kwargs["bar_offset"]
        else:
            bar_offset = 0.2

        do_percentage = kwargs.has_key("percentage")

        # Data.
        data = _copy.copy(concentration_aer)

        if do_percentage:
            data = [ls / ls.sum() * 100. for ls in data]

        if kwargs.has_key("threshold"):
            threshold = kwargs["threshold"]
        else:
            threshold = _core.AMC_NUMBER_CONCENTRATION_MINIMUM

        # Make bar plot.
        ind = bar_offset

        ymax = 0.
        for i in range(_DiscretizationSize.GetNsection()):
            if concentration_aer[i].max() > threshold:
                discretization_composition_ptr = _core.DiscretizationClassBase.GetDiscretizationComposition(i)

                yoff = 0.
                for j in range(AMC.GetNclass(i)):
                    hatch = discretization_composition_ptr.GetClassInternalMixing(j).GetHatch()
                    label = discretization_composition_ptr.GetClassInternalMixing(j).GetLabel()
                    color = discretization_composition_ptr.GetClassInternalMixing(j).GetColor()

                    ax.bar(ind, data[i][j], width = bar_width,
                           linewidth = line_width, bottom = yoff,
                           color = color, hatch = hatch, label = label)

                    yoff += data[i][j]

                if ymax < yoff:
                    ymax = yoff

            ind += bar_width + bar_offset

        # X-axis.
        loc = [(i + 1) * bar_offset + (i + 0.5) * bar_width for i in range(_DiscretizationSize.GetNsection())]
        label = ["%1.2g" % x for x in _DiscretizationSize.GetDiameterMean()]

        ax.set_xticks(loc)
        ax.set_xticklabels(label)
        ax.set_xlabel(ur"diameter $(d_p)$ [$\mu m$]")
        ax.set_xlim(0, ind)

        # Y-axis
        if do_percentage:
            ax.set_ylim(0, 100.)
        else:
            if kwargs.has_key("ymax"):
                ymax = kwargs["ymax"]
            ax.set_ylim(0., ymax * 1.1)

        if do_percentage:
            ax.set_ylabel(ur"number percentage")
        else:
            ax.set_ylabel(ur"number concentration  $[\#.m^{-3}]$")

        if kwargs.has_key("legend"):
            handles, labels = ax.get_legend_handles_labels()
            handles2 = []
            labels2 = []
            for h, l in zip(handles, labels):
                is_duplicate = False

                for l2 in labels2:
                    is_duplicate = l == l2
                    if is_duplicate:
                        break

                if not is_duplicate:
                    handles2.append(h)
                    labels2.append(l)

            if isinstance(kwargs["legend"], int):
                ax.legend(handles2[::-1], labels2[::-1], loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(handles2[::-1], labels2[::-1], bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotMassConcentration(concentration_aer, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        # the width of the bars
        if kwargs.has_key("line_width"):
            line_width = kwargs["line_width"]
        else:
            line_width = 1

        if kwargs.has_key("bar_width"):
            bar_width = kwargs["bar_width"]
        else:
            bar_width = 0.8

        if kwargs.has_key("bar_offset"):
            bar_offset = kwargs["bar_offset"]
        else:
            bar_offset = 0.2

        # Type of plot.
        if kwargs.has_key("percentage"):
            do_percentage = True
        else:
            do_percentage = False

        if kwargs.has_key("species"):
            species_index = kwargs["species"]
        else:
            species_index = -1

        # Data.
        if species_index < 0:
            data_species_percentage = [_numpy.transpose(tab) / tab.sum(1) for tab in concentration_aer]
            data = [tab.sum(1) for tab in concentration_aer]
        else:
            data = [tab[:, species_index] for tab in concentration_aer]

        if do_percentage:
            data = [ls / ls.sum() * 100. for ls in data]

        if kwargs.has_key("threshold"):
            threshold = kwargs["threshold"]
        else:
            threshold = _core.AMC_MASS_CONCENTRATION_MINIMUM

        # Make bar plot.
        ind = bar_offset

        ymax = 0.
        for i in range(_DiscretizationSize.GetNsection()):
            if concentration_aer[i].max() > threshold:
                discretization_composition_ptr = _core.DiscretizationClassBase.GetDiscretizationComposition(i)

                yoff = 0.
                for j in range(AMC.GetNclass(i)):
                    hatch = discretization_composition_ptr.GetClassInternalMixing(j).GetHatch()
                    label = discretization_composition_ptr.GetClassInternalMixing(j).GetLabel()

                    if species_index >= 0:
                        color = discretization_composition_ptr.GetClassInternalMixing(j).GetColor()
                    else:
                        color = "#FFFFFF"

                    ax.bar(ind, data[i][j], width = bar_width,
                           linewidth = line_width, bottom = yoff,
                           color = color, hatch = hatch, label = label)

                    yoff += data[i][j]

                if ymax < yoff:
                    ymax = yoff

                if species_index < 0:
                    yoff = 0.

                    for j in range(AMC.GetNclass(i)):
                        hatch = discretization_composition_ptr.GetClassInternalMixing(j).GetHatch()

                        ind_species = ind

                        for k in range(_Species.GetNspecies()):
                            color = _Species.GetColor(k)
                            #hatch = _Species.GetHatch(k)
                            label = _Species.GetLabel(k)

                            width_species = data_species_percentage[i][k, j] * bar_width

                            ax.bar(ind_species, data[i][j], width = width_species,
                                   linewidth = 0, bottom = yoff, color = color, hatch = hatch)

                            ax.bar(0, 0, width = 0, linewidth = 0, bottom = 0, 
                                   color = color, label = label)

                            ind_species += width_species

                        yoff += data[i][j]

            ind += bar_width + bar_offset

        # X-axis.
        loc = [(i + 1) * bar_offset + (i + 0.5) * bar_width for i in range(_DiscretizationSize.GetNsection())]
        label = ["%1.2g" % x for x in _DiscretizationSize.GetDiameterMean()]

        ax.set_xticks(loc)
        ax.set_xticklabels(label)
        ax.set_xlabel(ur"diameter $(d_p)$ [$\mu m$]")
        ax.set_xlim(0, ind)

        # Y-axis
        if do_percentage:
            ax.set_ylim(0, 100.)
        else:
            if kwargs.has_key("ymax"):
                ymax = kwargs["ymax"]
            ax.set_ylim(0., ymax * 1.1)

        if do_percentage:
            ax.set_ylabel(ur"mass percentage")
        else:
            if species_index < 0:
                ax.set_ylabel(ur"mass concentration  $[\mu g.m^{-3}]$")
            else:
                ax.set_ylabel(_Species.GetName(species_index) + ur" mass concentration  $[\mu g.m^{-3}]$")

        if kwargs.has_key("legend"):
            handles, labels = ax.get_legend_handles_labels()
            handles2 = []
            labels2 = []
            for h, l in zip(handles, labels):
                is_duplicate = False

                for l2 in labels2:
                    is_duplicate = l == l2
                    if is_duplicate:
                        break

                if not is_duplicate:
                    handles2.append(h)
                    labels2.append(l)

            if isinstance(kwargs["legend"], int):
                ax.legend(handles2[::-1], labels2[::-1], loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(handles2[::-1], labels2[::-1], bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotConcentration(concentration_aer_num, concentration_aer_mass, **kwargs):

        concentration_aer_num_pretty, concentration_aer_mass_pretty = \
            AMC.GetConcentrationAerPretty(concentration_aer_num, concentration_aer_mass)

        if kwargs.has_key("legend"):
            kwargs.pop("legend")

        if kwargs.has_key("threshold"):
            kwargs.pop("threshold")

        output_file = None
        if kwargs.has_key("output_file"):
            output_file = kwargs.pop("output_file")

        show = False
        if kwargs.has_key("show"):
            show = kwargs.pop("show")

        fig = AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                                          title = u"Number distribution",
                                          subplot = 221, **kwargs)

        if  kwargs.has_key("legend_number"):
            kwargs["legend"] = kwargs["legend_number"]

        if  kwargs.has_key("threshold_number"):
            kwargs["threshold"] = kwargs["threshold_number"]

        fig = AMC.PlotNumberConcentration(concentration_aer_num_pretty,
                                          fig = fig, subplot = 222,
                                          title = u"Percentage number distribution",
                                          percentage = True, **kwargs)

        if kwargs.has_key("legend"):
            kwargs.pop("legend")

        if  kwargs.has_key("threshold"):
            kwargs.pop("threshold")

        fig = AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                                        fig = fig, subplot = 223,
                                        title = u"Mass distribution", **kwargs)

        if  kwargs.has_key("legend_mass"):
            kwargs["legend"] = kwargs["legend_mass"]

        if  kwargs.has_key("threshold_mass"):
            kwargs["threshold"] = kwargs["threshold_mass"]

        fig = AMC.PlotMassConcentration(concentration_aer_mass_pretty,
                                        fig = fig, subplot = 224,
                                        title = u"Percentage mass distribution",
                                        percentage = True, **kwargs)

        if kwargs.has_key("text"):
            fig.text(kwargs["text"][0], kwargs["text"][1], kwargs["text"][2], fontsize = kwargs["text"][3])

        if output_file != None:
            _pylab.savefig(output_file)
            _pylab.clf()
        elif show:
            _pylab.show()

        return fig


    @staticmethod
    def Plot2d(concentration_aer, **kwargs):

        discretization_composition_ptr = _core.DiscretizationClassBase.GetDiscretizationComposition(0)
        for i in range(1, _DiscretizationSize.GetNsection()):
            if discretization_composition_ptr.GetId() != \
               _core.DiscretizationClassBase.GetDiscretizationComposition(i).GetId():
                raise ValueError("Plot2d() method is valid only if all size sections have same composition discretization.")

        if discretization_composition_ptr.GetNdim() != 1:
            raise ValueError("Plot2d method is valid only if composition discretization dimension is one.")

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.2, "right" : 0.2, "bottom" : 0.2, "top" : 0.2}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        _pylab.clf()
        fig = _pylab.figure()
        ax = fig.add_subplot(111)

        diameter_bound = _DiscretizationSize.GetDiameterBound()

        dimension_index = 0
        if kwargs.has_key("dimension_index"):
            dimension_index = kwargs["dimension_index"]

        if dimension_index > 1:
            raise ValueError("Dimension index value cannot be strictly greater than 1.")

        fraction_bound = []
        for i in range(discretization_composition_ptr.GetNclass()):
            polygon_index = _core.vector1i()
            discretization_composition_ptr.GetClassPolygonList(i, polygon_index)
            j = polygon_index[0]

            point_index = _core.vector1i()
            discretization_composition_ptr.GetPolygonPointList(j, point_index)

            class_abscisse = []
            for k in point_index:
                coordinate = _core.vector1r()
                discretization_composition_ptr.GetPointCoordinate(k, coordinate)
                class_abscisse.append(coordinate[0])

            if i == 0:
                fraction_bound.append(class_abscisse[0])
                fraction_bound.append(class_abscisse[1])
            else:
                fraction_bound.append(class_abscisse[1])

        fraction_bound = _numpy.array(fraction_bound, dtype = _numpy.float)

        # Put in matrix form for pcolor/pcolormesh
        diameter_bound = _numpy.array([diameter_bound for i in range(discretization_composition_ptr.GetNclass() + 1)])
        fraction_bound = _numpy.array([fraction_bound for i in range(_DiscretizationSize.GetNbound())]).transpose()

        concentration_aer_t = _copy.copy(concentration_aer).transpose()
        concentration_aer_t = _numpy.ma.masked_where(concentration_aer_t < _core.AMC_NUMBER_CONCENTRATION_MINIMUM,
                                                     concentration_aer_t)

        if not kwargs.has_key("color"):
            kwargs["color"] = "Jet"

        if isinstance(kwargs["color"], str):
            cmap = _pylab.get_cmap(kwargs["color"])
        else:
            cmap = _ListedColormap(kwargs["color"])

        norm =  _LogNorm(vmin = concentration_aer_t.min(),
                         vmax = concentration_aer_t.max())

        cs = ax.pcolormesh(diameter_bound, fraction_bound, concentration_aer_t,
                           norm = norm, cmap = cmap, edgecolors = 'k', linewidth = 0.1)

        ax.set_xlim(diameter_bound.min(), diameter_bound.max())
        ax.set_xlabel(ur"diameter bounds [$\mu m$]")
        ax.xaxis.set_ticks(diameter_bound[0])
        ax.xaxis.set_ticklabels(["%1.2g" % x for x in diameter_bound[0]])
        ax.set_xscale("log")

        species_index = _core.vector1i()
        discretization_composition_ptr.GetDimensionSpeciesIndex(dimension_index, species_index)

        ylabel = _Species.GetName(species_index[0])
        for i in range(1, len(species_index)):
            ylabel += _Species.GetName(species_index[i])
        ylabel += " fraction bounds [0, 1]"

        ax.set_ylabel(ylabel)
        ax.set_ylim(fraction_bound.min(), fraction_bound.max())
        ax.set_yticks(fraction_bound[:,0])

        cbar = _pylab.colorbar(cs)
        cbar.ax.set_ylabel(ur"[#$m^{-3}$]")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotComposition(composition, section = None, **kwargs):

        if section == None:
            section = range(AerosolData.GetNg())

        if not isinstance(composition, list):
            composition = [composition]

        if isinstance(section, int):
            section = [section]

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 12

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if kwargs.has_key("startangle"):
            startangle = kwargs["startangle"]
        else:
            startangle = 0

        if kwargs.has_key("radius"):
            radius = kwargs["radius"]
        else:
            radius = 1

        if kwargs.has_key("explode"):
            explode = kwargs["explode"]
        else:
            explode = [0 for i in range(_Species.GetNspecies())]

        if kwargs.has_key("shadow"):
            shadow = kwargs["shadow"]
        else:
            shadow = False

        if kwargs.has_key("autopct"):
            autopct = kwargs["autopct"]
        else:
            autopct = '%1.1f%%'

        if kwargs.has_key("pctdistance"):
            pctdistance = kwargs["pctdistance"]
        else:
            pctdistance = 0.8

        if kwargs.has_key("labels"):
            labels = kwargs["labels"]
        else:
            labels = [_Species.GetLabel(i) for i in range(_Species.GetNspecies())]

        if kwargs.has_key("colors"):
            colors = kwargs["colors"]
        else:
            colors = [_Species.GetColor(i) for i in range(_Species.GetNspecies())]

        if kwargs.has_key("nrow"):
            nrow = kwargs["nrow"]
        else:
            nrow = 1

        if kwargs.has_key("ncol"):
            ncol = kwargs["ncol"]
        else:
            ncol = 1

        if nrow * ncol < len(section):
            nrow = int(_numpy.sqrt(len(section)))
            ncol = int(float(len(section)) / nrow)
            if float(len(section)) % nrow > 0.:
                ncol += 1

        text_kwargs = {}
        text_kwargs["ha"] = "center"
        for k, v in kwargs.iteritems():
            if "text_" in k:
                text_kwargs[k[5:]] = v

        if kwargs.has_key("text_x"):
            text_x = kwargs["text_x"]
        else:
            text_x = 0.0

        if kwargs.has_key("text_y"):
            text_y = kwargs["text_y"]
        else:
            text_y = 1.1

        _pylab.clf()

        fig = _pylab.figure()

        h = 0
        for i, c in zip(section, composition):
            h += 1

            if len(c) != _Species.GetNspecies():
                continue

            ax = fig.add_subplot(nrow, ncol, h)

            wedges = ax.pie(c * 100, explode = explode, labels = labels, colors = colors,
                            autopct = autopct, pctdistance = pctdistance, shadow = shadow)

            for w in wedges[0]:
                w.set_linewidth(0)

            j = AMC.GetSectionIndex(i)
            k = AMC.GetClassIndex(i)

            discretization_composition_ptr = AMC.GetDiscretizationComposition(j)

            if kwargs.has_key("title_%d" % i):
                stitle = kwargs["title_%d" % i]
            else:
                stitle = "%d.%s" % (j + 1, discretization_composition_ptr.GetClassInternalMixing(k).GetLabel())
                #stitle = "%d.%s" % (j, discretization_composition_ptr.GetClassInternalMixing(k).GetName().split(":")[-1])
                #stitle = "%d.%d" % (j, k)

            ax.text(text_x, text_y, stitle, **text_kwargs)

            ax.set_aspect(1)

        # If many subplots.
        fig.tight_layout()

        # Legend.
        labels = [_Species.GetLabel(i) for i in range(_Species.GetNspecies())]

        labels2 = []
        wedges2 = []
        for i, s in enumerate(labels):
            if s not in labels2: 
                labels2.append(s)
                wedges2.append(wedges[0][i])

        legend_kwargs = {}
        for k, v in kwargs.iteritems():
            if "legend_" in k:
                legend_kwargs[k[7:]] = v

        ax.legend(wedges2, labels2, **legend_kwargs)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotParticle(concentration_aer_num, concentration_aer_mass, fig = None, **kwargs):

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if kwargs.has_key("color"):
            color = kwargs["color"]
        else:
            color = 'c'

        if kwargs.has_key("alpha"):
            alpha = kwargs["alpha"]
        else:
            alpha = 0.5

        if kwargs.has_key("size"):
            s = kwargs["size"]
        else:
            s = 20

        if fig == None:
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        if not kwargs.has_key("conc"):
            kwargs["conc"] = "num"

        # Data.
        section_index, diameter = AMC.ComputeDiscretizationDiameter(concentration_aer_num,
                                                                    concentration_aer_mass)


        if kwargs["conc"] == "num":
            conc = [concentration_aer_num[int(i)] for i in section_index]
        elif kwargs["conc"] == "mass":
            conc = AerosolData.ComputeConcentrationAerosolMassTotal(concentration_aer_mass)
            conc = [conc[i] for i in section_index]

        ax.scatter(diameter, conc, s = s, c = color, alpha = alpha)

        # X-axis.
        loc = _DiscretizationSize.GetDiameterBound()
        label = ["%1.2g" % x for x in loc]

        ax.set_xscale("log")
        ax.set_xticks(loc)
        ax.set_xticklabels(label)
        ax.set_xlabel(ur"diameter $(d_p)$ [$\mu m$]")
        ax.set_xlim(loc[0], loc[-1])

        # Y-axis
        if kwargs["conc"] == "num":
            ax.set_yscale("log")
            ax.set_ylabel(ur"number concentration  $[\#.m^{-3}]$")
        else:
            ax.set_ylabel(ur"mass concentration  $[\mu g.m^{-3}]$")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


class AMCtable(_core.AMC_Table):
    pass


if _core.AMC_HAS_RUN > 0:
    if _core.AMC_HAS_COAGULATION > 0:
        class AMX_EulerMixedBaseCoefMoving(_core.AMX_EulerMixedBaseCoefMoving):
            pass

        class AMX_EulerMixedTableCoefMoving(_core.AMX_EulerMixedTableCoefMoving):
            pass

        class AMX_EulerMixedBaseCoefStatic(_core.AMX_EulerMixedBaseCoefStatic):
            pass

        class AMX_EulerMixedTableCoefStatic(_core.AMX_EulerMixedTableCoefStatic):
            pass


        class AMX_EulerHybridBaseCoefMoving(_core.AMX_EulerHybridBaseCoefMoving):
            pass

        class AMX_EulerHybridTableCoefMoving(_core.AMX_EulerHybridTableCoefMoving):
            pass

        class AMX_EulerHybridBaseCoefStatic(_core.AMX_EulerHybridBaseCoefStatic):
            pass

        class AMX_EulerHybridTableCoefStatic(_core.AMX_EulerHybridTableCoefStatic):
            pass


        class AMX_EulerMovingBaseCoefMoving(_core.AMX_EulerMovingBaseCoefMoving):
            pass

        class AMX_EulerMovingTableCoefMoving(_core.AMX_EulerMovingTableCoefMoving):
            pass

        class AMX_EulerMovingBaseCoefStatic(_core.AMX_EulerMovingBaseCoefStatic):
            pass

        class AMX_EulerMovingTableCoefStatic(_core.AMX_EulerMovingTableCoefStatic):
            pass


    class AMX_EulerMixedBase(_core.AMX_EulerMixedBase):
        pass

    class AMX_EulerMixedTable(_core.AMX_EulerMixedTable):
        pass


    class AMX_EulerHybridBase(_core.AMX_EulerHybridBase):
        pass

    class AMX_EulerHybridTable(_core.AMX_EulerHybridTable):
        pass


    class AMX_EulerMovingBase(_core.AMX_EulerMovingBase):
        pass

    class AMX_EulerMovingTable(_core.AMX_EulerMovingTable):
        pass

 
    class AMX:
        # Static type.
        _type = 0

        @staticmethod
        def Type(**template):
            if AMX._type > 0:
                return AMX._type

            if not template.has_key("size_redistribution") or \
               not template.has_key("tabulation") or \
               (_core.AMC_HAS_COAGULATION > 0 and not template.has_key("repartition_coefficient")):
                raise ValueError("\n#\n# Need to specify several templates :\n" + \
                                 "#  - \"size_redistribution\" = \"mixed\", \"hybrid\" or \"moving\",\n" + \
                                 "#  - \"tabulation\" = \"y[es]\", \"n[o]\", \"t[rue]\" or \"f[alse]\",\n" + \
                                 "#  - \"repartition_coefficient\" = \"moving\" or \"static\".\n#")

            if _re.search("^\s*y[es]?\s*$|^\s*t[rue]?\s*$", template["tabulation"], _re.I):
                template["tabulation"] = True
            elif _re.search("^\s*no?\s*$|^\s*f[alse]?\s*$", template["tabulation"], _re.I):
                template["tabulation"] = False
            else:
                raise ValueError("Unknown value \"" + template["tabulation"] + "\" for tabulation" + \
                                 " template, expected \"y[es]\", \"n[o]\", \"t[rue]\" or \"f[alse]\".")

            if _re.search("^\s*mixed\s*$", template["size_redistribution"], _re.I):
                if template["tabulation"]:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMixedTableCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$",
                                    template["repartition_coefficient"], _re.I):
                        return AMX_EulerMixedTableCoefMoving.Type()
                    else:
                        return AMX_EulerMixedTable.Type()
                else:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMixedBaseCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMixedBaseCoefMoving.Type()
                    else:
                        return AMX_EulerMixedBase.Type()
            elif _re.search("^\s*hybrid\s*$", template["size_redistribution"], _re.I):
                if template["tabulation"]:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerHybridTableCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerHybridTableCoefMoving.Type()
                    else:
                        return AMX_EulerHybridTable.Type()
                else:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerHybridBaseCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerHybridBaseCoefMoving.Type()
                    else:
                        return AMX_EulerHybridBase.Type()
            elif _re.search("^\s*moving\s*$", template["size_redistribution"], _re.I):
                if template["tabulation"]:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMovingTableCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMovingTableCoefMoving.Type()
                    else:
                        return AMX_EulerMovingTable.Type()
                else:
                    if _re.search("^\s*static\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMovingBaseCoefStatic.Type()
                    elif _re.search("^\s*moving\s*$", template["repartition_coefficient"], _re.I):
                        return AMX_EulerMovingBaseCoefMoving.Type()
                    else:
                        return AMX_EulerMovingBase.Type()
            else:
                raise ValueError("Wrong template for \"size_redistribution\", " + \
                                 "should be \"mixed\", \"hybrid\" or \"moving\".")


        @staticmethod
        def Initiate(configuration_file, prefix, **kwargs):
            AMX._type = kwargs.get("type", AMX.Type(**kwargs))

            if _core.AMC_HAS_COAGULATION > 0:
                if AMX._type == AMX_EulerMixedBaseCoefMoving.Type():
                    AMX_EulerMixedBaseCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMixedTableCoefMoving.Type():
                    AMX_EulerMixedTableCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMixedBaseCoefStatic.Type():
                    AMX_EulerMixedBaseCoefStatic.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMixedTableCoefStatic.Type():
                    AMX_EulerMixedTableCoefStatic.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerHybridBaseCoefMoving.Type():
                    AMX_EulerHybridBaseCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerHybridTableCoefMoving.Type():
                    AMX_EulerHybridTableCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerHybridBaseCoefStatic.Type():
                    AMX_EulerHybridBaseCoefStatic.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerHybridTableCoefStatic.Type():
                    AMX_EulerHybridTableCoefStatic.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMovingBaseCoefMoving.Type():
                    AMX_EulerMovingBaseCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMovingTableCoefMoving.Type():
                    AMX_EulerMovingTableCoefMoving.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMovingBaseCoefStatic.Type():
                    AMX_EulerMovingBaseCoefStatic.Initiate(configuration_file, prefix)
                elif AMX._type == AMX_EulerMovingTableCoefStatic.Type():
                    AMX_EulerMovingTableCoefStatic.Initiate(configuration_file, prefix)

            if AMX._type == AMX_EulerMixedBase.Type():
                AMX_EulerMixedBase.Initiate(configuration_file, prefix)
            elif AMX._type == AMX_EulerMixedTable.Type():
                AMX_EulerMixedTable.Initiate(configuration_file, prefix)
            elif AMX._type == AMX_EulerHybridBase.Type():
                AMX_EulerHybridBase.Initiate(configuration_file, prefix)
            elif AMX._type == AMX_EulerHybridTable.Type():
                AMX_EulerHybridTable.Initiate(configuration_file, prefix)
            elif AMX._type == AMX_EulerMovingBase.Type():
                AMX_EulerMovingBase.Initiate(configuration_file, prefix)
            elif AMX._type == AMX_EulerMovingTable.Type():
                AMX_EulerMovingTable.Initiate(configuration_file, prefix)
            else:
                raise ValueError("Unknown type code \"" + str(AMX._type) + "\".")


        @staticmethod
        def Terminate(**kwargs):
            AMX._type = kwargs.get("type", AMX.Type(**kwargs))

            if _core.AMC_HAS_COAGULATION > 0:
                if AMX._type == AMX_EulerMixedBaseCoefMoving.Type():
                    AMX_EulerMixedBaseCoefMoving.Terminate()
                elif AMX._type == AMX_EulerMixedTableCoefMoving.Type():
                    AMX_EulerMixedTableCoefMoving.Terminate()
                elif AMX._type == AMX_EulerMixedBaseCoefStatic.Type():
                    AMX_EulerMixedBaseCoefStatic.Terminate()
                elif AMX._type == AMX_EulerMixedTableCoefStatic.Type():
                    AMX_EulerMixedTableCoefStatic.Terminate()
                elif AMX._type == AMX_EulerHybridBaseCoefMoving.Type():
                    AMX_EulerHybridBaseCoefMoving.Terminate()
                elif AMX._type == AMX_EulerHybridTableCoefMoving.Type():
                    AMX_EulerHybridTableCoefMoving.Terminate()
                elif AMX._type == AMX_EulerHybridBaseCoefStatic.Type():
                    AMX_EulerHybridBaseCoefStatic.Terminate()
                elif AMX._type == AMX_EulerHybridTableCoefStatic.Type():
                    AMX_EulerHybridTableCoefStatic.Terminate()
                elif AMX._type == AMX_EulerMovingBaseCoefMoving.Type():
                    AMX_EulerMovingBaseCoefMoving.Terminate()
                elif AMX._type == AMX_EulerMovingTableCoefMoving.Type():
                    AMX_EulerMovingTableCoefMoving.Terminate()
                elif AMX._type == AMX_EulerMovingBaseCoefStatic.Type():
                    AMX_EulerMovingBaseCoefStatic.Terminate()
                elif AMX._type == AMX_EulerMovingTableCoefStatic.Type():
                    AMX_EulerMovingTableCoefStatic.Terminate()

            if AMX._type == AMX_EulerMixedBase.Type():
                AMX_EulerMixedBase.Terminate()
            elif AMX._type == AMX_EulerMixedTable.Type():
                AMX_EulerMixedTable.Terminate()
            elif AMX._type == AMX_EulerHybridBase.Type():
                AMX_EulerHybridBase.Terminate()
            elif AMX._type == AMX_EulerHybridTable.Type():
                AMX_EulerHybridTable.Terminate()
            elif AMX._type == AMX_EulerMovingBase.Type():
                AMX_EulerMovingBase.Terminate()
            elif AMX._type == AMX_EulerMovingTable.Type():
                AMX_EulerMovingTable.Terminate()
            else:
                raise ValueError("Unknown type code \"" + str(AMX._type) + "\".")

            AMX._type = 0


        @staticmethod
        def Forward(concentration_aer_number, concentration_aer_mass, concentration_gas, time_step):
            if AMX._type == 0:
                raise Exception("AMC not yet initialized with a specific type, you must" + \
                                " call AMX.Initiate(configuration_file, prefix, ...) before.")

            if _core.AMC_HAS_COAGULATION > 0:
                if AMX._type == AMX_EulerMixedBaseCoefMoving.Type():
                    AMX_EulerMixedBaseCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                         concentration_gas, time_step)
                elif AMX._type == AMX_EulerMixedTableCoefMoving.Type():
                    AMX_EulerMixedTableCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerMixedBaseCoefStatic.Type():
                    AMX_EulerMixedBaseCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                         concentration_gas, time_step)
                elif AMX._type == AMX_EulerMixedTableCoefStatic.Type():
                    AMX_EulerMixedTableCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerHybridBaseCoefMoving.Type():
                    AMX_EulerHybridBaseCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerHybridTableCoefMoving.Type():
                    AMX_EulerHybridTableCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                           concentration_gas, time_step)
                elif AMX._type == AMX_EulerHybridBaseCoefStatic.Type():
                    AMX_EulerHybridBaseCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerHybridTableCoefStatic.Type():
                    AMX_EulerHybridTableCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                           concentration_gas, time_step)
                elif AMX._type == AMX_EulerMovingBaseCoefMoving.Type():
                    AMX_EulerMovingBaseCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerMovingTableCoefMoving.Type():
                    AMX_EulerMovingTableCoefMoving.Forward(concentration_aer_number, concentration_aer_mass,
                                                           concentration_gas, time_step)
                elif AMX._type == AMX_EulerMovingBaseCoefStatic.Type():
                    AMX_EulerMovingBaseCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                          concentration_gas, time_step)
                elif AMX._type == AMX_EulerMovingTableCoefStatic.Type():
                    AMX_EulerMovingTableCoefStatic.Forward(concentration_aer_number, concentration_aer_mass,
                                                           concentration_gas, time_step)

            if AMX._type == AMX_EulerMixedBase.Type():
                AMX_EulerMixedBase.Forward(concentration_aer_number, concentration_aer_mass,
                                           concentration_gas, time_step)
            elif AMX._type == AMX_EulerMixedTable.Type():
                AMX_EulerMixedTable.Forward(concentration_aer_number, concentration_aer_mass,
                                            concentration_gas, time_step)
            elif AMX._type == AMX_EulerHybridBase.Type():
                AMX_EulerHybridBase.Forward(concentration_aer_number, concentration_aer_mass,
                                            concentration_gas, time_step)
            elif AMX._type == AMX_EulerHybridTable.Type():
                AMX_EulerHybridTable.Forward(concentration_aer_number, concentration_aer_mass,
                                             concentration_gas, time_step)
            elif AMX._type == AMX_EulerMovingBase.Type():
                AMX_EulerMovingBase.Forward(concentration_aer_number, concentration_aer_mass,
                                            concentration_gas, time_step)
            elif AMX._type == AMX_EulerMovingTable.Type():
                AMX_EulerMovingTable.Forward(concentration_aer_number, concentration_aer_mass,
                                             concentration_gas, time_step)

