#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as core

import util, species, base, discretization, diameter, redistribution
import physics, thermodynamics, dynamics
import os as _os

if core.AMC_HAS_SURFACE_TENSION == 1:
    import surface_tension
        
if core.AMC_HAS_COLLISION_EFFICIENCY == 1:
    import collision_efficiency

if core.AMC_HAS_REPARTITION_COEFFICIENT == 1:
    import repartition_coefficient

if core.AMC_HAS_COAGULATION == 1:
    import coagulation

if core.AMC_HAS_CONDENSATION == 1:
    import condensation

if core.AMC_HAS_NUCLEATION == 1:
    import nucleation

if core.AMC_HAS_NUMERICS == 1:
    import numerics

if core.AMC_HAS_MODAL == 1:
    import modal

if core.AMC_HAS_NEW_PARTICLE_FORMATION == 1:
    import new_particle_formation

if core.AMC_HAS_MPI == 1:
    from mpi4py import MPI
else:
    print "AMC built without MPI support."

#
# License.
#

LICENSE = open(_os.path.dirname(_os.path.abspath(__file__)) + "/../LICENSE").read()

print """
AMC  Copyright (C) 2013-2015  INERIS
This program comes with ABSOLUTELY NO WARRANTY; for details type `print LICENSE'.
This is free software, and you are welcome to redistribute it
under certain conditions; type `print LICENSE' for details.

Author contact : Edouard Debry <edouard.debry@gmail.com>
"""


#
# Readme.
#

README = open(_os.path.dirname(_os.path.abspath(__file__)) + "/../README.md").read()
