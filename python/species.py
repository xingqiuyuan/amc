#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as _core

import numpy as _numpy
import pylab as _pylab
import copy as _copy
import util as _util
import sys as _sys
import subprocess as _subprocess
import os as _os

class Species(_core.Species):
    @staticmethod
    def GetNameList():
        return list(_core.Species_GetNameList())

    @staticmethod
    def GetLongNameList():
        return list(_core.Species_GetLongNameList())

    @staticmethod
    def GetSpeciesList():
        str_species = ""
        for i in range(_core.Species_GetNspecies()):
            str_species += _core.Species_GetStr(i) + "\n"
        return str_species

    @staticmethod
    def GetSemivolatile():
        semivolatile = _core.vector1i()
        _core.Species_GetSemivolatile(semivolatile)
        return list(semivolatile)

    @staticmethod
    def GetNotSemivolatile():
        not_semivolatile = _core.vector1i()
        _core.Species_GetNotSemivolatile(not_semivolatile)
        return list(not_semivolatile)

    @staticmethod
    def GetPhase(i):
        phase = _core.vector1i()
        _core.Species_GetPhase(i, phase)
        return list(phase)

    @staticmethod
    def GetProperty(name, i = None):
        if i == None:
            return _numpy.array(_core.Species_GetProperty(name), dtype = _numpy.float)
        else:
            return _core.Species_GetProperty(name, i)

    @staticmethod
    def GetPropertyList():
        return list(_core.Species_GetPropertyList())


    @staticmethod
    def ExportPropertyTableORG(name, output_file, **kwargs):
        stable = _core.Species_DisplayPropertyTable(_core.vector1s(name))
        stable = stable.replace("µ", "\mu ")
        stable = stable.replace("#", "\#")
        stable = stable.replace(">=", "\ge")

        fo = open(output_file, "w")
        fo.write(stable)
        fo.close()


    @staticmethod
    def ExportPropertyTable(name, output_file, **kwargs):
        stable = _core.Species_DisplayPropertyTable(_core.vector1s(name))
        stable = stable.replace("µ", "\mu ")
        stable = stable.replace("#", "\#")
        stable = stable.replace(">=", "$\ge$")

        if ".org" in output_file:
            fo = open(output_file, "w")
            fo.write(stable)
            fo.close()
        elif ".pdf" in output_file:
            output_file_org = output_file.replace(".pdf", ".org")
            header = """
#+TITLE:
#+LaTeX_CLASS: article
#+LaTeX_CLASS_OPTIONS: [a4paper, french, 12pt]
#+LATEX_HEADER: \pagestyle{empty}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage[french]{babel}
#+LATEX_HEADER: \usepackage{siunitx}
#+LATEX_HEADER: \usepackage[a4paper, landscape, lmargin=0.0in]{geometry}
#+OPTIONS: |:t f:nil author:nil *:t toc:nil
#+LANGUAGE: fr
#+OPTIONS: LaTeX:t
#+CAPTION: Physico-chemical properties of species in AMC.\n"""

            fo = open(output_file_org, "w")
            fo.write(header)
            fo.write(stable)
            fo.close()

            output_file_tex = output_file.replace(".pdf", ".tex")
            fo = open(output_file_tex, "w")
            fo.close()

            cmd = "/usr/bin/emacs --batch --visit %s -l %s/.emacs --execute='(org-export-as-pdf 3)' --kill" \
                  % (output_file_org, _os.environ["HOME"])
            p = _subprocess.Popen(cmd, shell = True, bufsize = 0, stdin = None,
                                  stdout = _subprocess.PIPE, stderr = _subprocess.PIPE,
                                  close_fds = True)

            stdout, stderr = p.communicate()
        else:
            raise ValueError("Extension \"" + output_file[-3:] + "\" not yet implemented.")


    @staticmethod
    def _plot_composition_(composition, labels, colors, fig = None, **kwargs):

        if kwargs.has_key("threshold"):
            composition = list(composition)

            i = -1
            j = 0
            while j < len(composition):
                if composition[j] * 100. < kwargs["threshold"]:
                    if i < 0:
                        i = j
                        labels[i] = [_copy.copy(labels[j])]
                        colors[i] = [_copy.copy(colors[j])]
                        j += 1
                    else:
                        labels[i].append(labels.pop(j))
                        colors[i].append(colors.pop(j))
                        composition[i] += composition.pop(j)
                else:
                    j += 1

            if not i < 0:
                if kwargs.has_key("threshold_label"):
                    labels[i] = kwargs["threshold_label"]
                else:
                    labels[i] = " ".join(labels[i])

                if kwargs.has_key("threshold_color"):
                    colors[i] = kwargs["threshold_color"]
                else:
                    colors[i] = _util.mean_hexcolor(colors[i])

            composition = _numpy.array(composition, dtype = _numpy.float)

        #
        # Figure settings.
        #

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 12

        if kwargs.has_key("height"):
            height = kwargs["height"]
        else:
            height = 12

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, height)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if kwargs.has_key("startangle"):
            startangle = kwargs["startangle"]
        else:
            startangle = 0

        if kwargs.has_key("radius"):
            radius = kwargs["radius"]
        else:
            radius = 1

        if kwargs.has_key("explode"):
            explode = kwargs["explode"]
        else:
            explode = [0 for i in range(len(composition))]

        if kwargs.has_key("shadow"):
            shadow = kwargs["shadow"]
        else:
            shadow = False

        if kwargs.has_key("autopct"):
            autopct = kwargs["autopct"]
        else:
            autopct = '%1.1f%%'

        if kwargs.has_key("pctdistance"):
            pctdistance = kwargs["pctdistance"]
        else:
            pctdistance = 0.8

        #
        # Figure.
        #

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        patches, texts, autotexts = ax.pie(composition, explode = explode, labels = labels, colors = colors,
                                           autopct = autopct, pctdistance = pctdistance, shadow = shadow)

        if kwargs.has_key("pie_size"):
            for t in texts:
                t.set_size(kwargs["pie_size"])

        # Title.
        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        ax.set_aspect(1)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotComposition(concentration, fig = None, group = None, **kwargs):
        # If no group default is species per species.
        if group == None:
            group = {}
            if kwargs.has_key("species_name"):
                for i, s in enumerate(kwargs["species_name"]):
                    group[s] = [i]
            else:
                for i in range(Species.GetNspecies()):
                    group[Species.GetName(i)] = [i]

        if kwargs.has_key("exclude"):
            for s in group.keys():
                if s in kwargs["exclude"]:
                    group.pop(s)

        # Make aggregated concentrations and colors for each group.
        labels = []
        colors = []
        composition = []
        for key,val in group.iteritems():
            if isinstance(val, int):
                aggregation = concentration[val]
                val = [val]
            elif isinstance(val, list):
                aggregation = _numpy.array([concentration[i] for i in val], dtype = _numpy.float).sum()
            else:
                raise ValueError("Group must be an integer or a list of integer.")

            # Avoid nul concentrations.
            if not aggregation > 0.:
                continue

            composition.append(aggregation)

            if kwargs.has_key("%s_label" % key):
                labels.append(kwargs["%s_label" % key])
            else:
                labels.append(key)

            if kwargs.has_key("%s_color" % key):
                colors.append(kwargs["%s_color" % key])
            else:
                if kwargs.has_key("species_name"):
                    species_name = [kwargs["species_name"][i] for i in val]
                    species_index = [Species.GetIndex(s) for s in species_name]
                    species_colors = [Species.GetColor(i) for i in species_index if i >= 0]
                else:
                    species_colors = [Species.GetColor(i) for i in val]

                colors.append(_util.mean_hexcolor(species_colors))

        composition = _numpy.array(composition, dtype = _numpy.float)

        # Eventually divide by other total, this usually happens with measurements.
        if kwargs.has_key("total"):
            total = kwargs["total"]

            # Non sense if total is inferior to sum.
            if total < composition.sum():
                total = composition.sum()
        else:
            total = composition.sum()

        if not total > 0.:
            raise ValueError("Total concentration is negative or nul.")

        composition /= total

        return Species._plot_composition_(composition, labels, colors, fig = fig, **kwargs)


    @staticmethod
    def PlotCompositionOrganicsAggregated(concentration, fig = None, **kwargs):
        group = {"organics" : []}
        for i in range(Species.GetNspecies()):
            if Species.IsOrganic(i):
                group["organics"].append(i)
            else:
                group[Species.GetName(i)] = i

        return Species.PlotComposition(concentration, fig, group = group, **kwargs)


    @staticmethod
    def PlotCompositionOrganicsOnly(concentration, fig = None, **kwargs):
        group = {}
        for i in range(Species.GetNspecies()):
            if Species.IsOrganic(i):
                group[Species.GetName(i)] = i

        return Species.PlotComposition(concentration, fig, group = group, **kwargs)

    @staticmethod
    def PlotCompositionOrganicCarbon(concentration, fig = None, **kwargs):
        group = {}
        for i in range(Species.GetNspecies()):
            if Species.IsOrganic(i):
                concentration[i] *= Species.GetCarbonNumber(i) * _core.AMC_CARBON_MOLAR_MASS / Species.GetMolarMass(i)
                group[Species.GetName(i)] = i

        return Species.PlotComposition(concentration, fig, group = group, **kwargs)


    @staticmethod
    def GetSortedSaturationVaporConcentration():
        index_ascending = _core.vector1i()
        saturation_vapor_concentration = _core.vector1r()
        _core.Species_GetSortedSaturationVaporConcentration(index_ascending, saturation_vapor_concentration)
        return _numpy.array(index_ascending, dtype = _numpy.int), \
            _numpy.array(saturation_vapor_concentration, dtype = _numpy.float)


    @staticmethod
    def PlotSortedSaturationVaporConcentration(concentration, index_ascending, saturation_vapor_concentration, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 30
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.2, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if kwargs.has_key("line_width"):
            line_width = kwargs["line_width"]
        else:
            line_width = 2

        # The width of the bars
        if kwargs.has_key("bar_width"):
            bar_width = kwargs["bar_width"]
        else:
            bar_width = 0.8

        if kwargs.has_key("bar_offset"):
            bar_offset = kwargs["bar_offset"]
        else:
            bar_offset = 0.2

        if kwargs.has_key("color"):
            color = kwargs["color"]
        else:
            color = {"gas" : "lightgray", "aer" : "darkslategray"}

        do_percentage = kwargs.has_key("percentage")

        # Data.
        data = _numpy.zeros([Species.GetNgas(), 2], dtype = _numpy.float)

        for i in range(Species.GetNgas()):
            data[i] = concentration[index_ascending[i]]

        # Keep only species with something.
        saturation_vapor_concentration = [saturation_vapor_concentration[i]
                                          for i in range(Species.GetNgas())
                                          if data[i].sum() > 0.]
        index_ascending = [index_ascending[i] for i in range(Species.GetNgas()) if data[i].sum() > 0.]
        data = _numpy.array([data[i] for i in range(Species.GetNgas()) if data[i].sum() > 0.])

        if do_percentage:
            data = _numpy.array([data[i] / data[i].sum() for i in range(len(data))]) * 100.
        else:
            if not kwargs.has_key("break_ratio"):
                kwargs["break_ratio"] = 10.

            top = data.sum(1)
            bottom = []
            while top.std() > kwargs["break_ratio"] * top.mean():
                imax = top.argmax()
                bottom.append(top[imax])
                top = _numpy.array([top[i] for i in range(len(top)) if i != imax])
            bottom = _numpy.array(bottom)

            kwargs["break_axis"] = [top.max() * 1.01, bottom.min() * 0.99]

        # Figure.
        _pylab.clf()

        if kwargs.has_key("break_axis"):
            fig, (ax1, ax3) = _pylab.subplots(2, 1, sharex = True)
        else:
            fig = _pylab.figure()
            ax1 = fig.add_subplot(111)

        # Make bar plot.
        ind = bar_offset

        loc = []
        label = []
        for i in range(len(data)):
            loc.append(ind + bar_width * 0.5)
            label.append(Species.GetLabel(int(index_ascending[i])))

            yoff = 0.
            ax1.bar(ind, data[i][1], width = bar_width, linewidth = 1,
                    bottom = yoff, color = color["aer"], label = "aer")

            if kwargs.has_key("break_axis"):
                ax3.bar(ind, data[i][1], width = bar_width, linewidth = 1,
                        bottom = yoff, color = color["aer"], label = "aer")

            yoff += data[i][1]
            ax1.bar(ind, data[i][0], width = bar_width, linewidth = 1,
                    bottom = yoff, color = color["gas"], label = "gas")

            if kwargs.has_key("break_axis"):
                ax3.bar(ind, data[i][0], width = bar_width, linewidth = 1,
                        bottom = yoff, color = color["gas"], label = "gas")

            ind += bar_width + bar_offset

        if kwargs.has_key("break_axis"):
            ymin, ymax = ax1.get_ylim()
            ax1.set_ylim(kwargs["break_axis"][1], ymax)
            ax3.set_ylim(ymin, kwargs["break_axis"][0])

            ax1.spines["bottom"].set_visible(False)
            ax3.spines["top"].set_visible(False)

            ax1.xaxis.tick_top()
            ax1.tick_params(labeltop = "off")
            ax3.xaxis.tick_bottom()

            d = 0.01
            kwg = dict(transform = ax1.transAxes, color = "k", clip_on = False)
            ax1.plot((-d, +d), (-d, +d), lw = 1.1, **kwg)
            ax1.plot((1-d, 1+d), (-d, +d), lw = 1.1, **kwg)
            kwg.update(transform = ax3.transAxes)
            ax3.plot((-d, +d), (1 - d, 1 + d), lw = 1.1, **kwg)
            ax3.plot((1-d, 1+d), (1-d, 1+d), lw = 1.1, **kwg)

        # Second axis.
        ax2 = ax1.twinx()
        ax2.semilogy(loc, saturation_vapor_concentration, "r-o", lw = line_width)
        if kwargs.has_key("break_axis"):
            ymin, ymax = ax2.get_ylim()
            ax2.set_ylim(ymin, ymax)
            ax2.spines["bottom"].set_visible(True)

        # X-axis.
        if  kwargs.has_key("xticks_rotation"):
            xticks_rotation = kwargs["xticks_rotation"]
        else:
            xticks_rotation = 45

        if kwargs.has_key("break_axis"):
            ax3.set_xticks(loc)
            ax3.set_xticklabels(label, rotation = xticks_rotation, ha = 'right')
            ax3.set_xlim(0, ind)
        else:
            ax1.set_xticks(loc)
            ax1.set_xticklabels(label, rotation = xticks_rotation, ha = 'right')
            ax1.set_xlim(0, ind)

        # Y-axis.
        if do_percentage:
            ax1.set_ylabel(ur"percentage")
            ax1.set_ylim(0, 100)
        else:
            ax1.set_ylabel(ur"concentration [$\mu g.m^{-3}$]")
            if kwargs.has_key("break_axis"):
                ax3.set_ylabel(ur"concentration [$\mu g.m^{-3}$]")

        ax2.set_ylabel(ur"saturation vapor concentration [$\mu g.m^{-3}$]")
        ax2.yaxis.label.set_color("red")
        ax2.tick_params(axis = "y", colors = "red")

        if kwargs.has_key("legend"):
            handles, labels = ax1.get_legend_handles_labels()
            handles2 = []
            labels2 = []
            for h, l in zip(handles, labels):
                is_duplicate = False

                for l2 in labels2:
                    is_duplicate = l == l2
                    if is_duplicate:
                        break

                if not is_duplicate:
                    handles2.append(h)
                    labels2.append(l)

            if isinstance(kwargs["legend"], int):
                ax1.legend(handles2[::-1], labels2[::-1], loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax1.legend(handles2[::-1], labels2[::-1], bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("legend argument must be either an integer or a list.")

        fig.subplots_adjust(hspace = 0.05)

        # Title.
        if kwargs.has_key("title"):
            ax1.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def GenerateRandomComposition():
        composition = _core.Species_GenerateRandomComposition()
        return _numpy.array(composition, dtype = _numpy.float)


    @staticmethod
    def GenerateComposition(**kwargs):
        composition = _numpy.zeros(Species.GetNspecies(), dtype = _numpy.float)
        for k, v in kwargs.iteritems():
            composition[Species.GetIndex(k)] = float(v)

        if composition.sum() == float(0):
            for i in range(len(composition)):
                composition[i] = float(1) / float(Species.GetNspecies())
        else:
            if composition.sum() != float(1):
                composition /= composition.sum()

        return composition


class SpeciesTracer(_core.SpeciesTracer):
    @staticmethod
    def GetSpeciesName(i):
        species_name = _core.vector1s()
        _core.SpeciesTracer.GetSpeciesName(i, species_name)
        return list(species_name)


    @staticmethod
    def GetSpeciesMassFraction(i):
        species_mass_fraction = _core.vector1r()
        _core.SpeciesTracer.GetSpeciesMassFraction(i, species_mass_fraction)
        return _numpy.array(species_mass_fraction, dtype = _numpy.float)


    @staticmethod
    def GetTracerList():
        str_tracer = ""
        for i in range(_core.SpeciesTracer.GetNtracer()):
            str_tracer += _core.SpeciesTracer_GetStr(i) + "\n"
        return str_tracer


class SpeciesEquilibrium(_core.SpeciesEquilibrium):
    @staticmethod
    def GetSpeciesList():
        str_species = ""
        for i in range(_core.SpeciesEquilibrium_GetNspecies()):
            str_species += _core.SpeciesEquilibrium_GetStr(i) + "\n"
        return str_species


    @staticmethod
    def GetIonic():
        ionic = _core.vector1i()
        _core.SpeciesEquilibrium_GetIonic(ionic)
        return list(ionic)


    @staticmethod
    def GetSolid():
        solid = _core.vector1i()
        _core.SpeciesEquilibrium_GetSolid(solid)
        return list(solid)

    @staticmethod
    def GetGroup(i):
        group = _core.vector1i()
        _core.SpeciesEquilibrium_GetGroup(i, group)
        return list(group)

    @staticmethod
    def GetNgroup():
        Ngroup = _core.vector1i()
        _core.SpeciesEquilibrium_GetNgroup(Ngroup)
        return list(Ngroup)


class Phase(_core.Phase):

    @staticmethod
    def GetSpecies(i):
        species = _core.vector1i()
        _core.Phase_GetSpecies(i, species)
        return list(species)
