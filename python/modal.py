#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import numpy as _numpy
import pylab as _pylab
import os as _os

config_file = _os.path.join(_os.path.dirname(_os.path.realpath(__file__)), "../share/"
                            + _core.ModalDistribution.GetConfigurationFile())


class ModalDistribution(_core.ModalDistribution):

    @staticmethod
    def GetAvailableDistribution():
        return list(_core.ModalDistribution_GetAvailableDistribution())


    def ComputeSectionalDistributionRegular(self, dmin, dmax, Nb, Nquadrature = _core.MODAL_DISTRIBUTION_NQUADRATURE_DEFAULT):
        diameter = []
        q = dmax / dmin
        for j in range(Nb + 1):
            diameter.append(dmin * _numpy.power(q, float(j) / float(Nb)))

        return self.ComputeSectionalDistribution(diameter, Nquadrature) + [diameter]


    def ComputeSectionalDistribution(self, diameter, Nquadrature = 0):
        diameter_vect = _core.vector1r(len(diameter))
        for i,d in enumerate(diameter):
            diameter_vect[i] = d

        distribution_number = _core.vector1r()
        distribution_volume = _core.vector1r()
        diameter_mean = _core.vector1r()

        _core.ModalDistribution.ComputeSectionalDistribution(self, diameter,
                                                             diameter_mean,
                                                             distribution_number,
                                                             distribution_volume,
                                                             Nquadrature)
        return [_numpy.array(diameter_mean, dtype = _numpy.float), \
                _numpy.array(distribution_number, dtype = _numpy.float), \
                _numpy.array(distribution_volume, dtype = _numpy.float)]


    def Plot(self, fig = None, **kwargs):

        if kwargs.has_key("N"):
            N = kwargs["N"]
        else:
            N = 10000

        if kwargs.has_key("dmin"):
            dmin = kwargs["dmin"]
        else:
            dmin = 0.001

        if dmin <= 0.:
            raise ValueError("Minimum diameter cannot be <= 0.")

        if kwargs.has_key("dmax"):
            dmax = kwargs["dmax"]
        else:
            dmax = 10.0

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)
        else:
            ax = fig.gca()

        if kwargs.has_key("fmt"):
            fmt = kwargs["fmt"]
        else:
            fmt = "k-"

        if kwargs.has_key("label"):
            label = kwargs["label"]
        else:
            label = self.GetName()

        if kwargs.has_key("lw"):
            lw = kwargs["lw"]
        else:
            lw = 1

        # Data.
        q = dmax / dmin
        diameter = [dmin * _numpy.power(q, float(i) / float(N - 1)) for i in range(N)]

        if kwargs.has_key("volume"):
            data = [self.GetVolumeDensity(d) for d in diameter]
        else:
            data = [self.GetNumberDensity(d) for d in diameter]

        data = _numpy.array(data, dtype = _numpy.float)
        if kwargs.has_key("log"):
            ax.loglog(diameter, data, fmt, label = label, lw = lw)
        else:
            ax.semilogx(diameter, data, fmt, label = label, lw = lw)

        ax.set_xlim(xmin = dmin, xmax = dmax)

        ymin, ymax = ax.get_ylim()
        if data.max() > ymax:
            ymax = 1.01 * data.max()
        if data.min() < ymin:
            ymin = max(data.min(), 0.001)

        ax.set_ylim(ymin = ymin, ymax = ymax)

        ax.set_xlabel("diameter $(d_p)$ [$\mu m$]")
        if kwargs.has_key("volume"):
            ax.set_ylabel(ur"$\frac{\,d\log V}{\,d\log d_p}$  $[\mu m^{3}.cm^{-3}]$")
        else:
            ax.set_ylabel(ur"$\frac{\,d\log N}{\,d\log d_p}$  $[\#.cm^{-3}]$")

        ax.yaxis.label.set_size(_pylab.matplotlib.rcParams["axes.labelsize"] + 7)

        if kwargs.has_key("legend"):
            if isinstance(kwargs["legend"], int):
                ax.legend(loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


def GetAvailableDistribution():
    return ModalDistribution.GetAvailableDistribution()


def Init(configuration_file = config_file):
    ModalDistribution.Init(configuration_file)
