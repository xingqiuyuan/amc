#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import ops as _ops
import numpy as _numpy


class ModelThermodynamicBase(_core.ModelThermodynamicBase):
    pass

class ModelThermodynamicVoid(_core.ModelThermodynamicVoid):
    pass

if _core.AMC_HAS_ISOROPIA == 1:
    class ModelThermodynamicIsoropia(_core.ModelThermodynamicIsoropia):

        if _core.AMC_HAS_TEST == 1:
            def Test(self, mode,
                     temperature,
                     relative_humidity,
                     concentration,
                     concentration_in_mol = False):

                output = _core.map_string_real()

                _core.ModelThermodynamicIsoropia.Test(self, mode,
                                                      temperature,
                                                      relative_humidity,
                                                      concentration,
                                                      output,
                                                      concentration_in_mol)
                output = dict(output)

                if output.has_key("Hp"):
                    if output["Hp"] > 0.:
                        output["pH"] = - _numpy.log10(output["Hp"])

                return output


            def TestForward(self,
                            temperature,
                            relative_humidity,
                            concentration,
                            concentration_in_mol = False):
                return self.Test(0, temperature, relative_humidity, concentration, concentration_in_mol)


            def TestReverse(self,
                            temperature,
                            relative_humidity,
                            concentration,
                            concentration_in_mol = False):
                return self.Test(1, temperature, relative_humidity, concentration, concentration_in_mol)
        else:
            pass


if _core.AMC_HAS_PANKOW == 1:
    class ModelThermodynamicPankow(_core.ModelThermodynamicPankow):

        if _core.AMC_HAS_TEST == 1:
            def Test(self, mode,
                     temperature,
                     concentration):

                output = _core.map_string_real()

                _core.ModelThermodynamicPankow.Test(self, mode,
                                                    temperature,
                                                    concentration,
                                                    output)

                output = dict(output)

                return output


            def TestForward(self,
                            temperature,
                            concentration):
                return self.Test(0, temperature, concentration)


            def TestReverse(self,
                            temperature,
                            concentration):
                return self.Test(1, temperature, concentration)
        else:
            pass


if _core.AMC_HAS_AEC == 1:
    class ModelThermodynamicAEC(_core.ModelThermodynamicAEC):
        def GetGammaInfiniteDilution(self):
            gamma_infinite = _core.vector1r()
            _core.ModelThermodynamicAEC.GetGammaInfiniteDilution(self, gamma_infinite)
            return _numpy.array(gamma_infinite, dtype = _numpy.float)


        def ComputeGammaCoefficientHydrophilic(self,
                                               temperature,
                                               concentration_aer_mass):
            gamma_hydrophilic = _core.vector1r()
            _core.ModelThermodynamicAEC.ComputeGammaCoefficientHydrophilic(self,
                                                                           temperature,
                                                                           _numpy.array(concentration_aer_mass, dtype = _numpy.float),
                                                                           gamma_hydrophilic)
            return _numpy.array(gamma_hydrophilic, dtype = _numpy.float)


        def ComputeGammaCoefficientHydrophobic(self,
                                               temperature,
                                               concentration_aer_mass):
            gamma_hydrophobic = _core.vector1r()
            _core.ModelThermodynamicAEC.ComputeGammaCoefficientHydrophobic(self,
                                                                           temperature,
                                                                           _numpy.array(concentration_aer_mass, dtype = _numpy.float),
                                                                           gamma_hydrophobic)
            return _numpy.array(gamma_hydrophobic, dtype = _numpy.float)


        if _core.AMC_HAS_TEST == 1:
            def Test(self, mode,
                     temperature,
                     relative_humidity,
                     concentration):

                output = _core.map_string_real()

                if self.GetName() == "aec":
                    _core.ModelThermodynamicAEC.Test(self, mode,
                                                     temperature,
                                                     relative_humidity,
                                                     _core.map_string_real(concentration),
                                                     output)
                elif self.GetName() == "h2o":
                    _core.ModelThermodynamicH2O.Test(self, mode,
                                                     temperature,
                                                     relative_humidity,
                                                     _core.map_string_real(concentration),
                                                     output)
                else:
                    raise Exception("Model \"" + self.GetName() + "\" not known.")

                output = dict(output)

                return output


            def TestForward(self,
                            temperature,
                            relative_humidity,
                            concentration):
                return self.Test(0, temperature, relative_humidity, concentration)


            def TestReverse(self,
                            temperature,
                            relative_humidity,
                            concentration):
                return self.Test(1, temperature, relative_humidity, concentration)


if _core.AMC_HAS_ISOROPIA == 1 and _core.AMC_HAS_AEC == 1:
    class ModelThermodynamicIsoropiaAEC(_core.ModelThermodynamicIsoropiaAEC):
        pass


if _core.AMC_HAS_H2O == 1:
    class ModelThermodynamicH2O(_core.ModelThermodynamicH2O, ModelThermodynamicAEC):
        pass


if _core.AMC_HAS_ISOROPIA == 1 and _core.AMC_HAS_H2O == 1:
    class ModelThermodynamicIsoropiaH2O(_core.ModelThermodynamicIsoropiaH2O):
        pass


if _core.AMC_HAS_ADSORPTION == 1:
    class ModelAdsorptionBase(_core.ModelAdsorptionBase):
        pass

    class ModelAdsorptionPankow(_core.ModelAdsorptionPankow):

        def GetSpecificSurfaceArea(self):
            specific_surface_area = _core.vector1r()
            _core.ModelAdsorptionPankow.GetSpecificSurfaceArea(self, specific_surface_area)
            return _numpy.array(specific_surface_area, dtype = _numpy.float)
