#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import ops as _ops
import numpy as _numpy


if _core.AMC_HAS_KELVIN_EFFECT == 1:
    class ParameterizationKelvinEffect(_core.ParameterizationKelvinEffect):

        def Test(self, temperature, diameter, liquid_water_content, concentration_aer_mass):
            kelvin_effect = _core.vector1r()
            _core.ParameterizationKelvinEffect.Test(self, temperature, diameter,
                                                    liquid_water_content,
                                                    concentration_aer_mass,
                                                    kelvin_effect)
            return kelvin_effect


class ParameterizationCondensationVoid(_core.ParameterizationCondensationVoid):
    pass

class CondensationCorrectionFactorDahneke(_core.CondensationCorrectionFactorDahneke):
    pass

class CondensationCorrectionFactorFuchsSutugin(_core.CondensationCorrectionFactorFuchsSutugin):
    pass

class CondensationFluxCorrectionAqueousVersion1(_core.CondensationFluxCorrectionAqueousVersion1):
    pass

class CondensationFluxCorrectionAqueousVersion2(_core.CondensationFluxCorrectionAqueousVersion2):
    pass

class CondensationFluxCorrectionDryInorganicSalt(_core.CondensationFluxCorrectionDryInorganicSalt):
    pass

class ParameterizationCondensationDiffusionLimitedBase:
    if _core.AMC_HAS_TEST == 1:
        def TestCoefficient(self,
                            temperature,
                            pressure,
                            diameter):

            condensation_coefficient = _core.vector1r()
            self._TestCoefficient_(temperature, pressure, diameter,
                                   condensation_coefficient)
            return _numpy.array(condensation_coefficient, dtype = _numpy.float)


        def Test(self, diameter, density,
            kelvin_effect, concentration_gas,
            concentration_aer_mass,
            equilibrium_aer_internal,
            equilibrium_gas_surface, **kwargs):

            temperature = 300.
            if kwargs.has_key("temperature"):
                temperature = kwargs["temperature"]

            pressure = 1.013e5
            if kwargs.has_key("pressure"):
                pressure = kwargs["pressure"]

            rate_aer_mass = _core.vector1r()
            self._Test_(temperature, pressure,
                        diameter, density,
                        kelvin_effect, concentration_gas,
                        concentration_aer_mass,
                        equilibrium_aer_internal,
                        equilibrium_gas_surface,
                        rate_aer_mass)
            return _numpy.array(rate_aer_mass, dtype = _numpy.float)
    else:
        pass


class ParameterizationCondensationDiffusionLimitedDahneke(_core.ParameterizationCondensationDiffusionLimitedDahneke,
                                                          ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedFuchsSutugin(_core.ParameterizationCondensationDiffusionLimitedFuchsSutugin,
                                                               ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1(
        _core.ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1(
        _core.ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt(
        _core.ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt(
        _core.ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2(
        _core.ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2(
        _core.ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt(
        _core.ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

class ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt(
        _core.ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt,
        ParameterizationCondensationDiffusionLimitedBase):
    pass

if _core.AMC_HAS_DIFFUSION_LIMITED_SOOT == 1:
    class ParameterizationCondensationDiffusionLimitedSootDahneke(_core.ParameterizationCondensationDiffusionLimitedSootDahneke):
        pass

    class ParameterizationCondensationDiffusionLimitedSootFuchsSutugin(_core.ParameterizationCondensationDiffusionLimitedSootFuchsSutugin):
        pass

    class ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1(_core.ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1):
        pass

    class ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1(_core.ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1):
        pass

    class ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1DryInorganicSalt(_core.ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous1DryInorganicSalt):
        pass

    class ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt(_core.ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt):
        pass

    class ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2(_core.ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2):
        pass

    class ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2(_core.ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2):
        pass

    class ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2DryInorganicSalt(_core.ParameterizationCondensationDiffusionLimitedSootDahnekeFluxCorrectionAqueous2DryInorganicSalt):
        pass

    class ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt(_core.ParameterizationCondensationDiffusionLimitedSootFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt):
        pass
