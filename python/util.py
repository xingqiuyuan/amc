#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import os as _os
import numpy as _numpy

import amc_core as _core
from amc_core import vector1i, vector1r, vector1s, vector2s, vector2r, vector2i, ArrayReal, set_random_generator, \
    convert_ppm_to_molecule_per_cm3, convert_ppt_to_molecule_per_cm3, convert_molecule_per_cm3_to_ppm, \
    convert_molecule_per_cm3_to_ppt

if _core.AMC_HAS_LOGGER == 1:
    class LoggerAMC(_core.AMCLogger):
        pass

if _core.AMC_HAS_TIMER == 1:
    class TimerCPU(_core.TimerCPU):
        @staticmethod
        def Display(i = -1):
            return dict(_core.TimerCPU.Display(i))

    class TimerWall(_core.TimerWall):
        @staticmethod
        def Display(i = -1):
            return dict(_core.TimerWall.Display(i))


if _core.AMC_HAS_STATISTICS == 1:
    class StatisticsAMC(_core.AMCStatistics):
        @staticmethod
        def GetNameList():
            list(_core.AMCStatistics.GetNamelist())

        @staticmethod
        def Display(i = - 1, precision = 6):
            return dict(_core.AMCStatistics.Display(i, precision))


#
# Helper functions for standard nested vectors.
#


def to_array(v, dtype = None):
    if dtype == None:
        if isinstance(v, _core.vector2i):
            dtype = _numpy.int
        else:
            dtype = _numpy.float

    try:
        return _numpy.array(v, dtype = dtype)
    except Exception:
        return [_numpy.array(v[i], dtype = dtype) for i in range(len(v))]


#
# Some useful functions. Though not very handy on the python side. Mainly intended for testing purpose.
#


def hex_to_rgb(hexcolor):
    hexcolor = hexcolor.lstrip("#")
    n = len(hexcolor)
    return tuple([int(hexcolor[i:i+n/3], 16) / 255. for i in range(0, n, n / 3)])

def rgb_to_hex(rgb):
    return "#%02x%02x%02x" % tuple([x * 255. for x in rgb])

def mean_hexcolor(hexcolor):
    rgb = _numpy.array([hex_to_rgb(s) for s in hexcolor], dtype = _numpy.float)
    return rgb_to_hex(rgb.sum(0) / len(rgb))


def generate_random_vector(n, vsum = 1.):
    return _numpy.array(_core.generate_random_vector(n, vsum), dtype = _numpy.float)


def compute_vector_sum(v, n = 0):
    return _core.compute_vector_sum(v, n)


def compute_matrix_determinant(matrix, n):
    return _core.compute_matrix_determinant(matrix, n)


def get_combination(p, n):
    index_reference = _core.vector1i()
    index = _core.vector1i()

    combination = []
    while _core.get_combination(p, n, index_reference, index):
        combination.append(_numpy.array(index, dtype = _numpy.int))
    return _numpy.array(combination)

                           
def find_polygon_convex_hull(p, n, point):
    edge = _core.vector1i()
    _core.find_polygon_convex_hull(p, n, point, edge)
    edge = _numpy.array(edge, dtype = _numpy.int)
    edge.shape = (len(edge) / p, p)
    return edge


def to_movie(path, output_file, **kwargs):
    if not kwargs.has_key("vcodec"):
        kwargs["vcodec"] = "wmv1"

    if not kwargs.has_key("fps"):
        kwargs["fps"] = 2

    if not kwargs.has_key("width"):
        kwargs["width"] = 800

    if not kwargs.has_key("height"):
        kwargs["height"] = 600

    command = "mencoder mf://%s -mf type=png:w=%d:h=%d:fps=%d -ovc lavc -lavcopts vcodec=%s -vf scale=%d:%d -oac copy -o %s" \
        % (path, kwargs["width"], kwargs["height"], kwargs["fps"],
           kwargs["vcodec"], kwargs["width"], kwargs["height"], output_file)

    if kwargs.has_key("debug"):
        sys.stderr.write(command + "\n")

    _os.system(command)
