#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as _core

from discretization import DiscretizationSize as _DiscretizationSize
from species import Species as _Species
import ops as _ops
import copy as _copy
import numpy as _numpy
import pylab as _pylab

#
# Size.
#

class RedistributionSizeBase(_core.RedistributionSizeBase):
    def GetDiameterBoundList(self):
        diameter_bound = _core.vector1r()
        _core.RedistributionSizeBase.GetDiameterBound(self, diameter_bound)
        return _numpy.array(diameter_bound, dtype = _numpy.float)

    def GetDiameterMeanList(self):
        diameter_mean = _core.vector1r()
        _core.RedistributionSizeBase.GetDiameterMean(self, diameter_mean)
        return _numpy.array(diameter_mean, dtype = _numpy.float)

    def Test(self, concentration_aer_number, concentration_aer_mass_total, **kwargs):
        Nsection = _DiscretizationSize.GetNsection()

        section_min = 0
        if kwargs.has_key("section_min"):
            section_min = kwargs["section_min"]

        section_max = Nsection
        if kwargs.has_key("section_max"):
            section_max = kwargs["section_max"]

        normalize = True
        if kwargs.has_key("normalize"):
            normalize = kwargs["normalize"]

        diameter_mean_redist = _core.vector1r(Nsection)
        concentration_aer_number_redist = _core.vector1r(Nsection)
        concentration_aer_mass_total_redist = _core.vector1r(Nsection)

        for i in range(Nsection):
            concentration_aer_number_redist[i] = concentration_aer_number[i]
            concentration_aer_mass_total_redist[i] = concentration_aer_mass_total[i]

        self._Test_(section_min, section_max,
                    concentration_aer_number_redist,
                    concentration_aer_mass_total_redist,
                    diameter_mean_redist, normalize)

        return _numpy.array(concentration_aer_number_redist, dtype = _numpy.float), \
            _numpy.array(concentration_aer_mass_total_redist, dtype = _numpy.float), \
            _numpy.array(diameter_mean_redist, dtype = _numpy.float)


if _core.AMC_HAS_LAYER > 0:
    class RedistributionLayer(_core.RedistributionLayer):
        pass

class RedistributionSizeEulerHybrid(_core.RedistributionSizeEulerHybrid, RedistributionSizeBase):
    pass

class RedistributionSizeEulerMixed(_core.RedistributionSizeEulerMixed, RedistributionSizeBase):
    pass

class RedistributionSizeMovingDiameter(_core.RedistributionSizeMovingDiameter, RedistributionSizeBase):
    pass

class RedistributionSizeTable(RedistributionSizeBase):

    def GetNdisc(self):
        Ndisc = _core.vector1i()
        self._GetNdisc_(Ndisc)
        return _numpy.array(Ndisc, dtype = _numpy.int)


    def SetNdisc(self, Ndisc):
        Ndisc_vect = _core.vector1i(2)
        for i in range(2):
            Ndisc_vect[i] = Ndisc[i]
        self._SetNdisc_(Ndisc)


    def GetTable(self):
        table_section_index = _core.vector2i()
        table_coefficient_number = _core.vector2r()
        table_coefficient_mass = _core.vector2r()

        self._GetTable_(table_section_index,
                        table_coefficient_number,
                        table_coefficient_mass)

        table = []
        for i in range(self.GetNtable()):
            table.append([_numpy.array(table_section_index[i], dtype = _numpy.int),
                          _numpy.array(table_coefficient_number[i], dtype = _numpy.float),
                          _numpy.array(table_coefficient_mass[i], dtype = _numpy.float)])
        return table


class RedistributionSizeTableEulerHybrid(_core.RedistributionSizeTableEulerHybrid, RedistributionSizeTable):
    pass


class RedistributionSizeTableEulerMixed(_core.RedistributionSizeTableEulerMixed, RedistributionSizeTable):
    pass


#
# Composition.
#


class RedistributionCompositionBaseBase(_core.RedistributionCompositionBaseBase):
    pass

class RedistributionCompositionBaseTable(_core.RedistributionCompositionBaseTable):
    pass

class RedistributionCompositionTableBase(_core.RedistributionCompositionTableBase):
    pass

class RedistributionCompositionTableTable(_core.RedistributionCompositionTableTable):
    pass


class RedistributionCompositionNestedCommon:

    def GetPolygonPointList(self, i):
        point_index = _core.vector1i()
        self.CollectPolygonPointList(i, point_index)
        return _numpy.array(point_index, dtype = _numpy.int)


    def GetPointCoordinate(self, i):
        coordinate = _core.vector1r()
        self.CollectPointCoordinate(i, coordinate)
        return _numpy.array(coordinate, dtype = _numpy.float)


    def GetPolygonPointCoordinateList(self):
        table = []
        for i in range(self.GetNpolygon()):
            tab = []
            point_index = self.GetPolygonPointList(i)
            for j in point_index:
                tab.append(self.GetPointCoordinate(int(j)))
            table.append(tab)
        return table


    def Plot2d(self, fig = None, **kwargs):
        discretization_composition_ptr = self.GetDiscretizationCompositionPtr()

        if discretization_composition_ptr.GetNdim() != 2:
            raise Exception("Composition discretization is not 2D.")

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 12
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        if kwargs.has_key("line_width"):
            line_width = kwargs["line_width"]
        else:
            line_width = 3

        if kwargs.has_key("point_size"):
            point_size = kwargs["point_size"]
        else:
            point_size = 40

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig != None:
            ax = fig.gca()
        else:
            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)

        polygon_point_list = self.GetPolygonPointCoordinateList()

        for point_list in polygon_point_list:
            x = [p[0] for p in point_list]
            y = [p[1] for p in point_list]
            x.append(point_list[0][0])
            y.append(point_list[0][1])

            ax.plot(x, y, color = "k", linestyle = "-", linewidth = line_width)
            ax.scatter(x, y, point_size, color = "k", marker = "o", zorder = 10)

        ax.set_xlim(xmin = -0.1, xmax = 1.1)
        ax.set_ylim(ymin = -0.1, ymax = 1.1)
        _pylab.axes().set_aspect(1., "box")

        if kwargs.has_key("title"):
            title = kwargs["title"]
            ax.set_title(title)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    if _core.AMC_HAS_TEST == 1:
        def TestSelf(self, concentration_aer_mass):
            if isinstance(concentration_aer_mass, list):
                concentration_aer_mass = _numpy.array(concentration_aer_mass, dtype = _numpy.float)

            Nclass_total = self.GetDiscretizationCompositionNestedPtr().GetNclassTotal()

            index_internal_mixing_vect = _core.vector1i(Nclass_total, 0)
            index_class_vect = _core.vector1i(Nclass_total, 0)
            concentration_mass_class_vect = _core.vector2r(self.GetNclassTotal(), _core.vector1r(_Species.GetNspecies() + 1, 0.))

            Nclass = self._TestSelf_(concentration_aer_mass,
                                     index_internal_mixing_vect,
                                     index_class_vect,
                                     concentration_mass_class_vect)

            table = []
            for i in range(Nclass):
                j = index_class_vect[i]
                k = index_internal_mixing_vect[i]
                name = self.GetDiscretizationCompositionNestedPtr().GetClassInternalMixing(k).GetName()
                vect = _numpy.array(concentration_mass_class_vect[j], dtype = _numpy.float)
                tab = [k, name, vect[-1], vect[:-1]]
                table.append(tab)

            return table


class RedistributionCompositionNestedBase(_core.RedistributionCompositionNestedBase,
                                          RedistributionCompositionNestedCommon):
    pass

class RedistributionCompositionNestedBaseTable(_core.RedistributionCompositionNestedBaseTable,
                                               RedistributionCompositionNestedCommon):
    pass

class RedistributionCompositionNestedTableBase(_core.RedistributionCompositionNestedTableBase,
                                               RedistributionCompositionNestedCommon):
    pass

class RedistributionCompositionNestedTable(_core.RedistributionCompositionNestedTable,
                                           RedistributionCompositionNestedCommon):
    pass


#
# Class
#


class RedistributionClassBase(_core.RedistributionClassBase):
    pass

class RedistributionClassTable(_core.RedistributionClassTable):
    pass

class RedistributionClassBaseTable(_core.RedistributionClassBaseTable):
    pass

class RedistributionClassTableBase(_core.RedistributionClassTableBase):
    pass


#
# Redistribution main class
#


class RedistributionSizeMovingCompositionBase(_core.RedistributionSizeMovingCompositionBase):
    pass

class RedistributionSizeMovingCompositionBaseTable(_core.RedistributionSizeMovingCompositionBaseTable):
    pass

class RedistributionSizeMovingCompositionTable(_core.RedistributionSizeMovingCompositionTable):
    pass

class RedistributionSizeMixedCompositionBase(_core.RedistributionSizeMixedCompositionBase):
    pass

class RedistributionSizeHybridCompositionBase(_core.RedistributionSizeHybridCompositionBase):
    pass

class RedistributionSizeMixedTableCompositionBase(_core.RedistributionSizeMixedTableCompositionBase):
    pass

class RedistributionSizeHybridTableCompositionBase(_core.RedistributionSizeHybridTableCompositionBase):
    pass

class RedistributionSizeMixedCompositionBaseTable(_core.RedistributionSizeMixedCompositionBaseTable):
    pass

class RedistributionSizeHybridCompositionBaseTable(_core.RedistributionSizeHybridCompositionBaseTable):
    pass

class RedistributionSizeMixedTableCompositionBaseTable(_core.RedistributionSizeMixedTableCompositionBaseTable):
    pass

class RedistributionSizeHybridTableCompositionBaseTable(_core.RedistributionSizeHybridTableCompositionBaseTable):
    pass

class RedistributionSizeMixedCompositionTable(_core.RedistributionSizeMixedCompositionTable):
    pass

class RedistributionSizeHybridCompositionTable(_core.RedistributionSizeHybridCompositionTable):
    pass

class RedistributionSizeMixedTableCompositionTable(_core.RedistributionSizeMixedTableCompositionTable):
    pass

class RedistributionSizeHybridTableCompositionTable(_core.RedistributionSizeHybridTableCompositionTable):
    pass
