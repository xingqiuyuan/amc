#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import numpy as _numpy
from util import to_array as _to_array


class DynamicsBase(_core.DynamicsBase):

    @staticmethod
    def ComputeTimeScale(concentration, rate):
        time_scale_index = _core.vector1i()
        time_scale_value = _core.vector1r()

        _core.DynamicsBaseBase.ComputeTimeScale(concentration, rate,
                                                time_scale_index,
                                                time_scale_value)

        return _numpy.array(time_scale_index, dtype = _numpy.int), \
            _numpy.array(time_scale_value, dtype = _numpy.float)

    @staticmethod
    def ComputeTimeScaleAerosolNumber(concentration, rate):
        return dict(_core.DynamicsBaseBase.ComputeTimeScaleAerosolNumber(concentration, rate))

    @staticmethod
    def ComputeTimeScaleAerosolMass(concentration, rate):
        return dict(_core.DynamicsBaseBase.ComputeTimeScaleAerosolMass(concentration, rate))

    @staticmethod
    def ComputeTimeScaleGas(concentration, rate):
        return dict(_core.DynamicsBaseBase.ComputeTimeScaleGas(concentration, rate))

    @staticmethod
    def RemoveUnstableParticle(concentration_aer_number,
                               concentration_aer_mass,
                               concentration_gas,
                               section_min = 0, section_max = -1):
        if section_max < 0:
            section_max = _core.AerosolData.GetNg()

        _core.DynamicsBaseBase.RemoveUnstableParticle(section_min, section_max,
                                                      concentration_aer_number,
                                                      concentration_aer_mass,
                                                      concentration_gas)


#
# Coagulation.
#


if _core.AMC_HAS_COAGULATION == 1:
    from coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencyVoid \
        as _ParameterizationCoagulationGravitationalCollisionEfficiencyVoid
    from coagulation import ParameterizationCoagulationTurbulent as _ParameterizationCoagulationTurbulent
    from coagulation import ParameterizationCoagulationBrownian as _ParameterizationCoagulationBrownian
    from coagulation import ParameterizationCoagulationVoid as _ParameterizationCoagulationVoid
    from coagulation import ParameterizationCoagulationUnit as _ParameterizationCoagulationUnit
    from coagulation import ParameterizationCoagulationTable as _ParameterizationCoagulationTable

    if _core.AMC_HAS_COLLISION_EFFICIENCY == 1:
        from coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander as \
            _ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander
        from coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencySeinfeld as \
            _ParameterizationCoagulationGravitationalCollisionEfficiencySeinfeld
        from coagulation import ParameterizationCoagulationGravitationalCollisionEfficiencyJacobson as \
            _ParameterizationCoagulationGravitationalCollisionEfficiencyJacobson

    if _core.AMC_HAS_WAALS_VISCOUS == 1:
        from coagulation import ParameterizationCoagulationBrownianWaalsViscous as \
            _ParameterizationCoagulationBrownianWaalsViscous


    class DynamicsCoagulationStaticCoefficientBase(_core.DynamicsCoagulationStaticCoefficientBase, DynamicsBase):

        @staticmethod
        def GetParameterizationCouple(i):
            couple_vect = _core.vector2i()
            _core.DynamicsCoagulationStaticCoefficientBase.GetParameterizationCouple(i, couple_vect)
            return _numpy.array([_numpy.array(ls, dtype = _numpy.int) for ls in couple_vect], dtype = _numpy.int)


        @staticmethod
        def GetParameterization(i):
            param_ptr = _core.DynamicsCoagulationStaticCoefficientBase.GetParameterizationPtr(i)
            if  param_ptr.GetName() == "Brownian":
                param = _ParameterizationCoagulationBrownian()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Waals/Viscous brownian" and _core.AMC_HAS_WAALS_VISCOUS == 1:
                param = _ParameterizationCoagulationBrownianWaalsViscous()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Turbulent":
                param = _ParameterizationCoagulationTurbulent()
                param.Copy(param_ptr)
            elif "Gravitational" in param_ptr.GetName() and _core.AMC_HAS_COLLISION_EFFICIENCY == 1:
                if "Friedlander" in param_ptr.GetName():
                    param = _ParameterizationCoagulationGravitationalFriedlander()
                    param.Copy(param_ptr)
                elif "Seinfeld" in param_ptr.GetName():
                    param = _ParameterizationCoagulationGravitationalSeinfeld()
                    param.Copy(param_ptr)
                elif "Jacobson" in param_ptr.GetName():
                    param = _ParameterizationCoagulationGravitationalJacobson()
                    param.Copy(param_ptr)
                else:
                    raise ValueError("Parameterization \"" + param_ptr.GetName() + "\" not known.")
            elif param_ptr.GetName() == "Void":
                param = _ParameterizationCoagulationVoid()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Unit":
                param = _ParameterizationCoagulationUnit()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Table":
                param = _ParameterizationCoagulationTable()
                param.Copy(param_ptr)
            else:
                raise ValueError("Parameterization name \"" + param_ptr.GetName() + "\" is currently"
                                 + " not implemented in python interface.")
            return param


        @staticmethod
        def Rate(concentration_aer_number, concentration_aer_mass, section_min = 0, section_max = -1):
            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            rate_aer_number = _core.vector1r(_core.AerosolData.GetNg(), 0.)
            rate_aer_mass = _core.vector1r(_core.AerosolData.GetNgNspecies(), 0.)

            _core.DynamicsCoagulationStaticCoefficientBase.Rate(section_min, section_max,
                                                                concentration_aer_number,
                                                                concentration_aer_mass,
                                                                rate_aer_number, rate_aer_mass)
            return _numpy.array(rate_aer_number, dtype = _numpy.float), \
                _numpy.array(rate_aer_mass, dtype = _numpy.float)


    class DynamicsCoagulationStaticCoefficientTable(_core.DynamicsCoagulationStaticCoefficientTable, DynamicsBase):
        pass


    class DynamicsCoagulationMovingCoefficientBase(_core.DynamicsCoagulationMovingCoefficientBase, DynamicsBase):

        @staticmethod
        def GetParameterizationCouple(i):
            couple_vect = _core.vector2i()
            _core.DynamicsCoagulationStaticCoefficientBase.GetParameterizationCouple(i, couple_vect)
            return _numpy.array([_numpy.array(ls, dtype = _numpy.int) for ls in couple_vect], dtype = _numpy.int)


        @staticmethod
        def GetParameterization(i):
            param_ptr = _core.DynamicsCoagulationStaticCoefficientBase.GetParameterizationPtr(i)
            if  param_ptr.GetName() == "Brownian":
                param = _ParameterizationCoagulationBrownian()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Waals/Viscous brownian" and _core.AMC_HAS_WAALS_VISCOUS == 1:
                param = _ParameterizationCoagulationBrownianWaalsViscous()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Turbulent":
                param = _ParameterizationCoagulationTurbulent()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Gravitational":
                param = _ParameterizationCoagulationGravitational()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Void":
                param = _ParameterizationCoagulationVoid()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Unit":
                param = _ParameterizationCoagulationUnit()
                param.Copy(param_ptr)
            elif param_ptr.GetName() == "Table":
                param = _ParameterizationCoagulationTable()
                param.Copy(param_ptr)
            else:
                raise ValueError("Parameterization name \"" + param_ptr.GetName() + "\" is currently"
                                 + " not implemented in python interface")
            return param


        @staticmethod
        def Rate(concentration_aer_number, concentration_aer_mass, section_min = 0, section_max = -1):
            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            rate_aer_number = _core.vector1r(_core.AerosolData.GetNg(), 0.)
            rate_aer_mass = _core.vector1r(_core.AerosolData.GetNgNspecies(), 0.)

            _core.DynamicsCoagulationMovingCoefficientBase.Rate(section_min, section_max,
                                                                concentration_aer_number,
                                                                concentration_aer_mass,
                                                                rate_aer_number, rate_aer_mass)
            return _numpy.array(rate_aer_number, dtype = _numpy.float), \
                _numpy.array(rate_aer_mass, dtype = _numpy.float)


    class DynamicsCoagulationMovingCoefficientTable(_core.DynamicsCoagulationMovingCoefficientTable, DynamicsBase):
        pass


#
# Condensation.
#


if _core.AMC_HAS_CONDENSATION == 1:

    from condensation import ParameterizationCondensationVoid as _ParameterizationCondensationVoid

    from condensation import ParameterizationCondensationDiffusionLimitedDahneke as \
        _ParameterizationCondensationDiffusionLimitedDahneke
    from condensation import ParameterizationCondensationDiffusionLimitedFuchsSutugin as \
        _ParameterizationCondensationDiffusionLimitedFuchsSutugin

    if _core.AMC_HAS_FLUX_CORRECTION_AQUEOUS == 1:
        from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1 as \
            _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1
        from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2 as \
            _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2

        from condensation import ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1 as \
            _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1
        from condensation import ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2 as \
            _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2

        if _core.AMC_HAS_FLUX_CORRECTION_DRY == 1:
            from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt as \
                _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt
            from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt as \
                _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt

            from condensation import \
                ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt as \
                _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt
            from condensation import \
                ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt as \
                _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt

    if _core.AMC_HAS_DIFFUSION_LIMITED_SOOT == 1:
        if _core.AMC_HAS_FLUX_CORRECTION_AQUEOUS == 1:
            from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1 as \
                _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1
            from condensation import ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2 as \
                _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2

            from condensation import ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1 as \
                _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1
            from condensation import ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2 as \
                _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2

            if _core.AMC_HAS_FLUX_CORRECTION_DRY == 1:
                from condensation import \
                    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt as \
                    _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous1DryInorganicSalt
                from condensation import \
                    ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt as \
                    _ParameterizationCondensationDiffusionLimitedDahnekeFluxCorrectionAqueous2DryInorganicSalt

                from condensation import \
                    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt as \
                    _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous1DryInorganicSalt
                from condensation import \
                    ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt as \
                    _ParameterizationCondensationDiffusionLimitedFuchsSutuginFluxCorrectionAqueous2DryInorganicSalt

    from species import Species as _Species

    class DynamicsCondensation(_core.DynamicsCondensation, DynamicsBase):

        @staticmethod
        def RateLagrangian(concentration_aer_number, concentration_aer_mass,
                           concentration_gas, section_min = 0, section_max = -1):
            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            rate_gas = _core.vector1r(_Species.GetNgas(), 0.)
            rate_aer_mass = _core.vector1r(_core.AerosolData.GetNgNspecies(), 0.)

            _core.DynamicsCondensation.RateLagrangian(section_min, section_max,
                                                      concentration_aer_number,
                                                      concentration_aer_mass,
                                                      concentration_gas,
                                                      rate_aer_mass, rate_gas)
            return _numpy.array(rate_aer_mass, dtype = _numpy.float), \
                _numpy.array(rate_gas, dtype = _numpy.float)


        @staticmethod
        def RateLagrangian(concentration_aer_number, concentration_aer_mass,
                           concentration_gas, rate_aer_mass, rate_gas,
                           section_min = 0, section_max = -1):
            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            _core.DynamicsCondensation.RateLagrangian(section_min, section_max,
                                                      concentration_aer_number,
                                                      concentration_aer_mass,
                                                      concentration_gas,
                                                      rate_aer_mass, rate_gas)


        @staticmethod
        def ComputeTimeScaleGrowth(concentration, rate):
            return dict(_core.DynamicsCondensation.ComputeTimeScaleGrowth(concentration, rate))


#
# Nucleation.
#


if _core.AMC_HAS_NUCLEATION == 1:

    from nucleation import ParameterizationNucleationBase as _ParameterizationNucleationBase
    from nucleation import ParameterizationNucleationPowerLaw as _ParameterizationNucleationPowerLaw
    from nucleation import ParameterizationNucleationVoid as _ParameterizationNucleationVoid

    class DynamicsNucleation(_core.DynamicsNucleation, DynamicsBase):
        @staticmethod
        def GetParameterizationIndex():
            parameterization_index = _core.vector1i()
            _core.DynamicsNucleationBase.GetParameterizationIndex(parameterization_index)
            return _numpy.array(parameterization_index, dtype = _numpy.int)


#
# Diffusion.
#


if _core.AMC_HAS_DIFFUSION > 0:
    class DynamicsDiffusion(_core.DynamicsDiffusion):
        pass
