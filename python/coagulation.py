#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.

import amc_core as _core

import numpy as _numpy
import pylab as _pylab
#from matplotlib import ticker, cm
from matplotlib.colors import LogNorm as _LogNorm


class ParameterizationCoagulationBase(_core.ParameterizationCoagulationBase):

    def GetVariableName(self):
        variable_name = _core.vector1s()
        _core.ParameterizationCoagulationBase.GetVariableName(self, variable_name)
        return list(variable_name)


    def TestKernel(self, variable, **kwargs):
        dmin1 = 0.001
        if kwargs.has_key("dmin1"):
            dmin1 = kwargs["dmin1"]

        dmax1 = 10.
        if kwargs.has_key("dmax1"):
            dmax1 = kwargs["dmax1"]

        dmin2 = 0.001
        if kwargs.has_key("dmin2"):
            dmin2 = kwargs["dmin2"]

        dmax2 = 10.
        if kwargs.has_key("dmax2"):
            dmax2 = kwargs["dmax2"]

        N1 = 100
        if kwargs.has_key("N1"):
            N1 = kwargs["N1"]

        N2 = 100
        if kwargs.has_key("N2"):
            N2 = kwargs["N2"]

        q1 = dmax1 / dmin1;
        if kwargs.has_key("diameter1"):
            diameter1 = kwargs["diameter1"]
            if not isinstance(diameter1, list):
                diameter1 = [diameter1]
        else:
            diameter1 = _numpy.array([dmin1 * _numpy.power(q1, float(i) / float(N1 - 1)) for i in range(N1)],
                                     dtype = _numpy.float)

        q2 = dmax2 / dmin2;
        if kwargs.has_key("diameter2"):
            diameter2 = kwargs["diameter2"]
            if not isinstance(diameter2, list):
                diameter2 = [diameter2]
        else:
            diameter2 = _numpy.array([dmin2 * _numpy.power(q2, float(i) / float(N2 - 1)) for i in range(N2)],
                                     dtype = _numpy.float)

        N1 = len(diameter1)
        N2 = len(diameter2)
        kernel = _numpy.zeros((N1, N2), dtype = _numpy.float)

        for i in range(N1):
            for j in range(N2):
                variable[0] = diameter1[i]
                variable[1] = diameter2[j]
                kernel[i, j] = self.KernelTable(variable)

        return diameter1, diameter2, kernel


    def Plot1d(self, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 4
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 2
        _pylab.matplotlib.rcParams["ytick.major.size"] = 4
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 2

        if fig != None:
            ax = fig.gca()
        else:
            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)

        if not kwargs.has_key("diameter1"):
            kwargs["diameter1"] = 1.

        diameter1, diameter2, kernel = self.TestKernel(**kwargs)
        kernel = kernel[0,:]

        if kwargs.has_key("fmt"):
            fmt = kwargs["fmt"]
        else:
            fmt = "k-"

        if kwargs.has_key("lw"):
            lw = kwargs["lw"]
        else:
            lw = 2

        if kwargs.has_key("label"):
            label = kwargs["label"]
        else:
            label = self.GetName()

        ax.loglog(diameter2, kernel, fmt, label = label, lw = lw)

        ax.set_xlim(diameter2.min(), diameter2.max())
        ax.set_ylim(kernel.min(), kernel.max())

        ax.set_xlabel(ur"diameter [$\mu m$]")
        ax.set_ylabel(ur"coagulation kernel [$m^3.s^{-1}$]")

        if kwargs.has_key("legend"):
            ax.legend(loc = kwargs["legend"])

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    def Plot2d(self, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 4
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 2
        _pylab.matplotlib.rcParams["ytick.major.size"] = 4
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 2

        _pylab.clf()
        fig = _pylab.figure()
        ax = fig.add_subplot(111)

        diameter1, diameter2, kernel = self.TestKernel(**kwargs)

        level = 30
        if kwargs.has_key("level"):
            level = kwargs["level"]

        level_min = max(1.e-20, kernel.min())
        if kwargs.has_key("level_min"):
            level_min = kwargs["level_min"]

        level_max = kernel.max()
        if kwargs.has_key("level_max"):
            level_max = kwargs["level_max"]

        if isinstance(level, int):
            level_max = kernel.max()
            level_min = _numpy.floor(_numpy.floor(_numpy.log10(level_min)))
            level_max = _numpy.floor(_numpy.ceil(_numpy.log10(level_max)))
            level = _numpy.logspace(level_min, level_max, level)

        cs = ax.contourf(diameter1, diameter2, kernel, level, norm = _LogNorm())
        #cs = ax.contourf(diam, diam, kernel, levels, locator = ticker.LogLocator())
        ax.set_xscale("log")
        ax.set_yscale("log")
        ax.set_xlabel(ur"diameter [$\mu m$]")
        ax.set_ylabel(ur"diameter [$\mu m$]")
        if kwargs.has_key("zticks"):
            zticks = kwargs["zticks"]
        else:
            zticks = level
        cbar = _pylab.colorbar(cs, ticks = zticks)
        cbar.ax.set_ylabel(ur"[$m^3.s^{-1}$]")

        if kwargs.has_key("title"):
            title = kwargs["title"]
        else:
            title = self.GetName() + " for two particles."

        ax.set_title(title)

        _pylab.axes().set_aspect(1., "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


class ParameterizationCoagulationUnit(_core.ParameterizationCoagulationUnit,
                                      ParameterizationCoagulationBase):
    pass


class ParameterizationCoagulationVoid(_core.ParameterizationCoagulationVoid,
                                      ParameterizationCoagulationBase):
    pass


class ParameterizationCoagulationGravitationalCollisionEfficiencyVoid(_core.ParameterizationCoagulationGravitationalCollisionEfficiencyVoid, ParameterizationCoagulationBase):
    def TestKernel(self, **kwargs):
        temperature = 300.0
        if kwargs.has_key("temperature"):
            temperature = kwargs["temperature"]

        pressure = 1.01325e5
        if kwargs.has_key("pressure"):
            pressure = kwargs["pressure"]

        relative_humidity = 0.7
        if kwargs.has_key("rh"):
            relative_humidity = kwargs["rh"]

        variable = _core.vector1r(self.GetNvariable(), 0)
        variable[2] = temperature
        variable[3] = pressure
        variable[4] = relative_humidity

        return ParameterizationCoagulationBase.TestKernel(self, variable, **kwargs)


if _core.AMC_HAS_COLLISION_EFFICIENCY == 1:
    class ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander(_core.ParameterizationCoagulationGravitationalCollisionEfficiencyFriedlander, ParameterizationCoagulationGravitationalCollisionEfficiencyVoid):
        pass

    class ParameterizationCoagulationGravitationalCollisionEfficiencySeinfeld(_core.ParameterizationCoagulationGravitationalCollisionEfficiencySeinfeld, ParameterizationCoagulationGravitationalCollisionEfficiencyVoid):
        pass

    class ParameterizationCoagulationGravitationalCollisionEfficiencyJacobson(_core.ParameterizationCoagulationGravitationalCollisionEfficiencyJacobson, ParameterizationCoagulationGravitationalCollisionEfficiencyVoid):
        pass


class ParameterizationCoagulationTurbulent(_core.ParameterizationCoagulationTurbulent,
                                           ParameterizationCoagulationBase):
    if _core.AMC_HAS_TEST == 1:
        def TestKernel(self, **kwargs):
            temperature = 300.0
            if kwargs.has_key("temperature"):
                temperature = kwargs["temperature"]

            pressure = 1.01325e5
            if kwargs.has_key("pressure"):
                pressure = kwargs["pressure"]

            relative_humidity = 0.7
            if kwargs.has_key("rh"):
                relative_humidity = kwargs["rh"]

            eddy_dissipation_rate = 5.e-4
            if kwargs.has_key("eddy_dissipation_rate"):
                eddy_dissipation_rate = kwargs["eddy_dissipation_rate"]

            variable = _core.vector1r(self.GetNvariable(), 0)
            variable[2] = temperature
            variable[3] = pressure
            variable[4] = relative_humidity
            variable[5] = eddy_dissipation_rate

            return ParameterizationCoagulationBase.TestKernel(self, variable, **kwargs)
    else:
        pass


class ParameterizationCoagulationBrownian(_core.ParameterizationCoagulationBrownian,
                                          ParameterizationCoagulationBase):
    if _core.AMC_HAS_TEST == 1:
        def TestKernel(self, **kwargs):
            temperature = 300.0
            if kwargs.has_key("temperature"):
                temperature = kwargs["temperature"]
            
            pressure = 1.01325e5
            if kwargs.has_key("pressure"):
                pressure = kwargs["pressure"]

            if kwargs.has_key("knmin"):
                self.SetKnudsenMin(kwargs["knmin"])

            if kwargs.has_key("knmax"):
                self.SetKnudsenMax(kwargs["knmax"])

            variable = _core.vector1r(self.GetNvariable(), 0)
            variable[2] = temperature
            variable[3] = pressure

            return ParameterizationCoagulationBase.TestKernel(self, variable, **kwargs)


class ParameterizationCoagulationTable(_core.ParameterizationCoagulationTable,
                                       ParameterizationCoagulationBase):

    def GetDisc(self, i):
        disc = _core.vector1r()
        _core.ParameterizationCoagulationTable.GetDisc(self, i, disc)
        return _numpy.array(disc, dtype= _numpy.float)

    def GetNdisc(self):
        Ndisc = _core.vector1i(self.GetNvariable())
        _core.ParameterizationCoagulationTable.GetNdisc(self, Ndisc)
        return _numpy.array(Ndisc, dtype= _numpy.int)

    def GetNpoint(self):
        Npoint = _core.vector1i(self.GetNvariable())
        _core.ParameterizationCoagulationTable.GetNpoint(self, Npoint)
        return _numpy.array(Npoint, dtype= _numpy.int)

    def GetTable(self, raw = False):
        kernel = _core.vector1r()
        _core.ParameterizationCoagulationTable.GetTable(self, kernel)
        kernel = _numpy.array(kernel, dtype= _numpy.float)
        if not raw:
            kernel.shape = self.GetNpoint()[::-1]
        return kernel

    if _core.AMC_HAS_TEST == 1:
        def TestKernel(self, **kwargs):
            temperature = 300.0
            if kwargs.has_key("temperature"):
                temperature = kwargs["temperature"]

            pressure = 1.01325e5
            if kwargs.has_key("pressure"):
                pressure = kwargs["pressure"]

            relative_humidity = 0.7
            if kwargs.has_key("rh"):
                relative_humidity = kwargs["rh"]

            eddy_dissipation_rate = 5.e-4
            if kwargs.has_key("eddy_dissipation_rate"):
                eddy_dissipation_rate = kwargs["eddy_dissipation_rate"]

            variable = _core.vector1r(_core.AMC_COAGULATION_TABLE_NUMBER_VARIABLE_MAX, 0)
            variable[2] = temperature
            variable[3] = pressure
            variable[4] = relative_humidity
            variable[5] = eddy_dissipation_rate

            return ParameterizationCoagulationBase.TestKernel(self, variable, **kwargs)


if _core.AMC_HAS_WAALS_VISCOUS == 1:
    class ParameterizationCoagulationBrownianWaalsViscous(_core.ParameterizationCoagulationBrownianWaalsViscous,
                                                          ParameterizationCoagulationBrownian,
                                                          ParameterizationCoagulationBase):
        if _core.AMC_HAS_TEST == 1:
            def TestKernel(self, **kwargs):
                if kwargs.has_key("hamaker"):
                    self.SetHamaker(kwargs["hamaker"])

                if kwargs.has_key("Nquadrature"):
                    self.SetNquadrature(kwargs["Nquadrature"])

                return ParameterizationCoagulationBrownian.TestKernel(self, **kwargs)


            def TestCorrectionFactor(self, **kwargs):
                if kwargs.has_key("hamaker"):
                    self.SetHamaker(kwargs["hamaker"])

                if kwargs.has_key("Nquadrature"):
                    self.SetNquadrature(kwargs["Nquadrature"])

                temperature = 300.0
                if kwargs.has_key("temperature"):
                    temperature = kwargs["temperature"]

                pressure = 1.01325e5
                if kwargs.has_key("pressure"):
                    pressure = kwargs["pressure"]

                variable = _core.vector1r(self.GetNvariable(), 0)
                variable[2] = temperature
                variable[3] = pressure

                dmin = 0.001
                if kwargs.has_key("dmin"):
                    dmin = kwargs["dmin"]

                dmax = 10.
                if kwargs.has_key("dmax"):
                    dmax = kwargs["dmax"]

                N = 1000
                if kwargs.has_key("N"):
                    N = kwargs["N"]

                q = dmax / dmin
                if kwargs.has_key("diameter"):
                    diameter = kwargs["diameter"]
                    if not isinstance(diameter, list):
                        diameter = [diameter]
                else:
                    diameter = _numpy.array([dmin * _numpy.power(q, float(i) / float(N - 1)) for i in range(N)],
                                            dtype = _numpy.float)

                N = len(diameter)
                correction_factor = _numpy.zeros(N, dtype = _numpy.float)
                for i in range(N):
                    variable[0] = diameter[i]
                    variable[1] = diameter[i]
                    correction_factor[i] = self.CorrectionFactorTable(variable)

                return diameter, correction_factor
        else:
            pass
