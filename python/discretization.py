#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core
from amc_core import AMC_PARTICLE_DENSITY_DEFAULT

import ops as _ops
import numpy as _numpy
import pylab as _pylab
import copy as _copy
from util import to_array as _to_array
from matplotlib.patches import Polygon as _Polygon
from matplotlib.collections import PatchCollection as _PatchCollection
from matplotlib.colors import LogNorm as _LogNorm
from matplotlib.ticker import LogFormatter as _LogFormatter


class DiscretizationSize(_core.DiscretizationSize):
    @staticmethod
    def GetDiameterBound():
        diameter_bound = _core.vector1r()
        _core.DiscretizationSize_GetDiameterBound(diameter_bound)
        return _numpy.array(diameter_bound, dtype = _numpy.float)


    @staticmethod
    def GetDiameterBoundLog():
        diameter_bound_log = _core.vector1r()
        _core.DiscretizationSize_GetDiameterBoundLog(diameter_bound_log)
        return _numpy.array(diameter_bound_log, dtype = _numpy.float)


    @staticmethod
    def GetDiameterWidthLog():
        diameter_width_log = _core.vector1r()
        _core.DiscretizationSize_GetDiameterWidthLog(diameter_width_log)
        return _numpy.array(diameter_width_log, dtype = _numpy.float)


    @staticmethod
    def GetDiameterBoundLog10():
        diameter_bound_log10 = _core.vector1r()
        _core.DiscretizationSize_GetDiameterBoundLog10(diameter_bound_log10)
        return _numpy.array(diameter_bound_log10, dtype = _numpy.float)


    @staticmethod
    def GetDiameterWidthLog10():
        diameter_width_log10 = _core.vector1r()
        _core.DiscretizationSize_GetDiameterWidthLog10(diameter_width_log10)
        return _numpy.array(diameter_width_log10, dtype = _numpy.float)


    @staticmethod
    def GetDiameterMean():
        diameter_mean = _core.vector1r()
        _core.DiscretizationSize_GetDiameterMean(diameter_mean)
        return _numpy.array(diameter_mean, dtype = _numpy.float)


    @staticmethod
    def GetMassMean():
        mass_mean = _core.vector1r()
        _core.DiscretizationSize_GetMassMean(mass_mean)
        return _numpy.array(mass_mean, dtype = _numpy.float)


    @staticmethod
    def GetMassBound():
        mass_bound = _core.vector1r()
        _core.DiscretizationSize_GetMassBound(mass_bound)
        return _numpy.array(mass_bound, dtype = _numpy.float)


    @staticmethod
    def PlotInternalMixingNumberConcentration(concentration_aer_num, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        if kwargs.has_key("lw"):
            lw = kwargs["lw"]
        else:
            lw = 1

        if kwargs.has_key("fmt"):
            fmt = kwargs["fmt"]
        else:
            fmt = "k-o"

        if kwargs.has_key("dNdlogDp"):
            concentration_aer_num = _numpy.array([x / y for x, y in zip(concentration_aer_num,
                                                                        DiscretizationSize.GetDiameterWidthLog())])

        if kwargs.has_key("label"):
            ax.loglog(DiscretizationSize.GetDiameterMean(), concentration_aer_num, fmt, label = kwargs["label"], lw = lw)
        else:
            ax.loglog(DiscretizationSize.GetDiameterMean(), concentration_aer_num, fmt, lw = lw)

        xloc = DiscretizationSize.GetDiameterBound()
        xlabel = ["%2.1g" % x for x in xloc]
        #ax.set_xticks(xloc)
        #ax.set_xticklabels(xlabel)

        ax.set_xlim(xloc[0], xloc[-1])
        ax.set_xlabel(ur"diameter [$\mu m$]")

        if kwargs.has_key("threshold"):
            threshold = kwargs["threshold"]
        else:
            threshold = 1. # \#.m^{-3}

        ax.set_ylim(threshold, concentration_aer_num.max() * 2)
        if kwargs.has_key("ylabel"):
            ax.set_ylabel(kwargs["ylabel"])
        else:
            ax.set_ylabel(ur"number concentration  $[\#.m^{-3}]$")

        if kwargs.has_key("legend"):
            if isinstance(kwargs["legend"], int):
                ax.legend(loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("Legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotInternalMixingMassConcentration(concentration_aer_mass, fig = None, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig == None:
            _pylab.clf()
            fig = _pylab.figure()

            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.add_subplot(111)
        else:
            if kwargs.has_key("subplot"):
                ax = fig.add_subplot(kwargs["subplot"])
            else:
                ax = fig.gca()

        if kwargs.has_key("lw"):
            lw = kwargs["lw"]
        else:
            lw = 1

        if kwargs.has_key("fmt"):
            fmt = kwargs["fmt"]
        else:
            fmt = "k-o"

        if kwargs.has_key("dQdlogDp"):
            concentration_aer_mass = _numpy.array([x / y for x, y in zip(concentration_aer_mass,
                                                                         DiscretizationSize.GetDiameterWidthLog())])

        if kwargs.has_key("label"):
            ax.semilogx(DiscretizationSize.GetDiameterMean(), concentration_aer_mass, fmt, label = kwargs["label"], lw = lw)
        else:
            ax.semilogx(DiscretizationSize.GetDiameterMean(), concentration_aer_mass, fmt, lw = lw)

        xloc = DiscretizationSize.GetDiameterBound()
        xlabel = ["%2.1g" % x for x in xloc]
        #ax.set_xticks(xloc)
        #ax.set_xticklabels(xlabel)

        ax.set_xlim(xloc[0], xloc[-1])
        ax.set_xlabel(ur"diameter [$\mu m$]")

        if kwargs.has_key("threshold"):
            ax.set_ylim(0., kwargs["threshold"])
        else:
            ax.set_ylim(0., concentration_aer_mass.max() * 1.1)

        if kwargs.has_key("ylabel"):
            ax.set_ylabel(kwargs["ylabel"])
        else:
            ax.set_ylabel(ur"mass concentration  $[\mu g.m^{-3}]$")

        if kwargs.has_key("legend"):
            if isinstance(kwargs["legend"], int):
                ax.legend(loc = kwargs["legend"])
            elif isinstance(kwargs["legend"], list): 
                ax.legend(bbox_to_anchor = kwargs["legend"])
            else:
                raise ValueError("Legend argument must be either an integer or a list.")

        if kwargs.has_key("title"):
            ax.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    @staticmethod
    def PlotInternalMixingConcentrationWithTime(date, concentration, **kwargs):
        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 24

        if kwargs.has_key("height"):
            height = kwargs["height"]
        else:
            height = 12

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, height)

        margins = {"left" : 0.2, "right" : 0.2, "bottom" : 0.2, "top" : 0.2}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        _pylab.clf()
        fig = _pylab.figure()
        ax1 = fig.add_subplot(111)

        # Mean diameter.
        diameter = DiscretizationSize.GetDiameterMean()

        if kwargs.has_key("log_scale"):
            concentration = _numpy.ma.masked_where(concentration <= 0., concentration)

        if not kwargs.has_key("color"):
            kwargs["color"] = "jet"

        if isinstance(kwargs["color"], str):
            cmap = _pylab.get_cmap(kwargs["color"])
        else:
            cmap = _ListedColormap(kwargs["color"])

        if kwargs.has_key("level"):
            level = kwargs["level"]
        else:
            level = 30

        if kwargs.has_key("log_scale"):
            level_min = _numpy.floor(_numpy.floor(_numpy.log10(concentration.min())))
            level_max = _numpy.floor(_numpy.ceil(_numpy.log10(concentration.max())))
            level = _numpy.logspace(level_min, level_max, level)

        ind = _numpy.arange(len(date))

        if kwargs.has_key("log_scale"):
            norm =  _LogNorm(vmin = level_min, vmax = level_max)
            cs = ax1.contourf(ind, diameter, concentration.transpose(), level, norm = _LogNorm(), cmap = cmap)
            cbar = _pylab.colorbar(cs, ticks = level, format = _LogFormatter(10, labelOnlyBase = False))
        else:
            cs = ax1.contourf(ind, diameter, concentration.transpose(), level, cmap = cmap)
            cbar = _pylab.colorbar(cs)

        if "num" in kwargs["unit"]:
            unit = ur"[#$.m^{-3}$]"
        elif "mass" in kwargs["unit"]:
            unit = ur"[$\mu g.m^{-3}$]"
        else:
            unit = kwargs["unit"]

        cbar.ax.set_ylabel(unit)

        ax1.set_ylim(diameter.min(), diameter.max())
        ax1.set_ylabel(ur"diameter [$\mu m$]")
        ax1.set_yscale("log")
        ax1.get_yaxis().set_major_formatter(_LogFormatter(10, labelOnlyBase = False))

        yticks_loc = DiscretizationSize.GetDiameterBound()
        yticks_label = ["%1.2f" % x for x in yticks_loc]
        _pylab.yticks(yticks_loc, yticks_label)

        ax2 = ax1.twinx()
        concentration_total = concentration.sum(1)
        if kwargs.has_key("log_scale"):
            ax2.semilogy(ind, concentration_total, "k-s", label = "total")
        else:
            ax2.plot(ind, concentration_total, "k-s", label = "total")
        ax2.legend(loc = 1)

        ax2.set_ylim(ymin = concentration_total.min() * 0.99, ymax = concentration_total.max() * 1.01)
        #ax2.set_ylabel(unit)

        if  kwargs.has_key("xticks_spacing"):
            xticks_spacing = kwargs["xticks_spacing"]
        else:
            xticks_spacing = 1

        if  kwargs.has_key("xticks_date_format"):
            xticks_date_format = kwargs["xticks_date_format"]
        else:
            xticks_date_format = "%Y-%m-%d %H:%M"

        if  kwargs.has_key("xticks_rotation"):
            xticks_rotation = kwargs["xticks_rotation"]
        else:
            xticks_rotation = 45

        ax1.set_xlim(xmin = 0, xmax = len(date) - 1)
        ax1.set_xlabel(u"date")

        xticks_loc = []
        xticks_label = []
        for h in range(0, len(date), xticks_spacing):
            xticks_loc.append(h)
            xticks_label.append(date[h].strftime(xticks_date_format))

        #_pylab.xticks(xticks_loc, xticks_label, rotation = xticks_rotation, ha = 'center')
        ax1.set_xticks(xticks_loc)
        ax1.set_xticklabels(xticks_label, rotation = xticks_rotation, ha = 'center')

        if kwargs.has_key("title"):
            ax1.set_title(kwargs["title"])

        if kwargs.has_key("aspect"):
            _pylab.axes().set_aspect(float(kwargs["aspect"]), "box")

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
            _pylab.clf()
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


class DiscretizationCompositionBase(_core.DiscretizationCompositionBase):

    def GetDimensionSpeciesIndex(self, i):
        species_index = _core.vector1i()
        _core.DiscretizationCompositionBase.GetDimensionSpeciesIndex(self, i, species_index)
        return _numpy.array(species_index, dtype = _numpy.int)


    def GetDimensionSpeciesName(self, i):
        species_index = self.GetDimensionSpeciesIndex(i)
        species_name = []
        for i in species_index:
            species_name.append(_core.Species_GetName(int(i)))
        return species_name


    def GetClassPolygonList(self, i):
        polygon_index = _core.vector1i()
        _core.DiscretizationCompositionBase.GetClassPolygonList(self, i, polygon_index)
        return _numpy.array(polygon_index, dtype = _numpy.int)


    def GetClassPointList(self, i):
        point_index = _core.vector1i()
        _core.DiscretizationCompositionBase.GetClassPointList(self, i, point_index)
        return _numpy.array(point_index, dtype = _numpy.int)


    def GetPolygonPointList(self, i):
        point_index = _core.vector1i()
        _core.DiscretizationCompositionBase.GetPolygonPointList(self, i, point_index)
        return _numpy.array(point_index, dtype = _numpy.int)


    def GetPointCoordinate(self, i):
        coordinate = _core.vector1r()
        _core.DiscretizationCompositionBase.GetPointCoordinate(self, i, coordinate)
        return _numpy.array(coordinate, dtype = _numpy.float)


    def GetClassPointCoordinateList(self, i):
        coordinate = []
        point_index = self.GetClassPointList(i)

        for i in point_index:
            coordinate.append(self.GetPointCoordinate(int(i)))
        return _numpy.array(coordinate, dtype = _numpy.float)


    def ComputeFraction(self, composition):
        return _numpy.array(_core.DiscretizationCompositionBase.ComputeFraction(self, composition), dtype = _numpy.float)


    def GenerateRandomComposition(self, class_index):
        composition = _core.vector1r()
        _core.DiscretizationCompositionBase.GenerateRandomComposition(self, class_index, composition, False)
        return _numpy.array(composition, dtype = _numpy.float)


    def GenerateRandomCompositionOnEdge(self, class_index):
        composition = _core.vector1r()
        _core.DiscretizationCompositionBase.GenerateRandomComposition(self, class_index, composition, True)
        return _numpy.array(composition, dtype = _numpy.float)


    def GenerateAverageComposition(self, class_index):
        composition = _core.vector1r()
        _core.DiscretizationCompositionBase.GenerateAverageComposition(self, class_index, composition)
        return _numpy.array(composition, dtype = _numpy.float)


    def Plot1d(self, fig = None, **kwargs):
        if self.GetNdim() != 1:
            raise Exception("Composition discretization is not 1D.")

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 12
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        if kwargs.has_key("bar_width"):
            bar_width = kwargs["bar_width"]
        else:
            bar_width = 0.4

        if kwargs.has_key("alpha"):
            alpha = kwargs["alpha"]
        else:
            alpha = 0.8

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig != None:
            ax = fig.gca()
        else:
            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)

        for i in range(self.GetNclass()):
            point_list = self.GetClassPointCoordinateList(i)

            label = self.GetDiscretizationCompositionNested(i).GetLabel()
            if label == "":
                label = self.GetClassName(i)

            color = self.GetDiscretizationCompositionNested(i).GetColor()

            ax.bar(0.0, abs(point_list[0][0] - point_list[1][0]),
                   bar_width, min(point_list[0][0], point_list[1][0]),
                   color = color, label = label, alpha = alpha)

        ax.set_xlim(xmin = -0.1, xmax = 1.1)
        ax.set_ylim(ymin = -0.1, ymax = 1.1)
        ax.tick_params(axis = 'x', which = 'both',
                       bottom = 'off', top = 'off',
                       labelbottom = 'off')

        _pylab.axes().set_aspect(1., "box")

        ax.set_ylabel(" + ".join(self.GetDimensionSpeciesName(0)))

        ax.legend(loc = 1)

        if kwargs.has_key("title"):
            title = kwargs["title"]
        else:
            title = self.GetName()

        ax.set_title(title)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    def Plot2d(self, fig = None, **kwargs):
        if self.GetNdim() != 2:
            raise Exception("Composition discretization is not 2D.")

        if kwargs.has_key("width"):
            width = kwargs["width"]
        else:
            width = 12
        width /= 2.54

        _pylab.matplotlib.rcParams["figure.figsize"] = (width, .75 * width)

        if kwargs.has_key("alpha"):
            alpha = kwargs["alpha"]
        else:
            alpha = 0.8

        if kwargs.has_key("point_size"):
            point_size = kwargs["point_size"]
        else:
            point_size = 50

        margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
        if kwargs.has_key("margins"):
            for k,v in kwargs["margins"].iteritems():
                margins[k] = v

        _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
        _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
        _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
        _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

        if kwargs.has_key("font_size"):
            font_size = kwargs["font_size"]
        else:
            font_size = 12

        _pylab.matplotlib.rcParams["font.size"] = font_size
        _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
        _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
        _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
        _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
        _pylab.matplotlib.rcParams["xtick.major.size"] = 2
        _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
        _pylab.matplotlib.rcParams["ytick.major.size"] = 2
        _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

        if fig != None:
            ax = fig.gca()
        else:
            _pylab.clf()
            fig = _pylab.figure()
            ax = fig.add_subplot(111)

        for i in range(self.GetNclass()):
            label = self.GetDiscretizationCompositionNested(i).GetLabel()
            if label == "":
                label = self.GetClassName(i)

            color = self.GetDiscretizationCompositionNested(i).GetColor()

            point_coordinate = list(self.GetClassPointCoordinateList(i))

            # Put points in correct order.
            centroid = [sum([p[0] for p in point_coordinate]) / len(point_coordinate),
                        sum([p[1] for p in point_coordinate]) / len(point_coordinate)]

            # Reorder thanks to polar coordinates.
            point_coordinate.sort(key = lambda p: _numpy.arctan2(p[1] - centroid[1], p[0] - centroid[0]))

            polygon = _Polygon(point_coordinate, True, color = color, lw = 0, label = label, alpha = alpha)
            ax.add_patch(polygon)

            if point_size > 0:
                ax.scatter([p[0] for p in point_coordinate],
                           [p[1] for p in point_coordinate],
                           point_size, color = "k", marker = "o", zorder = 10)

        ax.set_xlim(xmin = -0.1, xmax = 1.1)
        ax.set_ylim(ymin = -0.1, ymax = 1.1)
        _pylab.axes().set_aspect(1., "box")

        ax.set_xlabel(" + ".join(self.GetDimensionSpeciesName(0)))
        ax.set_ylabel(" + ".join(self.GetDimensionSpeciesName(1)))

        ax.legend(loc = 1)

        if kwargs.has_key("title"):
            title = kwargs["title"]
        else:
            title = self.GetName()

        ax.set_title(title)

        if kwargs.has_key("output_file"):
            _pylab.savefig(kwargs["output_file"])
        elif kwargs.has_key("show"):
            _pylab.show()

        return fig


    def Plot3d(self):
        if self.GetNdim() != 3:
            raise Exception("Composition discretization is not 3D.")

        raise Exception("Not yet implemented.")


class DiscretizationCompositionTable(_core.DiscretizationCompositionTable, DiscretizationCompositionBase):

    def ComputeFraction(self, composition):
        return _numpy.array(_core.DiscretizationCompositionTable.ComputeFraction(self, composition), dtype = _numpy.float)


    def GetTable(self):
        table_vect = _core.vector2i()
        _core.DiscretizationCompositionTable.GetTable(self, table_vect)

        table = []
        for i in range(len(table_vect)):
            table.append(list(table_vect[i]))

        return table


class DiscretizationClassBase(_core.DiscretizationClassBase):
    pass


class DiscretizationClassTable(_core.DiscretizationClassTable):
    pass


if _core.AMC_HAS_TRACE > 0:
    class Trace(_core.Trace):

        @staticmethod
        def GetGeneralSectionIndex(i):
            general_section_index = _core.vector1i()
            _core.Trace.GetGeneralSectionIndex(i, general_section_index)
            return _numpy.array(general_section_index, dtype = _numpy.int)

        @staticmethod
        def GetGeneralSectionIndexReverse():
            general_section_index_reverse = _core.vector1i()
            _core.Trace.GetGeneralSectionIndexReverse(general_section_index_reverse)
            return _numpy.array(general_section_index_reverse, dtype = _numpy.int)

        @staticmethod
        def GetSizeClassIndex(i):
            size_class_index = _core.vector1i()
            _core.Trace.GetSizeClassIndex(i, size_class_index)
            return _numpy.array(size_class_index, dtype = _numpy.int)


class DiscretizationBase(_core.DiscretizationBase):

    @staticmethod
    def GetGeneralSection():
        general_section = _core.vector2i()
        _core.DiscretizationBase.GetGeneralSection(general_section)
        return _to_array(general_section)

    @staticmethod
    def GetGeneralSectionReverse():
        general_section_reverse = _core.vector2i()
        _core.DiscretizationBase.GetGeneralSectionReverse(general_section_reverse)
        return _to_array(general_section_reverse)

    @staticmethod
    def GetClassCompositionName():
        class_composition_name = _core.vector2s()
        _core.DiscretizationBase.GetClassCompositionName(class_composition_name)
        return _to_array(class_composition_name)

    @staticmethod
    def ComputeDiameter(concentration_aer_num,
                        concentration_aer_mass):
        section_index = _core.vector1i()
        diameter = _core.vector1r()
        _core.DiscretizationBase.ComputeDiameter(concentration_aer_num,
                                                 concentration_aer_mass,
                                                 section_index, diameter)
        return _numpy.array(section_index, dtype = _numpy.int), \
            _numpy.array(diameter, dtype = _numpy.float)

    @staticmethod
    def ComputeSingleMass(concentration_aer_num,
                          concentration_aer_mass):
        section_index = _core.vector1i()
        single_mass = _core.vector1r()
        _core.DiscretizationBase.ComputeSingleMass(concentration_aer_num,
                                                   concentration_aer_mass,
                                                   section_index, single_mass)
        return _numpy.array(section_index, dtype = _numpy.int), \
            _numpy.array(single_mass, dtype = _numpy.float)


    @staticmethod
    def Redistribute(concentration_aer_number, concentration_aer_mass,
                     section_min = 0, section_max = -1):
        if section_max < 0:
            section_max = AerosolData.GetNg()

        _core.DiscretizationBase.Redistribute(section_min, section_max,
                                              concentration_aer_number,
                                              concentration_aer_mass)


class DiscretizationTable(_core.DiscretizationTable):
    pass
