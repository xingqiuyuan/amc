#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import ops as _ops
import numpy as _numpy
from util import to_array as _to_array
import pylab as _pylab
import copy as _copy
from matplotlib.colors import LogNorm as _LogNorm
from matplotlib.colors import ListedColormap as _ListedColormap


class Species(_core.SpeciesNPF):
    @staticmethod
    def GetName():
        name = _core.vector1s()
        _core.SpeciesNPF.GetName(name)
        return list(name)

    @staticmethod
    def GetSpeciesIndex():
        species_index = _core.vector1i()
        _core.SpeciesNPF.GetSpeciesIndex(species_index)
        return _numpy.array(species_index, dtype = _numpy.int)

    @staticmethod
    def GetGasIndex():
        gas_index = _core.vector1i()
        _core.SpeciesNPF.GetGasIndex(gas_index)
        return _numpy.array(gas_index, dtype = _numpy.int)


class DiscretizationSize(_core.DiscretizationSizeNPF):
    @staticmethod
    def GetDiameterBound():
        diameter_bound = _core.vector1r()
        _core.NucleiData.GetDiameterBound(diameter_bound)
        return _numpy.array(diameter_bound, dtype = _numpy.float)


class Particle(_core.ParticleNPF):
    @staticmethod
    def GetSectionParticleIndex(i):
        section_particle_index = _core.vector1i()
        _core.ParticleNPF.GetSectionParticleIndex(i, section_particle_index)
        return _numpy.array(section_particle_index, dtype = _numpy.int)

    @staticmethod
    def GetIndexActive():
        index_active = _core.vector1i()
        _core.ParticleNPF.GetIndexActive(index_active)
        return _numpy.array(index_active, dtype = _numpy.int)


class ParticleData(_core.ParticleDataNPF):
    @staticmethod
    def GetDiameter():
        diameter = _core.vector1r()
        _core.ParticleDataNPF.GetDiameter(diameter)
        return _numpy.array(diameter, dtype = _numpy.float)

    @staticmethod
    def GetDensity():
        density = _core.vector1r()
        _core.ParticleDataNPF.GetDensity(density)
        return _numpy.array(density, dtype = _numpy.float)

    @staticmethod
    def GetLiquidWaterContent():
        liquid_water_content = _core.vector1r()
        _core.ParticleDataNPF.GetLiquidWaterContent(liquid_water_content)
        return _numpy.array(liquid_water_content, dtype = _numpy.float)

    if _core.NPF_HAS_TEST > 0:
        @staticmethod
        def Test(N, pause = True, **kwargs):
            if kwargs.has_key("nucleation_number_max"):
                nucleation_number_max = kwargs["nucleation_number_max"]
            else:
                nucleation_number_max = _core.NPF_PARTICLE_DATA_TEST_NUCLEATION_NUMBER_MAX

            if kwargs.has_key("coagulation_frequency"):
                coagulation_frequency = kwargs["coagulation_frequency"]
            else:
                coagulation_frequency = _core.NPF_PARTICLE_DATA_TEST_COAGULATION_FREQUENCY

            if kwargs.has_key("condensation_flux_max"):
                condensation_flux_max = kwargs["condensation_flux_max"]
            else:
                condensation_flux_max = _core.NPF_PARTICLE_DATA_TEST_CONDENSATION_FLUX_MAX
                
            _core.ParticleDataNPF.Test(N, pause, nucleation_number_max,
                                       coagulation_frequency, condensation_flux_max)


class Concentration(_core.ConcentrationNPF):
    @staticmethod
    def GetGas():
        gas = _core.vector1r()
        _core.ConcentrationNPF.GetGas(gas)
        return _numpy.array(gas, dtype = _numpy.float)

    @staticmethod
    def GetNumber():
        number = _core.vector1r()
        _core.ConcentrationNPF.GetNumber(number)
        return _numpy.array(number, dtype = _numpy.float)

    @staticmethod
    def GetMass():
        mass = _core.vector2r()
        _core.ConcentrationNPF.GetMass(mass)
        return _to_array(mass)

    @staticmethod
    def GetMassTotal():
        mass_total = _core.vector1r()
        _core.ConcentrationNPF.GetMassTotal(mass_total)
        return _to_array(mass_total)

    @staticmethod
    def MapOnSizeDiscretization(diameter_bound):
        diameter_bound_vect = _core.vector1r(diameter_bound)
        number = _core.vector1r()
        mass = _core.vector2r()
        _core.ConcentrationNPF.MapOnSizeDiscretization(number, mass, diameter_bound_vect)
        return _numpy.array(number, dtype = _numpy.float), _to_array(mass)


class Dynamics(_core.DynamicsNPF):
    @staticmethod
    def GetCoagulationKernel():
        coagulation_kernel = _core.vector2r()
        _core.DynamicsNPF.GetCoagulationKernel(coagulation_kernel)
        return _to_array(coagulation_kernel)

    @staticmethod
    def GetCondensationCoefficient():
        condensation_coefficient = _core.vector2r()
        _core.DynamicsNPF.GetCondensationCoefficient(condensation_coefficient)
        return _to_array(condensation_coefficient)

    @staticmethod
    def GetNucleationRate():
        nucleation_rate = _core.vector1r()
        _core.DynamicsNPF.GetNucleationRate(nucleation_rate)
        return _numpy.array(nucleation_rate, dtype = _numpy.float)


class NewParticleFormation(_core.NewParticleFormation):
    @staticmethod
    def Run(concentration_gas, time_out):
        mass = _core.vector1r(_core.Species.GetNspecies(), 0.)
        nucleation_event, number = _core.NewParticleFormation.Run(concentration_gas, time_out, mass)
        return nucleation_event, number, _numpy.array(mass, dtype = _numpy.float)


    if _core.AMC_HAS_TEST == 1:
        @staticmethod
        def Test(parameterization_nucleation, time_out,
                 initial_gas, production_gas,
                 diameter_bound, time_output_frequency,
                 **kwargs):
            gas = _core.vector2r()
            number = _core.vector2r()
            mass = _core.vector3r()

            initial_gas_vect = _core.vector1r(initial_gas)
            diameter_bound_vect = _core.vector1r(diameter_bound)
            production_gas_vect = _core.vector2r(production_gas)

            diameter_bound *= 1.e-3

            _core.NewParticleFormation.Test(parameterization_nucleation, time_out,
                                            initial_gas_vect, production_gas_vect,
                                            diameter_bound_vect, time_output_frequency,
                                            gas, number, mass)

            width = 24 / 2.54
            _pylab.matplotlib.rcParams["figure.figsize"] = (2 * width, .75 * width)

            margins = {"left" : 0.1, "right" : 0.1, "bottom" : 0.1, "top" : 0.1}
            _pylab.matplotlib.rcParams["figure.subplot.left"] = margins["left"]
            _pylab.matplotlib.rcParams["figure.subplot.right"] = 1. - margins["right"]
            _pylab.matplotlib.rcParams["figure.subplot.bottom"] = margins["bottom"]
            _pylab.matplotlib.rcParams["figure.subplot.top"] = 1. - margins["top"]

            font_size = 12

            _pylab.matplotlib.rcParams["font.size"] = font_size
            _pylab.matplotlib.rcParams["axes.titlesize"] = font_size + 2
            _pylab.matplotlib.rcParams["axes.labelsize"] = font_size
            _pylab.matplotlib.rcParams["xtick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["ytick.labelsize"] = font_size
            _pylab.matplotlib.rcParams["legend.fontsize"] = font_size + 1
            _pylab.matplotlib.rcParams["xtick.major.size"] = 2
            _pylab.matplotlib.rcParams["xtick.minor.size"] = 1
            _pylab.matplotlib.rcParams["ytick.major.size"] = 2
            _pylab.matplotlib.rcParams["ytick.minor.size"] = 1

            time = _numpy.array([i * time_output_frequency for i in range(len(number))], dtype = _numpy.float)
            gas = _to_array(gas)
            number = _to_array(number)

            if number.max() < _core.NPF_NUMBER_CONCENTRATION_MINIMUM:
                raise ValueError("Max number concentration is below threshold %f #/cm3"
                                 % _core.NPF_NUMBER_CONCENTRATION_MINIMUM)

            fig = _pylab.figure()
            ax = fig.add_subplot(121)

            deltat = _core.NewParticleFormation.GetTimeStep()
            time_bound = _numpy.array([deltat] + [t + deltat for t in time], dtype = _numpy.float)
            diameter_bound = list(NucleiData.GetDiameterBound())
            diameter_bound = _numpy.array(diameter_bound + [2 * diameter_bound[-1]], dtype = _numpy.float) * 1000.

            # Put in matrix form for pcolor/pcolormesh
            time_bound = _numpy.array([time_bound for i in range(len(diameter_bound))])
            diameter_bound = _numpy.array([diameter_bound for i in range(len(time) + 1)]).transpose()

            concentration_number_t = _copy.copy(concentration_number).transpose()
            concentration_number_t = _numpy.ma.masked_where(concentration_number_t < _core.NPF_NUMBER_CONCENTRATION_MINIMUM,
                                                            concentration_number_t)

            if not kwargs.has_key("color"):
                kwargs["color"] = "Jet"

            if isinstance(kwargs["color"], str):
                cmap = _pylab.get_cmap(kwargs["color"])
            else:
                cmap = _ListedColormap(kwargs["color"])

            norm =  _LogNorm(vmin = concentration_number_t.min(),
                             vmax = concentration_number_t.max())

            cs = ax.pcolormesh(time_bound, diameter_bound, concentration_number_t, norm = norm, cmap = cmap)

            ax.axhline(diameter_bound[-2, 0], lw = 2, ls = "--", color = "k")

            ax.set_xlim(time_bound.min(), time_bound.max())
            ax.set_xlabel(ur"time [second]")
            #ax.set_xscale("log")

            ax.set_ylabel(ur"Nuclei diameter [nm]")
            ax.set_ylim(diameter_bound.min(), diameter_bound.max())
            ax.set_yticks(diameter_bound[:, 0])
            ax.set_yticklabels(["%1.2g" % x for x in diameter_bound[:, 0]])

            ax.set_yscale("log")
            cbar = _pylab.colorbar(cs)
            cbar.ax.set_ylabel(ur"Nuclei number concentration [#.$m^{-3}$]")

            ax.set_title("Nuclei number concentration time evolution.")

            ax = fig.add_subplot(122)

            species_index = NucleiData.GetSpeciesIndex()
            if kwargs.has_key("fmt"):
                for i in range(_core.NucleiData.GetNspecies() - 1):
                    ax.plot(time, concentration_gas_bulk[:, i], fmt[i], lw = 2,
                            label = _core.Species.GetName(int(species_index[i])))
            else:
                for i in range(_core.NucleiData.GetNspecies() - 1):
                    ax.plot(time, concentration_gas_bulk[:, i], lw = 2,
                            label = _core.Species.GetName(int(species_index[i])))


            ax.set_xlim(time.min(), time.max())
            ax.set_xlabel(ur"time [second]")
            #ax.set_xscale("log")

            ax.set_ylabel(ur"Bulk gas concentration [$\mu g.m^{-3}$]")
            ax.set_ylim(0., concentration_gas_bulk.max() * 1.01)

            if kwargs.has_key("legend"):
                ax.legend(loc = kwargs["legend"])
            else:
                ax.legend(loc = 1)

            ax.set_title("Gas bulk concentration time evolution.")

            if not kwargs.has_key("title"):
                kwargs["title"] = "New particle formation with nucleation parameterization \"" + \
                                  parameterization_nucleation.GetName() + \
                                  "\" and threshold diameter = %d nm" % diameter_bound[-2, 0]

            fig.suptitle(kwargs["title"], fontsize = _pylab.matplotlib.rcParams["axes.titlesize"])

            if kwargs.has_key("output_file"):
                _pylab.savefig(kwargs["output_file"])
                _pylab.clf()
            elif kwargs.has_key("show"):
                _pylab.show()
            else:
                return fig

            if kwargs.has_key("return"):
                return_concentration = []

                if "gas" in kwargs["return"]:
                    return_concentration.append(gas)

                if "num" in kwargs["return"]:
                    return_concentration.append(number)

                if "mass" in kwargs["return"]:
                    return_concentration.append(mass)

                return return_concentration
