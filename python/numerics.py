#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2015 INERIS
# Author(s) : Edouard Debry
# 
# This file is part of AMC.
# 
# AMC is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# AMC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with AMC.  If not, see <http://www.gnu.org/licenses/>.


import amc_core as _core

import numpy as _numpy

#
# Numerical solvers.
#

class NumericalSolverBase(_core.NumericalSolverBase):
    pass


if _core.AMC_HAS_COAGULATION == 1:
    class NumericalSolverCoagulationMovingBase(_core.NumericalSolverCoagulationMovingBase, NumericalSolverBase):
        pass

    class NumericalSolverCoagulationMovingTable(_core.NumericalSolverCoagulationMovingTable, NumericalSolverBase):
        pass

    class NumericalSolverCoagulationStaticBase(_core.NumericalSolverCoagulationStaticBase, NumericalSolverBase):
        pass

    class NumericalSolverCoagulationStaticTable(_core.NumericalSolverCoagulationStaticTable, NumericalSolverBase):
        pass


if _core.AMC_HAS_CONDENSATION == 1:
    class NumericalSolverCondensation(_core.NumericalSolverCondensation, NumericalSolverBase):

        @staticmethod
        def LagrangianExplicit(time_step, concentration_aer_number,
                               concentration_aer_mass, concentration_gas,
                               section_min = 0, section_max = -1):

            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            return _core.NumericalSolverCondensation.LagrangianExplicit(section_min, section_max, time_step,
                                                                        concentration_aer_number,
                                                                        concentration_aer_mass,
                                                                        concentration_gas)

if _core.AMC_HAS_NUCLEATION == 1:
    class NumericalSolverNucleation(_core.NumericalSolverNucleation, NumericalSolverBase):

        @staticmethod
        def FormationRate(time_step,
                          rate_aer_number, rate_aer_mass,
                          section_min = 0, section_max = -1):

            if section_max < 0:
                section_max = _core.AerosolData.GetNg()

            return _core.NumericalSolverNucleation.FormationRate(section_min, section_max, time_step,
                                                                 rate_aer_number, rate_aer_mass)
