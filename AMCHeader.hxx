// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.


#ifndef AMC_FILE_AMC_HEADER_HXX

//The C++ standard language version we are working with.
#ifdef AMC_WITH_DEBUG
#define XSTR(x) STR(x)
#define STR(x) #x
#pragma message(STR(__cplusplus) " = " XSTR(__cplusplus))
#define CBUG cerr << __FILE__ << ":" <<__FUNCTION__ << ":" << __LINE__ << ":" 
#endif

/*!
 * \file AMCHeader.hxx
 * \brief Main AMC header file.
 * \author Edouard Debry
 */

namespace std
{
}

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <unistd.h>
#include <libgen.h>
#include <string.h>
#include <stdint.h>
#include <cmath>
#include <limits>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <exception>
#include <vector>
#include <map>
#include <random>

namespace blitz
{
}

// Blitz++.
#include <blitz/array.h>

#ifndef SWIG
extern "C"
{
  void setulb_(const int*, const int*, double*, double*, double*,
               int*, double*, double*, double*, double*,
               double*, int*, char*, int*, char*, int*, int*, double*,
               int*, int*);
}
#endif

#ifndef DRIVER_WITH_AMC
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
#undef AMC_WITH_NEW_PARTICLE_FORMATION
#endif
#endif

// Util library.
namespace util
{
}
#include "util/util.hxx"

// Ops library.
#define OPS_WITH_EXCEPTION
#include "ops/OpsHeader.hxx"

// MPI.
#ifdef AMC_WITH_MPI
#define AMC_HAS_MPI 1
#define AMC_MPI_REAL MPI::DOUBLE
#define MPI_INCLUDED
#include <mpi.h>
#else
#define AMC_HAS_MPI 0
#endif

#ifdef AMC_WITH_LOGGER
//#ifdef AMC_WITH_DEBUG
//#define LOGGER_WITH_DEBUG
//#endif
#define LOGGER_WITH_OPS
#define LOGGER_WITH_TIME
#define LOGGER_WITH_FORMAT
#ifdef AMC_WITH_MPI
#define LOGGER_WITH_MPI
#endif
#include "logger/LoggerHeader.hxx"
#define AMC_HAS_LOGGER 1
#else
#define AMC_HAS_LOGGER 0
#endif

#ifdef AMC_WITH_TIMER
#define AMC_HAS_TIMER 1
#else
#define AMC_HAS_TIMER 0
#endif

#ifdef AMC_WITH_TEST
#define AMC_HAS_TEST 1
#else
#define AMC_HAS_TEST 0
#endif

#ifdef AMC_WITH_STATISTICS
#define AMC_HAS_STATISTICS 1
#else
#define AMC_HAS_STATISTICS 0
#endif

#if defined(AMC_WITH_TIMER) || defined(AMC_WITH_STATISTICS)
#include <ctime>
#include <chrono>
#endif

// Run time class.
#ifdef AMC_WITH_RUN
#define AMC_HAS_RUN 1
#else
#define AMC_HAS_RUN 0
#endif

// Modal distribution.
#ifdef AMC_WITH_MODAL
#define AMC_HAS_MODAL 1
#else
#define AMC_HAS_MODAL 0
#endif

// Choice of Cunningham coefficients for physical parameterizations.
#define AMC_PARAMETERIZATION_PHYSICS_CUNNINGHAM_WITH_ALLEN

// Threads.
#ifdef AMC_WITH_THREAD
#include <thread>
#include <mutex>
#include <omp.h>
#define AMC_HAS_THREAD 1
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
#define AMC_HAS_THREAD_THERMODYNAMIC 1
#else
#define AMC_HAS_THREAD_THERMODYNAMIC 0
#endif
#else
#define AMC_HAS_THREAD 0
#endif

// Isoropia.
#ifdef AMC_WITH_ISOROPIA
#define AMC_HAS_ISOROPIA 1
#else
#define AMC_HAS_ISOROPIA 0
#endif

// Pankow.
#ifdef AMC_WITH_PANKOW
#define AMC_HAS_PANKOW 1
#else
#define AMC_HAS_PANKOW 0
#endif

// AEC.
#ifdef AMC_WITH_AEC
#define AEC_WITH_UNIFAC
#define AMC_HAS_AEC 1
#else
#define AMC_HAS_AEC 0
#ifdef AMC_WITH_H2O
#undef AMC_WITH_H2O
#endif
#endif

// H2O.
#ifdef AMC_WITH_H2O
#define AMC_HAS_H2O 1
#else
#define AMC_HAS_H2O 0
#endif

// Gerber.
#ifdef AMC_WITH_GERBER
#define AMC_HAS_GERBER 1
#else
#define AMC_HAS_GERBER 0
#endif

// Fractal soot.
#ifdef AMC_WITH_FRACTAL_SOOT
#define AMC_HAS_FRACTAL_SOOT 1
#else
#define AMC_HAS_FRACTAL_SOOT 0
#endif

// Fractal soot.
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
#define AMC_HAS_DIFFUSION_LIMITED_SOOT 1
#else
#define AMC_HAS_DIFFUSION_LIMITED_SOOT 0
#endif

// Kelvin effect.
#ifdef AMC_WITH_KELVIN_EFFECT
#define AMC_HAS_KELVIN_EFFECT 1
#else
#define AMC_HAS_KELVIN_EFFECT 0
#endif

// Aqueous flux correction.
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
#define AMC_HAS_FLUX_CORRECTION_AQUEOUS 1
#else
#define AMC_HAS_FLUX_CORRECTION_AQUEOUS 0
#endif

// Dry flux correction.
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
#define AMC_HAS_FLUX_CORRECTION_DRY 1
#else
#define AMC_HAS_FLUX_CORRECTION_DRY 0
#endif

// Surface tension.
#ifdef AMC_WITH_SURFACE_TENSION
#define AMC_HAS_SURFACE_TENSION 1
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
#define AMC_HAS_SURFACE_TENSION_JACOBSON 1
#endif
#else
#define AMC_HAS_SURFACE_TENSION 0
#define AMC_HAS_SURFACE_TENSION_JACOBSON 0
#endif

// Adsorption.
#ifdef AMC_WITH_ADSORPTION
#define AMC_HAS_ADSORPTION 1
#else
#define AMC_HAS_ADSORPTION 0
#endif

// Collision efficiency.
#ifdef AMC_WITH_COLLISION_EFFICIENCY
#define AMC_HAS_COLLISION_EFFICIENCY 1
#else
#define AMC_HAS_COLLISION_EFFICIENCY 0
#endif

// Coagulation.
#ifdef AMC_WITH_COAGULATION
#define AMC_HAS_COAGULATION 1
#ifdef AMC_WITH_COLLISION_EFFICIENCY
#define AMC_COAGULATION_GRAVITATIONAL_CUNNINGHAM_WITH_FUCHS
//#define AMC_COAGULATION_GRAVITATIONAL_CUNNINGHAM_WITH_ALLEN
#endif
#else
#define AMC_HAS_COAGULATION 0
#endif

// Waals viscous forces.
#ifdef AMC_WITH_WAALS_VISCOUS
#define AMC_HAS_WAALS_VISCOUS 1
#else
#define AMC_HAS_WAALS_VISCOUS 0
#endif

// Condensation.
#ifdef AMC_WITH_CONDENSATION
#define AMC_HAS_CONDENSATION 1
#else
#define AMC_HAS_CONDENSATION 0
#endif

// Nucleation.
#ifdef AMC_WITH_NUCLEATION
#define AMC_HAS_NUCLEATION 1
#else
#define AMC_HAS_NUCLEATION 0
#endif

// Diffusion.
#ifdef AMC_WITH_DIFFUSION
#define AMC_HAS_DIFFUSION 1
#else
#define AMC_HAS_DIFFUSION 0
#endif

// Power law.
#ifdef AMC_WITH_POWER_LAW
#define AMC_HAS_POWER_LAW 1
#else
#define AMC_HAS_POWER_LAW 0
#endif

// Vehkamaki.
#ifdef AMC_WITH_VEHKAMAKI
#define AMC_HAS_VEHKAMAKI 1
#else
#define AMC_HAS_VEHKAMAKI 0
#endif

// Merikanto.
#ifdef AMC_WITH_MERIKANTO
#define AMC_HAS_MERIKANTO 1
#else
#define AMC_HAS_MERIKANTO 0
#endif

// New particle formation.
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
#define AMC_HAS_NEW_PARTICLE_FORMATION 1
#ifdef NPF_WITH_TEST
#define NPF_HAS_TEST 1
#else
#define NPF_HAS_TEST 0
#endif
#else
#define AMC_HAS_NEW_PARTICLE_FORMATION 0
#endif

#ifdef NPF_WITH_COAGULATION_TABLE
#define NPF_HAS_COAGULATION_TABLE 1
#else
#define NPF_HAS_COAGULATION_TABLE 0
#endif

// Numerics.
#ifdef AMC_WITH_NUMERICS
#define AMC_HAS_NUMERICS 1
#else
#define AMC_HAS_NUMERICS 0
#endif

// Repartition coefficients
#ifdef AMC_WITH_REPARTITION_COEFFICIENT
#define AMC_HAS_REPARTITION_COEFFICIENT 1
#else
#define AMC_HAS_REPARTITION_COEFFICIENT 0
#endif

// Discretization composition settings.
#ifdef AMC_WITH_DISCRETIZATION_COMPOSITION_TABLE_FAST
#define AMC_HAS_DISCRETIZATION_COMPOSITION_TABLE_FAST 1
#else
#define AMC_HAS_DISCRETIZATION_COMPOSITION_TABLE_FAST 0
#endif

// Redistribution composition settings.
#ifdef AMC_WITH_REDISTRIBUTION_COMPOSITION_TABLE_FAST
#define AMC_HAS_REDISTRIBUTION_COMPOSITION_TABLE_FAST 1
#else
#define AMC_HAS_REDISTRIBUTION_COMPOSITION_TABLE_FAST 0
#endif

// Layers.
#ifdef AMC_WITH_LAYER
#define AMC_HAS_LAYER 1
#else
#define AMC_HAS_LAYER 0
#endif

// Trace.
#ifdef AMC_WITH_TRACE
#define AMC_HAS_TRACE 1
#else
#define AMC_HAS_TRACE 0
#endif

// Mathematical constants.
#define AMC_PI                                 3.1415926535897932  // Pi
#define AMC_EXP                                2.718281828459045   // exp(1)
#define AMC_PI6                                0.5235987755982988  // Pi / 6
#define AMC_INV_PI6                            1.909859317102744   // 6 / Pi
#define AMC_FRAC3                              0.3333333333333333  // 1 / 3

// Physical constants.
#define AMC_SURFACE_TENSION_DEFAULT             5.e-2          // Default surface tension for all phases in N.m^{-1}
#define AMC_PARTICLE_DENSITY_DEFAULT            1.2            // Default particle density in g.cm^{-3}
#define AMC_WATER_DENSITY                       1.e-6          // Default particle density in µg.µm^{-3}
#define AMC_CONVERT_FROM_TORR_TO_PASCAL         133.322368     // Convert factor from torr to Pascal pression units.
#define AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3  1.e-6          // Convert factor from g.cm^{-3} to µg.µm^{-3}
#define AMC_CONVERT_LWC_FROM_UGM3_TO_LM3        1.e-9          // Convert the liquid water content from µg.m^{-3} to L.m^{-3}.
#define AMC_CONVERT_FROM_MMHG_TO_PASCAL         133.322368     // 1mmHg = 133.322368 Pa.
#define AMC_CONVERT_FROM_DYNCM_TO_NM            0.001          // 1 dyne/cm = 0.001 N/m.
#define AMC_BOLTZMANN_CONSTANT                  1.3806488e-23  // Boltzmann constant m^2.kg.s^{-2}.K^{-1}
#define AMC_IDEAL_GAS_CONSTANT                  8.314462175    // Ideal gas constant J.mol^{-1}.K^{-1}
#define AMC_AIR_MOLAR_MASS                      2.897e-2       // Air molar mass kg.mol^{-1}.
#define AMC_WATER_MOLAR_MASS                    18.e6          // Water molar mass µg.mol^{-1}.
#define AMC_TEMPERATURE_REFERENCE               298.0          // Standard reference temperature Kelvin.
#define AMC_PRESSURE_REFERENCE                  101300.0       // Standard reference pressure Pascal.
#define AMC_PRESSURE_ATM                        1.01325e5      // Definition of 1 atm in Pascal.
#define AMC_RELATIVE_HUMIDITY_DEFAULT           0.7            // Default relative humidity.
#define AMC_EDDY_DISSIPATION_RATE_DEFAULT       5.e-4          // Default eddy dissipation rate in m^2.s^{-3}.
#define AMC_AIR_FREE_MEAN_PATH_DEFAULT          6.51e-8        // Default free mean path in m.
#define AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT       1.83e-5        // Default air dynamic viscosity kg.m^{-1}.s^{-1}.
#define AMC_WATER_DYNAMIC_VISCOSITY_DEFAULT     8.90e-4        // Default water dynamic viscosity kg.m^{-1].s^{-1}.
#define AMC_AIR_DENSITY_DEFAULT                 1.2041         // Default air density kg.m^{-3}.
#define AMC_SPECIES_DENSITY_DEFAULT                1.2         // Default pure component density in g.cm^{-3}.
#define AMC_SPECIES_PARTITION_COEFFICIENT_DEFAULT  300         // Default very low volatile partition coefficient in m^3.µg^{-1}.
#define AMC_SPECIES_MOLAR_MASS_AVERAGE             250         // Average species molar masses in g.mol^{-1}.
#define AMC_WATER_SURFACE_TENSION_DEFAULT       7.2e-2         // Water surface tension in N.m^{-1}.
#define AMC_AVOGADRO_NUMBER                     6.02214129e23  // Avogadro number in #.mol^{-1}.
#define AMC_CARBON_MOLAR_MASS                   12.e-6         // Carbon molar mass in µg.mol^{-1}.
#define AMC_TEMPERATURE_ZERO_DEGREE_CELSIUS     273.15         // O°C in Kelvin.

// Numerical constants.
#define AMC_NUMBER_CONCENTRATION_MINIMUM        1.             // Default minimum particle number concentration in #.m^{-3}
#define AMC_MASS_CONCENTRATION_MINIMUM          1.e-20         // Default minimum particle number concentration in µg.m^{-3}
#define AMC_COAGULATION_NUMBER_TIME_STEP_MAX    100            // Maximum number of time steps for coagulation.
#define AMC_CONDENSATION_NUMBER_TIME_STEP_MAX   100            // Maximum number of time steps for condensation.

// Modeling constants.
#define AMC_AQUEOUS_PHASE_INDEX                 0              // Aqueous phase index.
#ifdef AMC_WITH_LAYER
#define AMC_LAYER_NUMBER_DEFAULT                1              // Default number of layers.
#define AMC_LAYER_SPECIES_DEFAULT               true           // By default all species are layered.
#endif

// The power function for cubic root.
#ifdef AMC_WITH_POW3
#define POW3(x) pow3(x)
#else
#define POW3(x) pow(x, AMC_FRAC3)
#endif

#ifndef CLIP
#define CLIP(x) x = x < real(0) ? real(0) : x
#endif

/*! \namespace AMC
 * 
 * Aerosol Model for Chimere.
 */
namespace AMC
{
  using namespace std;
#if __cplusplus > 199711L
  using namespace std::placeholders;
#endif

  using namespace blitz;

  using namespace util;

#if defined(AMC_WITH_TIMER) || defined(AMC_WITH_STATISTICS)
#if __cplusplus > 199711L
  typedef std::chrono::steady_clock clock_;
#else
  typedef std::chrono::monotonic_clock clock_;
#endif

  typedef std::chrono::time_point<clock_> point_type_;
  typedef std::chrono::duration<double> duration_type_;
#endif

  typedef double real;

  typedef vector<string>   vector1s;
  typedef vector<vector1s> vector2s;

  typedef vector<bool>     vector1b;
  typedef vector<vector1b> vector2b;

  typedef vector<int>      vector1i;
  typedef vector<vector1i> vector2i;
  typedef vector<vector2i> vector3i;

  typedef vector<real>     vector1r;
  typedef vector<vector1r> vector2r;
  typedef vector<vector2r> vector3r;

#define AMC_PARAMETERIZATION_REAL AMC::real
#define AMC_THERMODYNAMIC_REAL AMC::real

#ifdef AMC_WITH_LOGGER
  using namespace Logger;
#endif
}

#ifdef AMC_WITH_LOGGER
#include "share/AMCLogger.hxx"
#endif

#ifdef AMC_WITH_TIMER
#include "share/AMCTimer.hxx"
#endif

#ifdef AMC_WITH_STATISTICS
#include "share/AMCStatistics.hxx"
#endif

#include "share/Error.hxx"

#ifdef DRIVER_WITH_AMC
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  using namespace std;
  using namespace util;

#ifdef AMC_WITH_LOGGER
  using namespace Logger;
  using AMC::AMCLogger;
#endif

  typedef double real;

  typedef vector<int> vector1i;
  typedef vector<real> vector1r;
  typedef vector<string> vector1s;
  typedef vector<vector1i> vector2i;
  typedef vector<vector1r> vector2r;

#define NPF_PARAMETERIZATION_REAL NPF::real
}
#endif
#endif // DRIVER_WITH_AMC

#include "share/functions.hxx"
#ifdef AMC_WITH_MODAL
#include "share/ClassModalDistribution.hxx"
#endif

#ifdef DRIVER_WITH_AMC
#include "parameterization/ClassParameterizationPhysics.hxx"

#include "parameterization/ClassParameterizationDiameterBase.hxx"
#include "parameterization/ClassParameterizationDiameterFixed.hxx"
#include "parameterization/ClassParameterizationDiameterDensityFixed.hxx"
#include "parameterization/ClassParameterizationDiameterDensityMoving.hxx"
#ifdef AMC_WITH_GERBER
#include "parameterization/ClassParameterizationDiameterGerber.hxx"
#endif
#ifdef AMC_WITH_FRACTAL_SOOT
#include "parameterization/ClassParameterizationDiameterFractalSoot.hxx"
#endif
#ifdef AMC_WITH_SURFACE_TENSION
#include "parameterization/ClassParameterizationSurfaceTensionBase.hxx"
#include "parameterization/ClassParameterizationSurfaceTensionFixed.hxx"
#include "parameterization/ClassParameterizationSurfaceTensionAverage.hxx"
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
#include "parameterization/ClassParameterizationSurfaceTensionJacobson.hxx"
#endif
#endif
#include "parameterization/ClassParameterizationCollisionEfficiencyBase.hxx"
#ifdef AMC_WITH_COLLISION_EFFICIENCY
#include "parameterization/ClassParameterizationCollisionEfficiencyFriedlander.hxx"
#include "parameterization/ClassParameterizationCollisionEfficiencyJacobson.hxx"
#include "parameterization/ClassParameterizationCollisionEfficiencySeinfeld.hxx"
#endif
#ifdef AMC_WITH_COAGULATION
#include "parameterization/ClassParameterizationCoagulationBase.hxx"
#include "parameterization/ClassParameterizationCoagulationBrownian.hxx"
#ifdef AMC_WITH_WAALS_VISCOUS
#include "parameterization/ClassParameterizationCoagulationBrownianWaalsViscous.hxx"
#endif
#include "parameterization/ClassParameterizationCoagulationGravitational.hxx"
#include "parameterization/ClassParameterizationCoagulationTurbulent.hxx"
#include "parameterization/ClassParameterizationCoagulationUnit.hxx"
#include "parameterization/ClassParameterizationCoagulationVoid.hxx"
#include "parameterization/ClassParameterizationCoagulationTable.hxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "parameterization/ClassCondensationCorrectionFactorBase.hxx"
#include "parameterization/ClassCondensationCorrectionFactorDahneke.hxx"
#include "parameterization/ClassCondensationCorrectionFactorFuchsSutugin.hxx"
#ifdef AMC_WITH_KELVIN_EFFECT
#include "parameterization/ClassParameterizationKelvinEffect.hxx"
#endif
#include "parameterization/ClassCondensationFluxCorrectionAqueousBase.hxx"
#include "parameterization/ClassCondensationFluxCorrectionAqueousVoid.hxx"
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
#include "parameterization/ClassCondensationFluxCorrectionAqueousVersion1.hxx"
#include "parameterization/ClassCondensationFluxCorrectionAqueousVersion2.hxx"
#endif
#include "parameterization/ClassCondensationFluxCorrectionDryVoid.hxx"
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
#include "parameterization/ClassCondensationFluxCorrectionDryInorganicSalt.hxx"
#endif
#include "parameterization/ClassParameterizationCondensationBase.hxx"
#include "parameterization/ClassParameterizationCondensationVoid.hxx"
#include "parameterization/ClassParameterizationCondensationDiffusionLimited.hxx"
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
#include "parameterization/ClassParameterizationCondensationDiffusionLimitedSoot.hxx"
#endif
#endif
#ifdef AMC_WITH_NUCLEATION
#include "parameterization/ClassParameterizationNucleationBase.hxx"
#include "parameterization/ClassParameterizationNucleationVoid.hxx"
#ifdef AMC_WITH_POWER_LAW
#include "parameterization/ClassParameterizationNucleationPowerLaw.hxx"
#endif
#ifdef AMC_WITH_VEHKAMAKI
#include "parameterization/ClassParameterizationNucleationVehkamaki.hxx"
#endif
#ifdef AMC_WITH_MERIKANTO
#include "parameterization/ClassParameterizationNucleationMerikanto.hxx"
#endif
#endif
#include "thermodynamics/ClassModelThermodynamicBase.hxx"
#include "thermodynamics/ClassModelThermodynamicVoid.hxx"
#ifdef AMC_WITH_ISOROPIA
#include "thermodynamics/ClassModelThermodynamicIsoropia.hxx"
#endif
#ifdef AMC_WITH_PANKOW
#include "thermodynamics/ClassModelThermodynamicPankow.hxx"
#endif
#ifdef AMC_WITH_AEC
#include "thermodynamics/ClassModelThermodynamicAEC.hxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
#include "thermodynamics/ClassModelThermodynamicIsoropiaAEC.hxx"
#endif
#ifdef AMC_WITH_H2O
#include "thermodynamics/ClassModelThermodynamicH2O.hxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
#include "thermodynamics/ClassModelThermodynamicIsoropiaH2O.hxx"
#endif
#ifdef AMC_WITH_ADSORPTION
#include "thermodynamics/ClassModelAdsorptionBase.hxx"
#include "thermodynamics/ClassModelAdsorptionPankow.hxx"
#endif
#include "base/ClassConfiguration.hxx"

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  typedef AMC::ClassParameterizationCoagulationBrownianWaalsViscous npf_parameterization_coagulation_type;
  typedef AMC::ClassParameterizationCondensationDiffusionLimited<AMC::ClassCondensationCorrectionFactorDahneke, AMC::ClassCondensationFluxCorrectionAqueousVoid, AMC::ClassCondensationFluxCorrectionDryVoid> npf_parameterization_condensation_type;
  typedef AMC::ClassParameterizationNucleationBase npf_parameterization_nucleation_type;
}
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
#include "new_particle_formation/ClassSpecies.hxx"
#include "new_particle_formation/ClassDiscretizationSize.hxx"
#include "new_particle_formation/ClassParticle.hxx"
#include "new_particle_formation/ClassConcentration.hxx"
#include "new_particle_formation/ClassMeteoData.hxx"
#include "new_particle_formation/ClassParticleData.hxx"
#include "new_particle_formation/ClassDynamics.hxx"
#include "new_particle_formation/ClassNumericalSolver.hxx"
#include "new_particle_formation/ClassNewParticleFormation.hxx"
#endif
#include "species/ClassSpecies.hxx"
#include "species/ClassSpeciesTracer.hxx"
#include "species/ClassSpeciesEquilibrium.hxx"
#include "species/ClassPhase.hxx"
#include "base/ClassMeteorologicalData.hxx"
#include "discretization/ClassDiscretizationSize.hxx"
#include "discretization/ClassDiscretizationCompositionBase.hxx"
#include "discretization/ClassDiscretizationCompositionTable.hxx"
#include "discretization/ClassDiscretizationComposition.hxx"
#include "discretization/ClassDiscretizationClass.hxx"
#ifdef AMC_WITH_TRACE
#include "discretization/ClassTrace.hxx"
#endif
#include "discretization/ClassDiscretization.hxx"
#include "redistribution/ClassRedistributionSizeBase.hxx"
#include "redistribution/ClassRedistributionSizeMovingDiameter.hxx"
#include "redistribution/ClassRedistributionSizeEulerHybrid.hxx"
#include "redistribution/ClassRedistributionSizeEulerMixed.hxx"
#include "redistribution/ClassRedistributionSizeTable.hxx"
#include "redistribution/ClassRedistributionCompositionBase.hxx"
#include "redistribution/ClassRedistributionCompositionTable.hxx"
#include "redistribution/ClassRedistributionComposition.hxx"
#include "redistribution/ClassRedistributionClass.hxx"
#ifdef AMC_WITH_LAYER
#include "base/ClassLayer.hxx"
#endif
#include "base/ClassAerosolData.hxx"
#include "base/ClassThermodynamics.hxx"
#include "base/ClassParameterization.hxx"
#ifdef AMC_WITH_LAYER
#include "redistribution/ClassRedistributionLayer.hxx"
#endif
#include "redistribution/ClassRedistribution.hxx"
#ifdef AMC_WITH_REPARTITION_COEFFICIENT
#include "repartition_coefficient/ClassPDF.hxx"
#ifdef AMC_WITH_TRACE
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationTrace.hxx"
#endif
#ifdef AMC_WITH_LAYER
#include "repartition_coefficient/ClassRing.hxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationLayer.hxx"
#endif
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationBase.hxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationSize.hxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationStatic.hxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationMoving.hxx"
#endif
#include "dynamics/ClassDynamicsBase.hxx"
#ifdef AMC_WITH_COAGULATION
#include "dynamics/ClassDynamicsCoagulation.hxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "dynamics/ClassDynamicsCondensation.hxx"
#endif
#ifdef AMC_WITH_NUCLEATION
#include "dynamics/ClassDynamicsNucleation.hxx"
#endif
#ifdef AMC_WITH_DIFFUSION
#include "dynamics/ClassDynamicsDiffusion.hxx"
#endif
#ifdef AMC_WITH_NUMERICS
#include "numerics/ClassNumericalSolverBase.hxx"
#ifdef AMC_WITH_COAGULATION
#include "numerics/ClassNumericalSolverCoagulation.hxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "numerics/ClassNumericalSolverCondensation.hxx"
#endif
#ifdef AMC_WITH_NUCLEATION
#include "numerics/ClassNumericalSolverNucleation.hxx"
#endif
#endif
#include "base/ClassAMC.hxx"
#ifdef AMC_WITH_RUN
#include "base/ClassAMX.hxx"
#endif
#endif // DRIVER_WITH_AMC


#ifdef TRY
#undef TRY
#endif
#define TRY try                                         \
    {

#ifdef END
#undef END
#endif
#define END                                             \
  }                                                     \
    catch (AMC::Error& err)	                        \
      {                                                 \
        err.CoutWhat();                                 \
        return 1;                                       \
      }                                                 \
    catch (Ops::Error& err)	                        \
      {                                                 \
        err.CoutWhat();                                 \
        return 1;                                       \
      }                                                 \
    catch (std::exception& err)                         \
      {                                                 \
        cout << "C++ exception: "                       \
             << err.what() << endl;                     \
        return 1;                                       \
      }                                                 \
    catch (std::string& str)                            \
      {                                                 \
        cout << str << endl;                            \
        return 1;                                       \
      }                                                 \
    catch (const char* str)                             \
      {                                                 \
        cout << str << endl;                            \
        return 1;                                       \
      }                                                 \
    catch(...)                                          \
      {                                                 \
        cout << "Unknown exception..." <<endl;          \
        return 1;                                       \
      }

#define AMC_FILE_AMC_HEADER_HXX
#endif
