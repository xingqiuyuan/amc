// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_CPP

#include "AMC.hxx"

#ifdef AMC_WITH_LOGGER
#include "logger/Logger.cpp"
#endif

namespace AMC
{
  // Compute sum of vector or part of vector.
  template real compute_vector_sum<real>(const vector<real> &v, int n);

  // Compute the determinant of a matrix, stored in row major (C-style).
  template real compute_matrix_determinant<real>(const vector<real> &matrix, int n);

  // Find polygon convex hull.
  template void find_polygon_convex_hull<real>(const int &p, const int &n, const vector<real> &point, vector<int> &edge);

  // Generate random vectors.
  template vector<real> generate_random_vector<real>(const int &n, const real sum);

  // Unit conversion.
  template real convert_ppm_to_molecule_per_cm3<real>(const real &pressure,
                                                      const real &temperature,
                                                      const real concentration);
  template real convert_ppt_to_molecule_per_cm3<real>(const real &pressure,
                                                      const real &temperature,
                                                      const real concentration);
  template real convert_molecule_per_cm3_to_ppm<real>(const real &pressure,
                                                      const real &temperature,
                                                      const real concentration);
  template real convert_molecule_per_cm3_to_ppt<real>(const real &pressure,
                                                      const real &temperature,
                                                      const real concentration);

#ifdef AMC_WITH_LOGGER
  class AMCLogger;
#endif

#ifdef AMC_WITH_TIMER
  template class AMCTimer<CPU>;
  template class AMCTimer<Wall>;
#endif

#ifdef AMC_WITH_STATISTICS
  class AMCStatistics;
#endif

#ifdef AMC_WITH_MODAL
  // Independent modal distribution class.
  template class ClassModalDistribution<AMC::real>;
#endif

  // Physical parameterizations.
  class ClassParameterizationPhysics;

  class ClassParameterizationDiameterBase;
  class ClassParameterizationDiameterFixed;
  class ClassParameterizationDiameterDensityFixed;
  class ClassParameterizationDiameterDensityMoving;

  // Gerber's wet diameter formula.
#ifdef AMC_WITH_GERBER
  class ClassParameterizationDiameterGerber;
#endif

  // Fractal soot diameter parameterization.
#ifdef AMC_WITH_FRACTAL_SOOT
  class ClassParameterizationDiameterFractalSoot;
#endif

  // Configuration class.
  class ClassConfiguration;
  class init_flag;

  template bool ClassConfiguration::IsInitialized<init_flag::no>();
  template bool ClassConfiguration::IsInitialized<init_flag::amc>();
  template bool ClassConfiguration::IsInitialized<init_flag::amx>();

  template void ClassConfiguration::SetInitialized<init_flag::no>();
  template void ClassConfiguration::SetInitialized<init_flag::amc>();
  template void ClassConfiguration::SetInitialized<init_flag::amx>();

  // Species class.
  class ClassSpecies;
  class ClassSpeciesTracer;
  class ClassSpeciesEquilibrium;

  // Phase class.
  class ClassPhase;

#ifdef AMC_WITH_TRACE
  // Trace classes.
  class ClassTrace;
#endif

#ifdef AMC_WITH_LAYER
  // Layer classes.
  class ClassLayer;
#endif

  // Meteorological data.
  class ClassMeteorologicalData;

  // Thermodynamic models.
  class ClassModelThermodynamicBase;
  class ClassModelThermodynamicVoid;
#ifdef AMC_WITH_ISOROPIA
  class ClassModelThermodynamicIsoropia;
#endif
#ifdef AMC_WITH_PANKOW
  class ClassModelThermodynamicPankow;
#endif
#ifdef AMC_WITH_AEC
  class ClassModelThermodynamicAEC;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
  class ClassModelThermodynamicIsoropiaAEC;
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
  class ClassModelThermodynamicIsoropiaH2O;
#endif
#ifdef AMC_WITH_H2O
  class ClassModelThermodynamicH2O;
#endif

  // Adsorption.
#ifdef AMC_WITH_ADSORPTION
  class ClassModelAdsorptionBase;
  class ClassModelAdsorptionPankow;
#endif

  // Surface tension.
#ifdef AMC_WITH_SURFACE_TENSION
  class ClassParameterizationSurfaceTensionBase;
  class ClassParameterizationSurfaceTensionFixed;
  class ClassParameterizationSurfaceTensionAverage;
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
  class ClassParameterizationSurfaceTensionJacobson;
#endif
#endif

  // Collision efficiency.
  class ClassParameterizationCollisionEfficiencyBase;
#ifdef AMC_WITH_COLLISION_EFFICIENCY
  class ClassParameterizationCollisionEfficiencyFriedlander;
  class ClassParameterizationCollisionEfficiencyJacobson;
  class ClassParameterizationCollisionEfficiencySeinfeld;
#endif

  // Coagulation parameterizations.
#ifdef AMC_WITH_COAGULATION
  class ClassParameterizationCoagulationBase;
  class ClassParameterizationCoagulationBrownian;
#ifdef AMC_WITH_WAALS_VISCOUS
  class ClassParameterizationCoagulationBrownianWaalsViscous;
#endif
  template class ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyBase>;
#ifdef AMC_WITH_COLLISION_EFFICIENCY
  template class ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyFriedlander>;
  template class ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencyJacobson>;
  template class ClassParameterizationCoagulationGravitational<ClassParameterizationCollisionEfficiencySeinfeld>;
#endif
  class ClassParameterizationCoagulationTurbulent;
  class ClassParameterizationCoagulationUnit;
  class ClassParameterizationCoagulationVoid;
  class ClassParameterizationCoagulationTable;
#endif

  // Condensation parameterizations.
#ifdef AMC_WITH_CONDENSATION
  class ClassCondensationCorrectionFactorBase;
  class ClassCondensationCorrectionFactorDahneke;
  class ClassCondensationCorrectionFactorFuchsSutugin;
#ifdef AMC_WITH_KELVIN_EFFECT
  class ClassParameterizationKelvinEffect;
#endif
  class ClassCondensationFluxCorrectionAqueousBase;
  class ClassCondensationFluxCorrectionAqueousVoid;
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  class ClassCondensationFluxCorrectionAqueousVersion1;
  class ClassCondensationFluxCorrectionAqueousVersion2;
#endif
  class ClassCondensationFluxCorrectionDryVoid;
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  class ClassCondensationFluxCorrectionDryInorganicSalt;
#endif
  class ClassParameterizationCondensationBase;
  class ClassParameterizationCondensationVoid;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                   ClassCondensationFluxCorrectionAqueousVoid,
                                                                   ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                   ClassCondensationFluxCorrectionAqueousVoid,
                                                                   ClassCondensationFluxCorrectionDryVoid>;
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                   ClassCondensationFluxCorrectionAqueousVersion1,
                                                                   ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                   ClassCondensationFluxCorrectionAqueousVersion1,
                                                                   ClassCondensationFluxCorrectionDryVoid>;

  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                   ClassCondensationFluxCorrectionAqueousVersion2,
                                                                   ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                   ClassCondensationFluxCorrectionAqueousVersion2,
                                                                   ClassCondensationFluxCorrectionDryVoid>;
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                   ClassCondensationFluxCorrectionAqueousVersion1,
                                                                   ClassCondensationFluxCorrectionDryInorganicSalt>;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                   ClassCondensationFluxCorrectionAqueousVersion1,
                                                                   ClassCondensationFluxCorrectionDryInorganicSalt>;

  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorDahneke,
                                                                   ClassCondensationFluxCorrectionAqueousVersion2,
                                                                   ClassCondensationFluxCorrectionDryInorganicSalt>;
  template class ClassParameterizationCondensationDiffusionLimited<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                   ClassCondensationFluxCorrectionAqueousVersion2,
                                                                   ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                       ClassCondensationFluxCorrectionAqueousVoid,
                                                                       ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                       ClassCondensationFluxCorrectionAqueousVoid,
                                                                       ClassCondensationFluxCorrectionDryVoid>;
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                       ClassCondensationFluxCorrectionAqueousVersion1,
                                                                       ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                       ClassCondensationFluxCorrectionAqueousVersion1,
                                                                       ClassCondensationFluxCorrectionDryVoid>;

  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                       ClassCondensationFluxCorrectionAqueousVersion2,
                                                                       ClassCondensationFluxCorrectionDryVoid>;
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                       ClassCondensationFluxCorrectionAqueousVersion2,
                                                                       ClassCondensationFluxCorrectionDryVoid>;
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                       ClassCondensationFluxCorrectionAqueousVersion1,
                                                                       ClassCondensationFluxCorrectionDryInorganicSalt>;
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                       ClassCondensationFluxCorrectionAqueousVersion1,
                                                                       ClassCondensationFluxCorrectionDryInorganicSalt>;

  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorDahneke,
                                                                       ClassCondensationFluxCorrectionAqueousVersion2,
                                                                       ClassCondensationFluxCorrectionDryInorganicSalt>;
  template class ClassParameterizationCondensationDiffusionLimitedSoot<ClassCondensationCorrectionFactorFuchsSutugin,
                                                                       ClassCondensationFluxCorrectionAqueousVersion2,
                                                                       ClassCondensationFluxCorrectionDryInorganicSalt>;
#endif
#endif
#endif
#endif

  // Nucleation parameterizations.
#ifdef AMC_WITH_NUCLEATION
  class ClassParameterizationNucleationBase;
  class ClassParameterizationNucleationVoid;
#ifdef AMC_WITH_POWER_LAW
  class ClassParameterizationNucleationPowerLaw;
#endif
#ifdef AMC_WITH_VEHKAMAKI
  class ClassParameterizationNucleationVehkamaki;
#endif
#ifdef AMC_WITH_MERIKANTO
  class ClassParameterizationNucleationMerikanto;
#endif
#endif

  // Size and composition discretization.
  class ClassDiscretizationSize;
  class ClassDiscretizationCompositionBase;
  class ClassDiscretizationCompositionTable;
  template class ClassDiscretizationComposition<ClassDiscretizationCompositionBase>;
  template class ClassDiscretizationComposition<ClassDiscretizationCompositionTable>;
  template class ClassDiscretizationClass<ClassDiscretizationCompositionBase>;
  template class ClassDiscretizationClass<ClassDiscretizationCompositionTable>;
  template class ClassDiscretization<ClassDiscretizationCompositionBase>;
  template class ClassDiscretization<ClassDiscretizationCompositionTable>;

  // Redistribution size.
  class ClassRedistributionSizeBase;
  class ClassRedistributionSizeEulerMixed;
  class ClassRedistributionSizeEulerHybrid;
  class ClassRedistributionSizeMovingDiameter;
  template class ClassRedistributionSizeTable<ClassRedistributionSizeEulerMixed>;
  template class ClassRedistributionSizeTable<ClassRedistributionSizeEulerHybrid>;

  // Redistribution composition.
  template class ClassRedistributionCompositionBase<ClassDiscretizationCompositionBase>;
  template class ClassRedistributionCompositionBase<ClassDiscretizationCompositionTable>;
  template class ClassRedistributionCompositionTable<ClassDiscretizationCompositionBase>;
  template class ClassRedistributionCompositionTable<ClassDiscretizationCompositionTable>;

  template class ClassRedistributionComposition<redist_comp_base_base, disc_comp_base>;
  template class ClassRedistributionComposition<redist_comp_base_table, disc_comp_base>;
  template class ClassRedistributionComposition<redist_comp_table_base, disc_comp_table>;
  template class ClassRedistributionComposition<redist_comp_table_table, disc_comp_table>;

  // Aerosol data.
  class ClassAerosolData;

  // Thermodynamic models.
  class ClassThermodynamics;

  // Aerosol physical parameterizations.
  class ClassParameterization;

  // Redistribution.
  template class ClassRedistributionClass<redist_comp_base_base, disc_comp_base>;
  template class ClassRedistributionClass<redist_comp_table_table, disc_comp_table>;
  template class ClassRedistributionClass<redist_comp_base_table, disc_comp_base>;
  template class ClassRedistributionClass<redist_comp_table_base, disc_comp_table>;

#ifdef AMC_WITH_LAYER
  class ClassRedistributionLayer;
#endif

  template class ClassRedistribution<redist_size_mixed, redist_comp_base_base, disc_comp_base>;
  template class ClassRedistribution<redist_size_hybrid, redist_comp_base_base, disc_comp_base>;
  template class ClassRedistribution<redist_size_mixed_table, redist_comp_base_base, disc_comp_base>;
  template class ClassRedistribution<redist_size_hybrid_table, redist_comp_base_base, disc_comp_base>;
  template class ClassRedistribution<redist_size_moving, redist_comp_base_base, disc_comp_base>;

  template class ClassRedistribution<redist_size_mixed, redist_comp_base_table, disc_comp_base>;
  template class ClassRedistribution<redist_size_hybrid, redist_comp_base_table, disc_comp_base>;
  template class ClassRedistribution<redist_size_mixed_table, redist_comp_base_table, disc_comp_base>;
  template class ClassRedistribution<redist_size_hybrid_table, redist_comp_base_table, disc_comp_base>;
  template class ClassRedistribution<redist_size_moving, redist_comp_base_table, disc_comp_base>;

  template class ClassRedistribution<redist_size_mixed, redist_comp_table_table, disc_comp_table>;
  template class ClassRedistribution<redist_size_hybrid, redist_comp_table_table, disc_comp_table>;
  template class ClassRedistribution<redist_size_mixed_table, redist_comp_table_table, disc_comp_table>;
  template class ClassRedistribution<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table>;
  template class ClassRedistribution<redist_size_moving, redist_comp_table_table, disc_comp_table>;

#ifdef AMC_WITH_REPARTITION_COEFFICIENT
  class ClassPDF;
#ifdef AMC_WITH_TRACE
  class ClassCoefficientRepartitionCoagulationTrace;
#endif
#ifdef AMC_WITH_LAYER
  class ClassRing;
  class ClassCoefficientRepartitionCoagulationLayer;
#endif
  class ClassCoefficientRepartitionCoagulationBase;
  class ClassCoefficientRepartitionCoagulationSize;
  template class ClassCoefficientRepartitionCoagulationStatic<disc_comp_base>;
  template class ClassCoefficientRepartitionCoagulationStatic<disc_comp_table>;
  template class ClassCoefficientRepartitionCoagulationMoving<redist_comp_base_base, disc_comp_base>;
  template class ClassCoefficientRepartitionCoagulationMoving<redist_comp_table_table, disc_comp_table>;
#endif

  // Dynamics.
  class ClassDynamicsBase;

  // Coagulation.
#ifdef AMC_WITH_COAGULATION
  template class ClassDynamicsCoagulation<coag_coef_static_base>;
  template class ClassDynamicsCoagulation<coag_coef_static_table>;
  template class ClassDynamicsCoagulation<coag_coef_moving_base>;
  template class ClassDynamicsCoagulation<coag_coef_moving_table>;
#endif

  // Condensation.
#ifdef AMC_WITH_CONDENSATION
  class ClassDynamicsCondensation;
#endif

  // Nucleation.
#ifdef AMC_WITH_NUCLEATION
  class ClassDynamicsNucleation;
#endif

#ifdef AMC_WITH_DIFFUSION
  class ClassDynamicsDiffusion;
#endif

  // Numerics.
#ifdef AMC_WITH_NUMERICS
  class ClassNumericalSolverBase;
#ifdef AMC_WITH_COAGULATION
  template class ClassNumericalSolverCoagulation<coag_coef_moving_base>;
  template class ClassNumericalSolverCoagulation<coag_coef_moving_table>;
  template class ClassNumericalSolverCoagulation<coag_coef_static_base>;
  template class ClassNumericalSolverCoagulation<coag_coef_static_table>;
#endif
#ifdef AMC_WITH_CONDENSATION
  class ClassNumericalSolverCondensation;
#endif
#ifdef AMC_WITH_NUCLEATION
  class ClassNumericalSolverNucleation;
#endif
#endif

  // AMC base class.
  template class ClassAMC<ClassDiscretizationCompositionBase>;
  template class ClassAMC<ClassDiscretizationCompositionTable>;

#ifdef AMC_WITH_RUN
  // AMC executable class.
#ifdef AMC_WITH_COAGULATION
  template class ClassAMX<redist_size_mixed, redist_comp_base_base,
                          disc_comp_base, coag_coef_moving_base >;
  template class ClassAMX<redist_size_mixed_table, redist_comp_table_table,
                          disc_comp_table, coag_coef_moving_table >;
  template class ClassAMX<redist_size_mixed, redist_comp_base_base,
                          disc_comp_base, coag_coef_static_base >;
  template class ClassAMX<redist_size_mixed_table, redist_comp_table_table,
                          disc_comp_table, coag_coef_static_table >;

  template class ClassAMX<redist_size_hybrid, redist_comp_base_base,
                          disc_comp_base, coag_coef_moving_base >;
  template class ClassAMX<redist_size_hybrid_table, redist_comp_table_table,
                          disc_comp_table, coag_coef_moving_table >;
  template class ClassAMX<redist_size_hybrid, redist_comp_base_base,
                          disc_comp_base, coag_coef_static_base >;
  template class ClassAMX<redist_size_hybrid_table, redist_comp_table_table,
                          disc_comp_table, coag_coef_static_table >;

  template class ClassAMX<redist_size_moving, redist_comp_base_base,
                          disc_comp_base, coag_coef_moving_base >;
  template class ClassAMX<redist_size_moving, redist_comp_table_table,
                          disc_comp_table, coag_coef_moving_table >;
  template class ClassAMX<redist_size_moving, redist_comp_base_base,
                          disc_comp_base, coag_coef_static_base >;
  template class ClassAMX<redist_size_moving, redist_comp_table_table,
                          disc_comp_table, coag_coef_static_table >;
#endif

  // Without coagulation.
  template class ClassAMX<redist_size_mixed, redist_comp_base_base, disc_comp_base >;
  template class ClassAMX<redist_size_mixed_table, redist_comp_table_table, disc_comp_table >;

  template class ClassAMX<redist_size_hybrid, redist_comp_base_base, disc_comp_base >;
  template class ClassAMX<redist_size_hybrid_table, redist_comp_table_table, disc_comp_table >;

  template class ClassAMX<redist_size_moving, redist_comp_base_base, disc_comp_base >;
  template class ClassAMX<redist_size_moving, redist_comp_table_table, disc_comp_table >;
#endif
}

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  class ClassSpecies;
  class ClassDiscretizationSize;
  class ClassParticle;
  class ClassConcentration;
  class ClassMeteoData;
  class ClassParticleData;
  template class ClassDynamics<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
  template class ClassNumericalSolver<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
  template class ClassNewParticleFormation<npf_parameterization_coagulation_type, npf_parameterization_condensation_type>;
}
#endif

#define AMC_FILE_AMC_CPP
#endif
