// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_TYPEDEF_HXX

typedef AMC::ClassDiscretizationCompositionBase  disc_comp_base;
typedef AMC::ClassDiscretizationCompositionTable disc_comp_table;

typedef AMC::ClassRedistributionSizeEulerMixed  redist_size_mixed;
typedef AMC::ClassRedistributionSizeEulerHybrid redist_size_hybrid;
typedef AMC::ClassRedistributionSizeMovingDiameter redist_size_moving;
typedef AMC::ClassRedistributionSizeTable<redist_size_mixed>  redist_size_mixed_table;
typedef AMC::ClassRedistributionSizeTable<redist_size_hybrid> redist_size_hybrid_table;

typedef AMC::ClassRedistributionCompositionBase<disc_comp_base>   redist_comp_base_base;
typedef AMC::ClassRedistributionCompositionBase<disc_comp_table>  redist_comp_table_base;
typedef AMC::ClassRedistributionCompositionTable<disc_comp_base>  redist_comp_base_table;
typedef AMC::ClassRedistributionCompositionTable<disc_comp_table> redist_comp_table_table;

#ifdef AMC_WITH_COAGULATION
typedef AMC::ClassCoefficientRepartitionCoagulationStatic<disc_comp_base> coag_coef_static_base;
typedef AMC::ClassCoefficientRepartitionCoagulationStatic<disc_comp_table> coag_coef_static_table;
typedef AMC::ClassCoefficientRepartitionCoagulationMoving<redist_comp_base_base, disc_comp_base> coag_coef_moving_base;
typedef AMC::ClassCoefficientRepartitionCoagulationMoving<redist_comp_table_table, disc_comp_table> coag_coef_moving_table;
#else
typedef int coag_coef_static_base;
typedef int coag_coef_static_table;
typedef int coag_coef_moving_base;
typedef int coag_coef_moving_table;
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
typedef NPF::npf_parameterization_coagulation_type npf_parameterization_coagulation_type;
typedef NPF::npf_parameterization_condensation_type npf_parameterization_condensation_type;
#endif

#define AMC_FILE_TYPEDEF_HXX
#endif
