// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_PARTICLE_CXX

#include "ClassParticle.hxx"

namespace NPF
{
  // Free one particle.
  inline void ClassParticle::free_particle(const int h)
  {
    // Index of particle.
    const int i = index_active_[h];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "free particle " << i << " at index " << h << endl;
#endif

    // Nullify size.
    size_[i] = real(0);

    // Section index of particle.
    const int k = particle_section_index_[i];

    int &Nparticle_section_active = Nparticle_section_active_[k];
    vector1i &section_particle_index = section_particle_index_[k];

    // Say to the section that one particle is free.
    --Nparticle_section_active;

    for (int j = section_particle_index_local_[i]; j < Nparticle_section_active; ++j)
      {
        const int l = section_particle_index[j + 1];
        section_particle_index[j] = l;
        section_particle_index_local_[l]--;
      }

    section_particle_index[Nparticle_section_active] = i;
    section_particle_index_local_[i] = Nparticle_section_active;

    index_active_[h] = -1;
    index_active_reverse_[i] = -1;
  }


  // Find closest particle or new one in one given section.
  inline int ClassParticle::find_particle(const int i, const real &size, bool &new_particle)
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "particle size = " << size << endl;
    CBUG << "search " << (new_particle ? "new" : "existing") << " numerical particle in section " << i << endl;
#endif

    // Static number of particle in section.
    const int Nparticle_section = Nparticle_section_[i];

    // Current number of active particles in given section.
    int &Nparticle_section_active = Nparticle_section_active_[i];
    vector1i &section_particle_index = section_particle_index_[i];

    // Index of numerical particle.
    int j(-1);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "new_particle = " << (new_particle ? "yes" : "no") << endl;
    CBUG << "size section = " << i << endl;
    CBUG << "Nparticle_section_active = " << Nparticle_section_active << endl;
    CBUG << "Nparticle_section = " << Nparticle_section << endl;
#endif

    if (new_particle && Nparticle_section_active < Nparticle_section)
      {
        j = section_particle_index[Nparticle_section_active];
        size_[j] = size;

        index_active_[Nparticle_active_] = j;
        index_active_reverse_[j] = Nparticle_active_++;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "new particle index = " << j << endl;
#endif

        // Eventually move particle so that it remain in ascending order within section.
        for (int k = Nparticle_section_active; k > 0; --k)
          {
            int &u = section_particle_index[k - 1];
            int &v = section_particle_index[k];

            if (size_[u] > size_[v])
              {
                ++section_particle_index_local_[u];
                --section_particle_index_local_[v];
                const int w = u;
                u = v;
                v = w;
              }
          }

        ++Nparticle_section_active;
      }
    else
      {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        for (int k = 1; k < Nparticle_section_active; ++k)
          if (size_[section_particle_index[k - 1]] > size_[section_particle_index[k]])
            {
              CBUG << "i = " << i << endl;
              CBUG << "k = " << k << endl;
              CBUG << "section_particle_index = " << display_section_particle_index(i) << endl;
              throw AMC::Error("Particles in section i are not in ascending order at position k.");
            }
#endif

        // Search nearest particle.
        if (size <= size_[section_particle_index[0]])
          j = 0;
        else if (size >= size_[section_particle_index[Nparticle_section_active - 1]])
          j = Nparticle_section_active - 1;
        else
          {
            real delta, delta2(size - size_[section_particle_index[0]]);

            for (int k = 1; k <= Nparticle_section_active; ++k)
              {
                delta = delta2;
                delta2 = size - size_[section_particle_index[k]];

                if (delta2 <= real(0))
                  {
                    j = (delta < - delta2) ? k - 1 : k;
                    break;
                  }
              }
          }

        j = section_particle_index[j];
        new_particle = false;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "merge with particle " << j << endl;
#endif
      }

    return j;
  }


  // Remove particle from index.
  inline void ClassParticle::remove_particle()
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    const int Nparticle_active(Nparticle_active_);
#endif

    Nparticle_active_ = 0;
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      {
        const int &Nparticle_section_active = Nparticle_section_active_[i];
        vector1i &section_particle_index = section_particle_index_[i];

        for (int j = 0; j < Nparticle_section_active; ++j)
          {
            const int k = section_particle_index[j];
            index_active_[Nparticle_active_] = k;
            index_active_reverse_[k] = Nparticle_active_++;
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "Removed " << Nparticle_active - Nparticle_active_ << " particles." << endl;
#endif
  }


#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  // Debug methods.
  string ClassParticle::display_size()
  {
    // Display sie in diameter (nm).
    vector1r size(Nparticle_active_);
    for (int i = 0; i < Nparticle_active_; ++i)
      {
        size[i] = size_[index_active_[i]];
        size[i] = pow(size[i] * AMC_INV_PI6 / ClassDiscretizationSize::density_fixed_, AMC_FRAC3) * real(1e3);
      }

    return to_str(size);
  }


  string ClassParticle::display_section_size(const int i)
  {
    // Display size in diameter (nm).
    vector1r size(Nparticle_section_active_[i]);
    for (int j = 0; j < Nparticle_section_active_[i]; ++j)
      {
        size[i] = size_[section_particle_index_[i][j]];
        size[i] = pow(size[i] * AMC_INV_PI6 / ClassDiscretizationSize::density_fixed_, AMC_FRAC3) * real(1e3);
      }

    return to_str(size);
  }


  string ClassParticle::display_index_active()
  {
    vector1i index_active(index_active_.begin(), index_active_.begin() + Nparticle_active_);
    return to_str(index_active);
  }


  string ClassParticle::display_index_active_reverse()
  {
    map<int, int> index_active_reverse;
    for (int i = 0; i < Nparticle_active_; ++i)
      index_active_reverse[index_active_[i]] = index_active_reverse_[index_active_[i]];
    return to_str(index_active_reverse);
  }


  string ClassParticle::display_section_particle_index(const int i)
  {
    vector1i section_particle_index_active(section_particle_index_[i].begin(),
                                           section_particle_index_[i].begin() + Nparticle_section_active_[i]);
    vector1i section_particle_index_free(section_particle_index_[i].begin() + Nparticle_section_active_[i],
                                         section_particle_index_[i].begin() + Nparticle_section_[i]);

    return to_str(section_particle_index_active) + " " + to_str(section_particle_index_free);
  }


  string ClassParticle::display_section_particle_index()
  {
    ostringstream sout;
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      sout << i << " : " << display_section_particle_index(i) << endl;

    return sout.str();
  }


  inline void  ClassParticle::test_particle_section_ascending_order()
  {
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      for (int j = 1; j < Nparticle_section_active_[i]; ++j)
        if (size_[section_particle_index_[i][j - 1]] > size_[section_particle_index_[i][j]])
          {
            CBUG << "section_particle_index = " << display_section_particle_index(i) << endl;
            CBUG << "size = " << display_section_size(i) << endl;
            throw AMC::Error("Particles in section " + to_str(i) + " are not in ascending order.");
          }
  }


  inline void  ClassParticle::test_particle_ascending_order()
  {
    for (int h = 1; h < NPF::ClassParticle::Nparticle_active_; ++h)
      if (size_[index_active_[h - 1]] > size_[index_active_[h]])
          {
            CBUG << "size = " << display_size() << endl;
            throw AMC::Error("Particles at index " + to_str(h) + " are not in ascending order.");
          }
  }
#endif


  // Init static data.
  void ClassParticle::Init(Ops::Ops &ops)
  {
    if (ops.Is<int>("particle.number"))
      {
        Nparticle_ = ops.Get<int>("particle.number");
        Nparticle_section_.assign(NPF::ClassDiscretizationSize::Nsection_,
                                  Nparticle_ / NPF::ClassDiscretizationSize::Nsection_);

        for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
          if (i < Nparticle_ % NPF::ClassDiscretizationSize::Nsection_)
            Nparticle_section_[i]++;
      }
    else
      {
        ops.Set("particle.number", "", Nparticle_section_);
  
        if (int(Nparticle_section_.size()) != NPF::ClassDiscretizationSize::Nsection_)
          throw AMC::Error("Need to provide number of particles for all sections.");

        Nparticle_ = 0;
        for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
          Nparticle_ += Nparticle_section_[i];
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << Reset() << "Init particle based model with Nparticle = "
                         << Nparticle_ << " and " << NPF::ClassDiscretizationSize::Nsection_ << " sections." << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "Number of particles per section = " << Nparticle_section_ << endl;
#endif

    // Section index of particles.
    particle_section_index_.resize(Nparticle_);

    int h(-1);
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      for (int j = 0; j < Nparticle_section_[i]; ++j)
        particle_section_index_[++h] = i;

    section_particle_index_local_.resize(Nparticle_);
    section_particle_index_.resize(NPF::ClassDiscretizationSize::Nsection_);
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      section_particle_index_[i].resize(Nparticle_section_[i]);

    InitStep();
  }


  // Init step.
  void ClassParticle::InitStep()
  {
    // Active particles.
    Nparticle_active_ = 0;

    int h(-1);
    for (int i = 0; i < NPF::ClassDiscretizationSize::Nsection_; ++i)
      for (int j = 0; j < Nparticle_section_[i]; ++j)
        {
          section_particle_index_[i][j] = ++h;
          section_particle_index_local_[h] = j;
        }

    Nparticle_section_active_.assign(NPF::ClassDiscretizationSize::Nsection_, 0);

    index_active_.assign(Nparticle_, -1);
    index_active_reverse_.assign(Nparticle_, -1);
    size_.assign(Nparticle_, real(0));
  }


  // Clear static data.
  void ClassParticle::Clear()
  {
    Nparticle_ = 0;
    Nparticle_section_.clear();
    particle_section_index_.clear();

    Nparticle_active_ = 0;
    section_particle_index_.clear();
    section_particle_index_local_.clear();
    Nparticle_section_active_.clear();
    index_active_.clear();
    index_active_reverse_.clear();
    size_.clear();
  }


  // Get methods.
  int ClassParticle::GetNparticle()
  {
    return Nparticle_;
  }


  int ClassParticle::GetNparticleSection(const int i)
  {
    return Nparticle_section_[i];
  }


  int ClassParticle::GetParticleSectionIndex(const int i)
  {
    return particle_section_index_[i];
  }


  int ClassParticle::GetNparticleActive()
  {
    return Nparticle_active_;
  }


  int ClassParticle::GetNparticleSectionActive(const int i)
  {
    return Nparticle_section_active_[i];
  }


  void ClassParticle::GetSectionParticleIndex(const int i, vector<int> &section_particle_index)
  {
    section_particle_index = section_particle_index_[i];
  }


  int ClassParticle::GetSectionParticleIndexLocal(const int i)
  {
    return section_particle_index_local_[i];
  }


  void ClassParticle::GetIndexActive(vector<int> &index_active)
  {
    index_active.assign(index_active_.begin(), index_active_.begin() + Nparticle_active_);
  }


  void ClassParticle::GetSize(vector<real> &size)
  {
    size.resize(Nparticle_active_);

    for (int i = 0; i < Nparticle_active_; ++i)
      size[i] = size_[index_active_[i]];
  }


  // Sort particles.
  void ClassParticle::SortParticle()
  {
    // On a per section basis.

    int Nparticle_active(0);
    for (int h = 0; h < NPF::ClassDiscretizationSize::Nsection_; ++h)
      {
        const int &Nparticle_section_active = Nparticle_section_active_[h];
        vector1i &section_particle_index = section_particle_index_[h];

        int i(0), j(1);

        while (j < Nparticle_section_active)
          {
            int *u = &section_particle_index[i];
            int *v = &section_particle_index[j];

            if (size_[*u] > size_[*v])
              {
                int k(i), l(j);
                int w = *u;

                do {
                  section_particle_index_local_[*u]++;
                  section_particle_index_local_[*v]--;

                  *u = *v;
                  *v = w;

                  l = k--;

                  if (k < 0) break;

                  u = &section_particle_index[k];
                  v = &section_particle_index[l];
                  w = *u;
                }
                while (size_[*u] > size_[*v]);
              }

            i = j++;
          }

        for (i = 0; i < Nparticle_section_active; ++i)
          {
            const int j = section_particle_index[i];
            index_active_[Nparticle_active] = j;
            index_active_reverse_[j] = Nparticle_active++;
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    for (int h = 0; h < NPF::ClassDiscretizationSize::Nsection_; ++h)
      for (int i = 0; i < Nparticle_section_active_[h]; ++i)
        if (section_particle_index_local_[section_particle_index_[h][i]] != i)
          {
            CBUG << "h = " << h << endl;
            CBUG << "Nparticle_section_active = " << Nparticle_section_active_[h] << endl;
            CBUG << "section_particle_index = " << display_section_particle_index(h) << endl;
            CBUG << "section_particle_index_local = " << section_particle_index_local_ << endl;
            throw AMC::Error("Local index does not correspond to particle position.");
          }
#endif
  }
}

#define NPF_FILE_CLASS_PARTICLE_CXX
#endif
