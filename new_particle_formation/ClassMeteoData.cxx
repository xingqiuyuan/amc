// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_METEO_DATA_CXX

#include "ClassMeteoData.hxx"

namespace NPF
{
  // Init.
  void ClassMeteoData::Init()
  {
    pressure_ = real(AMC_PRESSURE_REFERENCE);
    temperature_ = real(AMC_TEMPERATURE_REFERENCE);
    air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);
    air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);

    gas_diffusivity_.assign(NPF::ClassSpecies::Nspecies_, real(0));
    gas_quadratic_mean_velocity_.assign(NPF::ClassSpecies::Nspecies_, real(0));

    InitStep();
  }


  // Init step.
  void ClassMeteoData::InitStep()
  {
    // Pressure.
    pressure_ = AMC::ClassMeteorologicalData::pressure_;

    // Temperature.
    temperature_ = AMC::ClassMeteorologicalData::temperature_;

    // Relative humidity.
    relative_humidity_ = AMC::ClassMeteorologicalData::relative_humidity_;

    // Air free mean path.
    air_free_mean_path_ = AMC::ClassMeteorologicalData::air_free_mean_path_;

    // Air dynamic viscosity.
    air_dynamic_viscosity_ = AMC::ClassMeteorologicalData::air_dynamic_viscosity_;

    // Gas quadratic mean velocity and diffusion coefficient.
    for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
      {
        const int j = NPF::ClassSpecies::gas_index_[i];
        gas_quadratic_mean_velocity_[i] = AMC::ClassMeteorologicalData::gas_quadratic_mean_velocity_[j];
        gas_diffusivity_[i] = AMC::ClassMeteorologicalData::gas_diffusivity_[j];
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "pressure = " << pressure_ << endl;
    CBUG << "temperature = " << temperature_ << endl;
    CBUG << "relative_humidity = " << relative_humidity_ << endl;
    CBUG << "air_free_mean_path = " << air_free_mean_path_ << endl;
    CBUG << "air_dynamic_viscosity = " << air_dynamic_viscosity_ << endl;
    CBUG << "gas_quadratic_mean_velocity = " << gas_quadratic_mean_velocity_ << endl;
    CBUG << "gas_diffusivity = " << gas_diffusivity_ << endl;
#endif
  }


  // Clear.
  void ClassMeteoData::Clear()
  {
    pressure_ = real(AMC_PRESSURE_REFERENCE);
    temperature_ = real(AMC_TEMPERATURE_REFERENCE);
    air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);
    air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);

    gas_diffusivity_.clear();
    gas_quadratic_mean_velocity_.clear();
  }
}

#define NPF_FILE_CLASS_METEO_DATA_CXX
#endif
