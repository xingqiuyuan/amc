// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_CONCENTRATION_CXX

#include "ClassConcentration.hxx"

namespace NPF
{
  // Compute total mass.
  void ClassConcentration::compute_mass_total()
  {
    mass_total_.assign(NPF::ClassSpecies::Nspecies_, real(0));

    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];
        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          mass_total_[k] += NPF::ClassConcentration::mass_[j][k];
      }
  }


  // Normalize concentration in order to get strict mass conservation.
  void ClassConcentration::normalize_mass()
  {
    vector1r ratio(NPF::ClassSpecies::Nspecies_, real(0));

    // At beginning, ratio is the current mass total.
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];
        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          ratio[k] += NPF::ClassConcentration::mass_[j][k];
      }

    // Then make ratio with older total mass.
    for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
      ratio[k] = ratio[k] > real(0) ? mass_total_[k] / ratio[k] : real(1);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "ratio = " << ratio << endl;
#endif

    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];
        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          NPF::ClassConcentration::mass_[j][k] *= ratio[k];
      }
  }


#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  // Debug methods.
  string ClassConcentration::display_number()
  {
    vector1r number(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      number[i] = number_[NPF::ClassParticle::index_active_[i]];

    return to_str(number);
  }

  string ClassConcentration::display_mass()
  {
    vector2r mass(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      mass[i] = mass_[NPF::ClassParticle::index_active_[i]];

    return to_str(mass);
  }

  void ClassConcentration::test_clip()
  {
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];

        bool clip = number_[j] < real(0);

        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          clip = clip || (mass_[j][k] < real(0));

        if (clip)
          {
            CBUG << "number = " << number_ << endl;
            CBUG << "mass = " << mass_ << endl;
            throw AMC::Error("Negative concentration for particle " + to_str(j) + " at index " + to_str(i) + ".");
          }
      }
  }


  void ClassConcentration::test_mass_conservation()
  {
    vector1r mass_total(NPF::ClassSpecies::Nspecies_, real(0));

    // At beginning, ratio is the current mass total.
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];
        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          mass_total[k] += NPF::ClassConcentration::mass_[j][k];
      }

    // Then make ratio with older total mass.
    for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
      if (mass_total_[k] > real(0))
        {
          const real mass_delta_relative = abs(mass_total[k] / mass_total_[k] - real(1));
          if (mass_delta_relative > NPF_CONCENTRATION_MASS_CONSERVATION_THRESHOLD)
            {
              CBUG << "mass_total_old = " << mass_total_[k] << endl;
              CBUG << "mass_total_new = " << mass_total[k] << endl;
              CBUG << "mass_delta_relative = " << mass_delta_relative << endl;
              CBUG << "threshold = " << NPF_CONCENTRATION_MASS_CONSERVATION_THRESHOLD << endl;
              throw AMC::Error("For species " + to_str(k) + " mass seems not conserved.");
            }
        }
  }
#endif


  // Init.
  void ClassConcentration::Init()
  {
    InitStep();
  }


  // Init step.
  void ClassConcentration::InitStep()
  {
    gas_.assign(NPF::ClassSpecies::Nspecies_, real(0));
    number_.assign(NPF::ClassParticle::Nparticle_, real(0));
    mass_.assign(NPF::ClassParticle::Nparticle_,
                 vector1r(NPF::ClassSpecies::Nspecies_, real(0)));
    mass_total_.assign(NPF::ClassSpecies::Nspecies_, real(0));
  }


  // Clear.
  void ClassConcentration::Clear()
  {
    gas_.clear();
    number_.clear();
    mass_.clear();
    mass_total_.clear();
  }


  // Get methods.
  void ClassConcentration::GetGas(vector<real> &gas)
  {
    gas = gas_;
  }

  void ClassConcentration::GetNumber(vector<real> &number)
  {
    number.assign(NPF::ClassParticle::Nparticle_active_, real(0));

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      number[h] = number_[NPF::ClassParticle::index_active_[h]];
  }

  void ClassConcentration::GetMass(vector<vector<real> > &mass)
  {
    mass.assign(NPF::ClassParticle::Nparticle_active_,
                vector1r(NPF::ClassSpecies::Nspecies_, real(0)));

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      mass[h] = mass_[NPF::ClassParticle::index_active_[h]];
  }

  void ClassConcentration::GetMassTotal(vector<real> &mass_total)
  {
    mass_total = mass_total_;
  }


  // Compute particle size (dry mass).
  void ClassConcentration::ComputeSize()
  {
    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        NPF::ClassParticle::size_[i] = real(0);

        for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
          NPF::ClassParticle::size_[i] += mass_[i][j];

        NPF::ClassParticle::size_[i] /= number_[i];
      }
  }


  // Compute concentrations on size discretization.
  void ClassConcentration::MapOnSizeDiscretization(const vector<real> &diameter_bound,
                                                   vector<real> &number, vector<vector<real> > &mass)
  {
    const int Nbound = int(diameter_bound.size());
    const int Nsection = Nbound - 1;

    vector1r mass_bound(Nbound);
    for (int i = 0; i < Nbound; ++i)
      mass_bound[i] = NPF::ClassDiscretizationSize::density_fixed_ * AMC_PI6
        * diameter_bound[i] * diameter_bound[i] * diameter_bound[i];

    if (number.empty()) number.assign(Nsection, real(0));
    if (mass.empty()) mass.assign(Nsection, vector1r(NPF::ClassSpecies::Nspecies_, real(0)));

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];
        const int j = AMC::search_index(mass_bound, size_[i]);

        // Ignore particles above given discretization, managed by ParticleCollect.
        if (j < Nsection)
          {
            number[j] += number_[i];
            for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
              mass[j][k] += mass_[i][k];
          }
      }
  }
}

#define NPF_FILE_CLASS_CONCENTRATION_CXX
#endif
