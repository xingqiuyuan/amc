// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_PARTICLE_DATA_CXX

#include "ClassParticleData.hxx"

namespace NPF
{
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  // Debug methods.
  void ClassParticleData::test_clip_data()
  {
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];

        bool clip = mass_[j] < real(0);
        clip = clip || (diameter_[j] < real(0));
        clip = clip || (density_[j] < real(0));

        if (clip)
          {
            CBUG << "mass = " << mass_ << endl;
            CBUG << "diameter = " << diameter_ << endl;
            CBUG << "density = " << density_ << endl;
            throw AMC::Error("Negative data for particle " + to_str(j) + " at index " + to_str(i) + ".");
          }
      }
  }


  void ClassParticleData::test_clip_lwc()
  {
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      {
        const int j = NPF::ClassParticle::index_active_[i];

        if (liquid_water_content_[j] < real(0))
          {
            CBUG << "liquid_water_content = " << liquid_water_content_ << endl;
            throw AMC::Error("Negative liquid water content for particle " + to_str(j) + " at index " + to_str(i) + ".");
          }
      }
  }
#endif


  // Free one particle.
  inline void ClassParticleData::free_particle(const int h, const bool evaporation)
  {
    // Index of particle.
    const int i = NPF::ClassParticle::index_active_[h];

    // Nullify concentrations for next time.
    NPF::ClassConcentration::number_[i] = real(0);

    for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
      {
        // Give back mass to gas phase in order to conserve mass, if it is evaporation.
        if (evaporation)
          NPF::ClassConcentration::gas_[j] += NPF::ClassConcentration::mass_[i][j];

        NPF::ClassConcentration::mass_[i][j] = real(0);
      }

    mass_[i] = real(0);
    density_[i] = real(0);
    diameter_[i] = real(0);
    liquid_water_content_[i] = real(0);

    NPF::ClassParticle::free_particle(h);
  }


  // Merge particle with another one i => j.
  inline void ClassParticleData::merge_particle(const int i, const int j)
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "Merge particle " << i << " into particle " << j << endl;
#endif

    real f = NPF::ClassConcentration::number_[j];

    NPF::ClassConcentration::number_[j] += NPF::ClassConcentration::number_[i];

    for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
      NPF::ClassConcentration::mass_[j][k] += NPF::ClassConcentration::mass_[i][k];

    liquid_water_content_[j] += liquid_water_content_[i];

    f /= NPF::ClassConcentration::number_[j];

    NPF::ClassParticle::size_[j] = (real(1) - f) * NPF::ClassParticle::size_[i] + f * NPF::ClassParticle::size_[j];
  }


  // Initialize new particle with another one i => j.
  inline void ClassParticleData::init_particle(const int i, const int j)
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "Init particle " << j << " with particle " << i << endl;
#endif

    NPF::ClassConcentration::number_[j] = NPF::ClassConcentration::number_[i];

    for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
      NPF::ClassConcentration::mass_[j][k] = NPF::ClassConcentration::mass_[i][k];

    liquid_water_content_[j] = liquid_water_content_[i];

    NPF::ClassParticle::size_[j] = NPF::ClassParticle::size_[i];
  }


  // Compute liquid water content.
  void ClassParticleData::compute_liquid_water_content(const int i)
  {
    // Molar solute.
    real molar_solute(real(0));
    for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
      molar_solute += NPF::ClassConcentration::mass_[i][j] / NPF::ClassSpecies::molar_mass_[j];

    // Liquid water content, raw approximation.
    NPF::ClassParticleData::liquid_water_content_[i] = molar_solute * NPF::ClassMeteoData::relative_humidity_
      / (real(1) - NPF::ClassMeteoData::relative_humidity_) * AMC_WATER_MOLAR_MASS;    
  }


  // Init.
  void ClassParticleData::init()
  {
    mass_.assign(NPF::ClassParticle::Nparticle_, real(0));
    density_.assign(NPF::ClassParticle::Nparticle_, real(0));
    diameter_.assign(NPF::ClassParticle::Nparticle_, real(0));
    liquid_water_content_.assign(NPF::ClassParticle::Nparticle_, real(0));
    concentration_gas_equilibrium_.assign(NPF::ClassParticle::Nparticle_,
                                          vector1r(NPF::ClassSpecies::Nspecies_, real(0)));
  }


  // Clear.
  void ClassParticleData::clear()
  {
    mass_.clear();
    density_.clear();
    diameter_.clear();
    liquid_water_content_.clear();
    concentration_gas_equilibrium_.clear();
  }


  // Init.
  void ClassParticleData::Init(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info() << Reset() << "Init Particle Data Base Model." << endl;
#endif

    // Clear everything.
    ClassParticleData::Clear();

    ClassSpecies::Init(ops);
    ClassDiscretizationSize::Init(ops);
    ClassParticle::Init(ops);
    ClassMeteoData::Init();
    ClassConcentration::Init();
    ClassParticleData::init();
  }


  // Clear.
  void ClassParticleData::Clear()
  {
    ClassParticleData::clear();
    ClassConcentration::Clear();
    ClassMeteoData::Clear();
    ClassParticle::Clear();
    ClassDiscretizationSize::Clear();
    ClassSpecies::Clear();
  }


  void ClassParticleData::GetDiameter(vector<real> &diameter)
  {
    diameter.resize(NPF::ClassParticle::Nparticle_active_);

    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      diameter[i] = diameter_[NPF::ClassParticle::index_active_[i]];
  }


  void ClassParticleData::GetDensity(vector<real> &density)
  {
    density.resize(NPF::ClassParticle::Nparticle_active_);

    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      density[i] = density_[NPF::ClassParticle::index_active_[i]];
  }


  void ClassParticleData::GetLiquidWaterContent(vector<real> &liquid_water_content)
  {
    liquid_water_content.resize(NPF::ClassParticle::Nparticle_active_);

    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      liquid_water_content[i] = liquid_water_content_[NPF::ClassParticle::index_active_[i]];
  }


  // Compute diameter and mass.
  void ClassParticleData::ComputeDiameterMass()
  {
    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        real volume(real(0));
        mass_[i] = real(0);

        for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
          {
            mass_[i] += NPF::ClassConcentration::mass_[i][j];
            volume += NPF::ClassConcentration::mass_[i][j] / NPF::ClassSpecies::density_[j];
          }

        mass_[i] += liquid_water_content_[i];
        volume += liquid_water_content_[i] / AMC_WATER_DENSITY;

        density_[i] = mass_[i] / volume;

        const real concentration_number_inv = real(1) / NPF::ClassConcentration::number_[i];
        mass_[i] *= concentration_number_inv;
        diameter_[i] = pow(volume * AMC_INV_PI6 * concentration_number_inv, AMC_FRAC3);
      }
  }


  // Remove particles.
  void ClassParticleData::RemoveParticle()
  {
    bool particle_freed(false);

    // Remove particles below threshold level. Nparticle_active_ does not change during this loop.
    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        if (NPF::ClassConcentration::number_[i] < real(NPF_CONCENTRATION_NUMBER_MINIMUM))
          {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
            CBUG << "free particle " << i << " at index " << h << endl;
            CBUG << "number = " << NPF::ClassConcentration::number_[i] << endl;
#endif
            particle_freed = true;
            NPF::ClassParticleData::free_particle(h, true);
          }
      }

    // Remove free particles from index. This changes Nparticle_active_.
    if (particle_freed)
      NPF::ClassParticle::remove_particle();
  }


  // Collect particle beyond size discretization.
  bool ClassParticleData::CollectParticle(real &number, vector<real> &mass)
  {
    bool collected_particle(false);

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        if (NPF::ClassParticle::size_[i] >= NPF::ClassDiscretizationSize::mass_bound_.back())
          {
            number += NPF::ClassConcentration::number_[i];
            for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
              mass[NPF::ClassSpecies::species_index_[j]] += NPF::ClassConcentration::mass_[i][j];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
            CBUG << "collect particle " << i << " at position " << h << endl;
            CBUG << "\tsize = " << NPF::ClassParticle::size_[i] << endl;
            CBUG << "\tthreshold = " << NPF::ClassDiscretizationSize::mass_bound_.back() << endl;
#endif

            // Free this particle. This does not change Nparticle_active_.
            NPF::ClassParticleData::free_particle(h);

            collected_particle = true;
          }
      }

    // Remove free particles from index. This changes Nparticle_active_.
    if (collected_particle)
      NPF::ClassParticle::remove_particle();

    return collected_particle;
  }


  // Manage particles.
  void ClassParticleData::ManageParticle()
  {
    const int Nparticle_active(NPF::ClassParticle::Nparticle_active_);

    bool managed_particle(false);

    for (int h = 0; h < Nparticle_active; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        // Particle section index.
        const int j = NPF::ClassParticle::particle_section_index_[i];

        // New particle section index.
        int k(j);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        if (NPF::ClassParticle::size_[i] >= NPF::ClassDiscretizationSize::mass_bound_.back())
          {
            CBUG << "i = " << i << endl;
            CBUG << "j = " << j << endl;
            CBUG << "size = " << NPF::ClassParticle::size_[i] << endl;
            CBUG << "threshold = " << NPF::ClassDiscretizationSize::mass_bound_.back() << endl;
            throw AMC::Error("Particle i is beyond discretization, shound not happend because of CollectParticle.");
          }
#endif

        // Search between 0 and j - 1 if below, between j + 1 up to end if above.
        if (NPF::ClassParticle::size_[i] < NPF::ClassDiscretizationSize::mass_bound_[j])
          k = AMC::search_index_descending(ClassDiscretizationSize::mass_bound_, ClassParticle::size_[i], j - 1);
        else if (NPF::ClassParticle::size_[i] >= NPF::ClassDiscretizationSize::mass_bound_[j + 1])
          k = AMC::search_index_ascending(ClassDiscretizationSize::mass_bound_, ClassParticle::size_[i], j + 1);

        // If particle changed of section, create one particle or merge
        // with existing one of this new section and free current particle.
        if (k != j)
          {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
            CBUG << "manage particle " << i << " at position " << h << " of section " << j << " found in section " << k << endl;
#endif

            bool new_particle(true);
            const int l = find_particle(k, NPF::ClassParticle::size_[i], new_particle);

            if (new_particle)
              init_particle(i, l);
            else
              merge_particle(i, l);

            // Then destroy current particle.
            free_particle(h);

            managed_particle = true;
          }
      }

    // Remove free particles from index. This changes Nparticle_active_.
    if (managed_particle)
      NPF::ClassParticle::remove_particle();
  }


#ifdef NPF_WITH_TEST
  // Test.
  void ClassParticleData::Test(int N, const bool pause,
                               const real nucleation_number_max,
                               const real coagulation_frequency,
                               const real condensation_flux_max)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << "<==================================================>" << Reset().Str() << endl;
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Test Particle Based model with " << "N = " << N
                         << ", nucleation_number_max = " << nucleation_number_max
                         << ", coagulation_frequency = " << coagulation_frequency
                         << ", condensation_flux_max = " << condensation_flux_max << endl;
#endif

#ifdef AMC_WITH_TIMER
    int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_npf_particle_data");
    AMC::AMCTimer<AMC::CPU>::Start();
#endif

    // Event count.
    int nucleation_event(0);
    int coagulation_event(0);
    int formation_event(0);

    // Collected particles.
    real formation_number(real(0));
    vector<real> formation_mass(NPF::ClassSpecies::Nspecies_, real(0));

    while (N-- > 0)
      {
        //
        // Nucleation.
        //

        const real nucleation_number = real(nucleation_number_max) * drand48();

        if (nucleation_number > real(NPF_CONCENTRATION_NUMBER_MINIMUM))
          {
            nucleation_event++;

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fblue() << Debug(0) << "<====== Nucleation ======>" << Reset().Str() << endl;
#endif

            // Create one particle in section 0 with random size.
            const real size = NPF::ClassDiscretizationSize::mass_bound_[0]
              + NPF::ClassDiscretizationSize::mass_width_[0] * real(drand48());

            // Create one particle in this section.
            bool new_particle(true);
            const int i = NPF::ClassParticle::find_particle(0, size, new_particle);

            NPF::ClassConcentration::number_[i] = nucleation_number * real(1e6);

            // Random composition.
            AMC::generate_random_vector(NPF::ClassSpecies::Nspecies_,
                                        NPF::ClassConcentration::mass_[i],
                                        size * nucleation_number);

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fblue() << Debug(0) << Reset() << "Create one particle in section 0:" << endl;
            *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "\tindex = " << i << endl;
            *AMCLogger::GetLog() << Fblue() << Debug(1) << Reset() << "\tnumber = "
                                 << NPF::ClassConcentration::number_[i] << endl;
            *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tsize = "
                                 << NPF::ClassParticle::size_[i] << endl;
            *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "\tmass = "
                                 << NPF::ClassConcentration::mass_[i] << endl;
            *AMCLogger::GetLog() << Fblue() << Debug(0) << "<====== Nucleation ======>" << Reset().Str() << endl;
#endif
          }

        //
        // Coagulation.
        //

        if (NPF::ClassParticle::Nparticle_active_ > 1 &&
            real(drand48()) * real(NPF::ClassParticle::Nparticle_)
            < real(NPF::ClassParticle::Nparticle_active_) * coagulation_frequency)
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fgreen() << Debug(0) << "<====== Coagulation ======>" << Reset().Str() << endl;
#endif
            coagulation_event++;

            const int h1 = int(drand48() * NPF::ClassParticle::Nparticle_active_);
            const int h2 = int(drand48() * h1);

            const int i1 = NPF::ClassParticle::index_active_[h1];
            const int i2 = NPF::ClassParticle::index_active_[h2];

            // Size of new particle.
            const real size3 = NPF::ClassParticle::size_[i1] + NPF::ClassParticle::size_[i2];

            // Search section index of new particle, necessarily greater than largest particle.
            const int j1 = NPF::ClassParticle::particle_section_index_[i1];
            const int j3 = AMC::search_index_ascending(NPF::ClassDiscretizationSize::mass_bound_, size3, j1);

            bool new_particle = drand48() < real(0.5);
            const int i3 = (NPF::ClassParticle::Nparticle_section_active_[j3] > 0)
              ? ClassParticle::find_particle(j3, size3, new_particle)
              : ClassParticle::find_particle(j1, size3, new_particle);

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fgreen() << Debug(0) << Reset() << "coagulate particles "
                                 << i1 << " and " << i2 << " into particle " << i3 << endl;
            *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "\tnew_particle = "
                                 << (new_particle ? "yes" : "no") << endl;
#endif

            if (new_particle)
              NPF::ClassConcentration::number_[i3] = real(0.5) *
                (NPF::ClassConcentration::number_[i1] + NPF::ClassConcentration::number_[i2]);

            NPF::ClassParticle::size_[i3] = size3;
            for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
              NPF::ClassConcentration::mass_[i3][j] = NPF::ClassConcentration::mass_[i1][j]
                + NPF::ClassConcentration::mass_[i2][j];

            if (i1 != i3) NPF::ClassConcentration::number_[i1] = real(0);
            if (i2 != i3) NPF::ClassConcentration::number_[i2] = real(0);

            // Eventually remove particle.
            ClassParticleData::RemoveParticle();

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fgreen() << Debug(0) << "<====== Coagulation ======>" << Reset().Str() << endl;
#endif
          }

        //
        // Condensation.
        //

        for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
          {
            const int i = NPF::ClassParticle::index_active_[h];
            for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
              NPF::ClassConcentration::mass_[i][j] *= real(1) + condensation_flux_max * real(drand48());
          }

        // Compute particle size.
        NPF::ClassConcentration::ComputeSize();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(0) << "<====== CollectParticle ======>" << Reset().Str() << endl;
#endif

        // Collect particles.
        if (NPF::ClassParticleData::CollectParticle(formation_number, formation_mass))
          formation_event++;

        // Eventually sort particles.
        if (NPF::ClassParticle::Nparticle_active_ > 1)
          NPF::ClassParticle::SortParticle();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fred() << Debug(0) << "<====== CollectParticle ======>" << Reset().Str() << endl;
#endif

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(0) << "<====== ManageParticle ======>" << Reset().Str() << endl;
#endif

        // Move particles through sections or merge them, if more than one section.
        if (NPF::ClassDiscretizationSize::Nsection_ > 1)
          ClassParticleData::ManageParticle();

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Debug(0) << "<====== ManageParticle ======>" << Reset().Str() << endl;
#endif

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Debug(0) << Reset() << "N = " << N << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(0) << Reset() << "Nparticle_active = "
                             << NPF::ClassParticle::Nparticle_active_ << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Nparticle_section_active = "
                             << NPF::ClassParticle::Nparticle_section_active_ << endl;
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "index_active = "
                             << NPF::ClassParticle::display_index_active() << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "index_active_reverse = "
                             << display_index_active_reverse() << endl;
        *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "section_particle_index = " << endl
                             << display_section_particle_index();
        *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "section_particle_index_local = "
                             << NPF::ClassParticle::section_particle_index_local_ << endl;
        NPF::ClassParticle::test_particle_section_ascending_order();
#endif
        *AMCLogger::GetLog() << Bmagenta() << Debug(0) << Reset() << "<================================================>" << endl;
#endif

        // Eventually wait for any keyboard key.
        if (pause) getchar();
      }

#ifdef AMC_WITH_TIMER
    AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#endif

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info() << "<==================================================>" << Reset().Str() << endl;
#ifdef AMC_WITH_TIMER
    *AMCLogger::GetLog() << Fyellow() << Info() << Reset() << "Performed 1 simulation in " << setprecision(12)
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
    *AMCLogger::GetLog() << Fblue() << Info() << Reset() << "nucleation_event = " << nucleation_event << endl;
    *AMCLogger::GetLog() << Fgreen() << Info() << Reset() << "coagulation_event = " << coagulation_event << endl;
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "formation event = " << formation_event << endl;
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "\tnumber = " << formation_number << endl;
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "\tmass = " << formation_mass << endl;
    *AMCLogger::GetLog() << Byellow() << Info() << "<==================================================>" << Reset().Str() << endl;
#endif
  }
#endif
}

#define NPF_FILE_CLASS_PARTICLE_DATA_CXX
#endif
