// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_PARTICLE_DATA_HXX

#ifdef NPF_WITH_TEST
#define NPF_PARTICLE_DATA_TEST_NUCLEATION_NUMBER_MAX 1000
#define NPF_PARTICLE_DATA_TEST_COAGULATION_FREQUENCY 1
#define NPF_PARTICLE_DATA_TEST_CONDENSATION_FLUX_MAX 0.1
#endif

namespace NPF
{
  /*!
   * \class ClassParticleData
   */
  class ClassParticleData : protected ClassMeteoData, protected ClassConcentration
  {
  public:

    typedef NPF::real real;
    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector1r vector1r;
    typedef typename NPF::vector2r vector2r;

  protected:

    /*!< Single mass of particles.*/
    static vector1r mass_;

    /*!< Diameter of particles.*/
    static vector1r diameter_;

    /*!< Density of particles.*/
    static vector1r density_;

    /*!< Liquid water content in µg.cm^{-3}.*/
    static vector1r liquid_water_content_;

    /*!< Gas equilibrium constant in µg.cm^{-3}.*/
    static vector2r concentration_gas_equilibrium_;

    /*!< Free one particle.*/
    static void free_particle(const int h, const bool evaporation = false);

    /*!< Merge particle with another one : i => j.*/
    static void merge_particle(const int i, const int j);

    /*!< Initialize new particle with another one : i => j.*/
    static void init_particle(const int i, const int j);

    /*!< Compute liquid water content.*/
    static void compute_liquid_water_content(const int i);

    /*!< Coagulation parameterization is my friend.*/
    friend void AMC::ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(vector2r &kernel) const;

    /*!< Condensation parameterization is my friend.*/
    template<class FC, class FW, class FD>
    friend void AMC::ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(vector2r &coefficient) const;

    /*!< Init.*/
    static void init();

    /*!< Clear.*/
    static void clear();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    /*!< Debug methods.*/
    static void test_clip_data();
    static void test_clip_lwc();
#endif

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear.*/
    static void Clear();

    /*!< Get methods.*/
    static void GetDiameter(vector<real> &diameter);
    static void GetDensity(vector<real> &density);
    static void GetLiquidWaterContent(vector<real> &liquid_water_content);

    /*!< Compute diameter and mass.*/
    static void ComputeDiameterMass();

    /*!< Remove particles.*/
    static void RemoveParticle();

    /*!< Collect particles.*/
    static bool CollectParticle(real &number, vector<real> &mass);

    /*!< Manage particles.*/
    static void ManageParticle();

#ifdef NPF_WITH_TEST
    /*!< Test.*/
    static void Test(int N = 10000, const bool pause = true,
                     const real nucleation_number_max = real(NPF_PARTICLE_DATA_TEST_NUCLEATION_NUMBER_MAX),
                     const real coagulation_frequency = real(NPF_PARTICLE_DATA_TEST_COAGULATION_FREQUENCY),
                     const real condensation_flux_max = real(NPF_PARTICLE_DATA_TEST_CONDENSATION_FLUX_MAX));
#endif
  };
}

#define NPF_FILE_CLASS_PARTICLE_DATA_HXX
#endif
