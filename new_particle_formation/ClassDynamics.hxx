// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_DYNAMICS_HXX

namespace NPF
{
  /*!
   * \class ClassDynamics
   */
  template<class C, class D>
  class ClassDynamics : protected ClassParticleData
  {
  public:

    typedef NPF::real real;

    typedef NPF::vector1i vector1i;
    typedef NPF::vector1r vector1r;
    typedef NPF::vector2r vector2r;

    typedef NPF::npf_parameterization_nucleation_type parameterization_nucleation_type;
    typedef C parameterization_coagulation_type; 
    typedef D parameterization_condensation_type; 

  protected:

    /*!< Nucleation parameterization.*/
    static parameterization_nucleation_type *parameterization_nucleation_;

    /*!< Coagulation kernel.*/
    static vector2r coagulation_kernel_;

    /*!< Condensation coefficient.*/
    static vector2r condensation_coefficient_;

    /*!< Nucleation diameters and rates.*/
    static bool nucleation_event_;
    static real nucleation_diameter_;
    static real nucleation_rate_number_;
    static vector1r nucleation_rate_mass_;

    /*!< Number of coagulation couples.*/
    static int Ncouple_;

#ifdef NPF_WITH_COAGULATION_TABLE
    /*!< Tabulated coagulation kernel.*/
    static bool is_coagulation_kernel_tabulated_;
    static int Ntable_;
    static real table_diameter_min_inv_;
    static real table_diameter_delta_log_inv_;
    static vector1r table_diameter_;
    static vector2r table_coagulation_kernel_;
#endif

    /*!< Coagulation parameterization.*/
    static parameterization_coagulation_type *parameterization_coagulation_;

    /*!< Condensation parameterization.*/
    static parameterization_condensation_type *parameterization_condensation_;

    /*!< Compute coagulation kernel.*/
    static void compute_coagulation_kernel();

#ifdef NPF_WITH_COAGULATION_TABLE
    /*!< Find position in table.*/
    static void find_table_position(const real &diameter, int &k1, int &l1, real &u1, real &v1);
#endif

    /*!< Limit ammonium condensation if exist.*/
    static void limit_ammonia_condensation();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    /*!< Debug methods.*/
    static string display_coagulation_kernel();
    static string display_condensation_coefficient();
#endif

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear static data.*/
    static void Clear();
#endif

    /*!< Get methods.*/
    static int GetNcouple();
    static parameterization_coagulation_type* GetParameterizationCoagulationPtr();
    static parameterization_condensation_type* GetParameterizationCondensationPtr();

    static parameterization_nucleation_type* GetParameterizationNucleationPtr();
    static void GetCoagulationKernel(vector<vector<real> > &coagulation_kernel);
    static void GetCondensationCoefficent(vector<vector<real> > &condensation_coefficient);
    static void GetNucleationRate(vector<real> &nucleation_rate);
    static real GetNucleationDiameter();

    /*!< Init one step.*/
    static void InitStep(parameterization_nucleation_type* parameterization_nucleation);

    /*!< Compute dynamic rate.*/
    static void ComputeCoagulationKernel();
    static void ComputeCondensationCoefficient();
    static void ComputeNucleationRate();
  };
}

#define NPF_FILE_CLASS_DYNAMICS_HXX
#endif
