// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_DISCRETIZATION_SIZE_CXX

#include "ClassDiscretizationSize.hxx"

namespace NPF
{
  // Init.
  void ClassDiscretizationSize::Init(Ops::Ops &ops)
  {
    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix(prefix_orig + "discretization.");

    // Diameter bounds.
    if (ops.Is<vector<real> >("diameter_bound"))
      {
        ops.Set("diameter_bound", "", diameter_bound_);
        Nbound_ = int(diameter_bound_.size());
        Nsection_ = Nbound_ - 1;
      }
    else
      {
        Nsection_ = ops.Get<int>("N", "", NPF_NEW_PARTICLE_FORMATION_NUMBER_SECTION_DEFAULT);
        Nbound_ = Nsection_ + 1;
        const real dmin = ops.Get<real>("dmin", "", NPF_NEW_PARTICLE_FORMATION_DIAMETER_MINIMUM_DEFAULT);
        const real dmax = ops.Get<real>("dmax", "", NPF_NEW_PARTICLE_FORMATION_DIAMETER_MAXIMUM_DEFAULT);

        diameter_bound_.resize(Nbound_);
        const real q = pow(dmax / dmin, real(1) / Nsection_);
        diameter_bound_[0] = dmin;
        for (int i = 1; i < Nbound_; ++i)
          diameter_bound_[i] = q * diameter_bound_[i - 1];
      }

    // Diameter from nanometer to micrometer.
    for (int i = 0; i < Nbound_; i++)
      diameter_bound_[i] *= real(1.e-3);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "Nsection = " << Nsection_ << endl;
    *AMCLogger::GetLog() << Fblue() << Debug(2) << Reset() << "diameter_bound = " << diameter_bound_ << endl;
#endif

    // Fixed particle density.
    density_fixed_ = ops.Get<real>("discretization.density_fixed", "", NPF_NEW_PARTICLE_FORMATION_PARTICLE_DENSITY_DEFAULT)
      * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "density_fixed = " << density_fixed_ << endl;
#endif

    mass_bound_.resize(Nbound_);
    for (int i = 0; i < Nbound_; ++i)
      mass_bound_[i] = density_fixed_ * AMC_PI6 * diameter_bound_[i] * diameter_bound_[i] * diameter_bound_[i];

    mass_width_.resize(Nsection_);
    for (int i = 0; i < Nsection_; ++i)
      mass_width_[i] = mass_bound_[i + 1] - mass_bound_[i];

    ops.SetPrefix(prefix_orig);
  }


  // Get methods.
  int ClassDiscretizationSize::GetNsection()
  {
    return Nsection_;
  }


  int ClassDiscretizationSize::GetNbound()
  {
    return Nbound_;
  }


  void ClassDiscretizationSize::GetDiameterBound(vector<real> &diameter_bound)
  {
    diameter_bound = diameter_bound_;
  }


  real ClassDiscretizationSize::GetDensityFixed()
  {
    return density_fixed_;
  }


  // Clear.
  void ClassDiscretizationSize::Clear()
  {
    density_fixed_ = NPF_NEW_PARTICLE_FORMATION_PARTICLE_DENSITY_DEFAULT * AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3;

    Nsection_ = 0;
    Nbound_ = 0;

    mass_bound_.clear();
    diameter_bound_.clear();
  }
}

#define NPF_FILE_CLASS_DISCRETIZATION_SIZE_CXX
#endif
