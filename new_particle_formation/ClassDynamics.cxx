// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_DYNAMICS_CXX

#include "ClassDynamics.hxx"

namespace NPF
{
  // Compute coagulation kernel.
  template<class C, class D>
  inline void ClassDynamics<C, D>::compute_coagulation_kernel()
  {
#ifdef NPF_WITH_COAGULATION_TABLE
    if (is_coagulation_kernel_tabulated_)
      for (int i1 = 0; i1 < NPF::ClassParticle::Nparticle_active_; ++i1)
        for (int i2 = 0; i2 <= i1; ++i2)
          {
            const int j1 = NPF::ClassParticle::index_active_[i1];
            const int j2 = NPF::ClassParticle::index_active_[i2];

            int k1, k2, l1, l2;
            real u1, u2, v1, v2;
            find_table_position(NPF::ClassParticleData::diameter_[j1], k1, l1, u1, v1);
            find_table_position(NPF::ClassParticleData::diameter_[j2], k2, l2, u2, v2);

            // kernel in m^3.s^{-1}.
            coagulation_kernel_[i1][i2] = table_coagulation_kernel_[k1][k2] * v1 * v2
              + table_coagulation_kernel_[k1][l2] * v1 * u2
              + table_coagulation_kernel_[l1][k2] * u1 * v2
              + table_coagulation_kernel_[l1][l2] * u1 * u2;
          }
    else
#endif
      parameterization_coagulation_->ComputeKernel(coagulation_kernel_);
  }


#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  // Debug methods.
  template<class C, class D>
  string ClassDynamics<C, D>::display_coagulation_kernel()
  {
    vector2r coagulation_kernel(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      coagulation_kernel[i] = coagulation_kernel_[i];
    return to_str(coagulation_kernel);
  }


  template<class C, class D>
  string ClassDynamics<C, D>::display_condensation_coefficient()
  {
    vector2r condensation_coefficient(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      condensation_coefficient[i] = condensation_coefficient_[i];
    return to_str(condensation_coefficient);
  }
#endif

#ifdef NPF_WITH_COAGULATION_TABLE
  // Find index in table.
  template<class C, class D>
  inline void ClassDynamics<C, D>::find_table_position(const real &diameter, int &k, int &l, real &u, real &v)
  {
    u = log(diameter * table_diameter_min_inv_) * table_diameter_delta_log_inv_;
    k = int(u + real(1.e-6));

    // Keep it inside.
    k = k < 0  ? 0 : k;
    k = k < Ntable_ ? k : Ntable_;

    u -= real(k);
    u = u < 0 ? 0 : u;
    u = u > 1 ? 1 : u;

    l = k + 1;
    v = real(1) - u;
  }
#endif

  // Limit ammonium condensation if exist.
  template<class C, class D>
  inline void ClassDynamics<C, D>::limit_ammonia_condensation()
  {
    const real &molar_sulfate = NPF::ClassSpecies::molar_mass_[AMC_NUCLEATION_SULFATE_INDEX];
    const real &molar_ammonia = NPF::ClassSpecies::molar_mass_[NPF::ClassSpecies::index_ammonium_];

    const real &gas_sulfate = NPF::ClassConcentration::gas_[AMC_NUCLEATION_SULFATE_INDEX];
    const real &gas_ammonia = NPF::ClassConcentration::gas_[NPF::ClassSpecies::index_ammonium_];

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        const vector1r &mass = NPF::ClassConcentration::mass_[i];
        const vector1r &condensation_coefficient = condensation_coefficient_[h];

        NPF::ClassParticleData::concentration_gas_equilibrium_[i][NPF::ClassSpecies::index_ammonium_] = real(0);

        if (mass[NPF::ClassSpecies::index_ammonium_] > real(0))
          {
            const real molar_ratio = real(0.5) * mass[AMC_NUCLEATION_SULFATE_INDEX] * molar_ammonia
              / (mass[NPF::ClassSpecies::index_ammonium_] * molar_sulfate);

            const real condensation_coefficient_ammonia =
              condensation_coefficient[NPF::ClassSpecies::index_ammonium_] / molar_ammonia;
            const real condensation_coefficient_sulfate =
              condensation_coefficient[AMC_NUCLEATION_SULFATE_INDEX] / molar_sulfate;

            if (condensation_coefficient_ammonia * gas_ammonia >
                real(0.5) * condensation_coefficient_sulfate * gas_sulfate * molar_ratio)
              NPF::ClassParticleData::concentration_gas_equilibrium_[i][NPF::ClassSpecies::index_ammonium_] =
                gas_ammonia - condensation_coefficient_sulfate / condensation_coefficient_ammonia * molar_ratio * gas_sulfate;
          }
      }
  }


  // Init.
  template<class C, class D>
  void ClassDynamics<C, D>::Init(Ops::Ops &ops)
  {
    string prefix_orig = ops.GetPrefix();

    // Coagulation.
    if (ops.Exists("coagulation"))
      {
        ops.SetPrefix(prefix_orig + "coagulation.");
        parameterization_coagulation_ = new parameterization_coagulation_type(ops);
        Ncouple_ = (NPF::ClassParticle::Nparticle_ + 1) * NPF::ClassParticle::Nparticle_ / 2;

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fcyan() << Info(1) << Reset() << "Init coagulation with parameterization \""
                             << parameterization_coagulation_->GetName() << "\", Ncouple = " << Ncouple_ << endl;
#endif

        coagulation_kernel_.resize(NPF::ClassParticle::Nparticle_);
        for (int i = 0; i < NPF::ClassParticle::Nparticle_; ++i)
          coagulation_kernel_[i].assign(i + 1, real(0));

#ifdef NPF_WITH_COAGULATION_TABLE
        // Possibly, tabulate coagulation kernel.
        if ((is_coagulation_kernel_tabulated_ = ops.Exists("table")))
          {
#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Bmagenta() << Info(1) << "Using tabulated coagulation kernel." << Reset().Str() << endl;
#endif
            if (ops.Exists("table.diameter"))
              {
                ops.Set("table.diameter", "", table_diameter_);
                Ntable_ = int(table_diameter_.size()) - 1;
              }
            else
              {
                Ntable_ = ops.Get<int>("table.N");
                const real dmin = ops.Get<real>("table.dmin");
                const real dmax = ops.Get<real>("table.dmax");

#ifdef AMC_WITH_LOGGER
                *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "dmin = " << dmin << endl;
                *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "dmax = " << dmax << endl;
#endif

                table_diameter_.resize(Ntable_ + 1);
                const real q = pow(dmax / dmin, real(1) / real(Ntable_));
                table_diameter_[0] = dmin;
                for (int i = 1; i <= Ntable_; ++i)
                  table_diameter_[i] = q * table_diameter_[i - 1];
              }

            // Turn into µm.
            for (int i = 0; i <= Ntable_; ++i)
              table_diameter_[i] *= real(1.e-3);

            table_diameter_min_inv_ = real(1) / table_diameter_[0];
            table_diameter_delta_log_inv_ = real(1) / log(table_diameter_[1] / table_diameter_[0]);

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "Ntable_ = " << Ntable_ << endl;
            *AMCLogger::GetLog() << Fmagenta() << Debug(1) << Reset() << "table_diameter = " << table_diameter_ << endl;
            *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "table_diameter_min_inv = "
                                 << table_diameter_min_inv_ << endl;
            *AMCLogger::GetLog() << Fmagenta() << Debug(3) << Reset() << "table_diameter_delta_log_inv = "
                                 << table_diameter_delta_log_inv_ << endl;
#endif

            table_coagulation_kernel_.assign(Ntable_ + 1, vector1r(Ntable_ + 1, real(0)));
          }
#endif

        ops.SetPrefix(prefix_orig);
      }

    // Condensation.
    if (ops.Exists("condensation"))
      {
        ops.SetPrefix(prefix_orig + "condensation.");
        parameterization_condensation_ = new parameterization_condensation_type(ops);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "Init condensation with parameterization \""
                             << parameterization_condensation_->GetName() << "\"" << endl;
#endif

        condensation_coefficient_.assign(NPF::ClassParticle::Nparticle_, vector1r(NPF::ClassSpecies::Nspecies_, real(0)));

        ops.SetPrefix(prefix_orig);
      }

    InitStep(NULL);
  }


  // Clear.
  template<class C, class D>
  void ClassDynamics<C, D>::Clear()
  {
    if (parameterization_coagulation_ != NULL)
      {
        delete parameterization_coagulation_;
        parameterization_coagulation_ = NULL;
        Ncouple_ = 0;
        coagulation_kernel_.clear();

#ifdef NPF_WITH_COAGULATION_TABLE
        if (is_coagulation_kernel_tabulated_)
          {
            Ntable_ = 0;
            table_diameter_.clear();
            table_diameter_min_inv_ = real(0);
            table_diameter_delta_log_inv_ = real(0);
            table_coagulation_kernel_.clear();
          }
#endif
      }

    if (parameterization_condensation_ != NULL)
      {
        delete parameterization_condensation_;
        parameterization_condensation_ = NULL;
        condensation_coefficient_.clear();
      }

    parameterization_nucleation_ = NULL;
    nucleation_event_ = false;
    nucleation_rate_number_ = real(0);
    nucleation_diameter_ = real(0);
    nucleation_rate_mass_.clear();
  }


  // Get methods.
  template<class C, class D>
  int ClassDynamics<C, D>::GetNcouple()
  {
    return Ncouple_;
  }


  template<class C, class D>
  C* ClassDynamics<C, D>::GetParameterizationCoagulationPtr()
  {
    return parameterization_coagulation_;
  }


  template<class C, class D>
  D* ClassDynamics<C, D>::GetParameterizationCondensationPtr()
  {
    return parameterization_condensation_;
  }


  template<class C, class D>
   NPF::npf_parameterization_nucleation_type* ClassDynamics<C, D>::GetParameterizationNucleationPtr()
  {
    return parameterization_nucleation_;
  }


  template<class C, class D>
  void ClassDynamics<C, D>::GetCoagulationKernel(vector<vector<real> > &coagulation_kernel)
  {
    coagulation_kernel.resize(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      coagulation_kernel[i] = coagulation_kernel_[i];
  }


  template<class C, class D>
  void ClassDynamics<C, D>::GetCondensationCoefficent(vector<vector<real> > &condensation_coefficient)
  {
    condensation_coefficient.assign(condensation_coefficient_.begin(),
                                    condensation_coefficient_.begin() + NPF::ClassParticleData::Nparticle_active_);
  }


  template<class C, class D>
  void ClassDynamics<C, D>::GetNucleationRate(vector<real> &nucleation_rate)
  {
    nucleation_rate = nucleation_rate_mass_;
    nucleation_rate.push_back(nucleation_rate_number_);
  }


  template<class C, class D>
  real ClassDynamics<C, D>::GetNucleationDiameter()
  {
    return nucleation_diameter_;
  }


  // Set methods.
  template<class C, class D>
  void ClassDynamics<C, D>::InitStep(parameterization_nucleation_type* parameterization_nucleation)
  {
    // Nucleation.
    parameterization_nucleation_ = parameterization_nucleation;

    nucleation_event_ = false;
    nucleation_rate_number_ = real(0);
    nucleation_diameter_ = real(0);
    nucleation_rate_mass_.assign(NPF::ClassSpecies::Nspecies_ + 1, real(0));

    // Coagulation.
    if (parameterization_coagulation_ != NULL)
      {
        for (int i = 0; i < NPF::ClassParticle::Nparticle_; ++i)
          coagulation_kernel_[i].assign(i + 1, real(0));

#ifdef NPF_WITH_COAGULATION_TABLE
        if (is_coagulation_kernel_tabulated_)
          parameterization_coagulation_->KernelTable(table_diameter_, table_coagulation_kernel_);
#endif
      }

    // Condensation.
    if (parameterization_condensation_ != NULL)
      condensation_coefficient_.assign(NPF::ClassParticle::Nparticle_, vector1r(NPF::ClassSpecies::Nspecies_, real(0)));
  }


  // Compute dynamic rate.

  template<class C, class D>
  void ClassDynamics<C, D>::ComputeCoagulationKernel()
  {
    if (parameterization_coagulation_ != NULL)
      compute_coagulation_kernel();
  }


  template<class C, class D>
  void ClassDynamics<C, D>::ComputeCondensationCoefficient()
  {
    if (parameterization_condensation_ != NULL)
      parameterization_condensation_->ComputeCoefficient(condensation_coefficient_);
  }


  template<class C, class D>
  void ClassDynamics<C, D>::ComputeNucleationRate()
  {
    if (! (nucleation_event_ = parameterization_nucleation_->ComputeKernel(NPF::ClassConcentration::gas_,
                                                                          nucleation_diameter_,
                                                                          nucleation_rate_number_,
                                                                           nucleation_rate_mass_)))
      {
        nucleation_rate_number_ = real(0);
        nucleation_rate_mass_.assign(NPF::ClassSpecies::Nspecies_, real(0));
      }
  }
}

#define NPF_FILE_CLASS_DYNAMICS_CXX
#endif
