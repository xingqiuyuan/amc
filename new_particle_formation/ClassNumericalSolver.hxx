// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_NUMERICAL_SOLVER_HXX

#define NPF_NEW_PARTICLE_FORMATION_TIME_STEP_EPSILON  1.e-3
#define NPF_NEW_PARTICLE_FORMATION_TIME_STEP_ALFA  0.5
#define NPF_NEW_PARTICLE_FORMATION_TIME_STEP_INITIAL  10.
#define NPF_NEW_PARTICLE_FORMATION_TIME_STEP_MAX  100.

namespace NPF
{
  /*!
   * \class ClassNumericalSolver
   */
  template<class C, class D>
  class ClassNumericalSolver : protected ClassDynamics<C, D>
  {
  public:

    typedef NPF::real real;

    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector1r vector1r;

    typedef NPF::npf_parameterization_nucleation_type parameterization_nucleation_type;
    typedef C paramaterization_coagulation_type; 
    typedef D paramaterization_condensation_type; 

  protected:

    /*!< Time step alfa.*/
    static real time_step_alfa_;

    /*!< Time step epsilon.*/
    static real time_step_epsilon_;

    /*!< Initial time step.*/
    static real time_step_initial_;

    /*!< Maximal time step.*/
    static real time_step_max_;

    /*!< Number and mass total.*/
    static real number_total_;
    static real mass_total_;

    /*!< Number and mass variation.*/
    static real number_variation_;
    static real mass_variation_;

    /*!< Condensation coefficients.*/
    static vector1r b_, c_, d_;
    static vector2r a_;

    /*!< Coagulation coefficients.*/
    static vector1r B_;
    static vector2r A_;

    /*!< Compute time step.*/
    static void compute_time_step(real &time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    /*!< Debug methods.*/
    static string display_A(const int Nparticle_active);
    static string display_B(const int Nparticle_active);
    static string display_a();
#endif

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Init step.*/
    static void InitStep();

    /*!< Clear.*/
    static void Clear();
#endif

    /*!< Forward one time step in nucleation and coagulation.*/
    static void ForwardNucleationCoagulation(const real &time_step);

    /*!< Forward one time step in condensation.*/
    static void ForwardCondensation(const real &time_step);

    /*!< Set methods.*/
    static void SetTimeStepInitial(const real time_step_initial = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_INITIAL);
    static void SetTimeStepMax(const real time_step_max = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_MAX);
    static void SetTimeStepAlfa(const real time_step_alfa = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_ALFA);
    static void SetTimeStepEpsilon(const real time_step_epsilon = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_EPSILON);
  };
}

#define NPF_FILE_CLASS_NUMERICAL_SOLVER_HXX
#endif
