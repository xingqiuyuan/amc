// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_NUMERICAL_SOLVER_CXX

#include "ClassNumericalSolver.hxx"

namespace NPF
{
  // Compute time step.
  template<class C, class D>
  inline void ClassNumericalSolver<C, D>::compute_time_step(real &time_step)
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_step_old = " << time_step << endl;
    CBUG << "number_total = " << number_total_ << endl;
    CBUG << "number_variation = " << number_variation_ << endl;
    CBUG << "mass_total = " << mass_total_ << endl;
    CBUG << "mass_variation = " << mass_variation_ << endl;
#endif

    real time_step_variation(real(0));
    if (number_variation_ > real(0))
      time_step_variation += sqrt(number_total_) / sqrt(number_variation_);

    if (mass_variation_ > real(0))
      time_step_variation += sqrt(mass_total_) / sqrt(mass_variation_);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_step_variation = " << time_step_variation << endl;
#endif

    if (time_step_variation > real(0))
      time_step *= pow(time_step_epsilon_ * time_step_variation, time_step_alfa_);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_step_new = " << time_step << endl;
#endif

    // Set max time step with condensation.
    real time_step_max(time_step_max_);

    for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
      if (NPF::ClassConcentration::gas_[i] > NPF_CONCENTRATION_GAS_MINIMUM && b_[i] > real(0))
        if (time_step_max * b_[i] > real(1))
          time_step_max = real(1) / b_[i];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_step_max = " << time_step_max << endl;
#endif

    time_step = time_step < time_step_max ? time_step : time_step_max;

    // Set min time step with nucleation.
    real time_step_min(real(0));

    if (NPF::ClassDynamics<C, D>::nucleation_event_)
      if (time_step_min * NPF::ClassDynamics<C, D>::nucleation_rate_number_ < real(NPF_CONCENTRATION_NUMBER_MINIMUM))
        time_step_min = real(NPF_CONCENTRATION_NUMBER_MINIMUM) / NPF::ClassDynamics<C, D>::nucleation_rate_number_;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_step_min = " << time_step_min << endl;
#endif

    time_step = time_step > time_step_min ? time_step : time_step_min;

    // Nullify for next time.
    number_total_ = real(0);
    number_variation_ = real(0);
    mass_total_ = real(0);
    mass_variation_ = real(0);
  }


#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  // Debug methods.
  template<class C, class D>
  string ClassNumericalSolver<C, D>::display_a()
  {
    vector2r a(NPF::ClassParticle::Nparticle_active_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_active_; ++i)
      a[i] = a_[i];
    return to_str(a);
  }


  template<class C, class D>
  string ClassNumericalSolver<C, D>::display_A(const int Nparticle_active)
  {
    ostringstream sout;
    for (int i = 0; i < Nparticle_active; ++i)
      sout << A_[i] << endl;
    return sout.str();
  }


  template<class C, class D>
  string ClassNumericalSolver<C, D>::display_B(const int Nparticle_active)
  {
    vector1r B(B_.begin(), B_.begin() + Nparticle_active);
    return to_str(B);
  }
#endif


  // Init.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::Init(Ops::Ops &ops)
  {
    time_step_initial_ = ops.Get<real>("time_step.initial", "", NPF_NEW_PARTICLE_FORMATION_TIME_STEP_INITIAL);
    time_step_max_ = ops.Get<real>("time_step.max", "", NPF_NEW_PARTICLE_FORMATION_TIME_STEP_MAX);
    time_step_alfa_ = ops.Get<real>("time_step.alfa", "", NPF_NEW_PARTICLE_FORMATION_TIME_STEP_ALFA);
    time_step_epsilon_ = ops.Get<real>("time_step.epsilon", "", NPF_NEW_PARTICLE_FORMATION_TIME_STEP_EPSILON);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "Init numerical solver with "
                         << "time_step_initial = " << time_step_initial_ << " second, "
                         << "time_step_max = " << time_step_max_ << " second, "
                         << "time_step_alfa = " << time_step_alfa_ << " and "
                         << "time_step_epsilon = " << time_step_epsilon_ << endl;
#endif

    InitStep();
  }


  // Init step.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::InitStep()
  {
    number_total_ = real(0);
    mass_total_ = real(0);
    number_variation_ = real(0);
    mass_variation_ = real(0);

    d_.assign(NPF::ClassSpecies::Nspecies_, real(0));
    c_.assign(NPF::ClassSpecies::Nspecies_, real(0));
    b_.assign(NPF::ClassSpecies::Nspecies_, real(0));
    a_.assign(NPF::ClassParticle::Nparticle_, vector1r(NPF::ClassSpecies::Nspecies_, real(1)));

    B_.assign(NPF::ClassParticle::Nparticle_, real(0));
    A_.resize(NPF::ClassParticle::Nparticle_);
    for (int i = 0; i < NPF::ClassParticle::Nparticle_; ++i)
      A_[i].assign(i + 1, real(0));
  }


  // Clear.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::Clear()
  {
    time_step_initial_ = real(NPF_NEW_PARTICLE_FORMATION_TIME_STEP_INITIAL);
    time_step_max_ = real(NPF_NEW_PARTICLE_FORMATION_TIME_STEP_MAX);
    time_step_alfa_ = real(NPF_NEW_PARTICLE_FORMATION_TIME_STEP_ALFA);
    time_step_epsilon_ = real(NPF_NEW_PARTICLE_FORMATION_TIME_STEP_EPSILON);

    number_total_ = real(0);
    mass_total_ = real(0);
    number_variation_ = real(0);
    mass_variation_ = real(0);

    d_.clear();
    c_.clear();
    b_.clear();
    a_.clear();

    B_.clear();
    A_.clear();
  }


  // Forward one time step in nucleation and coagulation.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::ForwardNucleationCoagulation(const real &time_step)
  {
    // New particles created by nucleation and/or coagulation.
    const int Nparticle_active = NPF::ClassParticle::Nparticle_active_;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fgreen().Str() << "<======================= Coagulation =======================>" << Reset().Str() << endl;
#endif

    // Coagulation of existing particles.
    if (NPF::ClassDynamics<C, D>::parameterization_coagulation_ != NULL)
      {
        NPF::ClassDynamics<C, D>::compute_coagulation_kernel();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "coagulation_kernel = " << NPF::ClassDynamics<C, D>::display_coagulation_kernel() << endl;
#endif

        // Loss.
        B_.assign(Nparticle_active, real(0));

        for (int i1 = 0; i1 < Nparticle_active; ++i1)
          {
            for (int i2 = 0; i2 <= i1; ++i2)
              B_[i1] += NPF::ClassDynamics<C, D>::coagulation_kernel_[i1][i2]
                * NPF::ClassConcentration::number_[NPF::ClassParticle::index_active_[i2]];
            for (int i2 = i1 + 1; i2 < Nparticle_active; ++i2)
              B_[i1] += NPF::ClassDynamics<C, D>::coagulation_kernel_[i2][i1] 
                * NPF::ClassConcentration::number_[NPF::ClassParticleData::index_active_[i2]];
          }

        for (int i = 0; i < Nparticle_active; ++i)
          A_[i].assign(i + 1, real(0));

        for (int i1 = 0; i1 < Nparticle_active; ++i1)
          {
            const int j1 = NPF::ClassParticle::index_active_[i1];

            for (int i2 = 0; i2 <= i1; ++i2)
              {
                const real factor = i1 > i2 ? real(1) : real(0.5);
                const real &coagulation_kernel = NPF::ClassDynamics<C, D>::coagulation_kernel_[i1][i2];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                CBUG << "<=================================>" << endl;
                CBUG << "coagulation_kernel = " << coagulation_kernel << endl;
#endif

                const int j2 = NPF::ClassParticle::index_active_[i2];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                CBUG << "i1 = " << i1 << ", j1 = " << j1 << endl;
                CBUG << "i2 = " << i2 << ", j2 = " << j2 << endl;
#endif

                // Section index of largest particle.
                const int k1 = NPF::ClassParticle::particle_section_index_[j1];

                // Size of new particle.
                const real size3 = NPF::ClassParticle::size_[j1] + NPF::ClassParticle::size_[j2];

                // Search section index of new particle, necessarily greater than largest particle.
                const int k3 = AMC::search_index_ascending(NPF::ClassDiscretizationSize::mass_bound_, size3, k1);

                // Number rate.
                const real flux_number = factor * coagulation_kernel
                  * NPF::ClassConcentration::number_[j1]
                  * NPF::ClassConcentration::number_[j2];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                CBUG << "flux_number = " << flux_number << endl;
#endif

                // Whether to create one particle or merge it with already existing ones.
                // It depends on concentrations and time step.
                bool new_particle = flux_number * time_step > real(NPF_CONCENTRATION_NUMBER_MINIMUM);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                CBUG << "new_particle = " << (new_particle ? "yes" : "no") << endl;
#endif

                const int j3 = (NPF::ClassParticle::Nparticle_section_active_[k3] > 0 || new_particle)
                  ? ClassParticle::find_particle(k3, size3, new_particle)
                  : ClassParticle::find_particle(k1, size3, new_particle);

                if (new_particle)
                  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                    CBUG << "coagulate into new particle " << j3 << endl;
#endif
                    real loss1 = time_step * coagulation_kernel * NPF::ClassConcentration::number_[j2];
                    real loss2 = time_step * coagulation_kernel * NPF::ClassConcentration::number_[j1];
                    loss1 = loss1 / (real(1) + loss1);
                    loss2 = loss2 / (real(1) + loss2);

                    NPF::ClassConcentration::number_[j3] += factor * real(0.5) *
                      (NPF::ClassConcentration::number_[j1] * loss1 +
                       NPF::ClassConcentration::number_[j2] * loss2);

                    NPF::ClassConcentration::number_[j1] *= loss1;
                    if (i1 > i2) NPF::ClassConcentration::number_[j2] *= loss2;

                    for (int l = 0; l < NPF::ClassSpecies::Nspecies_; ++l)
                      {
                        NPF::ClassConcentration::mass_[j3][l] += factor *
                          (NPF::ClassConcentration::mass_[j1][l] * loss1 +
                           NPF::ClassConcentration::mass_[j2][l] * loss2);

                        NPF::ClassConcentration::mass_[j1][l] *= loss1;
                        if (i1 > i2) NPF::ClassConcentration::mass_[j2][l] *= loss2;
                      }

                    // Liquid water content.
                    NPF::ClassParticleData::liquid_water_content_[j3] += factor *
                      (NPF::ClassParticleData::liquid_water_content_[j1] * loss1 +
                       NPF::ClassParticleData::liquid_water_content_[j2] * loss2);

                    NPF::ClassParticleData::liquid_water_content_[j1] *= loss1;
                    if (i1 > i2) NPF::ClassParticleData::liquid_water_content_[j2] *= loss2;
                  }
                else
                  {
                    const int i3 = ClassParticleData::index_active_reverse_[j3];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
                    CBUG << "coagulate into old particle " << j3 << ", i3 = " << i3 << endl;
#endif
                    A_[i3][i1] += factor * coagulation_kernel * NPF::ClassConcentration::number_[j2];
                    A_[i3][i2] += factor * coagulation_kernel * NPF::ClassConcentration::number_[j1];
                  }
              }
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fgreen().Str() << "<======================= Coagulation =======================>" << Reset().Str() << endl;
#endif

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fblue().Str() << "<======================= Nucleation =======================>" << Reset().Str() << endl;
#endif

    // Call to the nucleation parameterization.
    if ((NPF::ClassDynamics<C, D>::nucleation_event_ = NPF::ClassDynamics<C, D>::
         parameterization_nucleation_->ComputeKernel(NPF::ClassConcentration::gas_,
                                                     NPF::ClassDynamics<C, D>::nucleation_diameter_,
                                                     NPF::ClassDynamics<C, D>::nucleation_rate_number_,
                                                     NPF::ClassDynamics<C, D>::nucleation_rate_mass_)))
      {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "nucleation_rate_number = " << NPF::ClassDynamics<C, D>::nucleation_rate_number_ << endl;
#endif

        // Size of nucleated particle.
        //const real size_nucleation = AMC_PI6 * NPF::ClassDiscretizationSize::density_fixed_
        //  * NPF::ClassDynamics<C, D>::nucleation_diameter_
        //  * NPF::ClassDynamics<C, D>::nucleation_diameter_
        //  * NPF::ClassDynamics<C, D>::nucleation_diameter_;
        //
        // const int i = AMC::search_index_ascending(NPF::ClassDiscretizationSize::diameter_bound_, diameter_nucleation_);

        real size_nucleation(real(0));
        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          size_nucleation += NPF::ClassDynamics<C, D>::nucleation_rate_mass_[k];
        size_nucleation /=  NPF::ClassDynamics<C, D>::nucleation_rate_number_;

        // Size section of nucleated particles, we do not use the diameter, should be the same ... in principle.
        const int i = AMC::search_index_ascending(NPF::ClassDiscretizationSize::mass_bound_, size_nucleation);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "size_nucleation = " << size_nucleation << endl;
        CBUG << "size_section = " << i << endl;
#endif

        // By default, try to create one new particle.
        bool new_particle(true);
        const int j = NPF::ClassParticle::find_particle(i, size_nucleation, new_particle);

        NPF::ClassConcentration::number_[j] += NPF::ClassDynamics<C, D>::nucleation_rate_number_ * time_step;

        for (int k = 0; k < NPF::ClassSpecies::Nspecies_; ++k)
          {
            const real nucleation_flux_mass = time_step * NPF::ClassDynamics<C, D>::nucleation_rate_mass_[k];

            NPF::ClassConcentration::mass_[j][k] += nucleation_flux_mass;
            NPF::ClassConcentration::gas_[k] -= nucleation_flux_mass;
            CLIP(NPF::ClassConcentration::gas_[k]);
          }

        NPF::ClassParticleData::liquid_water_content_[j] += time_step * NPF::ClassDynamics<C, D>::nucleation_rate_mass_.back();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "liquid_water_content = " << NPF::ClassParticleData::liquid_water_content_[j] << endl;
#endif
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fblue().Str() << "<======================= Nucleation =======================>" << Reset().Str() << endl;
    cout << Fmagenta().Str() << "<======================= Solve coagulation and nucleation =======================>"
         << Reset().Str() << endl;
#endif

    if (NPF::ClassDynamics<C, D>::parameterization_coagulation_ != NULL)
      {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "A = " << display_A(Nparticle_active) << endl;
        CBUG << "B = " << display_B(Nparticle_active) << endl;
#endif

        // Integrate existing particles, if any.
        for (int i1 = 0; i1 < Nparticle_active; ++i1)
          {
            const int j1 = NPF::ClassParticleData::index_active_[i1];

            // Number concentration.
            real B = real(1) / (real(1) + time_step * (B_[i1] - real(0.5) * A_[i1][i1]));

            real A(real(0));
            for (int i2 = 0; i2 < i1; ++i2)
              A += A_[i1][i2] * NPF::ClassConcentration::number_[NPF::ClassParticle::index_active_[i2]];

            const real number_old = NPF::ClassConcentration::number_[j1];
            NPF::ClassConcentration::number_[j1] = (number_old + time_step * real(0.5) * A) * B;

            number_total_ += number_old * number_old;
            const real number_variation_local = NPF::ClassConcentration::number_[j1] - number_old;
            number_variation_ += number_variation_local * number_variation_local;

            // Mass concentration.
            B = real(1) / (real(1) + time_step * (B_[i1] - A_[i1][i1]));

            for (int l = 0; l < NPF::ClassSpecies::Nspecies_; ++l)
              {
                A = real(0);
                for (int i2 = 0; i2 < i1; ++i2)
                  A += A_[i1][i2] * NPF::ClassConcentration::mass_[NPF::ClassParticle::index_active_[i2]][l];

                NPF::ClassConcentration::mass_[j1][l] = (NPF::ClassConcentration::mass_[j1][l] + time_step * A) * B;
              }

            // Liquid water content.
            A = real(0);
            for (int i2 = 0; i2 < i1; ++i2)
              A += A_[i1][i2] * NPF::ClassParticleData::liquid_water_content_[NPF::ClassParticle::index_active_[i2]];

            NPF::ClassParticleData::liquid_water_content_[j1] =
              (NPF::ClassParticleData::liquid_water_content_[j1] + time_step * A) * B;
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fmagenta().Str() << "<======================= Solve coagulation and nucleation =======================>"
         << Reset().Str() << endl;
#endif
  }


  // Forward one time step in condensation.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::ForwardCondensation(const real &time_step)
  {
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fred().Str() << "<======================= Condensation =======================>" << Reset().Str() << endl;
#endif

    NPF::ClassDynamics<C, D>::parameterization_condensation_->
      ComputeCoefficient(NPF::ClassDynamics<C, D>::condensation_coefficient_);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "condensation_coefficient = " << NPF::ClassDynamics<C, D>::display_condensation_coefficient() << endl;
#endif

    // If ammonium is present, it cannot condense more than (NH4)2_SO4.
    if (NPF::ClassSpecies::index_ammonium_ > 0)
      NPF::ClassDynamics<C, D>::limit_ammonia_condensation();

    b_.assign(NPF::ClassSpecies::Nspecies_, real(0));

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        // Growth factor.
        real growth_factor(real(0));
        for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
          growth_factor += NPF::ClassDynamics<C, D>::condensation_coefficient_[h][j]
            * (NPF::ClassConcentration::gas_[j] - NPF::ClassParticleData::concentration_gas_equilibrium_[i][j]);
        growth_factor = real(1) + time_step  * growth_factor / (real(6) * NPF::ClassParticleData::mass_[i]);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "growth_factor = " << growth_factor << endl;
#endif

        for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
          {
            // Estimate condensation coefficient at time t + 1/2.
            NPF::ClassDynamics<C, D>::condensation_coefficient_[h][j] *= growth_factor;

            a_[h][j] = NPF::ClassDynamics<C, D>::condensation_coefficient_[h][j]
              * NPF::ClassConcentration::number_[i];
            b_[j] += a_[h][j];
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "a = " << display_a() << endl;
    CBUG << "b = " << b_ << endl;
#endif

    for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
      c_[i] = time_step / (real(1) + time_step * b_[i]);

    d_.assign(NPF::ClassSpecies::Nspecies_, real(0));

    for (int h = 0; h < NPF::ClassParticle::Nparticle_active_; ++h)
      {
        const int i = NPF::ClassParticle::index_active_[h];

        for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
          {
            const real mass_transfer = a_[h][j] * c_[j] *
              (NPF::ClassConcentration::gas_[j] - NPF::ClassParticleData::concentration_gas_equilibrium_[i][j]);

            const real mass_old = NPF::ClassConcentration::mass_[i][j];

            NPF::ClassConcentration::mass_[i][j] += mass_transfer;

            mass_total_ += mass_old * mass_old;
            const real mass_variation_local = NPF::ClassConcentration::mass_[i][j] - mass_old;
            mass_variation_ += mass_variation_local * mass_variation_local;

            d_[j] += mass_transfer;
          }
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "d = " << d_ << endl;
#endif

    // Ensure mass conservation.
    for (int j = 0; j < NPF::ClassSpecies::Nspecies_; ++j)
      {
        NPF::ClassConcentration::gas_[j] -= d_[j];
        if (NPF::ClassConcentration::gas_[j] < NPF_CONCENTRATION_GAS_MINIMUM)
          NPF::ClassConcentration::gas_[j] = real(0);
      }

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << Fred().Str() << "<======================= Condensation =======================>" << Reset().Str() << endl;
#endif
  }


  // Set methods.
  template<class C, class D>
  void ClassNumericalSolver<C, D>::SetTimeStepMax(const real time_step_max)
  {
    time_step_max_ = time_step_max;
  }

  template<class C, class D>
  void ClassNumericalSolver<C, D>::SetTimeStepInitial(const real time_step_initial)
  {
    time_step_initial_ = time_step_initial;
  }

  template<class C, class D>
  void ClassNumericalSolver<C, D>::SetTimeStepAlfa(const real time_step_alfa)
  {
    time_step_alfa_ = time_step_alfa;
  }

  template<class C, class D>
  void ClassNumericalSolver<C, D>::SetTimeStepEpsilon(const real time_step_epsilon)
  {
    time_step_epsilon_ = time_step_epsilon;
  }
}

#define NPF_FILE_CLASS_NUMERICAL_SOLVER_CXX
#endif
