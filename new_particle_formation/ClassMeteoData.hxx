// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_METEO_DATA_HXX

namespace NPF
{
  /*!
   * \class ClassMeteoData
   */
  class ClassMeteoData : virtual protected ClassSpecies
  {
  public:

    typedef NPF::real real;

    typedef typename NPF::vector1r vector1r;

  protected:

    /*!< Temperature.*/
    static real temperature_;

    /*!< Relative humidity.*/
    static real relative_humidity_;

    /*!< Pressure.*/
    static real pressure_;

    /*!< Air dynamic viscosity.*/
    static real air_dynamic_viscosity_;

    /*!< Air free mean path.*/
    static real air_free_mean_path_;

    /*!< Gas quadratic mean velocity.*/
    static vector1r gas_quadratic_mean_velocity_;

    /*!< Gas diffusion coefficient.*/
    static vector1r gas_diffusivity_;

#ifdef AMC_WITH_COAGULATION
#ifdef AMC_WITH_WAALS_VISCOUS
    /*!< Coagulation parameterization is my friend.*/
    friend void AMC::ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(vector2r &kernel) const;

    friend void AMC::ClassParameterizationCoagulationBrownianWaalsViscous::KernelTable(const vector1r &diameter,
                                                                                       vector2r &kernel) const;
#endif
#endif

#ifdef AMC_WITH_CONDENSATION
    /*!< Condensation parameterization is my friend.*/
    template<class FC, class FW, class FD>
    friend void AMC::ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(vector2r &coefficient) const;
#endif

    /*!< Nucleation parameterizations are my friends.*/
#ifdef AMC_WITH_VEHKAMAKI
    friend bool AMC::ClassParameterizationNucleationVehkamaki::ComputeKernel(const vector1r &concentration_gas,
                                                                             real &diameter,
                                                                             real &rate_aer_number,
                                                                             vector1r &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_MERIKANTO
    friend bool AMC::ClassParameterizationNucleationMerikanto::ComputeKernel(const vector1r &concentration_gas,
                                                                             real &diameter,
                                                                             real &rate_aer_number,
                                                                             vector1r &rate_aer_mass) const;
#endif

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init();
#endif

    /*!< Init step.*/
    static void InitStep();

#ifndef SWIG
    /*!< Clear.*/
    static void Clear();
#endif
  };
}

#define NPF_FILE_CLASS_METEO_DATA_HXX
#endif
