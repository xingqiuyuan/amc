// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_SPECIES_CXX

#include "ClassSpecies.hxx"

namespace NPF
{
  // Init.
  void ClassSpecies::Init(Ops::Ops &ops)
  {
    // Species list.
    ops.Set("species", "", name_);
    Nspecies_ = int(name_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "Nspecies = " << Nspecies_ << endl;
#endif

    // Look for AMC index.
    for (int i = 0; i < Nspecies_; ++i)
      {
        int j(0);

        for (j = 0; j < AMC::ClassSpecies::Nspecies_; ++j)
          if (name_[i] == AMC::ClassSpecies::name_[j])
            break;

        if (j < AMC::ClassSpecies::Nspecies_)
          species_index_.push_back(j);
        else
          throw AMC::Error("Unable to find species \"" + name_[i] + "\" in AMC species list.");

        if (AMC::ClassSpecies::semivolatile_reverse_[j] < 0)
          throw AMC::Error("Species \"" + name_[i] + "\" is not semivolatile in AMC.");
        else
          gas_index_.push_back(AMC::ClassSpecies::semivolatile_reverse_[j]);

#ifdef AMC_WITH_LOGGER
        *AMCLogger::GetLog() << Bblue() << Debug(1) << Reset() << "\tname = " << name_[i]
                             << ", index in AMC : species = " << species_index_.back()
                             << ", gas = " << gas_index_.back() << endl;
#endif
      }

    // Water is last, but has no index.
    species_index_.push_back(-1);
    gas_index_.push_back(-1);

    // Find ammonium, if any.
    for (int i = 0; i < Nspecies_; ++i)
      if (AMC::ClassSpecies::name_[species_index_[i]] == "NH3")
        {
          index_ammonium_ = i;
          break;
        }

    if (index_ammonium_ == 0)
      throw AMC::Error("Index of ammonium in New Particle Formation model cannot be 0 which is reserved for sulfate.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bgreen() << Debug(1) << Reset() << "index_ammonium = " << index_ammonium_ << endl;
#endif

    // Set species properties.
    density_.assign(Nspecies_, real(0));
    molar_mass_.assign(Nspecies_, real(0));
    accomodation_coefficient_.assign(Nspecies_, real(1));

    for (int i = 0; i < Nspecies_; ++i)
      {
        const int j = species_index_[i];

        density_[i] = AMC::ClassSpecies::density_[j]; // µg/µm3
        molar_mass_[i] = AMC::ClassSpecies::molar_mass_[j];
        accomodation_coefficient_[i] = AMC::ClassSpecies::accomodation_coefficient_[j];
      }
  }


  // Get methods.
  int ClassSpecies::GetNspecies()
  {
    return Nspecies_;
  }


  void ClassSpecies::GetName(vector<string> &name)
  {
    name = name_;
  }


  void ClassSpecies::GetSpeciesIndex(vector<int> &species_index)
  {
    species_index = species_index_;
  }


  void ClassSpecies::GetGasIndex(vector<int> &gas_index)
  {
    gas_index = gas_index_;
  }


  // Clear.
  void ClassSpecies::Clear()
  {
    Nspecies_ = 0;

    name_.clear();
    gas_index_.clear();
    species_index_.clear();

    density_.clear();
    molar_mass_.clear();
    accomodation_coefficient_.clear();
  }
}

#define NPF_FILE_CLASS_SPECIES_CXX
#endif
