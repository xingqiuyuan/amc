// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_SPECIES_HXX

namespace NPF
{
  /*!
   * \class ClassSpecies
   */
  class ClassSpecies
  {
  public:

    typedef NPF::real real;

    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector1r vector1r;
    typedef typename NPF::vector1s vector1s;

  protected:

    /*!< Number of species in particle.*/
    static int Nspecies_;

    /*!< Index of ammonium in species list, negative if does not exist.*/
    static int index_ammonium_;

    /*!< Name.*/
    static vector1s name_;

    /*!< Species index in AMC.*/
    static vector1i species_index_;

    /*!< Gas index in AMC.*/
    static vector1i gas_index_;

    /*!< Molar mass.*/
    static vector1r molar_mass_;

    /*!< Accomodation coefficient.*/
    static vector1r accomodation_coefficient_;

    /*!< Species specific density.*/
    static vector1r density_;

    /*!< Coagulation parameterization is my friend.*/
    friend void AMC::ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(vector2r &kernel) const;

    /*!< Condensation parameterization is my friend.*/
    template<class FC, class FW, class FD>
    friend void AMC::ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(vector2r &coefficient) const;

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init(Ops::Ops &ops);
#endif

    /*!< Get methods.*/
    static int GetNspecies();
    static void GetName(vector<string> &name);
    static void GetSpeciesIndex(vector<int> &species_index);
    static void GetGasIndex(vector<int> &gas_index);

#ifndef SWIG
    /*!< Clear.*/
    static void Clear();
#endif
  };
}

#define NPF_FILE_CLASS_SPECIES_HXX
#endif
