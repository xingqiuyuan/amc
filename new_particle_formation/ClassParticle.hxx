// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_PARTICLE_HXX

namespace NPF
{
  /*!
   * \class ClassParticle
   */
  class ClassParticle : protected ClassDiscretizationSize
  {
  public:

    typedef NPF::real real;
    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector2i vector2i;
    typedef typename NPF::vector1r vector1r;

  protected:

    /*!< Total number of particles.*/
    static int Nparticle_;

    /*!< Number of particles per section.*/
    static vector1i Nparticle_section_;

    /*!< Particle section index.*/
    static vector1i particle_section_index_;

    /*!< Total number of active particles.*/
    static int Nparticle_active_;

    /*!< Index of particles per section, active on the left, free on the right.*/
    static vector2i section_particle_index_;

    /*!< Local index of particles per section.*/
    static vector1i section_particle_index_local_;

    /*!< Number of active particles per section.*/
    static vector1i Nparticle_section_active_;

    /*!< Index of active particles.*/
    static vector1i index_active_;

    /*!< Reverse index of active particles.*/
    static vector1i index_active_reverse_;

    /*!< Size of particles.*/
    static vector1r size_;

    /*!< Free one particle.*/
    static void free_particle(const int h);

    /*!< Find closest particle or new one in one given section.*/
    static int find_particle(const int i, const real &size, bool &new_particle);

    /*!< Remove particle index.*/
    static void remove_particle();

    /*!< Coagulation parameterization is my friend.*/
    friend void AMC::ClassParameterizationCoagulationBrownianWaalsViscous::ComputeKernel(vector2r &kernel) const;

    /*!< Condensation parameterization is my friend.*/
    template<class FC, class FW, class FD>
    friend void AMC::ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::ComputeCoefficient(vector2r &coefficient) const;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    /*!< Debug methods.*/
    static string display_size();
    static string display_section_size(const int i);
    static string display_index_active();
    static string display_index_active_reverse();
    static string display_section_particle_index(const int i);
    static string display_section_particle_index();
    static void test_particle_section_ascending_order();
    static void test_particle_ascending_order();
#endif

  public:

#ifndef SWIG
    /*!< Init static data.*/
    static void Init(Ops::Ops &ops);
#endif

    /*!< Init step.*/
    static void InitStep();

#ifndef SWIG
    /*!< Clear static data.*/
    static void Clear();
#endif

    /*!< Get methods.*/
    static int GetNparticle();
    static int GetNparticleSection(const int i);
    static int GetParticleSectionIndex(const int i);

    static int GetNparticleActive();
    static int GetNparticleSectionActive(const int i);
    static void GetSectionParticleIndex(const int i, vector<int> &section_particle_index);
    static int GetSectionParticleIndexLocal(const int i);
    static void GetIndexActive(vector<int> &index_active);
    static void GetSize(vector<real> &size);

    /*!< Sort particles.*/
    static void SortParticle();
  };
}

#define NPF_FILE_CLASS_PARTICLE_HXX
#endif
