// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_NEW_PARTICLE_FORMATION_HXX


namespace NPF
{
  /*!
   * \class ClassNewParticleFormation
   */
  template<class C, class D>
  class ClassNewParticleFormation : protected ClassNumericalSolver<C, D>
  {
  public:

    typedef NPF::real real;

    typedef typename NPF::vector1r vector1r;

    typedef NPF::npf_parameterization_nucleation_type parameterization_nucleation_type;
    typedef C parameterization_coagulation_type; 
    typedef D parameterization_condensation_type; 

  private:

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    static bool debug_pause_;
#endif

#ifdef NPF_WITH_TEST
    static std::mutex test_mutex_;
    static bool is_test_;
    static real test_time_;
    static real test_time_out_;
    static real test_time_output_;
    static real test_time_step_;
    static real test_number_;
    static vector<real> test_mass_;
    static void thread_function_run_(parameterization_nucleation_type *parameterization_nucleation,
                                     const vector<real> &initial_gas, real time_out);
    static void thread_function_gas_(const vector<vector<real> > &production_gas);
    static void thread_function_save_(const vector<real> &diameter_bound, const real &time_output_frequency,
                                      vector<vector<real> > &gas, vector<vector<real> > &number,
                                      vector<vector<vector<real> > > &mass);
#endif

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Init step.*/
    static void InitStep(parameterization_nucleation_type *parameterization_nucleation);

    /*!< Clear static data.*/
    static void Clear();

    /*!< Whether to pause or not when debugging.*/
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    static bool ToggleDebugPause();
#endif

#ifndef SWIG
    /*!< Collect particle beyond size discretization.*/
    static bool CollectParticle(real &number, vector<real> &mass);
#endif

#ifdef SWIG
    %apply double *OUTPUT {real &number};
#endif

    /*!< Run new particle formation model over [0, time_out].*/
    static bool Run(const real *concentration_gas, real time_out, real &number, vector<real> &mass);

#ifdef SWIG
    %clear double &number;
#endif

#ifdef NPF_WITH_TEST
    static void Test(parameterization_nucleation_type *parameterization_nucleation, const real &time_out,
                     const vector<real> &initial_gas, const vector<vector<real> > &production_gas,
                     const vector<real> &diameter_bound, const real &time_output_frequency,
                     vector<vector<real> > &gas, vector<vector<real> > &number, vector<vector<vector<real> > > &mass);
#endif
  };
}

#define NPF_FILE_CLASS_NEW_PARTICLE_FORMATION_HXX
#endif
