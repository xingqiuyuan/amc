// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_DISCRETIZATION_SIZE_HXX

#define NPF_NEW_PARTICLE_FORMATION_PARTICLE_DENSITY_DEFAULT 1.5 // Default particle density in g.cm^{-3}.
#define NPF_NEW_PARTICLE_FORMATION_NUMBER_SECTION_DEFAULT 10
#define NPF_NEW_PARTICLE_FORMATION_DIAMETER_MINIMUM_DEFAULT 0.5   // nm
#define NPF_NEW_PARTICLE_FORMATION_DIAMETER_MAXIMUM_DEFAULT 10.   // nm

namespace NPF
{
  /*!
   * \class ClassDiscretizationSize
   */
  class ClassDiscretizationSize
  {
  public:

    typedef NPF::real real;

    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector1r vector1r;

  protected:

    /*!< Number of sections.*/
    static int Nsection_;

    /*!< Number of bounds.*/
    static int Nbound_;

    /*!< Fixed particle density.*/
    static real density_fixed_;

    /*!< Section diameter bounds.*/
    static vector1r diameter_bound_;

    /*!< Section mass bounds.*/
    static vector1r mass_bound_;

    /*!< Section mass width.*/
    static vector1r mass_width_;

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init(Ops::Ops &ops);
#endif

    /*!< Get methods.*/
    static int GetNsection();
    static int GetNbound();
    static void GetDiameterBound(vector<real> &diameter_bound);
    static real GetDensityFixed();

#ifndef SWIG
    /*!< Clear.*/
    static void Clear();
#endif
  };
}

#define NPF_FILE_CLASS_DISCRETIZATION_SIZE_HXX
#endif
