// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_NEW_PARTICLE_FORMATION_CXX

#include "ClassNewParticleFormation.hxx"

namespace NPF
{
#ifdef NPF_WITH_TEST
  template<class C, class D>
  void ClassNewParticleFormation<C, D>::thread_function_run_(parameterization_nucleation_type *parameterization_nucleation,
                                                             const vector<real> &initial_gas, real time_out)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "Thread run launched." << endl;
#endif

    // Init new particle formation model for this step.
    InitStep(parameterization_nucleation);

    // Particle collect.
    test_number_ = real(0);
    test_mass_.assign(AMC::ClassSpecies::GetNspecies(), real(0));

    // Initial gas concentration.
    const int Nspecies_amc = AMC::ClassSpecies::GetNspecies();
    real *concentration_gas = new real[Nspecies_amc];

    // Init gas concentration.
    for (int i = 0; i < Nspecies_amc; ++i)
      concentration_gas[i] = real(0);

    for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
      concentration_gas[NPF::ClassSpecies::gas_index_[i]] = initial_gas[i];

    // Run model.
    bool formation_event = Run(concentration_gas, time_out, test_number_, test_mass_);

    delete[] concentration_gas;
  }


  template<class C, class D>
  void ClassNewParticleFormation<C, D>::thread_function_save_(const vector<real> &diameter_bound, const real &time_output_frequency,
                                                              vector<vector<real> > &gas, vector<vector<real> > &number,
                                                              vector<vector<vector<real> > > &mass)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fblue() << Info(1) << Reset() << "Thread save launched." << endl;
#endif

    // Number of time output.
    const int Nt = ceil(test_time_out_ / time_output_frequency);
    gas.assign(Nt, vector1r(NPF::ClassSpecies::Nspecies_, real(0)));
    number.assign(Nt, vector1r(int(diameter_bound.size()), real(0)));
    mass.assign(Nt, vector2r(int(diameter_bound.size()), vector1r(NPF::ClassSpecies::Nspecies_, real(0))));

    // Save concentrations on given size grid at each time_output_frequency.
    test_time_output_ = real(0);

    int t(0);
    while (test_time_ < test_time_out_)
      if (test_time_ >= test_time_output_)
        {
          test_mutex_.lock();

#ifdef AMC_WITH_LOGGER
          *AMCLogger::GetLog() << Bblue() << Info(2) << "Save concentrations at time "
                               << test_time_output_ << " (" << t << ")" << Reset().Str() << endl;
#endif

          gas[t].assign(NPF::ClassConcentration::gas_.begin(), NPF::ClassConcentration::gas_.end());
          NPF::ClassConcentration::MapOnSizeDiscretization(diameter_bound, number[t], mass[t]);

          number[t].back() = test_number_;
          for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
            mass[t].back()[i] = test_mass_[NPF::ClassSpecies::species_index_[i]];

          test_time_output_ += time_output_frequency;
          t++;

          test_mutex_.unlock();
        }
  }


  template<class C, class D>
  void ClassNewParticleFormation<C, D>::thread_function_gas_(const vector<vector<real> > &production_gas)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fgreen() << Info(1) << Reset() << "Thread gas launched." << endl;
#endif

    real test_time_old(real(0));
    while (test_time_ < test_time_out_)
      {
        if (test_time_old < test_time_)
          {
            test_mutex_.lock();

#ifdef AMC_WITH_LOGGER
            *AMCLogger::GetLog() << Bgreen() << Info(1) << "Update gas production at time "
                                 << test_time_ << Reset().Str() << endl;
#endif

            for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
              NPF::ClassConcentration::gas_[i] += production_gas[i][0] * exp(- production_gas[i][1] * test_time_)
                * (real(1) - exp(- production_gas[i][1] * test_time_step_));

            test_time_old = test_time_;

            test_mutex_.unlock();
          }
      }
  }
#endif


  // Init.
  template<class C, class D>
  void ClassNewParticleFormation<C, D>::Init(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info() << Reset() << "Init New Particle Formation Model." << endl;
#endif

    // Clear everything.
    ClassNewParticleFormation<C, D>::Clear();

    ClassParticleData::Init(ops);
    ClassDynamics<C, D>::Init(ops);
    ClassNumericalSolver<C, D>::Init(ops);
  }


  // Init step.
  template<class C, class D>
  void ClassNewParticleFormation<C, D>::InitStep(parameterization_nucleation_type *parameterization_nucleation)
  {
    ClassParticle::InitStep();
    ClassMeteoData::InitStep();
    ClassConcentration::InitStep();
    ClassDynamics<C, D>::InitStep(parameterization_nucleation);
    ClassNumericalSolver<C, D>::InitStep();
  }


  // Clear.
  template<class C, class D>
  void ClassNewParticleFormation<C, D>::Clear()
  {
    ClassNumericalSolver<C, D>::Clear();
    ClassDynamics<C, D>::Clear();
    ClassParticleData::Clear();
  }


  // Whether to pause or not when debugging.
#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  template<class C, class D>
  bool ClassNewParticleFormation<C, D>::ToggleDebugPause()
  {
    debug_pause_ = debug_pause_ == false;
    CBUG << "Toggle pause on debug (" << (debug_pause_ ? "yes" : "no") << ")" << endl;
  }
#endif


  // Run new particle formation model over [0, time_out].
  template<class C, class D>
  bool ClassNewParticleFormation<C, D>::Run(const real *concentration_gas, real time_out, real &number, vector<real> &mass)
  {
    // Particle formation.
    bool formation_event(false);
    number = real(0);

    // Init gas concentration.
    for (int i = 0; i < NPF::ClassSpecies::Nspecies_; ++i)
      NPF::ClassConcentration::gas_[i] = concentration_gas[NPF::ClassSpecies::gas_index_[i]];

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "initial gas = " << NPF::ClassConcentration::gas_ << endl;
#endif

    // Initial time step, which depends only of nucleation if no initial particles.
    ClassDynamics<C, D>::ComputeNucleationRate();

    // The time step to be effectively used, time_step_ is updated during computation for next loop.
    real time_step(ClassNumericalSolver<C, D>::time_step_initial_);
    ClassNumericalSolver<C, D>::compute_time_step(time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    CBUG << "time_out = " << time_out << endl;
    CBUG << "time_step = " << time_step << endl;
#endif

    //
    // Time loop.
    //

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    int loop(0);
#endif

    while (time_out > real(0))
      {
        time_step = time_step > time_out ? time_out : time_step;
#ifdef NPF_WITH_TEST
        if (is_test_)
          time_step = time_step > (test_time_output_ - test_time_) ? (test_time_out_ - test_time_) : time_step;
        test_time_step_ = time_step;
#endif

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Byellow().Str() << "<==============================================================>" << Reset().Str() << endl;
        if (debug_pause_) getchar();
#endif

        // Compute diameter and mass of each particle.
        ClassParticleData::ComputeDiameterMass();

        // Forward half time step in nucleation and coagulation.
        ClassNumericalSolver<C, D>::ForwardNucleationCoagulation(real(0.5) * time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    cout << "<======================= Remove particle 1 =======================>" << endl;
#endif

        // Eventually remove particle.
        ClassParticleData::RemoveParticle();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << "<======================= Remove particle 1 =======================>" << endl;
#endif

        // Compute diameter and mass of each particle.
        ClassParticleData::ComputeDiameterMass();

        // Forward one time step in condensation.
        if (NPF::ClassDynamics<C, D>::parameterization_condensation_ != NULL)
          ClassNumericalSolver<C, D>::ForwardCondensation(time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        NPF::ClassConcentration::test_clip();
#endif

        // Compute particle size.
        ClassConcentration::ComputeSize();

        // Eventually sort particles.
        if (ClassParticle::Nparticle_active_ > 1)
          ClassParticle::SortParticle();

        // Compute diameter and mass of each particle.
        ClassParticleData::ComputeDiameterMass();

        // Forward half time step in nucleation and coagulation.
        ClassNumericalSolver<C, D>::ForwardNucleationCoagulation(real(0.5) * time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << "<======================= Remove particle 2 =======================>" << endl;
#endif

        // Eventually remove particle.
        ClassParticleData::RemoveParticle();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << "<======================= Remove particle 2 =======================>" << endl;
#endif

        // Compute particle size.
        ClassConcentration::ComputeSize();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Fcyan().Str() << "<======================= Collect particle =======================>" << Reset().Str() << endl;
#endif

        // Collect particles beyond size discretization.
        if (ClassParticleData::CollectParticle(number, mass))
          formation_event = true;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Fcyan().Str() << "<======================= Collect particle =======================>" << Reset().Str() << endl;
#endif

        // Eventually sort particles.
        if (ClassParticle::Nparticle_active_ > 1)
          ClassParticle::SortParticle();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Fmagenta().Str() << "<======================= Manage particle =======================>" << Reset().Str() << endl;
#endif

        // Move particles through sections or merge them, if more than one section.
        if (NPF::ClassDiscretizationSize::Nsection_ > 1)
          ClassParticleData::ManageParticle();


#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Fmagenta().Str() << "<======================= Manage particle =======================>" << Reset().Str() << endl;
#endif

        // Update time.
        time_out -= time_step;

#ifdef NPF_WITH_TEST
        test_time_ -= time_step;
#endif

        // Time step for next loop.
        ClassNumericalSolver<C, D>::compute_time_step(time_step);

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        CBUG << "time_step = " << time_step << endl;
#endif

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
        cout << Byellow().Str() << "<==============================================================>" << Reset().Str() << endl;
        CBUG << "time_out = " << time_out << ", loop = " << loop++ << endl;
        CBUG << "Nparticle_active = " << NPF::ClassParticle::Nparticle_active_ << Reset().Str() << endl;
        CBUG << "Nparticle_section_active = " << NPF::ClassParticle::Nparticle_section_active_ << endl;
        CBUG << "index_active = " << NPF::ClassParticle::display_index_active() << endl;
        CBUG << "index_active_reverse = " << NPF::ClassParticle::display_index_active_reverse() << endl;
        CBUG << "section_particle_index = " << endl << NPF::ClassParticle::display_section_particle_index();
        CBUG << "section_particle_index_local = " << NPF::ClassParticle::section_particle_index_local_ << endl;
        CBUG << "gas = " << NPF::ClassConcentration::gas_ << endl;
        CBUG << "number = " << NPF::ClassConcentration::display_number() << endl;
        CBUG << "size = " << NPF::ClassParticle::display_size() << endl;
        NPF::ClassParticle::test_particle_ascending_order();
        NPF::ClassParticle::test_particle_section_ascending_order();
#endif
      }

    return formation_event;
  }


#ifdef NPF_WITH_TEST
  template<class C, class D>
  void ClassNewParticleFormation<C, D>::Test(parameterization_nucleation_type *parameterization_nucleation, const real &time_out,
                                             const vector<real> &initial_gas, const vector<vector<real> > &production_gas,
                                             const vector<real> &diameter_bound, const real &time_output_frequency,
                                             vector<vector<real> > &gas, vector<vector<real> > &number,
                                             vector<vector<vector<real> > > &mass)
  {
    // Say that we are in testing mode.
    is_test_ = true;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Test New Particle Formation model"
                         << " with nucleation parameterization \"" << parameterization_nucleation->GetName()
                         << "\" and time_out = " << time_out << endl;
#endif

#ifdef AMC_WITH_TIMER
    int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_npf_" + parameterization_nucleation->GetName());
    AMC::AMCTimer<AMC::CPU>::Start();
#endif

    // Time out.
    test_time_out_ = time_out;

    thread thread_run_model(thread_function_run_, parameterization_nucleation, std::ref(initial_gas), time_out);
    thread thread_production_gas(thread_function_gas_, std::ref(production_gas));
    thread thread_save_concentration(thread_function_save_, std::ref(diameter_bound),
                                     std::ref(time_output_frequency), std::ref(gas),
                                     std::ref(number), std::ref(mass));

    thread_run_model.join();
    thread_production_gas.join();
    thread_save_concentration.join();

#ifdef AMC_WITH_TIMER
    AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Info(1) << Reset() << "Performed 1 simulation in " << setprecision(12)
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds." << endl;
#endif
#endif

    // Return to default mode.
    is_test_ = false;
  }
#endif
}

#define NPF_FILE_CLASS_NEW_PARTICLE_FORMATION_CXX
#endif
