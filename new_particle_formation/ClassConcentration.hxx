// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPF_FILE_CLASS_CONCENTRATION_HXX

#define NPF_CONCENTRATION_NUMBER_MINIMUM 1e6 // 1. #.cm^{-3}
#define NPF_CONCENTRATION_GAS_MINIMUM 1.e-6 // µg.m^{-3}

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
#define NPF_CONCENTRATION_MASS_CONSERVATION_THRESHOLD 1.e-6
#endif

namespace NPF
{
  /*!
   * \class ClassConcentration
   */
  class ClassConcentration : virtual protected ClassSpecies, protected ClassParticle
  {
  public:

    typedef NPF::real real;
    typedef typename NPF::vector1i vector1i;
    typedef typename NPF::vector1r vector1r;
    typedef typename NPF::vector2r vector2r;

  protected:

    /*!< Gas concentration in µg.cm^{-3}.*/
    static vector1r gas_;

    /*!< Number concentration in #.cm^{-3}.*/
    static vector1r number_;

    /*!< Mass concentration in µg.cm^{-3}.*/
    static vector2r mass_;

    /*!< Total mass concentration in µg.cm^{-3}.*/
    static vector1r mass_total_;

    /*!< Compute total mass.*/
    static void compute_mass_total();

    /*!< Normalize concentration in order to get strict mass conservation.*/
    static void normalize_mass();

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
    /*!< Debug methods.*/
    static string display_number();
    static string display_mass();
    static void test_clip();
    static void test_mass_conservation();
#endif

  public:

#ifndef SWIG
    /*!< Init.*/
    static void Init();
#endif

    /*!< Init step.*/
    static void InitStep();

#ifndef SWIG
    /*!< Clear.*/
    static void Clear();
#endif

    /*!< Get methods.*/
    static void GetGas(vector<real> &gas);
    static void GetNumber(vector<real> &number);
    static void GetMass(vector<vector<real> > &mass);
    static void GetMassTotal(vector<real> &mass_total);

    /*!< Compute particle size (dry mass).*/
    static void ComputeSize();

    /*!< Map concentrations on size discretization.*/
    static void MapOnSizeDiscretization(const vector<real> &diameter_bound,
                                        vector<real> &number, vector<vector<real> > &mass);
  };
}

#define NPF_FILE_CLASS_CONCENTRATION_HXX
#endif
