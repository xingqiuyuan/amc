// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_AMC_HXX

#include "AMCHeader.hxx"

#include "util/util.cxx"
#include "ops/Ops.hxx"

#ifdef AMC_WITH_LOGGER
#include "logger/Logger.hxx"
#endif

#ifdef AMC_WITH_LOGGER
#include "share/AMCLogger.cxx"
#endif

#ifdef AMC_WITH_TIMER
#include "share/AMCTimer.cxx"
#endif

#ifdef AMC_WITH_STATISTICS
#include "share/AMCStatistics.cxx"
#endif

#include "share/Error.cxx"
#include "share/functions.cxx"
#ifdef AMC_WITH_MODAL
#include "share/ClassModalDistribution.cxx"
#endif

#ifdef DRIVER_WITH_AMC
#include "parameterization/ClassParameterizationPhysics.cxx"

#include "parameterization/ClassParameterizationDiameterBase.cxx"
#include "parameterization/ClassParameterizationDiameterFixed.cxx"
#include "parameterization/ClassParameterizationDiameterDensityFixed.cxx"
#include "parameterization/ClassParameterizationDiameterDensityMoving.cxx"
#ifdef AMC_WITH_GERBER
#include "parameterization/ClassParameterizationDiameterGerber.cxx"
#endif
#ifdef AMC_WITH_FRACTAL_SOOT
#include "parameterization/ClassParameterizationDiameterFractalSoot.cxx"
#endif
#include "species/ClassSpecies.cxx"
#include "species/ClassSpeciesTracer.cxx"
#include "species/ClassSpeciesEquilibrium.cxx"
#include "species/ClassPhase.cxx"
#include "base/ClassMeteorologicalData.cxx"
#ifdef AMC_WITH_SURFACE_TENSION
#include "parameterization/ClassParameterizationSurfaceTensionBase.cxx"
#include "parameterization/ClassParameterizationSurfaceTensionFixed.cxx"
#include "parameterization/ClassParameterizationSurfaceTensionAverage.cxx"
#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
#include "parameterization/ClassParameterizationSurfaceTensionJacobson.cxx"
#endif
#endif
#include "parameterization/ClassParameterizationCollisionEfficiencyBase.cxx"
#ifdef AMC_WITH_COLLISION_EFFICIENCY
#include "parameterization/ClassParameterizationCollisionEfficiencyFriedlander.cxx"
#include "parameterization/ClassParameterizationCollisionEfficiencyJacobson.cxx"
#include "parameterization/ClassParameterizationCollisionEfficiencySeinfeld.cxx"
#endif
#ifdef AMC_WITH_COAGULATION
#include "parameterization/ClassParameterizationCoagulationBase.cxx"
#include "parameterization/ClassParameterizationCoagulationBrownian.cxx"
#ifdef AMC_WITH_WAALS_VISCOUS
#include "parameterization/ClassParameterizationCoagulationBrownianWaalsViscous.cxx"
#endif
#include "parameterization/ClassParameterizationCoagulationGravitational.cxx"
#include "parameterization/ClassParameterizationCoagulationTurbulent.cxx"
#include "parameterization/ClassParameterizationCoagulationUnit.cxx"
#include "parameterization/ClassParameterizationCoagulationVoid.cxx"
#include "parameterization/ClassParameterizationCoagulationTable.cxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "parameterization/ClassCondensationCorrectionFactorBase.cxx"
#include "parameterization/ClassCondensationCorrectionFactorDahneke.cxx"
#include "parameterization/ClassCondensationCorrectionFactorFuchsSutugin.cxx"
#ifdef AMC_WITH_KELVIN_EFFECT
#include "parameterization/ClassParameterizationKelvinEffect.cxx"
#endif
#include "parameterization/ClassCondensationFluxCorrectionAqueousBase.cxx"
#include "parameterization/ClassCondensationFluxCorrectionAqueousVoid.cxx"
#ifdef AMC_WITH_FLUX_CORRECTION_AQUEOUS
#include "parameterization/ClassCondensationFluxCorrectionAqueousVersion1.cxx"
#include "parameterization/ClassCondensationFluxCorrectionAqueousVersion2.cxx"
#endif
#include "parameterization/ClassCondensationFluxCorrectionDryVoid.cxx"
#ifdef AMC_WITH_FLUX_CORRECTION_DRY
#include "parameterization/ClassCondensationFluxCorrectionDryInorganicSalt.cxx"
#endif
#include "parameterization/ClassParameterizationCondensationBase.cxx"
#include "parameterization/ClassParameterizationCondensationVoid.cxx"
#include "parameterization/ClassParameterizationCondensationDiffusionLimited.cxx"
#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
#include "parameterization/ClassParameterizationCondensationDiffusionLimitedSoot.cxx"
#endif
#endif
#ifdef AMC_WITH_NUCLEATION
#include "parameterization/ClassParameterizationNucleationBase.cxx"
#include "parameterization/ClassParameterizationNucleationVoid.cxx"
#ifdef AMC_WITH_POWER_LAW
#include "parameterization/ClassParameterizationNucleationPowerLaw.cxx"
#endif
#ifdef AMC_WITH_VEHKAMAKI
#include "parameterization/ClassParameterizationNucleationVehkamaki.cxx"
#endif
#ifdef AMC_WITH_MERIKANTO
#include "parameterization/ClassParameterizationNucleationMerikanto.cxx"
#endif
#endif
#include "thermodynamics/ClassModelThermodynamicBase.cxx"
#include "thermodynamics/ClassModelThermodynamicVoid.cxx"
#ifdef AMC_WITH_ISOROPIA
#include "thermodynamics/ClassModelThermodynamicIsoropia.cxx"
#endif
#ifdef AMC_WITH_PANKOW
#include "thermodynamics/ClassModelThermodynamicPankow.cxx"
#endif
#ifdef AMC_WITH_AEC
#include "thermodynamics/ClassModelThermodynamicAEC.cxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
#include "thermodynamics/ClassModelThermodynamicIsoropiaAEC.cxx"
#endif
#ifdef AMC_WITH_H2O
#include "thermodynamics/ClassModelThermodynamicH2O.cxx"
#endif
#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_H2O)
#include "thermodynamics/ClassModelThermodynamicIsoropiaH2O.cxx"
#endif
#ifdef AMC_WITH_ADSORPTION
#include "thermodynamics/ClassModelAdsorptionBase.cxx"
#include "thermodynamics/ClassModelAdsorptionPankow.cxx"
#endif
#include "base/ClassConfiguration.cxx"

#include "discretization/ClassDiscretizationSize.cxx"
#include "discretization/ClassDiscretizationCompositionBase.cxx"
#include "discretization/ClassDiscretizationCompositionTable.cxx"
#include "discretization/ClassDiscretizationComposition.cxx"
#include "discretization/ClassDiscretizationClass.cxx"
#ifdef AMC_WITH_TRACE
#include "discretization/ClassTrace.cxx"
#endif
#include "discretization/ClassDiscretization.cxx"
#include "redistribution/ClassRedistributionSizeBase.cxx"
#include "redistribution/ClassRedistributionSizeMovingDiameter.cxx"
#include "redistribution/ClassRedistributionSizeEulerHybrid.cxx"
#include "redistribution/ClassRedistributionSizeEulerMixed.cxx"
#include "redistribution/ClassRedistributionSizeTable.cxx"
#include "redistribution/ClassRedistributionCompositionBase.cxx"
#include "redistribution/ClassRedistributionCompositionTable.cxx"
#include "redistribution/ClassRedistributionComposition.cxx"
#include "redistribution/ClassRedistributionClass.cxx"
#ifdef AMC_WITH_LAYER
#include "base/ClassLayer.cxx"
#endif
#include "base/ClassAerosolData.cxx"
#include "base/ClassThermodynamics.cxx"
#include "base/ClassParameterization.cxx"
#ifdef AMC_WITH_LAYER
#include "redistribution/ClassRedistributionLayer.cxx"
#endif
#include "redistribution/ClassRedistribution.cxx"
#ifdef AMC_WITH_REPARTITION_COEFFICIENT
#include "repartition_coefficient/ClassPDF.cxx"
#ifdef AMC_WITH_TRACE
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationTrace.cxx"
#endif
#ifdef AMC_WITH_LAYER
#include "repartition_coefficient/ClassRing.cxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationLayer.cxx"
#endif
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationBase.cxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationSize.cxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationStatic.cxx"
#include "repartition_coefficient/ClassCoefficientRepartitionCoagulationMoving.cxx"
#endif
#include "dynamics/ClassDynamicsBase.cxx"
#ifdef AMC_WITH_COAGULATION
#include "dynamics/ClassDynamicsCoagulation.cxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "dynamics/ClassDynamicsCondensation.cxx"
#endif
#ifdef AMC_WITH_NUCLEATION
#include "dynamics/ClassDynamicsNucleation.cxx"
#endif
#ifdef AMC_WITH_DIFFUSION
#include "dynamics/ClassDynamicsDiffusion.cxx"
#endif
#ifdef AMC_WITH_NUMERICS
#include "numerics/ClassNumericalSolverBase.cxx"
#ifdef AMC_WITH_COAGULATION
#include "numerics/ClassNumericalSolverCoagulation.cxx"
#endif
#ifdef AMC_WITH_CONDENSATION
#include "numerics/ClassNumericalSolverCondensation.cxx"
#endif
#ifdef AMC_WITH_NUCLEATION
#include "numerics/ClassNumericalSolverNucleation.cxx"
#endif
#endif
#include "base/ClassAMC.cxx"
#ifdef AMC_WITH_RUN
#include "base/ClassAMX.cxx"
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
#include "new_particle_formation/ClassSpecies.cxx"
#include "new_particle_formation/ClassDiscretizationSize.cxx"
#include "new_particle_formation/ClassParticle.cxx"
#include "new_particle_formation/ClassConcentration.cxx"
#include "new_particle_formation/ClassMeteoData.cxx"
#include "new_particle_formation/ClassParticleData.cxx"
#include "new_particle_formation/ClassDynamics.cxx"
#include "new_particle_formation/ClassNumericalSolver.cxx"
#include "new_particle_formation/ClassNewParticleFormation.cxx"
#endif
#endif // DRIVER_WITH_AMC

namespace AMC
{

#ifdef AMC_WITH_LOGGER
  LogStream AMCLogger::logger_;
#endif

#ifdef AMC_WITH_TIMER
  template<class C> int AMCTimer<C>::Ntimer_ = 0;
  template<class C> vector<typename C::duration_type_> AMCTimer<C>::timer_;
  template<class C> vector<string> AMCTimer<C>::name_;
  template<class C> typename C::point_type_ AMCTimer<C>::start_;
#endif

#ifdef AMC_WITH_STATISTICS
  int AMCStatistics::Nstatistics_ = 0;
  vector1b AMCStatistics::compute_rolling_;
  vector1i AMCStatistics::compute_frequency_;
  vector1i AMCStatistics::write_frequency_;
  // Set AMCStatistics.hxx or AMCTimer.hxx to see what point_type_ is.
  vector<point_type_> AMCStatistics::time_last_;
  map<string, int> AMCStatistics::index_;
  vector1i AMCStatistics::n_;
  vector1s AMCStatistics::name_;
  vector2r AMCStatistics::value_min_;
  vector2r AMCStatistics::value_max_;
  vector2r AMCStatistics::value_mean_;
  vector2r AMCStatistics::value_std_;
  vector<unsigned long> AMCStatistics::count_;
  vector1s AMCStatistics::file_path_;
#endif

#ifdef AMC_WITH_MODAL
  // Modal distribution.
  template<class T> bool ClassModalDistribution<T>::is_initialized_ = false;
  template<class T> string ClassModalDistribution<T>::configuration_file_ = MODAL_DISTRIBUTION_CONFIGURATION_FILE_DEFAULT;
#endif

#ifdef DRIVER_WITH_AMC
  // Configuration.
  const int init_flag::no;
  const int init_flag::amc;
  const int init_flag::amx;
  int ClassConfiguration::initialized_ = init_flag::no;
  string ClassConfiguration::configuration_file_ = "";
  string ClassConfiguration::prefix_ = "";
  Ops::Ops* ClassConfiguration::ops_ = NULL;

  // Size discretization.
  int ClassDiscretizationSize::Nsection_ = 0;
  int ClassDiscretizationSize::Nbound_ = 0;
  real ClassDiscretizationSize::density_fixed_;
  vector1r ClassDiscretizationSize::diameter_bound_;
  vector1r ClassDiscretizationSize::mass_bound_;
  vector1r ClassDiscretizationSize::mass_width_;
  vector1r ClassDiscretizationSize::diameter_mean_;
  vector1r ClassDiscretizationSize::surface_mean_;
  vector1r ClassDiscretizationSize::mass_mean_;
  vector1r ClassDiscretizationSize::mass_bound_log_;
  vector1r ClassDiscretizationSize::mass_mean_log_;
  vector1r ClassDiscretizationSize::mass_width_log_;

  // Species.
  int ClassSpecies::Nspecies_ = 0;
  int ClassSpecies::Ngas_ = 0;
  real ClassSpecies::molar_mass_average_ = real(AMC_SPECIES_MOLAR_MASS_AVERAGE);
  vector1b ClassSpecies::is_organic_;
  vector1b ClassSpecies::is_tracer_;
  vector2i ClassSpecies::phase_;
  vector1i ClassSpecies::semivolatile_;
  vector1i ClassSpecies::semivolatile_reverse_;
  vector1i ClassSpecies::carbon_number_;
  vector1s ClassSpecies::name_;
  vector1s ClassSpecies::long_name_;
  vector1s ClassSpecies::color_;
  vector1s ClassSpecies::hatch_;
  vector1s ClassSpecies::label_;
  vector1r ClassSpecies::drh_;
  vector1r ClassSpecies::molar_mass_;
  vector1r ClassSpecies::density_;
  vector1r ClassSpecies::molar_volume_;
  vector1r ClassSpecies::sorption_site_;
  vector1r ClassSpecies::surface_tension_;
  vector1r ClassSpecies::saturation_vapor_pressure_;
  vector1r ClassSpecies::saturation_vapor_concentration_;
  vector1r ClassSpecies::partition_coefficient_;
  vector1r ClassSpecies::vaporization_enthalpy_;
  vector1r ClassSpecies::accomodation_coefficient_;
  vector1r ClassSpecies::molecular_diameter_;
  vector1r ClassSpecies::collision_factor_;
  vector1r ClassSpecies::henry_constant_;
  vector1r ClassSpecies::antoine_law_a_;
  vector1r ClassSpecies::antoine_law_b_;
  vector1b ClassSpecies::use_antoine_law_;
  map<string, vector1r*> ClassSpecies::property_;
  map<string, string> ClassSpecies::property_unit_;

  // Species equilibrium.
  int ClassSpeciesEquilibrium::Nspecies_ = 0;
  int ClassSpeciesEquilibrium::Nsolid_ = 0;
  int ClassSpeciesEquilibrium::Nionic_ = 0;
  int ClassSpeciesEquilibrium::Norganic_ = 0;
  vector1i ClassSpeciesEquilibrium::ionic_;
  vector1i ClassSpeciesEquilibrium::solid_;
  vector1i ClassSpeciesEquilibrium::organic_;
  vector1s ClassSpeciesEquilibrium::name_;
  vector1r ClassSpeciesEquilibrium::molar_mass_;
  vector1r ClassSpeciesEquilibrium::density_;
  vector1i ClassSpeciesEquilibrium::Ngroup_;
  vector2i ClassSpeciesEquilibrium::group_;
  vector1i ClassSpeciesEquilibrium::electric_charge_;
  vector1i ClassSpeciesEquilibrium::carbon_number_;
  vector1b ClassSpeciesEquilibrium::is_organic_;
  vector2i ClassSpeciesEquilibrium::phase_;

  // Tracer.
  int ClassSpeciesTracer::Ntracer_ = 0;
  vector1i ClassSpeciesTracer::Nspecies_;
  vector1s ClassSpeciesTracer::name_;
  vector2s ClassSpeciesTracer::species_name_;
  vector2r ClassSpeciesTracer::species_mass_fraction_;
  vector2r ClassSpeciesTracer::species_molar_fraction_;

  // Aerosol phase.
  int ClassPhase::aqueous_phase_index_ = AMC_AQUEOUS_PHASE_INDEX;
  int ClassPhase::Nphase_ = 0;
  vector1s ClassPhase::name_;
  vector2i ClassPhase::species_;

  // Thermodynamics.
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
  mutex ClassModelThermodynamicBase::mutex_thermodynamic_;
#endif

#ifdef AMC_WITH_FLUX_CORRECTION_DRY
  bool ClassCondensationFluxCorrectionDryInorganicSalt::is_initiated_ = false;

  real ClassCondensationFluxCorrectionDryInorganicSalt::XK10_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK10_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK6_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK6_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK3_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK3_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK4_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK4_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK8_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK8_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK9_reference_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK9_ = real(0);

  real ClassCondensationFluxCorrectionDryInorganicSalt::XK10_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK10_coefficient1_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK6_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK6_coefficient1_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK3_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK3_coefficient1_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK4_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK4_coefficient1_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK8_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK8_coefficient1_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK9_coefficient0_ = real(0);
  real ClassCondensationFluxCorrectionDryInorganicSalt::XK9_coefficient1_ = real(0);
#endif

  // Meteorological related data.
  real ClassMeteorologicalData::temperature_ = real(AMC_TEMPERATURE_REFERENCE);
  real ClassMeteorologicalData::pressure_ = real(AMC_PRESSURE_REFERENCE);
  real ClassMeteorologicalData::relative_humidity_ = real(AMC_RELATIVE_HUMIDITY_DEFAULT);
  real ClassMeteorologicalData::eddy_dissipation_rate_ = real(AMC_EDDY_DISSIPATION_RATE_DEFAULT);
  real ClassMeteorologicalData::air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);
  real ClassMeteorologicalData::water_dynamic_viscosity_ = real(AMC_WATER_DYNAMIC_VISCOSITY_DEFAULT);
  real ClassMeteorologicalData::air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);
  real ClassMeteorologicalData::air_density_ = real(AMC_AIR_DENSITY_DEFAULT);
  real ClassMeteorologicalData::water_surface_tension_ = real(AMC_WATER_SURFACE_TENSION_DEFAULT);
  real ClassMeteorologicalData::sulfuric_acid_saturation_vapor_pressure_ = real(0);
  vector1r ClassMeteorologicalData::gas_diffusivity_;
  vector1r ClassMeteorologicalData::gas_quadratic_mean_velocity_;
  vector1r ClassMeteorologicalData::saturation_vapor_pressure_;
  vector1r ClassMeteorologicalData::saturation_vapor_concentration_;
  vector1r ClassMeteorologicalData::partition_coefficient_;
  vector1r ClassMeteorologicalData::henry_constant_;
  map<string, real* > ClassMeteorologicalData::variable_0d_;
  map<string, vector<real>* > ClassMeteorologicalData::variable_1d_;
  map<string, string> ClassMeteorologicalData::variable_0d_unit_;
  map<string, string> ClassMeteorologicalData::variable_1d_unit_;

  // Aerosol related data.
  int ClassAerosolData::Ng_ = 0;
  int ClassAerosolData::NgNspecies_ = 0;
  int ClassAerosolData::NgNspecies_equilibrium_ = 0;
  int ClassAerosolData::NgNgas_ = 0;
  int ClassAerosolData::NgNphase_ = 0;
  vector2i ClassAerosolData::general_section_;
  vector2i ClassAerosolData::general_section_reverse_;
  vector1i ClassAerosolData::phase_;
  vector1r ClassAerosolData::equilibrium_gas_surface_;
  vector1r ClassAerosolData::equilibrium_aer_internal_;
  vector1r ClassAerosolData::liquid_water_content_;
  vector1r ClassAerosolData::density_;
  vector1r ClassAerosolData::diameter_;
  vector1r ClassAerosolData::surface_;
  vector1r ClassAerosolData::mass_;
  vector1r ClassAerosolData::temperature_;
  vector1r ClassAerosolData::surface_tension_;
  vector1r ClassAerosolData::adsorption_coefficient_;
#ifdef AMC_WITH_LAYER
  int ClassAerosolData::NgNspeciesNlayer_ = 0;
  vector2r ClassAerosolData::layer_radius_;
  vector2r ClassAerosolData::layer_repartition_;
#endif

#ifdef AMC_WITH_LAYER
  int ClassLayer::Nspecies_layer_ = 0;
  bool ClassLayer::multiple_layer_ = false;
  int ClassLayer::Nlayer_max_ = 0;
  vector1i ClassLayer::Nlayer_;
  vector1i ClassLayer::species_index_layer_;
  vector1b ClassLayer::is_species_layered_;
  vector2r ClassLayer::layer_radius_fraction_;
#endif

#ifdef AMC_WITH_TRACE
  int ClassTrace::Ntrace_ = 0;
  vector1i ClassTrace::general_section_index_reverse_;
  vector2i ClassTrace::general_section_index_;
  vector2i ClassTrace::size_class_index_;
  vector1s ClassTrace::name_;
#endif


  // Thermodynamics.
  int ClassThermodynamics::Nmodel_thermo_ = 0;
  vector<ClassModelThermodynamicBase* > ClassThermodynamics::model_thermodynamic_list_;
  vector2i ClassThermodynamics::model_thermodynamic_section_;
  vector<vector<ClassModelThermodynamicBase* > > ClassThermodynamics::model_thermodynamic_;
#ifdef AMC_WITH_ADSORPTION
  ClassModelAdsorptionBase* ClassThermodynamics::model_adsorption_ = NULL;
  vector1i ClassThermodynamics::model_adsorption_section_;
#endif
#ifdef AMC_WITH_THREAD_THERMODYNAMIC
  int ClassThermodynamics::Nthread_;
  vector1i ClassThermodynamics::Ng_thread_;
  vector1i ClassThermodynamics::thread_offset_;
#endif


  // Parameterizations.
  int ClassParameterization::Nparam_diameter_ = 0;
  vector<ClassParameterizationDiameterBase* > ClassParameterization::parameterization_diameter_list_;
  vector2i ClassParameterization::parameterization_diameter_section_;
  vector<ClassParameterizationDiameterBase* >  ClassParameterization::parameterization_diameter_;
#ifdef AMC_WITH_GERBER
  ClassParameterizationDiameterGerber* ClassParameterization::parameterization_diameter_gerber_ = NULL;
#endif
#ifdef AMC_WITH_SURFACE_TENSION
  int ClassParameterization::Nparam_surface_tension_ = 0;
  vector<ClassParameterizationSurfaceTensionBase* > ClassParameterization::parameterization_surface_tension_list_;
  vector2i ClassParameterization::parameterization_surface_tension_section_;
  vector<ClassParameterizationSurfaceTensionBase* >  ClassParameterization::parameterization_surface_tension_;
#endif

  // Discretization.
  int ClassDiscretizationCompositionBase::id_counter_ = 0;

  template<class D> int ClassDiscretizationClass<D>::Nclass_max_ = 0;
  template<class D> vector<ClassDiscretizationComposition<D>* >
  ClassDiscretizationClass<D>::discretization_composition_list_;
  template<class D> vector<ClassDiscretizationComposition<D>* >
  ClassDiscretizationClass<D>::discretization_composition_;
  template<class D> vector1i ClassDiscretizationClass<D>::Nclass_;

  template<class D> int ClassDiscretization<D>::Ng_ = 0;
  template<class D> int ClassDiscretization<D>::Nspecies_ = 0;
  template<class D> vector2i ClassDiscretization<D>::general_section_;
  template<class D> vector2i ClassDiscretization<D>::general_section_reverse_;
  template<class D> vector2s ClassDiscretization<D>::class_composition_name_;

  // Redistribution.
  template<class D> int ClassRedistributionCompositionBase<D>::id_counter_ = 0;
  template<class R, class D> vector<ClassRedistributionComposition<R, D>* > ClassRedistributionClass<R, D>::redistribution_composition_list_;
  template<class R, class D> vector<ClassRedistributionComposition<R, D>* > ClassRedistributionClass<R, D>::redistribution_composition_;
  template<class R, class D> int ClassRedistributionClass<R, D>::Nclass_max_ = 0;

  template<class S, class R, class D> S* ClassRedistribution<S, R, D>::redistribution_size_ = NULL;
  template<class S, class R, class D> int ClassRedistribution<S, R, D>::Nsection_max_ = 0;
  template<class S, class R, class D> vector1r ClassRedistribution<S, R, D>::concentration_aer_number_work_;
  template<class S, class R, class D> vector1r ClassRedistribution<S, R, D>::concentration_aer_mass_work_;
  template<class S, class R, class D> vector1r ClassRedistribution<S, R, D>::concentration_aer_mass_total_work_;
#ifdef AMC_WITH_LAYER
  template<class S, class R, class D> Array<real, 2> ClassRedistribution<S, R, D>::layer_repartition_work_;
  vector2r ClassRedistributionLayer::layer_mass_fraction_;
  Array<real, 2> ClassRedistributionLayer::layer_redistribution_coefficient_a_;
  Array<real, 2> ClassRedistributionLayer::layer_redistribution_coefficient_b_;
#endif

#ifdef AMC_WITH_REPARTITION_COEFFICIENT
  int ClassPDF::Npdf_ = 14;
  int ClassPDF::Ncase_ = 20;
  int ClassPDF::Nstep_ = 9;
  vector1s ClassPDF::pdf_ = {"aaa", "aab", "aba", "abb", "abc", "ba", "bb", "bc", "bd", "caa", "cab", "cac", "cba", "cbb"};
  vector1i ClassPDF::case_ = {111, 112, 113, 121, 122, 131, 132, 14, 211, 212, 213, 221, 222, 311, 312, 313, 321, 322, 11, 13};
  vector1s ClassPDF::step_ = {"x_{ij}^-", "x_{i^-j}", "x_{ij^-}", "x_{ij}^{-+}", "x_{ij}", "x_{ij}^{+-}", "x_{ij^+}",
                              "x_{i^+j}", "x_{ij}^+"};

  vector1i ClassPDF::Npdf_case_ = {8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 5, 5};
  vector2i ClassPDF::case_pdf_ = {{0, 1, 3, 4, 7, 10, 12, 13},
                                  {0, 1, 3, 6, 7, 10, 12, 13},
                                  {0, 1, 3, 6, 9, 10, 12, 13},
                                  {0, 1, 5, 6, 7, 10, 12, 13},
                                  {0, 1, 5, 6, 9, 10, 12, 13},
                                  {0, 1, 3, 4, 7,  8, 12, 13},
                                  {0, 1, 3, 6, 7,  8, 12, 13},
                                  {0, 1, 5, 6, 7,  8, 12, 13},
                                  {0, 2, 3, 4, 7, 10, 12, 13},
                                  {0, 2, 3, 6, 7, 10, 12, 13},
                                  {0, 2, 3, 6, 9, 10, 12, 13},
                                  {0, 2, 3, 4, 7,  8, 12, 13},
                                  {0, 2, 3, 6, 7,  8, 12, 13},
                                  {0, 1, 3, 4, 7, 10, 11, 13},
                                  {0, 1, 3, 6, 7, 10, 11, 13},
                                  {0, 1, 3, 6, 9, 10, 11, 13},
                                  {0, 1, 5, 6, 7, 10, 11, 13},
                                  {0, 1, 5, 6, 9, 10, 11, 13},
                                  {0, 3, 4, 10, 13},
                                  {0, 3, 9, 10, 13}};

  vector1i ClassPDF::Nstep_case_ = {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 6, 6};
  vector2i ClassPDF::case_step_ = {{0, 1, 2, 4, 3, 5, 6, 7, 8},
                                   {0, 1, 2, 3, 4, 5, 6, 7, 8},
                                   {0, 1, 2, 3, 5, 4, 6, 7, 8},
                                   {0, 1, 3, 2, 4, 5, 6, 7, 8},
                                   {0, 1, 3, 2, 5, 4, 6, 7, 8},
                                   {0, 1, 2, 4, 3, 6, 5, 7, 8},
                                   {0, 1, 2, 3, 4, 6, 5, 7, 8},
                                   {0, 1, 3, 2, 4, 6, 5, 7, 8},
                                   {0, 2, 1, 4, 3, 5, 6, 7, 8},
                                   {0, 2, 1, 3, 4, 5, 6, 7, 8},
                                   {0, 2, 1, 3, 5, 4, 6, 7, 8},
                                   {0, 2, 1, 4, 3, 6, 5, 7, 8},
                                   {0, 2, 1, 3, 4, 6, 5, 7, 8},
                                   {0, 1, 2, 4, 3, 5, 7, 6, 8},
                                   {0, 1, 2, 3, 4, 5, 7, 6, 8},
                                   {0, 1, 2, 3, 5, 4, 7, 6, 8},
                                   {0, 1, 3, 2, 4, 5, 7, 6, 8},
                                   {0, 1, 3, 2, 5, 4, 7, 6, 8},
                                   {0, 2, 4, 3, 6, 8},
                                   {0, 2, 3, 4, 6, 8}};
#ifdef AMC_WITH_LAYER
#ifdef AMC_WITH_TEST
  vector<int> ClassRing::case_counter_test_ = vector1i(RING_NUMBER_CASE, 0);
#endif
#endif
#endif

#ifdef AMC_WITH_COAGULATION
  template<class C> int ClassDynamicsCoagulation<C>::Nparam_ = 0;
  template<class C> vector<ClassParameterizationCoagulationBase* > ClassDynamicsCoagulation<C>::parameterization_list_;
  template<class C> vector3i ClassDynamicsCoagulation<C>::parameterization_couple_;
  template<class C> C* ClassDynamicsCoagulation<C>::repartition_coefficient_ = NULL;
  template<class C> Array<real, 2> ClassDynamicsCoagulation<C>::kernel_;
#ifdef AMC_WITH_LAYER
  template<class C> Array<real, 2> ClassDynamicsCoagulation<C>::layer_repartition_rate_;
#endif
#endif

#ifdef AMC_WITH_CONDENSATION
  int ClassDynamicsCondensation::Niter_max_ = AMC_CONDENSATION_NITER_MAX_DEFAULT;
  double ClassDynamicsCondensation::accuracy_ = AMC_CONDENSATION_ACCURACY_DEFAULT;
  double ClassDynamicsCondensation::pgtol_ = AMC_CONDENSATION_PGTOL_DEFAULT;
  real ClassDynamicsCondensation::epsilon_ = AMC_CONDENSATION_EPSILON_DEFAULT;
  int ClassDynamicsCondensation::Nparam_ = 0;
  int ClassDynamicsCondensation::Nmodel_equilibrium_ = 0;
  real ClassDynamicsCondensation::qssa_threshold_min_ = AMC_CONDENSATION_QSSA_THRESHOLD_MIN_DEFAULT;
  real ClassDynamicsCondensation::qssa_threshold_max_ = AMC_CONDENSATION_QSSA_THRESHOLD_MAX_DEFAULT;
  vector<ClassParameterizationCondensationBase* > ClassDynamicsCondensation::parameterization_list_;
  vector2i ClassDynamicsCondensation::parameterization_section_;
  vector<ClassModelThermodynamicBase* > ClassDynamicsCondensation::equilibrium_model_list_;
#ifdef AMC_WITH_KELVIN_EFFECT
  ClassParameterizationKelvinEffect* ClassDynamicsCondensation::kelvin_effect_ = NULL;
#endif
#endif

#ifdef AMC_WITH_NUCLEATION
  int ClassDynamicsNucleation::Nparam_ = 0;
  int ClassDynamicsNucleation::parameterization_default_index_ = -1;
  vector<ClassParameterizationNucleationBase* > ClassDynamicsNucleation::parameterization_list_;
  vector1i ClassDynamicsNucleation::parameterization_section_;
  vector1i ClassDynamicsNucleation::parameterization_index_;
#ifdef AMC_WITH_POWER_LAW
  string ClassParameterizationNucleationPowerLaw::configuration_file_ = "power_law.lua";
#endif
#endif

#ifdef AMC_WITH_DIFFUSION
  int ClassDynamicsDiffusion::Nparam_ = 0;
#endif

#ifdef AMC_WITH_NUMERICS
#ifdef AMC_WITH_COAGULATION
  template<class C> int ClassNumericalSolverCoagulation<C>::Nt_max_ = AMC_COAGULATION_NUMBER_TIME_STEP_MAX;
  template<class C> bool ClassNumericalSolverCoagulation<C>::use_semi_implicit_ = false;
  template<class C> Array<real, 2> ClassNumericalSolverCoagulation<C>::A_;
  template<class C> Array<real, 3> ClassNumericalSolverCoagulation<C>::B_;
#endif
#ifdef AMC_WITH_CONDENSATION
  int ClassNumericalSolverCondensation::Nt_max_ = AMC_CONDENSATION_NUMBER_TIME_STEP_MAX;
#endif
#endif

  //#ifdef AMC_WITH_RUN
  // AMX.
  //#endif

#include "typedef.hxx"
#endif // DRIVER_WITH_AMC
}

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
namespace NPF
{
  int ClassSpecies::Nspecies_ = 0;
  int ClassSpecies::index_ammonium_ = -1;
  vector1s ClassSpecies::name_;
  vector1i ClassSpecies::species_index_;
  vector1i ClassSpecies::gas_index_;
  vector1r ClassSpecies::molar_mass_;
  vector1r ClassSpecies::accomodation_coefficient_;
  vector1r ClassSpecies::density_;

  int ClassDiscretizationSize::Nsection_ = 0;
  int ClassDiscretizationSize::Nbound_ = 0;
  real ClassDiscretizationSize::density_fixed_ = NPF_NEW_PARTICLE_FORMATION_PARTICLE_DENSITY_DEFAULT;
  vector1r ClassDiscretizationSize::diameter_bound_;
  vector1r ClassDiscretizationSize::mass_bound_;
  vector1r ClassDiscretizationSize::mass_width_;

  int ClassParticle::Nparticle_ = 0;
  vector1i ClassParticle::Nparticle_section_;
  vector1i ClassParticle::particle_section_index_;
  int ClassParticle::Nparticle_active_ = 0;
  vector2i ClassParticle::section_particle_index_;
  vector1i ClassParticle::section_particle_index_local_;
  vector1i ClassParticle::Nparticle_section_active_;
  vector1i ClassParticle::index_active_;
  vector1i ClassParticle::index_active_reverse_;
  vector1r ClassParticle::size_;

  real ClassMeteoData::pressure_ = real(AMC_PRESSURE_REFERENCE);
  real ClassMeteoData::temperature_ = real(AMC_TEMPERATURE_REFERENCE);
  real ClassMeteoData::relative_humidity_ = real(AMC_RELATIVE_HUMIDITY_DEFAULT);
  real ClassMeteoData::air_free_mean_path_ = real(AMC_AIR_FREE_MEAN_PATH_DEFAULT);
  real ClassMeteoData::air_dynamic_viscosity_ = real(AMC_AIR_DYNAMIC_VISCOSITY_DEFAULT);
  vector1r ClassMeteoData::gas_quadratic_mean_velocity_;
  vector1r ClassMeteoData::gas_diffusivity_;

  vector1r ClassConcentration::gas_;
  vector1r ClassConcentration::number_;
  vector2r ClassConcentration::mass_;
  vector1r ClassConcentration::mass_total_;

  vector1r ClassParticleData::mass_;
  vector1r ClassParticleData::diameter_;
  vector1r ClassParticleData::density_;
  vector1r ClassParticleData::liquid_water_content_;
  vector2r ClassParticleData::concentration_gas_equilibrium_;

  template<class C, class D> npf_parameterization_nucleation_type *ClassDynamics<C, D>::parameterization_nucleation_ = NULL;
  template<class C, class D> vector2r ClassDynamics<C, D>::coagulation_kernel_;
  template<class C, class D> vector2r ClassDynamics<C, D>::condensation_coefficient_;
  template<class C, class D> bool ClassDynamics<C, D>::nucleation_event_ = false;
  template<class C, class D> real ClassDynamics<C, D>::nucleation_diameter_ = real(0);
  template<class C, class D> real ClassDynamics<C, D>::nucleation_rate_number_ = real(0);
  template<class C, class D> vector1r ClassDynamics<C, D>::nucleation_rate_mass_;
  template<class C, class D> int ClassDynamics<C, D>::Ncouple_ = 0;
  template<class C, class D> C *ClassDynamics<C, D>::parameterization_coagulation_ = NULL;
  template<class C, class D> D *ClassDynamics<C, D>::parameterization_condensation_ = NULL;

#ifdef NPF_WITH_COAGULATION_TABLE
  template<class C, class D> bool ClassDynamics<C, D>::is_coagulation_kernel_tabulated_ = false;
  template<class C, class D> int ClassDynamics<C, D>::Ntable_ = 0;
  template<class C, class D> real ClassDynamics<C, D>::table_diameter_min_inv_ = real(0);
  template<class C, class D> real ClassDynamics<C, D>::table_diameter_delta_log_inv_ = real(0);
  template<class C, class D> vector1r ClassDynamics<C, D>::table_diameter_;
  template<class C, class D> vector2r ClassDynamics<C, D>::table_coagulation_kernel_;
#endif

  template<class C, class D> real ClassNumericalSolver<C, D>::number_total_ = real(0);
  template<class C, class D> real ClassNumericalSolver<C, D>::mass_total_ = real(0);
  template<class C, class D> real ClassNumericalSolver<C, D>::number_variation_ = real(0);
  template<class C, class D> real ClassNumericalSolver<C, D>::mass_variation_ = real(0);
  template<class C, class D> real ClassNumericalSolver<C, D>::time_step_initial_ = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_INITIAL;
  template<class C, class D> real ClassNumericalSolver<C, D>::time_step_max_ = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_MAX;
  template<class C, class D> real ClassNumericalSolver<C, D>::time_step_alfa_ = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_ALFA;
  template<class C, class D> real ClassNumericalSolver<C, D>::time_step_epsilon_ = NPF_NEW_PARTICLE_FORMATION_TIME_STEP_EPSILON;
  template<class C, class D> vector1r ClassNumericalSolver<C, D>::d_;
  template<class C, class D> vector1r ClassNumericalSolver<C, D>::c_;
  template<class C, class D> vector1r ClassNumericalSolver<C, D>::b_;
  template<class C, class D> vector2r ClassNumericalSolver<C, D>::a_;
  template<class C, class D> vector1r ClassNumericalSolver<C, D>::B_;
  template<class C, class D> vector2r ClassNumericalSolver<C, D>::A_;

#ifdef NPF_WITH_DEBUG_NEW_PARTICLE_FORMATION
  template<class C, class D> bool ClassNewParticleFormation<C, D>::debug_pause_(false);
#endif
#ifdef NPF_WITH_TEST
  template<class C, class D> std::mutex ClassNewParticleFormation<C, D>::test_mutex_;
  template<class C, class D> bool ClassNewParticleFormation<C, D>::is_test_ = false;
  template<class C, class D> real ClassNewParticleFormation<C, D>::test_time_ = real(0);
  template<class C, class D> real ClassNewParticleFormation<C, D>::test_time_out_ = real(0);
  template<class C, class D> real ClassNewParticleFormation<C, D>::test_time_output_ = real(0);
  template<class C, class D> real ClassNewParticleFormation<C, D>::test_time_step_ = real(0);
  template<class C, class D> real ClassNewParticleFormation<C, D>::test_number_ = real(0);
  template<class C, class D> vector<real> ClassNewParticleFormation<C, D>::test_mass_;
#endif
}
#endif

#define AMC_FILE_AMC_HXX
#endif
