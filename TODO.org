* todo:
  - [ ] coder solver numériques
  - [X] coder AEC
  - [ ] vérifier comment la limitation H+ doit prendre en compte l'effet kelvin
  - [X] class composition : comment est prise en compte la dernière classe, celle qui résulte de la
    négation de toutes les autres.
  - [ ] l'enthalpie de vaporisation est-elle la même chose que la chaleur de dissociation ?  à vérifier
    pour la constante de Henry en fonction de la température (loi de Van't Hoff).
  - [X] tabuler les coefficients de Gerber suivant Isoropia.
  - [-] AEC :
    - [ ] coder ZSR
    - [ ] vérifier bon usage des coefficients d'activité et comment calculer ceux des ions.
    - [X] mettre H2O dans AEC
  - [ ] qu'est-ce que metrix en entrée de setulb ?
  - [ ] comment est pris en compte l'internal mixing avec la tabulation de la composition ?
  - [X] redistribution en taille, avec la tabulation, y-a-t-il interpolation ? non, mais pas nécessaire
  - [ ] Isoropia doit prendre le contenu en eau liquide à l'entrée
  - [X] Enlever dependance à Lapack et limiter le nombre de dimension à 3.
  - [X] Changer le générateur de nombre aléatoire : prendre celui du C++.
  - [ ] Coder la thermo de NPF différemment.
  - [ ] Vérififer validité des paramétrisations de NPF, notamment vis-à-vis du diamètre
