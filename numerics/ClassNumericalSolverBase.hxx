// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_BASE_HXX

#define AMC_NUMERICAL_SOLVER_BASE_EXPLICIT_RATIO 0.8

namespace AMC
{
  /*! 
   * \class ClassNumericalSolverBase
   */
  class ClassNumericalSolverBase : protected ClassAerosolData
  {
  public:
    typedef AMC::real real;
    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;

  public:

    /*!< Explicit.*/
    static real Explicit(const int section_min,
                         const int section_max,
                         const real &time_step,
                         const real *rate_aer_number,
                         const real *rate_aer_mass,
                         const real *rate_gas,
                         real *concentration_aer_number,
                         real *concentration_aer_mass,
                         real *concentration_gas,
                         const real ratio = AMC_NUMERICAL_SOLVER_BASE_EXPLICIT_RATIO);
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_BASE_HXX
#endif
