// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_CONDENSATION_HXX


namespace AMC
{
  /*! 
   * \class ClassNumericalSolverCondensation
   */
  class ClassNumericalSolverCondensation : protected ClassNumericalSolverBase
  {
  public:
    typedef AMC::real real;
    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;

  protected:

    /*!< Maximum number of time steps.*/
    static int Nt_max_;

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNtMax();

    /*!< Eulerian rate.*/
    static void RateEulerian(const int section_min,
                             const int section_max,
                             const real &time_step,
                             const real *concentration_aer_number,
                             const real *concentration_aer_mass,
                             const real *concentration_gas,
                             vector<real> &rate_aer_number,
                             vector<real> &rate_aer_mass,
                             vector<real> &rate_gas);

    /*!< Explicit solver for condensation.*/
    static real LagrangianExplicit(const int section_min,
                                   const int section_max,
                                   const real &time_step,
                                   vector<real> &rate_aer_mass,
                                   vector<real> &rate_gas,
                                   real *concentration_aer_number,
                                   real *concentration_aer_mass,
                                   real *concentration_gas,
                                   const real ratio = AMC_NUMERICAL_SOLVER_BASE_EXPLICIT_RATIO);
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_CONDENSATION_HXX
#endif
