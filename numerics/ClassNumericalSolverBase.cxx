// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_BASE_CXX

#include "ClassNumericalSolverBase.hxx"

namespace AMC
{

  // Compute max time step.
  real ClassNumericalSolverBase::Explicit(const int section_min,
                                          const int section_max,
                                          const real &time_step,
                                          const real* rate_aer_number,
                                          const real* rate_aer_mass,
                                          const real* rate_gas,
                                          real *concentration_aer_number,
                                          real *concentration_aer_mass,
                                          real *concentration_gas,
                                          const real ratio)
  {
    // Find lowest time scale.
    real time_step_max(numeric_limits<real>::max());

    const int index_mass_min = ClassSpecies::Nspecies_ * section_min;
    const int index_mass_max = ClassSpecies::Nspecies_ * section_max;

    if (concentration_aer_number != NULL)
      for (int i = section_min; i < section_max; ++i)
        if (concentration_aer_number[i] > AMC_NUMBER_CONCENTRATION_MINIMUM)
          if (abs(rate_aer_number[i]) > real(0))
            {
              const real time_scale = concentration_aer_number[i] / abs(rate_aer_number[i]);
              if (time_step_max > time_scale) time_step_max = time_scale;
            }

    for (int i = index_mass_min; i < index_mass_max; ++i)
      if (concentration_aer_mass[i] > AMC_MASS_CONCENTRATION_MINIMUM)
        if (abs(rate_aer_mass[i]) > real(0))
          {
            const real time_scale = concentration_aer_mass[i] / abs(rate_aer_mass[i]);
            if (time_step_max > time_scale) time_step_max = time_scale;
          }

    if (concentration_gas != NULL)
      for (int i = 0; i < ClassSpecies::Ngas_; ++i)
        if (concentration_gas[i] > real(0))
          if (abs(rate_gas[i]) > real(0))
            {
              const real time_scale = concentration_gas[i] / abs(rate_gas[i]);
              if (time_step_max > time_scale) time_step_max = time_scale;
            }

    // Maximum admissible time step which prevent clipping.
    time_step_max *= ratio;

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_BASE
    CBUG << "time_step_max = " << time_step_max << endl;
#endif

    // Effective time step.
    const real time_step_effective = time_step > time_step_max ? time_step_max : time_step;

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_BASE
    CBUG << "time_step_effective = " << time_step_effective << endl;
#endif

    // Explicit 1st order integration.
    if (concentration_aer_number != NULL)
      for (int i = section_min; i < section_max; ++i)
        concentration_aer_number[i] += time_step_effective * rate_aer_number[i];

    for (int i = index_mass_min; i < index_mass_max; ++i)
      concentration_aer_mass[i] += time_step_effective * rate_aer_mass[i];

    if (concentration_gas != NULL)
      for (int i = 0; i < ClassSpecies::Ngas_; ++i)
        concentration_gas[i] += time_step_effective * rate_gas[i];

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_BASE
    CBUG << "time_step_remaining = " << time_step - time_step_effective << endl;
#endif

    // Finally return what we could not integrate.
    return time_step - time_step_effective;
  }
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_BASE_CXX
#endif
