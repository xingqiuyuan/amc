// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_COAGULATION_HXX

#ifdef AMC_WITH_TEST
#define AMC_NUMERICAL_SOLVER_COAGULATION_MASS_CONSERVATION_THRESHOLD 1.e-8
#endif

namespace AMC
{
  /*! 
   * \class ClassNumericalSolverCoagulation
   */
  template<class C>
  class ClassNumericalSolverCoagulation : protected ClassNumericalSolverBase, protected ClassDynamicsCoagulation<C>
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;

  protected:

    /*!< Maximum number of time steps.*/
    static int Nt_max_;

    /*!< Whether to use semi-implicit method or not.*/
    static bool use_semi_implicit_;

    /*!< Number matrix for semi-implicit method.*/
    static Array<real, 2> A_;

    /*!< Mass matrix for semi-implicit method.*/
    static Array<real, 3> B_;

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Clear.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNtMax();

    /*!< Explicit trapezoïdal rule.*/
    static void ETR(const int section_min,
                    const int section_max,
                    const real &time_step,
                    real *concentration_aer_number,
                    real *concentration_aer_mass);

    /*!< Semi implicit.*/
    static void SemiImplicit(const real &time_step,
                             real *concentration_aer_number,
                             real *concentration_aer_mass);

#ifdef AMC_WITH_TEST
    /*!< Test coagulation dynamics.*/
    static void Test(real time_step, real time_out,
                     real *concentration_aer_number,
                     real *concentration_aer_mass,
                     const string output_file = "");
#endif
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_COAGULATION_HXX
#endif
