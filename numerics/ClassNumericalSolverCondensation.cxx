// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_CONDENSATION_CXX

#include "ClassNumericalSolverCondensation.hxx"

namespace AMC
{
  // Init.
  void ClassNumericalSolverCondensation::Init(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bmagenta() << Info() << Reset()
                         << "Instantiate Numerical Solvers for condensation." << endl;
#endif

    // Clear everything.
    ClassNumericalSolverCondensation::Clear();

    string prefix_condensation = ClassConfiguration::GetPrefix() + ".dynamic.condensation.";
    ops.SetPrefix(prefix_condensation);

    Nt_max_ = ops.Get<int>("numeric.Nt_max", "", AMC_CONDENSATION_NUMBER_TIME_STEP_MAX);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fcyan() << Debug(1) << Reset() << "Nt_max = " << Nt_max_ << endl;
#endif
  }


  // Clear.
  void ClassNumericalSolverCondensation::Clear()
  {
    Nt_max_ = AMC_CONDENSATION_NUMBER_TIME_STEP_MAX;
  }


  // Get methods.
  int ClassNumericalSolverCondensation::GetNtMax()
  {
    return Nt_max_;
  }


  // Eulerian rate.
  void ClassNumericalSolverCondensation::RateEulerian(const int section_min,
                                                      const int section_max,
                                                      const real &time_step,
                                                      const real *concentration_aer_number,
                                                      const real *concentration_aer_mass,
                                                      const real *concentration_gas,
                                                      vector<real> &rate_aer_number,
                                                      vector<real> &rate_aer_mass,
                                                      vector<real> &rate_gas)
  {
  }


  // Explicit Euler solver for condensation.
  real ClassNumericalSolverCondensation::LagrangianExplicit(const int section_min,
                                                            const int section_max,
                                                            const real &time_step,
                                                            vector<real> &rate_aer_mass,
                                                            vector<real> &rate_gas,
                                                            real *concentration_aer_number,
                                                            real *concentration_aer_mass,
                                                            real *concentration_gas,
                                                            const real ratio)
  {
    // == Assuming all variables are up to date ==.

    // Compute time scales for aerosol mass concentrations.
    vector<int> time_scale_aer_index;
    vector<real> time_scale_aer_value;
    ClassDynamicsBase::ComputeTimeScale(concentration_aer_mass,
                                        rate_aer_mass,
                                        time_scale_aer_index,
                                        time_scale_aer_value);

    // Cutoff index, depending on time step.
    int cutoff(0);
    for (cutoff = 0; cutoff < ClassAerosolData::NgNspecies_; ++cutoff)
      {
        // Stores index in time scale ascending order.
        const int &i = time_scale_aer_index[cutoff];

        if (time_scale_aer_value[i] > time_step)
          break;
      }

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_CONDENSATION
    cout << "cutoff = " << cutoff << endl;
#endif

    // Treat concentrations which have a time scale inferior to given time step.
    // Eventually modify time step (which can only be less.)
    real time_step_max(numeric_limits<real>::max());

    // Loop in the time scale ascending order.
    for (int i = 0; i < cutoff; ++i)
      {
        // Aerosol mass species concentration index.
        const int j = time_scale_aer_index[i];

        // Gas species index.
        const int k = ClassSpecies::semivolatile_reverse_[j % ClassSpecies::Nspecies_];

        // If evaporation, give everything to gas phase and nullify aerosol concentration.
        // If condensation, limit the time step with respect to the aerosol time scale.
        if (rate_aer_mass[j] < real(0))
          {
            concentration_gas[k] += concentration_aer_mass[j];
            concentration_aer_mass[j] = real(0);

            // This substract the particle species rate to the gas one.
            rate_gas[k] += rate_aer_mass[j];

            // And nullify the aerosol rate.
            rate_aer_mass[j] = real(0);
          }
        else if (rate_aer_mass[j] > real(0))
          if (concentration_aer_mass[j] > real(0))
            if (time_step_max > time_scale_aer_value[j])
              time_step_max = time_scale_aer_value[j];
      }

    // Maximum admissible time step which prevent clipping.
    time_step_max *= ratio;

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_CONDENSATION
    CBUG << "time_step_max = " << time_step_max << endl;
#endif

    // Effective time step.
    const real time_step_effective = time_step > time_step_max ? time_step_max : time_step;

#ifdef AMC_WITH_DEBUG_NUMERICAL_SOLVER_CONDENSATION
    CBUG << "time_step_effective = " << time_step_effective << endl;
#endif

    // Integrate aerosol condensing concentrations with time scales below
    // effective time step, and aerosol concentrations with time scales above it.
    for (int i = 0; i < ClassAerosolData::NgNspecies_; i++)
      concentration_aer_mass[i] += time_step_effective * rate_aer_mass[i];

    // Integrate gas concentrations.
    for (int i = 0; i < ClassSpecies::Ngas_; i++)
      concentration_gas[i] += time_step_effective * rate_gas[i];

    // Adjust concentrations after dynamics integrated.
    ClassDynamicsBase::RemoveUnstableParticle(section_min, section_max,
                                              concentration_aer_number,
                                              concentration_aer_mass,
                                              concentration_gas);

    // Finally, return effective time step.
    return time_step - time_step_effective;
  }
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_CONDENSATION_CXX
#endif
