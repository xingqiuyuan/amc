// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_NUCLEATION_HXX


namespace AMC
{
  /*! 
   * \class ClassNumericalSolverNucleation
   */
  class ClassNumericalSolverNucleation : protected ClassNumericalSolverBase, protected ClassDynamicsNucleation
  {
  public:

    typedef AMC::real real;
    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;

  public:

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    /*!< Formation rate.*/
    static void FormationRate(const int section_min,
                              const int section_max,
                              const real &time_step,
                              const real *concentration_gas,
                              vector<real> &rate_aer_number,
                              vector<real> &rate_aer_mass,
                              const int location_index = -1);
#endif
  };
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_NUCLEATION_HXX
#endif
