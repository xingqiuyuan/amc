// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_COAGULATION_CXX

#include "ClassNumericalSolverCoagulation.hxx"

namespace AMC
{
  // Init.
  template<class C>
  void ClassNumericalSolverCoagulation<C>::Init(Ops::Ops &ops)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Info() << Reset()
                         << "Instantiate Numerical Solvers for coagulation." << endl;
#endif

    // Clear everything.
    ClassNumericalSolverCoagulation<C>::Clear();

    string prefix_coagulation = ClassConfiguration::GetPrefix() + ".dynamic.coagulation.";
    ops.SetPrefix(prefix_coagulation);

    Nt_max_ = ops.Get<int>("numeric.Nt_max", "", AMC_COAGULATION_NUMBER_TIME_STEP_MAX);
    use_semi_implicit_ = ops.Get<bool>("numeric.semi_implicit", "", false);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Debug(1) << Reset() << "Nt_max = " << Nt_max_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(1) << Reset() << "Use semi-implicit method ? "
                         << (use_semi_implicit_ ? "YES" : "NO") << endl;
#endif

    if (use_semi_implicit_)
      {
        A_.resize(ClassAerosolData::Ng_, ClassAerosolData::Ng_);
        B_.resize(ClassSpecies::Nspecies_, ClassAerosolData::Ng_, ClassAerosolData::Ng_);
      }
  }


  // Clear.
  template<class C>
  void ClassNumericalSolverCoagulation<C>::Clear()
  {
    Nt_max_ = AMC_COAGULATION_NUMBER_TIME_STEP_MAX;

    if (use_semi_implicit_)
      {
        A_.free();
        B_.free();
      }
  }


  // Get methods.
  template<class C>
  int ClassNumericalSolverCoagulation<C>::GetNtMax()
  {
    return Nt_max_;
  }


  // Explicit Trapezoïdal Rule.
  template<class C>
  void ClassNumericalSolverCoagulation<C>::ETR(const int section_min,
                                               const int section_max,
                                               const real &time_step,
                                               real *concentration_aer_number,
                                               real *concentration_aer_mass)
  {
    // Assume all physico-chemical quantities have been updated.
  }


  // Semi implicit.
  template<class C>
  void ClassNumericalSolverCoagulation<C>::SemiImplicit(const real &time_step,
                                                        real *concentration_aer_number,
                                                        real *concentration_aer_mass)
  {
    if (! use_semi_implicit_)
      throw AMC::Error("Semi-implicit method not set.");

    // Assume all physico-chemical quantities have been updated.

    // Compute coagulation kernel.
    ClassDynamicsCoagulation<C>::ComputeKernel();

    // Compute coagulation gain thanks to repartition coefficients.
    /*ClassDynamicsCoagulation<C>::repartition_coefficient_->SemiImplicit(concentration_aer_number,
                                                                          concentration_aer_mass,
                                                                          time_step,
                                                                          kernel_, A_, B_);*/
    /*
    // Compute loss for number and species mass of each general section.
    for (int i = 0; i < ClassAerosolData::Ng_; ++i)
      {
        const int ii = i * (i + 1) / 2;

        real loss(real(0));
        for (int j = 0; j <= i; j++)
          loss += ClassDynamicsCoagulation<C, D>::kernel_[i][j] * concentration_aer_number[j];
        for (int j = i + 1; j < ClassAerosolData::Ng_; j++)
          loss += ClassDynamicsCoagulation<C, D>::kernel_[j][i] * concentration_aer_number[j];

        A_[ii] += real(1) + time_step * loss;

        for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          B_[j][ii] += real(1) + time_step * loss;
      }*/
  }


#ifdef AMC_WITH_TEST
  // Test coagulation dynamics.
  template<class C>
  void ClassNumericalSolverCoagulation<C>::Test(real time_step, real time_out,
                                                real *concentration_aer_number,
                                                real *concentration_aer_mass,
                                                const string output_file)
  {
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bred() << Info() << "Test coagulation dynamics with simplified"
                         << " aerosol chemistry and physics." << Reset().Str() << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "time_out = " << time_out << endl;
    *AMCLogger::GetLog() << Fmagenta() << Info(1) << Reset() << "output_file = " << output_file << endl;
#endif

#ifdef AMC_WITH_TIMER
    int time_index = AMC::AMCTimer<AMC::CPU>::Add("test_coagulation_dynamics");
    AMC::AMCTimer<AMC::CPU>::Start();
#endif

    //
    // Time loop.
    //

    real time_step_effective, time_step_remaining(real(0));
    vector1r rate_aer_number(ClassAerosolData::Ng_), rate_aer_mass(ClassAerosolData::NgNspecies_),
      mass_conservation_ref(ClassSpecies::Nspecies_), mass_conservation(ClassSpecies::Nspecies_);

    // Compute mass conservation.
    ClassAerosolData::ComputeMassConservation(NULL, concentration_aer_mass, mass_conservation_ref);

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "mass_conservation_ref = "
                         << mass_conservation_ref << endl;
#endif


    while (time_out > real(0))
      {
#ifdef AMC_WITH_DEBUG_COAGULATION
        CBUG << "time_out = " << time_out << endl;
#endif

        // Compute mass conservation.
        ClassAerosolData::ComputeMassConservation(NULL, concentration_aer_mass, mass_conservation);

        real mass_difference(real(0));
        for (int i = 0; i < ClassSpecies::Nspecies_; ++i)
          if (mass_conservation_ref[i] > real(0))
            mass_difference += abs(mass_conservation[i] - mass_conservation_ref[i]) / mass_conservation_ref[i];

        if (mass_difference > AMC_NUMERICAL_SOLVER_COAGULATION_MASS_CONSERVATION_THRESHOLD)
          throw AMC::Error("Particle mass seems not conserved in coagulation process, ref = " +
                           to_str(mass_conservation_ref) + ", mass = " + to_str(mass_conservation) + ".");

#ifdef AMC_WITH_DEBUG_COAGULATION
        CBUG << "mass_conservation = " << mass_conservation << endl;
#endif

        // Save concentrations for later use.
        if (! output_file.empty() && time_step_remaining == real(0))
          ClassAerosolData::WriteConcentration(concentration_aer_number,
                                               concentration_aer_mass,
                                               NULL, output_file);

        // Compute internal diameter and masses.
        ClassParameterization::ComputeParticleDiameter(0, ClassAerosolData::Ng_,
                                                       concentration_aer_number,
                                                       concentration_aer_mass);

        // Compute coagulation kernel.
        ClassDynamicsCoagulation<C>::ComputeKernel();

        // Compute rates.
        rate_aer_number.assign(ClassAerosolData::Ng_, real(0));
        rate_aer_mass.assign(ClassAerosolData::NgNspecies_, real(0));

        ClassDynamicsCoagulation<C>::Rate(0, ClassAerosolData::Ng_,
                                          concentration_aer_number,
                                          concentration_aer_mass,
                                          rate_aer_number,
                                          rate_aer_mass);

        // Eventually limit time step if close from end.
        time_step = time_step > time_out ? time_out : time_step;

        // Prevent clipping.
        if (time_step_remaining > real(0))
          time_step_effective = time_step_remaining;
        else
          time_step_effective = time_step;

        time_step_remaining = time_step_effective;

        for (int i = 0; i < ClassAerosolData::Ng_; ++i)
          if (concentration_aer_number[i] + time_step_effective * rate_aer_number[i] < real(0))
            {
              const real time_step_clip = - real(0.999) * concentration_aer_number[i] / rate_aer_number[i];
              if (time_step_effective > time_step_clip)
                time_step_effective = time_step_clip;
            }

        for (int i = 0; i < ClassAerosolData::NgNspecies_; ++i)
          if (concentration_aer_mass[i] + time_step * rate_aer_mass[i] < real(0))
            {
              const real time_step_clip = - real(0.999) * concentration_aer_mass[i] / rate_aer_mass[i];
              if (time_step_effective > time_step_clip)
                time_step_effective = time_step_clip;
            }

        time_step_remaining -= time_step_effective;

#ifdef AMC_WITH_DEBUG_COAGULATION
        CBUG << "time_step_effective = " << time_step_effective << ", time_step_remaining = " << time_step_remaining << endl;
#endif

        // Explicit first order time integration.
        for (int i = 0; i < ClassAerosolData::Ng_; ++i)
          concentration_aer_number[i] += time_step_effective * rate_aer_number[i];

        for (int i = 0; i < ClassAerosolData::NgNspecies_; ++i)
          concentration_aer_mass[i] += time_step_effective * rate_aer_mass[i];

        time_out -= time_step_effective;
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fyellow() << Debug(1) << Reset() << "mass_conservation = " << mass_conservation << endl;
#endif

#ifdef AMC_WITH_TIMER
    AMC::AMCTimer<AMC::CPU>::Stop(time_index);
#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Byellow() << Info(1) << Reset() << "Performed 1 simulation in " << setprecision(12)
                         << AMC::AMCTimer<AMC::CPU>::GetTimer(time_index) << " seconds." << Reset().Str() << endl;
#endif
#endif
  }
#endif
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_COAGULATION_CXX
#endif
