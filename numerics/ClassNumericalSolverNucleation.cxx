// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_NUMERICAL_SOLVER_NUCLEATION_CXX

#include "ClassNumericalSolverNucleation.hxx"

namespace AMC
{
#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
  // Formation rate.
  void ClassNumericalSolverNucleation::FormationRate(const int section_min,
                                                     const int section_max,
                                                     const real &time_step,
                                                     const real *concentration_gas,
                                                     vector<real> &rate_aer_number,
                                                     vector<real> &rate_aer_mass,
                                                     const int location_index)
  {
    const int param_index = (ClassDynamicsNucleation::parameterization_index_.size() > location_index)
      ? ClassDynamicsNucleation::parameterization_index_[location_index]
      : ClassDynamicsNucleation::parameterization_default_index_;

    // If negative, no parameterization for that location, or not at all (Nparam_ == 0).
    if (param_index < 0)
      return;

    // If not space dependent, default to first parameterization.
    // Return if section_index is not within those we want to compute.
    const int section_index = ClassDynamicsNucleation::parameterization_section_[param_index];
    if (section_index >= section_min)
      if (section_index < section_max)
        return;

    // Proxy to parameterization.
    ClassParameterizationNucleationBase* parameterization_ptr =
      ClassDynamicsNucleation::parameterization_list_[param_index];

    // This set the new particle formation model with nucleation parameterization.
    NPF::ClassNewParticleFormation<NPF::npf_parameterization_coagulation_type,
                                   NPF::npf_parameterization_condensation_type>::InitStep(parameterization_ptr);

    // Give nucleation parameterization to new particle formation model
    // in order to let particles coagulate and grow until they reach the
    // the lowest diameter bound of AMC.
    real new_particle_number(real(0));
    vector1r new_particle_mass(ClassSpecies::Nspecies_, real(0));

    if (NPF::ClassNewParticleFormation<NPF::npf_parameterization_coagulation_type,
        NPF::npf_parameterization_condensation_type>::Run(concentration_gas,
                                                          time_step,
                                                          new_particle_number,
                                                          new_particle_mass))
      {
        // Now give back nuclei rates if something happened.
        real time_step_inv = real(1) / time_step;

        // We should check that formed particles still belongs to the general
        // section to which the nucleation parameterization is attached
        // as the particle formation model may alter the composition
        // of newly formed particles.
        rate_aer_number[section_index] += new_particle_number * time_step_inv;;

        int i = section_index * ClassSpecies::Nspecies_ - 1;
        for (int j = 0; j < ClassSpecies::Nspecies_; ++j)
          rate_aer_mass[++i] += new_particle_mass[j] * time_step_inv;
      }
  }
#endif
}

#define AMC_FILE_CLASS_NUMERICAL_SOLVER_NUCLEATION_CXX
#endif
