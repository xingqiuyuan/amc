#!/usr/bin/env bash

mkdir include

cd test

ln -s ../SConstruct
ln -s ../species/species.lua
ln -s ../share/hatch.lua
ln -s ../share/color.lua
ln -s ../parameterization/flux_correction_dry.lua
ln -s ../parameterization/flux_correction_aqueous.lua
ln -s ../thermodynamics/isoropia.lua
ln -s ../thermodynamics/pankow.lua
ln -s ../thermodynamics/aec.lua
