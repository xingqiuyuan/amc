// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_EQUILIBRIUM_CXX

#include "ClassSpeciesEquilibrium.hxx"

namespace AMC
{
  // Init method.
  void ClassSpeciesEquilibrium::Init(Ops::Ops &ops)
  {
    if (ops.Exists("species.equilibrium"))
      ops.Set("species.equilibrium", "", name_);
    Nspecies_ = int(name_.size());

    molar_mass_.resize(Nspecies_);
    density_.resize(Nspecies_);
    group_.resize(Nspecies_);
    Ngroup_.assign(Nspecies_, 0);
    electric_charge_.resize(Nspecies_);
    carbon_number_.assign(Nspecies_, 0);
    is_organic_.resize(Nspecies_);
    phase_.resize(Nspecies_);

    for (int i = 0; i < Nspecies_; i++)
      {
        ops.SetPrefix("species." + name_[i] + ".");

        // Group.
        if (ops.Exists("group"))
          {
            vector1s group;

            if (ops.Is<string>("group"))
              group.push_back(ops.Get<string>("group"));
            else
              ops.Set("group", "", group);

            Ngroup_[i] = int(group.size());

            group_[i].resize(Ngroup_[i]);
            for (int j = 0; j < Ngroup_[i]; ++j)
              group_[i][j] = ClassSpecies::GetIndex(group[j]);
          }

        // Molar mass.
        if (ops.Exists("molar_mass"))
          molar_mass_[i] = ops.Get<real>("molar_mass.g_mol") * real(1e6); // Convert from g.mol^{-1} to µg.mol^{-1}.
        else if (Ngroup_[i] == 1)
          molar_mass_[i] = ClassSpecies::GetMolarMass(group_[i][0]);
        else
          throw AMC::Error("No molar mass specified for species \"" + name_[i] + "\".");

        // Density.
        if (ops.Exists("density"))
          density_[i] = ops.Get<real>("density.g_cm3")
            * real(AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3); // Convert from g.cm^{-3} to µg.µm^{-3}.
        else if (Ngroup_[i] == 1)
          density_[i] = ClassSpecies::GetDensity(group_[i][0]);
        else
          density_[i] = real(AMC_SPECIES_DENSITY_DEFAULT);

        // Electric charge.
        electric_charge_[i] = ops.Get<int>("electric_charge", "", 0);

        // Is species organic ? No by default.
        is_organic_[i] = false;
        for (int j = 0; j < Ngroup_[i]; ++j)
          is_organic_[i] = ClassSpecies::IsOrganic(group_[i][j]) || is_organic_[i];
        is_organic_[i] = ops.Get<bool>("is_organic", "", is_organic_[i]);

        // Prefered phase of species. If species not organic, much likely aqueous.
        if (ops.Exists("phase"))
          {
            if (ops.Is<int>("phase"))
              {
                phase_[i].resize(1);
                phase_[i][0] = ops.Get<int>("phase");
              }
            else
              ops.Set("phase", "", phase_[i]);
          }
        else
          {
            // By default, if ionic or not organic, assume it absorbs in an aqueous phase.
            phase_[i].resize(1);
            if (electric_charge_[i] != 0 || (! is_organic_[i]))
              phase_[i][0] = AMC_AQUEOUS_PHASE_INDEX;
            else
              phase_[i][0] = 1;
          }

        // How many carbon molecules does have the species ? If not organic, much likely 0.
        carbon_number_[i] = 0;

        if (Ngroup_[i] == 1)
          carbon_number_[i] = ClassSpecies::GetCarbonNumber(group_[i][0]);

        carbon_number_[i] = ops.Get<int>("carbon_number", "", carbon_number_[i]);

        // Store index of ionic equilibrium species.
        if (electric_charge_[i] != 0)
          ionic_.push_back(i);

        // Store index of solid equilibrium species.
        if (phase_[i][0] < 0)
          solid_.push_back(i);

        // Store index of organic species (either dissolved or ionic).
        if (is_organic_[i])
          organic_.push_back(i);
      }

    Nionic_ = int(ionic_.size());
    Nsolid_ = int(solid_.size());
    Norganic_ = int(organic_.size());

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Loaded " << Nspecies_ << " equilibrium species." << endl;
    *AMCLogger::GetLog() << Fblue() << Debug() << Reset() << "List of equilibrium species : " << name_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << Reset() << "Nionic = " << Nionic_ << ", ionic = " << ionic_ << endl;
    *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "Nsolid = " << Nsolid_ << ", solid = " << solid_ << endl;
    *AMCLogger::GetLog() << Fmagenta() << Debug(2) << Reset() << "Norganic = " << Norganic_ << ", organic = " << organic_ << endl;
#endif
  }


  // Clear method.
  void ClassSpeciesEquilibrium::Clear()
  {
    Nspecies_ = 0;
    Nionic_ = 0;
    Nsolid_ = 0;
    Norganic_ = 0;
    name_.clear();
    Ngroup_.clear();
    group_.clear();
    molar_mass_.clear();
    electric_charge_.clear();
    carbon_number_.clear();
    phase_.clear();
    density_.clear();
    is_organic_.clear();
    ionic_.clear();
    solid_.clear();
    organic_.clear();
  }


  // Get methods.
  string ClassSpeciesEquilibrium::GetName(const int &i)
  {
    return name_[i];
  }


  bool ClassSpeciesEquilibrium::IsSolid(const int &i)
  {
    return phase_[i][0] < 0;
  }


  bool ClassSpeciesEquilibrium::IsIonic(const int &i)
  {
    return electric_charge_[i] != 0;
  }


  bool ClassSpeciesEquilibrium::IsOrganic(const int &i)
  {
    return is_organic_[i];
  }


  int ClassSpeciesEquilibrium::GetElectricCharge(const int &i)
  {
    return electric_charge_[i];
  }


  int ClassSpeciesEquilibrium::GetNspecies()
  {
    return Nspecies_;
  }


  int ClassSpeciesEquilibrium::GetNionic()
  {
    return Nionic_;
  }


  int ClassSpeciesEquilibrium::GetNsolid()
  {
    return Nsolid_;
  }


  int ClassSpeciesEquilibrium::GetNorganic()
  {
    return Norganic_;
  }


  string ClassSpeciesEquilibrium::GetStr(const int &i)
  {
    ostringstream sout;

    if (i< 0 || i >= Nspecies_)
      throw AMC::Error("Wrong species index, got " + to_str(i) +
                       ", expected [0, " + to_str(Nspecies_) + "[.");

    sout << setw(30) << right << "Name : " << setw(10) << left << name_[i] << endl
         << setw(30) << right << "Group : " << left << group_[i] << endl
         << setw(30) << right << "Molar mass : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << molar_mass_[i] << "µg/mol" << endl
         << setw(30) << right << "Density : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << density_[i] << "µg/µm3" << endl
         << setw(30) << right << "Organic ? : " << setw(10) << left
         << (is_organic_[i] ? "yes" : "no") << endl
         << setw(30) << right << "Phase : " << left << phase_[i] << endl
         << setw(30) << right << "Carbon number : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << carbon_number_[i] << endl
         << setw(30) << right << "Electric charge : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
         << electric_charge_[i] << "e" << endl;

    return sout.str();
  }


  void ClassSpeciesEquilibrium::GetSolid(vector<int> &solid)
  {
    solid = solid_;
  }

  void ClassSpeciesEquilibrium::GetIonic(vector<int> &ionic)
  {
    ionic = ionic_;
  }

  void ClassSpeciesEquilibrium::GetOrganic(vector<int> &organic)
  {
    organic = organic_;
  }

  void ClassSpeciesEquilibrium::GetNgroup(vector<int> &Ngroup)
  {
    Ngroup = Ngroup_;
  }

  void ClassSpeciesEquilibrium::GetGroup(const int &i, vector<int> &group)
  {
    group = group_[i];
  }
}

#define AMC_FILE_CLASS_SPECIES_EQUILIBRIUM_CXX
#endif
