// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PHASE_CXX

#include "ClassPhase.hxx"

namespace AMC
{
  // Init.
  void ClassPhase::Init(Ops::Ops &ops)
  {
    // Index of aqueous phase. If negative there is no possible aqueous phases.
    if (ops.Get<bool>("aqueous_phase", "", true))
      aqueous_phase_index_ = AMC_AQUEOUS_PHASE_INDEX;

    // Name of phases.
    if (ops.Exists("phase.name"))
      ops.Set("phase.name", "", name_);
    else
      {
        name_.push_back("aqueous");
        name_.push_back("organic_anhydrous");
      }

    // Number of phases.
    Nphase_ = int(name_.size());

    // Fill phase vector at first relying on configuration, then to species properties.
    species_.resize(Nphase_);

    for (int i = 0; i < Nphase_; i++)
      if (ops.Exists("phase." + name_[i]))
        {
          vector1s species_name;
          ops.Set("phase." + name_[i], "", species_name);

          species_[i].resize(int(species_name.size()));
          for (int j = 0; j < int(species_name.size()); ++j)
            ClassPhase::species_[i][j] = ClassSpecies::GetIndex(species_name[j]);
        }
      else
        for (int j = 0; j < ClassSpecies::Nspecies_; j++)
          for (int k = 0; k < int(ClassSpecies::phase_[j].size()); k++)
            if (ClassSpecies::phase_[j][k] == i)
              {
                species_[i].push_back(j);
                break;
              }

    // Correct the ClassSpecies phase_ field. Avoid negative (solid phase) values.
    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (ClassSpecies::phase_[i][0] >= 0)
        {
          ClassSpecies::phase_[i].clear();
          for (int j = 0; j < Nphase_; j++)
            for (int k = 0; k < int(species_[j].size()); k++)
              if (species_[j][k] == i)
                {
                  ClassSpecies::phase_[i].push_back(j);
                  break;
                }
        }

    for (int i = 0; i < ClassSpecies::Nspecies_; i++)
      if (ClassSpecies::phase_[i].empty())
        throw AMC::Error("Species \"" + ClassSpecies::name_[i] + "\" is not given any phase.");

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Nphase = " << Nphase_ << endl;
    *AMCLogger::GetLog() << Byellow() << Debug(1) << Reset() << "index_aqueous_phase = " << aqueous_phase_index_ << endl;
    for (int i = 0; i < Nphase_; i++)
      *AMCLogger::GetLog() << Fcyan() << Debug(2) << Reset() << "List of species for phase \""
                           << name_[i] << "\" (" << i << ") : " << species_[i] << endl;
#endif
  }


  // Get methods.
  int ClassPhase::GetNphase()
  {
    return Nphase_;
  }


  string ClassPhase::GetName(const int &i)
  {
    return name_[i];
  }


  void ClassPhase::GetSpecies(const int &i, vector<int> &species)
  {
    species = species_[i];
  }


  // Clear method.
  void ClassPhase::Clear()
  {
    aqueous_phase_index_ = AMC_AQUEOUS_PHASE_INDEX;
    Nphase_ = 0;
    name_.clear();
    species_.clear();
  }
}

#define AMC_FILE_CLASS_PHASE_CXX
#endif
