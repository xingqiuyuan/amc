// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_TRACER_HXX

#define AMC_SPECIES_TRACER_EPSILON 1.e-12

namespace AMC
{
  /*! 
   * \class ClassSpeciesTracer
   */
  class ClassSpeciesTracer
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;
   
  protected:

    /*!< Number of tracers.*/
    static int Ntracer_;

    /*!< Number of real species in tracer.*/
    static vector1i Nspecies_;

    /*!< Name of tracer.*/
    static vector1s name_;

    /*!< Name of real species.*/
    static vector2s species_name_;

    /*!< Mass fractions of real species.*/
    static vector2r species_mass_fraction_;

    /*!< Molar fractions of real species.*/
    static vector2r species_molar_fraction_;

  public:

    /*!< Add one tracer.*/
#ifndef SWIG
    static void Add(const string &name, Ops::Ops &ops);
#endif

     /*!< Clear data.*/
    static void Clear();

    /*!< Compute tracer properties from real species.*/
#ifndef SWIG
    static real ComputeMolarMass(int i, Ops::Ops &ops);
    static real ComputeDensity(int i, Ops::Ops &ops);
    static int  ComputeCarbonNumber(int i, Ops::Ops &ops);
    static bool IsOrganic(int i, Ops::Ops &ops);
#endif
    static real ComputeMolarMass(int i);
    static real ComputeDensity(int i);
    static int  ComputeCarbonNumber(int i);
    static bool IsOrganic(int i);

    /*!< Get methods.*/
    static int GetNtracer();
    static int GetNspecies(const int i);
    static string GetName(const int i);
#ifndef SWIG
    static vector<string> GetSpeciesName(const int i);
#endif
    static void GetSpeciesName(const int i, vector<string> &species_name);
    static void GetSpeciesMassFraction(const int i, vector<real> &species_mass_fraction);
    static void GetSpeciesMolarFraction(const int i, vector<real> &species_molar_fraction);
    static string GetStr(const int &i);
  };
}

#define AMC_FILE_CLASS_SPECIES_TRACER_HXX
#endif
