// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_HXX

namespace AMC
{
  /*! 
   * \class ClassSpecies
   */
  class ClassSpecies
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector1r vector1r;
   
  protected:

    /*!< Number of species.*/
    static int Nspecies_;

    /*!< Number of semivolatile species.*/
    static int Ngas_;

    /*!< Semivolatile species index.*/
    static vector1i semivolatile_;

    /*!< Semivolatile reverse species index.*/
    static vector1i semivolatile_reverse_;

    /*!< Number of carbon of species.*/
    static vector1i carbon_number_;

    /*!< Is species organic ?.*/
    static vector1b is_organic_;

    /*!< Is species tracer ?.*/
    static vector1b is_tracer_;

    /*!< Vector of phases for each species :
      0 is aqueous, >= 1 is anhydrous, < 0 are solid phases.*/
    static vector2i phase_;

    /*!< Chemical species name.*/
    static vector1s name_;

    /*!< Long name.*/
    static vector1s long_name_;

    /*!< Display.*/
    static vector1s color_;
    static vector1s hatch_;
    static vector1s label_;

    /*!< Average species molar mass in µg.mol^{-1}.*/
    static real molar_mass_average_;

    /*!< Molar mass in µg.mol^{-1}.*/
    static vector1r molar_mass_;

    /*!< Pure component density in µg.µm^{-3}.*/
    static vector1r density_;

    /*!< Molar volume in m^3.mol^{-1}.*/
    static vector1r molar_volume_;

    /*!< Surface tension in N.m^{-1}.*/
    static vector1r surface_tension_;

    /*!< Sorption site in mol.cm^{-2}.*/
    static vector1r sorption_site_;

    /*!< Deliquescence relative humidity.*/
    static vector1r drh_;

    /*!< Saturation vapor pressure in Pa at T = 298K.*/
    static vector1r saturation_vapor_pressure_;

    /*!< Saturation vapor concentration in µg.m^{-3}.*/
    static vector<real> saturation_vapor_concentration_;

    /*!< Coefficients for Antoine law.*/
    static vector1r antoine_law_a_;
    static vector1r antoine_law_b_;
    static vector1b use_antoine_law_;

    /*!< Partition coefficient in m^3.\mu g^{-1} at T = 298 K.*/
    static vector1r partition_coefficient_;

    /*!< Henry's constant in mol.L^{-1}.atm^{-1} at T = 298 K.*/
    static vector1r henry_constant_;

    /*!< Vaporization enthalpy in J.mol^{-1}.*/
    static vector1r vaporization_enthalpy_;

    /*!< Accomodation coefficient, adimensional, between ]0, 1].*/
    static vector1r accomodation_coefficient_;

    /*!< Molecular diameter in Angstreum.*/
    static vector1r molecular_diameter_;

    /*!< Collision factor.*/
    static vector1r collision_factor_;

    /*!< Map of physico-chemical properties.*/
    static map<string, vector1r*> property_;

    /*!< Map of property units.*/
    static map<string, string> property_unit_;

    /*!< Function sorting the saturation vapor concentration field.*/
    static bool sort_saturation_vapor_concentration(int i, int j);

    friend void ClassParameterizationDiameterDensityFixed::ComputeDiameter(const int section_min,
                                                                           const int section_max,
                                                                           const vector1i &parameterization_section,
                                                                           const real *concentration_aer_number,
                                                                           const real *concentration_aer_mass) const;

    friend ClassParameterizationDiameterDensityMoving::ClassParameterizationDiameterDensityMoving(Ops::Ops &ops);

    friend void ClassParameterizationDiameterDensityMoving::ComputeDiameter(const int section_min,
                                                                            const int section_max,
                                                                            const vector1i &parameterization_section,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

#ifdef AMC_WITH_GERBER
    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const vector1i &parameterization_section,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;

    friend void ClassParameterizationDiameterGerber::ComputeDiameter(const int section_min,
                                                                     const int section_max,
                                                                     const real *concentration_aer_number,
                                                                     const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_FRACTAL_SOOT
    friend void ClassParameterizationDiameterFractalSoot::ComputeDiameter(const int section_min,
                                                                          const int section_max,
                                                                          const vector1i &parameterization_section,
                                                                          const real *concentration_aer_number,
                                                                          const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_SURFACE_TENSION
    friend ClassParameterizationSurfaceTensionAverage::ClassParameterizationSurfaceTensionAverage(Ops::Ops &ops, const string name);

    friend void ClassParameterizationSurfaceTensionAverage::ComputeSurfaceTension(const int &section_min,
                                                                                  const int &section_max,
                                                                                  const vector1i &parameterization_section,
                                                                                  const real *concentration_aer_number,
                                                                                  const real *concentration_aer_mass) const;

#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
    friend void ClassParameterizationSurfaceTensionJacobson::ComputeSurfaceTension(const int &section_min,
                                                                                   const int &section_max,
                                                                                   const vector1i &parameterization_section,
                                                                                   const real *concentration_aer_number,
                                                                                   const real *concentration_aer_mass) const;
#endif
#endif

#ifdef AMC_WITH_ISOROPIA
    friend ClassModelThermodynamicIsoropia::ClassModelThermodynamicIsoropia(Ops::Ops &ops, const bool standalone);
#endif

#ifdef AMC_WITH_PANKOW
    friend ClassModelThermodynamicPankow::ClassModelThermodynamicPankow(Ops::Ops &ops, const bool standalone);

    friend void ClassModelThermodynamicPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &section_index,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicPankow::ComputeGlobalEquilibrium(const int &section_min,
                                                                        const int &section_max,
                                                                        const real *concentration_gas,
                                                                        const real *concentration_aer_mass,
                                                                        vector1r &concentration_aer_total,
                                                                        vector1r &equilibrium_aer_internal,
                                                                        real &liquid_water_content,
                                                                        vector1r &concentration_aer_equilibrium,
                                                                        vector1r &concentration_gas_equilibrium) const;
#endif

#ifdef AMC_WITH_AEC
    friend ClassModelThermodynamicAEC::ClassModelThermodynamicAEC(Ops::Ops &ops, const bool standalone, const string name);

    friend void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                     const int &section_max,
                                                                     const real *concentration_gas,
                                                                     const real *concentration_aer_mass,
                                                                     vector1r &concentration_aer_total,
                                                                     vector1r &equilibrium_aer_internal,
                                                                     real &liquid_water_content,
                                                                     vector1r &concentration_aer_equilibrium,
                                                                     vector1r &concentration_gas_equilibrium) const;
#ifdef AMC_WITH_TEST
    friend void ClassModelThermodynamicAEC::TestAMC(const real &temperature,
                                                    const real &relative_humidity,
                                                    const vector<real> &concentration_aer_mass,
                                                    vector<real> &equilibrium_aer_internal,
                                                    vector<real> &equilibrium_gas_surface) const;
#endif
#endif

#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
    friend void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                            const int &section_max,
                                                                            const vector1i &section_index,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropiaAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                             const int &section_max,
                                                                             const real *concentration_gas,
                                                                             const real *concentration_aer_mass,
                                                                             vector1r &concentration_aer_total,
                                                                             vector1r &equilibrium_aer_internal,
                                                                             real &liquid_water_content,
                                                                             vector1r &concentration_aer_equilibrium,
                                                                             vector1r &concentration_gas_equilibrium) const;

#ifdef AMC_WITH_TEST
    friend void ClassModelThermodynamicIsoropiaAEC::TestAMC(const real &temperature,
                                                            const real &relative_humidity,
                                                            const vector<real> &concentration_aer_mass,
                                                            vector<real> &equilibrium_aer_internal,
                                                            vector<real> &equilibrium_gas_surface) const;
#endif
#endif


    /*!< Condensation parameterizations are my friends.*/
#ifdef AMC_WITH_CONDENSATION
    friend void ClassParameterizationCondensationVoid::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;

#ifdef AMC_WITH_TEST
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    _TestCoefficient_(const real &temperature,
                      const real &pressure,
                      const real &diameter,
                      vector<real> &condensation_coefficient) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    _Test_(const real &temperature,
           const real &pressure,
           const real &diameter,
           const real &density,
           const vector<real> &concentration_gas,
           const vector<real> &concentration_aer_mass,
           const vector<real> &equilibrium_aer_internal,
           vector<real> &equilibrium_gas_surface,
           vector<real> &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;
#endif

#ifdef AMC_WITH_KELVIN_EFFECT
    friend ClassParameterizationKelvinEffect::ClassParameterizationKelvinEffect(Ops::Ops& ops);

    friend void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                                       const int section_max,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass,
                                                                       vector1r &kelvin_effect) const;

#ifdef AMC_WITH_TEST
    friend void ClassParameterizationKelvinEffect::Test(const real &temperature,
                                                        const real &diameter,
                                                        const real &density,
                                                        const real &liquid_water_content,
                                                        const vector<real> &surface_tension,
                                                        const vector<real> &concentration_aer_mass,
                                                        vector<real> &kelvin_effect) const;
#endif
#endif

    friend ClassCondensationFluxCorrectionAqueousBase::ClassCondensationFluxCorrectionAqueousBase(Ops::Ops &ops, const string name);

    friend ClassCondensationFluxCorrectionDryInorganicSalt::
    ClassCondensationFluxCorrectionDryInorganicSalt(Ops::Ops &ops);
#endif

#ifdef AMC_WITH_NUCLEATION
    friend void ClassParameterizationNucleationBase::set_index_amc(const string name);

#ifdef AMC_WITH_POWER_LAW
    friend ClassParameterizationNucleationPowerLaw::ClassParameterizationNucleationPowerLaw(Ops::Ops &ops);

    friend void ClassParameterizationNucleationPowerLaw::ComputeKernel(const int &section_index,
                                                                       const real *concentration_gas,
                                                                       vector1r &rate_aer_number,
                                                                       vector1r &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_VEHKAMAKI
    friend void ClassParameterizationNucleationVehkamaki::ComputeKernel(const int &section_index,
                                                                        const real *concentration_gas,
                                                                        vector1r &rate_aer_number,
                                                                        vector1r &rate_aer_mass) const;
#endif

#ifdef AMC_WITH_MERIKANTO
    friend void ClassParameterizationNucleationMerikanto::ComputeKernel(const int &section_index,
                                                                        const real *concentration_gas,
                                                                        vector1r &rate_aer_number,
                                                                        vector1r &rate_aer_mass) const;
#endif
#endif

#ifdef AMC_WITH_ADSORPTION
    friend void ClassModelAdsorptionPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_NEW_PARTICLE_FORMATION
    friend void NPF::ClassSpecies::Init(Ops::Ops &ops);
#endif

  public:

    /*!< Init data.*/
    static void Init(Ops::Ops &ops);

     /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static int GetNspecies();
    static int GetNgas();
    static string GetName(const int &i);
    static string GetLongName(const int &i);
    static vector<string> GetNameList();
    static vector<string> GetLongNameList();
    static string GetStr(const int &i);
#ifndef SWIG
    static vector<int> GetSemivolatile();
#endif
    static void GetSemivolatile(vector<int> &semivolatile);
    static void GetNotSemivolatile(vector<int> &not_semivolatile);
    static real GetDensity(const int &i);
    static real GetMolarMass(const int &i);
    static real GetCollisionFactor(const int &i);
    static real GetMolecularDiameter(const int &i);
    static int GetCarbonNumber(const int &i);

    static real GetProperty(const string &name, const int i);
    static string GetPropertyUnit(const string &name);
    static vector<string> GetPropertyList();
    static vector<real> GetProperty(const string &name);

    static int GetIndex(const string &name, const bool with_long_name = false);
    static int GetIndexGas(const string &name);
    static int GetIndexGas(const int &i);
    static bool IsOrganic(const int &i);
    static bool IsTracer(const int &i);
    static bool IsSemivolatile(const int &i);
    static void GetPhase(const int &i, vector<int> &phase);

    /*!< Display.*/
    static string GetColor(const int &i);
    static string GetHatch(const int &i);
    static string GetLabel(const int &i);

    /*!< Get sorted saturation vapor concentration.*/
    static void GetSortedSaturationVaporConcentration(vector<int> &index_ascending,
                                                      vector<real> &saturation_vapor_concentration);

    /*!< Compute saturation vapor pressure from Antoine Law (if exists).*/
    static real ComputeSaturationVaporPressureFromAntoineLaw(const int i);

    /*!< Compute OM to OC ratio, only relevant if species is organic.*/
    static real ComputeRatioOMtoOC(const int i);

    /*!< Display table of properties.*/
    static string DisplayPropertyTable(vector<string> name);

    /*!< Generate random composition.*/
#ifndef SWIG
    static void GenerateRandomComposition(vector1r &composition);
#endif
    static vector<real> GenerateRandomComposition();
  };
}

#define AMC_FILE_CLASS_SPECIES_HXX
#endif
