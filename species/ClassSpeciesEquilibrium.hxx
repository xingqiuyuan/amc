// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_EQUILIBRIUM_HXX

namespace AMC
{
  /*! 
   * \class ClassSpeciesEquilibrium
   */
  class ClassSpeciesEquilibrium
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;
    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;
    typedef typename AMC::vector1r vector1r;
   
  protected:

    /*!< Number of equilibrium species.*/
    static int Nspecies_;

    /*!< Number of solid among them.*/
    static int Nsolid_;

    /*!< Number of ionic among them.*/
    static int Nionic_;

    /*!< Number of organic among them.*/
    static int Norganic_;

    /*!< Chemical species name.*/
    static vector1s name_;

    /*!< Number of group for each species.*/
    static vector1i Ngroup_;

    /*!< Species group.*/
    static vector2i group_;

    /*!< Number of carbon of species.*/
    static vector1i carbon_number_;

    /*!< Is species organic ?.*/
    static vector1b is_organic_;

    /*!< Vector of phases for each species :
      0 is aqueous, >= 1 is anhydrous, < 0 are solid phases.*/
    static vector2i phase_;

    /*!< Molar mass in µg.mol^{-1}.*/
    static vector1r molar_mass_;

    /*!< Pure component density in µg.µm^{-3}.*/
    static vector1r density_;

    /*!< Electric charge.*/
    static vector1i electric_charge_;

    /*!< List of ionic species.*/
    static vector1i ionic_;

    /*!< List of solid species.*/
    static vector1i solid_;

    /*!< List of organic species.*/
    static vector1i organic_;

    /*!< Thermodynamic methods are my friends.*/
#ifdef AMC_WITH_ISOROPIA
    friend ClassModelThermodynamicIsoropia::ClassModelThermodynamicIsoropia(Ops::Ops &ops, const bool standalone);

    friend void ClassModelThermodynamicIsoropia::ComputeLocalEquilibrium(const int &section_min,
                                                                         const int &section_max,
                                                                         const vector1i &section_index,
                                                                         const real *concentration_aer_number,
                                                                         const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_PANKOW
    friend void ClassModelThermodynamicPankow::ComputeLocalEquilibrium(const int &section_min,
                                                                       const int &section_max,
                                                                       const vector1i &section_index,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_AEC
    friend ClassModelThermodynamicAEC::ClassModelThermodynamicAEC(Ops::Ops &ops, const bool standalone, const string name);

    friend void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;
#endif

#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
    friend void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                            const int &section_max,
                                                                            const vector1i &section_index,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;

    friend void ClassModelThermodynamicIsoropiaAEC::ComputeGlobalEquilibrium(const int &section_min,
                                                                             const int &section_max,
                                                                             const real *concentration_gas,
                                                                             const real *concentration_aer_mass,
                                                                             vector1r &concentration_aer_total,
                                                                             vector1r &equilibrium_aer_internal,
                                                                             real &liquid_water_content,
                                                                             vector1r &concentration_aer_equilibrium,
                                                                             vector1r &concentration_gas_equilibrium) const;
#endif

    /*!< Condensation parameterizations are my friends.*/
#ifdef AMC_WITH_CONDENSATION
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimited<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

#ifdef AMC_WITH_DIFFUSION_LIMITED_SOOT
    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeKernel(const int section_min,
                  const int section_max,
                  const vector1i &parameterization_section,
                  const real *concentration_aer_number,
                  const real *concentration_aer_mass,
                  const real *concentration_gas,
                  vector1r &condensation_coefficient,
                  vector1r &rate_aer_mass) const;

    template<class FC, class FW, class FD>
    friend void ClassParameterizationCondensationDiffusionLimitedSoot<FC, FW, FD>::
    ComputeCoefficient(const int section_min,
                       const int section_max,
                       const vector1i &parameterization_section,
                       const real *concentration_aer_number,
                       vector1r &condensation_coefficient) const;
#endif

    friend ClassCondensationFluxCorrectionAqueousBase::ClassCondensationFluxCorrectionAqueousBase(Ops::Ops &ops, const string name);

    friend ClassCondensationFluxCorrectionDryInorganicSalt::
    ClassCondensationFluxCorrectionDryInorganicSalt(Ops::Ops &ops);

#ifdef AMC_WITH_KELVIN_EFFECT
    friend void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                                       const int section_max,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass,
                                                                       vector1r &kelvin_effect) const;
#endif
#endif

  public:

    /*!< Init data.*/
    static void Init(Ops::Ops &ops);

     /*!< Clear data.*/
    static void Clear();

    /*!< Get methods.*/
    static string GetName(const int &i);
    static bool IsSolid(const int &i);
    static bool IsIonic(const int &i);
    static bool IsOrganic(const int &i);
    static int GetElectricCharge(const int &i);
    static int GetNspecies();
    static int GetNionic();
    static int GetNsolid();
    static int GetNorganic();
    static string GetStr(const int &i);
    static void GetSolid(vector<int> &solid);
    static void GetIonic(vector<int> &ionic);
    static void GetOrganic(vector<int> &organic);
    static void GetNgroup(vector<int> &Ngroup);
    static void GetGroup(const int &i, vector<int> &group);
  };
}

#define AMC_FILE_CLASS_SPECIES_EQUILIBRIUM_HXX
#endif
