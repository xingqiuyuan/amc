// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_TRACER_CXX

#include "ClassSpeciesTracer.hxx"

namespace AMC
{
  // Add one tracer.
  void ClassSpeciesTracer::Add(const string &name, Ops::Ops &ops)
  {
    name_.push_back(name);

    species_name_.push_back(vector1s());
    ops.Set("species", "", species_name_.back());
    Nspecies_.push_back(int(species_name_.back().size()));

    // Retrieve molar mass.
    vector1r species_molar_mass(Nspecies_.back(), real(0));
    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix("species.");

    for (int i = 0; i < Nspecies_.back(); ++i)
      if (ops.Exists(species_name_.back()[i] + ".molar_mass"))
        species_molar_mass[i] = ops.Get<real>(species_name_.back()[i] + ".molar_mass.g_mol") * real(1e6);
      else
        throw AMC::Error("No molar mass specified for species \"" + species_name_.back()[i] + "\".");

    ops.SetPrefix(prefix_orig);

    // Mass and molar fractions.
    species_mass_fraction_.push_back(vector1r(Nspecies_.back(), real(0)));
    species_molar_fraction_.push_back(vector1r(Nspecies_.back(), real(0)));

    if (ops.Exists("mass_fraction"))
      {
        ops.Set("mass_fraction", "", species_mass_fraction_.back());

        // Compute molar fraction from mass ones.
        real molar_total_inv(real(0));

        for (int i = 0; i < Nspecies_.back(); ++i)
          {
            species_molar_fraction_.back()[i] = species_mass_fraction_.back()[i] / species_molar_mass[i];
            molar_total_inv += species_molar_fraction_.back()[i];
          }

        molar_total_inv = real(1) / molar_total_inv;
        for (int i = 0; i < Nspecies_.back(); ++i)
          species_molar_fraction_.back()[i] *= molar_total_inv;
      }
    else if (ops.Exists("molar_fraction"))
      {
        ops.Set("molar_fraction", "", species_molar_fraction_.back());

        // Compute mass fraction from molar ones.
        real mass_total_inv(real(0));

        for (int i = 0; i < Nspecies_.back(); ++i)
          {
            species_mass_fraction_.back()[i] = species_molar_fraction_.back()[i] * species_molar_mass[i];
            mass_total_inv += species_mass_fraction_.back()[i];
          }

        mass_total_inv = real(1) / mass_total_inv;
        for (int i = 0; i < Nspecies_.back(); ++i)
          species_mass_fraction_.back()[i] *= mass_total_inv;
      }
    else
      throw AMC::Error("Neither \"mass_fraction\" nor \"molar_fraction\" specified for tracer \"" + name + "\".");

    // Check sum is unity for molar and mass fractions.
    if (abs(compute_vector_sum(species_mass_fraction_.back()) - real(1)) > real(AMC_SPECIES_TRACER_EPSILON))
      throw AMC::Error("For tracer \"" + name + "\", sum of mass fractions is != 1");

    if (abs(compute_vector_sum(species_molar_fraction_.back()) - real(1)) > real(AMC_SPECIES_TRACER_EPSILON))
      throw AMC::Error("For tracer \"" + name + "\", sum of molar fractions is != 1");

    // Increment number of tracers.
    Ntracer_++;

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Bblue() << Debug(1) << Reset() << "Added tracer \"" << name << "\" :" << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << Reset() << "\tspecies_name = " << species_name_.back() << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << Reset() << "\tspecies_mass_fraction = "
                         << species_mass_fraction_.back() << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(2) << Reset() << "\tspecies_molar_fraction = "
                         << species_molar_fraction_.back() << endl;
    *AMCLogger::GetLog() << Bgreen() << Debug(3) << Reset() << "\tNtracer = " << Ntracer_ << endl;
#endif
  }


  // Clear method.
  void ClassSpeciesTracer::Clear()
  {
    Ntracer_ = 0;
    name_.clear();
    Nspecies_.clear();
    species_name_.clear();
    species_mass_fraction_.clear();
    species_molar_fraction_.clear();
  }


  // Compute tracer properties from real species.
  real ClassSpeciesTracer::ComputeMolarMass(int i, Ops::Ops &ops)
  {
    i = i < 0 ? Ntracer_ + i : i;

    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix("species.");

    real molar_mass_inv(real(0));
    for (int j = 0; j < Nspecies_[i]; ++j)
      if (ops.Exists(species_name_.back()[i] + ".molar_mass"))
        molar_mass_inv += species_mass_fraction_[i][j]
          / (ops.Get<real>(species_name_.back()[i] + ".molar_mass.g_mol") * real(1e6));
      else
        throw AMC::Error("No molar mass specified for species \"" + species_name_.back()[i] + "\".");

    ops.SetPrefix(prefix_orig);

    return real(1) / molar_mass_inv;
  }


  real ClassSpeciesTracer::ComputeDensity(int i, Ops::Ops &ops)
  {
    i = i < 0 ? Ntracer_ + i : i;

    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix("species.");

    real density_inv(real(0));
    for (int j = 0; j < Nspecies_[i]; ++j)
      if (ops.Exists(species_name_[i][j] + ".density"))
        density_inv += species_mass_fraction_[i][j]
          / (ops.Get<real>(species_name_[i][j] + ".density.g_cm3") * real(AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3));
      else
        throw AMC::Error("No density specified for species \"" + species_name_[i][j] + "\".");

    ops.SetPrefix(prefix_orig);

    return real(1) / density_inv;
  }


  // Compute tracer properties from real species.
  int ClassSpeciesTracer::ComputeCarbonNumber(int i, Ops::Ops &ops)
  {
    i = i < 0 ? Ntracer_ + i : i;

    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix("species.");

    real carbon_number(real(0));
    for (int j = 0; j < Nspecies_[i]; ++j)
      carbon_number += species_molar_fraction_[i][j] * ops.Get<int>(species_name_[i][j] + ".carbon_number", "", 0);

    ops.SetPrefix(prefix_orig);

    return int(carbon_number);
  }


  bool ClassSpeciesTracer::IsOrganic(int i, Ops::Ops &ops)
  {
    i = i < 0 ? Ntracer_ + i : i;

    string prefix_orig = ops.GetPrefix();
    ops.SetPrefix("species.");

    real is_organic(false);
    for (int j = 0; j < Nspecies_[i]; ++j)
      is_organic = is_organic || ops.Get<bool>(species_name_[i][j] + ".is_organic", "", true);

    ops.SetPrefix(prefix_orig);

    return is_organic;
  }


  real ClassSpeciesTracer::ComputeMolarMass(int i)
  {
    Ops::Ops ops(ClassConfiguration::GetFile());
    real molar_mass = ComputeMolarMass(i, ops);
    ops.Close();

    return molar_mass;
  }


  real ClassSpeciesTracer::ComputeDensity(int i)
  {
    Ops::Ops ops(ClassConfiguration::GetFile());
    real density = ComputeDensity(i, ops);
    ops.Close();

    return density;
  }


  int ClassSpeciesTracer::ComputeCarbonNumber(int i)
  {
    Ops::Ops ops(ClassConfiguration::GetFile());
    int carbon_number = ComputeCarbonNumber(i, ops);
    ops.Close();

    return carbon_number;
  }


  bool ClassSpeciesTracer::IsOrganic(int i)
  {
    Ops::Ops ops(ClassConfiguration::GetFile());
    bool is_organic = IsOrganic(i, ops);
    ops.Close();

    return is_organic;
  }


  // Get methods.
  int ClassSpeciesTracer::GetNtracer()
  {
    return Ntracer_;
  }


  int ClassSpeciesTracer::GetNspecies(const int i)
  {
    return Nspecies_[i];
  }


  string ClassSpeciesTracer::GetName(const int i)
  {
    return name_[i];
  }


  vector<string> ClassSpeciesTracer::GetSpeciesName(const int i)
  {
    return species_name_[i];
  }

  void ClassSpeciesTracer::GetSpeciesName(const int i, vector<string> &species_name)
  {
    species_name = species_name_[i];
  }


  void ClassSpeciesTracer::GetSpeciesMassFraction(const int i, vector<real> &species_mass_fraction)
  {
    species_mass_fraction = species_mass_fraction_[i];
  }


  void ClassSpeciesTracer::GetSpeciesMolarFraction(const int i, vector<real> &species_molar_fraction)
  {
    species_molar_fraction = species_molar_fraction_[i];
  }


  string ClassSpeciesTracer::GetStr(const int &i)
  {
    ostringstream sout;

    if (i< 0 || i >= Ntracer_)
      throw AMC::Error("Wrong species index, got " + to_str(i) +
                       ", expected [0, " + to_str(Ntracer_) + "[.");

    sout << setw(30) << right << "Tracer : " << setw(10) << left << name_[i] << endl
         << setw(30) << right << "Species : " << left << species_name_[i] << endl
         << setw(30) << right << "Mass fraction : " << left << species_mass_fraction_[i] << endl
         << setw(30) << right << "Molar fraction : " << left << species_molar_fraction_[i] << endl;

    return sout.str();
  }
}

#define AMC_FILE_CLASS_SPECIES_TRACER_CXX
#endif
