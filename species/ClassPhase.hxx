// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_PHASE_HXX


namespace AMC
{
  /*! 
   * \class ClassPhase
   * \brief Class defining the various aerosol phases (aqueous, anhydrous ans solids).
   */
  class ClassPhase : protected ClassSpecies, protected ClassSpeciesEquilibrium
  {
  public:
    typedef AMC::real real;

    typedef typename AMC::vector1b vector1b;
    typedef typename AMC::vector1s vector1s;

    typedef typename AMC::vector1i vector1i;
    typedef typename AMC::vector2i vector2i;

    typedef typename AMC::vector1r vector1r;
    typedef typename AMC::vector2r vector2r;
   
  protected:

    /*!< Number of possible phases in particle.*/
    static int aqueous_phase_index_;

    /*!< Number of possible phases in particle.*/
    static int Nphase_;

    /*!< Name of each phase.*/
    static vector1s name_;

    /*!< Index of species which have one unique phase,
      last vector contains species which may have several phases.*/
    static vector2i species_;

#ifdef AMC_WITH_AEC
    friend void ClassModelThermodynamicAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                    const int &section_max,
                                                                    const vector1i &section_index,
                                                                    const real *concentration_aer_number,
                                                                    const real *concentration_aer_mass) const;
#endif

#if defined(AMC_WITH_ISOROPIA) && defined(AMC_WITH_AEC)
    friend void ClassModelThermodynamicIsoropiaAEC::ComputeLocalEquilibrium(const int &section_min,
                                                                            const int &section_max,
                                                                            const vector1i &section_index,
                                                                            const real *concentration_aer_number,
                                                                            const real *concentration_aer_mass) const;
#endif

#ifdef AMC_WITH_SURFACE_TENSION
    friend ClassParameterizationSurfaceTensionFixed::ClassParameterizationSurfaceTensionFixed(Ops::Ops &ops, const int &Ng);

    friend void ClassParameterizationSurfaceTensionFixed::ComputeSurfaceTension(const int &section_min,
                                                                                const int &section_max,
                                                                                const vector1i &parameterization_section,
                                                                                const real *concentration_aer_number,
                                                                                const real *concentration_aer_mass) const;

    friend void ClassParameterizationSurfaceTensionAverage::ComputeSurfaceTension(const int &section_min,
                                                                                  const int &section_max,
                                                                                  const vector1i &parameterization_section,
                                                                                  const real *concentration_aer_number,
                                                                                  const real *concentration_aer_mass) const;

#ifdef AMC_WITH_SURFACE_TENSION_JACOBSON
    friend ClassParameterizationSurfaceTensionJacobson::ClassParameterizationSurfaceTensionJacobson(Ops::Ops &ops);

    friend void ClassParameterizationSurfaceTensionJacobson::ComputeSurfaceTension(const int &section_min,
                                                                                   const int &section_max,
                                                                                   const vector1i &parameterization_section,
                                                                                   const real *concentration_aer_number,
                                                                                   const real *concentration_aer_mass) const;
#endif
#endif

#ifdef AMC_WITH_KELVIN_EFFECT
    friend void ClassParameterizationKelvinEffect::ComputeKelvinEffect(const int section_min,
                                                                       const int section_max,
                                                                       const real *concentration_aer_number,
                                                                       const real *concentration_aer_mass,
                                                                       vector1r &kelvin_effect) const;

#ifdef AMC_WITH_TEST
    friend void ClassParameterizationKelvinEffect::Test(const real &temperature,
                                                        const real &diameter,
                                                        const real &density,
                                                        const real &liquid_water_content,
                                                        const vector<real> &surface_tension,
                                                        const vector<real> &concentration_aer_mass,
                                                        vector<real> &kelvin_effect) const;
#endif
#endif

  public:

    /*!< Init.*/
    static void Init(Ops::Ops &ops);

    /*!< Get methods.*/
    static int GetNphase();
    static string GetName(const int &i);
    static void GetSpecies(const int &i, vector<int> &species);

    /*!< Clear method.*/
    static void Clear();
  };
}

#define AMC_FILE_CLASS_PHASE_HXX
#endif
