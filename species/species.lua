--
-- System wide definition of species.
--


species = {

   -- Water is hardcoded in AMC through the liquid water content field.

   --
   -- Inorganic species.
   --

   H2SO4 = {
      name = "sulfuric acid",
      is_organic = false,
      molar_mass = {g_mol = 98.0},
      density = {g_cm3 = 1.84},
      surface_tension = {N_m = 5.51e-2},
      molecular_diameter = {angstrom = 5.5},
      collision_factor = 77.3,
      display = {color = steelblue}
      --saturation_vapor_pressure = {Pa = 3.3e-3}
   },

   NH3 = {
      name = "ammonia",
      is_organic = false,
      molar_mass = {g_mol = 17.0},
      density = {g_cm3 = 0.91},
      surface_tension = {N_m = 5.705e-2},
      molecular_diameter = {angstrom = 2.9},
      collision_factor = 558.3,
      display = {color = indigo}
   },

   HNO3 = {
      name = "nitric acid",
      is_organic = false,
      molar_mass = {g_mol = 63.0},
      density = {g_cm3 = 1.5},
      surface_tension = {N_m = 5.87e-2},
      molecular_diameter = {angstrom = 3.3},
      collision_factor = 475.9,
      display = {color = lightblue}
   },

   HCl = {
      name = "chloridric acid",
      is_organic = false,
      molar_mass = {g_mol = 36.5},
      density = {g_cm3 = 1.15},
      surface_tension = {N_m = 7.e-2},
      molecular_diameter = {angstrom = 3.339},
      collision_factor = 344.7,
      display = {color = navy}
   },

   Na = {
      name = "sodium",
      is_organic = false,
      molar_mass = {g_mol = 23.0},
      density = {g_cm3 = 0.97},
      display = {color = silver}
   },

   --
   -- Dust species.
   --

   SiO2 = {
      name = "quartz",
      is_organic = false,
      molar_mass = {g_mol = 60.},
      density = {g_cm3 = 2.65}
   },

   Al2O3 = {
      name = "alumine",
      is_organic = false,
      molar_mass = {g_mol = 102.},
      density = {g_cm3 = 3.97}
   },

   CaO = {
      name = "calcium oxide, a.k.a. chaux vive",
      is_organic = false,
      molar_mass = {g_mol = 56.},
      density = {g_cm3 = 3.35}
   },

   Fe2O3 = {
      name = "ferric oxide",
      is_organic = false,
      molar_mass = {g_mol = 160.},
      density = {g_cm3 = 5.24}
   },

   MgO = {
      name = "magnesie",
      is_organic = false,
      molar_mass = {g_mol = 40.},
      density = {g_cm3 = 3.58}
   },

   Na2O = {
      name = "sodium oxide",
      is_organic = false,
      molar_mass = {g_mol = 62.},
      density = {g_cm3 = 2.27}
   },

   K2O = {
      name = "potassium oxide",
      is_organic = false,
      molar_mass = {g_mol = 94.2},
      density = {g_cm3 = 2.35}
   },

   DUST = {
      name = "dust (Jeong, 2008)",
      is_tracer = true,
      phase = -1,
      species = {"SiO2", "Al2O3", "CaO", "Fe2O3", "MgO", "Na2O", "K2O"},
      mass_fraction = {0.57, 0.17, 0.09, 0.07, 0.04, 0.03, 0.03},
      display = {color = ground, label = "dust"}
   },

   --
   -- Tracer for non-volatile primary organic species.
   --

   STER = {
      name = "steric acid",
      carbon_number = 18,
      molar_mass = {g_mol = 284.5},
      density = {g_cm3 = 0.9},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 3.34e-2},
      vaporization_enthalpy = {J_mol = 63.8e3},
      saturation_vapor_pressure = {mmHg = 1.96e-6},
   },

   ALK20 = {
      name = "icosane",
      carbon_number = 20,
      molar_mass = {g_mol = 282.5},
      density = {g_cm3 = 0.8},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 2.84e-2},
      vaporization_enthalpy = {J_mol = 56.4e3},
      saturation_vapor_pressure = {mmHg = 6.04e-6}
   },

   SYR = {
      name = "syringol",
      carbon_number = 8,
      molar_mass = {g_mol = 154.1},
      density = {g_cm3 = 1.2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 3.72e-2},
      vaporization_enthalpy = {J_mol = 52.3e3},
      henry_constant = {atm_m3_mol = 1.96e-9},
      saturation_vapor_pressure = {mmHg = 6.02e-3}
   },

   LIGN = {
      name = "lignoceric acid",
      carbon_number = 24,
      molar_mass = {g_mol = 368.6},
      density = {g_cm3 = 0.9},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 3.35e-2},
      vaporization_enthalpy = {J_mol = 69.3e3},
      saturation_vapor_pressure = {mmHg = 4.47e-7}
   },

   DIPHEN = {
      name = "diphenic acid (aromatic dicarbonxylic acid)",
      carbon_number = 14,
      molar_mass = {g_mol = 242.2},
      density = {g_cm3 = 1.3},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 6.22e-2},
      vaporization_enthalpy = {J_mol = 71.4e3},
      henry_constant = {atm_m3_mol = 1.68e-13},
      saturation_vapor_pressure = {mmHg = 5.03e-7}
   },

   POA = {
      name = "non-volatile primary organics",
      is_tracer = true,
      species = {"STER", "ALK20", "SYR", "LIGN", "DIPHEN"},
      mass_fraction = {0.05, 0.17, 0.11, 0.5, 0.17},
      display = {color = gold, label = "POA"}
   },

   --
   -- Elemental carbon. Not BC.
   --

   EC = {
      name = "elemental carbon",
      is_organic = false,
      phase = -1,
      carbon_number = 1,
      molar_mass = {g_mol = 12.0},
      density = {g_cm3 = 2.25},
      display = {color = darkgray, label = "EC"}
   },

   --
   -- Some semivolatile organic species used as surrogates in 3D modeling.
   --

   BiA2D = {
      name = "pinic acid",
      carbon_number = 9,
      phase = {0, 1},
      molar_mass = {g_mol = 186.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 4.46e-2},
      drh = 0.79,
      vaporization_enthalpy = {J_mol = 109.e3},
      saturation_vapor_pressure = {Pa = 1.9e-5, ug_m3 = 1.43, torr = 1.43e-7},
      henry_constant = {m3_ug = 6.25e-3},
      display = {color = aquamarine}
   },

   BiA1D = {
      name = "nor-pinonic acid",
      carbon_number = 10,
      phase = {0, 1},
      molar_mass = {g_mol = 170.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 88.e3},
      saturation_vapor_pressure = {Pa = 2.89e-5, ug_m3 = 1.98, torr = 2.17e-7},
      henry_constant = {m3_ug = 2.73e-3},
      display = {color = greenyellow}
   },

   BiA0D = {
      name = "C10 oxo aldehyde",
      carbon_number = 10,
      phase = {0, 1},
      molar_mass = {g_mol = 168.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 88.e3},
      saturation_vapor_pressure = {Pa = 3.6e-2, ug_m3 = 2.44e3},
      henry_constant = {m3_ug = 4.82e-5},
      display = {color = lime}
  },

   GLY = {
      name = "glyoxal",
      carbon_number = 2,
      phase = {0, 1},
      molar_mass = {g_mol = 58.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.1},
      surface_tension = {N_m = 2.82e-2},
      vaporization_enthalpy = {J_mol = 25.e3},
      saturation_vapor_pressure = {Pa = 29.3e3},
      saturation_vapor_concentration = 6.86e8,
      henry_constant = {m3_ug = 6.56e-4},
      display = {color = cadetblue}
   },

   MGLY = {
      name = "methyl-glyoxal",
      carbon_number = 3,
      phase = {0, 1},
      molar_mass = {g_mol = 72.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.046},
      surface_tension = {N_m = 2.71e-2},
      vaporization_enthalpy = {J_mol = 38.e3},
      saturation_vapor_pressure = {Pa = 29.3e3, ug_m3 = 8.51e8},
      henry_constant = {m3_ug = 5.78e-12},
      display = {color = seagreen}
   },

   AnBlP = {
      name = "methyl nitro benzoic acid",
      carbon_number = 8,
      molar_mass = {g_mol = 167.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 88.e3},
      saturation_vapor_pressure = {Pa = 2.67e-7},
      partition_coefficient = {m3_ug = 55.56},
      display = {color = tomato}
   },

   AnBmP = {
      name = "methyl hydroxy benzoic acid",
      carbon_number = 8,
      molar_mass = {g_mol = 152.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 5.81e-2},
      vaporization_enthalpy = {J_mol = 88.e3},
      saturation_vapor_pressure = {Pa = 4.0e-4},
      display = {color = orange}
   },

   BiBlP = {
      name = "C15 hydroxy nitrate aldehyde",
      carbon_number = 15,
      molar_mass = {g_mol = 298.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 175.e3},
      saturation_vapor_pressure = {Pa = 8.0e-8},
      display = {color = chartreuse}
   },

   BiBmP = {
      name = "C15 oxo aldehyde",
      carbon_number = 15,
      molar_mass = {g_mol = 236.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 175.e3},
      saturation_vapor_pressure = {Pa = 4.0e-5},
      display = {color = darkgreen}
   },

   BiISO1 = {
      name = "isoprene oxidation product 1",
      carbon_number = 10,
      molar_mass = {g_mol = 118.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 42.e3},
      saturation_vapor_pressure = {Pa = 3.17e-3},
      partition_coefficient = {m3_ug = 0.00862},
      display = {color = limegreen}
   },

   BiISO2 = {
      name = "isoprene oxidation product 2",
      carbon_number = 15,
      molar_mass = {g_mol = 136.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.3},
      surface_tension = {N_m = 3.e-2},
      vaporization_enthalpy = {J_mol = 42.e3},
      saturation_vapor_pressure = {Pa = 1.46e-5},
      partition_coefficient = {m3_ug = 1.62},
      display = {color = yellowgreen}
   },

   NOND = {
      name = "nonadecane",
      carbon_number = 19,
      molar_mass = {g_mol = 268.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 0.786},
      surface_tension = {N_m = 2.81e-2},
      vaporization_enthalpy = {J_mol = 96.4e3},
      saturation_vapor_pressure = {Pa = 6.105e-4},
      partition_coefficient = {m3_ug = 5.76e-3},
      display = {color = olive}
   },

   PTCA = {
      name = "pentacosane",
      carbon_number = 25,
      molar_mass = {g_mol = 354.},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 0.778},
      surface_tension = {N_m = 2.93e-2},
      vaporization_enthalpy = {J_mol = 127.e3},
      saturation_vapor_pressure = {Pa = 5.08e-8},
      partition_coefficient = {m3_ug = 1.378e2},
      display = {color = peru}
   },

   --
   -- Several organic, mostly PAH, species found in biomass combustion smoke.
   --

  ACEN = {
      name = "acenaphtene",
      carbon_number = 12,
      molar_mass = {g_mol = 154.207},
      density = {g_cm3 = 1.22},
      surface_tension = {N_m = 4.92e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 49.7e3},
      saturation_vapor_pressure = {mmHg = 0.0102, antoine_law = {a = 8.13, b = 2367}},
      display = {color = darkred}
   },

   ANTH = {
      name = "anthracene",
      carbon_number = 14,
      molar_mass = {g_mol = 178.229},
      density = {g_cm3 = 1.28},
      surface_tension = {N_m = 4.8e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 55.8e3},
      saturation_vapor_pressure = {mmHg = 0.000494, antoine_law = {a = 8.59, b = 2872}},
      display = {color = sandybrown}
   },

   BZAN = {
      name = "benzo (a) anthracene",
      carbon_number = 18,
      molar_mass = {g_mol = 228.288},
      density = {g_cm3 = 1.2544},
      surface_tension = {N_m = 5.35e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 66.7e3},
      saturation_vapor_pressure = {mmHg = 8.05e-7, antoine_law = {a = 9.34, b = 3760}},
      display = {color = tan}
   },

   BZAP = {
      name = "benzo (a) pyrene",
      carbon_number = 20,
      molar_mass = {g_mol = 252.309},
      density = {g_cm3 = 1.24},
      surface_tension = {N_m = 4.64e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 53.8e3},
      saturation_vapor_pressure = {Pa = 0.00399, antoine_law = {a = 10.71, b = 4465}},
      display = {color = saddlebrown}
   },

   BZBF = {
      name = "benzo (b) fluoranthene",
      carbon_number = 20,
      molar_mass = {g_mol = 252.309},
      density = {g_cm3 = 1.287},
      surface_tension = {N_m = 6.35e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 70.2e3},
      saturation_vapor_pressure = {mmHg = 1.3e-5, antoine_law = {a = 9.48, b = 4578}},
      display = {color = goldenrod}
   },

   BZEP = {
      name = "benzo (e) phenantrene",
      carbon_number = 18,
      molar_mass = {g_mol = 228.288},
      density = {g_cm3 = 1.19},
      surface_tension = {N_m = 5.35e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 67.9e3},
      saturation_vapor_pressure = {mmHg = 1.26e-6, antoine_law = {a = 9.66, b = 4139}},
      display = {color = burlywood}
   },

   BZPE = {
      name = "benzo (g,h,i) perylene",
      carbon_number = 22,
      molar_mass = {g_mol = 276.33},
      density = {g_cm3 = 1.379},
      surface_tension = {N_m = 7.42e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 74.1e3},
      saturation_vapor_pressure = {mmHg = 6.08e-8, antoine_law = {a = 10.76, b = 4915}},
      display = {color = firebrick}
   },

   BZKF = {
      name = "benzo (k) fluoranthene",
      carbon_number = 20,
      molar_mass = {g_mol = 252.309},
      density = {g_cm3 = 1.287},
      surface_tension = {N_m = 6.35e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 71.6e3},
      saturation_vapor_pressure = {mmHg = 7.65e-8, antoine_law = {a = 9.48, b = 4427}},
      display = {color = crimson}
   },

   ALK19 = {
      name = "nonadecane",
      carbon_number = 19,
      molar_mass = {g_mol = 268.},
      density = {g_cm3 = 0.786},
      surface_tension = {N_m = 2.81e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 55.0e3},
      saturation_vapor_pressure = {mmHg = 5.76e-5},
      display = {color = darkkhaki}
   },

   ALK33 = {
      name = "tritiacontane",
      carbon_number = 33,
      molar_mass = {g_mol = 464.53},
      density = {g_cm3 = 0.8},
      surface_tension = {N_m = 3.03e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 71.e3},
      saturation_vapor_pressure = {mmHg = 1.17e-10},
      display = {color = indianred}
   },

   CHRY = {
      name = "chrysene",
      carbon_number = 18,
      molar_mass = {g_mol = 228.288},
      density = {g_cm3 = 1.274},
      surface_tension = {N_m = 5.35e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 67.9e3},
      saturation_vapor_pressure = {mmHg = 1.26e-6, antoine_law = {a = 9.66, b = 4139}},
      display = {color = chocolate}
   },

   CORO = {
      name = "coronene",
      carbon_number = 24,
      molar_mass = {g_mol = 300.352},
      density = {g_cm3 = 1.371},
      surface_tension = {N_m = 8.58e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 77.0e3},
      saturation_vapor_pressure = {mmHg = 4.e-8, antoine_law = {a = 10.67, b = 5446}},
      display = {color = rosybrown}
   },

   DBZA = {
      name = "dibenzo (a,h) anthracene",
      carbon_number = 22,
      molar_mass = {g_mol = 278.346},
      density = {g_cm3 = 1.2},
      surface_tension = {N_m = 5.77e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 76.9e3},
      saturation_vapor_pressure = {mmHg = 2.5e-7, antoine_law = {a = 9.82, b = 5002}},
      display = {color = maroon}
   },

   FLRT = {
      name = "fluoranthene",
      carbon_number = 16,
      molar_mass = {g_mol = 202.25},
      density = {g_cm3 = 1.252},
      surface_tension = {N_m = 5.94e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 59.8e3},
      saturation_vapor_pressure = {mmHg = 6.08e-5, antoine_law = {a = 9.03, b = 3323}},
      display = {color = tomato}
   },

   FLRE = {
      name = "fluorene",
      carbon_number = 13,
      molar_mass = {g_mol = 166.218},
      density = {g_cm3 = 1.203},
      surface_tension = {N_m = 4.62e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 51.2e3},
      saturation_vapor_pressure = {mmHg = 0.00464, antoine_law = {a = 8.63, b = 2614}},
      display = {color = coral}
   },

   INPY = {
      name = "indeno (1,2,3-c,d) pyrene",
      carbon_number = 22,
      molar_mass = {g_mol = 276.33},
      density = {g_cm3 = 1.4},
      surface_tension = {N_m = 7.42e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 73.6e3},
      saturation_vapor_pressure = {mmHg = 6.6e-6, antoine_law = {a = 9.6, b = 4839}},
      display = {color = orange}
   },

   LVG = {
      name = "levoglucosan",
      phase = {0, 1},
      carbon_number = 6,
      molar_mass = {g_mol = 162.},
      density = {g_cm3 = 1.688},
      surface_tension = {N_m = 7.07e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 73.1e3},
      saturation_vapor_pressure = {mmHg = 2.47e-6},
      henry_constant = {atm_m3_mol = 1.42e-13},
      display = {color = deepskyblue}
   },

   NAPT = {
      name = "naphtalene",
      carbon_number = 10,
      molar_mass = {g_mol = 128.171},
      density = {g_cm3 = 1.0253},
      surface_tension = {N_m = 4.02e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 43.9e3},
      saturation_vapor_pressure = {mmHg = 0.299, antoine_law = {a = 8.06, b = 1923}},
      display = {color = lightpink}
   },

   PETN = {
      name = "phenantrene",
      carbon_number = 14,
      molar_mass = {g_mol = 178.23},
      density = {g_cm3 = 1.174},
      surface_tension = {N_m = 4.8e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 55.8e3},
      saturation_vapor_pressure = {mmHg = 0.000656, antoine_law = {a = 9.07, b = 2982}},
      display = {color = magenta}
   },

   PTAN = {
      name = "phytane",
      carbon_number = 20,
      molar_mass = {g_mol = 282.547},
      density = {g_cm3 = 0.9},
      surface_tension = {N_m = 2.65e-2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      vaporization_enthalpy = {J_mol = 54.2e3},
      saturation_vapor_pressure = {mmHg = 0.00333},
      display = {color = peru}
   },

   PYR = {
      name = "pyrene",
      carbon_number = 16,
      molar_mass = {g_mol = 202.25},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      density = {g_cm3 = 1.271},
      surface_tension = {N_m = 5.94e-2},
      vaporization_enthalpy = {J_mol = 81.35e3},
      saturation_vapor_pressure = {mmHg = 7.97e-5, antoine_law = {a = 9.49, b = 3370}},
      partition_coefficient = {m3_ug = 2.009e-2},
      display = {color = mediumorchid}
   },

   RETE = {
      name = "retene",
      carbon_number = 18,
      molar_mass = {g_mol = 234.36},
      density = {g_cm3 = 1.04},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 5.e-2},
      vaporization_enthalpy = {J_mol = 61.7e3},
      saturation_vapor_pressure = {mmHg = 1.45e-5},
      display = {color = darksalmon}
   },

   WBSVOC1 = {
      name = "1st surrogate WB", -- (Shrivastava, 2006)
      carbon_number = 20,
      molar_mass = {g_mol = 250.},
      density = {g_cm3 = 1.2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 5.0e-2},
      vaporization_enthalpy = {J_mol = 70.e3},
      saturation_vapor_pressure = {ug_m3 = 20.},
      display = {color = orchid, label = "WBSVOC1"}
   },

   WBSVOC2 = {
      name = "2nd surrogate WB", -- (Shrivastava, 2006)
      carbon_number = 15,
      molar_mass = {g_mol = 200.},
      density = {g_cm3 = 1.2},
      molecular_diameter = {angstrom = 8.39},
      collision_factor = 687.,
      surface_tension = {N_m = 5.0e-2},
      vaporization_enthalpy = {J_mol = 50.e3},
      saturation_vapor_pressure = {ug_m3 = 1.646e3},
      display = {color = sienna, label = "WBSVOC2"}
   },


   --
   -- Some ionic species from Isoropia and AEC.
   --


   Hp = {
      molar_mass = {g_mol = 1.0},
      electric_charge = 1
   },

   Na_aq = {
      group = "Na"
   },

   Nap = {
      group = "Na",
      molar_mass = {g_mol = 22.},
      electric_charge = 1
   },

   NH3aq = {
      group = "NH3"
   },

   NH4p = {
      group = "NH3",
      molar_mass = {g_mol = 18.},
      electric_charge = 1
   },

   HClaq = {
      group = "HCl"
   },

   Clm = {
      group = "HCl",
      molar_mass = {g_mol = 35.5},
      electric_charge = -1
   },

   H2SO4aq = {
      group = "H2SO4"
   },

   HSO4m = {
      group = "H2SO4",
      molar_mass = {g_mol = 97.},
      electric_charge = -1
   },

   SO42m = {
      group = "H2SO4",
      molar_mass = {g_mol = 96.},
      electric_charge = -2
   },

   HNO3aq = {
      group = "HNO3"
   },

   NO3m = {
      group = "HNO3",
      molar_mass = {g_mol = 62.},
      electric_charge = -1
   },

   OHm = {
      molar_mass = {g_mol = 17.},
      electric_charge = -1
   },

   BiA2Daq = {
      group = "BiA2D",
      phase = 0
   },

   BiA2Dm = {
      group = "BiA2D",
      molar_mass = {g_mol = 185.},
      electric_charge = -1
   },

   BiA2D2m = {
      group = "BiA2D",
      molar_mass = {g_mol = 184.},
      electric_charge = -2
   },

   BiA1Daq = {
      group = "BiA1D",
      phase = 0
   },

   BiA1Dm = {
      group = "BiA1D",
      molar_mass = {g_mol = 169.},
      electric_charge = -1
   },

   BiA0Daq = {
      group = "BiA0D",
      phase = 0
   },

   GLYaq = {
      group = "GLY",
      phase = 0
   },

   MGLYaq = {
      group = "MGLY",
      phase = 0
   },


   --
   -- Some solid species from Isoropia.
   --


   NaNO3 = {
      group = {"Na", "HNO3"},
      phase = -1,
      molar_mass = {g_mol = 85.},
      density = {g_cm3 = 2.26}
   },

   NH4NO3 = {
      group = {"NH3", "HNO3"},
      phase = -1,
      molar_mass = {g_mol = 80.},
      density = {g_cm3 = 1.725}
   },

   NaCl = {
      group = {"Na", "HCl"},
      phase = -1,
      molar_mass = {g_mol = 58.5},
      density = {g_cm3 = 2.165}
   },

   NH4Cl = {
      group = {"NH3", "HCl"},
      phase = -1,
      molar_mass = {g_mol = 53.5},
      density = {g_cm3 = 1.53}
   },

   Na2SO4 = {
      group = {"Na", "H2SO4"},
      phase = -1,
      molar_mass = {g_mol = 142.},
      density = {g_cm3 = 2.7}
   },

   NH4_2_SO4 = {
      group = {"NH3", "H2SO4"},
      phase = -1,
      molar_mass = {g_mol = 132.},
      density = {g_cm3 = 1.77}
   },

   NaHSO4 = {
      group = {"Na", "H2SO4"},
      phase = -1,
      molar_mass = {g_mol = 120.},
      density = {g_cm3 = 2.74}
   },

   NH4HSO4 = {
      group = {"NH3", "H2SO4"},
      phase = -1,
      molar_mass = {g_mol = 115.},
      density = {g_cm3 = 1.78}
   },

   NH4_4_H_SO4_2 = {
      group = {"NH3", "H2SO4"},
      phase = -1,
      molar_mass = {g_mol = 247.},
      density = {g_cm3 = 1.77}
   }
   }
