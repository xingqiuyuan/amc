// Copyright (C) 2013-2015 INERIS
// Author(s) : Edouard Debry
// 
// This file is part of AMC.
// 
// AMC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// AMC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with AMC.  If not, see <http://www.gnu.org/licenses/>.

#ifndef AMC_FILE_CLASS_SPECIES_CXX

#include "ClassSpecies.hxx"

namespace AMC
{
  // Function sorting the saturation vapor concentration field.
  bool ClassSpecies::sort_saturation_vapor_concentration(int i, int j)
  {
    return saturation_vapor_concentration_[semivolatile_[i]] < saturation_vapor_concentration_[semivolatile_[j]];
  }


  // Init method.
  void ClassSpecies::Init(Ops::Ops &ops)
  {
    string prefix_orig = ops.GetPrefix();

    ops.Set("species.model", "", name_);
    Nspecies_ = int(name_.size());

    drh_.resize(Nspecies_);
    partition_coefficient_.resize(Nspecies_);
    molar_mass_.resize(Nspecies_);
    density_.resize(Nspecies_);
    surface_tension_.resize(Nspecies_);
    sorption_site_.resize(Nspecies_);
    vaporization_enthalpy_.resize(Nspecies_);
    accomodation_coefficient_.resize(Nspecies_);
    molecular_diameter_.resize(Nspecies_);
    collision_factor_.resize(Nspecies_);
    saturation_vapor_pressure_.resize(Nspecies_);
    molar_volume_.resize(Nspecies_);
    henry_constant_.resize(Nspecies_);
    is_organic_.resize(Nspecies_);
    is_tracer_.resize(Nspecies_);
    phase_.resize(Nspecies_);
    semivolatile_reverse_.assign(Nspecies_, -1);
    carbon_number_.assign(Nspecies_, 0);
    long_name_.resize(Nspecies_);
    antoine_law_a_.assign(Nspecies_, real(-1));
    antoine_law_b_.assign(Nspecies_, real(-1));
    use_antoine_law_.assign(Nspecies_, false);

    // Display.
    color_.assign(Nspecies_, "");
    hatch_.assign(Nspecies_, "");
    label_.assign(Nspecies_, "");

    // Clear tracer class.
    ClassSpeciesTracer::Clear();

    // Which species are considered as semivolatile, regardless of their true properties.
    Ngas_ = 0;
    semivolatile_.clear();

    if (ops.Exists("species.not_semivolatile"))
      {
        vector1s not_semivolatile;
        ops.Set("species.not_semivolatile", "", not_semivolatile);

        int h(0);
        for (int i = 0; i < Nspecies_; ++i)
          {
            bool is_semivolatile(true);
            for (int j = 0; j < int(not_semivolatile.size()); ++j)
              if (name_[i] == not_semivolatile[j])
                {
                  is_semivolatile = false;
                  h++;
                  break;
                }

            if (is_semivolatile)
              {
                semivolatile_.push_back(i);
                Ngas_++;
              }
          }

        if (h < int(not_semivolatile.size()))
          throw AMC::Error("Some non semivolatile species were not found in the model species list.");
      }
    else if (ops.Exists("species.semivolatile"))
      {
        vector1s semivolatile;
        ops.Set("species.semivolatile", "", semivolatile);

        int h(0);
        for (int i = 0; i < Nspecies_; ++i)
          {
            bool is_semivolatile(false);
            for (int j = 0; j < int(semivolatile.size()); ++j)
              if (name_[i] == semivolatile[j])
                {
                  is_semivolatile = true;
                  h++;
                  break;
                }

            if (is_semivolatile)
              {
                semivolatile_.push_back(i);
                Ngas_++;
              }
          }

        if (h < int(semivolatile.size()))
          throw AMC::Error("Some semivolatile species were not found in the model species list.");
      }
    else
      {
        // By default, all species are semivolatile.
        Ngas_ = Nspecies_;
        semivolatile_.resize(Nspecies_);
        for (int i = 0; i < Nspecies_; ++i)
          semivolatile_[i] = i;
      }

    for (int i = 0; i < Ngas_ - 1; i++)
      if (semivolatile_[i] >= semivolatile_[i + 1])
        throw AMC::Error("Semivolatile species have to be given in ascending order.");

    for (int i = 0; i < Ngas_; i++)
      semivolatile_reverse_[semivolatile_[i]] = i;

    //
    // Species physico-chemical properties.
    //

    ops.SetPrefix("species.");
    molar_mass_average_ = ops.Get<real>("average_molar_mass", "", AMC_SPECIES_MOLAR_MASS_AVERAGE) * real(1e6);

    for (int i = 0; i < Nspecies_; i++)
      {
        ops.SetPrefix("species.");

        // Is species is an alias to another one.
        if (ops.Is<string>(name_[i]))
          ops.SetPrefix("species." + ops.Get<string>(name_[i]) + ".");
        else
          ops.SetPrefix("species." + name_[i] + ".");

        // Long name of species.
        long_name_[i] = ops.Get<string>("name", "", name_[i]);

        // Is species a tracer ? No by default.
        is_tracer_[i] = ops.Get<bool>("is_tracer", "", false);
        if (is_tracer_[i])
          ClassSpeciesTracer::Add(name_[i], ops);

        // Is species organic ? Yes by default.
        if (is_tracer_[i])
          is_organic_[i] = ClassSpeciesTracer::IsOrganic(-1, ops);
        else
          is_organic_[i] = ops.Get<bool>("is_organic", "", true);

        // Prefered phase of species. If species not organic, much likely aqueous.
        if (ops.Exists("phase"))
          {
            if (ops.Is<int>("phase"))
              {
                phase_[i].resize(1);
                phase_[i][0] = ops.Get<int>("phase");
              }
            else
              ops.Set("phase", "", phase_[i]);
          }
        else
          {
            phase_[i].resize(1);
            if (is_organic_[i])
              phase_[i][0] = 1;
            else
              phase_[i][0] = AMC_AQUEOUS_PHASE_INDEX;
          }

        // How many carbon molecules does have the species ? If not organic, much likely 0.
        if (! is_organic_[i])
          carbon_number_[i] = ops.Get<int>("carbon_number", "", 0);
        else
          {
            if (is_tracer_[i])
              carbon_number_[i] = ClassSpeciesTracer::ComputeCarbonNumber(-1, ops);
            else
              carbon_number_[i] = ops.Get<int>("carbon_number");

            if (carbon_number_[i] == 0)
              throw AMC::Error("Organic species \"" + name_[i] + "\" has not been given any carbon atoms.");
          }

        // Molar mass.
        if (is_tracer_[i])
          molar_mass_[i] = ClassSpeciesTracer::ComputeMolarMass(-1, ops);
        else
          if (ops.Exists("molar_mass")) // Convert from g.mol^{-1} to µg.mol^{-1}.
            molar_mass_[i] = ops.Get<real>("molar_mass.g_mol") * real(1e6);
          else
            throw AMC::Error("No molar mass specified for species \"" + name_[i] + "\".");

        // Specific density.
        if (is_tracer_[i])
          density_[i] = ClassSpeciesTracer::ComputeDensity(-1, ops);
        else
          if (ops.Exists("density")) // Convert from g.cm^{-3} to µg.µm^{-3}.
            density_[i] = ops.Get<real>("density.g_cm3") * real(AMC_CONVERT_FROM_GCM3_TO_MICROGMICROM3);
          else
            throw AMC::Error("No density specified for species \"" + name_[i] + "\".");

        // Molar volume in m^3.mol^{-1}.
        molar_volume_[i] = molar_mass_[i] / density_[i] * 1.e-18;

        // Sorption site in #.cm^{-2}. 1e-4 factor acount for translation of m2 to cm2.
        sorption_site_[i] = pow(real(1) / (molar_volume_[i] * sqrt(AMC_AVOGADRO_NUMBER)), real(2) * AMC_FRAC3) * real(1.e-4);
        sorption_site_[i] = ops.Get<real>("sorption_site", "", sorption_site_[i]);

        // Surface tension in N.m{-1}.
        surface_tension_[i] = real(0);
        if (ops.Exists("surface_tension"))
          surface_tension_[i] = ops.Get<real>("surface_tension.N_m", "", surface_tension_[i]);

        // Enthalpy of vaporization in J.mol^{-1}.
        vaporization_enthalpy_[i] = real(0);
        if (ops.Exists("vaporization_enthalpy"))
          vaporization_enthalpy_[i] = ops.Get<real>("vaporization_enthalpy.J_mol", "", vaporization_enthalpy_[i]);

        // Adimensional, between ]0, 1].
        accomodation_coefficient_[i] = ops.Get<real>("accomodation", "v > 0 and v <= 1", real(1));

        // Molecular diameter in Angstrom.
        molecular_diameter_[i] = real(0);
        if (ops.Exists("molecular_diameter"))
          molecular_diameter_[i] = ops.Get<real>("molecular_diameter.angstrom", "", molecular_diameter_[i]);

        // Collision factor of molecule.
        collision_factor_[i] = ops.Get<real>("collision_factor", "", real(0));

        // Deliquescence relative humidity, between [0, 1].
        drh_[i] = ops.Get<real>("drh", "", real(0));

        // Saturation vapor pressure in Pascal.
        saturation_vapor_pressure_[i] = real(0);
        if (ops.Exists("saturation_vapor_pressure"))
          {
            if (ops.Exists("saturation_vapor_pressure.ug_m3"))
              saturation_vapor_pressure_[i] = ops.Get<real>("saturation_vapor_pressure.ug_m3")
                * AMC_IDEAL_GAS_CONSTANT * AMC_TEMPERATURE_REFERENCE / molar_mass_[i];

            if (ops.Exists("saturation_vapor_pressure.torr"))
              saturation_vapor_pressure_[i] = ops.Get<real>("saturation_vapor_pressure.torr") * AMC_CONVERT_FROM_TORR_TO_PASCAL;

            if (ops.Exists("saturation_vapor_pressure.mmHg"))
              saturation_vapor_pressure_[i] = ops.Get<real>("saturation_vapor_pressure.mmHg") * AMC_CONVERT_FROM_MMHG_TO_PASCAL;

            if (ops.Exists("saturation_vapor_pressure.Pa"))
              saturation_vapor_pressure_[i] = ops.Get<real>("saturation_vapor_pressure.Pa");

            if (ops.Exists("saturation_vapor_pressure.antoine_law"))
              {
                antoine_law_a_[i] = ops.Get<real>("saturation_vapor_pressure.antoine_law.a");
                antoine_law_b_[i] = ops.Get<real>("saturation_vapor_pressure.antoine_law.b");

                // Default is not to use it except if no vapor pressure provided before.
                use_antoine_law_[i] = ops.Get<real>("saturation_vapor_pressure.antoine_law.use",
                                                    "", saturation_vapor_pressure_[i] == real(0));

                if (use_antoine_law_[i])
                  saturation_vapor_pressure_[i] =
                    pow(real(10), antoine_law_a_[i] - antoine_law_b_[i] / AMC_TEMPERATURE_REFERENCE);
              }
          }

        // Partition coefficient in m^3.µg^{-1}.
        // A null partition coefficient means the component is purely volatile.
        if (ops.Exists("partition_coefficient"))
          partition_coefficient_[i] = ops.Get<real>("partition_coefficient.m3_ug");
        else if (saturation_vapor_pressure_[i] > real(0))
          partition_coefficient_[i] = AMC_IDEAL_GAS_CONSTANT * AMC_TEMPERATURE_REFERENCE
            / (saturation_vapor_pressure_[i] * molar_mass_average_);
        else
          partition_coefficient_[i] = real(AMC_SPECIES_PARTITION_COEFFICIENT_DEFAULT);

        // Henry's law constant in mol.L^{-1}.atm^{-1}.
        // A null constant means the component is totally volatile.
        henry_constant_[i] = real(0);
        if (ops.Exists("henry_constant"))
          {
            if (ops.Exists("henry_constant.m3_ug"))
              henry_constant_[i] = ops.Get<real>("henry_constant.m3_ug")
                * real(AMC_PRESSURE_ATM) * real(1e9) / (AMC_IDEAL_GAS_CONSTANT * AMC_TEMPERATURE_REFERENCE);

            // Convert to mol.L^{-1}.atm^{-1}, 1e-3 accounts for conversion from m3 to Liter.
            if (ops.Exists("henry_constant.atm_m3_mol"))
              henry_constant_[i] = real(1.e-3) / ops.Get<real>("henry_constant.atm_m3_mol");

            if (ops.Exists("henry_constant.mol_L_atm"))
              henry_constant_[i] = ops.Get<real>("henry_constant.mol_L_atm");
          }

        // Display, not needed at run time.
        label_[i] = long_name_[i];
        if (ops.Exists("display"))
          {
            color_[i] = ops.Get<string>("display.color", "", color_[i]);
            hatch_[i] = ops.Get<string>("display.hatch", "", hatch_[i]);
            label_[i] = ops.Get<string>("display.label", "", label_[i]);
          }
      }

    // Compute saturation vapor concentration thanks to perfect gas law (µg.m^{-3}).
    saturation_vapor_concentration_.assign(Nspecies_, real(0));

    for (int i = 0; i < Nspecies_; ++i)
      saturation_vapor_concentration_[i] = saturation_vapor_pressure_[i] * molar_mass_[i]
        / (AMC_IDEAL_GAS_CONSTANT * AMC_TEMPERATURE_REFERENCE);

    // Property map.
    property_["density"] = &density_;
    property_["molar mass"] = &molar_mass_;
    property_["molar volume"] = &molar_volume_;
    property_["surface tension"] = &surface_tension_;
    property_["sorption site"] = &sorption_site_;
    property_["drh"] = &drh_;
    property_["saturation vapor pressure"] = &saturation_vapor_pressure_;
    property_["saturation vapor concentration"] = &saturation_vapor_concentration_;
    property_["partition coefficient"] = &partition_coefficient_;
    property_["henry constant"] = &henry_constant_;
    property_["vaporization enthalpy"] = &vaporization_enthalpy_;
    property_["accomodation coefficient"] = &accomodation_coefficient_;
    property_["molecular diameter"] = &molecular_diameter_;
    property_["collision factor"] = &collision_factor_;
    property_["antoine law a"] = &antoine_law_a_;
    property_["antoine law b"] = &antoine_law_b_;
    property_["use antoine law"] = new vector1r(Nspecies_);
    property_["carbon number"] = new vector1r(Nspecies_);
    property_["is organic ?"] = new vector1r(Nspecies_);
    property_["is tracer ?"] = new vector1r(Nspecies_);

    for (int i = 0; i < Nspecies_; ++i)
      {
        (*property_["use antoine law"])[i] = use_antoine_law_[i] ? real(1) : real(0);
        (*property_["carbon number"])[i] = real(carbon_number_[i]);
        (*property_["is organic ?"])[i] = is_organic_[i] ? real(1) : real(0);
        (*property_["is tracer ?"])[i] = is_tracer_[i] ? real(1) : real(0);
      }

    // Property unit map.
    property_unit_["density"] = "µg.µm^{-3}";
    property_unit_["molar mass"] = "µg.mol^{-1}";
    property_unit_["molar volume"] = "m^3.mol^{-1}";
    property_unit_["surface tension"] = "N.m^{-1}";
    property_unit_["sorption site"] = "#.cm^{-2}";
    property_unit_["drh"] = "[0,1]";
    property_unit_["saturation vapor pressure"] = "Pascal";
    property_unit_["saturation vapor concentration"] = "µg.m^{-3}";
    property_unit_["partition coefficient"] = "m^3.µg^{-1}";
    property_unit_["henry constant"] = "mol.L^{-1}.atm^{-1}";
    property_unit_["vaporization enthalpy"] = "J.mol^{-1}";
    property_unit_["accomodation coefficient"] = "]0, 1]";
    property_unit_["molecular diameter"] = "angstrom";
    property_unit_["collision factor"] = "real > 0";
    property_unit_["antoine law a"] = "real > 0";
    property_unit_["antoine law b"] = "real > 0";
    property_unit_["use antoine law"] = "bool";
    property_unit_["carbon number"] = "int >= 0";
    property_unit_["is organic ?"] = "bool";
    property_unit_["is tracer ?"] = "bool";


    // Possibly, redefine display for species.
    ops.SetPrefix(prefix_orig);
    if (ops.Exists("species.display"))
      {
        ops.SetPrefix(prefix_orig + "species.display.");

        vector1s group = ops.GetEntryList();

        for (int h = 0; h < int(group.size()); ++h)
          {
            ops.SetPrefix(prefix_orig + "species.display." + group[h] + ".");
            vector1s name;
            ops.Set("species", "", name);

            for (int i = 0; i < int(name.size()); ++i)
              {
                const int j = GetIndex(name[i]);
                if (j < 0) throw AMC::Error("Species \"" + name[i] + "\" not found in AMC species list.");

                color_[j] = ops.Get<string>("color", "", color_[i]);
                hatch_[j] = ops.Get<string>("hatch", "", hatch_[i]);
                label_[j] = ops.Get<string>("label", "", group[h]);
              }
          }

        ops.SetPrefix(prefix_orig);
      }

#ifdef AMC_WITH_LOGGER
    *AMCLogger::GetLog() << Fred() << Info() << Reset() << "Loaded " << Nspecies_ << " species." << endl;
    *AMCLogger::GetLog() << Fred() << Info(1) << Reset() << "List of model species : " << name_ << endl;
    *AMCLogger::GetLog() << Bred() << Info() << Reset() << "Number of semivolatile species among them : " << Ngas_ << endl;
    *AMCLogger::GetLog() << Bblue() << Info(1) << Reset() << "List of semivolatile among them : " << semivolatile_ << endl;
    *AMCLogger::GetLog() << Bblue() << Debug() << Reset() << "Semivolatile ? " << semivolatile_reverse_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "color = " << color_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "hatch = " << hatch_ << endl;
    *AMCLogger::GetLog() << Fgreen() << Debug(3) << Reset() << "label = " << label_ << endl;
#endif
  }


  // Clear method.
  void ClassSpecies::Clear()
  {
    // Always clear map first.
    property_.clear();
    property_unit_.clear();

    ClassSpeciesTracer::Clear();

    Nspecies_ = 0;
    Ngas_ = 0;
    name_.clear();
    color_.clear();
    hatch_.clear();
    label_.clear();
    drh_.clear();
    molar_mass_average_ = AMC_SPECIES_MOLAR_MASS_AVERAGE;
    molar_mass_.clear();
    molar_volume_.clear();
    density_.clear();
    surface_tension_.clear();
    sorption_site_.clear();
    saturation_vapor_pressure_.clear();
    saturation_vapor_concentration_.clear();
    partition_coefficient_.clear();
    henry_constant_.clear();
    vaporization_enthalpy_.clear();
    accomodation_coefficient_.clear();
    molecular_diameter_.clear();
    collision_factor_.clear();
    semivolatile_.clear();
    semivolatile_reverse_.clear();
    is_organic_.clear();
    is_tracer_.clear();
    carbon_number_.clear();
    long_name_.clear();
    phase_.clear();
    antoine_law_a_.clear();
    antoine_law_b_.clear();
    use_antoine_law_.clear();
  }


  // Get methods.
  int ClassSpecies::GetNspecies()
  {
    return Nspecies_;
  }


  int ClassSpecies::GetNgas()
  {
    return Ngas_;
  }


  string ClassSpecies::GetName(const int &i)
  {
    return name_[i];
  }


  string ClassSpecies::GetLongName(const int &i)
  {
    return long_name_[i];
  }


  vector<string> ClassSpecies::GetNameList()
  {
    return name_;
  }


  vector<string> ClassSpecies::GetLongNameList()
  {
    return long_name_;
  }


  string ClassSpecies::GetStr(const int &i)
  {
    ostringstream sout;

    if (i< 0 || i >= Nspecies_)
      throw AMC::Error("Wrong species index, got " + to_str(i) +
                       ", expected [0, " + to_str(Nspecies_) + "[.");

    sout << setw(35) << right << "Name : " << setw(10) << left << name_[i] << endl
         << setw(35) << right << "Long name : " << setw(10) << left << long_name_[i] << endl
         << setw(35) << right << "Organic ? : " << setw(10) << left << (is_organic_[i] ? "yes" : "no") << endl
         << setw(35) << right << "Tracer ? : " << setw(10) << left << (is_tracer_[i] ? "yes" : "no") << endl
         << setw(35) << right << "Phase : " << left << phase_[i] << endl
         << setw(35) << right << "Carbon number : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << carbon_number_[i] << property_unit_["carbon number"] << endl
         << setw(35) << right << "Molar mass : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << molar_mass_[i] << property_unit_["molar mass"] << endl
         << setw(35) << right << "Density : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << density_[i] << property_unit_["density"] << endl
         << setw(35) << right << "Molar volume : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << molar_volume_[i] << property_unit_["molar volume"] << endl
         << setw(35) << right << "Sorption site : " << scientific << setprecision(2) << left
         << setw(10) << setfill(' ') << sorption_site_[i] << property_unit_["sorption site"] << endl;

    if (surface_tension_[i] > real(0))
      sout << setw(35) << right << "Surface tension : " << scientific << setprecision(2) << left << setw(10)
           << setfill(' ') << surface_tension_[i] << property_unit_["surface tension"] << endl;

    if (vaporization_enthalpy_[i] > real(0))
      sout << setw(35) << right << "Vaporization enthalpy : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << vaporization_enthalpy_[i] << property_unit_["vaporization enthalpy"] << endl;

    if (accomodation_coefficient_[i] < real(1))
      sout << setw(35) << right << "Accomodation coefficient : " << scientific << setprecision(2) << left << setw(10)
           << setfill(' ') << accomodation_coefficient_[i] << property_unit_["accomodation coefficient"] << endl;

    if (molecular_diameter_[i] > real(0))
      sout << setw(35) << right << "Molecular diameter : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << molecular_diameter_[i] << property_unit_["molecular diameter"] << endl;

    if (collision_factor_[i] > real(0))
      sout << setw(35) << right << "Collision factor : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << collision_factor_[i] << property_unit_["collision factor"] << endl;

    if (drh_[i] > real(0))
      sout << setw(35) << right << "DRH : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << drh_[i] << property_unit_["drh"] << endl;

    if (partition_coefficient_[i] != real(AMC_SPECIES_PARTITION_COEFFICIENT_DEFAULT))
      sout << setw(35) << right << "Partition coefficient : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << partition_coefficient_[i] << property_unit_["partition coefficient"] << endl;

    if (saturation_vapor_pressure_[i] > real(0))
      sout << setw(35) << right << "Saturation vapor pressure : " << scientific << setprecision(2) << left
           << setw(10) << setfill(' ') << saturation_vapor_pressure_[i] << property_unit_["saturation vapor pressure"] << endl;

    if (henry_constant_[i] > real(0))
      sout << setw(35) << right << "Henry constant : " << scientific << setprecision(2) << left << setw(10) << setfill(' ')
           << henry_constant_[i] << property_unit_["henry constant"] << endl;

    return sout.str();
  }


  vector<int> ClassSpecies::GetSemivolatile()
  {
    return semivolatile_;
  }


  void ClassSpecies::GetSemivolatile(vector<int> &semivolatile)
  {
    semivolatile = semivolatile_;
  }


  void ClassSpecies::GetNotSemivolatile(vector<int> &not_semivolatile)
  {
    not_semivolatile.clear();
    for (int i = 0; i < Nspecies_; ++i)
      if (semivolatile_reverse_[i] < 0)
        not_semivolatile.push_back(i);
  }


  real ClassSpecies::GetDensity(const int &i)
  {
    return density_[i];
  }


  real ClassSpecies::GetMolarMass(const int &i)
  {
    return molar_mass_[i];
  }


  real ClassSpecies::GetCollisionFactor(const int &i)
  {
    return collision_factor_[i];
  }


  real ClassSpecies::GetMolecularDiameter(const int &i)
  {
    return molecular_diameter_[i];
  }


  int ClassSpecies::GetCarbonNumber(const int &i)
  {
    return carbon_number_[i];
  }


  real ClassSpecies::GetProperty(const string &name, const int i)
  {
    if (i < Nspecies_)
      for (map<string, vector1r*>::iterator it = property_.begin(); it != property_.end(); ++it)
        if ((it->first).find(name) != string::npos)
          return (*it->second)[i];

    // Return negative value if nothing found.
    return real(-1);
  }


  string ClassSpecies::GetPropertyUnit(const string &name)
  {
    for (map<string, string>::iterator it = property_unit_.begin(); it != property_unit_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return (it->second);

    return "Property \"" + name + "\" not found.";
  }


  vector<string> ClassSpecies::GetPropertyList()
  {
    vector<string> ls;
    for (map<string, vector1r* >::iterator it = property_.begin(); it != property_.end(); ++it)
      ls.push_back(it->first);

    return ls;
  }


  vector<real> ClassSpecies::GetProperty(const string &name)
  {
    for (map<string, vector1r*>::iterator it = property_.begin(); it != property_.end(); ++it)
      if ((it->first).find(name) != string::npos)
        return *(it->second);

    return vector1r();
  }


  int ClassSpecies::GetIndex(const string &name, const bool with_long_name)
  {
    int i(0);
    for (i = 0; i < Nspecies_; i++)
      if (name_[i] == name)
        return i;
      else if (with_long_name)
        if (name == long_name_[i])
          return i;

    return -1;
  }


  int ClassSpecies::GetIndexGas(const string &name)
  {
    int i(0);
    for (i = 0; i < Nspecies_; i++)
      if (name_[i] == name)
        return semivolatile_reverse_[i];

    return -1;
  }


  int ClassSpecies::GetIndexGas(const int &i)
  {
    return semivolatile_reverse_[i];
  }


  bool ClassSpecies::IsOrganic(const int &i)
  {
    return is_organic_[i];
  }


  bool ClassSpecies::IsTracer(const int &i)
  {
    return is_tracer_[i];
  }


  bool ClassSpecies::IsSemivolatile(const int &i)
  {
    return semivolatile_reverse_[i] >= 0;
  }


  void ClassSpecies::GetPhase(const int &i, vector<int> &phase)
  {
    phase = phase_[i];
  }


  // Display.
  string ClassSpecies::GetColor(const int &i)
  {
    return color_[i];
  }


  string ClassSpecies::GetHatch(const int &i)
  {
    return hatch_[i];
  }


  string ClassSpecies::GetLabel(const int &i)
  {
    return label_[i];
  }


  // Get sorted saturation vapor concentration.
  void ClassSpecies::GetSortedSaturationVaporConcentration(vector<int> &index_ascending,
                                                           vector<real> &saturation_vapor_concentration)
  {
    index_ascending = semivolatile_;

    // Sort them in ascending order.
    std::sort(index_ascending.begin(), index_ascending.end(), sort_saturation_vapor_concentration);

    saturation_vapor_concentration.clear();

    for (int i = 0; i < Ngas_; ++i)
      saturation_vapor_concentration.push_back(saturation_vapor_concentration_[index_ascending[i]]);
  }


  // Compute saturation vapor pressure from Antoine Law (if exists).
  real ClassSpecies::ComputeSaturationVaporPressureFromAntoineLaw(const int i)
  {
    if (antoine_law_a_[i] > real(0) && antoine_law_b_[i] > real(0))
      return pow(10, antoine_law_a_[i] - antoine_law_b_[i] / AMC_TEMPERATURE_REFERENCE);
    else
      return real(-1);
  }


  // Compute OM to OC ratio, only relevant if species is organic.
  real ClassSpecies::ComputeRatioOMtoOC(const int i)
  {
    if (is_organic_[i])
      return molar_mass_[i] / (carbon_number_[i] * AMC_CARBON_MOLAR_MASS);
    else
      return real(-1);
  }


  // Display table of properties.
  string ClassSpecies::DisplayPropertyTable(vector<string> name)
  {
    // Determine property list name.
    int h(0);
    for (int i = 0; i < (name.size()); ++i)
      for (map<string, vector1r*>::iterator it = property_.begin(); it != property_.end(); ++it)
        if ((it->first).find(name[i]) != string::npos)
          {
            name[h++] = it->first;
            break;
          }

    name.resize(h);

    // Size of table.
    const int nrow = Nspecies_ + 2;
    const int ncol = int(name.size()) + 2;

    // Fill each cell of table.
    vector<vector1s > cell(nrow, vector1s(ncol, ""));

    cell[0][0] = "Name";
    cell[0][1] = "Long name";

    cell[1][0] = "string";
    cell[1][1] = "string";

    for (int j = 2; j < ncol; ++j)
      {
        cell[0][j] = name[j - 2];
        const string unit = property_unit_[name[j - 2]];

        if (unit.find("^") != string::npos ||
            unit.find(".") != string::npos ||
            unit.find("{") != string::npos)
          cell[1][j] = "$" + unit + "$";
        else
          cell[1][j] = unit;
      }

    h = 2;
    for (int i = 0; i < Nspecies_; ++i)
      {
        cell[h][0] = name_[i];
        cell[h][1] = long_name_[i];

        size_t p = cell[h][1].find_first_not_of(" ");
        cell[h][1].erase(0, p);

        p = cell[h][1].find_last_not_of(" ");
        if (p != string::npos)
          cell[h][1].erase(p + 1);

        for (int j = 2; j < ncol; ++j)
          {
            const real value = (*property_[name[j - 2]])[i];

            if (cell[1][j] == "bool")
              cell[h][j] = value == 1 ? "yes" : "no";
            else
              {
                ostringstream sval;
                //sval << "\\num{" << setprecision(3) << value << "}";
                sval << "$" << setprecision(3) << value << "$";
                cell[h][j] = sval.str();
              }
          }

        h++;
      }

    // Compute width for each column.
    vector1i width(ncol, 0);

    for (int i = 0; i < nrow; ++i)
      for (int j = 0; j < ncol; ++j)
        if (width[j] < int(cell[i][j].size()))
          width[j] = int(cell[i][j].size());

    // Turn table into a string.
    stringstream sout;

    sout << "|";
    for (int j = 0; j < ncol; ++j)
      sout << " " << setw(width[j]) << cell[0][j] << " |";
    sout << endl;

    sout << "|";
    for (int j = 0; j < ncol; ++j)
      sout << " " << setw(width[j]) << cell[1][j] << " |";
    sout << endl;

    sout << "|";
    for (int j = 0; j < ncol - 1; ++j)
      sout << setw(width[j]) << setfill('-') << "+";
    sout << setw(width.back()) << setfill('-') << "|" << endl;

    sout << setfill(' ');
    for (int i = 2; i < nrow; ++i)
      {
        sout << "|";
        for (int j = 0; j < ncol; ++j)
          sout << " " << setw(width[j]) << cell[i][j] << " |";
        sout << endl;
      }

    return sout.str();
  }


  // Generate random composition.
  inline void ClassSpecies::GenerateRandomComposition(vector1r &composition)
  {
    generate_random_vector(Nspecies_, composition);
  }


  vector<real> ClassSpecies::GenerateRandomComposition()
  {
    vector<real> composition(Nspecies_, real(0));
    GenerateRandomComposition(composition);
    return composition;
  }
}

#define AMC_FILE_CLASS_SPECIES_CXX
#endif
